/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Simulation/Simulator/simulator_core_impl.h"

#include "Core/Communication/FoxgloveWebsocketServer/foxglove_websocket_server.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Simulation/Simulator/exception.h"
#include "Core/Tests/TestUtils/MockEnvironment/mock_environment.h"
#include "Core/Tests/TestUtils/MockScenarioEngine/mock_scenario_engine.h"

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <google/protobuf/util/message_differencer.h>
#include <gtest/gtest.h>

#include <memory>

namespace astas::simulation::simulator
{

using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;

class SimulatorCoreTestFixture : public testing::Test
{
  protected:
    std::unique_ptr<SimulatorCoreImpl> CreateDefaultSimulator()
    {
        SimulationParameters params;
        params.custom_astas_data_directory = astas_custom_data_directory_;

        auto user_settings = CreateDefaultSettings();

        return CreateSimulatorWithMockScenarioEngine(params, user_settings);
    }

    std::unique_ptr<simulation::simulator::SimulatorCoreImpl> CreateSimulatorWithMockScenarioEngine(
        const SimulationParameters& params,
        const service::user_settings::UserSettings& user_settings)
    {
        auto environment = std::make_shared<NiceMock<test_utils::MockEnvironment>>();

        if (!environment)
        {
            throw std::runtime_error("MockEnvironment could not be created!");
        }

        mock_environment_ = std::static_pointer_cast<test_utils::MockEnvironment>(environment);

        auto scenario_engine = std::make_unique<NiceMock<test_utils::MockScenarioEngine>>();

        if (!scenario_engine)
        {
            throw std::runtime_error("MockScenarioEngine could not be created!");
        }

        mock_scenario_engine_ = scenario_engine.get();

        if (user_settings.host_vehicle.host_control_mode == service::user_settings::HostControlMode::kExternal)
        {
            EXPECT_CALL(*mock_scenario_engine_, ActivateExternalHostControl()).Times(1);
            scenario_engine->ActivateExternalHostControl();
        }

        std::unique_ptr<simulation::simulator::SimulatorCoreImpl> simulator =
            std::make_unique<simulation::simulator::SimulatorCoreImpl>(
                std::move(scenario_engine), environment, params, user_settings);

        return simulator;
    }

    service::user_settings::UserSettings CreateDefaultSettings()
    {
        service::user_settings::UserSettings user_settings;
        user_settings.gtgen_log_directory = astas_log_directory_;
        return user_settings;
    }

    mantle_api::ScenarioInfo CreateDummyScenarioInformation()
    {
        mantle_api::ScenarioInfo info;
        info.scenario_timeout_duration = std::chrono::milliseconds{2'000};
        info.additional_information.emplace("full_scenario_path", "dummy_full_scenario_path");
        info.additional_information.emplace("vehicle_catalog_path", "dummy_vehicle_catalog_path");
        info.additional_information.emplace("full_map_path", "dummy_full_map_path");

        return info;
    }

    test_utils::MockScenarioEngine* mock_scenario_engine_{};
    std::shared_ptr<test_utils::MockEnvironment> mock_environment_{};
    fs::path astas_custom_data_directory_{"/tmp/astas_custom_data"};
    fs::path astas_log_directory_{astas_custom_data_directory_ / "logs"};
};

TEST_F(SimulatorCoreTestFixture, GivenSimulator_WhenInitStepAndShutdown_ThenNoExceptionThrownAndLogFolderCreated)
{
    auto simulator = CreateDefaultSimulator();

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_environment_, Step()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(1);

    EXPECT_NO_THROW(simulator->Init(); simulator->Step(); simulator->Shutdown());

    // Logging mechanism creates folders (at least "ASTAS_DATA/Log")
    EXPECT_TRUE(fs::exists(astas_log_directory_));
}

TEST_F(SimulatorCoreTestFixture, GivenConstructedSimulator_WhenNotInitialzied_ThenTimeScaleSet)
{
    auto simulator = CreateDefaultSimulator();
    EXPECT_DOUBLE_EQ(1.0, simulator->GetTimeScale());
}

TEST_F(SimulatorCoreTestFixture,
       GivenSimulationParamsWithoutDefinedStepSize_WhenSimulatorConstructed_ThenStepSizeIsDefault)
{
    auto simulator = CreateDefaultSimulator();
    std::chrono::milliseconds expected_default_step_size{10};

    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(1);

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    simulator->Init();

    EXPECT_EQ(expected_default_step_size, simulator->GetStepSize());
}

TEST_F(SimulatorCoreTestFixture, GivenConstructedSimulator_WhenInitialzied_ThenScenarioDurationSet)
{
    auto simulator = CreateDefaultSimulator();

    mantle_api::ScenarioInfo info;
    info.scenario_timeout_duration = std::chrono::milliseconds{30'000};

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(2).WillRepeatedly(Return(info));
    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    simulator->Init();

    EXPECT_EQ(std::chrono::milliseconds{30'000}, simulator->GetScenarioDuration());
}

/// Note: This test is a 1-to-1 copy of the astas_simulator_test in adp
TEST_F(SimulatorCoreTestFixture,
       GivenBlockingCommunication_WhenInitAndStepWithVMO_ThenGtAndSensorViewContainCorrectTimestamps)
{
    SimulationParameters params{};
    params.custom_astas_data_directory = astas_custom_data_directory_;
    params.step_size_ms = 40;
    params.use_gui = true;

    /* Original user settings from INI file:
        [HostVehicle]
        Movement = ExternalVehicle
        BlockingCommunication = true
    */
    auto user_settings = CreateDefaultSettings();
    user_settings.host_vehicle.host_control_mode = service::user_settings::HostControlMode::kExternal;
    user_settings.host_vehicle.blocking_communication = true;

    std::unique_ptr<SimulatorCoreImpl> simulator = CreateSimulatorWithMockScenarioEngine(params, user_settings);

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo())
        .Times(2)
        .WillRepeatedly(Return(CreateDummyScenarioInformation()));

    environment::map::AstasMap map{};
    ON_CALL(*mock_environment_, GetAstasMap()).WillByDefault(ReturnRef(map));

    environment::proto_groundtruth::SensorViewBuilder view_builder{
        map, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    ON_CALL(*mock_environment_, GetSensorViewBuilder()).WillByDefault(ReturnRef(view_builder));

    mantle_api::MockEntityRepository repo;
    ON_CALL(*mock_environment_, GetEntityRepository()).WillByDefault(ReturnRef(repo));

    simulator->Init();
    simulator->Step();

    astas_osi3::SensorView sensor_view_expected{};
    auto* ground_truth_expected = sensor_view_expected.mutable_global_ground_truth();
    auto* proto_moving_object = ground_truth_expected->add_moving_object();
    mantle_api::UniqueId id = 1;
    proto_moving_object->mutable_id()->set_value(id);

    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));

    auto sensor_view_actual = simulator->GetSensorView();
    auto ground_truth_actual = sensor_view_actual.global_ground_truth();
    EXPECT_EQ(ground_truth_actual.moving_object_size(), 1);
    EXPECT_EQ(ground_truth_actual.timestamp().seconds(), 0);
    EXPECT_EQ(ground_truth_actual.timestamp().nanos(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().seconds(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().nanos(), 0);

    simulator->Step();
    // because it's blocking communication, and no change in VehicleModelOut happened, the simulator didn't do step
    sensor_view_actual = simulator->GetSensorView();
    ground_truth_actual = sensor_view_actual.global_ground_truth();
    EXPECT_EQ(ground_truth_actual.timestamp().seconds(), 0);
    EXPECT_EQ(ground_truth_actual.timestamp().nanos(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().seconds(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().nanos(), 0);

    // now set some new TrafficUpdate timestamp and valid position to cause a new step to be calculated
    sensor_view_expected.Clear();
    ground_truth_expected = sensor_view_expected.mutable_global_ground_truth();
    sensor_view_expected.mutable_timestamp()->set_nanos(static_cast<std::int64_t>(40000000));
    ground_truth_expected->mutable_timestamp()->set_nanos(static_cast<std::int64_t>(40000000));
    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));

    simulator->Step();
    sensor_view_actual = simulator->GetSensorView();
    ground_truth_actual = sensor_view_actual.global_ground_truth();
    EXPECT_EQ(ground_truth_actual.timestamp().seconds(), 0);
    EXPECT_EQ(ground_truth_actual.timestamp().nanos(), 40000000);
    EXPECT_EQ(sensor_view_actual.timestamp().seconds(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().nanos(), 40000000);

    EXPECT_NO_THROW(simulator->Shutdown());
}

TEST_F(SimulatorCoreTestFixture,
       GivenBlockingCommunication_WhenInitAndStepWithTrafficUpdate_ThenGtAndSensorViewContainCorrectTimestamps)
{
    SimulationParameters params{};
    params.custom_astas_data_directory = astas_custom_data_directory_;
    params.step_size_ms = 40;
    params.use_gui = true;

    /* Original user settings from INI file:
        [HostVehicle]
        Movement = ExternalVehicle
        BlockingCommunication = true
    */
    auto user_settings = CreateDefaultSettings();
    user_settings.host_vehicle.host_control_mode = service::user_settings::HostControlMode::kExternal;
    user_settings.host_vehicle.blocking_communication = true;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, user_settings);

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo())
        .Times(2)
        .WillRepeatedly(Return(CreateDummyScenarioInformation()));

    environment::map::AstasMap map{};
    ON_CALL(*mock_environment_, GetAstasMap()).WillByDefault(ReturnRef(map));

    environment::proto_groundtruth::SensorViewBuilder view_builder{
        map, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    ON_CALL(*mock_environment_, GetSensorViewBuilder()).WillByDefault(ReturnRef(view_builder));

    mantle_api::MockEntityRepository repo;
    ON_CALL(*mock_environment_, GetEntityRepository()).WillByDefault(ReturnRef(repo));

    simulator->Init();
    simulator->Step();

    astas_osi3::SensorView sensor_view_expected{};
    auto* ground_truth_expected = sensor_view_expected.mutable_global_ground_truth();
    auto* proto_moving_object = ground_truth_expected->add_moving_object();
    mantle_api::UniqueId id = 1;
    proto_moving_object->mutable_id()->set_value(id);

    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));

    auto sensor_view_actual = simulator->GetSensorView();
    auto ground_truth_actual = sensor_view_actual.global_ground_truth();
    EXPECT_EQ(ground_truth_actual.moving_object_size(), 1);
    EXPECT_EQ(ground_truth_actual.timestamp().seconds(), 0);
    EXPECT_EQ(ground_truth_actual.timestamp().nanos(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().seconds(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().nanos(), 0);

    simulator->Step();
    // because it's blocking communication, and no change in VehicleModelOut happened, the simulator didn't do step
    sensor_view_actual = simulator->GetSensorView();
    ground_truth_actual = sensor_view_actual.global_ground_truth();
    EXPECT_EQ(ground_truth_actual.timestamp().seconds(), 0);
    EXPECT_EQ(ground_truth_actual.timestamp().nanos(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().seconds(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().nanos(), 0);

    // now set some new TrafficUpdate timestamp and valid position to cause a new step to be calculated
    sensor_view_expected.Clear();
    ground_truth_expected = sensor_view_expected.mutable_global_ground_truth();
    sensor_view_expected.mutable_timestamp()->set_nanos(static_cast<std::int64_t>(40000000));
    ground_truth_expected->mutable_timestamp()->set_nanos(static_cast<std::int64_t>(40000000));
    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));

    simulator->Step();
    sensor_view_actual = simulator->GetSensorView();
    ground_truth_actual = sensor_view_actual.global_ground_truth();
    EXPECT_EQ(ground_truth_actual.timestamp().seconds(), 0);
    EXPECT_EQ(ground_truth_actual.timestamp().nanos(), 40000000);
    EXPECT_EQ(sensor_view_actual.timestamp().seconds(), 0);
    EXPECT_EQ(sensor_view_actual.timestamp().nanos(), 40000000);

    EXPECT_NO_THROW(simulator->Shutdown());
}

TEST_F(SimulatorCoreTestFixture, GivenSimulationParamsWithStepSize_WhenSimulatorConstructed_ThenStepSizeSetFromParams)
{
    std::uint16_t expected_step_size_ms{42};

    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;
    params.step_size_ms = expected_step_size_ms;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(1);

    simulator->Init();
    EXPECT_EQ(std::chrono::milliseconds{expected_step_size_ms}, simulator->GetStepSize());
}

TEST_F(
    SimulatorCoreTestFixture,
    GivenSimuationParamsWithStepSize_WhenSysclockIsConfigured_ThenGtAndSensorViewContainCorrectTimestampsFromSysclock)
{
    SimulationParameters params{};

    params.custom_astas_data_directory = astas_custom_data_directory_;
    params.use_gui = true;

    /* Original user settings from INI file:
        [Clock]
        UseSystemClock=True
    */
    auto user_settings = CreateDefaultSettings();
    user_settings.clock.use_system_clock = true;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, user_settings);

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo())
        .Times(2)
        .WillRepeatedly(Return(CreateDummyScenarioInformation()));

    environment::map::AstasMap map{};
    ON_CALL(*mock_environment_, GetAstasMap()).WillByDefault(ReturnRef(map));

    environment::proto_groundtruth::SensorViewBuilder view_builder{
        map, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    ON_CALL(*mock_environment_, GetSensorViewBuilder()).WillByDefault(ReturnRef(view_builder));

    mantle_api::MockEntityRepository repo;
    ON_CALL(*mock_environment_, GetEntityRepository()).WillByDefault(ReturnRef(repo));

    astas_osi3::SensorView sensor_view_expected{};
    auto* ground_truth_expected = sensor_view_expected.mutable_global_ground_truth();
    auto* proto_moving_object = ground_truth_expected->add_moving_object();
    mantle_api::UniqueId id = 1;
    proto_moving_object->mutable_id()->set_value(id);

    auto now = std::chrono::system_clock::now();
    sensor_view_expected.mutable_timestamp()->set_seconds(
        static_cast<std::int64_t>(std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count()));
    ground_truth_expected->mutable_timestamp()->set_seconds(
        static_cast<std::int64_t>(std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count()));

    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));

    simulator->Init();
    simulator->Step();

    auto sensor_view_actual = simulator->GetSensorView();
    auto ground_truth_actual = sensor_view_actual.global_ground_truth();

    EXPECT_NE(ground_truth_actual.timestamp().seconds(), 0);
    EXPECT_NE(sensor_view_actual.timestamp().seconds(), 0);
    EXPECT_EQ(ground_truth_actual.moving_object_size(), 1);
    EXPECT_GE(ground_truth_actual.timestamp().seconds(),
              std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count());
    EXPECT_GE(sensor_view_actual.timestamp().seconds(),
              std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count());

    now = std::chrono::system_clock::now();
    simulator->Step();

    sensor_view_expected.Clear();
    ground_truth_expected = sensor_view_expected.mutable_global_ground_truth();
    sensor_view_expected.mutable_timestamp()->set_seconds(
        static_cast<std::int64_t>(std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count()));
    ground_truth_expected->mutable_timestamp()->set_seconds(
        static_cast<std::int64_t>(std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count()));
    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));

    sensor_view_actual = simulator->GetSensorView();
    ground_truth_actual = sensor_view_actual.global_ground_truth();
    EXPECT_GE(ground_truth_actual.timestamp().seconds(),
              std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count());
    EXPECT_GE(sensor_view_actual.timestamp().seconds(),
              std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count());

    EXPECT_NO_THROW(simulator->Shutdown());
}

TEST_F(SimulatorCoreTestFixture, GivenSimulationParams_WhenSimulatorInitSteps_ThenSensorViewContainsCorrectVersion)
{
    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_environment_, Step()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(1);

    simulator->Init();
    simulator->Step();

    astas_osi3::SensorView sensor_view_expected{};
    sensor_view_expected.mutable_version()->set_version_major(3);
    sensor_view_expected.mutable_version()->set_version_minor(5);
    sensor_view_expected.mutable_version()->set_version_patch(0);

    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));

    auto& sensor_view_actual = simulator->GetSensorView();

    EXPECT_TRUE(google::protobuf::util::MessageDifferencer::Equals(sensor_view_expected.version(),
                                                                   sensor_view_actual.version()));
}

TEST_F(SimulatorCoreTestFixture, GivenSimulationParams_WhenSimulatorInitSteps_ThenSensorViewContainsCorrectTimestamp)
{
    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_environment_, Step()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(1);

    simulator->Init();
    simulator->Step();

    astas_osi3::SensorView sensor_view_expected{};
    ON_CALL(*mock_environment_, GetSensorView()).WillByDefault(ReturnRef(sensor_view_expected));
    auto& sensor_view_actual = simulator->GetSensorView();

    auto sim_time = static_cast<std::int64_t>(service::utility::Clock::Instance().Now());
    sensor_view_expected.mutable_timestamp()->set_seconds(sim_time / 1000);
    sensor_view_expected.mutable_timestamp()->set_nanos(static_cast<std::uint32_t>((sim_time % 1000) * 1000000));

    EXPECT_TRUE(google::protobuf::util::MessageDifferencer::Equals(sensor_view_expected.timestamp(),
                                                                   sensor_view_actual.timestamp()));
}

TEST_F(SimulatorCoreTestFixture, GivenAstasGuiNotUsed_WhenSetDriverRelatedData_ThenExceptionIsThrown)
{
    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;
    params.use_gui = false;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));
    EXPECT_CALL(*mock_environment_, Init()).Times(1);

    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(1);

    simulator->Init();

    messages::ui::DriverRelatedData driver_related_data{};
    EXPECT_THROW(simulator->SetDriverRelatedData(driver_related_data), SimulatorException);
}

TEST_F(SimulatorCoreTestFixture, GivenAstasGuiUsed_WhenSetDriverRelatedData_ThenNoExceptionIsThrown)
{
    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;
    params.use_gui = true;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));
    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo())
        .Times(2)
        .WillRepeatedly(Return(CreateDummyScenarioInformation()));

    environment::map::AstasMap map{};
    environment::proto_groundtruth::SensorViewBuilder builder{
        map, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    mantle_api::MockEntityRepository repo;
    ON_CALL(*mock_environment_, GetAstasMap()).WillByDefault(ReturnRef(map));
    ON_CALL(*mock_environment_, GetSensorViewBuilder()).WillByDefault(ReturnRef(builder));
    ON_CALL(*mock_environment_, GetEntityRepository()).WillByDefault(ReturnRef(repo));

    simulator->Init();

    messages::ui::DriverRelatedData driver_related_data{};
    EXPECT_NO_THROW(simulator->SetDriverRelatedData(driver_related_data));
}

TEST_F(SimulatorCoreTestFixture, GivenSimulationParametersWithUseGUI_WhenInit_ThenServerPortIsQueryable)
{
    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;
    params.use_gui = true;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));
    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo())
        .Times(2)
        .WillRepeatedly(Return(CreateDummyScenarioInformation()));

    environment::map::AstasMap map{};
    environment::proto_groundtruth::SensorViewBuilder builder{
        map, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    mantle_api::MockEntityRepository repo;
    ON_CALL(*mock_environment_, GetAstasMap()).WillByDefault(ReturnRef(map));
    ON_CALL(*mock_environment_, GetSensorViewBuilder()).WillByDefault(ReturnRef(builder));
    ON_CALL(*mock_environment_, GetEntityRepository()).WillByDefault(ReturnRef(repo));

    simulator->Init();

    EXPECT_NE(0, simulator->GetServerPort());
}

TEST_F(SimulatorCoreTestFixture,
       GivenInternalMovementAndRouteHostVehicle_WhenInit_ThenHostVehicleInterfaceOfEnvironmentIsUsed)
{
    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;

    /* Original user settings from INI file:
        [HostVehicle]
        Movement = Astas
    */
    auto user_settings = CreateDefaultSettings();
    user_settings.host_vehicle.host_control_mode = service::user_settings::HostControlMode::kInternal;
    user_settings.host_vehicle.blocking_communication = true;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, user_settings);

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);

    environment::host::HostVehicleInterface host_vehicle_interface;
    ON_CALL(*mock_environment_, GetHostVehicleInterface()).WillByDefault(ReturnRef(host_vehicle_interface));
    EXPECT_CALL(*mock_environment_, GetHostVehicleInterface()).Times(1);

    simulator->Init();
    simulator->GetHostVehicleInterface();
}

TEST_F(SimulatorCoreTestFixture, GivenSimulatorNotInitialized_WhenIsFinished_ThenFalse)
{
    SimulationParameters params;
    params.custom_astas_data_directory = astas_custom_data_directory_;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    EXPECT_FALSE(simulator->IsFinished());
}

TEST_F(SimulatorCoreTestFixture, GivenSimulatorInitializedAndScenarioOver_WhenIsFinished_ThenTrue)
{
    SimulationParameters params;
    params.step_size_ms = 2'000;
    params.custom_astas_data_directory = astas_custom_data_directory_;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_environment_, Step()).Times(2);
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(1);

    simulator->Init();
    simulator->Step();  // initial step
    simulator->Step();

    EXPECT_CALL(*mock_scenario_engine_, IsFinished()).Times(1).WillOnce(Return(true));
    EXPECT_TRUE(simulator->IsFinished());
}

TEST_F(SimulatorCoreTestFixture,
       GivenSimulatorInitializedAndScenarioOver_WhenIsFinished_ThenTrueWithCorrectScenarioDuration)
{
    SimulationParameters params;
    params.step_size_ms = 2'000;
    params.custom_astas_data_directory = astas_custom_data_directory_;

    auto simulator = CreateSimulatorWithMockScenarioEngine(params, CreateDefaultSettings());

    mantle_api::ScenarioInfo info;
    info.scenario_timeout_duration = std::chrono::milliseconds{std::numeric_limits<std::uint64_t>::max()};
    EXPECT_CALL(*mock_scenario_engine_, GetScenarioInfo()).Times(2).WillRepeatedly(Return(info));
    EXPECT_CALL(*mock_scenario_engine_, IsFinished()).Times(1).WillOnce(Return(false));

    environment::host::HostVehicleModel host_vehicle_model{};
    ON_CALL(*mock_environment_, GetHostVehicleModel()).WillByDefault(ReturnRef(host_vehicle_model));

    EXPECT_CALL(*mock_environment_, Init()).Times(1);
    EXPECT_CALL(*mock_environment_, Step()).Times(2);

    simulator->Init();
    simulator->Step();  // initial step
    simulator->Step();

    EXPECT_EQ(std::chrono::milliseconds{std::numeric_limits<std::uint64_t>::max()}, simulator->GetScenarioDuration());
    EXPECT_FALSE(simulator->IsFinished());
}

}  // namespace astas::simulation::simulator
