/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/terminate_handler.h"

#include "Core/Service/Logging/log_setup.h"
#include "Core/Service/Logging/logging.h"

#include <cxxabi.h>  // GCC and Clang specific

#include <cstdlib>
#include <iostream>
#include <typeinfo>

namespace astas::simulation::simulator
{
/// @brief Reference to the previous terminate handler.
// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables) - accepted
std::terminate_handler g_previous_terminate_handler{nullptr};

/// @brief Sets our custom terminate handler and saves a reference to the current one.
void SetTerminateHandler()
{
    g_previous_terminate_handler = std::set_terminate(TerminateHandler);
    if (g_previous_terminate_handler != nullptr)
    {
        Debug("Storing previous terminate handler, to be executed after GTGen terminate handler");
    }
}

/// @brief GCC and Clang specific. Returns the current exception's type.
std::string CurrentExceptionTypeName()
{
    int status{0};

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg) - OK
    char* exception_buffer =
        abi::__cxa_demangle(abi::__cxa_current_exception_type()->name(), nullptr, nullptr, &status);
    std::string exception_name(exception_buffer);
    free(exception_buffer);  // NOLINT(cppcoreguidelines-no-malloc) - accepted
    return exception_name;
}

/// @brief Formats the current exception into a string containing the type and the what (if available).
std::string GetCurrentExceptionErrorMsg()
{
    std::string error_msg;

    if (auto exc = std::current_exception())
    {
        try
        {
            std::rethrow_exception(exc);
        }
        catch (const std::exception& e)
        {
            error_msg = fmt::format("{} \n    what(): {}", CurrentExceptionTypeName(), e.what());
        }
    }
    return error_msg;
}

void LogTerminateReasonAndCleanupLogging()
{
    if (std::current_exception() != nullptr)
    {
        Error("Terminating due to an unhandled exception:\n    {}\n\n", GetCurrentExceptionErrorMsg());
    }

    service::logging::LogSetup::Instance().CleanupLogging();
}

/// @brief Called when no catch handler can be found for a thrown exception, or for some other exceptional circumstance
/// that makes impossible to continue the exception handling process.
/// In such cases this function will shutdown spdlog so that logging information is available in the log file.
/// For more information see: https://akrzemi1.wordpress.com/2011/10/05/using-stdterminate/ and
/// https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.3.0/com.ibm.zos.v2r3.cbclx01/cplr155.htm
void TerminateHandler()
{
    LogTerminateReasonAndCleanupLogging();

    if (g_previous_terminate_handler != nullptr && g_previous_terminate_handler != TerminateHandler)
    {
        std::cerr << "Executing original terminate handler...";
        std::set_terminate(g_previous_terminate_handler);
        std::terminate();
    }
    else
    {
        std::_Exit(EXIT_FAILURE);
    }
}

}  // namespace astas::simulation::simulator
