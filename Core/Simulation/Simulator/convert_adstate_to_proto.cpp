/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/convert_adstate_to_proto.h"

namespace astas::simulation::simulator
{

messages::ui::AdState ConvertExternalControlStateToProto(mantle_api::ExternalControlState external_control_state)
{
    switch (external_control_state)
    {
        case mantle_api::ExternalControlState::kFull:
        case mantle_api::ExternalControlState::kLateralOnly:
        case mantle_api::ExternalControlState::kLongitudinalOnly:
        {
            return messages::ui::AD_STATE_ON;
        }
        break;
        case mantle_api::ExternalControlState::kOff:
        default:
        {
            return messages::ui::AD_STATE_OFF;
        }
        break;
    }
}

}  // namespace astas::simulation::simulator
