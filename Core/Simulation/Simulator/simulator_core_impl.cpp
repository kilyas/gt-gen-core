/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/simulator_core_impl.h"

#include "Core/Communication/AsyncServer/async_server.h"
#include "Core/Communication/Dispatchers/simulation_gui_dispatcher.h"  // TODO: move visualization data out of here
#include "Core/Communication/FoxgloveWebsocketServer/foxglove_websocket_server.h"
#include "Core/Environment/AstasEnvironment/astas_environment.h"
#include "Core/Environment/Host/vehicle_model_converter.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Gui/ui_data.h"
#include "Core/Service/Logging/log_setup.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Service/Version/version.h"
#include "Core/Simulation/Simulator/convert_adstate_to_proto.h"
#include "Core/Simulation/Simulator/exception.h"
#include "Core/Simulation/Simulator/formatting.h"
#include "Core/Simulation/Simulator/terminate_handler.h"

#include <MantleAPI/Execution/i_scenario_engine.h>
#include <fmt/chrono.h>
#include <units.h>

#include <chrono>

namespace astas::simulation::simulator
{

SimulatorCoreImpl::SimulatorCoreImpl(
    std::unique_ptr<mantle_api::IScenarioEngine> scenario_engine,
    std::shared_ptr<mantle_api::IEnvironment> environment,  // NOLINT(performance-unnecessary-value-param)
    SimulationParameters params,
    const service::user_settings::UserSettings& user_settings)
    : scenario_engine_{std::move(scenario_engine)},
      env_{std::static_pointer_cast<environment::api::IAstasEnvironment>(environment)},
      params_{std::move(params)},
      user_settings_{user_settings}
{
    ASTAS_PROFILE_SCOPE

    service::logging::LogSetup::Instance().AddConsoleLogger(params_.console_log_level);

    Info("=== Running GTGen core version {} ===", GetGtGenCoreVersion());
    Info("Received the following command line parameters:{:modified}", params_);

    if (!env_)
    {
        throw SimulatorException("Provided Environment could not be casted to AstasEnvironment.");
    }

    configured_step_size_ = mantle_api::Time{static_cast<double>(params_.step_size_ms)};
    configured_time_scale_ = user_settings_.host_vehicle.time_scale;

    if (user_settings_.clock.use_system_clock)
    {
        service::utility::Clock::Instance().UseSystemClock();
    }
    else
    {
        service::utility::Clock::Instance().SetTickDuration(configured_step_size_);
        service::utility::Clock::Instance().UseSimulationClock();
        service::utility::Clock::Instance().SetNow(units::time::millisecond_t{0});
    }

    SetTerminateHandler();
}

void SimulatorCoreImpl::Init()
{
    ASTAS_PROFILE_SCOPE

    InitLogging();

    env_->Init();
    scenario_engine_->Init();
    previous_vehicle_model_out_timestamp_ = env_->GetHostVehicleModel().vehicle_model_out.time_stamp;

    if (params_.use_gui)
    {
        LaunchGuiServer();
    }

    if (user_settings_.foxglove.websocket_server)
    {
        LaunchFoxgloveWebsocketServer();
    }

    init_called_ = true;
}

std::shared_ptr<mantle_api::IEnvironment> SimulatorCoreImpl::GetEnvironment() const
{
    if (env_ != nullptr)
    {
        return env_;
    }
    else
    {
        throw SimulatorException("Cannot return environment as it is not created yet!");
    }
}

void SimulatorCoreImpl::InitLogging() const
{
    auto log_directory = params_.custom_log_directory;
    if (log_directory.empty())
    {
        log_directory = user_settings_.gtgen_log_directory;
    }

    service::logging::LogSetup::Instance().AddFileLogger(
        log_directory, service::logging::FileLoggingOption::kAlwaysLogToNewFile, user_settings_.file_logging.log_level);
}

void SimulatorCoreImpl::LaunchGuiServer()
{
    ui_data_ = std::make_unique<service::gui::UiDataProvider>();
    async_server_ = std::make_unique<communication::AsyncServer>();

    communication::VisualizationData gui_data{
        env_->GetAstasMap(), *ui_data_, env_->GetSensorViewBuilder(), &env_->GetEntityRepository()};

    const auto info = scenario_engine_->GetScenarioInfo();
    ASSERT(info.additional_information.find("full_scenario_path") != info.additional_information.end());

    ASSERT(info.additional_information.find("full_map_path") != info.additional_information.end());

    gui_data.static_chunks = env_->GetStaticChunks();
    /// @todo disabled these to break dependencies on UserDataManager. Eventually LaunchGuiServer should be removed
    /// fully
    // gui_data.settings = user_data_manager_->GetUserSettings();
    // gui_data.user_settings_file_path = user_data_manager_->GetPathHandler().GetAstasDataUserSettingsFile();
    gui_data.sim_control_port = params_.control_server_port;

    std::function create_visualization_dispatcher = [gd = std::move(gui_data), env = env_] {
        return std::make_unique<communication::SimulationGuiDispatcher>(gd, env);
    };

    auto dispatcher_factory = std::make_unique<communication::DispatcherFactory>(
        std::move(create_visualization_dispatcher), "Scenario Visualization");

    async_server_->StartAcceptingConnections(std::move(dispatcher_factory), true);
}

void SimulatorCoreImpl::LaunchFoxgloveWebsocketServer()
{
    if (foxglove_websocket_server_ == nullptr)
    {
        foxglove_websocket_server_ = std::make_unique<communication::FoxgloveWebsocketServer>();
    }

    foxglove_websocket_server_->Start();
};

void SimulatorCoreImpl::SendGroundTruthToFoxgloveWebsocket()
{
    if (foxglove_websocket_server_ != nullptr)
    {
        foxglove_websocket_server_->Send(env_->GetSensorView());
    }
};

void SimulatorCoreImpl::Step()
{
    ASTAS_PROFILE_SCOPE

    if (initial_step_)
    {
        initial_step_ = false;
    }
    else if (user_settings_.host_vehicle.blocking_communication &&
             (env_->GetHostVehicleModel().vehicle_model_out.time_stamp == previous_vehicle_model_out_timestamp_))
    {
        return;
    }
    else
    {
        // the following is needed for fake clock
        // if real clock is used, it increases steadily
        // and thus Tick() is a noop. Hence,
        // the underlying assumption that in first step
        // clock is not increase doesn't hold in this
        // configuration
        service::utility::Clock::Instance().Tick();
    }

    scenario_engine_->Step();

    env_->Step();

    previous_vehicle_model_out_timestamp_ = env_->GetHostVehicleModel().vehicle_model_out.time_stamp;

    SendGroundTruthToFoxgloveWebsocket();
}

bool SimulatorCoreImpl::IsFinished() const
{
    if (init_called_)
    {
        return scenario_engine_->IsFinished();
    }
    return false;
}

void SimulatorCoreImpl::Shutdown()
{
    Info("=== Scenario simulation finished after specified duration of {} ===", GetScenarioDuration());
    shutdown_called_ = true;
}

SimulatorCoreImpl::~SimulatorCoreImpl()
{
    if (init_called_ && !shutdown_called_)
    {
        Shutdown();
    }

    service::logging::LogSetup::Instance().CleanupLogging();
}

std::chrono::milliseconds SimulatorCoreImpl::GetScenarioDuration() const
{
    auto duration = scenario_engine_->GetScenarioInfo().scenario_timeout_duration();

    if (duration > static_cast<double>(std::numeric_limits<std::int64_t>::max()))
    {
        return std::chrono::milliseconds(std::numeric_limits<std::int64_t>::max());
    }

    return std::chrono::milliseconds(static_cast<std::int64_t>(duration));
}

const IHostVehicleInterface& SimulatorCoreImpl::GetHostVehicleInterface() const
{
    return env_->GetHostVehicleInterface();
}

/// ---- Proto Interface -----------------------------------------------------------------------------------------------

const astas_osi3::SensorView& SimulatorCoreImpl::GetSensorView() const
{
    return env_->GetSensorView();
}

const std::vector<astas_osi3::TrafficCommand>& SimulatorCoreImpl::GetTrafficCommands() const
{
    return env_->GetTrafficCommands();
}

void SimulatorCoreImpl::SetTrafficUpdate(const astas_osi3::TrafficUpdate& traffic_update)
{
    auto& host_vehicle_model = env_->GetHostVehicleModel();
    host_vehicle_model.vehicle_model_out = environment::host::ConvertProtoToVehicleModelOut(traffic_update);

    if (ui_data_ != nullptr)
    {
        ui_data_->SetAdState(
            ConvertExternalControlStateToProto(host_vehicle_model.vehicle_model_out.had_control_state));
    }
}

std::uint16_t SimulatorCoreImpl::GetServerPort() const
{
    return async_server_->GetReservedPort();
}

void SimulatorCoreImpl::SetDriverRelatedData(messages::ui::DriverRelatedData driver_related_data)
{
    if (ui_data_ == nullptr)
    {
        throw SimulatorException(
            "Driver Related Data cannot be set when ASTAS_GUI is not used: Set use_gui flag to true in simulation "
            "parameters");
    }
    ui_data_->SetDriverRelatedData(std::move(driver_related_data));
}

void SimulatorCoreImpl::AddGuiMarker(double latitude, double longitude, const std::string& description, double radius)
{
    if (ui_data_ == nullptr)
    {
        throw SimulatorException(
            "Cannot add GUI markers when ASTAS_GUI is not used: Set use_gui flag to true in simulation parameters");
    }

    mantle_api::Position position =
        mantle_api::LatLonPosition{units::angle::degree_t(latitude), units::angle::degree_t(longitude)};
    auto xyz_position = env_->GetConverter()->Convert(position);
    xyz_position = env_->GetQueryService().GetUpwardsShiftedLanePosition(xyz_position, 0, true);

    messages::ui::Marker marker;
    marker.set_description(description);
    marker.set_radius(radius);
    marker.mutable_position()->set_x(xyz_position.x.value());
    marker.mutable_position()->set_y(xyz_position.y.value());
    marker.mutable_position()->set_z(xyz_position.z.value());

    ui_data_->AddGuiMarker(std::move(marker));

    Debug("Added a POI marker for \"{}\" at {}, {}", description, latitude, longitude);
    Debug(
        "POI marker will appear at {}, {}, {}", xyz_position.x.value(), xyz_position.y.value(), xyz_position.z.value());
}

}  // namespace astas::simulation::simulator
