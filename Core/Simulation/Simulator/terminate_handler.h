/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SIMULATION_SIMULATOR_TERMINATEHANDLER_H
#define GTGEN_CORE_SIMULATION_SIMULATOR_TERMINATEHANDLER_H

#include <string>

namespace astas::simulation::simulator
{

/// @brief Called when no catch handler can be found for a thrown exception, or for some other exceptional circumstance
/// that makes impossible to continue the exception handling process.
/// In such cases this function will shutdown spdlog so that logging information is available in the log file.
/// For more information see: https://akrzemi1.wordpress.com/2011/10/05/using-stdterminate/ and
/// https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.3.0/com.ibm.zos.v2r3.cbclx01/cplr155.htm
void TerminateHandler();

/// @brief Sets the custom terminate handler and saves a reference to the current one.
void SetTerminateHandler();

/// @brief Gets the current exception message
std::string GetCurrentExceptionErrorMsg();

/// @brief Logs the current exception message and shuts down the logging framework
void LogTerminateReasonAndCleanupLogging();

}  // namespace astas::simulation::simulator

#endif  // GTGEN_CORE_SIMULATION_SIMULATOR_TERMINATEHANDLER_H
