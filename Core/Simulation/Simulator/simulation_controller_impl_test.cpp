/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/simulation_controller_impl.h"

#include <gtest/gtest.h>

namespace astas::simulation::simulation_controller
{

class SimulationControllerTest : public testing::Test
{
  public:
    SimulationControllerTest()
    {
        sim_controller_.SetSimulationStepCallback([this] { step_executed_++; });
        sim_controller_.SetSimulationAbortCallback([this] { abort_executed_++; });
        sim_controller_.SetSleepFunction(
            [this](const std::function<bool()>& condition) { should_sleep_ = condition(); });
    }

  protected:
    SimulationControllerImpl sim_controller_;

    std::optional<bool> should_sleep_;
    int step_executed_{0};
    int abort_executed_{0};
};

TEST_F(SimulationControllerTest, GivenControllerNotPaused_WhenExecutingStepSeveralTimes_ThenStepFunctionCalled)
{
    sim_controller_.ExecuteStep();
    sim_controller_.ExecuteStep();

    EXPECT_EQ(step_executed_, 2);
}

TEST_F(SimulationControllerTest,
       GivenControllerNotAborted_WhenAbortCalledMultipleTimes_ThenAbortCallbackExecutedOnlyOnce)
{
    sim_controller_.AbortSimulation();
    sim_controller_.AbortSimulation();

    EXPECT_EQ(abort_executed_, 1);
}

TEST_F(SimulationControllerTest, GivenControllerPaused_WhenExecuteStep_ThenThreadShouldSleep)
{
    sim_controller_.PauseSimulation();
    sim_controller_.ExecuteStep();

    ASSERT_TRUE(should_sleep_.has_value());
    EXPECT_TRUE(should_sleep_.value());
}

TEST_F(SimulationControllerTest, GivenControllerPausedAndResumed_WhenExecuteStep_ThenThreadNotShouldSleep)
{
    sim_controller_.PauseSimulation();
    sim_controller_.ResumeSimulation();
    sim_controller_.ExecuteStep();

    ASSERT_TRUE(should_sleep_.has_value());
    EXPECT_FALSE(should_sleep_.value());
}

TEST_F(SimulationControllerTest, GivenControllerPausedAndAborted_WhenExecuteStep_ThenThreadShouldNotSleep)
{
    sim_controller_.PauseSimulation();
    sim_controller_.AbortSimulation();
    sim_controller_.ExecuteStep();

    ASSERT_TRUE(should_sleep_.has_value());
    EXPECT_FALSE(should_sleep_.value());
}

TEST_F(SimulationControllerTest, GivenControllerAborted_WhenExecuteStep_ThenNotStepped)
{
    sim_controller_.AbortSimulation();
    sim_controller_.ExecuteStep();

    EXPECT_EQ(step_executed_, 0);
}

TEST_F(SimulationControllerTest, GivenControllerShutDown_WhenExecuteStep_ThenNotStepped)
{
    sim_controller_.ShutDown();
    sim_controller_.ExecuteStep();

    EXPECT_EQ(step_executed_, 0);
}

TEST_F(SimulationControllerTest, GivenSimulationController_WhenStartControlServer_ThenServerPortIsNotZero)
{
    sim_controller_.StartControlServer("Info");
    EXPECT_NE(0, sim_controller_.GetServerPort());
}

}  // namespace astas::simulation::simulation_controller
