/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATORCOREIMPL_H
#define GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATORCOREIMPL_H

#include "Core/Communication/FoxgloveWebsocketServer/foxglove_websocket_server.h"
#include "Core/Environment/AstasEnvironment/i_astas_environment.h"
#include "Core/Export/i_host_vehicle_interface.h"
#include "Core/Export/simulation_parameters.h"
#include "astas_osi_sensorview.pb.h"
#include "astas_osi_trafficcommand.pb.h"
#include "astas_osi_trafficupdate.pb.h"
#include "driver_related_data.pb.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Execution/i_scenario_engine.h>

#include <chrono>
#include <memory>

/// Forward Declarations
namespace mantle_api
{
class IScenarioEngine;
}  // namespace mantle_api

namespace astas
{
namespace communication
{
class AsyncServer;
class FoxgloveWebsocketServer;
}  // namespace communication

namespace service
{
namespace gui
{
class UiDataProvider;
}
namespace scenario
{
class ScenarioData;
}  // namespace scenario
}  // namespace service

namespace environment
{
namespace api
{
class AstasEnvironment;
}
}  // namespace environment

namespace simulation
{
namespace simulator
{

/// @brief The main simulation class. It takes simulation parameters (from the command line or from the ADP) and
/// initializes the logging framework.
class SimulatorCoreImpl
{
  public:
    /// @brief Simulation constructor taking in the simulation parameters and the injected activity dependencies.
    /// Initialization of the logging is done here
    explicit SimulatorCoreImpl(std::unique_ptr<mantle_api::IScenarioEngine> scenario_engine,
                               std::shared_ptr<mantle_api::IEnvironment> environment,
                               SimulationParameters params,
                               const service::user_settings::UserSettings& user_settings);

    SimulatorCoreImpl(const SimulatorCoreImpl&) = delete;
    SimulatorCoreImpl(SimulatorCoreImpl&& rhs) = default;
    SimulatorCoreImpl& operator=(const SimulatorCoreImpl&) = delete;
    SimulatorCoreImpl& operator=(SimulatorCoreImpl&&) = delete;

    /// @brief Simulation destructor; ensures proper cleanup.
    ~SimulatorCoreImpl();

    /// @brief Initializes the simulation
    void Init();

    /// @brief Triggers the next step of the simulation
    void Step();

    /// @brief Returns true, when the scenario has ended
    bool IsFinished() const;

    /// @brief Shutdowns the simulation and cleans up
    void Shutdown();

    /// @brief Returns the environment of simulator
    std::shared_ptr<mantle_api::IEnvironment> GetEnvironment() const;

    /// @brief Used by the GTGen-Cli to capture the step size in milliseconds when the simulator is constructed
    std::chrono::milliseconds GetStepSize() const
    {
        return std::chrono::milliseconds(static_cast<std::int64_t>(configured_step_size_()));
    }

    /// @brief Used by the GTGen-Cli to capture the time scale factor when the simulator is constructed
    double GetTimeScale() const { return configured_time_scale_; }

    /// @brief Used by the GTGen-Cli to capture the update rate in milliseconds when the simulator is constructed
    std::chrono::milliseconds GetScenarioDuration() const;

    /// @brief Provides proto interfaces for OSI SensorView
    const astas_osi3::SensorView& GetSensorView() const;

    /// @brief Provides proto interfaces for osi traffic command
    const std::vector<astas_osi3::TrafficCommand>& GetTrafficCommands() const;

    /// @brief  Input allowing the Host vehicle to be controlled externally
    /// @note   This interface does not contain any information about the AD function state. This will be added in a
    /// future release
    void SetTrafficUpdate(const astas_osi3::TrafficUpdate& traffic_update);

    /// @brief Used by driver_related_data_test to connect test tcp client to the simulator's server
    std::uint16_t GetServerPort() const;

    /// @brief Provides driver related data proto interfaces
    void SetDriverRelatedData(messages::ui::DriverRelatedData driver_related_data);

    /// @brief Provides read access to internal host vehicle data
    /// @attention May only be called after Simulator::Init(), otherwise behavior is undefined
    const IHostVehicleInterface& GetHostVehicleInterface() const;

    /// @brief Adds a position with a description to be displayed in ASTAS_GUI
    void AddGuiMarker(double latitude, double longitude, const std::string& description, double radius);

  private:
    void InitUserData() const;
    void InitLogging() const;
    void LaunchFoxgloveWebsocketServer();
    void LaunchGuiServer();
    void SendGroundTruthToFoxgloveWebsocket();

    SimulationParameters params_;

    std::unique_ptr<communication::AsyncServer> async_server_{nullptr};
    std::unique_ptr<communication::FoxgloveWebsocketServer> foxglove_websocket_server_{nullptr};
    std::unique_ptr<service::gui::UiDataProvider> ui_data_;

    mantle_api::Time configured_step_size_{0};
    double configured_time_scale_{1};
    bool initial_step_{true};
    bool init_called_{false};
    bool shutdown_called_{false};

    std::shared_ptr<environment::api::IAstasEnvironment> env_{nullptr};
    std::unique_ptr<mantle_api::IScenarioEngine> scenario_engine_{nullptr};
    mantle_api::Time previous_vehicle_model_out_timestamp_{};
    service::user_settings::UserSettings user_settings_{};
};

}  // namespace simulator
}  // namespace simulation
}  // namespace astas

#endif  // GTGEN_CORE_SIMULATION_SIMULATOR_SIMULATORCOREIMPL_H
