/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Simulation/Simulator/simulation_controller_impl.h"

#include "Core/Communication/AsyncServer/async_server.h"
#include "Core/Communication/Dispatchers/simulation_control_dispatcher.h"
#include "Core/Service/Logging/log_setup.h"

namespace astas::simulation::simulation_controller
{

SimulationControllerImpl::SimulationControllerImpl()
    : async_server_{std::make_unique<communication::AsyncServer>()},
      step_function_{[]() { Error("Step function callback not set, SimulationController won't execute anything!"); }},
      abort_callback_{[]() { Warn("Simulation abort callback not set."); }},
      sleep_function_{[](const std::function<bool()>& condition) {
          while (condition())
          {
              std::this_thread::sleep_for(std::chrono::milliseconds(10));
          }
      }}
{
}

SimulationControllerImpl::~SimulationControllerImpl() = default;

void SimulationControllerImpl::StartControlServer(const std::string& console_log_level)
{
    service::logging::LogSetup::Instance().AddConsoleLogger(console_log_level);
    std::function create_simulation_control = [this] {
        return std::make_unique<communication::SimulationControlDispatcher>([this] { PauseSimulation(); },
                                                                            [this] { ResumeSimulation(); },
                                                                            [this] { StepImmediately(); },
                                                                            [this] { AbortSimulation(); });
    };
    auto dispatcher_factory =
        std::make_unique<communication::DispatcherFactory>(std::move(create_simulation_control), "Simulation Control");
    async_server_->StartAcceptingConnections(std::move(dispatcher_factory));
}

void SimulationControllerImpl::ExecuteStep()
{
    sleep_function_([this]() { return paused_ && !aborted_; });

    StepImmediately();
}

void SimulationControllerImpl::StepImmediately()
{
    if (aborted_)
    {
        Warn("Tried to step after simulation abort requested, nothing will happen.");
        return;
    }

    step_function_();
}

void SimulationControllerImpl::PauseSimulation()
{
    paused_ = true;
}

void SimulationControllerImpl::ResumeSimulation()
{
    paused_ = false;
}

void SimulationControllerImpl::AbortSimulation()
{
    if (!aborted_)
    {
        abort_callback_();
        aborted_ = true;
    }
    else
    {
        Warn(
            "Already requested to abort simulation. If nothing happens, the registered abort function might be "
            "without effect or dead locked.");
    }
}

void SimulationControllerImpl::SetSimulationAbortCallback(std::function<void()> func)
{
    abort_callback_ = std::move(func);
}

void SimulationControllerImpl::SetSimulationStepCallback(std::function<void()> func)
{
    step_function_ = std::move(func);
}

void SimulationControllerImpl::SetSleepFunction(
    const std::function<void(const std::function<bool()>& condition)>& sleep_function)
{
    sleep_function_ = sleep_function;
}

void SimulationControllerImpl::ShutDown()
{
    aborted_ = true;
    async_server_->StopServing();
}

std::uint16_t SimulationControllerImpl::GetServerPort() const
{
    return async_server_->GetReservedPort();
}

}  // namespace astas::simulation::simulation_controller
