/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SIMULATION_SIMULATOR_EXCEPTION_H
#define GTGEN_CORE_SIMULATION_SIMULATOR_EXCEPTION_H

#include "Core/Service/Utility/exceptions.h"

namespace astas::simulation::simulator
{
class SimulatorException : public service::utility::exception::AstasException
{
    using AstasException::AstasException;
};

}  // namespace astas::simulation::simulator

#endif  // GTGEN_CORE_SIMULATION_SIMULATOR_EXCEPTION_H
