/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/DateTime/time.h"

#include <MantleAPI/Common/time_utils.h>
#include <gtest/gtest.h>
#include <units.h>

#include <cstdint>
#include <memory>

namespace astas::service::utility
{
// TODO: Move the mantle_api::SecondsToTime tests to MantleAPI and clarify, if cutting-off after ms is desired
template <typename T>
class SecondsToSimulationTimeFloatingTypesTest : public testing::Test
{
};
TYPED_TEST_SUITE_P(SecondsToSimulationTimeFloatingTypesTest);

using FloatingTypes = testing::Types<float, double>;

TYPED_TEST_P(SecondsToSimulationTimeFloatingTypesTest, GivenMilliseconds_WhenSecondsToTime_ThenTimeContainsMilliseconds)
{
    const auto input1 = static_cast<TypeParam>(0.001);
    const mantle_api::Time expected_output1{1};
    const mantle_api::Time actual_output1{mantle_api::SecondsToTime(input1)};
    EXPECT_EQ(expected_output1, actual_output1) << "expected: " << expected_output1 << " - actual: " << actual_output1;

    const auto input2 = static_cast<TypeParam>(1.002);
    const mantle_api::Time expected_output2{1002};
    const mantle_api::Time actual_output2{mantle_api::SecondsToTime(input2)};
    EXPECT_EQ(expected_output2, actual_output2) << "expected: " << expected_output2 << " - actual: " << actual_output2;

    const auto input3 = static_cast<TypeParam>(10.0);
    const mantle_api::Time expected_output3{10000};
    const mantle_api::Time actual_output3{mantle_api::SecondsToTime(input3)};
    EXPECT_EQ(expected_output3, actual_output3) << "expected: " << expected_output3 << " - actual: " << actual_output3;

    const auto input4 = static_cast<TypeParam>(0.0);
    const mantle_api::Time expected_output4{0};
    const mantle_api::Time actual_output4{mantle_api::SecondsToTime(input4)};
    EXPECT_EQ(expected_output4, actual_output4) << "expected: " << expected_output4 << " - actual: " << actual_output4;
}

TYPED_TEST_P(SecondsToSimulationTimeFloatingTypesTest,
             GivenTimeSmallerThanMilliSeconds_WhenSecondsToTime_ThenJustCutOffInsteadOfRounding)
{
    const auto input1 = static_cast<TypeParam>(0.0001);
    const mantle_api::Time expected_output1{0};
    const mantle_api::Time actual_output1{mantle_api::SecondsToTime(input1)};
    EXPECT_EQ(expected_output1, actual_output1) << "expected: " << expected_output1 << " - actual: " << actual_output1;

    const auto input2 = static_cast<TypeParam>(32.5666);
    const mantle_api::Time expected_output2{32566};
    const mantle_api::Time actual_output2{mantle_api::SecondsToTime(input2)};
    EXPECT_EQ(expected_output2, actual_output2) << "expected: " << expected_output2 << " - actual: " << actual_output2;
}

REGISTER_TYPED_TEST_SUITE_P(SecondsToSimulationTimeFloatingTypesTest,
                            GivenMilliseconds_WhenSecondsToTime_ThenTimeContainsMilliseconds,
                            GivenTimeSmallerThanMilliSeconds_WhenSecondsToTime_ThenJustCutOffInsteadOfRounding);

INSTANTIATE_TYPED_TEST_SUITE_P(FloatingPointInput, SecondsToSimulationTimeFloatingTypesTest, FloatingTypes, /*unused*/);

template <typename T>
class SecondsToSimulationTimeIntegralTypesTest : public testing::Test
{
};
TYPED_TEST_SUITE_P(SecondsToSimulationTimeIntegralTypesTest);

using IntegralTypes = testing::Types<std::int32_t, std::int64_t, std::uint32_t, std::uint64_t>;

TYPED_TEST_P(SecondsToSimulationTimeIntegralTypesTest, GivenValidTimeInput_WhenSecondsToTime_ThenMantleTimeCorrect)
{
    const TypeParam input1{1};
    const mantle_api::Time expected_output1{1'000};
    const mantle_api::Time actual_output1{mantle_api::SecondsToTime(input1)};
    EXPECT_EQ(expected_output1, actual_output1) << "expected: " << expected_output1 << " - actual: " << actual_output1;

    const TypeParam input2{12'345};
    const mantle_api::Time expected_output2{12'345'000};
    const mantle_api::Time actual_output2{mantle_api::SecondsToTime(input2)};
    EXPECT_EQ(expected_output2, actual_output2) << "expected: " << expected_output2 << " - actual: " << actual_output2;

    const TypeParam input3{0};
    const mantle_api::Time expected_output3{0};
    const mantle_api::Time actual_output3{mantle_api::SecondsToTime(input3)};
    EXPECT_EQ(expected_output3, actual_output3) << "expected: " << expected_output3 << " - actual: " << actual_output3;
}

REGISTER_TYPED_TEST_SUITE_P(SecondsToSimulationTimeIntegralTypesTest,
                            GivenValidTimeInput_WhenSecondsToTime_ThenMantleTimeCorrect);

INSTANTIATE_TYPED_TEST_SUITE_P(IntegralTypeInput, SecondsToSimulationTimeIntegralTypesTest, IntegralTypes, /*unused*/);

TEST(SimulationTimeToSecondsTest, GivenMantleTime_WhenTimeToSeconds_ThenSecondsAsDoubleReturned)
{
    EXPECT_EQ(0.001, mantle_api::TimeToSeconds(mantle_api::Time{1}));
    EXPECT_EQ(0.012, mantle_api::TimeToSeconds(mantle_api::Time{12}));
    EXPECT_EQ(0.123, mantle_api::TimeToSeconds(mantle_api::Time{123}));
    EXPECT_EQ(1.234, mantle_api::TimeToSeconds(mantle_api::Time{1'234}));
    EXPECT_EQ(12.345, mantle_api::TimeToSeconds(mantle_api::Time{12'345}));
    EXPECT_EQ(123.456, mantle_api::TimeToSeconds(mantle_api::Time{123'456}));
    EXPECT_EQ(1234.567, mantle_api::TimeToSeconds(mantle_api::Time{1'234'567}));
    EXPECT_EQ(12345.678, mantle_api::TimeToSeconds(mantle_api::Time{12'345'678}));
    EXPECT_EQ(123456.789, mantle_api::TimeToSeconds(mantle_api::Time{123'456'789}));
}

TEST(TimeTest, GivenSystemTime_WhenTimeNow_ThenTimeValuesInValidRanges)
{
    auto t = Time::Now();

    EXPECT_GE(t.hour, 0);
    EXPECT_LT(t.hour, 24);
    EXPECT_GE(t.min, 0);
    EXPECT_LT(t.min, 60);
    EXPECT_GE(t.sec, 0);
    EXPECT_LT(t.sec, 60);
}

}  // namespace astas::service::utility
