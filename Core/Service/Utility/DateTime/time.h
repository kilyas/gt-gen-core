/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_DATETIME_TIME_H
#define GTGEN_CORE_SERVICE_UTILITY_DATETIME_TIME_H

#include <fmt/format.h>

namespace astas::service::utility
{

class Time final
{
  public:
    static Time Now();

    int hour{0};
    int min{0};
    int sec{0};

  private:
    Time() = default;

    void SetNow();
};

}  // namespace astas::service::utility

template <>
struct fmt::formatter<astas::service::utility::Time>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const astas::service::utility::Time& d, FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return format_to(ctx.out(), "{:0>2}-{:0>2}-{:0>2}", d.hour, d.min, d.sec);
    }
};

#endif  // GTGEN_CORE_SERVICE_UTILITY_DATETIME_TIME_H
