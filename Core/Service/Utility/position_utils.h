/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_POSITIONUTILS_H
#define GTGEN_CORE_SERVICE_UTILITY_POSITIONUTILS_H

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <optional>
#include <vector>

namespace astas::service::utility
{

mantle_api::Vec3<units::length::meter_t> GetVerticalShiftedPosition(
    mantle_api::Vec3<units::length::meter_t> initial_position,
    const mantle_api::BoundingBox& bounding_box);

mantle_api::Vec3<units::length::meter_t> GetLateralShiftedPosition(
    mantle_api::Vec3<units::length::meter_t> initial_position,
    mantle_api::Vec3<units::length::meter_t> direction,
    mantle_api::Vec3<units::length::meter_t> lane_normal,
    units::length::meter_t t_offset);

std::optional<mantle_api::Vec3<units::length::meter_t>> GetLineIntersection2d(
    const mantle_api::Vec3<units::length::meter_t>& line1_p1,
    const mantle_api::Vec3<units::length::meter_t>& line1_p2,
    const mantle_api::Vec3<units::length::meter_t>& line2_p1,
    const mantle_api::Vec3<units::length::meter_t>& line2_p2);

/// @brief Calculate the intersect point of a given line to a set of line segments. If the line segments contain only
/// two points, then this function returns line to line-segment intersection.
///
///@param line_p1  Arbitrary point on the given line
///@param line_p2  Arbitrary point on the given line but should be different than line_p1
///@param line_segments  Points on the line segments. The given line segments must be connected connected end to end,
/// such as the center line of a lane.
///@return The intersected position.  No value, if the intersected position cannot be calculated,
/// e.g. given line is parallel/coincident to the given line segments, line_p1 and line_p2 is overlapping
std::optional<mantle_api::Vec3<units::length::meter_t>> GetLineToLineSegmentsIntersection2D(
    const mantle_api::Vec3<units::length::meter_t>& line_p1,
    const mantle_api::Vec3<units::length::meter_t>& line_p2,
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& line_segments);

/// @brief Determines if one vector is on the right side of the other
///
///@param vec1 given vector1
///@param vec2 given vector2
///@return true if vec1 is on the right side of vec2, false on the left
bool IsRight2D(const mantle_api::Vec3<units::length::meter_t>& vec1,
               const mantle_api::Vec3<units::length::meter_t>& vec2);

/// @brief Calculates the distance in the x-y plane of two vectors
///
///@param vec1 given vector1
///@param vec2 given vector2
///@return distance in the x-y plane of two vectors
double GetDistance2D(const mantle_api::Vec3<units::length::meter_t>& vec1,
                     const mantle_api::Vec3<units::length::meter_t>& vec2);

/// @brief Calculates the 3D distance between two vectors
///
///@param vec1 given vector1
///@param vec2 given vector2
///@return 3D distance between two vectors
double GetDistance3D(const mantle_api::Vec3<units::length::meter_t>& vec1,
                     const mantle_api::Vec3<units::length::meter_t>& vec2);

/// @brief Determines if two vectors are equal
/// @param vec1 given vector1
/// @param vec2 given vector2
/// @return true if vectors are equal, false otherwise
bool IsAlmostEqual(const mantle_api::Vec3<units::length::meter_t>& vec1,
                   const mantle_api::Vec3<units::length::meter_t>& vec2);

}  // namespace astas::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_POSITIONUTILS_H
