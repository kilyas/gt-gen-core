/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/version.h"

#include <gtest/gtest.h>

#include <cstdint>

namespace astas
{

namespace
{
// Helper function since GTest seems not to be able to handle aggregate initialization properly
constexpr Version MakeVersion(std::int32_t major, std::int32_t minor, std::int32_t patch)
{
    return Version{major, minor, patch};
}

}  // namespace

TEST(VersionTest, GivenTwoVersions_WhenLessThan_ThenOperatorWorksCorrectly)
{
    EXPECT_LT(MakeVersion(2019, 0, 1), MakeVersion(2020, 0, 1)) << "Difference in major";
    EXPECT_LT(MakeVersion(2019, 0, 1), MakeVersion(2019, 1, 1)) << "Difference in minor";
    EXPECT_LT(MakeVersion(2019, 0, 1), MakeVersion(2019, 0, 2)) << "Difference in patch";
    EXPECT_LT(MakeVersion(2019, 10, 2), MakeVersion(2020, 2, 2)) << "Difference in major & minor";
    EXPECT_LT(MakeVersion(2019, 0, 5), MakeVersion(2019, 2, 2)) << "Difference in minor & patch";
    EXPECT_LT(MakeVersion(2019, 10, 2), MakeVersion(2020, 5, 1)) << "Difference in all";

    EXPECT_GE(MakeVersion(2020, 0, 1), MakeVersion(2019, 0, 1)) << "Difference in major";
    EXPECT_GE(MakeVersion(2019, 1, 1), MakeVersion(2019, 0, 1)) << "Difference in minor";
    EXPECT_GE(MakeVersion(2019, 0, 2), MakeVersion(2019, 0, 1)) << "Difference in patch";
    EXPECT_GE(MakeVersion(2020, 2, 2), MakeVersion(2019, 10, 2)) << "Difference in major & minor";
    EXPECT_GE(MakeVersion(2019, 2, 2), MakeVersion(2019, 0, 5)) << "Difference in minor & patch";
    EXPECT_GE(MakeVersion(2020, 5, 1), MakeVersion(2019, 10, 2)) << "Difference in all";
}

TEST(VersionTest, GivenTwoVersions_WhenLessEqual_ThenOperatorWorksCorrectly)
{
    // as LessThan
    EXPECT_LE(MakeVersion(2019, 0, 1), MakeVersion(2020, 0, 1)) << "Difference in major";
    EXPECT_LE(MakeVersion(2019, 0, 1), MakeVersion(2019, 1, 1)) << "Difference in minor";
    EXPECT_LE(MakeVersion(2019, 0, 1), MakeVersion(2019, 0, 2)) << "Difference in patch";
    EXPECT_LE(MakeVersion(2019, 10, 2), MakeVersion(2020, 2, 2)) << "Difference in major & minor";
    EXPECT_LE(MakeVersion(2019, 0, 5), MakeVersion(2019, 2, 2)) << "Difference in minor & patch";
    EXPECT_LE(MakeVersion(2019, 10, 2), MakeVersion(2020, 5, 1)) << "Difference in all";

    EXPECT_GT(MakeVersion(2020, 0, 1), MakeVersion(2019, 0, 1)) << "Difference in major";
    EXPECT_GT(MakeVersion(2019, 1, 1), MakeVersion(2019, 0, 1)) << "Difference in minor";
    EXPECT_GT(MakeVersion(2019, 0, 2), MakeVersion(2019, 0, 1)) << "Difference in patch";
    EXPECT_GT(MakeVersion(2020, 2, 2), MakeVersion(2019, 10, 2)) << "Difference in major & minor";
    EXPECT_GT(MakeVersion(2019, 2, 2), MakeVersion(2019, 0, 5)) << "Difference in minor & patch";
    EXPECT_GT(MakeVersion(2020, 5, 1), MakeVersion(2019, 10, 2)) << "Difference in all";

    // equal
    EXPECT_LE(MakeVersion(2019, 10, 2), MakeVersion(2019, 10, 2));
}

TEST(VersionTest, GivenTwoVersions_WhenNotEqual_ThenOperatorWorksCorrectly)
{
    EXPECT_EQ(MakeVersion(2019, 0, 1), MakeVersion(2019, 0, 1));

    EXPECT_NE(MakeVersion(2019, 1, 0), MakeVersion(2019, 0, 1));
    EXPECT_NE(MakeVersion(2019, 0, 2), MakeVersion(2019, 0, 1));
    EXPECT_NE(MakeVersion(2020, 1, 2), MakeVersion(2019, 0, 1));
}

TEST(VersionTest, GivenTwoVersions_WhenGreaterThan_ThenOperatorWorksCorrectly)
{
    EXPECT_GT(MakeVersion(2020, 0, 1), MakeVersion(2019, 0, 1)) << "Difference in major";
    EXPECT_GT(MakeVersion(2019, 1, 1), MakeVersion(2019, 0, 1)) << "Difference in minor";
    EXPECT_GT(MakeVersion(2019, 0, 2), MakeVersion(2019, 0, 1)) << "Difference in patch";
    EXPECT_GT(MakeVersion(2020, 2, 2), MakeVersion(2019, 10, 2)) << "Difference in major & minor";
    EXPECT_GT(MakeVersion(2019, 2, 2), MakeVersion(2019, 0, 5)) << "Difference in minor & patch";
    EXPECT_GT(MakeVersion(2020, 5, 1), MakeVersion(2019, 10, 2)) << "Difference in all";

    EXPECT_LE(MakeVersion(2019, 0, 1), MakeVersion(2020, 0, 1)) << "Difference in major";
    EXPECT_LE(MakeVersion(2019, 0, 1), MakeVersion(2019, 1, 1)) << "Difference in minor";
    EXPECT_LE(MakeVersion(2019, 0, 1), MakeVersion(2019, 0, 2)) << "Difference in patch";
    EXPECT_LE(MakeVersion(2019, 10, 2), MakeVersion(2020, 2, 2)) << "Difference in major & minor";
    EXPECT_LE(MakeVersion(2019, 0, 5), MakeVersion(2019, 2, 2)) << "Difference in minor & patch";
    EXPECT_LE(MakeVersion(2019, 10, 2), MakeVersion(2020, 5, 1)) << "Difference in all";
}

TEST(VersionTest, GivenTwoVersions_WhenGreaterEqual_ThenOperatorWorksCorrectly)
{
    // as GreaterThan
    EXPECT_GE(MakeVersion(2020, 0, 1), MakeVersion(2019, 0, 1)) << "Difference in major";
    EXPECT_GE(MakeVersion(2019, 1, 1), MakeVersion(2019, 0, 1)) << "Difference in minor";
    EXPECT_GE(MakeVersion(2019, 0, 2), MakeVersion(2019, 0, 1)) << "Difference in patch";
    EXPECT_GE(MakeVersion(2020, 2, 2), MakeVersion(2019, 10, 2)) << "Difference in major & minor";
    EXPECT_GE(MakeVersion(2019, 2, 2), MakeVersion(2019, 0, 5)) << "Difference in minor & patch";
    EXPECT_GE(MakeVersion(2020, 5, 1), MakeVersion(2019, 10, 2)) << "Difference in all";

    EXPECT_LT(MakeVersion(2019, 0, 1), MakeVersion(2020, 0, 1)) << "Difference in major";
    EXPECT_LT(MakeVersion(2019, 0, 1), MakeVersion(2019, 1, 1)) << "Difference in minor";
    EXPECT_LT(MakeVersion(2019, 0, 1), MakeVersion(2019, 0, 2)) << "Difference in patch";
    EXPECT_LT(MakeVersion(2019, 10, 2), MakeVersion(2020, 2, 2)) << "Difference in major & minor";
    EXPECT_LT(MakeVersion(2019, 0, 5), MakeVersion(2019, 2, 2)) << "Difference in minor & patch";
    EXPECT_LT(MakeVersion(2019, 10, 2), MakeVersion(2020, 5, 1)) << "Difference in all";

    // equal
    EXPECT_GE(MakeVersion(2019, 10, 2), MakeVersion(2019, 10, 2));
}

TEST(VersionTest, GivenTwoVersions_WhenConstExprComparison_ThenComparisonCorrect)
{
    static_assert(MakeVersion(2019, 0, 1) < MakeVersion(2020, 1, 2));
    static_assert(MakeVersion(2019, 0, 1) <= MakeVersion(2020, 1, 2));
    static_assert(MakeVersion(2020, 1, 2) > MakeVersion(2019, 0, 1));
    static_assert(MakeVersion(2020, 1, 2) >= MakeVersion(2019, 0, 1));

    static_assert(MakeVersion(2019, 1, 2) == MakeVersion(2019, 1, 2));
    static_assert(MakeVersion(2019, 1, 2) <= MakeVersion(2019, 1, 2));
    static_assert(MakeVersion(2019, 1, 2) >= MakeVersion(2019, 1, 2));

    static_assert(MakeVersion(2019, 1, 2) != MakeVersion(2019, 2, 2));
}

}  // namespace astas
