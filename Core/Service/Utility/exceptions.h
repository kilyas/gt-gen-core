/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_EXCEPTIONS_H
#define GTGEN_CORE_SERVICE_UTILITY_EXCEPTIONS_H

#include "Core/Service/MantleApiExtension/formatting.h"

#include <exception>
#include <string>

namespace astas::service::utility::exception
{
class AstasException : public std::exception
{
  public:
    template <typename... Args>
    explicit AstasException(const char* fmt, const Args&... args) noexcept : message_{fmt::format(fmt, args...)}
    {
    }

    const char* what() const noexcept override { return message_.c_str(); }

  private:
    std::string message_;
};

class RequestedAlreadyReservedId : public AstasException
{
  public:
    explicit RequestedAlreadyReservedId(std::uint64_t requested_id)
        : AstasException{"Tried to reserve a unique id that was already used/reserved: {}", requested_id}
    {
    }
};

/// @brief Exception related to everything file i/o
class FileAccess : public AstasException
{
    using AstasException::AstasException;
};

}  // namespace astas::service::utility::exception

#endif  // GTGEN_CORE_SERVICE_UTILITY_EXCEPTIONS_H
