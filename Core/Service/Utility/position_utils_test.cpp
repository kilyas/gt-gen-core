/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Utility/position_utils.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::service::utility
{
using units::literals::operator""_m;

TEST(GetLineIntersection2dTest, GivenTwoCoincidentlLines_WhenGetLineIntersection2d_ThenNoValueReturned)
{
    const auto line1_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line1_p2 = mantle_api::Vec3<units::length::meter_t>{2.0_m, 1.0_m, 0.0_m};
    const auto intersection = GetLineIntersection2d(line1_p1, line1_p2, line1_p1, line1_p2);

    EXPECT_FALSE(intersection.has_value());
}

TEST(GetLineIntersection2dTest, GivenTwoParallelLines_WhenGetLineIntersection2d_ThenNoValueReturned)
{
    const auto line1_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line1_p2 = mantle_api::Vec3<units::length::meter_t>{2.0_m, 1.0_m, 0.0_m};
    const auto line2_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.5_m, 0.0_m};
    const auto line2_p2 = mantle_api::Vec3<units::length::meter_t>{2.0_m, 1.5_m, 0.0_m};
    const auto intersection = GetLineIntersection2d(line1_p1, line1_p2, line2_p1, line2_p2);

    EXPECT_FALSE(intersection.has_value());
}

TEST(GetLineIntersection2dTest, GivenOverlappedPointsForLines_WhenGetLineIntersection2d_ThenNoValueReturned)
{
    const auto line1_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line2_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.5_m, 0.0_m};
    const auto intersection = GetLineIntersection2d(line1_p1, line1_p1, line2_p1, line2_p1);

    EXPECT_FALSE(intersection.has_value());
}

TEST(GetLineIntersection2dTest, GivenTwoLines_WhenGetLineIntersection2d_ThenCorrectValueReturned)
{
    // ^ y
    // |           o(2, 4)
    // |           |
    // |           |
    // | (1,1)o----x-----o(3,1)
    // |           |
    // |           |
    // |           o(2, 0)
    // --------------------------->x
    const auto expected_intersection = mantle_api::Vec3<units::length::meter_t>{2.0_m, 1.0_m, 0.0_m};

    const auto line1_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line1_p2 = mantle_api::Vec3<units::length::meter_t>{3.0_m, 1.0_m, 0.0_m};
    const auto line2_p1 = mantle_api::Vec3<units::length::meter_t>{2.0_m, 0.0_m, 0.0_m};
    const auto line2_p2 = mantle_api::Vec3<units::length::meter_t>{2.0_m, 4.0_m, 0.0_m};
    const auto intersection = GetLineIntersection2d(line1_p1, line1_p2, line2_p1, line2_p2);

    ASSERT_TRUE(intersection.has_value());
    EXPECT_TRIPLE(expected_intersection, intersection.value());
}

TEST(GetLineToLineSegmentsIntersection2DTest,
     GivenEmptyTargetCenterLine_WhenGetLineToLineSegmentsIntersection2D_ThenNoValueReturned)
{

    const auto line_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line_p2 = mantle_api::Vec3<units::length::meter_t>{2.0_m, 1.0_m, 0.0_m};
    const auto line_segments = std::vector<mantle_api::Vec3<units::length::meter_t>>{};
    const auto intersection = GetLineToLineSegmentsIntersection2D(line_p1, line_p2, line_segments);

    EXPECT_FALSE(intersection.has_value());
}

TEST(GetLineToLineSegmentsIntersection2DTest,
     GivenOverlappedPointsForLines_WhenGetLineToLineSegmentsIntersection2D_ThenNoValueReturned)
{

    const auto line_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line_segments_p1 = mantle_api::Vec3<units::length::meter_t>{5.0_m, 0.0_m, 0.0_m};
    const auto line_segments_p2 = mantle_api::Vec3<units::length::meter_t>{10.0_m, 0.0_m, 0.0_m};
    const auto line_segments =
        std::vector<mantle_api::Vec3<units::length::meter_t>>{line_segments_p1, line_segments_p2};
    const auto intersection = GetLineToLineSegmentsIntersection2D(line_p1, line_p1, line_segments);

    EXPECT_FALSE(intersection.has_value());
}

TEST(GetLineToLineSegmentsIntersection2DTest,
     GivenSourcePointExceedTargetLine_WhenGetLineToLineSegmentsIntersection2D_ThenNoValueReturned)
{
    //
    //      o p2 (1, 2)
    //      |
    //      o p1 (1, 1)
    //      |
    //      |
    //      x               o----------------o   line segments
    //                    (5,0)           (10,0)

    const auto line_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line_p2 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 2.0_m, 0.0_m};
    const auto line_segments_p1 = mantle_api::Vec3<units::length::meter_t>{5.0_m, 0.0_m, 0.0_m};
    const auto line_segments_p2 = mantle_api::Vec3<units::length::meter_t>{10.0_m, 0.0_m, 0.0_m};
    const auto line_segments =
        std::vector<mantle_api::Vec3<units::length::meter_t>>{line_segments_p1, line_segments_p2};
    const auto intersection = GetLineToLineSegmentsIntersection2D(line_p1, line_p2, line_segments);

    EXPECT_FALSE(intersection.has_value());
}

TEST(GetLineToLineSegmentsIntersection2DTest,
     GivenSourcePointTargetLine_WhenGetLineToLineSegmentsIntersection2D_ThenCorrectValueReturned)
{
    //        o p2 (1, 2)
    //        |
    //        o p1 (1, 1)
    //        |
    //        |
    //  o-----x---------o---------------o    line segments
    // (0,0)          (5,0)           (10,0)

    const auto line_p1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 0.0_m};
    const auto line_p2 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 2.0_m, 0.0_m};
    const auto line_segments_p1 = mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0.0_m};
    const auto line_segments_p2 = mantle_api::Vec3<units::length::meter_t>{5.0_m, 0.0_m, 0.0_m};
    const auto line_segments_p3 = mantle_api::Vec3<units::length::meter_t>{10.0_m, 0.0_m, 0.0_m};
    const auto line_segments =
        std::vector<mantle_api::Vec3<units::length::meter_t>>{line_segments_p1, line_segments_p2, line_segments_p3};
    const auto intersection = GetLineToLineSegmentsIntersection2D(line_p1, line_p2, line_segments);

    const auto expected_intersection = mantle_api::Vec3<units::length::meter_t>{1.0_m, 0.0_m, 0.0_m};

    ASSERT_TRUE(intersection.has_value());
    EXPECT_TRIPLE(expected_intersection, intersection.value());
}

TEST(IsRight2DTest, GivenPositionOnTheLeftOfTheLine_WhenIsRight2D_ThenReturnPositiveOne)
{
    //        ^  vec1(0, 1)
    //        |
    //        |
    //  o--------------->   vec2 = (1,0)
    //

    const auto vec1 = mantle_api::Vec3<units::length::meter_t>{0.0_m, 1.0_m, 0.0_m};
    const auto vec2 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 0.0_m, 0.0_m};

    EXPECT_FALSE(IsRight2D(vec1, vec2));
}

TEST(IsRight2DTest, GivenPositionOnTheRightOfTheLine_WhenIsRight2D_ThenReturnNegativeOne)
{
    //        ^  vec2(0, 1)
    //        |
    //        |
    //  o--------------->   vec1 = (1,0)
    //

    const auto vec2 = mantle_api::Vec3<units::length::meter_t>{0.0_m, 1.0_m, 0.0_m};
    const auto vec1 = mantle_api::Vec3<units::length::meter_t>{1.0_m, 0.0_m, 0.0_m};

    EXPECT_TRUE(IsRight2D(vec1, vec2));
}

TEST(GetDistance2D, GivenTwoVectors_WhenDistance2D_ThenDistanceComputedCorrectly)
{
    const auto vec2 = mantle_api::Vec3<units::length::meter_t>{5.0_m, 5.0_m, 7.8_m};
    const auto vec1 = mantle_api::Vec3<units::length::meter_t>{5.0_m, 0.0_m, 9.1_m};

    EXPECT_DOUBLE_EQ(5.0, GetDistance2D(vec1, vec2));
}

TEST(GetDistance3D, GivenTwoVectors_WhenDistance3D_ThenDistanceComputedCorrectly)
{
    const mantle_api::Vec3<units::length::meter_t> vec2{1.0_m, 1.0_m, 1.0_m};
    const mantle_api::Vec3<units::length::meter_t> vec1{0.0_m, 0.0_m, 0.0_m};
    const double expected_distance_m{1.7320508075688772};

    EXPECT_DOUBLE_EQ(expected_distance_m, GetDistance3D(vec1, vec2));
}

TEST(AreEqual, GivenSameVectors_WhenIsAlmostEqual_ThenExpectTrue)
{
    const mantle_api::Vec3<units::length::meter_t> vec{1.0_m, 1.0_m, 1.0_m};
    EXPECT_TRUE(IsAlmostEqual(vec, vec));
}

TEST(AreEqual, GivenVectorsWithValuesDifferenceLessThanEpsilon_WhenIsAlmostEqual_ThenExpectTrue)
{
    const mantle_api::Vec3<units::length::meter_t> vec1{1.0_m, 1.0_m, 1.0_m};
    const mantle_api::Vec3<units::length::meter_t> vec2{1.000001_m, 1.0_m, 1.0_m};
    EXPECT_TRUE(IsAlmostEqual(vec1, vec2));
}

TEST(AreEqual, GivenDifferentVectors_WhenIsAlmostEqual_ThenExpectTrue)
{
    const mantle_api::Vec3<units::length::meter_t> vec1{1.0_m, 1.0_m, 1.0_m};
    const mantle_api::Vec3<units::length::meter_t> vec2{1.1_m, 1.0_m, 1.0_m};
    EXPECT_FALSE(IsAlmostEqual(vec1, vec2));
}

}  // namespace astas::service::utility
