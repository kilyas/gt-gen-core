/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_ALGORITHMUTILS_H
#define GTGEN_CORE_SERVICE_UTILITY_ALGORITHMUTILS_H

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <memory>
#include <utility>
#include <vector>

namespace astas::service::utility
{

/// @brief Iterates over a range and for each pair of adjacent elements, searches and evaluates the given predicate.
///
/// This is intended to work similar as std::adjacent_find, but instead of just returning the iterator, it also returns
/// the result of the predicate. For this to work, the predicate needs to return a type with the semantics of
/// std::optional (or a pointer), specifically it needs to provide a conversion operator to bool to check if the correct
/// element-pair in the list has been found, and a dereference-operator (operator*) to return the value computed from
/// the element-pair.
///
/// @code{.cpp}
/// std::vector<int> v{0, 1, 2, 3, 4, 5, 6, 7};
/// const double value_to_find{3.3};
/// const auto& [iter, dist] = AdjacentFindAndEvaluate(v.begin(), v.end(),
///   [&value_to_find](int a, int b) -> std::optional<double> {
///     if (a <= value_to_find && value_to_find < b)
///     {
///         return std::optional<double>(static_cast<double>(value_to_find) - static_cast<double>(a));
///     }
///     else
///     {
///         return std::nullopt;
///     }
/// });
///
/// // --> *iter = 3
/// // --> dist = 0.3
/// @endcode
///
/// @tparam ForwardIt Forward iterator type.
/// @tparam BinaryEvaluator Binary predicate to evaluate, should be able to take arguments in form (const *ForwardIt&,
/// const *ForwardIt&)
/// @param first Start of the range iterator.
/// @param last Past-the-end of the range iterator.
/// @param evaluator Predicate to evaluate.
/// @return Pair containing the first of the iterator-pair for which predicate evaluated to true and the dereferenced
/// result of the evaluation of this iterator-pair.
template <typename ForwardIt, typename BinaryEvaluator>
auto AdjacentFindAndEvaluate(ForwardIt first, ForwardIt last, BinaryEvaluator evaluator)
{
    using Value = decltype(*evaluator(*first, *last));
    if (first == last)
    {
        return std::make_pair(last, Value{});
    }

    for (ForwardIt next = std::next(first); next != last; ++next, ++first)
    {
        if (auto result{evaluator(*first, *next)}; result)
        {
            return std::make_pair(first, *result);
        }
    }

    return std::make_pair(last, Value{});
}

/// @brief Mimics `std::for_each`, but applies function to each adjacent pair in the range `[first, last)`.
///
/// Example:
/// @code{.cpp}
/// std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8};
/// std::vector<std::pair<int, int>> p;
///
/// ForAdjacentEach(v.begin(), v.end(), [&p](int a, int b) { p.emplace_back(a, b); });
///
/// // p will contain:
/// // (1,2)
/// // (2,3)
/// // (3,4)
/// // (4,5)
/// // (5,6)
/// // (6,7)
/// // (7,8)
/// @endcode
///
/// @tparam InputIt Ranges iterator type, must meet the requirements of *LegacyInputIterator*.
/// @tparam BinaryFunction Function to apply for each adjacent pair, will be called as `function(*InputIt, *InputIt)`.
/// @param first First element in the range.
/// @param last Last element in the range.
/// @param function Binary function that is applied on each adjacent element-pair in the range.
/// @return
template <typename InputIt, typename BinaryFunction>
BinaryFunction ForAdjacentEach(InputIt first, InputIt last, BinaryFunction function)
{
    if (first != last)
    {
        for (InputIt next = std::next(first); next != last; ++next, ++first)
        {
            function(*first, *next);
        }
    }

    return function;
}

/// @brief Checks whether a collection contains an element given a predicate.
template <typename C, typename T>
auto ContainsIf(const C& v, const T& x) -> decltype(end(v), true)
{
    return end(v) != std::find_if(begin(v), end(v), x);
}

/// @brief Checks whether a collection contains an element given a value.
template <typename C, typename T>
auto Contains(const C& v, const T& x) -> decltype(end(v), true)
{
    return end(v) != std::find(begin(v), end(v), x);
}

/// @brief Checks whether vector A contains all elements of vector B.
template <typename T>
bool IsSubset(std::vector<T> a, std::vector<T> b)
{
    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());
    return std::includes(a.begin(), a.end(), b.begin(), b.end());
}

/// @brief Checks whether two collections are equal.
/// Should not be used to compare std::unordered_set, std::unordered_multiset, std::unordered_map, or
/// std::unordered_multimap because the order in which the elements are stored in those containers may be different even
/// if the two containers store the same elements.
template <typename T>
bool AreContainersEqual(const T& vector, const T& another_vector)
{
    return std::equal(vector.begin(), vector.end(), another_vector.begin(), another_vector.end());
}

template <typename T>
void SortAndRemoveDuplicates(T& vector)
{
    std::sort(vector.begin(), vector.end());
    vector.erase(std::unique(vector.begin(), vector.end()), vector.end());
}

template <typename T>
void RemoveAdjacentDuplicates(T& vector)
{
    vector.erase(std::unique(vector.begin(), vector.end()), vector.end());
}

template <typename T>
typename std::vector<std::unique_ptr<T>>::const_iterator FindObjectById(const std::vector<std::unique_ptr<T>>& vector,
                                                                        std::uint64_t id)
{
    auto it = std::find_if(vector.cbegin(), vector.cend(), [&id](const std::unique_ptr<T>& object) {
        return object->GetUniqueId() == id;
    });

    return it;
}

}  // namespace astas::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_ALGORITHMUTILS_H
