/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_VERSION_H
#define GTGEN_CORE_SERVICE_UTILITY_VERSION_H

#include <fmt/format.h>

#include <cstdint>

namespace astas
{

/// @brief This struct represents a version
struct Version
{
    std::int32_t major{0};
    std::int32_t minor{0};
    std::int32_t patch{0};
};

inline constexpr bool operator==(const Version& a, const Version& b) noexcept
{
    return a.major == b.major && a.minor == b.minor && a.patch == b.patch;
}

inline constexpr bool operator!=(const Version& a, const Version& b) noexcept
{
    return !(a == b);
}

inline constexpr bool operator<(const Version& a, const Version& b) noexcept
{
    if (a.major == b.major)
    {
        if (a.minor == b.minor)
        {
            return a.patch < b.patch;
        }

        return a.minor < b.minor;
    }

    return a.major < b.major;
}

inline constexpr bool operator>(const Version& a, const Version& b) noexcept
{
    return b < a;
}

inline constexpr bool operator<=(const Version& a, const Version& b) noexcept
{
    return !(b < a);
}

inline constexpr bool operator>=(const Version& a, const Version& b) noexcept
{
    return !(a < b);
}

}  // namespace astas

namespace fmt
{
template <>
struct formatter<astas::Version>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const astas::Version& v, FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return format_to(ctx.out(), "{}.{}.{}", v.major, v.minor, v.patch);
    }
};
}  // namespace fmt

#endif  // GTGEN_CORE_SERVICE_UTILITY_VERSION_H
