/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Utility/orientation_utils.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::service::utility
{

using units::literals::operator""_deg;
using units::literals::operator""_rad;
using units::literals::operator""_mps;
using Orientation3_rad = mantle_api::Orientation3<units::angle::radian_t>;
using Orientation3_deg = mantle_api::Orientation3<units::angle::degree_t>;
using Velocity = mantle_api::Vec3<units::velocity::meters_per_second_t>;

constexpr auto m_pi_4_rad{units::angle::radian_t{M_PI_4}};
constexpr auto m_pi_2_rad{units::angle::radian_t{M_PI_2}};
constexpr auto m_pi_3_4_rad{m_pi_2_rad + m_pi_4_rad};
constexpr auto m_pi_rad{units::angle::radian_t{M_PI}};

struct CalculateClampedDeltaOrientationTestType
{
    Orientation3_rad orientation_to;
    Orientation3_rad orientation_from;
    Orientation3_rad expected_delta_orientation;
};

using CalculateClampedDeltaOrientationTest = testing::TestWithParam<CalculateClampedDeltaOrientationTestType>;

INSTANTIATE_TEST_SUITE_P(
    OrientationUtils,
    CalculateClampedDeltaOrientationTest,
    testing::Values(
        CalculateClampedDeltaOrientationTestType{// 0° - 0° = 0°
                                                 Orientation3_rad{0_rad, 0_rad, 0_rad},
                                                 Orientation3_rad{0_rad, 0_rad, 0_rad},
                                                 Orientation3_rad{0_rad, 0_rad, 0_rad}},
        CalculateClampedDeltaOrientationTestType{// -180° - 180° = 0°
                                                 Orientation3_rad{-m_pi_rad, -m_pi_rad, -m_pi_rad},
                                                 Orientation3_rad{m_pi_rad, m_pi_rad, m_pi_rad},
                                                 Orientation3_rad{0_rad, 0_rad, 0_rad}},
        CalculateClampedDeltaOrientationTestType{// 180° - (-180°) = 0°
                                                 Orientation3_rad{m_pi_rad, m_pi_rad, m_pi_rad},
                                                 Orientation3_rad{-m_pi_rad, -m_pi_rad, -m_pi_rad},
                                                 Orientation3_rad{0_rad, 0_rad, 0_rad}},
        CalculateClampedDeltaOrientationTestType{// 90° - 45° = 45°
                                                 Orientation3_rad{m_pi_2_rad, m_pi_2_rad, m_pi_2_rad},
                                                 Orientation3_rad{m_pi_4_rad, m_pi_4_rad, m_pi_4_rad},
                                                 Orientation3_rad{m_pi_4_rad, m_pi_4_rad, m_pi_4_rad}},
        CalculateClampedDeltaOrientationTestType{// -45° - (-90°) = 45°
                                                 Orientation3_rad{-m_pi_4_rad, -m_pi_4_rad, -m_pi_4_rad},
                                                 Orientation3_rad{-m_pi_2_rad, -m_pi_2_rad, -m_pi_2_rad},
                                                 Orientation3_rad{m_pi_4_rad, m_pi_4_rad, m_pi_4_rad}},
        CalculateClampedDeltaOrientationTestType{// 45° - (-45°) = 90°
                                                 Orientation3_rad{m_pi_4_rad, m_pi_4_rad, m_pi_4_rad},
                                                 Orientation3_rad{-m_pi_4_rad, -m_pi_4_rad, -m_pi_4_rad},
                                                 Orientation3_rad{m_pi_2_rad, m_pi_2_rad, m_pi_2_rad}},
        CalculateClampedDeltaOrientationTestType{// -135° - 135° = 90°
                                                 Orientation3_rad{-m_pi_3_4_rad, -m_pi_3_4_rad, -m_pi_3_4_rad},
                                                 Orientation3_rad{m_pi_3_4_rad, m_pi_3_4_rad, m_pi_3_4_rad},
                                                 Orientation3_rad{m_pi_2_rad, m_pi_2_rad, m_pi_2_rad}},
        CalculateClampedDeltaOrientationTestType{// 135° - (-135)° = -90°
                                                 Orientation3_rad{m_pi_3_4_rad, m_pi_3_4_rad, m_pi_3_4_rad},
                                                 Orientation3_rad{-m_pi_3_4_rad, -m_pi_3_4_rad, -m_pi_3_4_rad},
                                                 Orientation3_rad{-m_pi_2_rad, -m_pi_2_rad, -m_pi_2_rad}},
        CalculateClampedDeltaOrientationTestType{// 180° - 0° = 180°
                                                 Orientation3_rad{m_pi_rad, m_pi_rad, m_pi_rad},
                                                 Orientation3_rad{0_rad, 0_rad, 0_rad},
                                                 Orientation3_rad{m_pi_rad, m_pi_rad, m_pi_rad}},
        CalculateClampedDeltaOrientationTestType{// -180° - 0° = -180°
                                                 Orientation3_rad{-m_pi_rad, -m_pi_rad, -m_pi_rad},
                                                 Orientation3_rad{0_rad, 0_rad, 0_rad},
                                                 Orientation3_rad{-m_pi_rad, -m_pi_rad, -m_pi_rad}},
        CalculateClampedDeltaOrientationTestType{// 90° - (-90°) = 180°
                                                 Orientation3_rad{m_pi_2_rad, m_pi_2_rad, m_pi_2_rad},
                                                 Orientation3_rad{-m_pi_2_rad, -m_pi_2_rad, -m_pi_2_rad},
                                                 Orientation3_rad{m_pi_rad, m_pi_rad, m_pi_rad}},
        CalculateClampedDeltaOrientationTestType{// 90° - (-135°) = -135°
                                                 Orientation3_rad{m_pi_2_rad, m_pi_2_rad, m_pi_2_rad},
                                                 Orientation3_rad{-m_pi_3_4_rad, -m_pi_3_4_rad, -m_pi_3_4_rad},
                                                 Orientation3_rad{-m_pi_3_4_rad, -m_pi_3_4_rad, -m_pi_3_4_rad}},
        CalculateClampedDeltaOrientationTestType{// -90° - 135° = 135°
                                                 Orientation3_rad{-m_pi_2_rad, -m_pi_2_rad, -m_pi_2_rad},
                                                 Orientation3_rad{m_pi_3_4_rad, m_pi_3_4_rad, m_pi_3_4_rad},
                                                 Orientation3_rad{m_pi_3_4_rad, m_pi_3_4_rad, m_pi_3_4_rad}},
        CalculateClampedDeltaOrientationTestType{// -90° - 90° = -180°
                                                 Orientation3_rad{-m_pi_2_rad, -m_pi_2_rad, -m_pi_2_rad},
                                                 Orientation3_rad{m_pi_2_rad, m_pi_2_rad, m_pi_2_rad},
                                                 Orientation3_rad{-m_pi_rad, -m_pi_rad, -m_pi_rad}}));

TEST_P(CalculateClampedDeltaOrientationTest,
       GivenTwoOrientations_WhenCalculateClampedDeltaOrientation_ThenValuesGetClamped)
{
    const auto test_vector = GetParam();
    const auto delta_orientation =
        CalculateClampedDeltaOrientation(test_vector.orientation_to, test_vector.orientation_from);
    EXPECT_EQ(delta_orientation, test_vector.expected_delta_orientation);
}

struct RotateVectorTestType
{
    Orientation3_deg orientation;
    Velocity world_frame_velocity;
    Velocity expected_local_frame_velocity;
};

using RotateVectorTest = testing::TestWithParam<RotateVectorTestType>;

INSTANTIATE_TEST_SUITE_P(OrientationUtils,
                         RotateVectorTest,
                         testing::Values(RotateVectorTestType{Orientation3_deg{90_deg, 0_deg, 0_deg},
                                                              Velocity{25_mps, 0_mps, 0_mps},
                                                              Velocity{0_mps, 25_mps, 0_mps}},
                                         RotateVectorTestType{Orientation3_deg{0_deg, 90_deg, 0_deg},
                                                              Velocity{25_mps, 0_mps, 0_mps},
                                                              Velocity{0_mps, 0_mps, -25_mps}},
                                         RotateVectorTestType{Orientation3_deg{0_deg, 0_deg, 90_deg},
                                                              Velocity{0_mps, 25_mps, 0_mps},
                                                              Velocity{0_mps, 0_mps, 25_mps}},
                                         RotateVectorTestType{Orientation3_deg{180_deg, 90_deg, 45_deg},
                                                              Velocity{25_mps, 0_mps, 25_mps},
                                                              Velocity{-17.67766953_mps, 17.67766953_mps, -25._mps}}));

TEST_P(RotateVectorTest,
       GivenAnOrientationAndVelocity_WhenRotatingVelocityWithOrientation_ThenRotatedVelocityIsAsExpected)
{
    const auto values{GetParam()};
    const auto local_frame_velocity{RotateVector(values.world_frame_velocity, values.orientation)};
    EXPECT_TRIPLE(values.expected_local_frame_velocity, local_frame_velocity);
}

}  // namespace astas::service::utility
