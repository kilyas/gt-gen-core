/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/unique_id_provider.h"

#include "Core/Service/Logging/logging.h"  // IWYU pragma: keep
#include "Core/Service/Logging/logging_template.h"
#include "Core/Service/Utility/exceptions.h"

#include <utility>

namespace astas::service::utility
{

void UniqueIdProvider::Reset()
{
    reserved_ids_ = {0};
    last_provided_id_ = 0;
}

mantle_api::UniqueId UniqueIdProvider::GetUniqueId()
{
    const auto found_id = SearchNextUnusedId(last_provided_id_ + 1);
    reserved_ids_.insert(found_id);
    return found_id;
}

void UniqueIdProvider::ReserveId(mantle_api::UniqueId id_to_reserve)
{
    const auto ret = reserved_ids_.insert(id_to_reserve);

    if (!ret.second)
    {
        ASTAS_LOG_AND_THROW_WITH_FILE_DETAILS(exception::RequestedAlreadyReservedId{id_to_reserve});
    }
}

void UniqueIdProvider::ReserveIds(const std::vector<mantle_api::UniqueId>& ids_to_reserve)
{
    for (const auto& id : ids_to_reserve)
    {
        ReserveId(id);
    }
}

void UniqueIdProvider::ReserveIds(mantle_api::UniqueId from, mantle_api::UniqueId to)
{
    for (mantle_api::UniqueId id = from; id <= to; ++id)
    {
        ReserveId(id);
    }
}

bool UniqueIdProvider::IsIdReserved(const mantle_api::UniqueId id) const
{
    return reserved_ids_.find(id) != reserved_ids_.end();
}

// NOLINTNEXTLINE(misc-no-recursion)
mantle_api::UniqueId UniqueIdProvider::SearchNextUnusedId(mantle_api::UniqueId search_start_id)
{
    if (IsIdReserved(search_start_id))
    {
        return SearchNextUnusedId(search_start_id + 1);
    }

    return last_provided_id_ = search_start_id;
}

mantle_api::UniqueId UniqueIdProvider::TryToReserveOrGenerateUniqueId(const mantle_api::UniqueId object_id,
                                                                      [[maybe_unused]] const std::string& object_type)
{
    if (!IsIdReserved(object_id))
    {
        ReserveId(object_id);

        return object_id;
    }

    auto unique_id = GetUniqueId();

    TRACE("Found duplicated ID '{}' of object type '{}'. New generated ID for this object is '{}'.",
          object_id,
          object_type,
          unique_id);

    return unique_id;
}

}  // namespace astas::service::utility
