/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Utility/derivative_utils.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

namespace astas::service::utility
{

using units::literals::operator""_s;
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;
using PositionVector = mantle_api::Vec3<units::length::meter_t>;
using VelocityVector = mantle_api::Vec3<units::velocity::meters_per_second_t>;
using AccelerationVector = mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>;
using OrientationVector = mantle_api::Orientation3<units::angle::radian_t>;
using OrientationRateVector = mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>;
using OrientationAccelerationVector =
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>;

TEST(GetDerivative, GivenANullElapsedTime_WhenCallingGetDerivative_ThenAssertionHit)
{
    EXPECT_THROW((GetDerivative<units::length::meter_t, units::velocity::meters_per_second_t>(0_m, 0_m, 0_s)),
                 std::logic_error);
}

TEST(
    GetDerivative,
    GivenThreeLengthsAndAnElapsedTime_WhenProcessingTheDerivatives_ThenFunctionReturnsExpectedVelocitiesAndAcceleration)
{
    const auto elapsed_time{2.0_s};
    const auto length_0{0.0_m};
    const auto length_1{2.0_m};
    const auto length_2{3.0_m};
    const auto expected_velocity_0{1.0_mps};
    const auto expected_velocity_1{0.5_mps};
    const auto expected_acceleration{-0.25_mps_sq};

    const auto velocity_0{
        GetDerivative<units::length::meter_t, units::velocity::meters_per_second_t>(length_1, length_0, elapsed_time)};

    const auto velocity_1{
        GetDerivative<units::length::meter_t, units::velocity::meters_per_second_t>(length_2, length_1, elapsed_time)};

    const auto acceleration{
        GetDerivative<units::velocity::meters_per_second_t, units::acceleration::meters_per_second_squared_t>(
            velocity_1, velocity_0, elapsed_time)};

    EXPECT_EQ(expected_velocity_0, velocity_0);
    EXPECT_EQ(expected_velocity_1, velocity_1);
    EXPECT_EQ(expected_acceleration, acceleration);
}

TEST(
    GetPositionDerivativeVector,
    GivenThreePositionVectorsAndAnElapsedTime_WhenProcessingTheDerivatives_ThenFunctionReturnsExpectedVelocitiesAndAcceleration)
{
    const auto elapsed_time{2.0_s};
    const auto position_0{PositionVector{0.0_m, 0.0_m, 0.0_m}};
    const auto position_1{PositionVector{2.0_m, 2.0_m, 2.0_m}};
    const auto position_2{PositionVector{3.0_m, 3.0_m, 3.0_m}};
    const auto expected_velocity_0{VelocityVector(1.0_mps, 1.0_mps, 1.0_mps)};
    const auto expected_velocity_1{VelocityVector(0.5_mps, 0.5_mps, 0.5_mps)};
    const auto expected_acceleration{AccelerationVector(-0.25_mps_sq, -0.25_mps_sq, -0.25_mps_sq)};

    const auto velocity_0{GetPositionDerivativeVector<units::length::meter_t, units::velocity::meters_per_second_t>(
        position_1, position_0, elapsed_time)};

    const auto velocity_1{GetPositionDerivativeVector<units::length::meter_t, units::velocity::meters_per_second_t>(
        position_2, position_1, elapsed_time)};

    const auto acceleration{GetPositionDerivativeVector<units::velocity::meters_per_second_t,
                                                        units::acceleration::meters_per_second_squared_t>(
        velocity_1, velocity_0, elapsed_time)};

    EXPECT_TRIPLE(expected_velocity_0, velocity_0);
    EXPECT_TRIPLE(expected_velocity_1, velocity_1);
    EXPECT_TRIPLE(expected_acceleration, acceleration);
}

TEST(
    GetOrientationDerivative,
    GivenThreeOrientationVectorsAndAnElapsedTime_WhenProcessingTheDerivatives_ThenFunctionReturnsExpectedOrientationRatesAndOrientationAcceleration)
{
    const auto elapsed_time{2.0_s};
    const auto orientation_0{OrientationVector{0.0_rad, 0.0_rad, 0.0_rad}};
    const auto orientation_1{OrientationVector{2.0_rad, 2.0_rad, 2.0_rad}};
    const auto orientation_2{OrientationVector{3.0_rad, 3.0_rad, 3.0_rad}};
    const auto expected_orientation_rate_0{OrientationRateVector(1.0_rad_per_s, 1.0_rad_per_s, 1.0_rad_per_s)};
    const auto expected_orientation_rate_1{OrientationRateVector(0.5_rad_per_s, 0.5_rad_per_s, 0.5_rad_per_s)};
    const auto expected_orientation_acceleration{
        OrientationAccelerationVector(-0.25_rad_per_s_sq, -0.25_rad_per_s_sq, -0.25_rad_per_s_sq)};

    const auto orientation_rate_0{
        GetOrientationDerivativeVector<units::angle::radian_t, units::angular_velocity::radians_per_second_t>(
            orientation_1, orientation_0, elapsed_time)};

    const auto orientation_rate_1{
        GetOrientationDerivativeVector<units::angle::radian_t, units::angular_velocity::radians_per_second_t>(
            orientation_2, orientation_1, elapsed_time)};

    const auto orientation_acceleration{
        GetOrientationDerivativeVector<units::angular_velocity::radians_per_second_t,
                                       units::angular_acceleration::radians_per_second_squared_t>(
            orientation_rate_1, orientation_rate_0, elapsed_time)};

    EXPECT_TRIPLE(expected_orientation_rate_0, orientation_rate_0);
    EXPECT_TRIPLE(expected_orientation_rate_1, orientation_rate_1);
    EXPECT_TRIPLE(expected_orientation_acceleration, orientation_acceleration);
}

}  // namespace astas::service::utility
