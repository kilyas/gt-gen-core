/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/unique_id_provider.h"

#include "Core/Service/Utility/exceptions.h"

#include <fmt/format.h>
#include <gtest/gtest.h>

#include <array>

namespace astas::service::utility
{

TEST(GetUniqueIdTest, GivenReservedIds_WhenGetUniqueId_ThenDoesNotReturnReservedIds)
{
    UniqueIdProvider provider{};

    EXPECT_EQ(1, provider.GetUniqueId());

    provider.ReserveId(2);
    EXPECT_EQ(3, provider.GetUniqueId());

    provider.ReserveIds({4, 5});
    EXPECT_EQ(6, provider.GetUniqueId());

    provider.ReserveIds({7, 9});
    EXPECT_EQ(8, provider.GetUniqueId());

    provider.ReserveIds({10, 12});
    EXPECT_EQ(11, provider.GetUniqueId());

    provider.ReserveIds({13, 14});
    EXPECT_EQ(15, provider.GetUniqueId());

    provider.ReserveIds(16, 30);
    EXPECT_EQ(31, provider.GetUniqueId());
}

TEST(ResetTest, GivenReservedIds_WhenReset_ThenPreviouslyReservedIdsReturned)
{
    UniqueIdProvider provider{};

    provider.GetUniqueId();
    provider.ReserveIds({2, 5});

    provider.Reset();

    EXPECT_EQ(1, provider.GetUniqueId());
}

TEST(ReserveIdTest, GivenIdZeroReservedForHost_WhenReserveIdZero_ThenExceptionThrown)
{
    UniqueIdProvider provider{};

    EXPECT_THROW({ provider.ReserveId(0); }, exception::RequestedAlreadyReservedId);
}

TEST(IsIdReservedTest, GivenIdWhichIsNotReserved_WhenCheckingIfIdIsReserved_ThenFalseIsReturned)
{
    UniqueIdProvider provider{};
    mantle_api::UniqueId id{34};

    const bool is_id_reserved = provider.IsIdReserved(id);

    ASSERT_FALSE(is_id_reserved);
}

TEST(IsIdReservedTest, GivenIdWhichIsReserved_WhenCheckingIfIdIsReserved_ThenTrueIsReturned)
{
    UniqueIdProvider provider{};
    provider.ReserveId(34);
    mantle_api::UniqueId id{34};

    const bool is_id_reserved = provider.IsIdReserved(id);

    ASSERT_TRUE(is_id_reserved);
}

TEST(TryToReserveOrGenerateUniqueIdTest, GivenMultipleUniqueIds_WhenGeneratingIds_ThenForEachGivenIdAUniqueIdIsReturned)
{
    constexpr std::array<mantle_api::UniqueId, 5> unique_ids{1, 2, 3, 4, 5};
    UniqueIdProvider unique_id_provider;

    for (const mantle_api::UniqueId unique_id : unique_ids)
    {
        const mantle_api::UniqueId generated_id =
            unique_id_provider.TryToReserveOrGenerateUniqueId(unique_id, "Barrier");
        EXPECT_EQ(unique_id, generated_id) << fmt::format("With unique id: {}", unique_id);
    }
}

TEST(TryToReserveOrGenerateUniqueIdTest,
     GivenMultipleIdenticalIds_WhenGeneratingIds_ThenForEachGivenIdAUniqueIdIsReturned)
{
    constexpr std::array<mantle_api::UniqueId, 5> ids{1, 3, 3, 3, 4};
    constexpr std::array<mantle_api::UniqueId, 5> expected_ids{1, 3, 2, 4, 5};
    UniqueIdProvider unique_id_provider;

    for (std::size_t i{0}; i < ids.size(); i++)
    {
        const mantle_api::UniqueId generated_id = unique_id_provider.TryToReserveOrGenerateUniqueId(ids.at(i), "Pole");
        EXPECT_EQ(expected_ids.at(i), generated_id) << fmt::format("At index: {}", i);
    }
}

}  // namespace astas::service::utility
