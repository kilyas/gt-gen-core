/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_CLOCK_H
#define GTGEN_CORE_SERVICE_UTILITY_CLOCK_H

#include <MantleAPI/Common/time_utils.h>
#include <units.h>

#include <cstdint>

namespace astas::service::utility
{
using units::literals::operator""_ms;

/// @brief
class Clock
{
  public:
    static Clock& Instance()
    {
        static Clock clock;
        return clock;
    }

    /// @brief   Get the current value of the Clock. Depending on the operating mode, it is either the system time
    /// (when UseSystemClock() has been called) or the simulation time (when UseSystemTime() has been called).
    /// @return  Current mantle_api::Time
    mantle_api::Time Now();

    /// @brief  Sets the clock to the current time and resets the internal tick counter.
    void SetNow(const mantle_api::Time& now);

    /// @brief  Sets the tick duration, aka the simulation step size. See `step_size_ms` in the SimulationParameters.
    void SetTickDuration(const mantle_api::Time& duration);

    /// @brief  This enables the clock to return a real time. This is i.e. enabled for quemu testing. See
    /// `use_system_clock` user settings.
    void UseSystemClock();

    /// @brief  This is the default use case. Use a simulation time, which is decoupled from the real clock. Starting by
    /// zero the time will be increased in every step (independent from how long the step actually took) by the given
    /// tick duration.
    void UseSimulationClock();

    /// @brief  Tick the clock: Internally increases the tick counter. For system clock usage this has no effect because
    /// the real time is returned. For simulation clock this will lead to an new simulation time based on the last time
    /// and the tick duration.
    void Tick();

    /// @brief  Returns only the full seconds of a given time (e.g. time=10.2s => 10s)
    /// @param time  Input time
    /// @return  Seconds
    std::int64_t Seconds(const mantle_api::Time& time);

    /// @brief  Returns the fraction of a second of a given time in nanoseconds (e.g. time=10.2s => 200,000,000ns)
    /// @param time  Input time
    /// @return  Nanoseconds
    std::uint32_t Nanos(const mantle_api::Time& time);

  private:
    bool use_system_clock_{false};
    std::int64_t tick_counter_{0};
    mantle_api::Time start_time_{40_ms};
    mantle_api::Time tick_duration_{40_ms};
};

}  // namespace astas::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_CLOCK_H
