/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_ORIENTATIONUTILS_H
#define GTGEN_CORE_SERVICE_UTILITY_ORIENTATIONUTILS_H

#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"
#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

namespace astas::service::utility
{

/// @brief Returns the rotated parameter vector
///
///@param vector the vector to be rotated
///@param orientation the rotation in orientation form to apply around each axis
///@return the rotated vector
template <typename T>
mantle_api::Vec3<T> RotateVector(const mantle_api::Vec3<T>& vector,
                                 const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    const auto rotation_matrix{service::glmwrapper::CreateRotationMatrix(orientation)};
    const auto rotated_glm_vector{rotation_matrix * service::glmwrapper::ToGlmVec3(vector)};
    mantle_api::Vec3<T> rotated_mantle_vector;
    rotated_mantle_vector.x = static_cast<T>(rotated_glm_vector[0]);
    rotated_mantle_vector.y = static_cast<T>(rotated_glm_vector[1]);
    rotated_mantle_vector.z = static_cast<T>(rotated_glm_vector[2]);
    return rotated_mantle_vector;
}

/// @brief Returns the rotated parameter vector
///
///@param vector the vector to be rotated
///@param orientation the rotation in orientation form to apply around each axis
///@return the rotated vector
template <typename T>
mantle_api::Vec3<T> RotateVector(const mantle_api::Vec3<T>& vector,
                                 const mantle_api::Orientation3<units::angle::degree_t>& orientation)
{
    const mantle_api::Orientation3<units::angle::radian_t> orientation_radian{
        orientation.yaw, orientation.pitch, orientation.roll};
    return RotateVector(vector, orientation_radian);
}

mantle_api::Orientation3<units::angle::radian_t> CalculateClampedDeltaOrientation(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation_to,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation_from);

}  // namespace astas::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_ORIENTATIONUTILS_H
