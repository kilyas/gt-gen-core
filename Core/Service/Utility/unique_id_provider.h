/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_UNIQUEIDPROVIDER_H
#define GTGEN_CORE_SERVICE_UTILITY_UNIQUEIDPROVIDER_H

#include <MantleAPI/Common/i_identifiable.h>

#include <limits>
#include <set>
#include <string>
#include <vector>

namespace astas::service::utility
{

/// @brief Non thread safe implementation
class UniqueIdProvider
{
  public:
    /// @return The invalid id, which is uint64::max
    static constexpr mantle_api::UniqueId GetInvalidId() { return std::numeric_limits<mantle_api::UniqueId>::max(); };

    /// @brief Forgets all reserved ids and resets the id counter
    void Reset();

    /// @throws RequestedAlreadyReservedId
    void ReserveId(mantle_api::UniqueId id_to_reserve);

    /// @throws RequestedAlreadyReservedId
    void ReserveIds(const std::vector<mantle_api::UniqueId>& ids_to_reserve);

    /// @brief Reserves all ids [from, to]
    /// @throws RequestedAlreadyReservedId
    void ReserveIds(mantle_api::UniqueId from, mantle_api::UniqueId to);

    /// @return The next available unique id
    mantle_api::UniqueId GetUniqueId();

    /// @brief Checks if ID is already reserved
    /// @param id ID to check
    /// @return true if id is already reserved, false otherwise
    bool IsIdReserved(mantle_api::UniqueId id) const;

    /// @brief Tries to reserve the given object_id. If object_id is an already taken ID, a new ID will be generated.
    /// Otherwise, object_id will be returned
    /// @param object_id Object ID
    /// @param object_type Type of the object. Needed for logging purposes
    /// @return object_id or a new unique ID
    mantle_api::UniqueId TryToReserveOrGenerateUniqueId(mantle_api::UniqueId object_id, const std::string& object_type);

  private:
    mantle_api::UniqueId SearchNextUnusedId(mantle_api::UniqueId search_start_id);

    mantle_api::UniqueId last_provided_id_{0};

    /// @brief id 0 is always reserved for the host vehicle
    ///
    //// @attention If you change this, add host vehicle accordingly in ScenarioInstance::ReserveScenarioEntityIds() !
    std::set<mantle_api::UniqueId> reserved_ids_{0};
};

}  // namespace astas::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_UNIQUEIDPROVIDER_H
