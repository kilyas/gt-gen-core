/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/math_utils.h"

namespace astas::service::utility
{

units::angle::radian_t AbsAngle(const units::angle::radian_t alpha, const units::angle::radian_t beta)
{
    const units::angle::radian_t angle{alpha - beta};
    const units::angle::radian_t abs_angle{std::abs(static_cast<double>(angle))};

    return abs_angle;
}

}  // namespace astas::service::utility
