/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_DERIVATIVEUTILS_H
#define GTGEN_CORE_SERVICE_UTILITY_DERIVATIVEUTILS_H

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <cassert>
#include <type_traits>

namespace astas::service::utility
{

/// @brief Calculates the derivative of two values after an elapsed time.
///
///@warning D1 needs to be the next derived unit accross time after D0. Example: If D0 is a length, D1 has to be a
/// speed.
///@param current_value the current value
///@param previous_value the previous value
///@return the derivative over time of the two values
template <typename D0, typename D1>
D1 GetDerivative(D0 current_value, D0 previous_value, units::time::second_t elapsed_time)
{
    static_assert(units::traits::is_unit_t<D0>() && units::traits::is_unit_t<D1>(),
                  "Template parameters must be units");
    static_assert(std::is_same<decltype(D1{} * units::time::second_t{}), D0>::value,
                  "The D1 template parameter has to be the next derived unit accross time after D0");
    if (elapsed_time.value() <= 0.0)
    {
        throw std::logic_error("GetDerivative : elapsed time must be positive");
    }
    return {(current_value - previous_value) / elapsed_time};
}

/// @brief Calculates the derivative of two position-derived vectors after an elapsed time.
///
///@warning D1 needs to be the next derived unit accross time after D0. Example: If D0 is a position, D1 has to be a
/// velocity.
///@param current_value the current value of the position-derived vector
///@param previous_value the previous value of the position-derived vector
///@return the derivative of the position-derived vectors
template <typename D0, typename D1>
mantle_api::Vec3<D1> GetPositionDerivativeVector(const mantle_api::Vec3<D0>& current_value,
                                                 const mantle_api::Vec3<D0>& previous_value,
                                                 units::time::second_t elapsed_time)
{
    mantle_api::Vec3<D1> derivative;
    derivative.x = GetDerivative<D0, D1>(current_value.x, previous_value.x, elapsed_time);
    derivative.y = GetDerivative<D0, D1>(current_value.y, previous_value.y, elapsed_time);
    derivative.z = GetDerivative<D0, D1>(current_value.z, previous_value.z, elapsed_time);
    return derivative;
}

/// @brief Calculates the derivative of two orientation-derived vectors after an elapsed time.
///
///@warning D1 needs to be the next derivated unit accross time after D0. Example: If D0 is an orientation, D1 has to be
/// an orientation rate.
///@param current_value the current value of the orientation-derived vector
///@param previous_value the previous value of the orientation-derived vector
///@return the derivative of the orientation-derived vectors
template <typename D0, typename D1>
mantle_api::Orientation3<D1> GetOrientationDerivativeVector(const mantle_api::Orientation3<D0>& current_value,
                                                            const mantle_api::Orientation3<D0>& previous_value,
                                                            units::time::second_t elapsed_time)
{
    mantle_api::Orientation3<D1> derivative;
    derivative.yaw = GetDerivative<D0, D1>(current_value.yaw, previous_value.yaw, elapsed_time);
    derivative.pitch = GetDerivative<D0, D1>(current_value.pitch, previous_value.pitch, elapsed_time);
    derivative.roll = GetDerivative<D0, D1>(current_value.roll, previous_value.roll, elapsed_time);
    return derivative;
}

}  // namespace astas::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_DERIVATIVEUTILS_H
