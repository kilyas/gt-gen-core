/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Utility/clock.h"

#include <chrono>

namespace astas::service::utility
{
mantle_api::Time Clock::Now()
{
    if (use_system_clock_)
    {
        return std::chrono::system_clock::now().time_since_epoch();
    }

    return start_time_ + tick_counter_ * tick_duration_;
}

void Clock::SetNow(const mantle_api::Time& now)
{
    start_time_ = now;
    tick_counter_ = 0;
}

void Clock::SetTickDuration(const mantle_api::Time& duration)
{
    tick_duration_ = duration;
}

void Clock::UseSystemClock()
{
    use_system_clock_ = true;
}

void Clock::UseSimulationClock()
{
    use_system_clock_ = false;
}

void Clock::Tick()
{
    ++tick_counter_;
}

std::int64_t Clock::Seconds(const mantle_api::Time& time)
{
    return static_cast<std::int64_t>(time) / 1000;
}

std::uint32_t Clock::Nanos(const mantle_api::Time& time)
{
    return static_cast<std::uint32_t>((static_cast<std::int64_t>(time) % 1000) * 1000000);
}

}  // namespace astas::service::utility
