/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_UTILITY_STRINGUTILS_H
#define GTGEN_CORE_SERVICE_UTILITY_STRINGUTILS_H

#include <sstream>
#include <string>
#include <vector>

namespace astas::service::utility
{
/// @brief Trims the string on both ends: Removes all characters contained in the matcher string
void Trim(std::string& s, const std::string& matcher = std::string(" "));

/// @brief Splits up a string into a vector. By default the string is split by coma and empty parts are dismissed
std::vector<std::string> Split(const std::string& s, char delimiter = ',', bool ignore_empty_elements = true);

/// @brief Checks if the given string ends with the given ending.
bool EndsWith(std::string s, std::string ending, bool ignore_case = false);

/// @brief Checks in the given string represents a double value.
bool IsDouble(const std::string& input);

/// @brief Transforms the input string to only contain lower-case characters.
std::string ToLower(const std::string& input);

/// @brief Removes all whitespaces in the given string.
void RemoveAllWhitespaces(std::string& string_to_modify);

/// @brief Turn a string of `"{ a, b,c }"` to an std::vector of `["a", "b", "c"]`.
std::vector<std::string> StringListToVector(std::string s);

/// @brief Turn a std::vector of `["a", "b", "c"]` to a string separated with a delimiter which is by default a comma
/// `"a,b,c"`.
template <typename T>
std::string VectorToString(const std::vector<T>& input, char delimiter = ',')
{
    std::ostringstream output;
    if (!input.empty())
    {
        output << input[0];
        for (size_t i = 1; i < input.size(); ++i)
        {
            output << delimiter << input[i];
        }
    }
    return output.str();
}

}  // namespace astas::service::utility

#endif  // GTGEN_CORE_SERVICE_UTILITY_STRINGUTILS_H
