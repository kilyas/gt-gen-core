/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/clock.h"

#include "Core/Service/Logging/logging.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <units.h>

#include <chrono>
#include <string>

namespace astas::service::utility
{
using units::literals::operator""_ms;
using units::literals::operator""_s;

class ClockTest : public testing::Test
{
  public:
    void SetUp() override
    {
        Clock::Instance().SetNow(0_ms);
        Clock::Instance().UseSimulationClock();
    }
};

TEST_F(ClockTest, GivenSimulationClockIsInitialized_WhenNow_ThenNowIs0)
{
    EXPECT_EQ(0_ms, Clock::Instance().Now());
}

TEST_F(ClockTest, GivenSystemClockIsInitialized_WhenNow_ThenNowIsReasonable)
{
    Clock::Instance().UseSystemClock();
    auto now_seconds = units::time::second_t{std::chrono::system_clock::now().time_since_epoch()};
    EXPECT_NE(units::time::second_t{0}, Clock::Instance().Now());
    EXPECT_LE(now_seconds, Clock::Instance().Now());
}

TEST_F(ClockTest, GivenSimulationClock_WhenSet_ThenNowTimeIsEqualSetTime)
{
    Clock::Instance().SetNow(1234_ms);
    EXPECT_EQ(1234_ms, Clock::Instance().Now());
}

TEST_F(ClockTest, GivenSystemClock_WhenSet_ThenNowTimeIsNotSetTime)
{
    Clock::Instance().UseSystemClock();
    Clock::Instance().SetNow(1234_ms);
    EXPECT_NE(1234_ms, Clock::Instance().Now());
}

TEST_F(ClockTest, GivenSimulationClock_WhenTick_ThenNowTimeIsOldTimeIncreasedByTickTime)
{
    auto tick_duration = 40_ms;
    Clock::Instance().SetTickDuration(tick_duration);
    auto old = Clock::Instance().Now();

    Clock::Instance().Tick();
    auto now = Clock::Instance().Now();

    EXPECT_EQ(old + tick_duration, now);
}

TEST_F(ClockTest, GivenSimulationClock_WhenReset_ThenNowTimeIsResetTime)
{
    Clock::Instance().Tick();
    Clock::Instance().Tick();

    auto expected_time = 40_ms;
    Clock::Instance().SetNow(expected_time);

    EXPECT_EQ(expected_time, Clock::Instance().Now());
    Clock::Instance().Tick();
    EXPECT_EQ(expected_time + expected_time, Clock::Instance().Now());
}

TEST_F(ClockTest, GivenSimulationClock_WhenLog_ThenFormattedCorrectly)
{
    testing::internal::CaptureStdout();
    Info("Test");
    std::string std_out = testing::internal::GetCapturedStdout();
    EXPECT_THAT(std_out, testing::HasSubstr("[0.000s]"));

    testing::internal::CaptureStdout();
    Clock::Instance().SetNow(1234_ms);
    Info("Test");
    std_out = testing::internal::GetCapturedStdout();
    EXPECT_THAT(std_out, testing::HasSubstr("[1.234s]"));
}

TEST_F(ClockTest, GivenSystemClock_WhenLog_ThenFormattedCorrectly)
{
    Clock::Instance().UseSystemClock();
    testing::internal::CaptureStdout();
    Info("Test");
    std::string std_out = testing::internal::GetCapturedStdout();
    EXPECT_THAT(std_out, Not(testing::HasSubstr("[0.000s]")));
}

TEST_F(ClockTest, GivenClock_WhenCallSeconds_ThenReturnSeconds)
{
    EXPECT_EQ(10, Clock::Instance().Seconds(10.7_s));
}

TEST_F(ClockTest, GivenClock_WhenCallNanos_ThenReturnNanos)
{
    EXPECT_EQ(753000000, Clock::Instance().Nanos(3.753_s));
}
}  // namespace astas::service::utility
