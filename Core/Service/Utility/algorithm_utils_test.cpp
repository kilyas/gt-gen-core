/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Utility/algorithm_utils.h"

#include <MantleAPI/Test/test_utils.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <optional>
#include <utility>

namespace astas::service::utility
{

TEST(AdjacentFindAndEvaluateTest,
     GivenVectorAndValueToFindContained_WhenAdjacentFindAndEvaluate_ThenReturnedIteratorPointsToCorrectVectorEntry)
{
    std::vector<int> v{0, 1, 2, 3, 4, 5, 6, 7};
    const double value_to_find{3.3};
    const auto& [iter, dist] =
        AdjacentFindAndEvaluate(v.begin(), v.end(), [&value_to_find](int a, int b) -> std::optional<double> {
            if (a <= value_to_find && value_to_find < b)
            {
                return std::optional<double>(static_cast<double>(value_to_find) - static_cast<double>(a));
            }
            else
            {
                return std::nullopt;
            }
        });

    EXPECT_EQ(3, std::distance(v.begin(), iter));
    EXPECT_EQ(3, *iter);
    EXPECT_DOUBLE_EQ(0.3, dist);
}

TEST(AdjacentFindAndEvaluateTest,
     GivenVectorAndValueToFindNotContained_WhenAdjacentFindAndEvaluate_ThenReturnedIteratorPointsToEnd)
{
    std::vector<int> v{0, 1, 2, 3, 4, 5, 6, 7};
    const int value_to_find{10};
    const auto& [iter, value] =
        AdjacentFindAndEvaluate(v.begin(), v.end(), [&value_to_find](int a, int b) -> std::optional<int> {
            if (a <= value_to_find && value_to_find < b)
            {
                return std::optional<int>(value_to_find);
            }
            else
            {
                return std::nullopt;
            }
        });

    EXPECT_EQ(v.end(), iter);
    EXPECT_EQ(0, value);
}

TEST(AdjacentFindAndEvaluateTest, GivenEmptyVector_WhenAdjacentFindAndEvaluate_ThenReturnedIteratorPointsToEnd)
{
    std::vector<int> v{};
    const auto& [iter, value] = AdjacentFindAndEvaluate(
        v.begin(), v.end(), [](int, int) -> std::optional<int> { return std::optional<int>(1); });

    EXPECT_EQ(v.end(), iter);
    EXPECT_EQ(0, value);
}

TEST(AdjacentFindAndEvaluateTest, GivenVectorWithOneElement_WhenAdjacentFindAndEvaluate_ThenReturnedIteratorPointsToEnd)
{
    std::vector<int> v{1};
    const auto& [iter, value] = AdjacentFindAndEvaluate(
        v.begin(), v.end(), [](int, int) -> std::optional<int> { return std::optional<int>(1); });

    EXPECT_EQ(v.end(), iter);
    EXPECT_EQ(0, value);
}

TEST(ForAdjacentEachTest, GivenVector_WhenForAdjacentEach_ThenEachPossibleAdjacentElementPairsReturned)
{
    std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8};
    std::vector<std::pair<int, int>> p;

    ForAdjacentEach(v.begin(), v.end(), [&p](int a, int b) { p.emplace_back(a, b); });

    ASSERT_EQ(7, p.size());
    EXPECT_EQ(std::make_pair(1, 2), p[0]);
    EXPECT_EQ(std::make_pair(2, 3), p[1]);
    EXPECT_EQ(std::make_pair(3, 4), p[2]);
    EXPECT_EQ(std::make_pair(4, 5), p[3]);
    EXPECT_EQ(std::make_pair(5, 6), p[4]);
    EXPECT_EQ(std::make_pair(6, 7), p[5]);
    EXPECT_EQ(std::make_pair(7, 8), p[6]);
}

TEST(ForAdjacentEachTest, GivenEmptyVector_WhenForAdjacentEach_ThenNothingReturned)
{
    std::vector<int> v{};
    std::vector<std::pair<int, int>> p;

    ForAdjacentEach(v.begin(), v.end(), [&p](int a, int b) { p.emplace_back(a, b); });

    ASSERT_EQ(0, p.size());
}

TEST(ForAdjacentEachTest, GivenVectorWithTwoElements_WhenForAdjacentEachCalledTwice_ThenBothPairsReturned)
{
    std::vector<int> v{1, 2};
    std::vector<std::pair<int, int>> p;

    auto fun{ForAdjacentEach(v.begin(), v.end(), [&p](int a, int b) { p.emplace_back(a, b); })};

    ASSERT_EQ(1, p.size());
    EXPECT_EQ(std::make_pair(1, 2), p[0]);

    fun(20, 30);
    ASSERT_EQ(2, p.size());
    EXPECT_EQ(std::make_pair(1, 2), p[0]);
    EXPECT_EQ(std::make_pair(20, 30), p[1]);
}

TEST(ContainsIf, GivenInputVectorWithSearchValueContained_WhenContainsIf_ThenReturnTrue)
{
    std::vector<int> input_vector{1, 2, 3, 4};
    constexpr int search_value = 3;

    const auto& find_condition = [](const int& element) { return element == search_value; };

    EXPECT_TRUE(ContainsIf(input_vector, find_condition));
}

TEST(ContainsIf, GivenInputVectorWithoutSearchValue_WhenContainsIf_ThenReturnFalse)
{
    std::vector<int> input_vector{1, 2, 3, 4};
    constexpr int search_value = 42;

    const auto& find_condition = [](const int& element) { return element == search_value; };

    EXPECT_FALSE(ContainsIf(input_vector, find_condition));
}

TEST(Contains, GivenInputVectorWithSearchValue_WhenContains_ThenReturnTrue)
{
    std::vector<int> input_vector{1, 2, 3, 4};
    constexpr int search_value = 3;

    EXPECT_TRUE(Contains(input_vector, search_value));
}

TEST(Contains, GivenInputVectorWithoutSearchValue_WhenContains_ThenReturnFalse)
{
    std::vector<int> input_vector{1, 2, 3, 4};
    constexpr int search_value = 42;

    EXPECT_FALSE(Contains(input_vector, search_value));
}

TEST(SortAndRemoveDuplicates, GivenSortedVectorWithoutDuplicates_WhenApplying_ThenVectorDoesNotChange)
{
    std::vector<int> expected_vector{1, 2, 3, 4};
    std::vector<int> input_vector{1, 2, 3, 4};

    SortAndRemoveDuplicates(input_vector);

    EXPECT_EQ(expected_vector, input_vector);
}

TEST(SortAndRemoveDuplicates, GivenUnsortedVectorWithoutDuplicates_WhenApplying_ThenVectorIsSortedButNothingIsRemoved)
{
    std::vector<int> expected_vector{1, 2, 3, 4};
    std::vector<int> input_vector{2, 1, 4, 3};

    SortAndRemoveDuplicates(input_vector);

    EXPECT_EQ(expected_vector, input_vector);
}

TEST(SortAndRemoveDuplicates, GivenVectorWithDuplicates_WhenApplying_ThenVectorIsSortedAndAllDuplicatesAreRemoved)
{
    std::vector<int> expected_vector{1, 2, 3, 4};
    std::vector<int> input_vector{1, 1, 2, 3, 3, 4, 1};

    SortAndRemoveDuplicates(input_vector);

    EXPECT_EQ(expected_vector, input_vector);
}

TEST(RemoveAdjacentDuplicates, GivenUnsortedVectorWithoutDuplicates_WhenApplying_ThenVectorDoesNotChange)
{
    std::vector<int> expected_vector{2, 1, 3, 4};
    std::vector<int> input_vector{2, 1, 3, 4};

    RemoveAdjacentDuplicates(input_vector);

    EXPECT_EQ(expected_vector, input_vector);
}

TEST(RemoveAdjacentDuplicates, GivenUnsortedVectorWithAdjacentDuplicates_WhenApplying_ThenAdjacentDuplicatesAreRemoved)
{
    std::vector<int> expected_vector{1, 2, 3, 4, 1};
    std::vector<int> input_vector{1, 1, 2, 3, 3, 3, 4, 1};

    RemoveAdjacentDuplicates(input_vector);

    EXPECT_EQ(expected_vector, input_vector);
}

TEST(FindObjectByIdTest, GivenEmptyEntites_WhenFindObjectById_ThenNoEntityReturned)
{
    std::vector<std::unique_ptr<mantle_api::IEntity>> entities{};

    auto found = FindObjectById(entities, 1);
    EXPECT_FALSE(found != entities.end());
}

TEST(FindObjectByIdTest, GivenEntites_WhenFindObjectByNonExistentId_ThenNoEntityReturned)
{
    std::vector<std::unique_ptr<mantle_api::IEntity>> entities{};
    auto mock_vehicle = std::make_unique<mantle_api::MockVehicle>();
    auto mock_pedestrian = std::make_unique<mantle_api::MockPedestrian>();
    EXPECT_CALL(*mock_vehicle, GetUniqueId()).WillRepeatedly(testing::Return(0));
    EXPECT_CALL(*mock_pedestrian, GetUniqueId()).WillRepeatedly(testing::Return(1));

    entities.push_back(std::move(mock_vehicle));
    entities.push_back(std::move(mock_pedestrian));

    auto found = FindObjectById(entities, 2);
    EXPECT_FALSE(found != entities.end());
}

TEST(FindObjectByIdTest, GivenEntites_WhenFindObjectByExistentId_ThenExpectedEntityIsReturned)
{
    std::vector<std::unique_ptr<mantle_api::IEntity>> entities{};
    auto mock_vehicle = std::make_unique<mantle_api::MockVehicle>();
    auto mock_pedestrian = std::make_unique<mantle_api::MockPedestrian>();
    mock_vehicle->SetName("host");
    mock_pedestrian->SetName("pedestrian1");
    EXPECT_CALL(*mock_vehicle, GetUniqueId()).WillRepeatedly(testing::Return(0));
    EXPECT_CALL(*mock_pedestrian, GetUniqueId()).WillRepeatedly(testing::Return(1));

    entities.push_back(std::move(mock_vehicle));
    entities.push_back(std::move(mock_pedestrian));

    auto found = FindObjectById(entities, 1);
    ASSERT_TRUE(found != entities.end());

    EXPECT_EQ(found->get()->GetName(), "pedestrian1");
}

}  // namespace astas::service::utility
