/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/GlmWrapper/glm_basic_scalar_utils.h"

#include <gtest/gtest.h>

namespace astas::service::glmwrapper
{

TEST(RadiansTest, GivenDegrees_WhenRadians_ThenRadianValueReturned)
{
    EXPECT_DOUBLE_EQ(0.78539816339744828, Radians(45.0));
}

TEST(ClampTest, GivenValueBelowMin_WhenClamp_ThenClampedValueIsMin)
{
    double min = 0;
    double max = 5;

    EXPECT_EQ(min, Clamp(-6, min, max));
}

TEST(ClampTest, GivenValueAboveMax_WhenClamp_ThenClampedValueIsMax)
{
    double min = 0;
    double max = 5;

    EXPECT_EQ(max, Clamp(6, min, max));
}

TEST(ClampTest, GivenValueInRange_WhenClamp_ThenClampedDoesNotChange)
{
    double min = 0;
    double max = 5;

    EXPECT_EQ(3, Clamp(3, min, max));
}

TEST(MixTest, GivenDoubles_WhenComputingMix_ThenMixIsCorrect)
{
    // See: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/mix.xhtml
    // internally computes: pt0×(1−d)+pt1×d
    double expected_mix_double{11};

    double a = 3;
    double b = 5;
    double d = 4;

    EXPECT_EQ(expected_mix_double, Mix(a, b, d));
}

}  // namespace astas::service::glmwrapper
