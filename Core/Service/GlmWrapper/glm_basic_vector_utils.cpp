/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"

#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/norm.hpp>

namespace astas::service::glmwrapper
{

double Distance(const mantle_api::Vec3<units::length::meter_t>& pos1,
                const mantle_api::Vec3<units::length::meter_t>& pos2)
{
    glm::dvec3 glm_pos1 = ToGlmVec3(pos1);
    glm::dvec3 glm_pos2 = ToGlmVec3(pos2);
    return glm::distance(glm_pos1, glm_pos2);
}

double Distance2(const mantle_api::Vec3<units::length::meter_t>& pos1,
                 const mantle_api::Vec3<units::length::meter_t>& pos2)
{
    glm::dvec3 glm_pos1 = ToGlmVec3(pos1);
    glm::dvec3 glm_pos2 = ToGlmVec3(pos2);
    return glm::distance2(glm_pos1, glm_pos2);
}

double Length(const mantle_api::Vec3<units::length::meter_t>& vec)
{
    glm::dvec3 glm_vec = ToGlmVec3(vec);
    return glm::length(glm_vec);
}

double Length2(const mantle_api::Vec3<units::length::meter_t>& vec)
{
    glm::dvec3 glm_vec = ToGlmVec3(vec);
    return glm::length2(glm_vec);
}

mantle_api::Vec3<units::length::meter_t> Normalize(const mantle_api::Vec3<units::length::meter_t>& vec)
{
    glm::dvec3 glm_vec = ToGlmVec3(vec);
    auto normalized = glm::normalize(glm_vec);
    return ToMantleVec3Length(normalized);
}

mantle_api::Vec3<units::length::meter_t> Cross(const mantle_api::Vec3<units::length::meter_t>& vec1,
                                               const mantle_api::Vec3<units::length::meter_t>& vec2)
{
    glm::dvec3 glm_vec1 = ToGlmVec3(vec1);
    glm::dvec3 glm_vec2 = ToGlmVec3(vec2);

    return ToMantleVec3Length(glm::cross(glm_vec1, glm_vec2));
}

double Dot(const mantle_api::Vec3<units::length::meter_t>& pt0, const mantle_api::Vec3<units::length::meter_t>& pt1)
{
    glm::dvec3 glm_pt0 = ToGlmVec3(pt0);
    glm::dvec3 glm_pt1 = ToGlmVec3(pt1);
    return glm::dot(glm_pt0, glm_pt1);
}

mantle_api::Vec3<units::length::meter_t> Normal(const mantle_api::Vec3<units::length::meter_t>& pt0,
                                                const mantle_api::Vec3<units::length::meter_t>& pt1,
                                                const mantle_api::Vec3<units::length::meter_t>& direction)
{
    glm::dvec3 glm_pt0 = ToGlmVec3(pt0);
    glm::dvec3 glm_pt1 = ToGlmVec3(pt1);
    glm::dvec3 glm_direction = ToGlmVec3(direction);

    glm::dvec3 line = glm_pt0 - glm_pt1;
    glm::dvec3 normal = glm::normalize(glm::cross(line, glm_direction));

    return ToMantleVec3Length(normal);
}

mantle_api::Vec3<units::length::meter_t> Mix(const mantle_api::Vec3<units::length::meter_t>& pt0,
                                             const mantle_api::Vec3<units::length::meter_t>& pt1,
                                             double d)
{
    glm::dvec3 glm_pt0 = ToGlmVec3(pt0);
    glm::dvec3 glm_pt1 = ToGlmVec3(pt1);
    return ToMantleVec3Length(glm::mix(glm_pt0, glm_pt1, d));
}

}  // namespace astas::service::glmwrapper
