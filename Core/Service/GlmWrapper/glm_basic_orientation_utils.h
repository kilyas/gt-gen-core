/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICORIENTATIONUTILS_H
#define GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICORIENTATIONUTILS_H

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <glm/ext/quaternion_double.hpp>
#include <glm/vec3.hpp>

#include <vector>

namespace astas::service::glmwrapper
{
glm::dmat3 CreateRotationMatrix(const mantle_api::Orientation3<units::angle::degree_t>& orientation);
glm::dmat3 CreateRotationMatrix(const mantle_api::Orientation3<units::angle::radian_t>& orientation);

glm::dmat3 CreateRotationMatrix(const glm::dvec3& forward, const glm::dvec3& up);

/**@brief Get a quaternion from these euler angles where:
 * roll Angle around X
 * pitch Angle around Y
 * yaw Angle around Z
 * See: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles*/
glm::dquat ToQuaternion(const mantle_api::Orientation3<units::angle::radian_t>& orientation);

/**@brief Get euler angles from a quaternion where:
 * roll Angle around X
 * pitch Angle around Y
 * yaw Angle around Z*/
mantle_api::Orientation3<units::angle::radian_t> FromQuaternion(const glm::dquat& quat);

mantle_api::Orientation3<units::angle::radian_t> FromBasisVectors(
    const mantle_api::Vec3<units::length::meter_t>& forward,
    const mantle_api::Vec3<units::length::meter_t>& up);

mantle_api::Orientation3<units::angle::radian_t> FromRotationMatrix(const glm::dmat3& mat);

/// @brief Returns the world space left vector where the local coordinate system is based on the standard for
/// vehicle dynamics and handling DIN ISO 8855. Rotations are applied according to Tait-Bryan angles using the
/// Z-Y'-X" convention.
///
/// References:
/// - https://de.wikipedia.org/wiki/Fahrzeugkoordinatensystem
/// - https://en.wikipedia.org/wiki/Axes_conventions#Conventions_for_land_vehicles
/// - https://en.wikipedia.org/wiki/Euler_angles#Tait%E2%80%93Bryan_angles
mantle_api::Vec3<units::length::meter_t> GetWorldSpaceLeftVector(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation);

/// @brief Returns the world space forward vector where the local coordinate system is based on the standard for
/// vehicle dynamics and handling DIN ISO 8855. Rotations are applied according to Tait-Bryan angles using the
/// Z-Y'-X" convention.
///
/// References:
/// - https://de.wikipedia.org/wiki/Fahrzeugkoordinatensystem
/// - https://en.wikipedia.org/wiki/Axes_conventions#Conventions_for_land_vehicles
/// - https://en.wikipedia.org/wiki/Euler_angles#Tait%E2%80%93Bryan_angles
mantle_api::Vec3<units::length::meter_t> GetWorldSpaceForwardVector(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation);

/// @brief Returns the world space up vector where the local coordinate system is based on the standard for
/// vehicle dynamics and handling DIN ISO 8855. Rotations are applied according to Tait-Bryan angles using the
/// Z-Y'-X" convention.
///
/// References:
/// - https://de.wikipedia.org/wiki/Fahrzeugkoordinatensystem
/// - https://en.wikipedia.org/wiki/Axes_conventions#Conventions_for_land_vehicles
/// - https://en.wikipedia.org/wiki/Euler_angles#Tait%E2%80%93Bryan_angles
mantle_api::Vec3<units::length::meter_t> GetWorldSpaceUpVector(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation);

}  // namespace astas::service::glmwrapper

#endif  // GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICORIENTATIONUTILS_H
