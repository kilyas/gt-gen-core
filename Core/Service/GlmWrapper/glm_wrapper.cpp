/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/norm.hpp>

namespace astas::service::glmwrapper
{

glm::dmat4 GetLocalToWorldSpaceMatrix(const mantle_api::Vec3<units::length::meter_t>& position,
                                      const mantle_api::Orientation3<units::angle::radian_t>& orientation)
{
    glm::dvec3 glm_position{position.x(), position.y(), position.z()};
    return glm::translate(glm::dmat4(1.0F), glm_position) * glm::mat4_cast(ToQuaternion(orientation));
}

mantle_api::Vec3<units::length::meter_t> ToGlobalSpace(
    const mantle_api::Vec3<units::length::meter_t>& position,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation,
    const mantle_api::Vec3<units::length::meter_t>& local_position)
{
    auto transformation_matrix = glmwrapper::GetLocalToWorldSpaceMatrix(position, orientation);
    auto global_position =
        transformation_matrix * glm::dvec4{local_position.x(), local_position.y(), local_position.z(), 1.0};

    return ToMantleVec3Length(global_position);
}

std::vector<mantle_api::Vec3<units::length::meter_t>> TransformPolylinePointsFromWorldToLocal(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline_points,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> local_points;
    local_points.reserve(polyline_points.size());

    const auto transform_matrix = glm::inverse(GetLocalToWorldSpaceMatrix(local_origin, local_orientation));

    for (const auto& point : polyline_points)
    {
        auto position4d = glm::dvec4{point.x(), point.y(), point.z, 1.0};
        position4d = transform_matrix * position4d;
        local_points.emplace_back(mantle_api::Vec3<units::length::meter_t>{units::length::meter_t(position4d.x),
                                                                           units::length::meter_t(position4d.y),
                                                                           units::length::meter_t(position4d.z)});
    }
    return local_points;
}

mantle_api::Vec3<units::length::meter_t> TransformPositionFromWorldToLocal(
    const mantle_api::Vec3<units::length::meter_t>& world_position,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation)
{
    const auto converted_point =
        TransformPolylinePointsFromWorldToLocal({world_position}, local_origin, local_orientation);
    return converted_point.at(0);
}

// TODO: why is shape extreme max (aka second) not used???
mantle_api::Vec3<units::length::meter_t> CalculateObjectPosition(
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>& shape_extremes,
    const mantle_api::Vec3<units::length::meter_t>& local_position,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation,
    const mantle_api::Dimension3& dimensions)
{
    const mantle_api::Vec3<units::length::meter_t> local_centerpoint{shape_extremes.first.x + dimensions.length / 2,
                                                                     shape_extremes.first.y + dimensions.width / 2,
                                                                     shape_extremes.first.z + dimensions.height / 2};

    const auto world_centerpoint = GetLocalToWorldSpaceMatrix(local_position, local_orientation) *
                                   glm::dvec4{local_centerpoint.x, local_centerpoint.y, local_centerpoint.z, 1.0};

    return service::glmwrapper::ToMantleVec3Length(world_centerpoint);
}

mantle_api::Vec3<units::length::meter_t> GetPositionOnLineSegment(
    const mantle_api::Vec3<units::length::meter_t>& line_start,
    const mantle_api::Vec3<units::length::meter_t>& line_end,
    double distance_from_line_start)
{
    glm::dvec3 glm_line_start = ToGlmVec3(line_start);
    glm::dvec3 glm_line_end = ToGlmVec3(line_end);

    auto forward = glm::normalize(glm_line_end - glm_line_start);
    auto position_on_segment = forward * distance_from_line_start;
    return ToMantleVec3Length(position_on_segment);
}

}  // namespace astas::service::glmwrapper
