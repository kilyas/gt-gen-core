/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GLMWRAPPER_GLMWRAPPER_H
#define GTGEN_CORE_SERVICE_GLMWRAPPER_GLMWRAPPER_H

#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"
#include "Core/Service/GlmWrapper/glm_basic_scalar_utils.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <glm/ext/quaternion_double.hpp>
#include <glm/vec3.hpp>

#include <vector>

namespace astas::service::glmwrapper
{

glm::dmat4 GetLocalToWorldSpaceMatrix(const mantle_api::Vec3<units::length::meter_t>& position,
                                      const mantle_api::Orientation3<units::angle::radian_t>& orientation);

mantle_api::Vec3<units::length::meter_t> ToGlobalSpace(
    const mantle_api::Vec3<units::length::meter_t>& position,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation,
    const mantle_api::Vec3<units::length::meter_t>& local_position);

/// @brief Transforms world polyline positions to local.
/// @param polyline_points world polyline points to be transformed
/// @param transform_matrix transformation matrix
/// @return converted polyline points
std::vector<mantle_api::Vec3<units::length::meter_t>> TransformPolylinePointsFromWorldToLocal(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline_points,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation);

/// @brief Transforms world position to local coordinate system.
/// @param world_position world point position to be transformed
/// @param local_origin  local coordinate system origin
/// @param local_orientation  local system orientation
/// @return transformed point
mantle_api::Vec3<units::length::meter_t> TransformPositionFromWorldToLocal(
    const mantle_api::Vec3<units::length::meter_t>& world_position,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation);

/// @brief calculates object dimensions from given shape extremes
/// @param shape_extremes extremes of a points that outline an object
/// @return position of a object
mantle_api::Vec3<units::length::meter_t> CalculateObjectPosition(
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>& shape_extremes,
    const mantle_api::Vec3<units::length::meter_t>& local_position,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation,
    const mantle_api::Dimension3& dimensions);

mantle_api::Vec3<units::length::meter_t> GetPositionOnLineSegment(
    const mantle_api::Vec3<units::length::meter_t>& line_start,
    const mantle_api::Vec3<units::length::meter_t>& line_end,
    double distance_from_line_start);

}  // namespace astas::service::glmwrapper

#endif  // GTGEN_CORE_SERVICE_GLMWRAPPER_GLMWRAPPER_H
