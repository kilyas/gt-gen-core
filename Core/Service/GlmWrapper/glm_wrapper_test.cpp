/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <glm/gtx/string_cast.hpp>
#include <gtest/gtest.h>

namespace astas::service::glmwrapper
{
using units::literals::operator""_rad;
using units::literals::operator""_m;

constexpr double epsilon = 1.0e-3;

TEST(GetLocalToWorldSpaceMatrix,
     GivenPositionAndOrientation_WhenGetLocalToWorldSpaceMatrix_ThenResultWorldMatrixIsExpectedMatrix)
{
    const mantle_api::Vec3<units::length::meter_t>& position{1.0_m, 2.0_m, 3.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> orientation{2.0_rad, 3.0_rad, 1.0_rad};
    const glm::dmat4 expected_world_mat{0.411982,
                                        -0.900198,
                                        -0.141120,
                                        0.000000,
                                        -0.540712,
                                        -0.116867,
                                        -0.833050,
                                        0.000000,
                                        0.733417,
                                        0.419507,
                                        -0.534895,
                                        0.000000,
                                        1.000000,
                                        2.000000,
                                        3.000000,
                                        1.000000};

    const glm::dmat4 local_to_world_mat = GetLocalToWorldSpaceMatrix(position, orientation);

    std::cout << "Result dmat4:" << std::endl;
    std::cout << glm::to_string(local_to_world_mat) << std::endl;
    std::cout << "Expected dmat4:" << std::endl;
    std::cout << glm::to_string(expected_world_mat) << std::endl;

    for (int i = 0; i < glm::dmat4::length(); ++i)
    {
        EXPECT_NEAR(expected_world_mat[i].x, local_to_world_mat[i].x, epsilon);
        EXPECT_NEAR(expected_world_mat[i].y, local_to_world_mat[i].y, epsilon);
        EXPECT_NEAR(expected_world_mat[i].z, local_to_world_mat[i].z, epsilon);
        EXPECT_NEAR(expected_world_mat[i].w, local_to_world_mat[i].w, epsilon);
    }
}

TEST(GetPositionOnLineSegmentTest, GivenTwoPointsDefiningLineSegment_WhenComputingPointForDistance_ThenPointIsCorrect)
{
    mantle_api::Vec3<units::length::meter_t> pos1{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> pos2{0.0_m, 10.0_m, 0.0_m};
    double distance{2.34};
    mantle_api::Vec3<units::length::meter_t> expected_point{0.0_m, 2.34_m, 0.0_m};

    EXPECT_EQ(expected_point, GetPositionOnLineSegment(pos1, pos2, distance));
}

TEST(CalculateObjectPositionTest,
     GivenShapeExtremesAndLocalPoseAndDimension_WhenCalculatingObjectPosition_ThenObjectPositionIsCalculated)
{
    std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>> shape_extremes{
        {100_m, 100_m, 1_m}, {200_m, 200_m, 1_m}};
    mantle_api::Vec3<units::length::meter_t> local_position{1_m, 0.5_m, 0.0_m};
    mantle_api::Orientation3<units::angle::radian_t> local_orientation{units::angle::radian_t(M_PI_2), 0_rad, 0_rad};
    mantle_api::Dimension3 dimensions{4_m, 2_m, 2_m};

    mantle_api::Vec3<units::length::meter_t> expected_obj_position{-100.0_m, 102.5_m, 2.0_m};

    EXPECT_TRIPLE(expected_obj_position,
                  CalculateObjectPosition(shape_extremes, local_position, local_orientation, dimensions));
}

TEST(ToGlobalSpaceTest, GivenPositionOrientationAndLocalPoint_WhenToGlobalSpace_ThenGlobalVec3dReturned)
{
    mantle_api::Vec3<units::length::meter_t> position{1_m, 1_m, 0_m};
    mantle_api::Orientation3<units::angle::radian_t> orientation{units::angle::radian_t(M_PI_2), 0_rad, 0_rad};
    mantle_api::Vec3<units::length::meter_t> local_position{1_m, 0.5_m, 0.0_m};

    mantle_api::Vec3<units::length::meter_t> expected_global_position{0.5_m, 2_m, 0_m};
    EXPECT_TRIPLE(expected_global_position, ToGlobalSpace(position, orientation, local_position));
}

TEST(TransformPolylinePointsFromWorldToLocalTest,
     GivenDiagonalPolylineWithThreePoints_WhenCalculatingLocalPolylinePoints_ThenLocalPolylinePointsAreCorrect)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline_points{
        {100_m, 100_m, 1_m}, {150_m, 150_m, 1_m}, {200_m, 200_m, 1_m}};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_local_polyline_points{
        {141.421356236_m, -0.000635009_m, 0_m}, {70.710678118_m, -0.000317505_m, 0_m}, {-0_m, -0_m, 0_m}};

    const mantle_api::Orientation3<units::angle::radian_t> orientation{-2.35619_rad, 0_rad, 0_rad};

    const auto actual_local_polyline_points =
        TransformPolylinePointsFromWorldToLocal(polyline_points, polyline_points.back(), orientation);

    ASSERT_EQ(actual_local_polyline_points.size(), expected_local_polyline_points.size());
    for (std::size_t i{0}; i < expected_local_polyline_points.size(); ++i)
    {
        EXPECT_TRIPLE(expected_local_polyline_points[i], actual_local_polyline_points[i]);
    }
}

TEST(TransformPointFromWorldToLocalTest, GivenPoint_WhenCalculatingTransformPointToLocal_ThenLocalPositionIsCorrect)
{
    const mantle_api::Vec3<units::length::meter_t> global_position{150_m, 150_m, 1_m};
    const mantle_api::Vec3<units::length::meter_t> expected_local_position{70.710678118_m, -0.000317505_m, 0_m};

    const mantle_api::Vec3<units::length::meter_t> local_orign{200_m, 200_m, 1_m};
    const mantle_api::Orientation3<units::angle::radian_t> local_orientation{-2.35619_rad, 0_rad, 0_rad};

    const auto actual_local_point = TransformPositionFromWorldToLocal(global_position, local_orign, local_orientation);

    EXPECT_TRIPLE(expected_local_position, actual_local_point);
}

}  // namespace astas::service::glmwrapper
