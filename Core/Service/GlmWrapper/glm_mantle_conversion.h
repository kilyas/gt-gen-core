/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GLMWRAPPER_GLMMANTLECONVERSION_H
#define GTGEN_CORE_SERVICE_GLMWRAPPER_GLMMANTLECONVERSION_H

#include <MantleAPI/Common/vector.h>
#include <glm/glm.hpp>

#include <algorithm>
#include <vector>

namespace astas::service::glmwrapper
{

template <typename T>
inline glm::dvec3 ToGlmVec3(const mantle_api::Vec3<T>& vec)
{
    return {vec.x(), vec.y(), vec.z()};
}

template <typename T>
inline std::vector<glm::dvec3> ToVectorGlmVec3(const std::vector<mantle_api::Vec3<T>>& vector)
{
    std::vector<glm::dvec3> glm_vector;
    glm_vector.reserve(vector.size());
    std::transform(vector.cbegin(), vector.cend(), std::back_inserter(glm_vector), [](const mantle_api::Vec3<T>& vec3) {
        return ToGlmVec3(vec3);
    });
    return glm_vector;
}

inline mantle_api::Vec3<units::length::meter_t> ToMantleVec3Length(const glm::dvec3& glm_vec)
{
    return mantle_api::Vec3<units::length::meter_t>{
        units::length::meter_t(glm_vec.x), units::length::meter_t(glm_vec.y), units::length::meter_t(glm_vec.z)};
}

inline mantle_api::Vec3<units::velocity::meters_per_second_t> ToMantleVec3Velocity(const glm::dvec3& glm_vec)
{
    return mantle_api::Vec3<units::velocity::meters_per_second_t>{units::velocity::meters_per_second_t(glm_vec.x),
                                                                  units::velocity::meters_per_second_t(glm_vec.y),
                                                                  units::velocity::meters_per_second_t(glm_vec.z)};
}

inline mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> ToMantleVec3Acceleration(
    const glm::dvec3& glm_vec)
{
    return mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{
        units::acceleration::meters_per_second_squared_t(glm_vec.x),
        units::acceleration::meters_per_second_squared_t(glm_vec.y),
        units::acceleration::meters_per_second_squared_t(glm_vec.z)};
}

inline std::vector<mantle_api::Vec3<units::length::meter_t>> ToMantleVectorOfVec3(
    const std::vector<glm::dvec3>& glm_vector)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> mantle_vector;
    mantle_vector.reserve(glm_vector.size());
    std::transform(
        glm_vector.cbegin(), glm_vector.cend(), std::back_inserter(mantle_vector), [](const glm::vec3& vec3) {
            return ToMantleVec3Length(vec3);
        });
    return mantle_vector;
}

}  // namespace astas::service::glmwrapper

#endif  // GTGEN_CORE_SERVICE_GLMWRAPPER_GLMMANTLECONVERSION_H
