/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICSCALARUTILS_H
#define GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICSCALARUTILS_H

namespace astas::service::glmwrapper
{

double Radians(double deg);

double Clamp(double a, double b, double c);

double Mix(double d1, double d2, double d3);

}  // namespace astas::service::glmwrapper

#endif  // GTGEN_CORE_SERVICE_GLMWRAPPER_GLMBASICSCALARUTILS_H
