/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::service::glmwrapper
{
using units::literals::operator""_m;

TEST(DistanceTest, GivenTwoPoints_WhenComputingDistance_ThenDistanceIsCorrect)
{
    mantle_api::Vec3<units::length::meter_t> pos1{10.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> pos2{10.0_m, 10.0_m, 0.0_m};
    double expected_distance{10.0};

    EXPECT_EQ(expected_distance, Distance(pos1, pos2));
}

TEST(DistanceTest, GivenTwoPoints_WhenComputingDistance2_ThenDistance2IsCorrect)
{
    mantle_api::Vec3<units::length::meter_t> pos1{10.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> pos2{10.0_m, 10.0_m, 0.0_m};
    double expected_distance{100.0};

    EXPECT_EQ(expected_distance, Distance2(pos1, pos2));
}

TEST(LengthTest, GivenVector_WhenComputingLength_ThenLengthIsCorrect)
{
    mantle_api::Vec3<units::length::meter_t> vec{3.0_m, 4.0_m, 0.0_m};
    double expected_length{5.0};

    EXPECT_EQ(expected_length, Length(vec));
}

TEST(LengthTest, GivenVector_WhenComputingLength2_ThenLength2IsCorrect)
{
    mantle_api::Vec3<units::length::meter_t> vec{3.0_m, 4.0_m, 0.0_m};
    double expected_length2{25.0};

    EXPECT_EQ(expected_length2, Length2(vec));
}

TEST(NormalizeTest, GivenVector_WhenNormalize_ThenLengthOfVectorIsOne)
{
    mantle_api::Vec3<units::length::meter_t> input{5_m, 5_m, 5_m};
    mantle_api::Vec3<units::length::meter_t> normalized{0.57735027_m, 0.57735027_m, 0.57735027_m};

    ASSERT_NEAR(1.0, normalized.Length()(), 0.00001);
    EXPECT_TRIPLE(normalized, Normalize(input));
}

TEST(CrossTest, GivenVector_WhenComputingCross_ThenCrossIsCorrect)
{
    // a_y​* b_z​− a_z​* b_y
    // a_z​* b_x​− a_x​* b_z
    // a_x​* b_y​− a_y​* b_x​​
    mantle_api::Vec3<units::length::meter_t> a{3.0_m, 4.0_m, 1.0_m};
    mantle_api::Vec3<units::length::meter_t> b{4.0_m, 5.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_cross{-5.0_m, 4.0_m, -1.0_m};

    EXPECT_TRIPLE(expected_cross, Cross(a, b));
}

TEST(DotTest, GivenTwoPoints_WhenComputingDot_ThenDotIsCorrect)
{
    // See https://de.wikipedia.org/wiki/Skalarprodukt section: "Beispiele"
    mantle_api::Vec3<units::length::meter_t> pt0{5_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> pt1{3_m, 0_m, 0_m};
    EXPECT_EQ(15, Dot(pt0, pt1));

    pt0 = mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m};
    pt1 = mantle_api::Vec3<units::length::meter_t>{-7_m, 8_m, 9_m};
    EXPECT_EQ(36, Dot(pt0, pt1));

    pt0 = mantle_api::Vec3<units::length::meter_t>{5_m, 0_m, 0_m};
    pt1 = mantle_api::Vec3<units::length::meter_t>{0_m, 3_m, 0_m};
    EXPECT_EQ(0, Dot(pt0, pt1));
}

TEST(NormalTest, GivenTwoPointsAndDirection_WhenPointsOnPlaneWithZeroZ_ThenNormalVectorGoesUp)
{
    mantle_api::Vec3<units::length::meter_t> pt0{1_m, 1_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> pt1{2_m, 2_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> direction{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> expected_normal{0_m, 0_m, 1_m};

    auto normal = Normal(pt0, pt1, direction);

    EXPECT_TRIPLE(expected_normal, normal);
}

TEST(MixTest, GivenVectors_WhenComputingMix_ThenMixIsCorrect)
{
    // See: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/mix.xhtml
    mantle_api::Vec3<units::length::meter_t> pt0{5_m, 10_m, 2_m};
    mantle_api::Vec3<units::length::meter_t> pt1{3_m, -1_m, 7_m};
    double d = 4;

    // internally computes: pt0×(1−d)+pt1×d
    mantle_api::Vec3<units::length::meter_t> expected_mix_vector{-3_m, -34_m, 22_m};
    EXPECT_EQ(expected_mix_vector, Mix(pt0, pt1, d));
}

}  // namespace astas::service::glmwrapper
