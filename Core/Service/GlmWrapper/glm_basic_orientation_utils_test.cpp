/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/GlmWrapper/glm_basic_orientation_utils.h"

#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::service::glmwrapper
{
using units::literals::operator""_m;
using units::literals::operator""_deg;
using units::literals::operator""_rad;

TEST(CreateRotationMatrix,
     GivenBasisWithYawOnlyRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    // 45°  yaw
    // 0°   pitch
    // 0°   roll

    const glm::dvec3 expected_forward = glm::normalize(glm::dvec3{1.0, 1.0, 0.0});
    const glm::dvec3 expected_up{0.0, 0.0, 1.0};
    const glm::dvec3 expected_left = glm::normalize(glm::dvec3{-1.0, 1.0, 0.0});

    const glm::dmat3 rot_mat = CreateRotationMatrix(expected_forward, expected_up);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}

TEST(CreateRotationMatrix,
     GivenBasisWithYawOnlyMantleRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    const glm::dvec3 expected_forward = glm::normalize(glm::dvec3{1.0, 1.0, 0.0});
    const glm::dvec3 expected_up{0.0, 0.0, 1.0};
    const glm::dvec3 expected_left = glm::normalize(glm::dvec3{-1.0, 1.0, 0.0});

    mantle_api::Orientation3<units::angle::degree_t> orientation;
    orientation.yaw = 45_deg;
    orientation.pitch = 0_deg;
    orientation.roll = 0_deg;

    const glm::dmat3 rot_mat = CreateRotationMatrix(orientation);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}

TEST(CreateRotationMatrix,
     GivenBasisWithRollOnlyRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    // 0°  yaw
    // 0°   pitch
    // 45°   roll
    const glm::dvec3 expected_forward{1.0, 0.0, 0.0};
    const glm::dvec3 expected_up = glm::normalize(glm::dvec3{0.0, -1.0, 1.0});
    const glm::dvec3 expected_left = glm::normalize(glm::dvec3{0.0, 1.0, 1.0});

    const glm::dmat3 rot_mat = CreateRotationMatrix(expected_forward, expected_up);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}
TEST(CreateRotationMatrix,
     GivenBasisWithRollOnlyMantleRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    const glm::dvec3 expected_forward{1.0, 0.0, 0.0};
    const glm::dvec3 expected_up = glm::normalize(glm::dvec3{0.0, -1.0, 1.0});
    const glm::dvec3 expected_left = glm::normalize(glm::dvec3{0.0, 1.0, 1.0});

    mantle_api::Orientation3<units::angle::degree_t> orientation;
    orientation.yaw = 0_deg;
    orientation.pitch = 0_deg;
    orientation.roll = 45_deg;

    const glm::dmat3 rot_mat = CreateRotationMatrix(orientation);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}

TEST(OrientationTypeSpec,
     GivenBasisWithPitchOnlyRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    // 0°  yaw
    // 45°   pitch
    // 0°   roll
    const glm::dvec3 expected_forward = glm::normalize(glm::dvec3{1.0, 0.0, -1.0});
    const glm::dvec3 expected_up = glm::normalize(glm::dvec3{1.0, 0.0, 1.0});
    const glm::dvec3 expected_left{0.0, 1.0, 0.0};

    const glm::dmat3 rot_mat = CreateRotationMatrix(expected_forward, expected_up);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}

TEST(OrientationTypeSpec,
     GivenBasisWithPitchOnlyMantleRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    const glm::dvec3 expected_forward = glm::normalize(glm::dvec3{1.0, 0.0, -1.0});
    const glm::dvec3 expected_up = glm::normalize(glm::dvec3{1.0, 0.0, 1.0});
    const glm::dvec3 expected_left{0.0, 1.0, 0.0};

    mantle_api::Orientation3<units::angle::degree_t> orientation;
    orientation.yaw = 0_deg;
    orientation.pitch = 45_deg;
    orientation.roll = 0_deg;

    const glm::dmat3 rot_mat = CreateRotationMatrix(orientation);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}

TEST(OrientationTypeSpec,
     GivenBasisWithYawAndRollRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    // 0°  yaw
    // 45°   pitch
    // 45°   roll

    const glm::dvec3 expected_forward = glm::normalize(glm::dvec3{1.0, 1.0, 0.0});
    // https://repl.it/@AlexFrasson/rotation
    const glm::dvec3 expected_up{0.5, -0.5, 0.70710678118};
    const glm::dvec3 expected_left{-0.5, 0.5, 0.70710678118};

    const glm::dmat3 rot_mat = CreateRotationMatrix(expected_forward, expected_up);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}

TEST(OrientationTypeSpec,
     GivenBasisWithYawAndRollMantleRotation_WhenLocalBasisIsMultipliedByTheRotationMatrix_ThenResultWillBeTheInputBasis)
{
    const glm::dvec3 expected_forward{0.70710678118, 0.70710678118, 0.0};
    // https://repl.it/@AlexFrasson/rotation
    const glm::dvec3 expected_up{0.5, -0.5, 0.70710678118};
    const glm::dvec3 expected_left{-0.5, 0.5, 0.70710678118};

    mantle_api::Orientation3<units::angle::degree_t> orientation;
    orientation.yaw = 45_deg;
    orientation.pitch = 0_deg;
    orientation.roll = 45_deg;

    const glm::dmat3 rot_mat = CreateRotationMatrix(orientation);

    const glm::dvec3 rotated_local_forward = rot_mat * ForwardVector();
    const glm::dvec3 rotated_local_up = rot_mat * UpVector();
    const glm::dvec3 rotated_local_left = rot_mat * LeftVector();

    EXPECT_TRIPLE(expected_forward, rotated_local_forward);
    EXPECT_TRIPLE(expected_up, rotated_local_up);
    EXPECT_TRIPLE(expected_left, rotated_local_left);
}

TEST(ToQuaternion, GivenOrientation_WhenToQuaternion_ThenExpectedQuaternion)
{
    const mantle_api::Orientation3<units::angle::radian_t> orientation{1.11_rad, 2.22_rad, 3.333_rad};
    const glm::dquat expected_quaternion{0, 0.42129016, 0.16049547, -0.78016137};

    glm::dquat result_quaternion = ToQuaternion(orientation);
    EXPECT_TRIPLE(expected_quaternion, result_quaternion);
}

TEST(ToQuaternion,
     GivenYawRollAndPitchOrientation_WhenToQauternionNormalized_ThenQuaternionNormalizedLeftVectorIsReturned)
{
    // https://repl.it/@AlexFrasson/rotation
    const glm::dvec3 expected_left_vector{-0.146446609, 0.853553391, 0.5};

    mantle_api::Orientation3<units::angle::radian_t> orientation;
    orientation.pitch = 45.0_deg;
    orientation.yaw = 45.0_deg;
    orientation.roll = 45.0_deg;

    const glm::dvec4 local_left_vector{LeftVector().x, LeftVector().y, LeftVector().z, 0.0};
    const auto quaternion = ToQuaternion(orientation);
    glm::dvec3 result_left_vector = glm::normalize(quaternion * local_left_vector);

    EXPECT_TRIPLE(expected_left_vector, result_left_vector);
}

TEST(FromQuaternion, GivenQuaternion_WhenFromQuaternion_ThenExpectedOrientation)
{
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        2.85384890_rad, 0.71729499_rad, -0.55334064_rad};
    const glm::dquat quaternion{0, 0.42129016, 0.16049547, -0.78016137};

    mantle_api::Orientation3<units::angle::radian_t> orientation = FromQuaternion(quaternion);

    EXPECT_TRIPLE(expected_orientation, orientation);
}

TEST(FromQuaternion, GivenQuaternion_WhenFromQuaternion_ThenOrientationWithPitchEqualToHalfOfPIIsReturned)
{
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        -2.75561940_rad, units::angle::radian_t(glm::half_pi<double>()), 0.0_rad};
    const glm::dquat quaternion{0, 0.82129016, 0.16049547, -0.78016137};

    mantle_api::Orientation3<units::angle::radian_t> orientation = FromQuaternion(quaternion);

    EXPECT_TRIPLE(expected_orientation, orientation);
}

TEST(FromQuaternion, GivenQuaternion_WhenFromQuaternion_ThenOrientationWithPitchEqualToMinusHalfOfPIIsReturned)
{
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        -3.52756590_rad, units::angle::radian_t(-glm::half_pi<double>()), 0.0_rad};
    const glm::dquat quaternion{0, -0.82129016, 0.16049547, -0.78016137};

    mantle_api::Orientation3<units::angle::radian_t> orientation = FromQuaternion(quaternion);

    EXPECT_TRIPLE(expected_orientation, orientation);
}

TEST(OrientationTypeSpec,
     GivenBasisWithYawOnlyRotation_WhenOrientationIsCreatedFromBasis_ThenYawOnlyOrientationWillBeReturned)
{
    const mantle_api::Vec3<units::length::meter_t> forward = Normalize({1.0_m, 1.0_m, 0.0_m});
    const mantle_api::Vec3<units::length::meter_t> up{0.0_m, 0.0_m, 1.0_m};

    mantle_api::Orientation3<units::angle::radian_t> expected_orientation;
    expected_orientation.pitch = 0.0_deg;
    expected_orientation.yaw = 45.0_deg;
    expected_orientation.roll = 0.0_deg;

    mantle_api::Orientation3<units::angle::radian_t> actual_orientation = FromBasisVectors(forward, up);

    EXPECT_TRIPLE(expected_orientation, actual_orientation);
}

TEST(OrientationTypeSpec,
     GivenBasisWithRollOnlyRotation_WhenOrientationIsCreatedFromBasis_ThenRollOnlyOrientationWillBeReturned)
{
    const mantle_api::Vec3<units::length::meter_t> forward{1.0_m, 0.0_m, 0.0_m};
    const mantle_api::Vec3<units::length::meter_t> up = Normalize({0.0_m, -1.0_m, 1.0_m});

    mantle_api::Orientation3<units::angle::radian_t> expected_orientation;
    expected_orientation.pitch = 0.0_deg;
    expected_orientation.yaw = 0.0_deg;
    expected_orientation.roll = 45.0_deg;

    mantle_api::Orientation3<units::angle::radian_t> actual_orientation = FromBasisVectors(forward, up);

    EXPECT_TRIPLE(expected_orientation, actual_orientation);
}

TEST(OrientationTypeSpec,
     GivenBasisWithPitchOnlyRotation_WhenOrientationIsCreatedFromBasis_ThenPitchOnlyOrientationWillBeReturned)
{
    const mantle_api::Vec3<units::length::meter_t> forward = Normalize({1.0_m, 0.0_m, -1.0_m});
    const mantle_api::Vec3<units::length::meter_t> up = Normalize({1.0_m, 0.0_m, 1.0_m});

    mantle_api::Orientation3<units::angle::radian_t> expected_orientation;
    expected_orientation.pitch = 45.0_deg;
    expected_orientation.yaw = 0.0_deg;
    expected_orientation.roll = 0.0_deg;

    mantle_api::Orientation3<units::angle::radian_t> actual_orientation = FromBasisVectors(forward, up);

    EXPECT_TRIPLE(expected_orientation, actual_orientation);
}

TEST(OrientationTypeSpec,
     GivenBasisWithYawAndRollRotation_WhenOrientationIsCreatedFromBasis_ThenYawAndRollOrientationWillBeReturned)
{
    const mantle_api::Vec3<units::length::meter_t> forward = Normalize({1.0_m, 1.0_m, 0.0_m});
    // https://repl.it/@AlexFrasson/rotation
    const mantle_api::Vec3<units::length::meter_t> up{0.5_m, -0.5_m, 0.70710678118_m};

    mantle_api::Orientation3<units::angle::radian_t> expected_orientation;
    expected_orientation.pitch = 0.0_deg;
    expected_orientation.yaw = 45.0_deg;
    expected_orientation.roll = 45.0_deg;

    mantle_api::Orientation3<units::angle::radian_t> actual_orientation = FromBasisVectors(forward, up);

    EXPECT_TRIPLE(expected_orientation, actual_orientation);
}

TEST(OrientationTypeSpec, GivenZeroOrientation_WhenLeftVectorIsRequested_ThenLocalSpaceLeftVectorIsReturned)
{
    const mantle_api::Vec3<units::length::meter_t> expected_left_vector{0.0_m, 1.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> orientation_zero{0.0_rad, 0.0_rad, 0.0_rad};

    auto left_vector = GetWorldSpaceLeftVector(orientation_zero);

    EXPECT_TRIPLE(expected_left_vector, left_vector);
}

TEST(OrientationTypeSpec, GivenPitchOnlyOrientation_WhenLeftVectorIsRequested_ThenLocalSpaceLeftVectorIsReturned)
{
    const mantle_api::Vec3<units::length::meter_t> expected_left_vector{0.0_m, 1.0_m, 0.0_m};

    mantle_api::Orientation3<units::angle::radian_t> orientation;
    orientation.pitch = 45.0_deg;
    orientation.yaw = 0.0_deg;
    orientation.roll = 0.0_deg;

    auto left_vector = GetWorldSpaceLeftVector(orientation);

    EXPECT_TRIPLE(expected_left_vector, left_vector);
}

TEST(OrientationTypeSpec, GivenYawOnlyOrientation_WhenLeftVectorIsRequested_ThenWorldSpaceLeftVectorIsReturned)
{
    const auto expected_left_vector = Normalize(mantle_api::Vec3<units::length::meter_t>{-1.0_m, 1.0_m, 0.0_m});

    mantle_api::Orientation3<units::angle::radian_t> orientation;
    orientation.pitch = 0.0_deg;
    orientation.yaw = 45.0_deg;
    orientation.roll = 0.0_deg;

    auto left_vector = GetWorldSpaceLeftVector(orientation);

    EXPECT_TRIPLE(expected_left_vector, left_vector);
}

TEST(OrientationTypeSpec, GivenRollOnlyOrientation_WhenLeftVectorIsRequested_ThenWorldSpaceLeftVectorIsReturned)
{
    const auto expected_left_vector = Normalize(mantle_api::Vec3<units::length::meter_t>{0.0_m, 1.0_m, 1.0_m});

    mantle_api::Orientation3<units::angle::radian_t> orientation;
    orientation.pitch = 0.0_deg;
    orientation.yaw = 0.0_deg;
    orientation.roll = 45.0_deg;

    auto left_vector = GetWorldSpaceLeftVector(orientation);

    EXPECT_TRIPLE(expected_left_vector, left_vector);
}

TEST(OrientationTypeSpec, GivenYawAndPitchOrientation_WhenLeftVectorIsRequested_ThenWorldSpaceLeftVectorIsReturned)
{
    const auto expected_left_vector = Normalize(mantle_api::Vec3<units::length::meter_t>{-1.0_m, 1.0_m, 0.0_m});

    mantle_api::Orientation3<units::angle::radian_t> orientation;
    orientation.pitch = 45.0_deg;
    orientation.yaw = 45.0_deg;
    orientation.roll = 0.0_deg;

    auto left_vector = GetWorldSpaceLeftVector(orientation);

    EXPECT_TRIPLE(expected_left_vector, left_vector);
}

TEST(OrientationTypeSpec, GivenYawAndRollOrientation_WhenLeftVectorIsRequested_ThenWorldSpaceLeftVectorIsReturned)
{
    // Check the cuboid room example: http://www.euclideanspace.com/maths/geometry/rotations/euler/
    // https://repl.it/@AlexFrasson/rotation
    const auto expected_left_vector = mantle_api::Vec3<units::length::meter_t>{-0.5_m, 0.5_m, 0.70710678118_m};

    mantle_api::Orientation3<units::angle::radian_t> orientation;
    orientation.pitch = 0.0_deg;
    orientation.yaw = 45.0_deg;
    orientation.roll = 45.0_deg;

    auto left_vector = GetWorldSpaceLeftVector(orientation);

    EXPECT_TRIPLE(expected_left_vector, left_vector);
}

TEST(OrientationTypeSpec, GivenYawRollAndPitchOrientation_WhenLeftVectorIsRequested_ThenWorldSpaceLeftVectorIsReturned)
{
    // https://repl.it/@AlexFrasson/rotation
    const auto expected_left_vector = mantle_api::Vec3<units::length::meter_t>{-0.146446609_m, 0.853553391_m, 0.5_m};

    mantle_api::Orientation3<units::angle::radian_t> orientation;
    orientation.pitch = 45.0_deg;
    orientation.yaw = 45.0_deg;
    orientation.roll = 45.0_deg;

    auto left_vector = GetWorldSpaceLeftVector(orientation);

    EXPECT_TRIPLE(expected_left_vector, left_vector);
}

TEST(GetWorldSpaceForwardVector, GivenOrientationYaw_WhenGetWorldSpaceForwardVector_ThenForwadVectorPointToY)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation{units::angle::radian_t(M_PI_2), 0_rad, 0_rad};

    mantle_api::Vec3<units::length::meter_t> expected_forward_vector{0_m, 1_m, 0_m};
    EXPECT_TRIPLE(expected_forward_vector, GetWorldSpaceForwardVector(orientation));
}

TEST(GetWorldSpaceUpVector, GivenOrientationYaw_WhenGetWorldSpaceUpVector_ThenForwadVectorPointToZ)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation{units::angle::radian_t(M_PI_2), 0_rad, 0_rad};

    mantle_api::Vec3<units::length::meter_t> expected_forward_vector{0_m, 0_m, 1_m};
    EXPECT_TRIPLE(expected_forward_vector, GetWorldSpaceUpVector(orientation));
}

}  // namespace astas::service::glmwrapper
