/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"

#include <gtest/gtest.h>

namespace astas::service::gt_conversion
{
using units::literals::operator""_m;
using units::literals::operator""_rad;

TEST(DimensionTest, GivenMantleDimension_WhenFillProtoObject_ThenProtoDimenstionFilled)
{
    mantle_api::Dimension3 input{1_m, 2_m, 3_m};
    astas_osi3::Dimension3d output;
    FillProtoObject(input, &output);

    EXPECT_EQ(input.length(), output.length());
    EXPECT_EQ(input.width(), output.width());
    EXPECT_EQ(input.height(), output.height());
}

TEST(DimensionTest, GivenMantleDimension_WhenCreateProtoObject_ThenProtoDimenstionFilled)
{
    mantle_api::Dimension3 input{1_m, 2_m, 3_m};
    auto output = CreateProtoObject<astas_osi3::Dimension3d>(input);

    EXPECT_EQ(input.length(), output.length());
    EXPECT_EQ(input.width(), output.width());
    EXPECT_EQ(input.height(), output.height());
}

TEST(OrientationTest, GivenMantleOrientation_WhenFillProtoObject_ThenProtoOrientationFilled)
{
    mantle_api::Orientation3<units::angle::radian_t> input{1_rad, 2_rad, 3_rad};
    astas_osi3::Orientation3d output;
    FillProtoObject(input, &output);

    EXPECT_EQ(input.yaw(), output.yaw());
    EXPECT_EQ(input.pitch(), output.pitch());
    EXPECT_EQ(input.roll(), output.roll());
}

TEST(OrientationTest, GivenMantleOrientation_WhenCreateFillProtoObject_ThenProtoOrientationFilled)
{
    mantle_api::Orientation3<units::angle::radian_t> input{1_rad, 2_rad, 3_rad};
    auto output = CreateProtoObject<astas_osi3::Orientation3d>(input);

    EXPECT_EQ(input.yaw(), output.yaw());
    EXPECT_EQ(input.pitch(), output.pitch());
    EXPECT_EQ(input.roll(), output.roll());
}

TEST(VectorTest, GivenMantleVector_WhenFillProtoObject_ThenProtoVectorFilled)
{
    mantle_api::Vec3<units::length::meter_t> input{1_m, 2_m, 3_m};
    astas_osi3::Vector3d output;
    FillProtoObject(input, &output);

    EXPECT_EQ(input.x(), output.x());
    EXPECT_EQ(input.y(), output.y());
    EXPECT_EQ(input.z(), output.z());
}

TEST(VectorTest, GivenMantleVector_WhenCreateProtoObject_ThenProtoVectorFilled)
{
    mantle_api::Vec3<units::length::meter_t> input{1_m, 2_m, 3_m};
    auto output = CreateProtoObject<astas_osi3::Vector3d>(input);

    EXPECT_EQ(input.x(), output.x());
    EXPECT_EQ(input.y(), output.y());
    EXPECT_EQ(input.z(), output.z());
}

}  // namespace astas::service::gt_conversion
