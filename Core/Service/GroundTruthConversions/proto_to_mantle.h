/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GROUNDTRUTHCONVERSIONS_PROTOTOMANTLE_H
#define GTGEN_CORE_SERVICE_GROUNDTRUTHCONVERSIONS_PROTOTOMANTLE_H

#include "astas_osi_common.pb.h"
#include "astas_osi_object.pb.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>

namespace astas::service::gt_conversion
{

inline mantle_api::Dimension3 ToDimension3(const astas_osi3::Dimension3d& proto_dimension)
{
    return {units::length::meter_t(proto_dimension.length()),
            units::length::meter_t(proto_dimension.width()),
            units::length::meter_t(proto_dimension.height())};
}

inline mantle_api::Orientation3<units::angle::radian_t> ToOrientation3(
    const astas_osi3::Orientation3d& proto_orientation)
{
    return {units::angle::radian_t(proto_orientation.yaw()),
            units::angle::radian_t(proto_orientation.pitch()),
            units::angle::radian_t(proto_orientation.roll())};
}

inline mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> ToOrientation3Rate(
    const astas_osi3::Orientation3d& proto_orientation)
{
    return {units::angular_velocity::radians_per_second_t(proto_orientation.yaw()),
            units::angular_velocity::radians_per_second_t(proto_orientation.pitch()),
            units::angular_velocity::radians_per_second_t(proto_orientation.roll())};
}

inline mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> ToOrientation3Acceleration(
    const astas_osi3::Orientation3d& proto_orientation)
{
    return {units::angular_acceleration::radians_per_second_squared_t(proto_orientation.yaw()),
            units::angular_acceleration::radians_per_second_squared_t(proto_orientation.pitch()),
            units::angular_acceleration::radians_per_second_squared_t(proto_orientation.roll())};
}

inline mantle_api::Vec3<units::length::meter_t> ToVec3Length(const astas_osi3::Vector3d& proto_vector)
{
    return {units::length::meter_t(proto_vector.x()),
            units::length::meter_t(proto_vector.y()),
            units::length::meter_t(proto_vector.z())};
}

inline mantle_api::Vec3<units::velocity::meters_per_second_t> ToVec3Velocity(const astas_osi3::Vector3d& proto_vector)
{
    return {units::velocity::meters_per_second_t(proto_vector.x()),
            units::velocity::meters_per_second_t(proto_vector.y()),
            units::velocity::meters_per_second_t(proto_vector.z())};
}

inline mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> ToVec3Acceleration(
    const astas_osi3::Vector3d& proto_vector)
{
    return {units::acceleration::meters_per_second_squared_t(proto_vector.x()),
            units::acceleration::meters_per_second_squared_t(proto_vector.y()),
            units::acceleration::meters_per_second_squared_t(proto_vector.z())};
}

inline mantle_api::Time ToTime(const astas_osi3::Timestamp& timestamp)
{
    auto timestamp_in_seconds = static_cast<double>(timestamp.seconds());
    timestamp_in_seconds += timestamp.nanos() / 1e+9;

    return mantle_api::SecondsToTime(timestamp_in_seconds);
}

inline mantle_api::IndicatorState ToIndicatorState(
    const astas_osi3::MovingObject_VehicleClassification_LightState_IndicatorState indicator_state)
{
    switch (indicator_state)
    {
        case astas_osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_UNKNOWN:
            return mantle_api::IndicatorState::kUnknown;
        case astas_osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OTHER:
            return mantle_api::IndicatorState::kOther;
        case astas_osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_OFF:
            return mantle_api::IndicatorState::kOff;
        case astas_osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_LEFT:
            return mantle_api::IndicatorState::kLeft;
        case astas_osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_RIGHT:
            return mantle_api::IndicatorState::kRight;
        case astas_osi3::MovingObject_VehicleClassification_LightState_IndicatorState_INDICATOR_STATE_WARNING:
            return mantle_api::IndicatorState::kWarning;
        default:
            return mantle_api::IndicatorState::kUnknown;
    }
}

}  // namespace astas::service::gt_conversion

#endif  // GTGEN_CORE_SERVICE_GROUNDTRUTHCONVERSIONS_PROTOTOMANTLE_H
