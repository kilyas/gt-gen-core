/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/FileSystem/file_system_utils.h"

#include "Core/Service/Exception/exception.h"

#include <pwd.h>
#include <unistd.h>

#include <algorithm>
#include <cassert>
#include <cctype>
#include <fstream>
#include <map>

namespace astas::service::file_system
{

void ReplaceTildeWithAbsoluteHomeDirectoryPath(fs::path& path)
{
    if (*path.begin() == "~")
    {
        fs::path absolute_path = GetHomeDirectory();
        for (auto it = std::next(path.begin()); it != path.end(); it++)
        {
            absolute_path /= *it;
        }
        path = absolute_path;
    }
}

fs::path GetHomeDirectory()
{
    const char* home_dir = std::getenv("HOME");

    // Sometimes getenv returns null for HOME - in that case look up the user record from the password database. The
    // pw_dir represents the home directory
    if (home_dir == nullptr)
    {
        home_dir = getpwuid(getuid())->pw_dir;
    }
    return fs::path{home_dir};
}

fs::path GetDefaultAstasDataDirectory()
{
    return GetHomeDirectory() / "ASTAS_DATA";
}

void CreateDirectoryIfNotExisting(const fs::path& path)
{
    if (!exists(path))
    {
        fs::create_directories(path);
    }
}

fs::path CreateOrWipeDirectoryToEnsureEmpty(const fs::path& root, const std::string& folder_name)
{
    assert(!folder_name.empty() && "foldername must be not empty");
    fs::path empty_folder{root / folder_name};
    create_directories(root);
    if (fs::exists(empty_folder))
    {
        fs::remove_all(empty_folder);
    }
    fs::create_directory(empty_folder);
    return empty_folder;
}

std::string GetFileExtension(const fs::path& file)
{
    std::string extension{file.extension().string()};
    // extension() may be empty - and contains '.'
    if (extension.size() > 1)
    {
        extension = extension.substr(1);
        std::transform(extension.begin(), extension.end(), extension.begin(), [](unsigned char elem) {
            return std::tolower(elem);
        });
    }
    return extension;
}

bool IsNdsMap(const fs::path& map_path)
{
    return GetFileExtension(map_path) == "nds";
}

bool IsOdrMap(const fs::path& map_path)
{
    return GetFileExtension(map_path) == "xodr";
}

bool ContainsPath(fs::path super_path, fs::path sub_path)
{
    if (!super_path.is_absolute())
    {
        super_path = fs::absolute(super_path);
    }
    if (!sub_path.is_absolute())
    {
        sub_path = fs::absolute(sub_path);
    }
    // Note: paths should be normalized but can't because they might not exist and the implementation
    // of fs::canonical throws if the path doesn't exist
    //    super_path = fs::canonical(super_path);
    //    sub_path = fs::canonical(sub_path);
    if (std::distance(super_path.begin(), super_path.end()) > std::distance(sub_path.begin(), sub_path.end()))
    {
        return false;
    }
    auto mismatch = std::mismatch(super_path.begin(), super_path.end(), sub_path.begin(), sub_path.end());
    return mismatch.first == super_path.end();
}

bool FileExists(const fs::path& file_path)
{
    return fs::exists(file_path);
}

void CreateEmptyFile(const fs::path& file_path)
{
    CreateDirectoryIfNotExisting(file_path.parent_path());

    std::ofstream ofs(file_path);
    ofs.close();
}

void CreateFile(const fs::path& file_path, const std::string& file_content)
{
    CreateDirectoryIfNotExisting(file_path.parent_path());

    std::ofstream ofs(file_path);
    ofs << file_content;
    ofs.close();
}

fs::path SearchRecursivelyInDirectory(const fs::path& file, const fs::path& directory)
{
    fs::path top_level_file{directory / file};
    if (fs::exists(top_level_file))
    {
        return fs::canonical(top_level_file);
    }

    if (!fs::exists(directory))
    {
        throw ServiceException("Cannot recursively search in non existing directory: {}. Current directory is: {}",
                               directory.string(),
                               fs::current_path().string());
    }

    auto predicate = [&file](const auto& entry) {
        return fs::is_regular_file(entry.path()) && entry.path().filename() == file;
    };

    fs::recursive_directory_iterator default_files{directory};
    auto it = std::find_if(begin(default_files), end(default_files), predicate);
    if (it != end(default_files))
    {
        return fs::canonical(it->path());
    }

    return {};
}

void SetRequiredReadWritePermissions(const fs::path& file_path)
{
    if (fs::is_regular_file(file_path))
    {
        fs::permissions(
            file_path, fs::perms::owner_read | fs::perms::owner_write | fs::perms::group_read | fs::perms::others_read);
    }
}

fs::path DetermineBackupFileName(const fs::path& input_file)
{
    auto backup_file = input_file;
    backup_file.replace_extension(input_file.extension().string() + ".bak");

    int backup_number = 1;
    while (fs::exists(backup_file))
    {
        backup_file.replace_extension(".bak" + std::to_string(backup_number));
        backup_number++;
    }

    return backup_file;
}

}  // namespace astas::service::file_system
