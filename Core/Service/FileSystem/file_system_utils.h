/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_FILESYSTEM_FILESYSTEMUTILS_H
#define GTGEN_CORE_SERVICE_FILESYSTEM_FILESYSTEMUTILS_H

#include "Core/Service/FileSystem/filesystem.h"

#include <string>

namespace astas::service::file_system
{
/// @brief Changes the path from ~/foo/bar to /home/user/foo/bar. If no '~' is contained, it does nothing
void ReplaceTildeWithAbsoluteHomeDirectoryPath(fs::path& path);

/// @brief Returns the home directory
fs::path GetHomeDirectory();

/// @brief Returns the default ASTAS_DATA directory
fs::path GetDefaultAstasDataDirectory();

/// @brief If the directory does not exist it is created
void CreateDirectoryIfNotExisting(const fs::path& path);

/// @brief If the directory already exists, it is cleaned. If is does not exist it is created
fs::path CreateOrWipeDirectoryToEnsureEmpty(const fs::path& root, const std::string& folder_name);

/// @brief Checks if the given path exists
bool FileExists(const fs::path& file_path);

/// @brief Creates an empty file
void CreateEmptyFile(const fs::path& file_path);

/// @brief Creates a file with the given content
void CreateFile(const fs::path& file_path, const std::string& file_content);

/// @brief Returns the extension of the given filename without leading '.'
std::string GetFileExtension(const fs::path& file);

/// @brief Checks if the given path is an NDS file.
bool IsNdsMap(const fs::path& map_path);

/// @brief Checks if the given path is an xodr file.
bool IsOdrMap(const fs::path& map_path);

/// @brief Returns true iff `sub_path` identifies a file/directory that is contained inside `super_path`
bool ContainsPath(fs::path super_path, fs::path sub_path);

/// @brief Searches a file in the given directory recursively
fs::path SearchRecursivelyInDirectory(const fs::path& file, const fs::path& directory);

/// @brief Sets permissions to standard for user created files
void SetRequiredReadWritePermissions(const fs::path& file_path);

/// @brief Determines the backup path
/// @param input_file The file to be backed up
/// @return The backup path of the input_file appended with a '.bak'
/// @note If there is already one backup file existing, then the returned backup will have the extension
///       '.bak1'. If a backup with a number also exists, the function will determine a number not yet taken.
///       This means calling this functions multiple times on the same input_file it will result in '.bak', '.bak1',
///       '.bak2', ...
fs::path DetermineBackupFileName(const fs::path& input_file);

}  // namespace astas::service::file_system

#endif  // GTGEN_CORE_SERVICE_FILESYSTEM_FILESYSTEMUTILS_H
