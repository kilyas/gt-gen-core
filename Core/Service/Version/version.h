/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_VERSION_VERSION_H
#define GTGEN_CORE_SERVICE_VERSION_VERSION_H

#include "Core/Service/Utility/version.h"

namespace astas
{

/// @brief This is the current GTGEN core version. Changes must be applied in the packaging BUILD file accordingly.
constexpr Version gtgen_core_version{GTGEN_CORE_VERSION_MAJOR, GTGEN_CORE_VERSION_MINOR, GTGEN_CORE_VERSION_PATCH};

///@brief Returns the current astas core lib version
std::string GetGtGenCoreVersion();

}  // namespace astas

#endif  // GTGEN_CORE_SERVICE_VERSION_VERSION_H
