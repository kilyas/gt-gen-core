/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_PROFILING_PROFILING_H
#define GTGEN_CORE_SERVICE_PROFILING_PROFILING_H

#ifdef ASTAS_PROFILING_ENABLED

#include "Tracy.hpp"

#define ASTAS_PROFILE_NEW_FRAME FrameMark
#define ASTAS_PROFILE_NEW_FRAME_NAMED(name) FrameMarkNamed(name)

#define ASTAS_PROFILE_SCOPE ZoneScoped
#define ASTAS_PROFILE_SCOPE_NAMED(name) ZoneScopedN(name)

#define ASTAS_PROFILE_MESSAGE(txt, size) TracyMessage(txt, size)

#else

#define ASTAS_PROFILE_NEW_FRAME
#define ASTAS_PROFILE_NEW_FRAME_NAMED(name)

#define ASTAS_PROFILE_SCOPE
#define ASTAS_PROFILE_SCOPE_NAMED(name)

#define ASTAS_PROFILE_MESSAGE(txt, size)

#endif

#endif  // GTGEN_CORE_SERVICE_PROFILING_PROFILING_H
