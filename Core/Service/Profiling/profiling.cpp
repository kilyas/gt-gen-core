/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

// We could not profile memory usage due to mismatched allocations when NautilusMiddleware was set to
// NautilusMiddleware::kRos. It seemed that delete was called for an address which was not previously allocated. See
// section 3.8 of Tracy's manual (https://bitbucket.org/wolfpld/tracy/downloads/tracy.pdf).
// As we do not use NautilusMiddleware anymore the above described problem could be solved.
// Memory could be profiled by removing the following guard and uncommenting the define TRACY_ON_DEMAND in
// ThirdParty/tracy/BUILD.
// However, due to an unknown reason, when profiling memory usage, all function calls disappear from the profiler. This
// seems to be a bug either in our setup or within Tracy itself and should be further investigated.
#ifdef ASTAS_PROFILING_WIP

#ifdef ASTAS_PROFILING_ENABLED

#include "Tracy.hpp"

#include <cstdlib>

void* operator new(std::size_t count)
{
    auto ptr = malloc(count);
    TracyAlloc(ptr, count);
    return ptr;
}

void operator delete(void* ptr) noexcept
{
    TracyFree(ptr);
    free(ptr);
}

void operator delete(void* ptr, std::size_t) noexcept
{
    TracyFree(ptr);
    free(ptr);
}

#endif

#endif
