/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_LOGGING_LOGGINGTEMPLATE_H
#define GTGEN_CORE_SERVICE_LOGGING_LOGGINGTEMPLATE_H

#include "Core/Service/Logging/simulation_time_and_context_log_prefix.h"
#include "Core/Service/Logging/spdlog_wrapper.h"

namespace logging_template
{
/// Note: Do not use this file directly. The intended usage is via the "logging.h" file and registration of the
/// desired namespace to a logger name.

/// Note: spdlog::get("loggername") might be quite slow, since it is protected by a mutex. Therefore instead of
/// using the logger registry a handle to the created logger is kept here in a static variable for quick access.
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - designed like this, OK
#define ENABLE_PACKAGE_LOGGING(packageName)                                                                       \
    /* NOLINTNEXTLINE(cert-err58-cpp, cppcoreguidelines-avoid-non-const-global-variables) */                      \
    static std::shared_ptr<spdlog::logger> g_package_logger{nullptr};                                             \
    inline const std::shared_ptr<spdlog::logger>& GetLogger()                                                     \
    {                                                                                                             \
        if (g_package_logger == nullptr)                                                                          \
        {                                                                                                         \
            g_package_logger = spdlog::default_logger()->clone(packageName);                                      \
        }                                                                                                         \
        return g_package_logger;                                                                                  \
    }                                                                                                             \
                                                                                                                  \
    template <typename... Args>                                                                                   \
    inline void Debug(const char* fmt, const Args&... args)                                                       \
    {                                                                                                             \
        using astas::service::logging::SimulationTimeAndContextLogPrefix;                                         \
        GetLogger()->debug(fmt::format("{}{}", SimulationTimeAndContextLogPrefix::Instance().Get(), fmt).c_str(), \
                           args...);                                                                              \
        GetLogger()->flush();                                                                                     \
    }                                                                                                             \
    template <typename T>                                                                                         \
    inline void Debug(const T& msg)                                                                               \
    {                                                                                                             \
        Debug("{}", msg);                                                                                         \
    }                                                                                                             \
    template <typename... Args>                                                                                   \
    inline void Info(const char* fmt, const Args&... args)                                                        \
    {                                                                                                             \
        using astas::service::logging::SimulationTimeAndContextLogPrefix;                                         \
        GetLogger()->info(fmt::format("{}{}", SimulationTimeAndContextLogPrefix::Instance().Get(), fmt).c_str(),  \
                          args...);                                                                               \
        GetLogger()->flush();                                                                                     \
    }                                                                                                             \
    template <typename T>                                                                                         \
    inline void Info(const T& msg)                                                                                \
    {                                                                                                             \
        Info("{}", msg);                                                                                          \
    }                                                                                                             \
    template <typename... Args>                                                                                   \
    inline void Warn(const char* fmt, const Args&... args)                                                        \
    {                                                                                                             \
        using astas::service::logging::SimulationTimeAndContextLogPrefix;                                         \
        GetLogger()->warn(fmt::format("{}{}", SimulationTimeAndContextLogPrefix::Instance().Get(), fmt).c_str(),  \
                          args...);                                                                               \
        GetLogger()->flush();                                                                                     \
    }                                                                                                             \
    template <typename T>                                                                                         \
    inline void Warn(const T& msg)                                                                                \
    {                                                                                                             \
        Warn("{}", msg);                                                                                          \
    }                                                                                                             \
    template <typename... Args>                                                                                   \
    inline void Error(const char* fmt, const Args&... args)                                                       \
    {                                                                                                             \
        using astas::service::logging::SimulationTimeAndContextLogPrefix;                                         \
        GetLogger()->error(fmt::format("{}{}", SimulationTimeAndContextLogPrefix::Instance().Get(), fmt).c_str(), \
                           args...);                                                                              \
        GetLogger()->flush();                                                                                     \
    }                                                                                                             \
    template <typename T>                                                                                         \
    inline void Error(const T& msg)                                                                               \
    {                                                                                                             \
        Error("{}", msg);                                                                                         \
    }                                                                                                             \
    template <typename Exception>                                                                                 \
    inline void LogAndThrow(const Exception& e)                                                                   \
    {                                                                                                             \
        Error(e.what());                                                                                          \
        throw e;                                                                                                  \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - designed like this, OK
#define TRACE(...) SPDLOG_LOGGER_TRACE(GetLogger(), __VA_ARGS__);

#ifndef NDEBUG
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - designed like this, OK
#define ASSERT(expression) assert(expression);
#else
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - designed like this, OK
#define ASSERT(expression)                                                 \
    if (!(expression))                                                     \
    {                                                                      \
        spdlog::default_logger()->error("ASSERTION HIT: {}", #expression); \
        std::terminate();                                                  \
    }
#endif

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - designed like this, OK
#define ASTAS_LOG_AND_THROW_WITH_FILE_DETAILS(astas_exception) \
    Error("EXCEPTION: {}", astas_exception.what());            \
    throw astas_exception;

}  // namespace logging_template

#endif  // GTGEN_CORE_SERVICE_LOGGING_LOGGINGTEMPLATE_H
