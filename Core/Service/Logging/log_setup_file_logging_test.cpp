/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Logging/log_setup.h"

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include <thread>

namespace astas::service::logging
{

class LogFrameworkGuard
{
  public:
    LogFrameworkGuard(const std::string& log_path,
                      FileLoggingOption file_logging_options,
                      const std::string& file_log_level = "Info",
                      const std::string& console_log_level = "Info")
    {
        logging::LogSetup::Instance().AddConsoleLogger(console_log_level);
        logging::LogSetup::Instance().AddFileLogger(log_path, file_logging_options, file_log_level);
    }

    ~LogFrameworkGuard() { logging::LogSetup::Instance().CleanupLogging(); }
    LogFrameworkGuard(LogFrameworkGuard const&) = delete;
    LogFrameworkGuard& operator=(const LogFrameworkGuard&) = delete;
    LogFrameworkGuard(LogFrameworkGuard&&) = delete;
    LogFrameworkGuard& operator=(LogFrameworkGuard&&) = delete;
};

class FileLoggingTest : public testing::Test
{
  public:
    void SetupLogFolder()
    {
        log_path_ = file_system::CreateOrWipeDirectoryToEnsureEmpty(fs::current_path(), "FileLoggingTest");
        ASSERT_TRUE(fs::exists(log_path_));
        ASSERT_EQ(0, std::distance(fs::directory_iterator(log_path_), fs::directory_iterator{}));
    }

    int GetLogFileCount()
    {
        return static_cast<int>(std::distance(fs::directory_iterator(log_path_), fs::directory_iterator{}));
    }

  protected:
    fs::path log_path_;
};

TEST_F(FileLoggingTest, GivenAlwaysLogToNewFile_WhenSetupLog_ThenLogFileCreated)
{
    SetupLogFolder();
    LogFrameworkGuard guard(log_path_, FileLoggingOption::kAlwaysLogToNewFile);

    ASSERT_EQ(1, GetLogFileCount());

    for (const auto& file : fs::directory_iterator(log_path_))
    {
        EXPECT_THAT(file.path().filename().string(), testing::HasSubstr("_astas_core.log"));
    }
}

TEST_F(FileLoggingTest, GivenAlwaysLogToNewFile_WhenSetupLogTwice_ThenTwoLogFilesCreated)
{
    SetupLogFolder();

    {
        LogFrameworkGuard guard(log_path_, FileLoggingOption::kAlwaysLogToNewFile);
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    {
        LogFrameworkGuard guard(log_path_, FileLoggingOption::kAlwaysLogToNewFile);
    }

    ASSERT_EQ(2, GetLogFileCount());
    for (const auto& file : fs::directory_iterator(log_path_))
    {
        EXPECT_THAT(file.path().filename().string(), testing::HasSubstr("_astas_core.log"));
        EXPECT_NE(0, fs::file_size(file));
    }
}

TEST_F(FileLoggingTest, GivenNoFileLogging_WhenSetupLog_ThenNoLogFileCreated)
{
    SetupLogFolder();

    {
        LogFrameworkGuard guard(log_path_, FileLoggingOption::kNoFileLogging);
    }

    ASSERT_EQ(0, GetLogFileCount());
}

TEST_F(FileLoggingTest, GivenFileLoggingOff_WhenSetupLog_ThenNoLogFileCreated)
{
    SetupLogFolder();

    {
        LogFrameworkGuard guard(log_path_, FileLoggingOption::kOverwritePreviousLogFile, "Off");
    }

    ASSERT_EQ(0, GetLogFileCount());
}

TEST_F(FileLoggingTest, GivenOverwritePreviousFile_WhenSetupLogTwice_ThenOneLogFileCreated)
{
    SetupLogFolder();

    {
        LogFrameworkGuard guard(log_path_, FileLoggingOption::kOverwritePreviousLogFile);
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));

    {
        LogFrameworkGuard guard(log_path_, FileLoggingOption::kOverwritePreviousLogFile);
    }

    ASSERT_EQ(1, GetLogFileCount());

    for (const auto& file : fs::directory_iterator(log_path_))
    {
        EXPECT_EQ("astas_core.log", file.path().filename().string());
        EXPECT_NE(0, fs::file_size(file));
    }
}

TEST_F(FileLoggingTest, GivenNonDefaultFileLogLevel_WhenSetupLog_ThenFileLoggerWithCorrectLogLevel)
{
    SetupLogFolder();

    LogFrameworkGuard guard(log_path_, FileLoggingOption::kOverwritePreviousLogFile, "Error");

    ASSERT_EQ(1, GetLogFileCount());

    for (const auto& file : fs::directory_iterator(log_path_))
    {
        EXPECT_EQ("astas_core.log", file.path().filename().string());
        EXPECT_EQ(0, fs::file_size(file));
    }
}

TEST_F(FileLoggingTest, GivenConsoleLogLevelOff_WhenLogging_ThenOnlyLogToFile)
{
    SetupLogFolder();

    LogFrameworkGuard guard(log_path_, FileLoggingOption::kOverwritePreviousLogFile, "Info", "Off");

    ASSERT_EQ(1, GetLogFileCount());

    for (const auto& file : fs::directory_iterator(log_path_))
    {
        EXPECT_EQ("astas_core.log", file.path().filename().string());
    }
}

}  // namespace astas::service::logging
