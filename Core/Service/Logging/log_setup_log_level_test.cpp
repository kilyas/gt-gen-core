/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Logging/log_setup.h"
#include "Core/Service/Logging/logging.h"

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

namespace astas::service::logging
{

class LogFrameworkGuard
{
  public:
    explicit LogFrameworkGuard(const std::string& console_log_level, const std::string& file_log_level)
    {
        logging::LogSetup::Instance().AddConsoleLogger(console_log_level);
        logging::LogSetup::Instance().AddFileLogger(
            fs::current_path(), FileLoggingOption::kAlwaysLogToNewFile, file_log_level);
    }

    ~LogFrameworkGuard()
    {
        logging::LogSetup::Instance().CleanupLogging();
        environment::g_package_logger = nullptr;
    }
    LogFrameworkGuard(LogFrameworkGuard const&) = delete;
    LogFrameworkGuard& operator=(const LogFrameworkGuard&) = delete;
    LogFrameworkGuard(LogFrameworkGuard&&) = delete;
    LogFrameworkGuard& operator=(LogFrameworkGuard&&) = delete;
};

TEST(LogLevelTest, GivenLogging_WhenLogNoError_ThenLogOnStdOut)
{
    LogFrameworkGuard guard("info", "off");
    testing::internal::CaptureStdout();
    testing::internal::CaptureStderr();

    Info("info-message");
    Warn("warn-message");

    std::string std_out = testing::internal::GetCapturedStdout();
    std::string std_err = testing::internal::GetCapturedStderr();
    EXPECT_THAT(std_out, testing::HasSubstr("info-message"));
    EXPECT_THAT(std_out, testing::HasSubstr("warn-message"));
    EXPECT_EQ("", std_err);
}

TEST(LogLevelTest, DISABLED_GivenLogging_WhenLogError_ThenErrorLoggedToStdErr)
{
    LogFrameworkGuard guard("info", "off");
    testing::internal::CaptureStderr();
    testing::internal::CaptureStdout();

    Error("error-message");

    std::string std_out = testing::internal::GetCapturedStdout();
    std::string std_err = testing::internal::GetCapturedStderr();
    EXPECT_EQ("", std_out);
    EXPECT_THAT(std_err, testing::HasSubstr("error-message"));
}

TEST(LogLevelTest, GivenConsoleLogLevelAsString_WhenSetupLogging_ThenLogLevelCorrect)
{
    {
        LogFrameworkGuard guard("un-supported-log-level-fallback-to-info", "also-not-supported");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(2, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::info, spdlog::default_logger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::info, spdlog::default_logger()->sinks().back()->level());

        EXPECT_EQ(spdlog::level::info, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::info, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("TrAcE", "Trace");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(2, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->sinks().back()->level());

        EXPECT_EQ(spdlog::level::trace, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::trace, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("DeBuG", "Debug");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(2, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::debug, spdlog::default_logger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::debug, spdlog::default_logger()->sinks().back()->level());

        EXPECT_EQ(spdlog::level::debug, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::debug, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("InFo", "Info");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(2, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::info, spdlog::default_logger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::info, spdlog::default_logger()->sinks().back()->level());

        EXPECT_EQ(spdlog::level::info, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::info, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("warN", "Warn");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(2, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::warn, spdlog::default_logger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::warn, spdlog::default_logger()->sinks().back()->level());

        EXPECT_EQ(spdlog::level::warn, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::warn, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("ERRoR", "Error");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(2, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::err, spdlog::default_logger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::err, spdlog::default_logger()->sinks().back()->level());

        EXPECT_EQ(spdlog::level::err, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::err, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("oFf", "Off");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(1, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::off, spdlog::default_logger()->sinks().front()->level());

        EXPECT_EQ(spdlog::level::off, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::off, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("oFf", "Error");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(2, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::off, spdlog::default_logger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::err, spdlog::default_logger()->sinks().back()->level());

        EXPECT_EQ(spdlog::level::off, environment::GetLogger()->sinks().front()->level());
        EXPECT_EQ(spdlog::level::err, environment::GetLogger()->sinks().back()->level());
    }

    {
        LogFrameworkGuard guard("error", "off");
        EXPECT_EQ(spdlog::level::trace, spdlog::default_logger()->level());
        EXPECT_EQ(1, spdlog::default_logger()->sinks().size());
        EXPECT_EQ(spdlog::level::err, spdlog::default_logger()->sinks().front()->level());
    }
}

}  // namespace astas::service::logging
