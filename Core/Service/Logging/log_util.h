/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_LOGGING_LOGUTIL_H
#define GTGEN_CORE_SERVICE_LOGGING_LOGUTIL_H

#include <fmt/format.h>

#include <fstream>
#include <string>

namespace astas::service::logging
{

class LogMessageAsFile final
{
  public:
    template <typename... Args>
    LogMessageAsFile(const std::string& filename, bool overwrite, const char* fmt, const Args&... args)
    {
        std::ofstream os(filename.c_str(), overwrite ? std::ios::out : std::ios::app);
        if (!os.is_open())
        {
            return;
        }

        std::string message{fmt::format(fmt, args...)};

        os << message;
        os.flush();
        os.close();
    }
};

}  // namespace astas::service::logging

#endif  // GTGEN_CORE_SERVICE_LOGGING_LOGUTIL_H
