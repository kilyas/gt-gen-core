/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Logging/scoped_duration_logger.h"

#include <MantleAPI/Common/time_utils.h>
#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

namespace astas::service::logging
{

TEST(ScopedDurationLoggerTest, GivenScopeWithDurationLogger_WhenLeavingScope_ThenDurationIsLogged)
{
    std::string expected_message{"Checking a scope duration"};
    std::string expected_duration{" took: 1ms"};
    testing::internal::CaptureStdout();

    {
        ScopedDurationLogger scoped_logger(expected_message);
        std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<std::int64_t>(mantle_api::Time{1}())));
    }

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(expected_message));
}

}  // namespace astas::service::logging
