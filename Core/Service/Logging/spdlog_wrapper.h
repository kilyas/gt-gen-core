/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_LOGGING_SPDLOGWRAPPER_H
#define GTGEN_CORE_SERVICE_LOGGING_SPDLOGWRAPPER_H

// ###########################################################################
//  Attention!
//  The macro below must be set before including spdlog headers
//  DO NOT INCLUDE SPDLOG headers directly!
// ###########################################################################
#ifndef NDEBUG
#undef SPDLOG_ACTIVE_LEVEL
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#endif

constexpr auto astas_logger_name{"astas"};

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/spdlog.h>

#endif  // GTGEN_CORE_SERVICE_LOGGING_SPDLOGWRAPPER_H
