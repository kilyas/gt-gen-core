/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_LOGGING_SCOPEDLOGGINGCONTEXT_H
#define GTGEN_CORE_SERVICE_LOGGING_SCOPEDLOGGINGCONTEXT_H

#include "Core/Service/Logging/simulation_time_and_context_log_prefix.h"

#include <string>

namespace astas::service::logging
{
/// Within a scope, this class will add another context to the log output
/// i.e.:
/// ScopedLoggingContext scoped_logging_context_guard("Foo");
/// Info("bar");
/// will result in a logging like: "...[info][SimulationTime][Foo] bar"
struct ScopedLoggingContext
{
    explicit ScopedLoggingContext(const std::string& ctx)
    {
        SimulationTimeAndContextLogPrefix::Instance().PushLoggingContext(ctx);
    }
    ~ScopedLoggingContext() { SimulationTimeAndContextLogPrefix::Instance().PopLoggingContext(); }
    ScopedLoggingContext(const ScopedLoggingContext& other) = default;
    ScopedLoggingContext(ScopedLoggingContext&& other) = default;
    ScopedLoggingContext& operator=(const ScopedLoggingContext& other) = default;
    ScopedLoggingContext& operator=(ScopedLoggingContext&& other) = default;
};

}  // namespace astas::service::logging

#endif  // GTGEN_CORE_SERVICE_LOGGING_SCOPEDLOGGINGCONTEXT_H
