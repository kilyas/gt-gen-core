/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Logging/simulation_time_and_context_log_prefix.h"

#include "Core/Service/MantleApiExtension/formatting.h"
#include "Core/Service/Utility/clock.h"

#include <fmt/format.h>

namespace astas::service::logging
{
std::string SimulationTimeAndContextLogPrefix::Get() const
{
    if (ctx_.empty())
    {
        return fmt::format("[{}] ", utility::Clock::Instance().Now());
    }
    else
    {
        return fmt::format("[{}] [{}] ", utility::Clock::Instance().Now(), ctx_.top());
    }
}

void SimulationTimeAndContextLogPrefix::PushLoggingContext(const std::string& ctx)
{
    ctx_.push(ctx);
}

void SimulationTimeAndContextLogPrefix::PopLoggingContext()
{
    if (!ctx_.empty())
    {
        ctx_.pop();
    }
}

}  // namespace astas::service::logging
