/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_LOGGING_SIMULATIONTIMEANDCONTEXTLOGPREFIX_H
#define GTGEN_CORE_SERVICE_LOGGING_SIMULATIONTIMEANDCONTEXTLOGPREFIX_H

#include <stack>
#include <string>

namespace astas::service::logging
{
/// If a logging context was pushed to the context stack, the Get call returns a string in the form:
/// [1.234s] [ContextStr]
/// else
/// [1.234s]
/// Where 1.234s stand as example SimulationTime value
///
/// By default ASTAS logging uses date+time + logger name + log level
/// If additional context are set via the ScopedLoggingContext, this class is used without the user noticing
class SimulationTimeAndContextLogPrefix
{
  public:
    static SimulationTimeAndContextLogPrefix& Instance()
    {
        static SimulationTimeAndContextLogPrefix g_time_and_context_prefix;
        return g_time_and_context_prefix;
    }
    std::string Get() const;
    void PushLoggingContext(const std::string& ctx);
    void PopLoggingContext();

  private:
    std::stack<std::string> ctx_{};

    SimulationTimeAndContextLogPrefix() = default;
};

}  // namespace astas::service::logging

#endif  // GTGEN_CORE_SERVICE_LOGGING_SIMULATIONTIMEANDCONTEXTLOGPREFIX_H
