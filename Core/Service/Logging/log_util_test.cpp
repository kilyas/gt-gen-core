/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/Logging/log_util.h"

#include "Core/Service/FileSystem/file_system_utils.h"

#include <gtest/gtest.h>

namespace astas::service::logging
{

class LogUtilTest : public testing::Test
{
  public:
    void SetUp() override
    {
        if (file_system::FileExists(file_path_))
        {
            fs::remove(file_path_);
        }

        ASSERT_FALSE(file_system::FileExists(file_path_));
    }
    void TearDown() override {}

  protected:
    fs::path file_path_{"foo.txt"};
};

TEST_F(LogUtilTest, GivenNonExistentFilePath_WhenLogMessageAsFile_ThenFileExists)
{
    LogMessageAsFile(file_path_, false, "{}, {}, {}\n", 1.111, 2.222, 3.333);
    EXPECT_TRUE(file_system::FileExists(file_path_));
    EXPECT_GT(fs::file_size(file_path_), 0);
}

TEST_F(LogUtilTest, GivenNonExistentFilePath_WhenLogMessageAsFileWithOverwriteEmpty_ThenFileSizeIsZero)
{
    LogMessageAsFile(file_path_, false, "{}, {}, {}\n", 1.111, 2.222, 3.333);
    LogMessageAsFile(file_path_, true, "");
    EXPECT_EQ(fs::file_size(file_path_), 0);
}

}  // namespace astas::service::logging
