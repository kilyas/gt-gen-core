/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/Logging/logging.h"

#include "Core/Service/Logging/scoped_logging_context.h"
#include "Core/Service/Utility/DateTime/date.h"
#include "Core/Service/Utility/DateTime/time.h"
#include "Core/Service/Utility/clock.h"

#include <MantleAPI/Common/time_utils.h>
#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

namespace astas::namespace_without_registered_logger
{
void Log(const std::string& log_message)
{
    Info(log_message);
}
}  // namespace astas::namespace_without_registered_logger

namespace astas::environment
{
namespace foo
{
void Log(const std::string& log_message)
{
    Info(log_message);
}
}  // namespace foo

void Log(const std::string& log_message)
{
    Info(log_message);
}
}  // namespace astas::environment

namespace astas::service::logging
{

TEST(LoggingTest, GivenLoggingInRegisteredNamespace_WhenLog_ThenLoggerContextAsRegistered)
{
    std::string expected_context{"environment"};
    testing::internal::CaptureStdout();

    environment::Log("foo");

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(expected_context));
}

TEST(LoggingTest, GivenLoggingInRegisteredSubNamespace_WhenLog_ThenContextAsRegisteredParent)
{
    std::string expected_context{"environment"};
    testing::internal::CaptureStdout();

    environment::foo::Log("foo");

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(expected_context));
}

TEST(LoggingTest, GivenLoggingInNotRegisteredSubNamespace_WhenLog_ThenContextIsAstasDefaultLogger)
{
    std::string expected_context{astas_logger_name};
    testing::internal::CaptureStdout();

    namespace_without_registered_logger::Log("foo");

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(expected_context));
}

TEST(LoggingTest, GivenLoggingWithScopedContext_WhenLog_ThenContextIsAdded)
{
    std::string expected_context{astas_logger_name};
    std::string expected_additional_context{"bar"};
    testing::internal::CaptureStdout();

    {
        ScopedLoggingContext scoped_logging_context_guard(expected_additional_context);
        Info("foo");
    }

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(expected_context));
    EXPECT_THAT(stdout, testing::HasSubstr(expected_additional_context));
}

TEST(LoggingTest, GivenSimulationTime_WhenLog_ThenSimulationTimeInLog)
{
    service::utility::Clock::Instance().SetNow(mantle_api::Time{1'234});
    std::string expected_simulation_time{"[1.234s]"};
    testing::internal::CaptureStdout();

    Info("foo");

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(expected_simulation_time));
}

TEST(LoggingTest, GivenTime_WhenLog_ThenCurrentDateAndTimeInLog)
{
    testing::internal::CaptureStdout();

    auto now = service::utility::Time::Now();
    std::string expected_date_and_time{
        fmt::format("[{} {:02d}:{:02d}:", utility::Date::Today(), now.hour, now.min)};  // ignore seconds and nanos

    Info("foo");

    std::string stdout = testing::internal::GetCapturedStdout();
    EXPECT_THAT(stdout, testing::HasSubstr(expected_date_and_time));
}

}  // namespace astas::service::logging
