/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_GUI_UIDATA_H
#define GTGEN_CORE_SERVICE_GUI_UIDATA_H

#include "ad_state.pb.h"
#include "driver_related_data.pb.h"
#include "marker.pb.h"

#include <mutex>
#include <utility>

namespace astas::service::gui
{

struct UiData
{
    messages::ui::AdState ad_state{messages::ui::AD_STATE_OFF};

    messages::ui::DriverRelatedData driver_related_data{};
    std::vector<messages::ui::Marker> markers{};
};

/// @brief Represents UI information that should be shown in GUI, additionally to Gt and Map
class UiDataProvider
{
  public:
    /// @note Since members are set separately, the returned struct might not represent the state within only
    /// one time stamp (when the getter is used between the setters).
    /// If this should ever be a problem, the provider could stepped to fill the UiData
    /// at the end of each sim step (like the Gt Builder).
    UiData GetUiData() const
    {
        std::scoped_lock lock(mutex_);
        return ui_data_;
    }

    void SetAdState(messages::ui::AdState ad_state)
    {
        std::scoped_lock lock(mutex_);
        ui_data_.ad_state = ad_state;
    }

    void SetDriverRelatedData(messages::ui::DriverRelatedData driver_data)
    {
        std::scoped_lock lock(mutex_);
        ui_data_.driver_related_data = std::move(driver_data);
    }

    void AddGuiMarker(messages::ui::Marker marker)
    {
        std::scoped_lock lock(mutex_);
        ui_data_.markers.push_back(std::move(marker));
    }

  private:
    UiData ui_data_{};

    mutable std::mutex mutex_;
};

}  // namespace astas::service::gui

#endif  // GTGEN_CORE_SERVICE_GUI_UIDATA_H
