/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_FORMATTING_H
#define GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_FORMATTING_H

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Map/map_details.h>
#include <fmt/format.h>

namespace fmt
{
template <>
struct formatter<mantle_api::OpenDriveLanePosition>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const mantle_api::OpenDriveLanePosition& odr_pos, FormatContext& ctx)
    {
        return format_to(ctx.out(),
                         "road {} lane {} s {:.3f} t {:.3f}",
                         odr_pos.road,
                         odr_pos.lane,
                         odr_pos.s_offset(),
                         odr_pos.t_offset());
    }
};

template <>
struct formatter<mantle_api::LatLonPosition>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const mantle_api::LatLonPosition& nds_pos, FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return format_to(ctx.out(),
                         "{:.9f}, {:.9f}",
                         units::angle::degree_t(nds_pos.latitude),
                         units::angle::degree_t(nds_pos.longitude));
    }
};

template <typename T>
struct formatter<mantle_api::Vec3<T>>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const mantle_api::Vec3<T>& vec, FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return format_to(ctx.out(), "{:.9f}, {:.9f}, {:.9f}", vec.x(), vec.y(), vec.z());
    }
};

// using Position = std::variant<mantle_api::OpenDriveLanePosition, mantle_api::LatLonPosition,
// mantle_api::Vec3<units::length::meter_t>>;

template <>
struct formatter<mantle_api::Position>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const mantle_api::Position& position, FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        if (auto odr_lane_position = std::get_if<mantle_api::OpenDriveLanePosition>(&position))
        {
            return format_to(ctx.out(), "{}", *odr_lane_position);
        }
        if (auto lat_lon_position = std::get_if<mantle_api::LatLonPosition>(&position))
        {
            return format_to(ctx.out(), "{}", *lat_lon_position);
        }
        if (auto vector_position = std::get_if<mantle_api::Vec3<units::length::meter_t>>(&position))
        {
            return format_to(ctx.out(), "{}", *vector_position);
        }
        return format_to(
            ctx.out(), "Scenario Position formatter is no treating this variant member! Please contact ASTAS support!");
    }
};

// Formatter specialization for units types
template <typename Unit>
struct formatter<Unit, std::enable_if_t<units::traits::is_unit_t<Unit>::value, char>> : formatter<double>
{
    template <typename FormatContext>
    auto format(Unit unit, FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return formatter<double>::format(unit(), ctx);
    }
};

template <>
struct formatter<mantle_api::Time>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const mantle_api::Time& time, FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return format_to(ctx.out(), "{:.3f}s", time() * 0.001);
    }
};

}  // namespace fmt

#endif  // GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_FORMATTING_H
