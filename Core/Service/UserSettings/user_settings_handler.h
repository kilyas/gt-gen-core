/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSHANDLER_H
#define GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSHANDLER_H

#include "Core/Service/UserSettings/user_settings.h"
#include "Core/Service/Utility/version.h"

#include <memory>
#include <string>

namespace mINI  // NOLINT(readability-identifier-naming)
{
template <typename T>
class INIMap;
using INIStructure = INIMap<INIMap<std::string>>;
}  // namespace mINI

namespace astas::service::user_settings
{
class UserSettingsReader;
class UserSettingsWriter;

class UserSettingsHandler
{
  public:
    UserSettingsHandler();
    UserSettingsHandler(UserSettingsHandler const&) = delete;
    UserSettingsHandler& operator=(const UserSettingsHandler&) = delete;
    UserSettingsHandler(UserSettingsHandler&&) = delete;
    UserSettingsHandler& operator=(UserSettingsHandler&&) = delete;
    ~UserSettingsHandler();

    /// @brief Initializes the handler
    /// @param ini_file_path Path to the UserSettings.ini that should be handled
    void Init(std::string ini_file_path);

    /// @brief Reads the user settings file to the user_settings_ struct
    void ReadFromFile();

    /// @brief Writes the user_settings_ struct to the file
    void WriteToFile(const Version& astas_version);

    /// @brief Provides read-only access to the user settings
    const UserSettings& GetUserSettings() const { return user_settings_; };

  private:
    void WriteUserSettingsHeader(const Version& astas_version);

    std::unique_ptr<UserSettingsReader> user_settings_reader_{nullptr};
    std::unique_ptr<UserSettingsWriter> user_settings_writer_{nullptr};

    // mINI representation of the file
    std::unique_ptr<mINI::INIStructure> ini_structure_{nullptr};

    std::string ini_file_path_{};
    UserSettings user_settings_{};
};

}  // namespace astas::service::user_settings

#endif  // GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSHANDLER_H
