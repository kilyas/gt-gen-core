/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/user_settings_path_list.h"

#include "Core/Service/Exception/exception.h"
#include "Core/Service/FileSystem/file_system_utils.h"

#include <gtest/gtest.h>

using astas::service::file_system::GetHomeDirectory;

namespace astas::service::user_settings
{

TEST(UserSettingsPathListTest, GivenMalformedUserSettingsPathListString_WhenCreate_ThenExecptionThrown)
{
    EXPECT_THROW(UserSettingsPathList::Create("a,b,c"), ServiceException) << "a,b,c";
    EXPECT_THROW(UserSettingsPathList::Create("{a,b,c"), ServiceException) << "{a,b,c";
    EXPECT_THROW(UserSettingsPathList::Create("a,b,c}"), ServiceException) << "a,b,c}";
}

TEST(UserSettingsPathListTest, GivenEmptyList_WhenAccessingMembers_ThenNoElementsContained)
{
    UserSettingsPathList empty_list{};
    EXPECT_TRUE(empty_list.elements.empty());
    EXPECT_EQ(empty_list.raw_string, "");
}

TEST(UserSettingsPathListTest, GivenValidList_WhenAccessingMembers_ThenElementsContained)
{
    UserSettingsPathList valid_list = UserSettingsPathList::Create("{one,two,three}");
    EXPECT_EQ(valid_list.elements.size(), std::size_t(3));
    EXPECT_EQ(valid_list.elements.at(0), "one");
    EXPECT_EQ(valid_list.elements.at(1), "two");
    EXPECT_EQ(valid_list.elements.at(2), "three");
}

TEST(UserSettingsPathListTest, GivenValidListWithWhitspaces_WhenCreate_ThenListContainesNoUnneccessaryWhitspaces)
{
    UserSettingsPathList needs_trimming = UserSettingsPathList::Create("  {    one,    two, three,, }   ");
    EXPECT_EQ(needs_trimming.elements.size(), std::size_t(3));
    EXPECT_EQ(needs_trimming.elements.at(0), "one");
    EXPECT_EQ(needs_trimming.elements.at(1), "two");
    EXPECT_EQ(needs_trimming.elements.at(2), "three");
}

TEST(UserSettingsPathListTest, GivenValidList_WhenPrint_ThenCurlyBracesContained)
{
    std::string expected_string = "{one, two, three}";
    UserSettingsPathList list = UserSettingsPathList::Create(expected_string);
    std::string formatted_string = fmt::format("{}", list);
    EXPECT_EQ(expected_string, formatted_string);
}

TEST(UserSettingsPathListTest, GivenValidList_WhenTilde_ThenTildeIsResolvedToHome)
{
    std::string expected_string = "{ ~/foobar }";
    UserSettingsPathList list = UserSettingsPathList::Create(expected_string);
    EXPECT_EQ(list.elements.at(0), (GetHomeDirectory() / "foobar").string());
}

}  // namespace astas::service::user_settings
