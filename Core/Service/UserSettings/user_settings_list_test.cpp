/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/user_settings_list.h"

#include "Core/Service/Exception/exception.h"

#include <gtest/gtest.h>

namespace astas::service::user_settings
{

TEST(UserSettingsListTest, GivenMalformedUserSettingsListString_WhenCreate_ThenExecptionThrown)
{
    EXPECT_THROW(UserSettingsList::Create("a,b,c"), ServiceException) << "a,b,c";
    EXPECT_THROW(UserSettingsList::Create("{a,b,c"), ServiceException) << "{a,b,c";
    EXPECT_THROW(UserSettingsList::Create("a,b,c}"), ServiceException) << "a,b,c}";
}

TEST(UserSettingsListTest, GivenEmptyList_WhenAccessingMembers_ThenNoElementsContained)
{
    UserSettingsList empty_list{};
    EXPECT_TRUE(empty_list.elements.empty());
    EXPECT_EQ(empty_list.raw_string, "");
}

TEST(UserSettingsListTest, GivenValidList_WhenAccessingMembers_ThenElementsContained)
{
    UserSettingsList valid_list = UserSettingsList::Create("{one,two,three}");
    EXPECT_EQ(valid_list.elements.size(), std::size_t(3));
    EXPECT_EQ(valid_list.elements.at(0), "one");
    EXPECT_EQ(valid_list.elements.at(1), "two");
    EXPECT_EQ(valid_list.elements.at(2), "three");
}

TEST(UserSettingsListTest, GivenValidListWithWhitspaces_WhenCreate_ThenListContainesNoUnneccessaryWhitspaces)
{
    UserSettingsList needs_trimming = UserSettingsList::Create("  {    one,    two, three,, }   ");
    EXPECT_EQ(needs_trimming.elements.size(), std::size_t(3));
    EXPECT_EQ(needs_trimming.elements.at(0), "one");
    EXPECT_EQ(needs_trimming.elements.at(1), "two");
    EXPECT_EQ(needs_trimming.elements.at(2), "three");
}

TEST(UserSettingsListTest, GivenValidList_WhenPrint_ThenCurlyBracesContained)
{
    std::string expected_string = "{one, two, three}";
    UserSettingsList list = UserSettingsList::Create(expected_string);
    std::string formatted_string = fmt::format("{}", list);
    EXPECT_EQ(expected_string, formatted_string);
}

}  // namespace astas::service::user_settings
