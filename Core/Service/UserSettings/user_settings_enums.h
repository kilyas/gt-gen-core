/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSENUMS_H
#define GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSENUMS_H

#include "Core/Service/Exception/exception.h"

#include <fmt/format.h>

#include <algorithm>
#include <map>

namespace astas::service::user_settings
{

/// @brief How the host is controlled
enum class HostControlMode
{
    kExternal,
    kInternal
};

/// @brief Represents the UI appearance
enum class ColorScheme
{
    kDark,
    kLight
};

/// @brief Represents the ClusterExecution section in the UserSettings ini file
enum class QualityLevel
{
    kFastest,
    kFast,
    kSimple,
    kGood,
    kBeautiful,
    kFantastic
};

/// @brief Here all enums are mapped to a string, to allow a generic lookup
template <typename E>
inline std::map<E, std::string> GetEnumLookupMap()
{
    if constexpr (std::is_same_v<E, ColorScheme>)
    {
        return {{ColorScheme::kDark, "Dark"}, {ColorScheme::kLight, "Light"}};
    }
    else if constexpr (std::is_same_v<E, HostControlMode>)
    {
        return {{HostControlMode::kExternal, "ExternalVehicle"}, {HostControlMode::kInternal, "Astas"}};
    }
    else if constexpr (std::is_same_v<E, QualityLevel>)
    {
        static std::map<E, std::string> quality_map{{QualityLevel::kFastest, "Fastest"},
                                                    {QualityLevel::kFast, "Fast"},
                                                    {QualityLevel::kSimple, "Simple"},
                                                    {QualityLevel::kGood, "Good"},
                                                    {QualityLevel::kBeautiful, "Beautiful"},
                                                    {QualityLevel::kFantastic, "Fantastic"}};

        return quality_map;
    }
    else
    {
        throw ServiceException("Unknown enumeration type {} for for finding enum-to-string lookup table",
                               typeid(E).name());
    }
}

/// @brief Generic lookup to the the string representation for a given enumeration value
template <typename E>
inline std::string EnumValueToString(const E& enum_value)
{
    const auto lookup_map = GetEnumLookupMap<E>();
    auto it = lookup_map.find(enum_value);
    // GetEnumLookupMap will throw for an unknown enum type.
    // Thus, if this point is reached, the iterator will never point to end() and we can safely return the value
    return it->second;
}

/// @brief Generic lookup to the enum value for a given enumeration string
template <typename E>
inline E ToEnum(const std::string& string_value)
{
    const auto lookup_map = GetEnumLookupMap<E>();

    const auto it =
        std::find_if(lookup_map.begin(), lookup_map.end(), [&string_value](const std::pair<E, std::string>& pr) {
            return pr.second == string_value;
        });

    if (it != lookup_map.end())
    {
        return it->first;
    }
    else
    {
        throw ServiceException("No enum exist for \"{}\"", string_value);
    }
}

}  // namespace astas::service::user_settings

///////////////////////////////////////////////////////////////////////////////
// In the section below: The boilerplate code for the fmt printing

template <>
struct fmt::formatter<astas::service::user_settings::QualityLevel>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const astas::service::user_settings::QualityLevel& quality_level, FormatContext& ctx)
    {
        return format_to(ctx.out(), "{}", astas::service::user_settings::EnumValueToString(quality_level));
    }
};

template <>
struct fmt::formatter<astas::service::user_settings::ColorScheme>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(  // NOLINT(readability-identifier-naming)
        const astas::service::user_settings::ColorScheme& color_scheme,
        FormatContext& ctx)
    {
        return format_to(ctx.out(), "{}", astas::service::user_settings::EnumValueToString(color_scheme));
    }
};

template <>
struct fmt::formatter<astas::service::user_settings::HostControlMode>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(  // NOLINT(readability-identifier-naming)
        const astas::service::user_settings::HostControlMode& movement,
        FormatContext& ctx)
    {
        return format_to(ctx.out(), "{}", astas::service::user_settings::EnumValueToString(movement));
    }
};

#endif  // GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSENUMS_H
