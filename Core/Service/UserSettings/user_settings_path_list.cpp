/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/user_settings_path_list.h"

#include "Core/Service/Exception/exception.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Utility/string_utils.h"

namespace astas::service::user_settings
{

/// @brief Parses a coma separated user settings and fills the data structure accordingly
UserSettingsPathList UserSettingsPathList::Create(std::string settings_string)
{
    UserSettingsPathList list;

    list.elements = service::utility::StringListToVector(settings_string);
    list.raw_string = std::move(settings_string);

    for (auto& element : list.elements)
    {
        auto element_path = fs::path(element);
        service::file_system::ReplaceTildeWithAbsoluteHomeDirectoryPath(element_path);
        element = element_path.string();
    }

    return list;
}

}  // namespace astas::service::user_settings
