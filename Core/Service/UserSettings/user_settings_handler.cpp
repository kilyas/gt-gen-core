/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/user_settings_handler.h"

#include "Core/Service/UserSettings/Internal/user_settings_reader.h"
#include "Core/Service/UserSettings/Internal/user_settings_writer.h"

#include <mINI/ini.h>

namespace astas::service::user_settings
{
UserSettingsHandler::UserSettingsHandler()
    : user_settings_reader_{std::make_unique<UserSettingsReader>()},
      user_settings_writer_{std::make_unique<UserSettingsWriter>()}
{
}

// Note: Class with forward declared unique_ptr members needs a non-inline default constructor
UserSettingsHandler::~UserSettingsHandler() = default;

void UserSettingsHandler::Init(std::string ini_file_path)
{
    ini_structure_ = std::make_unique<mINI::INIStructure>();
    ini_file_path_ = std::move(ini_file_path);

    user_settings_writer_->SetUserSettings(ini_file_path_, ini_structure_.get(), &user_settings_);
}

void UserSettingsHandler::WriteToFile(const Version& astas_version)
{
    WriteUserSettingsHeader(astas_version);
    user_settings_writer_->WriteSettingsStructToFile();
}

void UserSettingsHandler::WriteUserSettingsHeader(const Version& astas_version)
{
    /// @todo documentation link to be added below when available
    std::ofstream ofs(ini_file_path_);
    ofs << "# Version : " << fmt::format("{}", astas_version)
        << "\n# Documentation : see GT-Gen Core project from https://gitlab.eclipse.org/eclipse/openpass";
    ofs.close();
}

void UserSettingsHandler::ReadFromFile()
{
    user_settings_reader_->ReadFromFile(ini_file_path_, user_settings_);
}

}  // namespace astas::service::user_settings
