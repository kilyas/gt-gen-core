/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/user_settings_enums.h"

#include "Core/Service/Exception/exception.h"

#include <gtest/gtest.h>

namespace astas::service::user_settings
{

TEST(UserSettingsEnumsTest, GivenValidInputEnum_WhenEnumValueToString_ThenEnumAsStringReturned)
{
    EXPECT_EQ(EnumValueToString(HostControlMode::kExternal), "ExternalVehicle");
    EXPECT_EQ(EnumValueToString(HostControlMode::kInternal), "Astas");

    EXPECT_EQ(EnumValueToString(ColorScheme::kDark), "Dark");
    EXPECT_EQ(EnumValueToString(ColorScheme::kLight), "Light");

    EXPECT_EQ(EnumValueToString(QualityLevel::kFastest), "Fastest");
    EXPECT_EQ(EnumValueToString(QualityLevel::kFast), "Fast");
    EXPECT_EQ(EnumValueToString(QualityLevel::kSimple), "Simple");
    EXPECT_EQ(EnumValueToString(QualityLevel::kGood), "Good");
    EXPECT_EQ(EnumValueToString(QualityLevel::kBeautiful), "Beautiful");
    EXPECT_EQ(EnumValueToString(QualityLevel::kFantastic), "Fantastic");
}

TEST(UserSettingsEnumsTest, GivenInvalidInputEnum_WhenEnumValueToString_ThenExceptionThrown)
{
    enum class Buuh
    {
        kBuuh
    };

    EXPECT_THROW(EnumValueToString(Buuh::kBuuh), ServiceException);
}

TEST(UserSettingsEnumsTest, GivenValidInputString_WhenToEnum_ThenEnumReturned)
{
    EXPECT_EQ(ToEnum<HostControlMode>("Astas"), HostControlMode::kInternal);
    EXPECT_EQ(ToEnum<ColorScheme>("Dark"), ColorScheme::kDark);
    EXPECT_EQ(ToEnum<QualityLevel>("Fantastic"), QualityLevel::kFantastic);
}

TEST(UserSettingsEnumsTest, GivenInvalidInputString_WhenToEnum_ThenExceptionThrown)
{
    enum class Buuh
    {
        kBuuh
    };
    EXPECT_THROW(ToEnum<Buuh>("kBuuh"), ServiceException);
    EXPECT_THROW(ToEnum<HostControlMode>("Whatever"), ServiceException);
}

}  // namespace astas::service::user_settings
