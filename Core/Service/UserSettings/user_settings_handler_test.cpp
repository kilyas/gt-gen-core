/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/user_settings_handler.h"

#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Tests/TestUtils/resolve_dir_paths.h"

#include <gtest/gtest.h>

#include <fstream>

namespace astas::service::user_settings
{

TEST(UserSettingsHandlerTest, GivenIniFile_WhenInitAndRead_ThenUserSettingsLoaded)
{
    UserSettingsHandler user_settings_handler;
    user_settings_handler.Init(test_utils::ResolveDirPath("Core/Tests/Data/UserSettings") + "/InternalMovement.ini");
    user_settings_handler.ReadFromFile();

    EXPECT_EQ(user_settings::HostControlMode::kInternal,
              user_settings_handler.GetUserSettings().host_vehicle.host_control_mode);
}

TEST(UserSettingsHandlerTest, GivenNonExistingIni_WhenInitAndWrite_ThenFileIsWritten)
{
    Version expected_version{6, 6, 6};
    fs::path new_ini_file_path(test_utils::ResolveDirPath("Core/Tests/Data/UserSettings") + "/NonExistentSettings.ini");
    fs::remove(new_ini_file_path);

    UserSettingsHandler user_settings_handler;
    user_settings_handler.Init(new_ini_file_path.string());
    user_settings_handler.WriteToFile(expected_version);

    ASSERT_TRUE(fs::exists(new_ini_file_path));

    std::ifstream written_settings(new_ini_file_path.string());
    std::string version_line;
    std::getline(written_settings, version_line);

    ASSERT_NE(version_line.find(fmt::format("{}", expected_version)), std::string::npos);
}

}  // namespace astas::service::user_settings
