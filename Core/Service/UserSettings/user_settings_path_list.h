/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSPATHLIST_H
#define GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSPATHLIST_H

#include <fmt/format.h>

#include <string>
#include <vector>

namespace astas::service::user_settings
{

/// @brief Represents a user settings comma separated list. These lists must be enclosed in { }
struct UserSettingsPathList
{
    /// @brief Parses a comma separated user settings and fills the data structure accordingly
    static UserSettingsPathList Create(std::string settings_string);

    std::vector<std::string> elements{};
    std::string raw_string{""};
};

}  // namespace astas::service::user_settings

///////////////////////////////////////////////////////////////////////////////
// In the section below: The boilerplate code for the fmt printing

template <>
struct fmt::formatter<astas::service::user_settings::UserSettingsPathList>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const astas::service::user_settings::UserSettingsPathList& user_settings_list, FormatContext& ctx)
    {
        return format_to(ctx.out(), "{}", user_settings_list.raw_string);
    }
};

#endif  // GTGEN_CORE_SERVICE_USERSETTINGS_USERSETTINGSPATHLIST_H
