/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/Internal/user_settings_reader.h"

#include "Core/Service/Exception/exception.h"
#include "Core/Service/Logging/logging.h"

namespace astas::service::user_settings
{

void UserSettingsReader::ReadFromFile(const std::string& ini_file_path, UserSettings& user_settings)
{
    Info("Reading user settings file from: {}", ini_file_path);

    mINI::INIStructure ini_structure{ReadIniStructureFromFile(ini_file_path)};

    ParseUserSettingsFromIniStructure(ini_structure, user_settings);

    Debug("Running with the following user setting values:\n{}", user_settings);
}

mINI::INIStructure UserSettingsReader::ReadIniStructureFromFile(const std::string& ini_file_path)
{
    mINI::INIFile file{ini_file_path};
    mINI::INIStructure ini_structure;

    if (file.read(ini_structure))
    {
        return ini_structure;
    }
    else
    {
        ASTAS_LOG_AND_THROW_WITH_FILE_DETAILS(
            ServiceException("Reading user settings failed: mINI could not read file: {}", ini_file_path))
    }
}

void UserSettingsReader::ParseUserSettingsFromIniStructure(const mINI::INIStructure& ini_structure,
                                                           UserSettings& user_settings)
{
    ini_structure_ = &ini_structure;
    user_settings_ = &user_settings;

    GetFileLogging();
    GetGroundTruth();
    GetHostVehicle();
    GetMap();
    GetMapChunking();
    GetUserDirectories();
    GetClock();
    GetFoxglove();

    ini_structure_ = nullptr;
    user_settings_ = nullptr;
}

void UserSettingsReader::GetFileLogging()
{
    Get(user_settings_->file_logging.log_level, "FileLogging", "LogLevel");
}

void UserSettingsReader::GetGroundTruth()
{
    Get(user_settings_->ground_truth.lane_marking_distance_in_m, "GroundTruth", "LaneMarkingDistance");
    Get(user_settings_->ground_truth.lane_marking_downsampling_epsilon, "GroundTruth", "SimplifyLaneMarkingsEpsilon");
    Get(user_settings_->ground_truth.lane_marking_downsampling, "GroundTruth", "SimplifyLaneMarkings");
    Get(user_settings_->ground_truth.allow_invalid_lane_locations, "GroundTruth", "AllowInvalidLaneLocations");
}

void UserSettingsReader::GetHostVehicle()
{
    Get(user_settings_->host_vehicle.host_control_mode, "HostVehicle", "Movement");
    Get(user_settings_->host_vehicle.blocking_communication, "HostVehicle", "BlockingCommunication");
    Get(user_settings_->host_vehicle.time_scale, "HostVehicle", "TimeScale");
    Get(user_settings_->host_vehicle.recovery_mode, "HostVehicle", "RecoveryMode");
}

void UserSettingsReader::GetMap()
{
    Get(user_settings_->map.include_obstacles, "Map", "IncludeObstacles");
}

void UserSettingsReader::GetMapChunking()
{
    Get(user_settings_->map_chunking.chunk_grid_size, "MapChunking", "ChunkGridSize");
    Get(user_settings_->map_chunking.cells_per_direction, "MapChunking", "CellsPerDirection");
}

void UserSettingsReader::GetUserDirectories()
{
    Get(user_settings_->user_directories.scenarios, "UserDirectories", "Scenarios");
    Get(user_settings_->user_directories.maps, "UserDirectories", "Maps");
    Get(user_settings_->user_directories.plugins, "UserDirectories", "Plugins");
}

void UserSettingsReader::GetClock()
{
    Get(user_settings_->clock.use_system_clock, "Clock", "UseSystemClock");
}

void UserSettingsReader::GetFoxglove()
{
    Get(user_settings_->foxglove.websocket_server, "Foxglove", "WebsocketServer");
}

}  // namespace astas::service::user_settings
