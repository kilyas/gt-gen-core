/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_USERSETTINGS_INTERNAL_USERSETTINGSWRITER_H
#define GTGEN_CORE_SERVICE_USERSETTINGS_INTERNAL_USERSETTINGSWRITER_H

#define MINI_CASE_SENSITIVE
#include "Core/Service/UserSettings/user_settings.h"

#include <mINI/ini.h>

#include <string>

namespace astas::service::user_settings
{

class UserSettingsWriter
{
  public:
    /// @param ini_file_path Path to the loaded UserSettings.ini file
    /// @param ini_structure Internal ini representation that will be written to
    /// @param user_settings Internal UserSettings that can be written from
    void SetUserSettings(std::string ini_file_path, mINI::INIStructure* ini_structure, UserSettings* user_settings);

    /// @brief Writes the stored UserSettings struct back to the internal ini struct and then to file
    void WriteSettingsStructToFile();

    /// @brief Directly writes the passed ini_structure to file
    void WriteIniStructToFile(mINI::INIStructure& ini_structure);

  private:
    /// @brief Sets the given user settings value to the internal INI structure. This structure will be written to file
    template <typename T>
    void Set(const T& value, const std::string& group, const std::string& entry)
    {
        if constexpr (std::is_same_v<T, bool>)
        {
            (*ini_structure_)[group][entry] = value ? "true" : "false";
        }
        else
        {
            // fmt::format can handle enums if a fmt::formatter is defined
            (*ini_structure_)[group][entry] = fmt::format("{}", value);
        }
    }

    /// @section Helper functions to fill the ini data structure from the service::user_settings data structures
    void SetFileLogging();
    void SetGroundTruth();
    void SetHostVehicle();
    void SetMap();
    void SetMapChunking();
    void SetUserDirectories();
    void SetClock();
    void SetFoxglove();

    std::string ini_file_path_{};
    mINI::INIStructure* ini_structure_{nullptr};
    UserSettings* user_settings_{nullptr};
};

}  // namespace astas::service::user_settings

#endif  // GTGEN_CORE_SERVICE_USERSETTINGS_INTERNAL_USERSETTINGSWRITER_H
