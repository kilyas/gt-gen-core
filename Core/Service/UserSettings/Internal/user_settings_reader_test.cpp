/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/Internal/user_settings_reader.h"

#include "Core/Service/Exception/exception.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Tests/TestUtils/resolve_dir_paths.h"

#include <gtest/gtest.h>

namespace astas::service::user_settings
{

class UserSettingsReaderTest : public testing::Test
{
  protected:
    UserSettingsReader user_settings_reader_;
    UserSettings user_settings_;
};

TEST_F(UserSettingsReaderTest, GivenNonExistingIniPath_WhenReadFromFile_ThenThrowException)
{
    std::string non_existing_ini_path{"blub/I/do/not/exist.ini"};

    EXPECT_THROW(user_settings_reader_.ReadFromFile(non_existing_ini_path, user_settings_), ServiceException);
}

TEST_F(UserSettingsReaderTest, GivenIniPath_WhenReadFromFile_ThenValuesReadFromFile)
{
    user_settings_reader_.ReadFromFile(
        fs::path(test_utils::ResolveDirPath("Core/Tests/Data/UserSettings") + "/NonDefaultValues.ini"), user_settings_);

    // [FileLogging]
    EXPECT_EQ(user_settings_.file_logging.log_level, "Error");

    // [GroundTruth]
    EXPECT_EQ(user_settings_.ground_truth.lane_marking_distance_in_m, 42.0);
    EXPECT_FALSE(user_settings_.ground_truth.allow_invalid_lane_locations);

    // [HostVehicle]
    EXPECT_EQ(user_settings_.host_vehicle.host_control_mode, HostControlMode::kExternal);
    EXPECT_TRUE(user_settings_.host_vehicle.blocking_communication);
    EXPECT_EQ(user_settings_.host_vehicle.time_scale, 0.25);
    EXPECT_TRUE(user_settings_.host_vehicle.recovery_mode);

    // [Map]
    EXPECT_TRUE(user_settings_.map.include_obstacles);

    // [MapChunking]
    EXPECT_EQ(user_settings_.map_chunking.chunk_grid_size, 123);
    EXPECT_EQ(user_settings_.map_chunking.cells_per_direction, 3);

    // [UserDirectories]
    EXPECT_EQ(user_settings_.user_directories.scenarios.elements.size(), 1);
    EXPECT_EQ(user_settings_.user_directories.scenarios.elements.at(0), "SCENARIO TEST");
    EXPECT_EQ(user_settings_.user_directories.maps.elements.size(), 2);
    EXPECT_EQ(user_settings_.user_directories.maps.elements.at(0), "MAPS");
    EXPECT_EQ(user_settings_.user_directories.maps.elements.at(1), "TEST");

    // [Clock]
    EXPECT_TRUE(user_settings_.clock.use_system_clock);

    // [Foxglove]
    EXPECT_TRUE(user_settings_.foxglove.websocket_server);
}

TEST_F(UserSettingsReaderTest, GivenEmptyIniFile_WhenReadFromFile_ThenUseDefaultSettings)
{
    user_settings_reader_.ReadFromFile(
        fs::path(test_utils::ResolveDirPath("Core/Tests/Data/UserSettings") + "/Empty.ini"), user_settings_);

    // [FileLogging]
    EXPECT_EQ(user_settings_.file_logging.log_level, "Debug");

    // [GroundTruth]
    EXPECT_EQ(user_settings_.ground_truth.lane_marking_distance_in_m, 0.4);
    EXPECT_EQ(user_settings_.ground_truth.lane_marking_downsampling_epsilon, 0.01);
    EXPECT_EQ(user_settings_.ground_truth.lane_marking_downsampling, true);
    EXPECT_TRUE(user_settings_.ground_truth.allow_invalid_lane_locations);

    // [HostVehicle]
    EXPECT_EQ(user_settings_.host_vehicle.host_control_mode, HostControlMode::kExternal);
    EXPECT_FALSE(user_settings_.host_vehicle.blocking_communication);
    EXPECT_EQ(user_settings_.host_vehicle.time_scale, 1.0);
    EXPECT_FALSE(user_settings_.host_vehicle.recovery_mode);

    // [Map]
    EXPECT_FALSE(user_settings_.map.include_obstacles);

    // [MapChunking]
    EXPECT_EQ(user_settings_.map_chunking.chunk_grid_size, 50);
    EXPECT_EQ(user_settings_.map_chunking.cells_per_direction, 2);

    // [UserDirectories]
    EXPECT_TRUE(user_settings_.user_directories.scenarios.elements.empty());
    EXPECT_TRUE(user_settings_.user_directories.maps.elements.empty());

    // [Clock]
    EXPECT_FALSE(user_settings_.clock.use_system_clock);

    // [Foxglove]
    EXPECT_FALSE(user_settings_.foxglove.websocket_server);
}

}  // namespace astas::service::user_settings
