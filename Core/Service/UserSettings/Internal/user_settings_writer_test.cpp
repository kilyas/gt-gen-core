/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Service/UserSettings/Internal/user_settings_writer.h"

#include "Core/Service/Exception/exception.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/UserSettings/Internal/user_settings_reader.h"
#include "Core/Tests/TestUtils/resolve_dir_paths.h"

#include <gtest/gtest.h>

namespace astas::service::user_settings
{

/// @brief Tests different combinations of reading/writing/modifying settings
class UserSettingsReadWriteTest : public testing::Test
{
  protected:
    UserSettingsReader user_settings_reader_;
    UserSettingsWriter user_settings_writer_;
    mINI::INIStructure ini_structure_;
    UserSettings user_settings_;
};

TEST_F(UserSettingsReadWriteTest, GivenInvalidSettingsFile_WhenWriteSettingsStructToFile_ThenExceptionThrown)
{
    fs::path invalid_path = fs::path("non/existing/path/settings.ini");

    user_settings_writer_.SetUserSettings(invalid_path, &ini_structure_, &user_settings_);

    EXPECT_THROW(user_settings_writer_.WriteSettingsStructToFile(), ServiceException);
}

TEST_F(UserSettingsReadWriteTest,
       GivenNotExistingUserSettings_WhenWriteSettingsStructToFile_ThenFileCreatedWithDefaultSettings)
{
    fs::path new_ini_file_path = fs::path(test_utils::ResolveDirPath("Core/Tests/Data/UserSettings") + "/BrandNew.ini");
    fs::remove(new_ini_file_path);

    // Write initial ini file
    {
        user_settings_writer_.SetUserSettings(new_ini_file_path, &ini_structure_, &user_settings_);

        EXPECT_NO_THROW(user_settings_writer_.WriteSettingsStructToFile());
        EXPECT_TRUE(fs::exists(new_ini_file_path));
    }

    // Use reader object to validate writing of the file
    user_settings_reader_.ReadFromFile(new_ini_file_path, user_settings_);

    // [FileLogging]
    EXPECT_EQ(user_settings_.file_logging.log_level, "Debug");

    // [GroundTruth]
    EXPECT_EQ(user_settings_.ground_truth.lane_marking_distance_in_m, 0.4);
    EXPECT_EQ(user_settings_.ground_truth.lane_marking_downsampling_epsilon, 0.01);
    EXPECT_EQ(user_settings_.ground_truth.lane_marking_downsampling, true);
    EXPECT_TRUE(user_settings_.ground_truth.allow_invalid_lane_locations);

    // [HostVehicle]
    EXPECT_EQ(user_settings_.host_vehicle.host_control_mode, HostControlMode::kExternal);
    EXPECT_EQ(user_settings_.host_vehicle.time_scale, 1.0);
    EXPECT_FALSE(user_settings_.host_vehicle.blocking_communication);
    EXPECT_FALSE(user_settings_.host_vehicle.recovery_mode);

    // [Map]
    EXPECT_FALSE(user_settings_.map.include_obstacles);

    // [MapChunking]
    EXPECT_EQ(user_settings_.map_chunking.chunk_grid_size, 50);
    EXPECT_EQ(user_settings_.map_chunking.cells_per_direction, 2);

    // [UserDirectories]
    EXPECT_TRUE(user_settings_.user_directories.scenarios.elements.empty());
    EXPECT_TRUE(user_settings_.user_directories.maps.elements.empty());

    // [Clock]
    EXPECT_FALSE(user_settings_.clock.use_system_clock);

    // [Foxglove]
    EXPECT_FALSE(user_settings_.foxglove.websocket_server);
}

TEST_F(UserSettingsReadWriteTest,
       GivenIniFileWithCustomSettings_WhenWriteDefaultUserSettings_ThenIniFileOverwrittenWithDefaultValues)
{

    // duplicate ini file, since we modify it in this test
    fs::path non_default_read_only =
        fs::path(test_utils::ResolveDirPath("Core/Tests/Data/UserSettings") + "/NonDefaultValues.ini");
    fs::path non_default_ini_copy = non_default_read_only.parent_path() / "NonDefaultValuesCopy.ini";
    fs::copy(non_default_read_only, non_default_ini_copy, fs::copy_options::overwrite_existing);

    // Read from file and write modifications back
    {
        user_settings_reader_.ReadFromFile(non_default_ini_copy, user_settings_);

        UserSettings& change_to_default_now = user_settings_;
        change_to_default_now = UserSettings{};
        user_settings_writer_.SetUserSettings(non_default_ini_copy, &ini_structure_, &user_settings_);
        user_settings_writer_.WriteSettingsStructToFile();
    }

    // Use another reader to validate that writing worked
    {
        UserSettingsReader user_settings_reader;
        mINI::INIStructure ini_structure;
        UserSettings written_default_settings;
        // the non-default copy file now contains all default values
        user_settings_reader.ReadFromFile(non_default_ini_copy, user_settings_);
        UserSettings default_settings{};

        // [FileLogging]
        EXPECT_EQ(written_default_settings.file_logging.log_level, default_settings.file_logging.log_level);

        EXPECT_EQ(written_default_settings.ground_truth.lane_marking_distance_in_m,
                  default_settings.ground_truth.lane_marking_distance_in_m);
        EXPECT_EQ(written_default_settings.ground_truth.lane_marking_downsampling_epsilon,
                  default_settings.ground_truth.lane_marking_downsampling_epsilon);
        EXPECT_EQ(written_default_settings.ground_truth.lane_marking_downsampling,
                  default_settings.ground_truth.lane_marking_downsampling);
        EXPECT_EQ(written_default_settings.ground_truth.allow_invalid_lane_locations,
                  default_settings.ground_truth.allow_invalid_lane_locations);

        // [HostVehicle]
        EXPECT_EQ(written_default_settings.host_vehicle.host_control_mode,
                  default_settings.host_vehicle.host_control_mode);
        EXPECT_EQ(written_default_settings.host_vehicle.blocking_communication,
                  default_settings.host_vehicle.blocking_communication);
        EXPECT_EQ(written_default_settings.host_vehicle.time_scale, default_settings.host_vehicle.time_scale);
        EXPECT_EQ(written_default_settings.host_vehicle.recovery_mode, default_settings.host_vehicle.recovery_mode);

        // [Map]
        EXPECT_EQ(written_default_settings.map.include_obstacles, default_settings.map.include_obstacles);

        // [MapChunking]
        EXPECT_EQ(written_default_settings.map_chunking.chunk_grid_size, default_settings.map_chunking.chunk_grid_size);
        EXPECT_EQ(written_default_settings.map_chunking.cells_per_direction,
                  default_settings.map_chunking.cells_per_direction);

        // [UserDirectories]
        EXPECT_EQ(written_default_settings.user_directories.scenarios.elements,
                  default_settings.user_directories.scenarios.elements);
        EXPECT_EQ(written_default_settings.user_directories.scenarios.raw_string,
                  default_settings.user_directories.scenarios.raw_string);
        EXPECT_EQ(written_default_settings.user_directories.maps.elements,
                  default_settings.user_directories.maps.elements);
        EXPECT_EQ(written_default_settings.user_directories.maps.raw_string,
                  default_settings.user_directories.maps.raw_string);

        // [Clock]
        EXPECT_FALSE(user_settings_.clock.use_system_clock);

        // [Foxglove]
        EXPECT_FALSE(user_settings_.foxglove.websocket_server);
    }
}

TEST_F(
    UserSettingsReadWriteTest,
    GivenEmptyIniFile_WhenReadingAndWritingChangedHostSettingBack_ThenIniFileContainsAllDefaultValuesAndTheCustomHostSettings)
{
    // duplicate ini file, since we modify it in this test
    fs::path empty_ini = fs::path(test_utils::ResolveDirPath("Core/Tests/Data/UserSettings") + "/Empty.ini");
    fs::path empty_ini_copy = empty_ini.parent_path() / "EmptyCopy.ini";
    fs::copy(empty_ini, empty_ini_copy, fs::copy_options::overwrite_existing);

    // Read empty ini file so that the default values are uses, then write some changes back
    // Therefore the empty file will be filled with the default values + the changed values
    {
        user_settings_reader_.ReadFromFile(empty_ini_copy, user_settings_);

        user_settings_.host_vehicle.time_scale = 10.0;
        user_settings_.host_vehicle.host_control_mode = HostControlMode::kInternal;
        user_settings_.host_vehicle.recovery_mode = true;
        user_settings_.foxglove.websocket_server = true;

        user_settings_writer_.SetUserSettings(empty_ini_copy, &ini_structure_, &user_settings_);
        user_settings_writer_.WriteSettingsStructToFile();
    }

    // Check that the changed values have really be written
    {
        UserSettingsReader user_settings_reader;
        mINI::INIStructure ini_structure;
        UserSettings written_settings;
        user_settings_reader.ReadFromFile(empty_ini_copy, written_settings);

        // [HostVehicle]
        EXPECT_EQ(written_settings.host_vehicle.host_control_mode, user_settings_.host_vehicle.host_control_mode);
        EXPECT_EQ(written_settings.host_vehicle.time_scale, user_settings_.host_vehicle.time_scale);
        EXPECT_EQ(written_settings.host_vehicle.blocking_communication,
                  user_settings_.host_vehicle.blocking_communication);  // still a default value
        EXPECT_EQ(written_settings.host_vehicle.recovery_mode, user_settings_.host_vehicle.recovery_mode);

        // [Foxglove]
        EXPECT_EQ(written_settings.foxglove.websocket_server, user_settings_.foxglove.websocket_server);
    }
}

}  // namespace astas::service::user_settings
