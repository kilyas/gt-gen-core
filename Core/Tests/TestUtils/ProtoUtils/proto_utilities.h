/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_PROTOUTILS_PROTOUTILITIES_H
#define GTGEN_CORE_TESTS_TESTUTILS_PROTOUTILS_PROTOUTILITIES_H

#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Traffic/i_entity.h>

namespace astas_osi3
{
inline bool operator==(const Identifier& rhs, const Identifier& lhs)
{
    return rhs.value() == lhs.value();
}

inline bool operator!=(const Identifier& rhs, const Identifier& lhs)
{
    return !(rhs == lhs);
}

}  // namespace astas_osi3

namespace astas::test_utils
{

void SetupEntity(mantle_api::IEntity* entity);

void AddArtificialPedestrians(int number, environment::api::EntityRepository& entity_repository);

void AddArtificialVehicles(int number, environment::api::EntityRepository& entity_repository);

void AddArtificialTrafficVehicle(mantle_api::UniqueId id,
                                 mantle_api::IndicatorState indicator,
                                 const std::string& model_reference,
                                 environment::api::EntityRepository& entity_repository);

void AddArtificialHostVehicle(mantle_api::IndicatorState indicator,
                              const std::string& model_reference,
                              mantle_api::ExternalControlState had_control_state,
                              environment::api::EntityRepository& entity_repository);

void AddArtificialPedestrian(mantle_api::UniqueId id,
                             const std::string& model_reference,
                             environment::api::EntityRepository& entity_repository);

void SetupBasicLaneGroup(mantle_api::UniqueId lane_group_id, environment::map::AstasMap& astas_map);

// TODO: at some point all the custom and duplicated proto helper functions defined in many tests should end up here
std::optional<astas_osi3::MovingObject> GetHostVehicle(const astas_osi3::GroundTruth& gt);

std::optional<astas_osi3::MovingObject> GetTrafficVehicleById(const astas_osi3::GroundTruth& gt, std::uint64_t id);

std::optional<astas_osi3::MovingObject> GetMovingObjectByName(const astas_osi3::GroundTruth& gt,
                                                              const std::string& name);

astas_osi3::Vector3d GetDelta(const astas_osi3::Vector3d& a, const astas_osi3::Vector3d& b);

astas_osi3::Vector3d CreatePositionVector(double x, double y, double z);

std::vector<mantle_api::UniqueId> GetAllAntecessorIds(const astas_osi3::Lane& gt_lane);

std::vector<mantle_api::UniqueId> GetAllSuccessorIds(const astas_osi3::Lane& gt_lane);

std::vector<const astas_osi3::LaneBoundary*> GetLaneBoundariesByIds(
    const astas_osi3::GroundTruth& proto_gt,
    const google::protobuf::RepeatedPtrField<astas_osi3::Identifier>& ids);

std::vector<const astas_osi3::MovingObject*> GetObjectsByType(const astas_osi3::GroundTruth& proto_gt,
                                                              astas_osi3::MovingObject::Type type);

const astas_osi3::MovingObject* GetObjectById(const astas_osi3::GroundTruth& proto_gt, mantle_api::UniqueId id);

const astas_osi3::Lane* GetLaneById(const astas_osi3::GroundTruth& proto_gt, mantle_api::UniqueId id);

mantle_api::Vec3<units::length::meter_t> GetVehiclePosition(const std::optional<astas_osi3::MovingObject>& vehicle);

mantle_api::Vec3<units::velocity::meters_per_second_t> GetVehicleVelocity(
    const std::optional<astas_osi3::MovingObject>& vehicle);

mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> GetVehicleAcceleration(
    const std::optional<astas_osi3::MovingObject>& vehicle);

mantle_api::Orientation3<units::angle::radian_t> GetVehicleOrientation(
    const std::optional<astas_osi3::MovingObject>& vehicle);

mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> GetVehicleOrientationRate(
    const std::optional<astas_osi3::MovingObject>& vehicle);

mantle_api::Dimension3 GetVehicleDimension(const std::optional<astas_osi3::MovingObject>& vehicle);

std::vector<mantle_api::Vec3<units::length::meter_t>> GetLaneCenterline(
    const google::protobuf::RepeatedPtrField<astas_osi3::Lane>& lanes,
    std::uint64_t lane_id);
std::vector<mantle_api::Vec3<units::length::meter_t>> GetObjectConcatenatedCenterline(
    const astas_osi3::MovingObject& object,
    const google::protobuf::RepeatedPtrField<astas_osi3::Lane>& lanes);

double GetPositionDistanceToLaneCenterline(const mantle_api::Vec3<units::length::meter_t>& position,
                                           const google::protobuf::RepeatedPtrField<astas_osi3::Lane>& lanes,
                                           std::uint64_t lane_id);

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_PROTOUTILS_PROTOUTILITIES_H
