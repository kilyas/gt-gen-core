/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_EXCEPTIONTESTING_H
#define GTGEN_CORE_TESTS_TESTUTILS_EXCEPTIONTESTING_H

#include <gtest/gtest.h>

#include <string>

namespace astas
{

/// Helper for asserting that a given function throws an expected exception
template <typename ExceptionT, typename FunctionT>
inline testing::AssertionResult ExpectExceptionImpl(FunctionT fn,
                                                    const std::string& expected_error,
                                                    const bool error_is_substring = false)
{
    try
    {
        fn();
    }
    catch (const ExceptionT& ex)
    {
        const auto report_error = [&ex]() -> std::string {
            return "The following exception description does not match the expectation: '" + std::string{ex.what()} +
                   "'";
        };
        if (error_is_substring && (std::string{ex.what()}.find(expected_error) == std::string::npos))
        {
            return testing::AssertionFailure() << report_error();
        }
        if ((!error_is_substring) && (ex.what() != expected_error))
        {
            return testing::AssertionFailure() << report_error();
        }

        return testing::AssertionSuccess();
    }
    catch (...)
    {
        return testing::AssertionFailure() << "Catched unexpected exception type";
    }

    return testing::AssertionFailure() << "Function did not throw any exception";
}

template <typename ExceptionT, typename FunctionT>
inline testing::AssertionResult ExpectException(FunctionT fn, const std::string& expected_error)
{
    return ExpectExceptionImpl<ExceptionT>(std::move(fn), expected_error);
}

template <typename ExceptionT, typename FunctionT>
inline testing::AssertionResult ExpectExceptionContains(FunctionT fn, const std::string& expected_error)
{
    return ExpectExceptionImpl<ExceptionT>(std::move(fn), expected_error, true);
}

}  // namespace astas

#endif  // GTGEN_CORE_TESTS_TESTUTILS_EXCEPTIONTESTING_H
