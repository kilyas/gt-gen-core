/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "convert_first_custom_traffic_command_of_host_vehicle_to_route.h"

#include "Core/Service/Utility/string_utils.h"
#include "astas_osi_trafficcommand.pb.h"

#include <algorithm>
#include <cstdint>
#include <string>
#include <vector>

namespace astas::test_utils
{

std::vector<std::uint64_t> ConvertFirstCustomTrafficCommandOfHostVehicleToRoute(
    const std::vector<astas_osi3::TrafficCommand>& commands)
{
    std::vector<std::uint64_t> path{};
    const auto& first_host_vehicle_traffic_command_iterator =
        std::find_if(commands.cbegin(), commands.cend(), [](const auto& command) {
            return command.traffic_participant_id().value() == 0;
        });

    if (first_host_vehicle_traffic_command_iterator != commands.cend())
    {
        const auto& actions = first_host_vehicle_traffic_command_iterator->action();
        // Find custom action with command type "route"
        const auto& route_action_iterator = std::find_if(actions.cbegin(), actions.cend(), [](const auto& action) {
            return action.has_custom_action() && action.custom_action().command_type() == "route";
        });
        if (route_action_iterator != actions.cend())
        {
            // Convert comma separated string into a vector of lane IDs
            const auto& lane_id_strings =
                astas::service::utility::Split(route_action_iterator->custom_action().command());
            std::transform(
                lane_id_strings.cbegin(), lane_id_strings.cend(), std::back_inserter(path), [](const auto& element) {
                    return std::stoull(element);
                });
        }
    }
    return path;
}

}  // namespace astas::test_utils
