/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MINIMALPOINTDISTANCETOPOLYLINE_H
#define GTGEN_CORE_TESTS_TESTUTILS_MINIMALPOINTDISTANCETOPOLYLINE_H

#include <MantleAPI/Common/vector.h>

#include <vector>

namespace astas::test_utils
{

// @TODO: only used in path_control_component_test -> this should not be part of this package
double MinimalPointDistanceToPolyline(const std::vector<mantle_api::Vec3<units::length::meter_t>>& points,
                                      const mantle_api::Vec3<units::length::meter_t>& p);

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MINIMALPOINTDISTANCETOPOLYLINE_H
