/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MOCKENVIRONMENT_MOCKENVIRONMENT_H
#define GTGEN_CORE_TESTS_TESTUTILS_MOCKENVIRONMENT_MOCKENVIRONMENT_H

#include "Core/Environment/AstasEnvironment/i_astas_environment.h"

#include <gmock/gmock.h>

namespace astas::test_utils
{

class MockEnvironment : public environment::api::IAstasEnvironment
{
  public:
    MOCK_METHOD(void, Init, (), ());
    MOCK_METHOD(void, Step, (), ());

    MOCK_METHOD(environment::host::HostVehicleModel&, GetHostVehicleModel, (), ());

    MOCK_METHOD(const environment::map::AstasMap&, GetAstasMap, (), (const));
    MOCK_METHOD(const environment::proto_groundtruth::SensorViewBuilder&, GetSensorViewBuilder, (), (const));
    MOCK_METHOD(environment::chunking::StaticChunkList, GetStaticChunks, (), (const));
    MOCK_METHOD(const astas_osi3::SensorView&, GetSensorView, (), (const));
    MOCK_METHOD(const environment::host::HostVehicleInterface&, GetHostVehicleInterface, (), (const));
    MOCK_METHOD(const std::vector<astas_osi3::TrafficCommand>&, GetTrafficCommands, (), (const));

    MOCK_METHOD(void,
                CreateMap,
                (const std::string& absolute_map_file_path, const mantle_api::MapDetails& map_details),
                (override));

    MOCK_METHOD(void, AddEntityToController, (mantle_api::IEntity & entity, std::uint64_t controller_id), (override));
    MOCK_METHOD(void, RemoveEntityFromController, (std::uint64_t entity_id, std::uint64_t controller_id), (override));

    MOCK_METHOD(void,
                UpdateControlStrategies,
                (mantle_api::UniqueId entity_id,
                 std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies),
                (override));
    MOCK_METHOD(bool,
                HasControlStrategyGoalBeenReached,
                (std::uint64_t entity_id, mantle_api::ControlStrategyType type),
                (const override));

    MOCK_METHOD(mantle_api::ILaneLocationQueryService&, GetQueryService, (), (const override));
    MOCK_METHOD(mantle_api::ICoordConverter*, GetConverter, (), (const override));
    MOCK_METHOD(mantle_api::IGeometryHelper*, GetGeometryHelper, (), (const override));

    MOCK_METHOD(mantle_api::IEntityRepository&, GetEntityRepository, (), (override));
    MOCK_METHOD(const mantle_api::IEntityRepository&, GetEntityRepository, (), (const override));
    MOCK_METHOD(mantle_api::IControllerRepository&, GetControllerRepository, (), (override));
    MOCK_METHOD(const mantle_api::IControllerRepository&, GetControllerRepository, (), (const override));

    MOCK_METHOD(void, SetDateTime, (mantle_api::Time date_time), (override));
    MOCK_METHOD(mantle_api::Time, GetDateTime, (), (override));
    MOCK_METHOD(mantle_api::Time, GetSimulationTime, (), (override));

    MOCK_METHOD(void, SetWeather, (mantle_api::Weather weather), (override));
    MOCK_METHOD(void, SetRoadCondition, (std::vector<mantle_api::FrictionPatch> friction_patches), (override));
    MOCK_METHOD(void,
                SetTrafficSignalState,
                (const std::string& traffic_signal_name, const std::string& traffic_signal_state),
                (override));
    MOCK_METHOD(void,
                ExecuteCustomCommand,
                (const std::vector<std::string>& actors, const std::string& type, const std::string& command),
                (override));
    MOCK_METHOD(void, SetUserDefinedValue, (const std::string& name, const std::string& value), (override));
    MOCK_METHOD(std::optional<std::string>, GetUserDefinedValue, (const std::string& name), (override));

    MOCK_METHOD(void,
                SetDefaultRoutingBehavior,
                (mantle_api::DefaultRoutingBehavior default_routing_behavior),
                (override));
    MOCK_METHOD(void,
                AssignRoute,
                (mantle_api::UniqueId entity_id, mantle_api::RouteDefinition route_definition),
                (override));
    MOCK_METHOD(void, InitTrafficSwarmService, (const mantle_api::TrafficSwarmParameters& parameters), (override));
    MOCK_METHOD(mantle_api::ITrafficSwarmService&, GetTrafficSwarmService, (), (override));
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MOCKENVIRONMENT_MOCKENVIRONMENT_H
