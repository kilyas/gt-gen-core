/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_OPENDRIVEPASSTHROUGHCONVERTER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_OPENDRIVEPASSTHROUGHCONVERTER_H

#include "Core/Environment/Map/Common/coordinate_converter.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/vector.h>

namespace astas::test_utils
{
class PassThroughCoordinateConverter
    : public environment::map::IConverter<mantle_api::OpenDriveLanePosition, mantle_api::Vec3<units::length::meter_t>>
{
  public:
    using environment::map::IConverter<mantle_api::OpenDriveLanePosition,
                                       mantle_api::Vec3<units::length::meter_t>>::Convert;

    std::optional<mantle_api::Vec3<units::length::meter_t>> Convert(
        const mantle_api::OpenDriveLanePosition& coord) const override
    {
        return mantle_api::Vec3<units::length::meter_t>{coord.s_offset, coord.t_offset, units::length::meter_t(2)};
    }

    std::optional<mantle_api::OpenDriveLanePosition> Convert(
        const mantle_api::Vec3<units::length::meter_t>& coord) const override
    {
        return mantle_api::OpenDriveLanePosition{"0", 0, coord.x, coord.y};
    }
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_OPENDRIVEPASSTHROUGHCONVERTER_H
