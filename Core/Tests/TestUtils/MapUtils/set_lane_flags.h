/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_SETLANEFLAGS_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_SETLANEFLAGS_H

#include "Core/Environment/Map/AstasMap/astas_map.h"

#include <MantleAPI/Common/i_identifiable.h>

namespace astas::test_utils
{

inline void SetShoulderLaneFlag(const mantle_api::UniqueId lane_id, environment::map::AstasMap& map)
{
    auto lane = map.FindLane(lane_id);
    if (lane != nullptr)
    {
        lane->flags.SetShoulderLane();
    }
}

inline void SetMergeLaneFlag(const mantle_api::UniqueId lane_id, environment::map::AstasMap& map)
{
    auto lane = map.FindLane(lane_id);
    if (lane != nullptr)
    {
        lane->flags.SetMergeLane();
    }
}

inline void SetSplitLaneFlag(const mantle_api::UniqueId lane_id, environment::map::AstasMap& map)
{
    auto lane = map.FindLane(lane_id);
    if (lane != nullptr)
    {
        lane->flags.SetSplitLane();
    }
}

inline void SetNormalLaneFlag(const mantle_api::UniqueId lane_id, environment::map::AstasMap& map)
{
    auto lane = map.FindLane(lane_id);
    if (lane != nullptr)
    {
        lane->flags.SetNormalLane();
    }
}

inline void SetDrivableLaneFlag(const mantle_api::UniqueId lane_id, environment::map::AstasMap& map)
{
    auto lane = map.FindLane(lane_id);
    if (lane != nullptr)
    {
        lane->flags.SetDrivable();
    }
}

inline void SetExitLaneFlag(const mantle_api::UniqueId lane_id, environment::map::AstasMap& map)
{
    auto lane = map.FindLane(lane_id);
    if (lane != nullptr)
    {
        lane->flags.SetExitLane();
    }
}

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_SETLANEFLAGS_H
