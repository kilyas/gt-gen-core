/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_ASTASMAPBUILDER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_ASTASMAPBUILDER_H

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/Map/AstasMap/lane_boundary.h"
#include "Core/Environment/Map/AstasMap/patch.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>

#include <map>
#include <memory>

namespace astas::test_utils
{

/// @brief Very simple ASTAS map builder. Is a friend of the AstasMap and this prevents that we have too many test
/// friends. Just a very simple implementation, which fits the current testing needs, feel free to change/extend.
class AstasMapBuilder
{
  public:
    AstasMapBuilder() = default;

    void AddLane(mantle_api::UniqueId lane_group_id,
                 mantle_api::UniqueId lane_id,
                 const std::vector<mantle_api::Vec3<units::length::meter_t>>& centerline = {},
                 const std::vector<environment::map::LaneBoundary::Points>& right_boundary_points = {},
                 const std::vector<mantle_api::UniqueId>& successors = {},
                 const std::vector<mantle_api::UniqueId>& predecessors = {});

    void AddRoadObject(const environment::map::RoadObject& road_object);
    void AddTrafficSign(const std::shared_ptr<environment::map::TrafficSign>& sign);
    void AddTrafficLight(const environment::map::TrafficLight& traffic_light);

    void AddPatch(const std::shared_ptr<environment::map::Patch>& patch);

    void SetIds(mantle_api::UniqueId lane_group_id, mantle_api::UniqueId lane_id);

    void SetDrivable(bool drivable);

    void SetCenterLine(const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line);
    void ClearCenterLine();

    void SetRightBoundaries(const std::vector<environment::map::LaneBoundary::Points>& right_boundaries);
    void AddRightBoundary(const environment::map::LaneBoundary::Points& right_boundary);
    void ClearRightBoundaries();

    void SetSuccessors(const std::vector<mantle_api::UniqueId>& successors);
    void AddSuccessor(mantle_api::UniqueId successor);
    void ClearSuccessors();

    void SetPredecessors(const std::vector<mantle_api::UniqueId>& predecessors);
    void AddPredecessor(mantle_api::UniqueId predecessor);
    void ClearPredecessors();

    void SetLeftAdjacentLanes(const std::vector<mantle_api::UniqueId>& left_adjacent_lanes);
    void AddLeftAdjacentLane(mantle_api::UniqueId left_adjacent_lane);
    void ClearLeftAdjacentLanes();

    void SetRightAdjacentLanes(const std::vector<mantle_api::UniqueId>& right_adjacent_lanes);
    void AddRightAdjacentLane(mantle_api::UniqueId right_adjacent_lane);
    void CleaRightAdjacentLanes();

    void SetLeftLaneBoundaries(const std::vector<mantle_api::UniqueId>& left_lane_boundaries);
    void AddLeftLaneBoundaries(mantle_api::UniqueId left_lane_boundaries);
    void ClearLeftLaneBoundaries();

    void SetRightLaneBoundaries(const std::vector<mantle_api::UniqueId>& right_lane_boundaries);
    void AddRightLaneBoundaries(mantle_api::UniqueId right_lane_boundaries);
    void CleaRightLaneBoundaries();

    void SetMapPath(std::string fake_path);

    void CreateLane();

    void Clear();

    environment::map::Lane& GetMutableLane(mantle_api::UniqueId lane_id);

    environment::map::AstasMap astas_map;

  private:
    mantle_api::UniqueId lane_id_{};
    mantle_api::UniqueId lane_group_id_{};
    std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_{};
    std::vector<environment::map::LaneBoundary::Points> right_boundaries_{};
    std::vector<mantle_api::UniqueId> successors_{};
    std::vector<mantle_api::UniqueId> predecessors_{};

    std::vector<mantle_api::UniqueId> left_adjacent_lanes_{};
    std::vector<mantle_api::UniqueId> right_adjacent_lanes_{};

    std::vector<mantle_api::UniqueId> left_lane_boundaries_{};
    std::vector<mantle_api::UniqueId> right_lane_boundaries_{};

    bool drivable_{false};
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPUTILS_ASTASMAPBUILDER_H
