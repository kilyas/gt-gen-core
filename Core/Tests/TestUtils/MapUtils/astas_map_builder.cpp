/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Tests/TestUtils/MapUtils/astas_map_builder.h"

#include "Core/Environment/Map/AstasMap/astas_map.h"

namespace astas::test_utils
{

void AstasMapBuilder::AddLane(const mantle_api::UniqueId lane_group_id,
                              const mantle_api::UniqueId lane_id,
                              const std::vector<mantle_api::Vec3<units::length::meter_t>>& centerline,
                              const std::vector<environment::map::LaneBoundary::Points>& right_boundary_points,
                              const std::vector<mantle_api::UniqueId>& successors,
                              const std::vector<mantle_api::UniqueId>& predecessors)
{
    if (astas_map.FindLaneGroup(lane_group_id) == nullptr)
    {
        astas_map.AddLaneGroup(environment::map::LaneGroup{lane_group_id, environment::map::LaneGroup::Type::kUnknown});
    }

    environment::map::Lane lane{lane_id};

    if (drivable_)
    {
        lane.flags.SetDrivable();
    }

    lane.center_line = centerline;

    for (std::size_t i{0}; i < right_boundary_points.size(); ++i)
    {
        const auto lane_boundary_id = lane_id + 1000 + i;

        auto boundary = environment::map::LaneBoundary{lane_boundary_id,
                                                       environment::map::LaneBoundary::Type::kSolidLine,
                                                       environment::map::LaneBoundary::Color::kWhite};
        boundary.points = right_boundary_points[i];

        astas_map.AddLaneBoundary(lane_group_id, boundary);
        lane.right_lane_boundaries.emplace_back(lane_boundary_id);
    }

    for (const auto& successor : successors)
    {
        lane.successors.push_back(successor);
    }

    for (const auto& predecessor : predecessors)
    {
        lane.predecessors.push_back(predecessor);
    }

    for (const auto& right_lanes : right_adjacent_lanes_)
    {
        lane.right_adjacent_lanes.push_back(right_lanes);
    }

    for (const auto& left_lanes : left_adjacent_lanes_)
    {
        lane.left_adjacent_lanes.push_back(left_lanes);
    }

    for (const auto& left_boundary : left_lane_boundaries_)
    {
        lane.left_lane_boundaries.push_back(left_boundary);
    }

    for (const auto& right_boundary : right_lane_boundaries_)
    {
        lane.right_lane_boundaries.push_back(right_boundary);
    }

    astas_map.AddLane(lane_group_id, lane);
}

void AstasMapBuilder::SetIds(const mantle_api::UniqueId lane_group_id, const mantle_api::UniqueId lane_id)
{
    lane_id_ = lane_id;
    lane_group_id_ = lane_group_id;
}

void AstasMapBuilder::SetDrivable(bool drivable)
{
    drivable_ = drivable;
}

void AstasMapBuilder::SetCenterLine(const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line)
{
    center_line_ = center_line;
}
void AstasMapBuilder::ClearCenterLine()
{
    center_line_.clear();
}

void AstasMapBuilder::SetRightBoundaries(const std::vector<environment::map::LaneBoundary::Points>& right_boundaries)
{
    right_boundaries_ = right_boundaries;
}
void AstasMapBuilder::AddRightBoundary(const environment::map::LaneBoundary::Points& right_boundary)
{
    right_boundaries_.emplace_back(right_boundary);
}
void AstasMapBuilder::ClearRightBoundaries()
{
    right_boundaries_.clear();
}

void AstasMapBuilder::SetSuccessors(const std::vector<mantle_api::UniqueId>& successors)
{
    successors_ = successors;
}
void AstasMapBuilder::AddSuccessor(const mantle_api::UniqueId successor)
{
    successors_.emplace_back(successor);
}
void AstasMapBuilder::ClearSuccessors()
{
    successors_.clear();
}

void AstasMapBuilder::SetPredecessors(const std::vector<mantle_api::UniqueId>& predecessors)
{
    predecessors_ = predecessors;
}
void AstasMapBuilder::AddPredecessor(const mantle_api::UniqueId predecessor)
{
    predecessors_.emplace_back(predecessor);
}
void AstasMapBuilder::ClearPredecessors()
{
    predecessors_.clear();
}

void AstasMapBuilder::SetLeftAdjacentLanes(const std::vector<mantle_api::UniqueId>& left_adjacent_lanes)
{
    left_adjacent_lanes_ = left_adjacent_lanes;
}
void AstasMapBuilder::AddLeftAdjacentLane(const mantle_api::UniqueId left_adjacent_lane)
{
    left_adjacent_lanes_.push_back(left_adjacent_lane);
}
void AstasMapBuilder::ClearLeftAdjacentLanes()
{
    left_adjacent_lanes_.clear();
}

void AstasMapBuilder::SetRightAdjacentLanes(const std::vector<mantle_api::UniqueId>& right_adjacent_lanes)
{
    right_adjacent_lanes_ = right_adjacent_lanes;
}

void AstasMapBuilder::AddRightAdjacentLane(const mantle_api::UniqueId right_adjacent_lane)
{
    right_adjacent_lanes_.push_back(right_adjacent_lane);
}
void AstasMapBuilder::CleaRightAdjacentLanes()
{
    right_adjacent_lanes_.clear();
}

void AstasMapBuilder::SetLeftLaneBoundaries(const std::vector<mantle_api::UniqueId>& left_lane_boundaries)
{
    left_lane_boundaries_ = left_lane_boundaries;
}
void AstasMapBuilder::AddLeftLaneBoundaries(const mantle_api::UniqueId left_lane_boundaries)
{
    left_lane_boundaries_.push_back(left_lane_boundaries);
}
void AstasMapBuilder::ClearLeftLaneBoundaries()
{
    left_lane_boundaries_.clear();
}

void AstasMapBuilder::SetRightLaneBoundaries(const std::vector<mantle_api::UniqueId>& right_lane_boundaries)
{
    right_lane_boundaries_ = right_lane_boundaries;
}
void AstasMapBuilder::AddRightLaneBoundaries(const mantle_api::UniqueId right_lane_boundaries)
{
    right_lane_boundaries_.push_back(right_lane_boundaries);
}

void AstasMapBuilder::CleaRightLaneBoundaries()
{
    right_lane_boundaries_.clear();
}

void AstasMapBuilder::CreateLane()
{
    AddLane(lane_group_id_, lane_id_, center_line_, right_boundaries_, successors_, predecessors_);
}

void AstasMapBuilder::SetMapPath(std::string fake_path)
{
    astas_map.path = std::move(fake_path);
}

void AstasMapBuilder::AddRoadObject(const environment::map::RoadObject& road_object)
{
    astas_map.road_objects.push_back(road_object);
}

void AstasMapBuilder::AddTrafficSign(const std::shared_ptr<environment::map::TrafficSign>& sign)
{
    astas_map.traffic_signs.push_back(sign);
}

void AstasMapBuilder::AddTrafficLight(const environment::map::TrafficLight& traffic_light)
{
    astas_map.traffic_lights.push_back(traffic_light);
}

void AstasMapBuilder::AddPatch(const std::shared_ptr<environment::map::Patch>& patch)
{
    astas_map.patches.push_back(patch);
}

void AstasMapBuilder::Clear()
{
    ClearCenterLine();
    ClearRightBoundaries();
    ClearSuccessors();
    ClearPredecessors();
    CleaRightAdjacentLanes();
    ClearLeftAdjacentLanes();
}

environment::map::Lane& AstasMapBuilder::GetMutableLane(const mantle_api::UniqueId lane_id)
{
    return astas_map.GetLane(lane_id);
}

}  // namespace astas::test_utils
