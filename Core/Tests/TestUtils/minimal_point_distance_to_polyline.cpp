/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Tests/TestUtils/minimal_point_distance_to_polyline.h"

#include "Core/Service/GlmWrapper/glm_mantle_conversion.h"
#include "glm/gtx/norm.hpp"

namespace astas::test_utils
{

/// @brief Returns distance of point p from segment (v, w).
double DistancePointLinesegment(const mantle_api::Vec3<units::length::meter_t>& v,
                                const mantle_api::Vec3<units::length::meter_t>& w,
                                const mantle_api::Vec3<units::length::meter_t>& p)
{
    glm::dvec3 glm_v = service::glmwrapper::ToGlmVec3(v);
    glm::dvec3 glm_w = service::glmwrapper::ToGlmVec3(w);
    glm::dvec3 glm_p = service::glmwrapper::ToGlmVec3(p);

    double l2 = glm::length2(glm_v - glm_w);
    if (l2 < 1.0e-9)
    {
        return glm::distance(glm_p, glm_v);
    }
    double t = std::max(0.0, std::min(1.0, (glm::dot(glm_w - glm_v, glm_p - glm_v) / l2)));
    glm::dvec3 projection = glm_v + (glm_w - glm_v) * t;
    return glm::distance(glm_p, projection);
}

double MinimalPointDistanceToPolyline(const std::vector<mantle_api::Vec3<units::length::meter_t>>& points,
                                      const mantle_api::Vec3<units::length::meter_t>& p)
{
    double minimum_distance = DBL_MAX;
    for (std::size_t j = 1; j < points.size(); ++j)
    {
        double distance = DistancePointLinesegment(points[j - 1], points[j], p);
        minimum_distance = std::min(distance, minimum_distance);
    }
    return minimum_distance;
}

}  // namespace astas::test_utils
