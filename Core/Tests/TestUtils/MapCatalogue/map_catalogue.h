/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MAPCATALOGUE_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MAPCATALOGUE_H

#include "Core/Tests/TestUtils/MapCatalogue/lane_group_catalogue.h"

#include <vector>

namespace astas::test_utils
{

class MapCatalogue
{
  public:
    static std::unique_ptr<environment::map::AstasMap> EmptyMap()
    {
        auto astas_map = std::make_unique<environment::map::AstasMap>();
        astas_map->axis_aligned_world_bounding_box = environment::map::BoundingBox2d{{0.0, 0.0}, {42.0, 42.0}};
        astas_map->coordinate_converter = std::make_unique<test_utils::MockCoordinateConverter>();

        auto spatial_hash =
            std::make_unique<environment::map::SpatialHash>(astas_map->axis_aligned_world_bounding_box, 50);
        astas_map->SetSpatialHash(std::move(spatial_hash));

        return astas_map;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// --------------------
    /// | x x (Lane 3) x x | Configurable number of center line points and configurable distance between them
    /// --------------------
    ///
    /// IDs
    /// ----------------------
    /// LaneGroup: 0
    /// Right LaneBoundary: 1
    /// Left LaneBoundary: 2
    /// Lane: 3
    /// ----------------------
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneEastingLaneWithNPoints(int num_points,
                                                                                        int point_distance)
    {
        LaneCatalogue::LaneConfiguration config;
        config.origin = {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        config.number_of_center_line_points = num_points;
        config.distance_between_center_line_points = point_distance;

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithNPoints(config));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    ///         .     .
    ///       .    .
    ///    .    .
    ///  .    .
    ///
    /// Number of center line points and distance between them is depending on the passed in arguments+
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneNorthEastingLaneWithNPoints(int num_points,
                                                                                             int point_distance)
    {
        LaneCatalogue::LaneConfiguration config;
        config.origin = {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        config.number_of_center_line_points = num_points;
        config.distance_between_center_line_points = point_distance;

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightNorthEastingLaneWithNPoints(config));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// See MapWithOneEastingLaneWithNPoints
    ///
    /// The road can be rolled right:
    /// Left boundary  y: 4  °
    /// Center line    y: 2      °
    /// Right boundary y: 0          °
    ///
    /// Note: A offsetting the lane vertically via config.origin.z is currently not supported
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneEastingLaneWithNPointsRolledRight(int num_points,
                                                                                                   int point_distance)
    {
        LaneCatalogue::LaneConfiguration config;
        config.origin = {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        config.number_of_center_line_points = num_points;
        config.distance_between_center_line_points = point_distance;
        config.rolled = LaneCatalogue::Rolled::kRight;

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithNPoints(config));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// See MapWithOneEastingLaneWithNPoints
    ///
    /// The road can be rolled left:
    /// Left boundary  y: -2         °
    /// Center line    y: 0      °
    /// Right boundary y: 2  °
    ///
    /// Note: A offsetting the lane vertically via config.origin.z is currently not supported
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneEastingLaneWithNPointsRolledLeft(int num_points,
                                                                                                  int point_distance)
    {
        LaneCatalogue::LaneConfiguration config;
        config.origin = {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        config.number_of_center_line_points = num_points;
        config.distance_between_center_line_points = point_distance;
        config.rolled = LaneCatalogue::Rolled::kLeft;

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithNPoints(config));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// See MapWithOneEastingLaneWithNPoints
    ///
    /// The road is pitched upwards
    /// - each new center line points gets +20 in z
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneEastingLaneWithNPointsPitchedUp(int num_points,
                                                                                                 int point_distance)
    {
        LaneCatalogue::LaneConfiguration config;
        config.origin = {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        config.number_of_center_line_points = num_points;
        config.distance_between_center_line_points = point_distance;
        config.pitched = LaneCatalogue::Pitched::kUp;

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithNPoints(config));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// See MapWithOneEastingLaneWithNPoints
    ///
    /// The road is pitched downwards
    /// - each new center line points gets -20 in z
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneEastingLaneWithNPointsPitchedDown(int num_points,
                                                                                                   int point_distance)
    {
        LaneCatalogue::LaneConfiguration config;
        config.origin = {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        config.number_of_center_line_points = num_points;
        config.distance_between_center_line_points = point_distance;
        config.pitched = LaneCatalogue::Pitched::kDown;

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithNPoints(config));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// --------------------
    /// | x x (Lane 3) x x | y: 0
    /// --------------------
    /// ^                 ^
    /// (0;0)           (99;0)
    ///
    /// IDs
    /// ----------------------
    /// LaneGroup: 0
    /// Right LaneBoundary: 1
    /// Left LaneBoundary: 2
    /// Lane: 3
    /// ----------------------
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneEastingLaneWithHundredPoints()
    {
        RawMapBuilder builder;
        mantle_api::Vec3<units::length::meter_t> origin{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// --------------------
    /// | x x (Lane 3 & 7) x x | y: 0
    /// --------------------
    /// ^                 ^
    /// (0;0)           (99;0)
    ///
    /// IDs
    /// ----------------------
    /// LaneGroup: 0
    /// Right LaneBoundary: 1
    /// Left LaneBoundary: 2
    /// Lane: 3
    /// ----------------------
    /// ----------------------
    /// LaneGroup: 4
    /// Right LaneBoundary: 5
    /// Left LaneBoundary: 6
    /// Lane: 7
    /// ----------------------
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithTwoOverlappingEastingLaneWithHundredPointsEach()
    {
        RawMapBuilder builder;
        mantle_api::Vec3<units::length::meter_t> origin{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// --------------------
    /// | x x (Lane 3 & 7) x x | y: 0
    /// --------------------
    /// ^                 ^
    /// (0;0;0)          (99;0;0) <-3
    /// (0;0;5)          (99;0;5) <-7
    ///
    /// IDs
    /// ----------------------
    /// LaneGroup: 0
    /// Right LaneBoundary: 1
    /// Left LaneBoundary: 2
    /// Lane: 3
    /// ----------------------
    /// ----------------------
    /// LaneGroup: 4
    /// Right LaneBoundary: 5
    /// Left LaneBoundary: 6
    /// Lane: 7
    /// ----------------------
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap>
    MapWithTwoOverlappingEastingLanesAtDifferentAltitudesWithHundredPointsEach()
    {
        RawMapBuilder builder;
        mantle_api::Vec3<units::length::meter_t> origin_lower{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        mantle_api::Vec3<units::length::meter_t> origin_upper{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(5)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin_lower));
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin_upper));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// --------------------
    /// |      (Lane)      | y: 0
    /// --------------------
    /// ^                 ^
    /// (0;0)           (6000;0)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOne6kmEastingLaneWithTwoPoints()
    {
        RawMapBuilder builder;
        mantle_api::Vec3<units::length::meter_t> origin{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::Straight6kmEastingLaneWithTwoPoints(origin));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|------------------
    /// | x x (Lane 3) x x | x x (Lane 7) x x | y: 0
    /// -------------------|------------------
    /// ^                  ^                 ^
    /// (0;0)           (99;0)            (198;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------
    /// LaneGroup: 0            |    LaneGroup: 4
    /// Right LaneBoundary: 1   |    Right LaneBoundary: 5
    /// Left LaneBoundary: 2    |    Left LaneBoundary: 6
    /// Lane: 3                 |    Lane: 7
    /// ------------------------|--------------------------
    ///
    /// Note: Lanes are connected (predecessor, successor)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithTwoConnectedEastingLanesWithHundredPointsEach()
    {
        return MapWithTwoConnectedEastingLanes(LaneGroupOrder::kNatural);
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|------------------
    /// | x x (Lane 7) x x | x x (Lane 3) x x | y: 0
    /// -------------------|------------------
    /// ^                  ^                 ^
    /// (0;0)           (99;0)            (198;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------
    /// LaneGroup: 4            |    LaneGroup: 0
    /// Right LaneBoundary: 5   |    Right LaneBoundary: 1
    /// Left LaneBoundary: 6    |    Left LaneBoundary: 2
    /// Lane: 7                 |    Lane: 3
    /// ------------------------|--------------------------
    ///
    /// Note: Lanes are connected (predecessor, successor)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithTwoConnectedEastingLanesAndInvertedLaneGroupOrder()
    {
        return MapWithTwoConnectedEastingLanes(LaneGroupOrder::kInverted);
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|------------------|--------------------
    /// | x x (Lane 3) x x | x x (Lane 7) x x | x x (Lane 11) x x | y: 0
    /// -------------------|------------------|--------------------
    /// ^                 ^                   ^                   ^
    /// (0;0)           (99;0)             (198,0)             (297;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 0            |    LaneGroup: 4          |    LaneGroup: 8
    /// Right LaneBoundary: 1   |    Right LaneBoundary: 5 |    Right LaneBoundary: 9
    /// Left LaneBoundary: 2    |    Left LaneBoundary: 6  |    Left LaneBoundary: 10
    /// Lane: 3                 |    Lane: 7               |    Lane: 11
    /// ------------------------|--------------------------|--------------------------
    ///
    /// Note: Lanes are connected (predecessor, successor)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithThreeConnectedEastingLanesWithHundredPointsEach()
    {
        RawMapBuilder builder;
        mantle_api::Vec3<units::length::meter_t> origin{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        origin = {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        origin = {units::length::meter_t(198), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|------------------|--------------------|--------------------|--------------------
    /// | x x (Lane 3) x x | x x (Lane 7) x x | x x (Lane 11) x x  | x x (Lane 15) x x  | x x (Lane 19) x x | y = 0
    /// -------------------|------------------|--------------------|--------------------|--------------------
    /// ^                 ^                   ^                    ^                    ^                   ^
    /// (0;0)           (99;0)             (198,0)             (297;0)               (396;0)             (495;0)
    ///
    /// IDs:
    /// ------------------|-------------------|-------------------|--------------------|--------------------
    /// LaneGroup: 0      | LaneGroup: 4      | LaneGroup: 8      | LaneGroup: 12      | LaneGroup: 16
    /// Right Boundary: 1 | Right Boundary: 5 | Right Boundary: 9 | Right Boundary: 13 | Right Boundary: 17
    /// Left Boundary: 2  | Left Boundary: 6  | Left Boundary: 10 | Left Boundary: 14  | Left Boundary: 18
    /// Lane: 3           | Lane: 7           | Lane: 11          | Lane: 15           | Lane: 19
    /// ------------------|-------------------|-------------------|--------------------|--------------------
    ///
    /// Note: Lanes are connected (predecessor, successor)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithFiveConnectedEastingLanesWithHundredPointsEach()
    {
        RawMapBuilder builder;
        mantle_api::Vec3<units::length::meter_t> origin{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        origin = {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        origin = {units::length::meter_t(198), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        origin = {units::length::meter_t(297), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        origin = {units::length::meter_t(396), units::length::meter_t(0), units::length::meter_t(0)};
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origin));
        return builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|-------------------|--------------------
    /// | x x (Lane 8) x x | x x (Lane 18) x x | x x (Lane 28) x x | y: 8
    /// -------------------|-------------------|--------------------
    /// | x x (Lane 5) x x | x x (Lane 15) x x | x x (Lane 25) x x | y: 4
    /// -------------------|-------------------|--------------------
    /// | x x (Lane 2) x x | x x (Lane 12) x x | x x (Lane 22) x x | y: 0
    /// -------------------|-------------------|--------------------
    /// ^                  ^                   ^                   ^
    /// (0;0)            (99;0)             (198,0)             (297;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 9            |    LaneGroup: 19         |    LaneGroup: 29
    /// Right LaneBoundary: 6   |    Right LaneBoundary: 16|    Right LaneBoundary: 26
    /// Left LaneBoundary: 7    |    Left LaneBoundary: 17 |    Left LaneBoundary: 27
    /// Lane: 8                 |    Lane: 18              |    Lane: 28
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 9            |    LaneGroup: 19         |    LaneGroup: 29
    /// Right LaneBoundary: 3   |    Right LaneBoundary: 13|    Right LaneBoundary: 23
    /// Left LaneBoundary: 4    |    Left LaneBoundary: 14 |    Left LaneBoundary: 24
    /// Lane: 5                 |    Lane: 15              |    Lane: 25
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 9            |    LaneGroup: 19         |    LaneGroup: 29
    /// Right LaneBoundary: 0   |    Right LaneBoundary: 10|    Right LaneBoundary: 20
    /// Left LaneBoundary: 1    |    Left LaneBoundary: 11 |    Left LaneBoundary: 21
    /// Lane: 2                 |    Lane: 12              |    Lane: 22
    /// ------------------------|--------------------------|--------------------------
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection.
    ///
    /// Horizontal lanes are connected (predecessor, successor)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> Map3x3StraightEastingLanesWithHundredPoints()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanes(
            map_builder, {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanes(
            map_builder, {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanes(
            map_builder, {units::length::meter_t(198), units::length::meter_t(0), units::length::meter_t(0)});

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|-----------------\
    /// | x x (Lane 6) x x | x (L. 13) x \    \ y: 4
    /// -------------------|--------------\---|------------------
    /// | x x (Lane 3) x x | x x (Lane 9) x x | x (Lane 17) x x | y: 0
    /// -------------------|------------------|------------------
    /// ^                 ^                   ^                 ^
    /// (0;0)           (99;0)              (198;0)           (297;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------|
    /// LaneGroup: 0            |    LaneGroup: 7          |
    /// Right LaneBoundary: 4   |    Right LaneBoundary: 11|
    /// Left LaneBoundary: 5    |    Left LaneBoundary: 12 |
    /// Lane: 6                 |    Lane: 13              |
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 0            |    LaneGroup: 7          |    LaneGroup: 14
    /// Right LaneBoundary: 1   |    Right LaneBoundary: 8 |    Right LaneBoundary: 15
    /// Left LaneBoundary: 2    |    Left LaneBoundary: 9  |    Left LaneBoundary: 16
    /// Lane: 3                 |    Lane: 10              |    Lane: 17
    /// ------------------------|--------------------------|--------------------------
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection.
    ///
    /// Horizontal lanes are connected (predecessor, successor). Merging lane and lane 17 are connected, too.
    /// Lane 13 has a center line which "joins" the center line of lane 10.
    /// Thus the last points of lane 13 and 10 are equal.
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapMergeTwoLanesToTheRightIntoOneLane()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanes(
            map_builder, {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanesMergeToRight(
            map_builder, {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(
            map_builder, {units::length::meter_t(198), units::length::meter_t(0), units::length::meter_t(0)});

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// ------------------|------------------|-------------------
    /// | x (Lane 6)  x x | x (Lane 13)  x x | x (Lane 17)  x x | y: 4
    /// ------------------|--------------/---|-------------------
    /// | x (Lane 3) x x  | x (L. 10) x /   / y: 0              ^
    /// ------------------|----------------/                  (297;4)
    /// ^                 ^
    /// (0;0)           (99;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 0            |    LaneGroup: 7          |    LaneGroup: 14
    /// Right LaneBoundary: 4   |    Right LaneBoundary: 11|    Right LaneBoundary: 15
    /// Left LaneBoundary: 5    |    Left LaneBoundary: 12 |    Left LaneBoundary: 16
    /// Lane: 6                 |    Lane: 13              |    Lane: 17
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 0            |    LaneGroup: 7          |
    /// Right LaneBoundary: 1   |    Right LaneBoundary: 8 |
    /// Left LaneBoundary: 2    |    Left LaneBoundary: 9  |
    /// Lane: 3                 |    Lane: 10              |
    /// ------------------------|--------------------------|
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection.
    ///
    /// Horizontal lanes are connected (predecessor, successor). Merging lane and lane 17 are connected, too.
    /// Lane 10 has a center line which "joins" the center line of lane 13.
    /// Thus the last points of lane 10 and 13 are equal.
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapMergeTwoLanesToTheLeftIntoOneLane()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanes(
            map_builder, {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanesMergeToLeft(
            map_builder, {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(
            map_builder, {units::length::meter_t(198), units::length::meter_t(4), units::length::meter_t(0)});

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    ///                         /-------------|--------------------
    ///                        /  / x (L.9) x | x x (Lane 16) x x | y: 4
    /// -------------------|-----/------------|--------------------
    /// | x x (Lane 2) x x | x x (Lane 6) x x | x x (Lane 13) x x | y: 0
    /// -------------------|------------------|--------------------
    /// ^                  ^                  ^                   ^
    /// (0;0)            (99;0)            (198;0)             (297;0)
    ///
    /// IDs:
    ///                         |--------------------------|------------------------
    ///                         |    LaneGroup: 10         |    LaneGroup: 17
    ///                         |    Right LaneBoundary: 7 |    Right LaneBoundary: 14
    ///                         |    Left LaneBoundary: 8  |    Left LaneBoundary: 15
    ///                         |    Lane: 9               |    Lane: 16
    /// ------------------------|--------------------------|--------------------------
    /// LaneGroup: 0            |    LaneGroup: 10         |    LaneGroup: 17
    /// Right LaneBoundary: 0   |    Right LaneBoundary: 4 |    Right LaneBoundary: 11
    /// Left LaneBoundary: 1    |    Left LaneBoundary: 5  |    Left LaneBoundary: 12
    /// Lane: 2                 |    Lane: 6               |    Lane: 13
    /// ------------------------|--------------------------|--------------------------
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection.
    ///
    /// Lanes are connected (predecessor, successor). Splitting lane 9 and lane 16 are connected, too.
    /// Lane 6 has a center line which "begins" at the same position as lane 9.
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapSplitOneLaneToTheLeftIntoTwoLanes()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(
            map_builder, {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanesSplitToLeft(
            map_builder, {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanes(
            map_builder, {units::length::meter_t(198), units::length::meter_t(0), units::length::meter_t(0)});

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    ///                         /-------------|
    ///                        /  / x (L.9) x |
    /// -------------------|-----/------------|
    /// | x x (Lane 2) x x | x x (Lane 6) x x |
    /// -------------------|------------------|
    /// ^                  ^                  ^
    /// (0;0)            (99;0)            (198;0)
    ///
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapSplitOneLaneToTheLeftIntoTwoLanesEachWithoutSuccessors()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(
            map_builder, {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanesSplitToLeft(
            map_builder, {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)});

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|-------------------|--------------------
    /// | x x (Lane 2) x x | x x (Lane 9) x x  | x x (Lane 16) x x | y: 0
    /// -------------------|-----\-------------|--------------------
    ///                        \  \ x (L.6) x  | x x (Lane 13) x x | y: -4
    ///                         \--------------|--------------------
    ///                                        ^                   ^
    ///                                     (198;0)            (297;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------|------------------------
    /// LaneGroup: 3            |    LaneGroup: 10         |    LaneGroup: 17
    /// Right LaneBoundary: 0   |    Right LaneBoundary: 7 |    Right LaneBoundary: 14
    /// Left LaneBoundary: 1    |    Left LaneBoundary: 8  |    Left LaneBoundary: 15
    /// Lane: 2                 |    Lane: 9               |    Lane: 16
    /// ------------------------|--------------------------|--------------------------
    ///                         |    LaneGroup: 10         |    LaneGroup: 17
    ///                         |    Right LaneBoundary: 4 |    Right LaneBoundary: 11
    ///                         |    Left LaneBoundary: 5  |    Left LaneBoundary: 12
    ///                         |    Lane: 6               |    Lane: 13
    ///                         |--------------------------|--------------------------
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection.
    ///
    /// Lanes are connected (predecessor, successor). Splitting lane 6 and lane 2 are connected, too.
    /// Thus, lane 6 has a center line which "begins" at the same position as lane 9.
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapSplitOneLaneToTheRightIntoTwoLanes()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(
            map_builder, {units::length::meter_t(0), units::length::meter_t(4), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanesSplitToRight(
            map_builder, {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanes(
            map_builder, {units::length::meter_t(198), units::length::meter_t(0), units::length::meter_t(0)});

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// -------------------|-------------------|--------------------
    /// | x x (Lane 2) x x | x x (Lane 6) x x  | x x (Lane 13) x x | y: 0
    /// -------------------|-----\-------------|--------------------
    ///                        \  \ x (L.9) x  |                     y: -4
    ///                         \--------------|
    ///                                        ^                   ^
    ///                                     (198;0)            (297;0)
    ///
    /// IDs:
    /// ------------------------|--------------------------|------------------------
    /// LaneGroup: 3            |    LaneGroup: 10         |    LaneGroup: 14
    /// Right LaneBoundary: 0   |    Right LaneBoundary: 7 |    Right LaneBoundary: 11
    /// Left LaneBoundary: 1    |    Left LaneBoundary: 8  |    Left LaneBoundary: 12
    /// Lane: 2                 |    Lane: 9               |    Lane: 13
    /// ------------------------|--------------------------|--------------------------
    ///                         |    LaneGroup: 10         |
    ///                         |    Right LaneBoundary: 4 |
    ///                         |    Left LaneBoundary: 5  |
    ///                         |    Lane: 6               |
    ///                         |--------------------------|
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection.
    ///
    /// Lanes are connected (predecessor, successor). Splitting lane 6 and lane 2 are connected, too.
    /// Thus, lane 9 has a center line which "begins" at the same position as lane 6.
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapSplitOneLaneToTheRightIntoTwoLanesWithShortJunction()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(
            map_builder, {units::length::meter_t(0), units::length::meter_t(4), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanesSplitToRight(
            map_builder, {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)});
        LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(
            map_builder, {units::length::meter_t(198), units::length::meter_t(4), units::length::meter_t(0)});

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// --------------------
    /// | x x (Lane 5) x x |
    /// --------------------
    /// | x x (Lane 2) x x |
    /// --------------------
    /// ^                  ^
    /// (0;0)            (99;0)
    ///
    /// IDs:
    /// -------------------------
    /// LaneGroup: 6            |
    /// Right LaneBoundary: 1   |
    /// Left LaneBoundary: 4    |
    /// Lane: 5                 |
    /// -------------------------
    /// LaneGroup: 6            |
    /// Right LaneBoundary: 0   |
    /// Left LaneBoundary: 1    |
    /// Lane: 2                 |
    /// -------------------------
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection and share lane boundaries.
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithTwoParallelLanesWithSharedBoundary()
    {
        RawMapBuilder map_builder;

        LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanes(
            map_builder, {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)});
        auto astas_map = map_builder.Build();

        const auto& lane_group = astas_map->GetLaneGroups().front();
        const auto& right_lane = astas_map->GetLane(lane_group.lane_ids.front());
        const auto shared_boundary_id = right_lane.left_lane_boundaries.front();

        auto& left_lane = astas_map->GetLane(lane_group.lane_ids.back());
        auto redundant_boundary_id = left_lane.right_lane_boundaries.front();
        left_lane.right_lane_boundaries = {shared_boundary_id};

        auto& lane_group_boundaries = astas_map->GetLaneGroup(left_lane.parent_lane_group_id).lane_boundary_ids;
        lane_group_boundaries.erase(
            std::remove(lane_group_boundaries.begin(), lane_group_boundaries.end(), redundant_boundary_id),
            lane_group_boundaries.end());

        return astas_map;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// --------------------
    /// | x x (Lane 5) x x | Shoulder
    /// --------------------
    /// | x x (Lane 2) x x | Driving
    /// --------------------
    /// ^                  ^
    /// (0;0)            (99;0)
    ///
    /// IDs:
    /// -------------------------
    /// LaneGroup: 6            |
    /// Right LaneBoundary: 1   |
    /// Left LaneBoundary: 4    |
    /// Lane: 5                 |
    /// -------------------------
    /// LaneGroup: 6            |
    /// Right LaneBoundary: 0   |
    /// Left LaneBoundary: 1    |
    /// Lane: 2                 |
    /// -------------------------
    ///
    /// Note:
    /// Adjacent lanes have a bi-directional connection and share lane boundaries.
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithDrivingAndShoulderLaneWithSharedBoundary()
    {
        auto astas_map = MapWithTwoParallelLanesWithSharedBoundary();
        astas_map->FindLane(5)->flags.SetShoulderLane();

        return astas_map;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// ----------- < y = 2.5
    /// |x  x  x  | < y = 2.0
    /// ----|  x  |
    ///     |--x--|
    ///        ^
    ///       (0,0)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapOneLaneNorthingToWestingCurve()
    {
        mantle_api::Vec3<units::length::meter_t> not_yet_used_origin{
            units::length::meter_t(0), units::length::meter_t(4), units::length::meter_t(0)};

        RawMapBuilder map_builder;
        LaneGroupCatalogue::AddLaneGroupWithOneNorthingToWestingCurve(map_builder, not_yet_used_origin);

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// Contains a map with two lane groups. Each lane group has one lane.
    /// Each lane group represents a semi circle, like "[" and "]", forming more  or less a (not so round) circle
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> SimpleClosedLoopMap()
    {
        RawMapBuilder map_builder;
        LaneGroupCatalogue::AddLaneGroupWithRightSideCounterClockwiseSemiCircle(map_builder);
        LaneGroupCatalogue::AddLaneGroupWithLeftSideCounterClockwiseSemiCircle(map_builder);

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// x------------------x
    /// --------------------
    /// | x x (Lane 3) x x |
    /// --------------------
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapWithOneEastingLaneWithHundredPointsAndDoubleLeftBoundary()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> additional_left_boundary_coords{
            {units::length::meter_t(0), units::length::meter_t(10), units::length::meter_t(0)},
            {units::length::meter_t(99), units::length::meter_t(10), units::length::meter_t(0)}};
        mantle_api::Vec3<units::length::meter_t> origin{
            units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
        auto lane_and_boundaries = LaneCatalogue::StraightEastingLaneWithHundredPoints(origin);

        environment::map::LaneBoundary additional_left_boundary =
            BuildWhiteLineBoundary(additional_left_boundary_coords);
        lane_and_boundaries.first.left_lane_boundaries.push_back(additional_left_boundary.id);
        lane_and_boundaries.second.push_back(additional_left_boundary);

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(lane_and_boundaries);
        return builder.Build();
    }

    static std::unique_ptr<environment::map::AstasMap> MapOdrProblematicCurve()
    {
        mantle_api::Vec3<units::length::meter_t> not_yet_used_origin{
            units::length::meter_t(0), units::length::meter_t(4), units::length::meter_t(0)};

        RawMapBuilder map_builder;
        LaneGroupCatalogue::MapOdrProblematicCurve(map_builder, not_yet_used_origin);

        return map_builder.Build();
    }

    static std::unique_ptr<environment::map::AstasMap> MapOdrProblematicStraightLine()
    {
        mantle_api::Vec3<units::length::meter_t> not_yet_used_origin{
            units::length::meter_t(0), units::length::meter_t(4), units::length::meter_t(0)};

        RawMapBuilder map_builder;
        LaneGroupCatalogue::MapOdrProblematicStraightLine(map_builder, not_yet_used_origin);

        return map_builder.Build();
    }

    static std::unique_ptr<environment::map::AstasMap> MapOdrProblematicCurvyLine()
    {
        mantle_api::Vec3<units::length::meter_t> not_yet_used_origin{
            units::length::meter_t(0), units::length::meter_t(4), units::length::meter_t(0)};

        RawMapBuilder map_builder;
        LaneGroupCatalogue::MapOdrProblematicCurvyLine(map_builder, not_yet_used_origin);

        return map_builder.Build();
    }

    static std::unique_ptr<environment::map::AstasMap> MapOneLaneDuplicatedBoundariesOnEachSide()
    {
        mantle_api::Vec3<units::length::meter_t> not_yet_used_origin{
            units::length::meter_t(0), units::length::meter_t(4), units::length::meter_t(0)};

        RawMapBuilder map_builder;
        LaneGroupCatalogue::LaneWithSpatiallyIdenticalDuplicatedBoundaries(map_builder, not_yet_used_origin);

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    /// Counter Clockwise Circle Lanes
    /// outer lane radius : 102.0
    /// middle lane radius : 100.0
    /// inner lane radius : 98.0
    ///                             |
    ///          ___________________|___________________
    ///         /                   |                   \
    ///     (lane19)    ____________|____________    (lane9)
    ///       /        /            |            \        \
    ///      /     (lane16)    _____|_____     (lane6)     \
    ///     /        /        /     |     \        \        \
    ///    /        /     (lane13)  |  (lane3)      \        \
    ///   /        /       /        |       \        \        \
    ///---------------------------(0,0)-------------------------------
    ///   \        \       \        |       /        /        /
    ///    \        \    (lane23)   |   (lane33)    /        /
    ///     \        \       \______|_____/        /        /
    ///      \     (lane26)         |           (lane36)   /
    ///       \        \____________|_____________/       /
    ///     (lane29)                |                  (lane39)
    ///         \___________________|___________________/
    ///                             |
    ///                             |
    /// IDs:
    /// ----------------------|--------------------------|--------------------------|------------------------
    /// LaneGroup: 0   (NE)   |  LaneGroup: 10  (NW)     |  LaneGroup: 20  (SW)     |  LaneGroup: 30 (SE)
    /// Lane: 3               |  Lane: 13                |  Lane: 23                |  Lane: 33
    /// Right LaneBoundary: 1 |  Right LaneBoundary: 11  |  Right LaneBoundary: 21  |  Right LaneBoundary: 31
    /// Left LaneBoundary:  2 |  Left LaneBoundary:  12  |  Left LaneBoundary:  22  |  Left LaneBoundary:  32
    /// ----------------------|--------------------------|--------------------------|------------------------
    /// Lane: 6               |  Lane: 16                |  Lane: 26                |  Lane: 36
    /// Right LaneBoundary: 5 |  Right LaneBoundary: 14  |  Right LaneBoundary: 24  |  Right LaneBoundary: 34
    /// Left LaneBoundary:  4 |  Left LaneBoundary:  15  |  Left LaneBoundary:  25  |  Left LaneBoundary:  35
    /// ----------------------|--------------------------|--------------------------|------------------------
    /// Lane: 9               |  Lane: 19                |  Lane: 29                |  Lane: 39
    /// Right LaneBoundary: 8 |  Right LaneBoundary: 17  |  Right LaneBoundary: 27  |  Right LaneBoundary: 38
    /// Left LaneBoundary:  7 |  Left LaneBoundary:  18  |  Left LaneBoundary:  28  |  Left LaneBoundary:  37
    /// ----------------------|--------------------------|--------------------------|------------------------
    ///
    /// Note:
    /// Lanes have a bi-directional longitudinal connection (predecessor/successor).
    ///
    /// Circle lanes are connected, e.g. lane36 : (predecessors: lane26, successors: lane6)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> CircleClosedLoopMapWithFourLaneGroups()
    {

        RawMapBuilder map_builder;
        LaneGroupCatalogue::AddLaneGroupWithNorthEastCounterClockwiseQuarterCircle(map_builder);
        LaneGroupCatalogue::AddLaneGroupWithNorthWestCounterClockwiseQuarterCircle(map_builder);
        LaneGroupCatalogue::AddLaneGroupWithSouthWestCounterClockwiseQuarterCircle(map_builder);
        LaneGroupCatalogue::AddLaneGroupWithSouthEastCounterClockwiseQuarterCircle(map_builder);

        return map_builder.Build();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /// @verbatim
    ///
    ///
    /// --------------------
    /// | x x (Lane 3) x x |
    /// --------------------
    /// | x x (Lane 6) x x |
    /// --------------------
    /// | x x (Lane 9) x x |
    /// --------------------
    /// ^                  ^
    /// (-5.0, -10)      (-5.0, 1990)
    ///
    /// @endverbatim
    static std::unique_ptr<environment::map::AstasMap> MapStraightRoad2km()
    {
        RawMapBuilder map_builder;
        LaneGroupCatalogue::AddLaneGroupWithStraightRoad2km(map_builder);
        return map_builder.Build();
    }

  private:
    enum class LaneGroupOrder
    {
        kNatural,
        kInverted
    };

    static std::unique_ptr<environment::map::AstasMap> MapWithTwoConnectedEastingLanes(LaneGroupOrder ordering)
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> origins{
            {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)},
            {units::length::meter_t(99), units::length::meter_t(0), units::length::meter_t(0)},
        };

        if (ordering == LaneGroupOrder::kInverted)
        {
            std::reverse(origins.begin(), origins.end());
        }

        RawMapBuilder builder;
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origins[0]));
        builder.AddNewLaneGroup().AddFromCatalogue(LaneCatalogue::StraightEastingLaneWithHundredPoints(origins[1]));

        return builder.Build();
    }
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MAPCATALOGUE_H
