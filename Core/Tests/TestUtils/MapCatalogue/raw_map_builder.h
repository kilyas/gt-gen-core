/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWMAPBUILDER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWMAPBUILDER_H

#include "Core/Environment/Map/AstasMap/Internal/lane_utility.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/AstasMap/astas_map_finalizer.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Tests/TestUtils/MapCatalogue/lane_catalogue.h"
#include "Core/Tests/TestUtils/MapCatalogue/mock_coordinate_converter.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_lane_group_builder.h"

namespace astas::test_utils
{

class RawMapBuilder
{
  public:
    RawMapBuilder() { CreateDefaultMap(); }

    std::unique_ptr<environment::map::AstasMap> Build(
        environment::map::AstasMap::SourceMapType map_type = environment::map::AstasMap::SourceMapType::kUnknown)
    {
        astas_map_->SetSourceMapType(map_type);
        astas_map_->coordinate_converter = std::make_unique<test_utils::MockCoordinateConverter>();

        environment::map::AstasMapFinalizer finalizer(*astas_map_);
        finalizer.Finalize();
        return std::move(astas_map_);
    }

    RawMapBuilder& AddNewLaneGroup()
    {
        RawLaneGroupBuilder lane_group_builder;
        auto lane_group = lane_group_builder.Build();
        astas_map_->AddLaneGroup(lane_group);

        current_lane_group_id_ = lane_group.id;

        return *this;
    }

    RawMapBuilder& AddFromCatalogue(
        const std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>& lane_and_boundaries)
    {
        assert(current_lane_group_id_ != std::numeric_limits<mantle_api::UniqueId>::max() &&
               "AddNewLaneGroup() must be called first");

        auto new_lane = lane_and_boundaries.first;
        ConnectNewLaneIfOverlappingWithExisting(new_lane);
        astas_map_->AddLaneBoundaries(current_lane_group_id_, lane_and_boundaries.second);

        auto left_boundary = lane_and_boundaries.second.front().points;
        for (const auto& point : left_boundary)
        {
            environment::map::AddPointsToLanesPolygonAndUpdateBoundingBox(new_lane,
                                                                          {{point.position.x, point.position.y}});
        }

        auto right_boundary = lane_and_boundaries.second.back().points;
        std::reverse(std::begin(right_boundary), std::end(right_boundary));
        for (const auto& point : right_boundary)
        {
            environment::map::AddPointsToLanesPolygonAndUpdateBoundingBox(new_lane,
                                                                          {{point.position.x, point.position.y}});
        }

        astas_map_->AddLane(current_lane_group_id_, new_lane);
        return *this;
    }

    void AddStraightEastingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                 units::length::meter_t(0.0),
                                                 units::length::meter_t(0.0),
                                                 units::length::meter_t(0.0)})
    {
        auto lane_and_boundaries = LaneCatalogue::StraightEastingLaneWithTwoPoints(origin);
        AddFromCatalogue(lane_and_boundaries);
    }

    void StraightEastingLaneWithFivePoints()
    {
        auto lane_and_boundaries = LaneCatalogue::StraightEastingLaneWithFivePoints();
        AddFromCatalogue(lane_and_boundaries);
    }

    void StraightEastingLaneWithHundredPoints()
    {
        auto lane_and_boundaries = LaneCatalogue::StraightEastingLaneWithHundredPoints();
        AddFromCatalogue(lane_and_boundaries);
    }

    void AddLongStraightEastingLaneWithThreePoints()
    {
        auto lane_and_boundaries = LaneCatalogue::LongStraightEastingLaneWithThreePoints();
        AddFromCatalogue(lane_and_boundaries);
    }

    void Reset() { CreateDefaultMap(); }

    environment::map::Lane& GetLastAddedLaneHandle() { return astas_map_->GetLanes().back(); }

  protected:
    void CreateDefaultMap()
    {
        test_utils::MapCatalogueIdProvider::Instance().Reset();
        astas_map_ = std::make_unique<environment::map::AstasMap>();
    }

    void ConnectNewLaneIfOverlappingWithExisting(environment::map::Lane& new_lane)
    {
        for (auto& lane : astas_map_->GetLanes())
        {
            ConnectTwoLanes(lane, new_lane);
        }
    }

    void ConnectTwoLanes(environment::map::Lane& lane, environment::map::Lane& new_lane)
    {
        if (service::glmwrapper::Distance(lane.center_line.back(), new_lane.center_line.front()) < 0.01)
        {
            lane.successors.push_back(new_lane.id);
            new_lane.predecessors.push_back(lane.id);
        }

        if (service::glmwrapper::Distance(lane.center_line.front(), new_lane.center_line.back()) < 0.01)
        {
            lane.predecessors.push_back(new_lane.id);
            new_lane.successors.push_back(lane.id);
        }
    }

    enum class NeighbourType
    {
        kLeft,
        kRight,
        kNone
    };

    std::pair<environment::map::Lane*, NeighbourType> GetNeighborLane(
        const std::vector<environment::map::LaneBoundary>& new_boundaries)
    {
        for (const auto& new_boundary : new_boundaries)
        {
            for (const auto& existing_boundary : astas_map_->GetLaneBoundaries())
            {
                if (new_boundary.points == existing_boundary.points)
                {
                    for (auto& lane : astas_map_->GetLanes())
                    {
                        for (const auto& left_boundary_id : lane.left_lane_boundaries)
                        {
                            if (left_boundary_id == existing_boundary.id)
                            {
                                return {&lane, NeighbourType::kRight};
                            }
                        }

                        for (const auto& right_boundary_id : lane.right_lane_boundaries)
                        {
                            if (right_boundary_id == existing_boundary.id)
                            {
                                return {&lane, NeighbourType::kLeft};
                            }
                        }
                    }
                }
            }
        }
        return {nullptr, NeighbourType::kNone};
    }
    void ConnectLeftAndRightLanes(environment::map::Lane& left_lane, environment::map::Lane& right_lane)
    {
        left_lane.right_adjacent_lanes.push_back(right_lane.id);
        right_lane.left_adjacent_lanes.push_back(left_lane.id);
    }

    mantle_api::UniqueId current_lane_group_id_{std::numeric_limits<mantle_api::UniqueId>::max()};
    std::unique_ptr<environment::map::AstasMap> astas_map_{nullptr};
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWMAPBUILDER_H
