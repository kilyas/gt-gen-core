/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEBUILDER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEBUILDER_H

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_lane_boundary_builder.h"

namespace astas::test_utils
{

class RawLaneBuilder
{
  public:
    RawLaneBuilder() { CreateDefaultLane(); }

    void SetId(mantle_api::UniqueId id) { lane_->id = id; }
    void SetParentLaneGroupId(mantle_api::UniqueId id) { lane_->parent_lane_group_id = id; }

    void SetFlags(const environment::map::LaneFlags& flags) { lane_->flags = flags; }

    void SetCenterLine(const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line)
    {
        lane_->center_line = center_line;
    }

    void AddLeftBoundary(const mantle_api::UniqueId& id) { lane_->left_lane_boundaries.push_back(id); }

    void AddRightBoundary(const mantle_api::UniqueId& id) { lane_->right_lane_boundaries.push_back(id); }

    void AddLeftAdjacentLane(const mantle_api::UniqueId& id) { lane_->left_adjacent_lanes.push_back(id); }

    void AddRightAdjacentLane(const mantle_api::UniqueId& id) { lane_->right_adjacent_lanes.push_back(id); }

    void AddPredecessor(const mantle_api::UniqueId& id) { lane_->predecessors.push_back(id); }

    void AddSuccessor(const mantle_api::UniqueId& id) { lane_->successors.push_back(id); }

    void SetShape(const environment::map::Polygon2d& shape) { lane_->shape_2d = shape; }

    void SetBoundingBox(const environment::map::BoundingBox2d& bounding_box)
    {
        lane_->axis_aligned_bounding_box = bounding_box;
    }

    environment::map::Lane Build() { return *lane_; }

    void Reset() { CreateDefaultLane(); }

  protected:
    void CreateDefaultLane()
    {
        lane_ = std::make_unique<environment::map::Lane>(MapCatalogueIdProvider::Instance().GetNewId());
        lane_->flags.SetDrivable();
    }

    std::unique_ptr<environment::map::Lane> lane_{nullptr};
    double lane_width_{3};
};

inline environment::map::Lane BuildLaneWithBoundaries(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line,
    const std::vector<mantle_api::UniqueId>& left_boundary_ids,
    const std::vector<mantle_api::UniqueId>& right_boundary_ids)
{
    RawLaneBuilder lane_builder;
    lane_builder.SetCenterLine(center_line);

    for (const auto& id : left_boundary_ids)
    {
        lane_builder.AddLeftBoundary(id);
    }

    for (const auto& id : right_boundary_ids)
    {
        lane_builder.AddRightBoundary(id);
    }

    return lane_builder.Build();
}

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEBUILDER_H
