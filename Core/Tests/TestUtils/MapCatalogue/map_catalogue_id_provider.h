/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MAPCATALOGUEIDPROVIDER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MAPCATALOGUEIDPROVIDER_H

#include <MantleAPI/Common/i_identifiable.h>

namespace astas::test_utils
{
/// @todo: consider using the real one - but that one is not static and needs to be passed throu everywhere
class MapCatalogueIdProvider
{
  public:
    static MapCatalogueIdProvider& Instance()
    {
        static MapCatalogueIdProvider map_catalogue_id_provider;
        return map_catalogue_id_provider;
    }

    mantle_api::UniqueId GetNewId();
    void Reset();

  private:
    MapCatalogueIdProvider() = default;

    mantle_api::UniqueId id_{0};
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MAPCATALOGUEIDPROVIDER_H
