/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEGROUPBUILDER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEGROUPBUILDER_H

#include "Core/Environment/Map/AstasMap/lane_group.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_lane_builder.h"

namespace astas::test_utils
{

class RawLaneGroupBuilder
{
  public:
    RawLaneGroupBuilder() { CreateDefaultLaneGroup(); }

    void SetType(environment::map::LaneGroup::Type type) { lane_group_->type = type; }

    environment::map::LaneGroup Build() { return *lane_group_; }

    void Reset() { CreateDefaultLaneGroup(); }

  protected:
    void CreateDefaultLaneGroup()
    {
        lane_group_ = std::make_unique<environment::map::LaneGroup>(MapCatalogueIdProvider::Instance().GetNewId(),
                                                                    environment::map::LaneGroup::Type::kUnknown);
    }

    std::unique_ptr<environment::map::LaneGroup> lane_group_{nullptr};
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEGROUPBUILDER_H
