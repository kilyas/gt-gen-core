/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEBOUNDARYBUILDER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEBOUNDARYBUILDER_H

#include "Core/Environment/Map/AstasMap/lane_boundary.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue_id_provider.h"

#include <algorithm>
#include <memory>

namespace astas::test_utils
{

class RawLaneBoundaryBuilder
{
  public:
    RawLaneBoundaryBuilder() { CreateDefaultLaneBoundary(); }

    void SetId(mantle_api::UniqueId id) { boundary_->id = id; }
    void SetParentLaneGroupId(mantle_api::UniqueId id) { boundary_->parent_lane_group_id = id; }
    void SetMirroredFrom(mantle_api::UniqueId id) { boundary_->mirrored_from = id; }

    void SetType(environment::map::LaneBoundary::Type type) { boundary_->type = type; }
    void SetColor(environment::map::LaneBoundary::Color color) { boundary_->color = color; }
    void SetPoints(const environment::map::LaneBoundary::Points& points) { boundary_->points = points; }

    environment::map::LaneBoundary Build() { return *boundary_; }

    void Reset() { CreateDefaultLaneBoundary(); }

  protected:
    void CreateDefaultLaneBoundary()
    {
        boundary_ = std::make_unique<environment::map::LaneBoundary>(MapCatalogueIdProvider::Instance().GetNewId(),
                                                                     environment::map::LaneBoundary::Type::kUnknown,
                                                                     environment::map::LaneBoundary::Color::kUnknown);
    }

    std::unique_ptr<environment::map::LaneBoundary> boundary_{nullptr};
};

inline environment::map::LaneBoundary BuildWhiteLineBoundary(const environment::map::LaneBoundary::Points& points)
{
    RawLaneBoundaryBuilder boundary_lane_builder;
    boundary_lane_builder.SetColor(environment::map::LaneBoundary::Color::kWhite);
    boundary_lane_builder.SetType(environment::map::LaneBoundary::Type::kSolidLine);

    boundary_lane_builder.SetPoints(points);
    return boundary_lane_builder.Build();
}

inline environment::map::LaneBoundary BuildWhiteLineBoundary(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& coords)
{
    environment::map::LaneBoundary::Points points;
    std::transform(coords.begin(),
                   coords.end(),
                   std::back_inserter(points),
                   [](const mantle_api::Vec3<units::length::meter_t>& c) {
                       return environment::map::LaneBoundary::Point{c, 0, 0};
                   });

    return BuildWhiteLineBoundary(points);
}

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_RAWLANEBOUNDARYBUILDER_H
