/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MOCKCOORDINATECONVERTER_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MOCKCOORDINATECONVERTER_H

#include "Core/Environment/Map/Common/coordinate_converter.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/vector.h>
#include <gmock/gmock.h>

#include <memory>
#include <optional>
#include <variant>

namespace astas::test_utils
{

using UnderlyingMapCoordinate = std::variant<mantle_api::OpenDriveLanePosition, mantle_api::LatLonPosition>;

class MockCoordinateConverter
    : public environment::map::IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>
{
  public:
    std::optional<mantle_api::Vec3<units::length::meter_t>> Convert(
        const UnderlyingMapCoordinate& position) const override
    {
        // TODO: hack - UnderlyingMapCoordinate should really be of type mantle_api::Position
        if (std::holds_alternative<mantle_api::LatLonPosition>(position))
        {
            auto pos = std::get<mantle_api::LatLonPosition>(position);
            // This is not correct to use lat/lon in rad as x and y but ok for a mock
            return mantle_api::Vec3<units::length::meter_t>{units::length::meter_t(pos.latitude()),
                                                            units::length::meter_t(pos.longitude()),
                                                            units::length::meter_t(0.0)};
        }
        return mantle_api::Vec3<units::length::meter_t>{
            units::length::meter_t(0.0), units::length::meter_t(0.0), units::length::meter_t(0.0)};
    }

    MOCK_METHOD(std::optional<UnderlyingMapCoordinate>,
                Convert,
                (const mantle_api::Vec3<units::length::meter_t>& position),
                (const, override));
};

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_MOCKCOORDINATECONVERTER_H
