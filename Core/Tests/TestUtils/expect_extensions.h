/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_EXPECTEXTENSIONS_H
#define GTGEN_CORE_TESTS_TESTUTILS_EXPECTEXTENSIONS_H

#include "Core/Service/MantleApiExtension/formatting.h"
#include "astas_map.pb.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <fmt/format.h>
#include <gtest/gtest.h>

namespace astas
{

constexpr double orientation_accuracy = 1E-7;

///////////////////////////////////////////////////////////////////////////
// Customize-Section. Add you specializations here if you want to support a new struct with three fields
template <typename T>
inline auto MakeTuple(const T& vec3)
{
    return std::make_tuple(vec3.x, vec3.y, vec3.z, "x", "y", "z");
}

template <typename T>
inline auto MakeTuple(const mantle_api::Vec3<T>& vec3)
{
    return std::make_tuple(vec3.x(), vec3.y(), vec3.z(), "x", "y", "z");
}

inline auto MakeTuple(const messages::map::Vector3d& vec3)
{
    return std::make_tuple(vec3.x(), vec3.y(), vec3.z(), "x", "y", "z");
}

template <typename T>
inline auto MakeTuple(const mantle_api::Orientation3<T>& orientation3)
{
    return std::make_tuple(orientation3.yaw(), orientation3.pitch(), orientation3.roll(), "yaw", "pitch", "roll");
}

inline auto MakeTuple(const messages::map::Orientation3d& orientation3)
{
    return std::make_tuple(orientation3.yaw(), orientation3.pitch(), orientation3.roll(), "yaw", "pitch", "roll");
}

inline auto MakeTuple(const mantle_api::Dimension3& dimension3)
{
    return std::make_tuple(dimension3.width(), dimension3.length(), dimension3.height(), "width", "length", "height");
}

inline auto MakeTuple(const messages::map::Dimension3d& dimension3)
{
    return std::make_tuple(dimension3.width(), dimension3.length(), dimension3.height(), "width", "length", "height");
}
///////////////////////////////////////////////////////////////////////////

template <typename T>
inline constexpr auto Number(const T& val)
{
    return fmt::format("{: .8f}", val);
}

template <typename T>
inline constexpr auto PassOrFail(bool result, const T& diff)
{
    return result ? "PASS" : fmt::format("FAIL (diff: {:+.8f})", diff);
}

inline auto HeaderRow()
{
    return fmt::format(" {:^6} | {:^18} | {:^18} | {:^18}", "", "Expected", "Actual", "Result");
}

template <typename T>
inline constexpr auto ContentRow(const std::string& component, const T& val1, const T& val2, bool result)
{
    return fmt::format(
        " {:^6} | {:>18} | {:>18} | {:<18}", component, Number(val1), Number(val2), PassOrFail(result, val1 - val2));
}

template <typename T>
inline constexpr auto Table(const T& expected,
                            const T& actual,
                            bool equal_0,
                            bool equal_1,
                            bool equal_2,
                            double accuracy)
{
    return fmt::format("{}\n{}\n{}\n{}\nAccuracy: {}\nActual value: {:.8f}, {:.8f}, {:.8f}",
                       HeaderRow(),
                       ContentRow(std::get<3>(expected), std::get<0>(expected), std::get<0>(actual), equal_0),
                       ContentRow(std::get<4>(expected), std::get<1>(expected), std::get<1>(actual), equal_1),
                       ContentRow(std::get<5>(expected), std::get<2>(expected), std::get<2>(actual), equal_2),
                       accuracy,
                       std::get<0>(actual),
                       std::get<1>(actual),
                       std::get<2>(actual));
}

template <typename E, typename A>
inline constexpr auto ExpectNear(const E& expected, const A& actual, double accuracy)
{
    auto expected_tuple = MakeTuple(expected);
    auto actual_tuple = MakeTuple(actual);
    bool equal_0 = mantle_api::AlmostEqual(std::get<0>(expected_tuple), std::get<0>(actual_tuple), accuracy, true);
    bool equal_1 = mantle_api::AlmostEqual(std::get<1>(expected_tuple), std::get<1>(actual_tuple), accuracy, true);
    bool equal_2 = mantle_api::AlmostEqual(std::get<2>(expected_tuple), std::get<2>(actual_tuple), accuracy, true);

    if (!equal_0 || !equal_1 || !equal_2)
    {
        return Table(expected_tuple, actual_tuple, equal_0, equal_1, equal_2, accuracy);
    }
    return std::string{};
}

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - must be a macro, otherwise you cannot click-jump to the location
#define EXPECT_TRIPLE_NEAR(expected, actual, accuracy)                      \
    {                                                                       \
        std::string error_message = ExpectNear(expected, actual, accuracy); \
        if (!error_message.empty())                                         \
        {                                                                   \
            GTEST_NONFATAL_FAILURE_("EXPECT_TRIPLE_NEAR") << error_message; \
        }                                                                   \
    }

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - must be a macro, otherwise you cannot click-jump to the location
#define EXPECT_TRIPLE(expected, actual) EXPECT_TRIPLE_NEAR(expected, actual, 1E-4)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - must be a macro, otherwise you cannot click-jump to the location
#define EXPECT_TRIPLE_ZERO(actual) EXPECT_TRIPLE(decltype(actual){}, actual)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage) - must be a macro, otherwise you cannot click-jump to the location
#define EXPECT_ENUM_EQ(expected, actual) \
    EXPECT_EQ(expected, actual) << fmt::format("Expected: {} but is actually: {}", expected, actual);

}  // namespace astas

#endif  // GTGEN_CORE_TESTS_TESTUTILS_EXPECTEXTENSIONS_H
