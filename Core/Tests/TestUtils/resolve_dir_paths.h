/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_RESOLVEDIRPATHS_H
#define GTGEN_CORE_TESTS_TESTUTILS_RESOLVEDIRPATHS_H

#include <string>

namespace astas::test_utils
{

std::string ResolveDirPath(const std::string& path);

}  // namespace astas::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_RESOLVEDIRPATHS_H
