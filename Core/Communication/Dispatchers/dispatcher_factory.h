/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_DISPATCHERS_DISPATCHERFACTORY_H
#define GTGEN_CORE_COMMUNICATION_DISPATCHERS_DISPATCHERFACTORY_H

#include "Core/Communication/Common/i_dispatcher.h"

#include <functional>
#include <memory>
#include <string>

namespace astas::communication
{

/// @brief Generic factory for any kind of IDispatcher (IDispatcher until refactored)
class DispatcherFactory
{
  public:
    /// @param create_function Generator lambda that is executed by Create()
    /// @param dispatcher_purpose e.g. SimulationController, used for logging
    DispatcherFactory(std::function<std::unique_ptr<IDispatcher>()> create_function, std::string dispatcher_purpose);

    /// @return Fully constructed and usable IDispatcher
    std::unique_ptr<IDispatcher> Create() const;

    /// @return Purpose description, e.g. SimulationController, used for logging
    std::string GetDispatcherPurpose() const { return purpose_; };

  private:
    std::function<std::unique_ptr<IDispatcher>()> create_function_{nullptr};

    std::string purpose_{};
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_DISPATCHERS_DISPATCHERFACTORY_H
