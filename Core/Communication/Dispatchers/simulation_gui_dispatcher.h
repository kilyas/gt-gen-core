/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_DISPATCHERS_SIMULATIONGUIDISPATCHER_H
#define GTGEN_CORE_COMMUNICATION_DISPATCHERS_SIMULATIONGUIDISPATCHER_H

#include "Core/Communication/Common/i_dispatcher.h"
#include "Core/Communication/Dispatchers/conversion_request_processor.h"
#include "Core/Communication/Serialization/message_converter.h"
#include "Core/Environment/Chunking/world_chunk.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Service/FileSystem/filesystem.h"

#include <MantleAPI/Execution/i_environment.h>
#include <MantleAPI/Traffic/i_entity_repository.h>

#include <functional>
#include <memory>
#include <mutex>

namespace astas::communication::statemachine
{
struct GuiConnectionState;
}

namespace astas::communication
{

/// @brief Struct that collects data required for simulation visualization
struct VisualizationData
{
    VisualizationData(const environment::map::AstasMap& astas_map,
                      const service::gui::UiDataProvider& ui_data_provider,
                      const environment::proto_groundtruth::SensorViewBuilder& the_sensor_view_builder,
                      mantle_api::IEntityRepository* entity_repository)
        : map{astas_map},
          ui_data{ui_data_provider},
          sensor_view_builder{the_sensor_view_builder},
          repo{entity_repository}
    {
    }

    const environment::map::AstasMap& map;
    const service::gui::UiDataProvider& ui_data;
    const environment::proto_groundtruth::SensorViewBuilder& sensor_view_builder;
    mantle_api::IEntityRepository* repo{nullptr};
    environment::chunking::StaticChunkList static_chunks;
    service::user_settings::UserSettings settings;
    fs::path user_settings_file_path;
    std::uint16_t sim_control_port{0};
};

class SimulationGuiDispatcher final : public IDispatcher
{
  public:
    explicit SimulationGuiDispatcher(VisualizationData visualization_data,
                                     std::shared_ptr<mantle_api::IEnvironment> environment);

    SimulationGuiDispatcher(const SimulationGuiDispatcher&) = delete;
    SimulationGuiDispatcher(SimulationGuiDispatcher&&) = delete;
    SimulationGuiDispatcher& operator=(const SimulationGuiDispatcher&) = delete;
    SimulationGuiDispatcher& operator=(SimulationGuiDispatcher&&) = delete;

    ~SimulationGuiDispatcher() override;

    /// @copydoc IDispatcher::Initialize()
    void Initialize(std::function<void(std::unique_ptr<MessageContainer>)> async_send) override;

    /// @copydoc IDispatcher::ProcessMessage()
    bool ProcessMessage(const messages::gui::MessageWrapper& received_message) noexcept override;

    /// @copydoc IDispatcher::ShutDown()
    void ShutDown() override;

  private:
    VisualizationData visualization_data_;
    std::shared_ptr<mantle_api::IEnvironment> env_{nullptr};

    /// Actions
    void AcknowledgeConnection();
    void SendPing();

    void SendScenarioData();
    void SendGtAndAdditionalUiData();
    bool SetValue(const std::string& name, const std::string& value);

    bool HandleConversionRequest(const messages::editor::EditorApi& editor_request);

    MessageConverter message_converter_{};
    std::unique_ptr<statemachine::GuiConnectionState> connection_state_;

    std::unique_ptr<ConversionRequestProcessor> map_conversion_request_processor_{nullptr};

    std::function<void(std::unique_ptr<MessageContainer>)> async_send_;

    bool initialized_{false};
    std::mutex mutex_{};
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_DISPATCHERS_SIMULATIONGUIDISPATCHER_H
