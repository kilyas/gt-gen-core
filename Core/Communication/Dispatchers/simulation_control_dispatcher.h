/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_DISPATCHERS_SIMULATIONCONTROLDISPATCHER_H
#define GTGEN_CORE_COMMUNICATION_DISPATCHERS_SIMULATIONCONTROLDISPATCHER_H

#include "Core/Communication/Common/i_dispatcher.h"
#include "Core/Communication/Serialization/message_converter.h"

#include <functional>
#include <memory>
#include <mutex>

namespace astas::communication
{

class SimulationControlDispatcher final : public IDispatcher
{
    using Command = std::function<void()>;

  public:
    SimulationControlDispatcher(Command pause, Command resume, Command step, Command abort);

    /// @copydoc IDispatcher::Initialize()
    void Initialize(std::function<void(std::unique_ptr<MessageContainer>)> async_send) override;

    /// @copydoc IDispatcher::ProcessMessage()
    bool ProcessMessage(const messages::gui::MessageWrapper& received_message) noexcept override;

    /// @copydoc IDispatcher::ShutDown()
    void ShutDown() override;

  private:
    /// Actions
    void AcknowledgeConnection();
    void AcknowledgeCommand(messages::gui::MessageType ack_type);

    MessageConverter message_converter_{};

    std::function<void(std::unique_ptr<MessageContainer>)> async_send_{nullptr};

    Command pause_{nullptr};
    Command resume_{nullptr};
    Command step_{nullptr};
    Command abort_{nullptr};

    bool initialized_{false};
    std::mutex mutex_{};
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_DISPATCHERS_SIMULATIONCONTROLDISPATCHER_H
