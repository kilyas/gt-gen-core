/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/connection_state.h"

#include <gtest/gtest.h>

namespace astas::communication::statemachine
{

class GuiConnectionStateTest : public testing::Test
{
  public:
    GuiConnectionStateTest()
    {
        state_machine_.ping = [] {};
        state_machine_.core_ack = [] {};
        state_machine_.send_ground_truth = [] {};
        state_machine_.send_scenario_data = [] {};

        state_machine_.initiate();
    }

  protected:
    GuiConnectionState state_machine_{};
};

TEST_F(GuiConnectionStateTest, GivenNotConnected_WhenReceivedAreYouCore_ThenConnected)
{
    state_machine_.process_event(statemachine::ReceivedAreYouCore{});
    EXPECT_TRUE(state_machine_.connected);
}

TEST_F(GuiConnectionStateTest, GivenNotConnected_WhenTransitionToAwaitingRequests_ThenCoreAckCalled)
{
    bool core_ack_called{false};

    state_machine_.core_ack = [&core_ack_called] { core_ack_called = true; };

    // transition to AwaitingRequests
    state_machine_.process_event(statemachine::ReceivedAreYouCore{});

    EXPECT_TRUE(core_ack_called);
}

TEST_F(GuiConnectionStateTest, GivenAwaitingRequestsState_WhenReceivedPing_ThenPingCallbackExecuted)
{
    bool ping_called{false};
    state_machine_.ping = [&ping_called] { ping_called = true; };

    // get into AwaitingRequests state
    state_machine_.process_event(statemachine::ReceivedAreYouCore{});

    // send the ping event
    state_machine_.process_event(statemachine::ReceivedPing{});

    EXPECT_TRUE(ping_called);
}

TEST_F(GuiConnectionStateTest, GivenAwaitingRequestsState_WhenReceivedGiveScen_ThenSendScenarioDataExecuted)
{
    bool send_data_called{false};
    state_machine_.send_scenario_data = [&send_data_called] { send_data_called = true; };

    // get into AwaitingRequests state
    state_machine_.process_event(statemachine::ReceivedAreYouCore{});

    // send the event
    state_machine_.process_event(statemachine::ReceivedScenarioDataRequest{});

    EXPECT_TRUE(send_data_called);
}

TEST_F(GuiConnectionStateTest, GivenAwaitingRequestsState_WhenReceivedGtRequest_ThenSendGroundTruthExecuted)
{
    bool send_gt_called{false};
    state_machine_.send_ground_truth = [&send_gt_called] { send_gt_called = true; };

    // get into AwaitingRequests state
    state_machine_.process_event(statemachine::ReceivedAreYouCore{});

    // send the event
    state_machine_.process_event(statemachine::ReceivedGTRequest{});

    EXPECT_TRUE(send_gt_called);
}

TEST_F(GuiConnectionStateTest, GivenVisualizingState_WhenReceivedGtRequest_ThenSendGroundTruthExecuted)
{
    int num_send_gt_calls{0};
    state_machine_.send_ground_truth = [&num_send_gt_calls] { num_send_gt_calls++; };

    // get into Visualizing state
    state_machine_.process_event(statemachine::ReceivedAreYouCore{});
    state_machine_.process_event(statemachine::ReceivedGTRequest{});

    // send Gt event again
    state_machine_.process_event(statemachine::ReceivedGTRequest{});

    EXPECT_EQ(num_send_gt_calls, 2);
}

TEST_F(GuiConnectionStateTest, GivenVisualizingState_WhenReceivedPing_ThenPingCallbackExecuted)
{
    bool ping_called{false};
    state_machine_.ping = [&ping_called] { ping_called = true; };

    // get into Visualizing state
    state_machine_.process_event(statemachine::ReceivedAreYouCore{});
    state_machine_.process_event(statemachine::ReceivedGTRequest{});

    // send the ping event
    state_machine_.process_event(statemachine::ReceivedPing{});

    EXPECT_TRUE(ping_called);
}

}  // namespace astas::communication::statemachine
