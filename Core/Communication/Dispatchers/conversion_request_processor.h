/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_DISPATCHERS_CONVERSIONREQUESTPROCESSOR_H
#define GTGEN_CORE_COMMUNICATION_DISPATCHERS_CONVERSIONREQUESTPROCESSOR_H

#include "Core/Communication/Serialization/message_converter.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "editor_api.pb.h"

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/vector.h>

#include <functional>
#include <memory>
#include <optional>
#include <string>

namespace astas::communication
{

class ConversionRequestProcessor
{

  public:
    /// @brief Handles requests for map<->world coordinate/pose conversions of the ScenarioEditorSession
    /// @param converter Map converter with already converted map
    /// @param async_send Callback to send messages
    ConversionRequestProcessor(const environment::map::AstasMap& map,
                               std::function<void(std::unique_ptr<MessageContainer>)> async_send);

    /// @brief Performs requested conversion from odr/nds position to world pose and sends a reply
    /// @return If the message has been processed. Also true if the conversion failed (then the error_message
    /// payload is set). Only false if the request was malformed or not supported.
    bool ProcessWorldPoseRequest(const messages::editor::EditorApi& editor_request_wrapper);

    /// @brief Performs requested conversion from world position to odr/nds position and sends a reply
    /// @return If the message has been processed. Also true if the conversion failed (then the error_message
    /// payload is set). Only false if the request was malformed or not supported.
    bool ProcessNativeMapPositionRequest(const messages::editor::EditorApi& editor_request_wrapper);

  private:
    void SendConvertedWorldPose(const mantle_api::Pose& world_pose);
    void SendConvertedNativeMapPosition(const mantle_api::Position& position);
    void SendErrorResponse(const std::string& error_message);

    std::optional<mantle_api::Pose> ConvertToWorldPose(const mantle_api::Position& coordinate,
                                                       std::string& error_message,
                                                       bool is_nds);

    template <class T>
    bool ProcessNativeMapPositionRequest(const messages::editor::EditorApi& editor_request_wrapper);

    template <class T>
    std::optional<T> ConvertToMapPosition(const mantle_api::Vec3<units::length::meter_t>& position,
                                          std::string& error_message);

    MessageConverter message_converter_{};
    const environment::map::AstasMap& map_;
    environment::map::LaneLocationProvider lane_location_provider_;

    std::function<void(std::unique_ptr<MessageContainer>)> async_send_{nullptr};
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_DISPATCHERS_CONVERSIONREQUESTPROCESSOR_H
