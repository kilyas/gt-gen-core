/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/simulation_control_dispatcher.h"

#include <gtest/gtest.h>

#include <memory>

namespace astas::communication
{

class SimulationControlDispatcherTest : public testing::Test
{
  protected:
    SimulationControlDispatcherTest()
        : dispatcher_{[this] { pause_called_ = true; },
                      [this] { resume_called_ = true; },
                      [this] { step_called_ = true; },
                      [this] { abort_called_ = true; }}
    {
        fake_async_send_ = [this]([[maybe_unused]] std::unique_ptr<MessageContainer> msg) {
            async_send_called_ = true;
            sent_msg_type_ = msg->message_wrapper.message_type_identifier();
        };

        dispatcher_.Initialize(fake_async_send_);
    }

    bool pause_called_{false};
    bool resume_called_{false};
    bool step_called_{false};
    bool abort_called_{false};

    std::function<void(std::unique_ptr<MessageContainer>)> fake_async_send_;
    bool async_send_called_{false};
    messages::gui::MessageType sent_msg_type_{};

    SimulationControlDispatcher dispatcher_;
};

TEST_F(SimulationControlDispatcherTest,
       SessionInitialized_ProcessSupportedMessage_AsyncSendCalledAndConnectionAcknowledged)
{
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::AREUCORE);
    EXPECT_TRUE(dispatcher_.ProcessMessage(message_wrapper));
    EXPECT_TRUE(async_send_called_);
    EXPECT_EQ(sent_msg_type_, messages::gui::ICONTROL);
}

TEST_F(SimulationControlDispatcherTest, GivenSessionInitialized_WhenProcessUnsupportedMessage_ThenNothingSent)
{
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::GROUNDTR);
    EXPECT_FALSE(dispatcher_.ProcessMessage(message_wrapper));
    EXPECT_FALSE(async_send_called_);
}

TEST_F(SimulationControlDispatcherTest,
       GivenSessionInitialized_WhenProcessPauseRequest_ThenPauseCalledAndCommandAcknowledged)
{
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::PAUSESIM);
    EXPECT_TRUE(dispatcher_.ProcessMessage(message_wrapper));
    EXPECT_TRUE(pause_called_);
    EXPECT_EQ(sent_msg_type_, messages::gui::PAUSESIM);
}

TEST_F(SimulationControlDispatcherTest,
       GivenSessionInitialized_WhenProcessResumeRequest_ThenResumeCalledAndCommandAcknowledged)
{
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::RESUMSIM);
    EXPECT_TRUE(dispatcher_.ProcessMessage(message_wrapper));
    EXPECT_TRUE(resume_called_);
    EXPECT_EQ(sent_msg_type_, messages::gui::RESUMSIM);
}

TEST_F(SimulationControlDispatcherTest,
       GivenSessionInitialized_WhenProcessStepRequest_ThenStepCalledAndCommandAcknowledged)
{
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::STEPSIMU);
    EXPECT_TRUE(dispatcher_.ProcessMessage(message_wrapper));
    EXPECT_TRUE(step_called_);
    EXPECT_EQ(sent_msg_type_, messages::gui::STEPSIMU);
}

TEST_F(SimulationControlDispatcherTest,
       GivenSessionInitialized_WhenProcessEndOfSimRequest_ThenAbortCalledAndCommandAcknowledged)
{
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::ENDOFSIM);
    EXPECT_TRUE(dispatcher_.ProcessMessage(message_wrapper));
    EXPECT_TRUE(abort_called_);
    EXPECT_EQ(sent_msg_type_, messages::gui::ENDOFSIM);
}

TEST(SimulationControlSessionDeathTest, GivenSessionConstructedWithNullCallback_WhenInitializeCalled_ThenAssertionHit)
{
    SimulationControlDispatcher dispatcher{nullptr, nullptr, nullptr, nullptr};
    EXPECT_DEATH(dispatcher.Initialize(nullptr), "");
}

TEST(SimulationControlUninitializedDispatcherTest,
     SessionConstructedButNotInitialized_ProcessMessageCalled_JustReturnsFalse)
{
    SimulationControlDispatcher dispatcher{nullptr, nullptr, nullptr, nullptr};
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::AREUCORE);
    EXPECT_FALSE(dispatcher.ProcessMessage(message_wrapper));
}

}  // namespace astas::communication
