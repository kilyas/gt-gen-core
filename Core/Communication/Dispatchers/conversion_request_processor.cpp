/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/conversion_request_processor.h"

#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/exceptions.h"

namespace astas::communication
{

using ProtoEditorApi = messages::editor::EditorApi;

ConversionRequestProcessor::ConversionRequestProcessor(
    const environment::map::AstasMap& map,
    std::function<void(std::unique_ptr<MessageContainer>)> async_send)
    : map_{map}, lane_location_provider_{map}, async_send_{std::move(async_send)}
{
}

bool ConversionRequestProcessor::ProcessWorldPoseRequest(const ProtoEditorApi& editor_request_wrapper)
{
    bool is_nds{false};
    mantle_api::Position provided_pos;
    if (editor_request_wrapper.payload_case() == ProtoEditorApi::PayloadCase::kOdrPosition)
    {
        const auto& proto_pos = editor_request_wrapper.odr_position();
        mantle_api::OpenDriveLanePosition odr_pos;
        odr_pos.s_offset = units::length::meter_t(proto_pos.s_offset());
        odr_pos.t_offset = units::length::meter_t(proto_pos.t_offset());
        odr_pos.road = std::to_string(proto_pos.road_id());
        odr_pos.lane = proto_pos.lane_id();
        provided_pos = odr_pos;
    }
    else if (editor_request_wrapper.payload_case() == ProtoEditorApi::PayloadCase::kLatLonPosition)
    {
        const auto& proto_pos = editor_request_wrapper.lat_lon_position();
        mantle_api::LatLonPosition nds_pos;
        nds_pos.latitude = units::angle::degree_t(proto_pos.latitude());
        nds_pos.longitude = units::angle::degree_t(proto_pos.longitude());
        provided_pos = nds_pos;
        is_nds = true;
    }
    else
    {
        Error("Received EditorApi request without valid payload! Looks like an error in the ScenarioEditor.");
        return false;
    }

    std::string error_message{};
    const auto world_pose = ConvertToWorldPose(provided_pos, error_message, is_nds);
    if (world_pose)
    {
        SendConvertedWorldPose(world_pose.value());
    }
    else
    {
        SendErrorResponse(error_message);
    }
    return true;
}

std::optional<mantle_api::Pose> ConversionRequestProcessor::ConvertToWorldPose(const mantle_api::Position& coordinate,
                                                                               std::string& error_message,
                                                                               const bool is_nds)
{
    std::optional<mantle_api::Pose> result{std::nullopt};
    try
    {
        mantle_api::Pose pose{};
        auto converted_position =
            map_.coordinate_converter->Convert(environment::map::GetUnderlyingMapCoordinate(coordinate));
        if (!converted_position)
        {
            error_message = fmt::format("Couldn't find a valid world position for map coordinate {}", coordinate);
            return std::nullopt;
        }

        pose.position = *converted_position;
        const auto lane_location = lane_location_provider_.GetLaneLocation(pose.position);
        if (lane_location.IsValid())
        {
            pose.orientation = lane_location.GetOrientation();

            if (is_nds)  // nds converter doesn't provide z ...
            {
                pose.position.z = lane_location.projected_centerline_point.z;
            }
        }
        result = pose;
    }
    catch (const service::utility::exception::AstasException& exception)
    {
        // The converters return optionals to not have exception on failed position conversions, so
        // if there was still an exception, it probably shouldn't be there!
        error_message = fmt::format(
            "GTGen Core throw an exception during conversion of coordinate {}, please report to GTGen support! "
            "Exception: {}",
            coordinate,
            exception.what());
    }
    return result;
}

bool ConversionRequestProcessor::ProcessNativeMapPositionRequest(
    const messages::editor::EditorApi& editor_request_wrapper)
{
    const auto requested_type = editor_request_wrapper.request();
    if (requested_type == messages::editor::EDITOR_REQUEST_GET_ODR_POSITION)
    {
        return ProcessNativeMapPositionRequest<mantle_api::OpenDriveLanePosition>(editor_request_wrapper);
    }
    else if (requested_type == messages::editor::EDITOR_REQUEST_GET_NDS_POSITION)
    {
        return ProcessNativeMapPositionRequest<mantle_api::LatLonPosition>(editor_request_wrapper);
    }

    Error("Received not supported native map conversion request.");
    return false;
}

template <class T>
bool ConversionRequestProcessor::ProcessNativeMapPositionRequest(
    const messages::editor::EditorApi& editor_request_wrapper)
{
    if (editor_request_wrapper.payload_case() != messages::editor::EditorApi::PayloadCase::kWorldPose)
    {
        Error(
            "Received EditorApi request for native map position without world pose in payload! Looks like an error "
            "in the ScenarioEditor.");
        return false;
    }

    const auto& proto_pos = editor_request_wrapper.world_pose().position();
    const mantle_api::Vec3<units::length::meter_t> world_pos{units::length::meter_t(proto_pos.x()),
                                                             units::length::meter_t(proto_pos.y()),
                                                             units::length::meter_t(proto_pos.z())};

    std::string error_message{};
    const auto converted_pos = ConvertToMapPosition<T>(world_pos, error_message);
    if (converted_pos)
    {
        SendConvertedNativeMapPosition(converted_pos.value());
    }
    else
    {
        SendErrorResponse(error_message);
    }
    return true;
}

template <class T>
std::optional<T> ConversionRequestProcessor::ConvertToMapPosition(
    const mantle_api::Vec3<units::length::meter_t>& position,
    std::string& error_message)
{
    std::optional<T> result{std::nullopt};
    try
    {
        auto conversion_result = map_.coordinate_converter->Convert(position);

        if (conversion_result)
        {
            result = std::get<T>(conversion_result.value());
        }
        else
        {
            error_message = fmt::format("Couldn't find a valid map position for world coordinate {}", position);
        }
    }
    catch (const service::utility::exception::AstasException& exception)
    {
        // The converters return optionals to not have exception on failed position conversions, so
        // if there was still an exception, it probably shouldn't be there!
        error_message = fmt::format(
            "GTGen Core throw an exception during conversion of coordinate {}, please report to GTGen support! "
            "Exception: {}",
            position,
            exception.what());
    }
    return result;
}

void ConversionRequestProcessor::SendConvertedWorldPose(const mantle_api::Pose& world_pose)
{
    auto message = message_converter_.CreateMessage<EditorApiResponse>(world_pose);
    async_send_(std::move(message));
}

void ConversionRequestProcessor::SendConvertedNativeMapPosition(const mantle_api::Position& position)
{
    auto message = message_converter_.CreateMessage<EditorApiResponse>(position);
    async_send_(std::move(message));
}

void ConversionRequestProcessor::SendErrorResponse(const std::string& error_message)
{
    auto message = message_converter_.CreateMessage<EditorApiResponse>(error_message);
    async_send_(std::move(message));
}

}  // namespace astas::communication
