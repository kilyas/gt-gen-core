/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/simulation_control_dispatcher.h"

#include "Core/Service/Logging/logging.h"

namespace astas::communication
{

SimulationControlDispatcher::SimulationControlDispatcher(Command pause, Command resume, Command step, Command abort)
    : pause_{std::move(pause)}, resume_{std::move(resume)}, step_{std::move(step)}, abort_{std::move(abort)}
{
}

void SimulationControlDispatcher::Initialize(std::function<void(std::unique_ptr<MessageContainer>)> async_send)
{
    ASSERT(pause_ && resume_ && step_ && abort_ && "One of the SimulationControl callbacks is null!")

    async_send_ = async_send;
    initialized_ = true;
}

bool SimulationControlDispatcher::ProcessMessage(const messages::gui::MessageWrapper& received_message) noexcept
{
    std::scoped_lock lock(mutex_);

    if (!initialized_)
    {
        Warn("Tried to progress message before dispatcher initialization or after shutdown! Will just return.");
        return false;
    }

    const auto msg_type = received_message.message_type_identifier();
    switch (msg_type)
    {
        case messages::gui::MessageType::AREUCORE:
        {
            AcknowledgeConnection();
            return true;
        }
        case messages::gui::MessageType::PAUSESIM:
        {
            Info("Pausing simulation.");
            pause_();
            AcknowledgeCommand(msg_type);
            return true;
        }
        case messages::gui::MessageType::STEPSIMU:
        {
            Info("Do simulation step.");
            step_();
            AcknowledgeCommand(msg_type);
            return true;
        }
        case messages::gui::MessageType::RESUMSIM:
        {
            Info("Resuming simulation.");
            resume_();
            AcknowledgeCommand(msg_type);
            return true;
        }
        case messages::gui::MessageType::ENDOFSIM:
        {
            Info("Aborting simulation.");
            abort_();
            AcknowledgeCommand(msg_type);
            return true;
        }
        default:
        {
            return false;
        }
    }
}

void SimulationControlDispatcher::AcknowledgeConnection()
{
    const auto pid = getpid();
    auto message = message_converter_.CreateMessage<ControllerConnectionAcknowledgement>(pid);
    async_send_(std::move(message));
}

void SimulationControlDispatcher::AcknowledgeCommand(const messages::gui::MessageType ack_type)
{
    auto message = message_converter_.CreateMessage<CommandAcknowledgement>(ack_type);
    async_send_(std::move(message));
}

void SimulationControlDispatcher::ShutDown()
{
    std::scoped_lock lock(mutex_);
    initialized_ = false;
}

}  // namespace astas::communication
