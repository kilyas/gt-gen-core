/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_DISPATCHERS_THREADSAFEGROUNDTRUTHWRAPPER_H
#define GTGEN_CORE_COMMUNICATION_DISPATCHERS_THREADSAFEGROUNDTRUTHWRAPPER_H

#include "astas_osi_groundtruth.pb.h"

#include <mutex>

namespace astas::communication
{

class ThreadSafeGtWrapper
{
  public:
    ThreadSafeGtWrapper()
    {
        last_returned_timestamp_.set_seconds(-1);
        gt_.mutable_timestamp()->CopyFrom(last_returned_timestamp_);
    }

    astas_osi3::GroundTruth GetGroundTruth()
    {
        std::scoped_lock lock(gt_mutex_);
        last_returned_timestamp_.CopyFrom(gt_.timestamp());
        return gt_;
    }

    void SetGroundTruth(const astas_osi3::GroundTruth& gt)
    {
        std::scoped_lock lock(gt_mutex_);
        gt_ = gt;
    }

    bool HasNewGt()
    {
        std::scoped_lock lock(gt_mutex_);
        return last_returned_timestamp_.seconds() != gt_.timestamp().seconds() ||
               last_returned_timestamp_.nanos() != gt_.timestamp().nanos();
    }

  private:
    std::mutex gt_mutex_{};
    astas_osi3::GroundTruth gt_{};
    astas_osi3::Timestamp last_returned_timestamp_{};
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_DISPATCHERS_THREADSAFEGROUNDTRUTHWRAPPER_H
