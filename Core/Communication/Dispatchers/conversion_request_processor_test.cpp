/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/conversion_request_processor.h"

#include "Core/Tests/TestUtils/MapUtils/astas_map_builder.h"

#include <gtest/gtest.h>

namespace astas::communication
{

using ProtoEditorApi = messages::editor::EditorApi;

class ConversionRequestProcessorTest : public testing::Test
{
  protected:
    ConversionRequestProcessorTest() = default;

    void CreateConversionProcessor()
    {
        std::function fake_async_send = [this](std::unique_ptr<MessageContainer> msg) {
            sent_message_ = std::move(msg);
        };

        processor_ = std::make_unique<ConversionRequestProcessor>(map_builder_.astas_map, fake_async_send);
    }

    test_utils::AstasMapBuilder map_builder_{};

    std::unique_ptr<ConversionRequestProcessor> processor_{nullptr};
    std::unique_ptr<MessageContainer> sent_message_{nullptr};
};

TEST_F(ConversionRequestProcessorTest,
       GivenNotSupportedRequestType_WhenProcessNativeMapPositionRequest_ThenNothingProcessed)
{
    CreateConversionProcessor();

    messages::gui::MessageWrapper msg_wrapper{};
    msg_wrapper.set_message_type_identifier(messages::gui::SCENEDIT);
    msg_wrapper.mutable_editor_api()->set_request(messages::editor::EDITOR_REQUEST_GET_WORLD_POSE);

    EXPECT_FALSE(processor_->ProcessNativeMapPositionRequest(*msg_wrapper.mutable_editor_api()));
    EXPECT_EQ(sent_message_, nullptr);
}

TEST_F(ConversionRequestProcessorTest,
       MapPosRequestWithoutWorldPosePayload_ProcessNativeMapPositionRequest_NothingProcessed)
{
    CreateConversionProcessor();

    messages::gui::MessageWrapper msg_wrapper{};
    msg_wrapper.set_message_type_identifier(messages::gui::SCENEDIT);
    msg_wrapper.mutable_editor_api()->set_request(messages::editor::EDITOR_REQUEST_GET_ODR_POSITION);
    msg_wrapper.mutable_editor_api()->set_error_message("this is not a world pose payload");

    EXPECT_FALSE(processor_->ProcessNativeMapPositionRequest(*msg_wrapper.mutable_editor_api()));
    EXPECT_EQ(sent_message_, nullptr);
}

}  // namespace astas::communication
