/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/simulation_gui_dispatcher.h"

#include "Core/Communication/Common/test_helper.h"
#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_map_builder.h"
#include "Core/Tests/TestUtils/MapUtils/astas_map_builder.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace astas::communication
{
using units::literals::operator""_m;

class SimulationGuiDispatcherTest : public testing::Test
{
  protected:
    SimulationGuiDispatcherTest()
    {
        env_ = std::make_shared<mantle_api::MockEnvironment>();

        sensor_view_builder_ = std::make_unique<environment::proto_groundtruth::SensorViewBuilder>(
            map_builder_.astas_map, mantle_api::Time{10}, service::user_settings::MapChunking{});

        communication::VisualizationData gui_data{
            map_builder_.astas_map, ui_data_, *sensor_view_builder_, &entity_repository_};
        dispatcher_ = std::make_unique<SimulationGuiDispatcher>(gui_data, env_);
    }

    test_utils::AstasMapBuilder map_builder_{};
    service::gui::UiDataProvider ui_data_{};

    std::unique_ptr<environment::proto_groundtruth::SensorViewBuilder> sensor_view_builder_{nullptr};

    std::unique_ptr<SimulationGuiDispatcher> dispatcher_{nullptr};
    environment::api::EntityRepository entity_repository_{nullptr};
    std::shared_ptr<mantle_api::MockEnvironment> env_{nullptr};
};

TEST_F(SimulationGuiDispatcherTest, GivenSessionInitialized_WhenProcessSupportedMessage_ThenAsyncSendTriggered)
{
    bool async_send_called{false};
    std::function fake_async_send = [&async_send_called]([[maybe_unused]] std::unique_ptr<MessageContainer> msg) {
        async_send_called = true;
    };

    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::AREUCORE);
    EXPECT_TRUE(dispatcher_->ProcessMessage(message_wrapper));
    EXPECT_TRUE(async_send_called);
}

TEST_F(SimulationGuiDispatcherTest, GivenSessionInitialized_WhenProcessUnsupportedMessage_ThenNothingSent)
{
    bool async_send_called{false};
    std::function fake_async_send = [&async_send_called]([[maybe_unused]] std::unique_ptr<MessageContainer> msg) {
        async_send_called = true;
    };

    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::PAUSESIM);
    EXPECT_FALSE(dispatcher_->ProcessMessage(message_wrapper));
    EXPECT_FALSE(async_send_called);
}

TEST_F(SimulationGuiDispatcherTest,
       GivenSessionConstructedButNotInitialized_WhenProcessMessageCalled_ThenJustReturnsFalse)
{
    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::AREUCORE);
    EXPECT_FALSE(dispatcher_->ProcessMessage(message_wrapper));
}

TEST_F(SimulationGuiDispatcherTest, GivenSessionInitialized_WhenSetValueMessage_ThenSetValueIsCalledInEnvironment)
{
    std::unique_ptr<MessageContainer> sent_msg;
    std::function fake_async_send = [&sent_msg](std::unique_ptr<MessageContainer> msg) { sent_msg = std::move(msg); };

    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::SETVALUE);
    auto test_interface = message_wrapper.mutable_test_interface();
    std::string expected_name = "MyName";
    std::string expected_value = "MyValue";
    test_interface->set_name(expected_name);
    test_interface->set_value(expected_value);
    EXPECT_CALL(*env_, SetUserDefinedValue(expected_name, expected_value)).Times(1);
    EXPECT_TRUE(dispatcher_->ProcessMessage(message_wrapper));
}

TEST_F(SimulationGuiDispatcherTest, GivenSessionInitializedButNoEnvironment_WhenSetValueMessage_ThenMessageNotProcessed)
{
    env_ = nullptr;
    communication::VisualizationData gui_data{
        map_builder_.astas_map, ui_data_, *sensor_view_builder_, &entity_repository_};
    dispatcher_ = std::make_unique<SimulationGuiDispatcher>(gui_data, env_);

    std::unique_ptr<MessageContainer> sent_msg;
    std::function fake_async_send = [&sent_msg](std::unique_ptr<MessageContainer> msg) { sent_msg = std::move(msg); };

    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper message_wrapper{};
    message_wrapper.set_message_type_identifier(messages::gui::SETVALUE);
    auto test_interface = message_wrapper.mutable_test_interface();
    test_interface->set_name("MyName");
    test_interface->set_value("MyValue");
    EXPECT_FALSE(dispatcher_->ProcessMessage(message_wrapper));
}

class SimulationGuiDispatcherConversionTest : public testing::Test
{
  protected:
    SimulationGuiDispatcherConversionTest() : env_{std::make_shared<mantle_api::MockEnvironment>()} {};

    void CreateSessionWithNdsMap()
    {
        test_utils::RawMapBuilder builder;
        builder.AddNewLaneGroup().StraightEastingLaneWithFivePoints();
        astas_map_ = builder.Build(environment::map::AstasMap::SourceMapType::kNDS);

        gt_builder_ = std::make_unique<environment::proto_groundtruth::SensorViewBuilder>(
            *astas_map_, mantle_api::Time{10}, service::user_settings::MapChunking{});

        communication::VisualizationData gui_data{*astas_map_, ui_data_, *gt_builder_, &entity_repository_};

        dispatcher_ = std::make_unique<SimulationGuiDispatcher>(gui_data, env_);
    }

    void CreateSessionWithOdrMap()
    {
        test_utils::RawMapBuilder builder;
        builder.AddNewLaneGroup().StraightEastingLaneWithFivePoints();
        astas_map_ = builder.Build(environment::map::AstasMap::SourceMapType::kOpenDrive);

        gt_builder_ = std::make_unique<environment::proto_groundtruth::SensorViewBuilder>(
            *astas_map_, mantle_api::Time{10}, service::user_settings::MapChunking{});

        communication::VisualizationData gui_data{*astas_map_, ui_data_, *gt_builder_, &entity_repository_};

        dispatcher_ = std::make_unique<SimulationGuiDispatcher>(gui_data, env_);
    }

    void CreateSessionWithNoMap()
    {
        astas_map_ = test_utils::MapCatalogue::EmptyMap();

        gt_builder_ = std::make_unique<environment::proto_groundtruth::SensorViewBuilder>(
            *astas_map_, mantle_api::Time{10}, service::user_settings::MapChunking{});

        communication::VisualizationData gui_data{*astas_map_, ui_data_, *gt_builder_, &entity_repository_};

        dispatcher_ = std::make_unique<SimulationGuiDispatcher>(gui_data, env_);
    }

    std::unique_ptr<SimulationGuiDispatcher> dispatcher_{nullptr};

    service::gui::UiDataProvider ui_data_{};
    std::unique_ptr<environment::map::AstasMap> astas_map_{nullptr};
    std::unique_ptr<environment::proto_groundtruth::SensorViewBuilder> gt_builder_{nullptr};
    environment::api::EntityRepository entity_repository_{nullptr};
    std::shared_ptr<mantle_api::MockEnvironment> env_{nullptr};
};

TEST_F(SimulationGuiDispatcherConversionTest,
       RequestToConvertWorldPosToOdrPosWithOdrMap_ProcessingConversionRequest_Processed)
{
    std::unique_ptr<MessageContainer> sent_msg;
    std::function fake_async_send = [&sent_msg](std::unique_ptr<MessageContainer> msg) { sent_msg = std::move(msg); };
    /// Because the map builder created the astas map with a mock coordinate converter, the return type is always the
    /// same (0, 0, 0)
    messages::map::Pose expected_pose{};

    CreateSessionWithOdrMap();
    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper handshake{};
    handshake.set_message_type_identifier(messages::gui::AREUCORE);
    messages::gui::MessageWrapper conversion_request{};
    conversion_request.set_message_type_identifier(messages::gui::SCENEDIT);
    conversion_request.mutable_editor_api()->set_request(messages::editor::EDITOR_REQUEST_GET_ODR_POSITION);
    conversion_request.mutable_editor_api()->mutable_world_pose()->CopyFrom(expected_pose);

    ASSERT_TRUE(dispatcher_->ProcessMessage(handshake));
    ASSERT_TRUE(dispatcher_->ProcessMessage(conversion_request));

    CheckPoseEquality(sent_msg->message_wrapper.editor_api().world_pose(), expected_pose);
}

TEST_F(SimulationGuiDispatcherConversionTest,
       RequestToConvertWorldPosToNdsPosWithOdrMap_ProcessingConversionRequest_NotProcessed)
{
    std::function fake_async_send = [](std::unique_ptr<MessageContainer> /*msg*/) {};

    CreateSessionWithOdrMap();
    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper handshake{};
    handshake.set_message_type_identifier(messages::gui::AREUCORE);
    messages::gui::MessageWrapper conversion_request{};
    conversion_request.set_message_type_identifier(messages::gui::SCENEDIT);
    conversion_request.mutable_editor_api()->set_request(messages::editor::EDITOR_REQUEST_GET_NDS_POSITION);

    ASSERT_TRUE(dispatcher_->ProcessMessage(handshake));
    ASSERT_FALSE(dispatcher_->ProcessMessage(conversion_request));
}

TEST_F(SimulationGuiDispatcherConversionTest,
       RequestToConvertWorldPosToNdsPosWithNdsMap_ProcessingConversionRequest_Processed)
{
    std::unique_ptr<MessageContainer> sent_msg;
    std::function fake_async_send = [&sent_msg](std::unique_ptr<MessageContainer> msg) { sent_msg = std::move(msg); };
    /// Because the map builder created the astas map with a mock coordinate converter, the return type is always the
    /// same (0, 0, 0)
    messages::map::Pose expected_pose{};

    CreateSessionWithNdsMap();
    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper handshake{};
    handshake.set_message_type_identifier(messages::gui::AREUCORE);
    messages::gui::MessageWrapper conversion_request{};
    conversion_request.set_message_type_identifier(messages::gui::SCENEDIT);
    conversion_request.mutable_editor_api()->set_request(messages::editor::EDITOR_REQUEST_GET_NDS_POSITION);
    conversion_request.mutable_editor_api()->mutable_world_pose()->CopyFrom(expected_pose);

    ASSERT_TRUE(dispatcher_->ProcessMessage(handshake));
    ASSERT_TRUE(dispatcher_->ProcessMessage(conversion_request));

    CheckPoseEquality(sent_msg->message_wrapper.editor_api().world_pose(), expected_pose);
}

TEST_F(SimulationGuiDispatcherConversionTest,
       RequestToConvertWorldPosToOdrPosWithNdsMap_ProcessingConversionRequest_NotProcessed)
{
    std::function fake_async_send = [](std::unique_ptr<MessageContainer> /*msg*/) {};

    CreateSessionWithNdsMap();
    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper handshake{};
    handshake.set_message_type_identifier(messages::gui::AREUCORE);
    messages::gui::MessageWrapper conversion_request{};
    conversion_request.set_message_type_identifier(messages::gui::SCENEDIT);
    conversion_request.mutable_editor_api()->set_request(messages::editor::EDITOR_REQUEST_GET_ODR_POSITION);

    ASSERT_TRUE(dispatcher_->ProcessMessage(handshake));
    ASSERT_FALSE(dispatcher_->ProcessMessage(conversion_request));
}

TEST_F(SimulationGuiDispatcherConversionTest,
       GivenRequestToConvertToWorldPos_WhenProcessingConversionRequest_ThenNotProcessed)
{
    std::function fake_async_send = [](std::unique_ptr<MessageContainer> /*msg*/) {};

    CreateSessionWithNoMap();
    dispatcher_->Initialize(fake_async_send);

    messages::gui::MessageWrapper handshake{};
    handshake.set_message_type_identifier(messages::gui::AREUCORE);
    messages::gui::MessageWrapper conversion_request{};
    conversion_request.set_message_type_identifier(messages::gui::SCENEDIT);
    conversion_request.mutable_editor_api()->set_request(messages::editor::EDITOR_REQUEST_GET_WORLD_POSE);

    ASSERT_TRUE(dispatcher_->ProcessMessage(handshake));
    ASSERT_FALSE(dispatcher_->ProcessMessage(conversion_request));
}

}  // namespace astas::communication
