/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Dispatchers/connection_state.h"

#include "Core/Service/Logging/logging.h"

#include <boost/statechart/transition.hpp>

namespace astas::communication::statemachine
{

NotConnected::NotConnected()
{
    Debug("Client connected - waiting for handshake from GUI.");
}

boost_sc::result NotConnected::react(const ReceivedAreYouCore&)
{
    context<GuiConnectionState>().core_ack();
    return transit<AwaitingRequests>();
}

AwaitingRequests::AwaitingRequests(my_context ctx) : my_base(ctx)
{
    Debug("GUI connected, waiting for requests.");
    context<GuiConnectionState>().connected = true;
}

boost_sc::result AwaitingRequests::react(const ReceivedPing&)
{
    context<GuiConnectionState>().ping();
    return transit<AwaitingRequests>();
}

boost_sc::result AwaitingRequests::react(const ReceivedScenarioDataRequest&)
{
    Debug("Client requested scenario and map.");
    context<GuiConnectionState>().send_scenario_data();
    return transit<AwaitingRequests>();
}

boost_sc::result AwaitingRequests::react(const ReceivedGTRequest&)
{
    context<GuiConnectionState>().send_ground_truth();
    return transit<Visualizing>();
}

Visualizing::Visualizing(){TRACE("Client started to visualize.")}

boost_sc::result Visualizing::react(const ReceivedPing&)
{
    context<GuiConnectionState>().ping();
    return transit<Visualizing>();
}

boost_sc::result Visualizing::react(const ReceivedGTRequest&)
{
    context<GuiConnectionState>().send_ground_truth();
    return transit<Visualizing>();
}

}  // namespace astas::communication::statemachine
