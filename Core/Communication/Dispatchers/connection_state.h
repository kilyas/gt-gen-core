/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_DISPATCHERS_CONNECTIONSTATE_H
#define GTGEN_CORE_COMMUNICATION_DISPATCHERS_CONNECTIONSTATE_H

#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/state_machine.hpp>

namespace astas::communication::statemachine
{
namespace boost_sc = boost::statechart;

struct NotConnected;

/// THE STATE MACHINE
/// The GuiConnectionState handles transitions between the internal states of a connection with a gui.
/// Transitions are triggered by input from an AsyncRead.
/// Transitions usually trigger an action (std::function), followed by AsyncRead again.
struct GuiConnectionState : boost_sc::state_machine<GuiConnectionState, NotConnected>
{
    std::function<void()> send_scenario_data{nullptr};
    std::function<void()> send_ground_truth{nullptr};
    std::function<void()> core_ack{nullptr};
    std::function<void()> ping{nullptr};

    bool connected{false};
};

/// EVENTS
struct ReceivedAreYouCore : boost_sc::event<ReceivedAreYouCore>
{
};

struct ReceivedPing : boost_sc::event<ReceivedPing>
{
};

struct ReceivedScenarioDataRequest : boost_sc::event<ReceivedScenarioDataRequest>
{
};

struct ReceivedGTRequest : boost_sc::event<ReceivedGTRequest>
{
};

/// STATES
struct NotConnected : boost_sc::simple_state<NotConnected, GuiConnectionState>
{
    NotConnected();
    using reactions = boost::mpl::list<boost_sc::custom_reaction<ReceivedAreYouCore>>;

    boost_sc::result react(const ReceivedAreYouCore& event);  // NOLINT(readability-identifier-naming)
};

struct AwaitingRequests : boost_sc::state<AwaitingRequests, GuiConnectionState>
{
    explicit AwaitingRequests(my_context ctx);
    using reactions = boost::mpl::list<boost_sc::custom_reaction<ReceivedPing>,
                                       boost_sc::custom_reaction<ReceivedScenarioDataRequest>,
                                       boost_sc::custom_reaction<ReceivedGTRequest>>;

    boost_sc::result react(const ReceivedPing& event);                 // NOLINT(readability-identifier-naming)
    boost_sc::result react(const ReceivedScenarioDataRequest& event);  // NOLINT(readability-identifier-naming)
    boost_sc::result react(const ReceivedGTRequest& event);            // NOLINT(readability-identifier-naming)
};

struct Visualizing : boost_sc::simple_state<Visualizing, GuiConnectionState>
{
    Visualizing();
    using reactions =
        boost::mpl::list<boost_sc::custom_reaction<ReceivedPing>, boost_sc::custom_reaction<ReceivedGTRequest>>;

    boost_sc::result react(const ReceivedPing& event);       // NOLINT(readability-identifier-naming)
    boost_sc::result react(const ReceivedGTRequest& event);  // NOLINT(readability-identifier-naming)
};

}  // namespace astas::communication::statemachine

#endif  // GTGEN_CORE_COMMUNICATION_DISPATCHERS_CONNECTIONSTATE_H
