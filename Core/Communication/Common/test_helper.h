/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_COMMON_TESTHELPER_H
#define GTGEN_CORE_COMMUNICATION_COMMON_TESTHELPER_H

#include "Core/Tests/TestUtils/expect_extensions.h"
#include "astas_map.pb.h"
#include "scenario.pb.h"

#include <gtest/gtest.h>

namespace astas::communication
{

inline void CheckPoseEquality(const messages::map::Pose& actual, const messages::map::Pose& expected)
{
    EXPECT_TRIPLE(expected.position(), actual.position());
    EXPECT_TRIPLE(expected.orientation(), actual.orientation());
}

inline void CheckOdrPositionEquality(const messages::scenario::OpenDrivePosition& actual_pos,
                                     const messages::scenario::OpenDrivePosition& expected_pos)
{
    EXPECT_EQ(expected_pos.lane_id(), actual_pos.lane_id());
    EXPECT_EQ(expected_pos.road_id(), actual_pos.road_id());
    EXPECT_NEAR(expected_pos.s_offset(), actual_pos.s_offset(), 0.01);
    EXPECT_NEAR(expected_pos.t_offset(), actual_pos.t_offset(), 0.01);
}

inline void CheckNdsPositionEquality(const messages::scenario::LatLonPosition& actual_pos,
                                     const messages::scenario::LatLonPosition& expected_pos)
{
    // accuracy according to https://gis.stackexchange.com/a/8674
    EXPECT_NEAR(actual_pos.longitude(), expected_pos.longitude(), 0.000001);
    EXPECT_NEAR(actual_pos.latitude(), expected_pos.latitude(), 0.000001);
}

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_COMMON_TESTHELPER_H
