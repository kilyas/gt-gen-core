/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_COMMON_EXCEPTIONS_H
#define GTGEN_CORE_COMMUNICATION_COMMON_EXCEPTIONS_H

#include "Core/Service/Utility/exceptions.h"

namespace astas::communication::exception
{

class AsyncServerException : public service::utility::exception::AstasException
{
    using service::utility::exception::AstasException::AstasException;
};

class AcceptorCreationError : public AsyncServerException
{
    using AsyncServerException::AsyncServerException;
};

class ServiceRunError : public AsyncServerException
{
    using AsyncServerException::AsyncServerException;
};

class DispatcherError : public service::utility::exception::AstasException
{
    using service::utility::exception::AstasException::AstasException;
};

}  // namespace astas::communication::exception

#endif  // GTGEN_CORE_COMMUNICATION_COMMON_EXCEPTIONS_H
