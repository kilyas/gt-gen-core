/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_ASYNCSERVER_ASYNCSERVER_H
#define GTGEN_CORE_COMMUNICATION_ASYNCSERVER_ASYNCSERVER_H

#include "Core/Communication/Dispatchers/dispatcher_factory.h"

#include <memory>

namespace astas::communication
{

/// @brief Asynchronous server for communication with GUI
/// @todo After growing for a while, this needs a refactoring:
/// - at the moment, we have a shared_ptr and multiple mutexes required to synchronize the components accessing
///   simulator data, accessing the network, the cli, ...
/// - the mutexes should be removed by introducing some kind of queueing mechanism, the queued actions could be
///   processed in an AsyncServer.Tick()
/// - also split down the AsyncServer a little further
/// - when component access is thread safe without mutexes, the shared_ptr should be completely removed
/// - note: asio callbacks are called even after io_service_.stop() or .reset() and worker_.join(), see:
/// - PR#2583 - issuecomment-1810034
class AsyncServer
{
  public:
    AsyncServer();
    AsyncServer(AsyncServer&& other) = delete;
    AsyncServer(const AsyncServer&) = delete;
    AsyncServer& operator=(const AsyncServer&) = delete;
    AsyncServer& operator=(AsyncServer&& other) = delete;
    ~AsyncServer();

    /// @brief Starts asynchronous service accepting GUI connections, tries to bind to a port in [min_port, max_port]
    /// @param dispatcher_factory Factory to create dispatchers for each accepted connection
    /// @param allow_multiple Enables concurrent connections. By default new connection requests will be queued.
    /// @throws AsyncServerException::AcceptorCreationError, AsyncServerException::ServiceRunError
    void StartAcceptingConnections(std::unique_ptr<DispatcherFactory> dispatcher_factory, bool allow_multiple = false);

    /// @brief Provides the port reserved by the server if StartAcceptingConnections has been called, 0 otherwise.
    std::uint16_t GetReservedPort() const;

    /// @brief Interrupts the server and shuts down its current sessions. The server can be restarted by calling
    /// StartAcceptingConnections() with a new DispatcherFactory again
    void StopServing();

  private:
    class Impl;
    std::unique_ptr<Impl> impl_;
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_ASYNCSERVER_ASYNCSERVER_H
