/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/AsyncServer/network_session.h"

#include "Core/Service/Logging/logging.h"

#include <boost/bind.hpp>
#include <unistd.h>

#include <string>
#include <utility>

namespace astas::communication
{

NetworkSession::NetworkSession(boost::asio::io_service& io_service, std::string connection_purpose)
    : purpose_{std::move(connection_purpose)},
      socket_{io_service},
      mutable_read_buffer_{read_buffer_.prepare(0)},
      output_stream_{&send_buffer_}
{
}

void NetworkSession::Start(DispatcherFactory& dispatcher_factory)
{
    dispatcher_ = dispatcher_factory.Create();
    dispatcher_->Initialize(
        [this](auto&& place_holder_1) { AsyncSend(std::forward<decltype(place_holder_1)>(place_holder_1)); });

    // initial read expecting AREUCORE
    AsyncRead();
}

void NetworkSession::AsyncRead()
{
    // try to get message size
    boost::asio::async_read(socket_,
                            boost::asio::buffer(msg_size_buffer_, msg_size_buffer_size),
                            // NOLINTNEXTLINE(modernize-avoid-bind) - accepted
                            boost::bind(&NetworkSession::OnMessageSizeReceived,
                                        shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
}

void NetworkSession::OnMessageSizeReceived(const boost::system::error_code& error_code,
                                           const std::size_t bytes_transferred)
{
    if (error_code.value() != 0 || bytes_transferred == 0)
    {
        HandleMessageReceiveError(error_code);
        return;
    }

    const std::uint32_t message_size = message_converter_.GetMessageSize(msg_size_buffer_.data(), msg_size_buffer_size);
    if (message_size == 0)
    {
        Warn(
            "{} server wasn't able to determine message size (or it was 0), closing this connection and waiting "
            "for new ones",
            purpose_);
        socket_.close();
        return;
    }

    mutable_read_buffer_ = boost::asio::mutable_buffers_1{read_buffer_.prepare(message_size)};
    // read the actual message
    boost::asio::async_read(socket_,
                            mutable_read_buffer_,
                            // NOLINTNEXTLINE(modernize-avoid-bind) - accepted
                            boost::bind(&NetworkSession::OnMessageReceived,
                                        shared_from_this(),
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred));
}

void NetworkSession::OnMessageReceived(const boost::system::error_code& error_code, const std::size_t bytes_transferred)
{
    if (error_code.value() != 0)
    {
        HandleMessageReceiveError(error_code);
        return;
    }

    const auto* received_data = boost::asio::buffer_cast<const char*>(mutable_read_buffer_);
    const auto* message = message_converter_.TryGetProtoMessageWrapper(received_data, bytes_transferred);
    if (message == nullptr)
    {
        Debug(
            "{} server received message that it couldn't interpret! Dropping the message and continue "
            "listening...",
            purpose_);
        AsyncRead();
    }
    else
    {
        bool processed_message = dispatcher_->ProcessMessage(*message);
        if (!processed_message)
        {
            Debug(
                "{} server received not supported message type in wrapper message. Dropping the message "
                "and continue listening...",
                purpose_);
            AsyncRead();
        }
    }
}

void NetworkSession::HandleMessageReceiveError(const boost::system::error_code& error_code)
{
    if (error_code.message() == "End of file")
    {
        Debug("Looks like {} connection has been closed by GUI. Waiting for new connections...", purpose_);
        socket_.close();
    }
    else
    {
        Warn(
            "{} server encountered an error when it tried to receive a message, closing this connection and waiting "
            "for new ones. Error was: {}.",
            purpose_,
            error_code.message());
        socket_.close();
    }
}

void NetworkSession::AsyncSend(std::unique_ptr<MessageContainer> message)
{
    // first send size of following message
    num_bytes_to_transfer_ = static_cast<std::size_t>(message->message_wrapper.ByteSize());
    bool size_sent = SendMessageSize(num_bytes_to_transfer_);

    // now send the actual message
    const auto serialized_bytes = message->Serialize(output_stream_);
    if (!serialized_bytes || !size_sent)
    {
        Warn("{} server failed to serialize or send message, closing this connection and waiting for new ones.",
             purpose_);
        socket_.close();
        return;
    }

    socket_.async_write_some(send_buffer_.data(),
                             // NOLINTNEXTLINE(modernize-avoid-bind) - accepted
                             boost::bind(&NetworkSession::OnMessageSent,
                                         shared_from_this(),
                                         boost::asio::placeholders::error,
                                         boost::asio::placeholders::bytes_transferred));
}

bool NetworkSession::SendMessageSize(std::size_t message_byte_size)
{
    msg_size_.set_num_bytes(static_cast<std::uint32_t>(message_byte_size));

    ASSERT((msg_size_.ByteSize() == msg_size_buffer_size) &&
           "Serialized size of MessageSize is not aligned with msg_size_buffer_size!");

    msg_size_.SerializeToArray(&msg_size_buffer_, msg_size_.ByteSize());
    boost::system::error_code error;
    write(socket_, boost::asio::buffer(msg_size_buffer_, msg_size_buffer_size), error);

    return error.value() == 0;
}

void NetworkSession::OnMessageSent(const boost::system::error_code& error_code, const std::size_t bytes_transferred)
{
    if (error_code.value() != 0 || bytes_transferred == 0)
    {
        TRACE("{} server failed to send message, closing this connection and waiting for new ones. Error was: {}.",
              purpose_,
              error_code.message());
        socket_.close();
        return;
    }

    // remove sent data from buffer
    send_buffer_.consume(bytes_transferred);

    // send what is left
    auto bytes_left = num_bytes_to_transfer_ - bytes_transferred;
    if (bytes_left > 0)
    {
        num_bytes_to_transfer_ = bytes_left;
        socket_.async_write_some(send_buffer_.data(),
                                 // NOLINTNEXTLINE(modernize-avoid-bind) - accepted
                                 boost::bind(&NetworkSession::OnMessageSent,
                                             shared_from_this(),
                                             boost::asio::placeholders::error,
                                             boost::asio::placeholders::bytes_transferred));
    }
    else  // if nothing's left to write, start to listen again
    {
        AsyncRead();
    }
}

void NetworkSession::ShutDown()
{
    std::scoped_lock lock{shutdown_mutex_};
    if (dispatcher_ != nullptr)
    {
        dispatcher_->ShutDown();
    }
}

NetworkSession::~NetworkSession()
{
    ShutDown();
}

}  // namespace astas::communication
