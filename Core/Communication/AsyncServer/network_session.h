/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_ASYNCSERVER_NETWORKSESSION_H
#define GTGEN_CORE_COMMUNICATION_ASYNCSERVER_NETWORKSESSION_H

#include "Core/Communication/Dispatchers/dispatcher_factory.h"
#include "Core/Communication/Serialization/message_converter.h"

#include <boost/asio.hpp>

#include <array>
#include <utility>

namespace astas::communication
{

using boost::asio::ip::tcp;

/// @brief Handles communication with remote client via the provided boost::asio::io_service
/// No (custom) exceptions are thrown, depending on their severity problems will be ignored or the connection closed.
/// If the connection is closed during a read/send action, a debug or trace message will be logged
class NetworkSession : public std::enable_shared_from_this<NetworkSession>
{
  public:
    NetworkSession(boost::asio::io_service& io_service, std::string connection_purpose);
    ~NetworkSession();
    NetworkSession(const NetworkSession&) = delete;
    NetworkSession& operator=(const NetworkSession&) = delete;
    NetworkSession(NetworkSession&&) = delete;
    NetworkSession& operator=(NetworkSession&&) = delete;

    tcp::socket& GetSocket() { return socket_; }

    /// @brief Launches a session with a dispatcher created by dispatcher_factory
    void Start(DispatcherFactory& dispatcher_factory);

    /// @brief Manually shuts down the session, required if tasks are preventing the io_service from shutting down
    void ShutDown();

  private:
    void AsyncRead();
    void AsyncSend(std::unique_ptr<MessageContainer> message);
    void OnMessageSizeReceived(const boost::system::error_code& error_code, std::size_t bytes_transferred);
    void OnMessageReceived(const boost::system::error_code& error_code, std::size_t bytes_transferred);
    void HandleMessageReceiveError(const boost::system::error_code& error_code);
    bool SendMessageSize(std::size_t message_byte_size);
    void OnMessageSent(const boost::system::error_code& error_code, std::size_t bytes_transferred);

    MessageConverter message_converter_{};
    std::unique_ptr<IDispatcher> dispatcher_{nullptr};
    std::string purpose_{};

    tcp::socket socket_;

    boost::asio::streambuf read_buffer_;
    boost::asio::mutable_buffers_1 mutable_read_buffer_;
    boost::asio::streambuf send_buffer_;
    std::ostream output_stream_;
    std::size_t num_bytes_to_transfer_{0};

    std::mutex shutdown_mutex_{};

    // 1 byte header, 4 bytes uint32 containing the size of the message that will be sent
    static constexpr std::uint16_t msg_size_buffer_size{5};
    messages::gui::MessageSize msg_size_{};
    std::array<char, msg_size_buffer_size> msg_size_buffer_{};
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_ASYNCSERVER_NETWORKSESSION_H
