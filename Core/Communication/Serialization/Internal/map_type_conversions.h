/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPTYPECONVERSIONS_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPTYPECONVERSIONS_H

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/Map/AstasMap/lane_boundary.h"
#include "Core/Environment/Map/AstasMap/lane_group.h"
#include "Core/Service/Osi/stationary_object_types.h"
#include "astas_map.pb.h"

namespace astas::communication
{

inline messages::map::LaneGroupType AstasToProtoLaneGroupType(environment::map::LaneGroup::Type astas_lane_group_type)
{
    using astas_type = environment::map::LaneGroup::Type;
    using proto_type = messages::map::LaneGroupType;

    switch (astas_lane_group_type)
    {
        case astas_type::kOther:
        {
            return proto_type::LANE_GROUP_TYPE_OTHER;
        }
        case astas_type::kOneWay:
        {
            return proto_type::LANE_GROUP_TYPE_ONE_WAY;
        }
        case astas_type::kJunction:
        {
            return proto_type::LANE_GROUP_TYPE_JUNCTION;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::LANE_GROUP_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::LaneBoundaryColor AstasToProtoBoundaryColor(
    environment::map::LaneBoundary::Color astas_boundary_color)
{
    using astas_type = environment::map::LaneBoundary::Color;
    using proto_type = messages::map::LaneBoundaryColor;

    switch (astas_boundary_color)
    {
        case astas_type::kOther:
        {
            return proto_type::BOUNDARY_COLOR_OTHER;
        }
        case astas_type::kNone:
        {
            return proto_type::BOUNDARY_COLOR_NONE;
        }
        case astas_type::kWhite:
        {
            return proto_type::BOUNDARY_COLOR_WHITE;
        }
        case astas_type::kYellow:
        {
            return proto_type::BOUNDARY_COLOR_YELLOW;
        }
        case astas_type::kRed:
        {
            return proto_type::BOUNDARY_COLOR_RED;
        }
        case astas_type::kBlue:
        {
            return proto_type::BOUNDARY_COLOR_BLUE;
        }
        case astas_type::kGreen:
        {
            return proto_type::BOUNDARY_COLOR_GREEN;
        }
        case astas_type::kViolet:
        {
            return proto_type::BOUNDARY_COLOR_VIOLET;
        }
        case astas_type::kLightGray:
        {
            return proto_type::BOUNDARY_COLOR_LIGHTGRAY;
        }
        case astas_type::kGray:
        {
            return proto_type::BOUNDARY_COLOR_GRAY;
        }
        case astas_type::kDarkGray:
        {
            return proto_type::BOUNDARY_COLOR_DARKGRAY;
        }
        case astas_type::kBlack:
        {
            return proto_type::BOUNDARY_COLOR_BLACK;
        }
        case astas_type::kCyan:
        {
            return proto_type::BOUNDARY_COLOR_CYAN;
        }
        case astas_type::kOrange:
        {
            return proto_type::BOUNDARY_COLOR_ORANGE;
        }
        case astas_type::kStandard:
        {
            return proto_type::BOUNDARY_COLOR_STANDARD;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::BOUNDARY_COLOR_UNKNOWN;
        }
    }
}

inline messages::map::LaneBoundaryType AstasToProtoBoundaryType(
    environment::map::LaneBoundary::Type astas_boundary_type)
{
    using astas_type = environment::map::LaneBoundary::Type;
    using proto_type = messages::map::LaneBoundaryType;

    switch (astas_boundary_type)
    {
        case astas_type::kOther:
        {
            return proto_type::BOUNDARY_TYPE_OTHER;
        }
        case astas_type::kNoLine:
        {
            return proto_type::BOUNDARY_TYPE_NOLINE;
        }
        case astas_type::kSolidLine:
        {
            return proto_type::BOUNDARY_TYPE_SOLIDLINE;
        }
        case astas_type::kDashedLine:
        {
            return proto_type::BOUNDARY_TYPE_DASHEDLINE;
        }
        case astas_type::kBottsDots:
        {
            return proto_type::BOUNDARY_TYPE_BOTTSDOTS;
        }
        case astas_type::kRoadEdge:
        {
            return proto_type::BOUNDARY_TYPE_ROADEDGE;
        }
        case astas_type::kSnowEdge:
        {
            return proto_type::BOUNDARY_TYPE_SNOWEDGE;
        }
        case astas_type::kGrassEdge:
        {
            return proto_type::BOUNDARY_TYPE_GRASSEDGE;
        }
        case astas_type::kGravelEdge:
        {
            return proto_type::BOUNDARY_TYPE_GRAVELEDGE;
        }
        case astas_type::kSoilEdge:
        {
            return proto_type::BOUNDARY_TYPE_SOILEDGE;
        }
        case astas_type::kGuardRail:
        {
            return proto_type::BOUNDARY_TYPE_GUARDRAIL;
        }
        case astas_type::kCurb:
        {
            return proto_type::BOUNDARY_TYPE_CURB;
        }
        case astas_type::kStructure:
        {
            return proto_type::BOUNDARY_TYPE_STRUCTURE;
        }
        case astas_type::kBarrier:
        {
            return proto_type::BOUNDARY_TYPE_BARRIER;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::BOUNDARY_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::LaneType AstasToProtoLaneType(environment::map::LaneFlags lane_flags)
{
    if (lane_flags.IsDrivable())
    {
        return messages::map::LaneType::LANE_TYPE_DRIVING;
    }
    return messages::map::LaneType::LANE_TYPE_NON_DRIVING;
}

inline messages::map::RoadObjectType AstasToProtoRoadObjectType(osi::StationaryObjectEntityType astas_road_object_type)
{
    using astas_type = osi::StationaryObjectEntityType;
    using proto_type = messages::map::RoadObjectType;

    switch (astas_road_object_type)
    {
        case astas_type::kOther:
        {
            return proto_type::ROAD_OBJECT_TYPE_OTHER;
        }
        case astas_type::kBridge:
        {
            return proto_type::ROAD_OBJECT_TYPE_BRIDGE;
        }
        case astas_type::kBuilding:
        {
            return proto_type::ROAD_OBJECT_TYPE_BUILDING;
        }
        case astas_type::kPole:
        {
            return proto_type::ROAD_OBJECT_TYPE_POLE;
        }
        case astas_type::kPylon:
        {
            return proto_type::ROAD_OBJECT_TYPE_PYLON;
        }
        case astas_type::kDelineator:
        {
            return proto_type::ROAD_OBJECT_TYPE_DELINEATOR;
        }
        case astas_type::kTree:
        {
            return proto_type::ROAD_OBJECT_TYPE_TREE;
        }
        case astas_type::kBarrier:
        {
            return proto_type::ROAD_OBJECT_TYPE_BARRIER;
        }
        case astas_type::kVegetation:
        {
            return proto_type::ROAD_OBJECT_TYPE_VEGETATION;
        }
        case astas_type::kCurbstone:
        {
            return proto_type::ROAD_OBJECT_TYPE_CURBSTONE;
        }
        case astas_type::kWall:
        {
            return proto_type::ROAD_OBJECT_TYPE_WALL;
        }
        case astas_type::kVerticalStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_VERTICALSTRUCTURE;
        }
        case astas_type::kRectangularStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_RECTANGULARSTRUCTURE;
        }
        case astas_type::kOverheadStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_OVERHEADSTRUCTURE;
        }
        case astas_type::kReflectiveStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_REFLECTIVESTRUCTURE;
        }
        case astas_type::kConstructionSiteElement:
        {
            return proto_type::ROAD_OBJECT_TYPE_CONSTRUCTIONSITEELEMENT;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::ROAD_OBJECT_TYPE_UNKNOWN;
        }
    }
}

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPTYPECONVERSIONS_H
