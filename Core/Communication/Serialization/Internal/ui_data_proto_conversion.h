/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_UIDATAPROTOCONVERSION_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_UIDATAPROTOCONVERSION_H

#include "Core/Service/Gui/ui_data.h"
#include "ui_data.pb.h"

namespace astas::communication
{

/// @brief Converts additional UI info to corresponding proto message
/// @param ad_active Currently the only additional info, more will follow
/// @param proto_message Proto message that should be filled
void FillUiDataProtoRepresentation(const service::gui::UiData& ui_data, messages::ui::UiData* proto_message);

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_UIDATAPROTOCONVERSION_H
