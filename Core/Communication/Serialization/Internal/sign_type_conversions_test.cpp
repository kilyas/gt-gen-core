/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/sign_type_conversions.h"

#include <gtest/gtest.h>

#include <utility>

namespace astas::communication
{

//
// AstasToProtoRoadMarkingType
//
class RoadMarkingTypeConverterTestFixture
    : public testing::TestWithParam<std::tuple<osi::OsiRoadMarkingsType, messages::map::RoadMarkingType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    SignTypeConverter,
    RoadMarkingTypeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi::OsiRoadMarkingsType, messages::map::RoadMarkingType>>{
        std::make_tuple(osi::OsiRoadMarkingsType::kUnknown, messages::map::RoadMarkingType::ROAD_MARKING_TYPE_UNKNOWN),
        std::make_tuple(osi::OsiRoadMarkingsType::kOther, messages::map::RoadMarkingType::ROAD_MARKING_TYPE_OTHER),
        std::make_tuple(osi::OsiRoadMarkingsType::kPaintedTrafficSign,
                        messages::map::RoadMarkingType::ROAD_MARKING_TYPE_PAINTEDTRAFFICSIGN),
        std::make_tuple(osi::OsiRoadMarkingsType::kSymbolicTrafficSign,
                        messages::map::RoadMarkingType::ROAD_MARKING_TYPE_SYMBOLICTRAFFICSIGN),
        std::make_tuple(osi::OsiRoadMarkingsType::kTextualTrafficSign,
                        messages::map::RoadMarkingType::ROAD_MARKING_TYPE_TEXTUALTRAFFICSIGN),
        std::make_tuple(osi::OsiRoadMarkingsType::kGenericSymbol,
                        messages::map::RoadMarkingType::ROAD_MARKING_TYPE_GENERICSYMBOL),
        std::make_tuple(osi::OsiRoadMarkingsType::kGenericLine,
                        messages::map::RoadMarkingType::ROAD_MARKING_TYPE_GENERICLINE),
        std::make_tuple(osi::OsiRoadMarkingsType::kGenericText,
                        messages::map::RoadMarkingType::ROAD_MARKING_TYPE_GENERICTEXT)}));

TEST_P(RoadMarkingTypeConverterTestFixture, GivenRoadMarkingType_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    osi::OsiRoadMarkingsType astas_type = std::get<0>(GetParam());
    messages::map::RoadMarkingType expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoRoadMarkingType(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoRoadMarkingColor
//
class RoadMarkingColorConverterTestFixture
    : public testing::TestWithParam<std::tuple<osi::OsiRoadMarkingColor, messages::map::RoadMarkingColor>>
{
};

INSTANTIATE_TEST_SUITE_P(
    SignTypeConverter,
    RoadMarkingColorConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi::OsiRoadMarkingColor, messages::map::RoadMarkingColor>>{
        std::make_tuple(osi::OsiRoadMarkingColor::kUnknown,
                        messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_UNKNOWN),
        std::make_tuple(osi::OsiRoadMarkingColor::kOther, messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_OTHER),
        std::make_tuple(osi::OsiRoadMarkingColor::kWhite, messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_WHITE),
        std::make_tuple(osi::OsiRoadMarkingColor::kYellow, messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_YELLOW),
        std::make_tuple(osi::OsiRoadMarkingColor::kBlue, messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_BLUE),
        std::make_tuple(osi::OsiRoadMarkingColor::kRed, messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_RED),
        std::make_tuple(osi::OsiRoadMarkingColor::kGreen, messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_GREEN),
        std::make_tuple(osi::OsiRoadMarkingColor::kViolet,
                        messages::map::RoadMarkingColor::ROAD_MARKING_COLOR_VIOLET)}));

TEST_P(RoadMarkingColorConverterTestFixture, GivenRoadMarkingColor_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    osi::OsiRoadMarkingColor astas_type = std::get<0>(GetParam());
    messages::map::RoadMarkingColor expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoRoadMarkingColor(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoSignVariability
//
class SignVariabilityConverterTestFixture
    : public testing::TestWithParam<std::tuple<osi::OsiTrafficSignVariability, messages::map::SignVariability>>
{
};

INSTANTIATE_TEST_SUITE_P(
    SignTypeConverter,
    SignVariabilityConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi::OsiTrafficSignVariability, messages::map::SignVariability>>{
        std::make_tuple(osi::OsiTrafficSignVariability::kUnknown,
                        messages::map::SignVariability::SIGNVARIABILITY_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignVariability::kOther, messages::map::SignVariability::SIGNVARIABILITY_OTHER),
        std::make_tuple(osi::OsiTrafficSignVariability::kFixed, messages::map::SignVariability::SIGNVARIABILITY_FIXED),
        std::make_tuple(osi::OsiTrafficSignVariability::kVariable,
                        messages::map::SignVariability::SIGNVARIABILITY_VARIABLE)}));

TEST_P(SignVariabilityConverterTestFixture, GivenSignVariability_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    osi::OsiTrafficSignVariability astas_type = std::get<0>(GetParam());
    messages::map::SignVariability expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoSignVariability(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoDirectionScope
//
class DirectionScopeConverterTestFixture
    : public testing::TestWithParam<std::tuple<osi::OsiTrafficSignDirectionScope, messages::map::DirectionScope>>
{
};

INSTANTIATE_TEST_SUITE_P(
    SignTypeConverter,
    DirectionScopeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi::OsiTrafficSignDirectionScope, messages::map::DirectionScope>>{
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kUnknown,
                        messages::map::DirectionScope::DIRECTION_SCOPE_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kOther,
                        messages::map::DirectionScope::DIRECTION_SCOPE_OTHER),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kNoDirection,
                        messages::map::DirectionScope::DIRECTION_SCOPE_NODIRECTION),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kLeft, messages::map::DirectionScope::DIRECTION_SCOPE_LEFT),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kRight,
                        messages::map::DirectionScope::DIRECTION_SCOPE_RIGHT),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kLeftRight,
                        messages::map::DirectionScope::DIRECTION_SCOPE_LEFTRIGHT)}));

TEST_P(DirectionScopeConverterTestFixture, GivenSignVariability_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    osi::OsiTrafficSignDirectionScope astas_type = std::get<0>(GetParam());
    messages::map::DirectionScope expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoDirectionScope(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoSignUnit
//
class SignUnitConverterTestFixture
    : public testing::TestWithParam<std::tuple<osi::OsiTrafficSignValueUnit, messages::map::TrafficSignUnit>>
{
};

INSTANTIATE_TEST_SUITE_P(
    SignTypeConverter,
    SignUnitConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi::OsiTrafficSignValueUnit, messages::map::TrafficSignUnit>>{
        std::make_tuple(osi::OsiTrafficSignValueUnit::kUnknown, messages::map::TrafficSignUnit::SIGN_UNIT_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kOther, messages::map::TrafficSignUnit::SIGN_UNIT_OTHER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kNoUnit, messages::map::TrafficSignUnit::SIGN_UNIT_NOUNIT),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kKilometerPerHour,
                        messages::map::TrafficSignUnit::SIGN_UNIT_KILOMETERPERHOUR),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMilePerHour,
                        messages::map::TrafficSignUnit::SIGN_UNIT_MILEPERHOUR),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMeter, messages::map::TrafficSignUnit::SIGN_UNIT_METER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kKilometer, messages::map::TrafficSignUnit::SIGN_UNIT_KILOMETER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kFeet, messages::map::TrafficSignUnit::SIGN_UNIT_FEET),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMile, messages::map::TrafficSignUnit::SIGN_UNIT_MILE),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMetricTon, messages::map::TrafficSignUnit::SIGN_UNIT_METRICTON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kLongTon, messages::map::TrafficSignUnit::SIGN_UNIT_LONGTON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kShortTon, messages::map::TrafficSignUnit::SIGN_UNIT_SHORTTON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMinutes, messages::map::TrafficSignUnit::SIGN_UNIT_MINUTES),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kDay, messages::map::TrafficSignUnit::SIGN_UNIT_DAY),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kPercentage,
                        messages::map::TrafficSignUnit::SIGN_UNIT_PERCENTAGE)}));

TEST_P(SignUnitConverterTestFixture, GivenSignUnit_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    osi::OsiTrafficSignValueUnit astas_type = std::get<0>(GetParam());
    messages::map::TrafficSignUnit expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoSignUnit(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoMainSign
//

class MainSignConverterTestFixture
    : public testing::TestWithParam<std::tuple<osi::OsiTrafficSignType, messages::map::MainTrafficSignType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    SignTypeConverter,
    MainSignConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi::OsiTrafficSignType, messages::map::MainTrafficSignType>>{
        std::make_tuple(osi::OsiTrafficSignType::kUnknown, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignType::kOther, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OTHER),
        std::make_tuple(osi::OsiTrafficSignType::kDangerSpot,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DANGERSPOT),
        std::make_tuple(osi::OsiTrafficSignType::kZebraCrossing,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ZEBRACROSSING),
        std::make_tuple(osi::OsiTrafficSignType::kFlight, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_FLIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kCattle, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CATTLE),
        std::make_tuple(osi::OsiTrafficSignType::kHorseRiders,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HORSERIDERS),
        std::make_tuple(osi::OsiTrafficSignType::kAmphibians,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_AMPHIBIANS),
        std::make_tuple(osi::OsiTrafficSignType::kFallingRocks,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_FALLINGROCKS),
        std::make_tuple(osi::OsiTrafficSignType::kSnowOrIce,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SNOWORICE),
        std::make_tuple(osi::OsiTrafficSignType::kLooseGravel,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_LOOSEGRAVEL),
        std::make_tuple(osi::OsiTrafficSignType::kWaterside,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_WATERSIDE),
        std::make_tuple(osi::OsiTrafficSignType::kClearance,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CLEARANCE),
        std::make_tuple(osi::OsiTrafficSignType::kMovableBridge,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MOVABLEBRIDGE),
        std::make_tuple(osi::OsiTrafficSignType::kRightBeforeLeftNextIntersection,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_RIGHTBEFORELEFTNEXTINTERSECTION),
        std::make_tuple(osi::OsiTrafficSignType::kTurnLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TURNLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kTurnRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TURNRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kDoubleTurnLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DOUBLETURNLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kDoubleTurnRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DOUBLETURNRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kHillDownwards,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HILLDOWNWARDS),
        std::make_tuple(osi::OsiTrafficSignType::kHillUpwards,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HILLUPWARDS),
        std::make_tuple(osi::OsiTrafficSignType::kUnevenRoad,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_UNEVENROAD),
        std::make_tuple(osi::OsiTrafficSignType::kRoadSlipperyWetOrDirty,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROADSLIPPERYWETORDIRTY),
        std::make_tuple(osi::OsiTrafficSignType::kSideWinds,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWINDS),
        std::make_tuple(osi::OsiTrafficSignType::kRoadNarrowing,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROADNARROWING),
        std::make_tuple(osi::OsiTrafficSignType::kRoadNarrowingRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROADNARROWINGRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kRoadNarrowingLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROADNARROWINGLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kRoadWorks,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROADWORKS),
        std::make_tuple(osi::OsiTrafficSignType::kTrafficQueues,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TRAFFICQUEUES),
        std::make_tuple(osi::OsiTrafficSignType::kTwoWayTraffic,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TWOWAYTRAFFIC),
        std::make_tuple(osi::OsiTrafficSignType::kAttentionTrafficLight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ATTENTIONTRAFFICLIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kPedestrians,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PEDESTRIANS),
        std::make_tuple(osi::OsiTrafficSignType::kChildrenCrossing,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CHILDRENCROSSING),
        std::make_tuple(osi::OsiTrafficSignType::kCycleRoute,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CYCLEROUTE),
        std::make_tuple(osi::OsiTrafficSignType::kDeerCrossing,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DEERCROSSING),
        std::make_tuple(osi::OsiTrafficSignType::kUngatedLevelCrossing,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_UNGATEDLEVELCROSSING),
        std::make_tuple(osi::OsiTrafficSignType::kLevelCrossingMarker,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_LEVELCROSSINGMARKER),
        std::make_tuple(osi::OsiTrafficSignType::kRailwayTrafficPriority,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_RAILWAYTRAFFICPRIORITY),
        std::make_tuple(osi::OsiTrafficSignType::kGiveWay, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_GIVEWAY),
        std::make_tuple(osi::OsiTrafficSignType::kStop, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_STOP),
        std::make_tuple(osi::OsiTrafficSignType::kPriorityToOppositeDirection,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRIORITYTOOPPOSITEDIRECTION),
        std::make_tuple(osi::OsiTrafficSignType::kPriorityToOppositeDirectionUpsideDown,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRIORITYTOOPPOSITEDIRECTIONUPSIDEDOWN),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedLeftTurn,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURN),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedRightTurn,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTTURN),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedStraight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDSTRAIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedRightWay,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTWAY),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedLeftWay,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDLEFTWAY),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedRightTurnAndStraight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTTURNANDSTRAIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kRoundabout,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROUNDABOUT),
        std::make_tuple(osi::OsiTrafficSignType::kOnewayLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ONEWAYLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kOnewayRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ONEWAYRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kPassLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PASSLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kPassRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PASSRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kSideLaneOpenForTraffic,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDELANEOPENFORTRAFFIC),
        std::make_tuple(osi::OsiTrafficSignType::kSideLaneClosedForTraffic,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDELANECLOSEDFORTRAFFIC),
        std::make_tuple(osi::OsiTrafficSignType::kSideLaneClosingForTraffic,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDELANECLOSINGFORTRAFFIC),
        std::make_tuple(osi::OsiTrafficSignType::kBusStop, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BUSSTOP),
        std::make_tuple(osi::OsiTrafficSignType::kTaxiStand,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TAXISTAND),
        std::make_tuple(osi::OsiTrafficSignType::kBicyclesOnly,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BICYCLESONLY),
        std::make_tuple(osi::OsiTrafficSignType::kHorseRidersOnly,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HORSERIDERSONLY),
        std::make_tuple(osi::OsiTrafficSignType::kPedestriansOnly,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PEDESTRIANSONLY),
        std::make_tuple(osi::OsiTrafficSignType::kBicyclesPedestriansSharedOnly,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSHAREDONLY),
        std::make_tuple(osi::OsiTrafficSignType::kBicyclesPedestriansSeparatedLeftOnly,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSEPARATEDLEFTONLY),
        std::make_tuple(osi::OsiTrafficSignType::kBicyclesPedestriansSeparatedRightOnly,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSEPARATEDRIGHTONLY),
        std::make_tuple(osi::OsiTrafficSignType::kPedestrianZoneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PEDESTRIANZONEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kPedestrianZoneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PEDESTRIANZONEEND),
        std::make_tuple(osi::OsiTrafficSignType::kBicycleRoadBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BICYCLEROADBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kBicycleRoadEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BICYCLEROADEND),
        std::make_tuple(osi::OsiTrafficSignType::kBusLane, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BUSLANE),
        std::make_tuple(osi::OsiTrafficSignType::kBusLaneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BUSLANEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kBusLaneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BUSLANEEND),
        std::make_tuple(osi::OsiTrafficSignType::kAllProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ALLPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kMotorizedMultitrackProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MOTORIZEDMULTITRACKPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kTrucksProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TRUCKSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kBicyclesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BICYCLESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kMotorcyclesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MOTORCYCLESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kMopedsProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MOPEDSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kHorseRidersProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HORSERIDERSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kHorseCarriagesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HORSECARRIAGESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kCattleProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CATTLEPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kBusesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_BUSESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kCarsProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CARSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kCarsTrailersProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CARSTRAILERSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kTrucksTrailersProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TRUCKSTRAILERSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kTractorsProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TRACTORSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kPedestriansProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PEDESTRIANSPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kMotorVehiclesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MOTORVEHICLESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kHazardousGoodsVehiclesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HAZARDOUSGOODSVEHICLESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kOverWeightVehiclesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OVERWEIGHTVEHICLESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kVehiclesAxleOverWeightProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_VEHICLESAXLEOVERWEIGHTPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kVehiclesExcessWidthProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_VEHICLESEXCESSWIDTHPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kVehiclesExcessHeightProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_VEHICLESEXCESSHEIGHTPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kVehiclesExcessLengthProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_VEHICLESEXCESSLENGTHPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kDoNotEnter,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DONOTENTER),
        std::make_tuple(osi::OsiTrafficSignType::kSnowChainsRequired,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SNOWCHAINSREQUIRED),
        std::make_tuple(osi::OsiTrafficSignType::kWaterPollutantVehiclesProhibited,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_WATERPOLLUTANTVEHICLESPROHIBITED),
        std::make_tuple(osi::OsiTrafficSignType::kEnvironmentalZoneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ENVIRONMENTALZONEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kEnvironmentalZoneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ENVIRONMENTALZONEEND),
        std::make_tuple(osi::OsiTrafficSignType::kNoUTurnLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NOUTURNLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kNoUTurnRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NOUTURNRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedUTurnLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDUTURNLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kPrescribedUTurnRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRESCRIBEDUTURNRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kMinimumDistanceForTrucks,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MINIMUMDISTANCEFORTRUCKS),
        std::make_tuple(osi::OsiTrafficSignType::kSpeedLimitBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SPEEDLIMITBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kSpeedLimitZoneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SPEEDLIMITZONEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kSpeedLimitZoneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SPEEDLIMITZONEEND),
        std::make_tuple(osi::OsiTrafficSignType::kMinimumSpeedBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MINIMUMSPEEDBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kOvertakingBanBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OVERTAKINGBANBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kOvertakingBanForTrucksBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OVERTAKINGBANFORTRUCKSBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kSpeedLimitEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SPEEDLIMITEND),
        std::make_tuple(osi::OsiTrafficSignType::kMinimumSpeedEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MINIMUMSPEEDEND),
        std::make_tuple(osi::OsiTrafficSignType::kOvertakingBanEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OVERTAKINGBANEND),
        std::make_tuple(osi::OsiTrafficSignType::kOvertakingBanForTrucksEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OVERTAKINGBANFORTRUCKSEND),
        std::make_tuple(osi::OsiTrafficSignType::kAllRestrictionsEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ALLRESTRICTIONSEND),
        std::make_tuple(osi::OsiTrafficSignType::kNoStopping,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NOSTOPPING),
        std::make_tuple(osi::OsiTrafficSignType::kNoParking,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NOPARKING),
        std::make_tuple(osi::OsiTrafficSignType::kNoParkingZoneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NOPARKINGZONEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kNoParkingZoneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NOPARKINGZONEEND),
        std::make_tuple(osi::OsiTrafficSignType::kRightOfWayNextIntersection,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_RIGHTOFWAYNEXTINTERSECTION),
        std::make_tuple(osi::OsiTrafficSignType::kRightOfWayBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_RIGHTOFWAYBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kRightOfWayEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_RIGHTOFWAYEND),
        std::make_tuple(osi::OsiTrafficSignType::kPriorityOverOppositeDirection,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRIORITYOVEROPPOSITEDIRECTION),
        std::make_tuple(osi::OsiTrafficSignType::kPriorityOverOppositeDirectionUpsideDown,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRIORITYOVEROPPOSITEDIRECTIONUPSIDEDOWN),
        std::make_tuple(osi::OsiTrafficSignType::kTownBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOWNBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kTownEnd, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOWNEND),
        std::make_tuple(osi::OsiTrafficSignType::kCarParking,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CARPARKING),
        std::make_tuple(osi::OsiTrafficSignType::kCarParkingZoneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CARPARKINGZONEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kCarParkingZoneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CARPARKINGZONEEND),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkHalfParkingLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKHALFPARKINGLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkHalfParkingRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKHALFPARKINGRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkParkingLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKPARKINGLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkParkingRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKPARKINGRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkPerpendicularHalfParkingLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARHALFPARKINGLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkPerpendicularHalfParkingRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARHALFPARKINGRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkPerpendicularParkingLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARPARKINGLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kSidewalkPerpendicularParkingRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARPARKINGRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kLivingStreetBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_LIVINGSTREETBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kLivingStreetEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_LIVINGSTREETEND),
        std::make_tuple(osi::OsiTrafficSignType::kTunnel, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TUNNEL),
        std::make_tuple(osi::OsiTrafficSignType::kEmergencyStoppingLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_EMERGENCYSTOPPINGLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kEmergencyStoppingRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_EMERGENCYSTOPPINGRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYEND),
        std::make_tuple(osi::OsiTrafficSignType::kExpresswayBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_EXPRESSWAYBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kExpresswayEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_EXPRESSWAYEND),
        std::make_tuple(osi::OsiTrafficSignType::kNamedHighwayExit,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NAMEDHIGHWAYEXIT),
        std::make_tuple(osi::OsiTrafficSignType::kNamedExpresswayExit,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NAMEDEXPRESSWAYEXIT),
        std::make_tuple(osi::OsiTrafficSignType::kNamedRoadExit,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NAMEDROADEXIT),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayExit,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYEXIT),
        std::make_tuple(osi::OsiTrafficSignType::kExpresswayExit,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_EXPRESSWAYEXIT),
        std::make_tuple(osi::OsiTrafficSignType::kOnewayStreet,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ONEWAYSTREET),
        std::make_tuple(osi::OsiTrafficSignType::kCrossingGuards,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CROSSINGGUARDS),
        std::make_tuple(osi::OsiTrafficSignType::kDeadend, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DEADEND),
        std::make_tuple(osi::OsiTrafficSignType::kDeadendExcludingDesignatedActors,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DEADENDEXCLUDINGDESIGNATEDACTORS),
        std::make_tuple(osi::OsiTrafficSignType::kFirstAidStation,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_FIRSTAIDSTATION),
        std::make_tuple(osi::OsiTrafficSignType::kPoliceStation,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_POLICESTATION),
        std::make_tuple(osi::OsiTrafficSignType::kTelephone,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TELEPHONE),
        std::make_tuple(osi::OsiTrafficSignType::kFillingStation,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_FILLINGSTATION),
        std::make_tuple(osi::OsiTrafficSignType::kHotel, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HOTEL),
        std::make_tuple(osi::OsiTrafficSignType::kInn, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_INN),
        std::make_tuple(osi::OsiTrafficSignType::kKiosk, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_KIOSK),
        std::make_tuple(osi::OsiTrafficSignType::kToilet, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOILET),
        std::make_tuple(osi::OsiTrafficSignType::kChapel, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CHAPEL),
        std::make_tuple(osi::OsiTrafficSignType::kTouristInfo,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOURISTINFO),
        std::make_tuple(osi::OsiTrafficSignType::kRepairService,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_REPAIRSERVICE),
        std::make_tuple(osi::OsiTrafficSignType::kPedestrianUnderpass,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PEDESTRIANUNDERPASS),
        std::make_tuple(osi::OsiTrafficSignType::kPedestrianBridge,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PEDESTRIANBRIDGE),
        std::make_tuple(osi::OsiTrafficSignType::kCamperPlace,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CAMPERPLACE),
        std::make_tuple(osi::OsiTrafficSignType::kAdvisorySpeedLimitBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ADVISORYSPEEDLIMITBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kAdvisorySpeedLimitEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ADVISORYSPEEDLIMITEND),
        std::make_tuple(osi::OsiTrafficSignType::kPlaceName,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PLACENAME),
        std::make_tuple(osi::OsiTrafficSignType::kTouristAttraction,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOURISTATTRACTION),
        std::make_tuple(osi::OsiTrafficSignType::kTouristRoute,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOURISTROUTE),
        std::make_tuple(osi::OsiTrafficSignType::kTouristArea,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOURISTAREA),
        std::make_tuple(osi::OsiTrafficSignType::kShoulderNotPassableMotorVehicles,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SHOULDERNOTPASSABLEMOTORVEHICLES),
        std::make_tuple(osi::OsiTrafficSignType::kShoulderUnsafeTrucksTractors,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SHOULDERUNSAFETRUCKSTRACTORS),
        std::make_tuple(osi::OsiTrafficSignType::kTollBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOLLBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kTollEnd, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOLLEND),
        std::make_tuple(osi::OsiTrafficSignType::kTollRoad,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TOLLROAD),
        std::make_tuple(osi::OsiTrafficSignType::kCustoms, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CUSTOMS),
        std::make_tuple(osi::OsiTrafficSignType::kInternationalBorderInfo,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_INTERNATIONALBORDERINFO),
        std::make_tuple(osi::OsiTrafficSignType::kStreetlightRedBand,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_STREETLIGHTREDBAND),
        std::make_tuple(osi::OsiTrafficSignType::kFederalHighwayRouteNumber,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_FEDERALHIGHWAYROUTENUMBER),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayRouteNumber,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYROUTENUMBER),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayInterchangeNumber,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYINTERCHANGENUMBER),
        std::make_tuple(osi::OsiTrafficSignType::kEuropeanRouteNumber,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_EUROPEANROUTENUMBER),
        std::make_tuple(osi::OsiTrafficSignType::kFederalHighwayDirectionLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_FEDERALHIGHWAYDIRECTIONLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kFederalHighwayDirectionRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_FEDERALHIGHWAYDIRECTIONRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kPrimaryRoadDirectionLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRIMARYROADDIRECTIONLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kPrimaryRoadDirectionRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PRIMARYROADDIRECTIONRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kSecondaryRoadDirectionLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SECONDARYROADDIRECTIONLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kSecondaryRoadDirectionRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_SECONDARYROADDIRECTIONRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionDesignatedActorsLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONDESIGNATEDACTORSLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionDesignatedActorsRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONDESIGNATEDACTORSRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kRoutingDesignatedActors,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROUTINGDESIGNATEDACTORS),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionToHighwayLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONTOHIGHWAYLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionToHighwayRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONTOHIGHWAYRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionToLocalDestinationLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONTOLOCALDESTINATIONLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionToLocalDestinationRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONTOLOCALDESTINATIONRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kConsolidatedDirections,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_CONSOLIDATEDDIRECTIONS),
        std::make_tuple(osi::OsiTrafficSignType::kStreetName,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_STREETNAME),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionPreannouncement,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENT),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionPreannouncementLaneConfig,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENTLANECONFIG),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionPreannouncementHighwayEntries,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENTHIGHWAYENTRIES),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayAnnouncement,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYANNOUNCEMENT),
        std::make_tuple(osi::OsiTrafficSignType::kOtherRoadAnnouncement,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OTHERROADANNOUNCEMENT),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayAnnouncementTruckStop,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYANNOUNCEMENTTRUCKSTOP),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayPreannouncementDirections,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYPREANNOUNCEMENTDIRECTIONS),
        std::make_tuple(osi::OsiTrafficSignType::kPoleExit,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_POLEEXIT),
        std::make_tuple(osi::OsiTrafficSignType::kHighwayDistanceBoard,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_HIGHWAYDISTANCEBOARD),
        std::make_tuple(osi::OsiTrafficSignType::kDetourLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DETOURLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kDetourRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DETOURRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kNumberedDetour,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_NUMBEREDDETOUR),
        std::make_tuple(osi::OsiTrafficSignType::kDetourBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DETOURBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kDetourEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DETOUREND),
        std::make_tuple(osi::OsiTrafficSignType::kDetourRoutingBoard,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DETOURROUTINGBOARD),
        std::make_tuple(osi::OsiTrafficSignType::kOptionalDetour,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OPTIONALDETOUR),
        std::make_tuple(osi::OsiTrafficSignType::kOptionalDetourRouting,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_OPTIONALDETOURROUTING),
        std::make_tuple(osi::OsiTrafficSignType::kRouteRecommendation,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROUTERECOMMENDATION),
        std::make_tuple(osi::OsiTrafficSignType::kRouteRecommendationEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ROUTERECOMMENDATIONEND),
        std::make_tuple(osi::OsiTrafficSignType::kAnnounceLaneTransitionLeft,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ANNOUNCELANETRANSITIONLEFT),
        std::make_tuple(osi::OsiTrafficSignType::kAnnounceLaneTransitionRight,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ANNOUNCELANETRANSITIONRIGHT),
        std::make_tuple(osi::OsiTrafficSignType::kAnnounceRightLaneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ANNOUNCERIGHTLANEEND),
        std::make_tuple(osi::OsiTrafficSignType::kAnnounceLeftLaneEnd,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ANNOUNCELEFTLANEEND),
        std::make_tuple(osi::OsiTrafficSignType::kAnnounceRightLaneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ANNOUNCERIGHTLANEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kAnnounceLeftLaneBegin,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ANNOUNCELEFTLANEBEGIN),
        std::make_tuple(osi::OsiTrafficSignType::kAnnounceLaneConsolidation,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_ANNOUNCELANECONSOLIDATION),
        std::make_tuple(osi::OsiTrafficSignType::kDetourCityBlock,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DETOURCITYBLOCK),
        std::make_tuple(osi::OsiTrafficSignType::kGate, messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_GATE),
        std::make_tuple(osi::OsiTrafficSignType::kPoleWarning,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_POLEWARNING),
        std::make_tuple(osi::OsiTrafficSignType::kTrafficCone,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TRAFFICCONE),
        std::make_tuple(osi::OsiTrafficSignType::kMobileLaneClosure,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_MOBILELANECLOSURE),
        std::make_tuple(osi::OsiTrafficSignType::kReflectorPost,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_REFLECTORPOST),
        std::make_tuple(osi::OsiTrafficSignType::kDirectionalBoardWarning,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_DIRECTIONALBOARDWARNING),
        std::make_tuple(osi::OsiTrafficSignType::kGuidingPlate,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_GUIDINGPLATE),
        std::make_tuple(osi::OsiTrafficSignType::kGuidingPlateWedges,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_GUIDINGPLATEWEDGES),
        std::make_tuple(osi::OsiTrafficSignType::kParkingHazard,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_PARKINGHAZARD),
        std::make_tuple(osi::OsiTrafficSignType::kTrafficLightGreenArrow,
                        messages::map::MainTrafficSignType::MAIN_SIGN_TYPE_TRAFFICLIGHTGREENARROW)}));

TEST_P(MainSignConverterTestFixture, GivenMainSign_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    osi::OsiTrafficSignType astas_type = std::get<0>(GetParam());
    messages::map::MainTrafficSignType expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoMainSign(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoSupplementarySign
//
class SupplementarySignConverterTestFixture
    : public testing::TestWithParam<
          std::tuple<environment::map::OsiSupplementarySignType, messages::map::SupplementarySignType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    SignTypeConverter,
    SupplementarySignConverterTestFixture,
    testing::ValuesIn(std::vector<
                      std::tuple<environment::map::OsiSupplementarySignType, messages::map::SupplementarySignType>>{
        std::make_tuple(environment::map::OsiSupplementarySignType::kUnknown,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_UNKNOWN),
        std::make_tuple(environment::map::OsiSupplementarySignType::kOther,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_OTHER),
        std::make_tuple(environment::map::OsiSupplementarySignType::kNoSign,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_NOSIGN),
        std::make_tuple(environment::map::OsiSupplementarySignType::kText,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_TEXT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kSpace,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_SPACE),
        std::make_tuple(environment::map::OsiSupplementarySignType::kTime,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_TIME),
        std::make_tuple(environment::map::OsiSupplementarySignType::kArrow,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_ARROW),
        std::make_tuple(environment::map::OsiSupplementarySignType::kConstrainedTo,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_CONSTRAINEDTO),
        std::make_tuple(environment::map::OsiSupplementarySignType::kExcept,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_EXCEPT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kValidForDistance,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_VALIDFORDISTANCE),
        std::make_tuple(environment::map::OsiSupplementarySignType::kPriorityRoadBottomLeftFourWay,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTFOURWAY),
        std::make_tuple(environment::map::OsiSupplementarySignType::kPriorityRoadTopLeftFourWay,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADTOPLEFTFOURWAY),
        std::make_tuple(
            environment::map::OsiSupplementarySignType::kPriorityRoadBottomLeftThreeWayStraight,
            messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTTHREEWAYSTRAIGHT),
        std::make_tuple(
            environment::map::OsiSupplementarySignType::kPriorityRoadBottomLeftThreeWaySideways,
            messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTTHREEWAYSIDEWAYS),
        std::make_tuple(environment::map::OsiSupplementarySignType::kPriorityRoadTopLeftThreeWayStraight,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADTOPLEFTTHREEWAYSTRAIGHT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kPriorityRoadBottomRightFourWay,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTFOURWAY),
        std::make_tuple(environment::map::OsiSupplementarySignType::kPriorityRoadTopRightFourWay,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADTOPRIGHTFOURWAY),
        std::make_tuple(
            environment::map::OsiSupplementarySignType::kPriorityRoadBottomRightThreeWayStraight,
            messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTTHREEWAYSTRAIGHT),
        std::make_tuple(
            environment::map::OsiSupplementarySignType::kPriorityRoadBottomRightThreeWaySideway,
            messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTTHREEWAYSIDEWAY),
        std::make_tuple(environment::map::OsiSupplementarySignType::kPriorityRoadTopRightThreeWayStraight,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PRIORITYROADTOPRIGHTTHREEWAYSTRAIGHT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kValidInDistance,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_VALIDINDISTANCE),
        std::make_tuple(environment::map::OsiSupplementarySignType::kStopIn,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_STOPIN),
        std::make_tuple(environment::map::OsiSupplementarySignType::kLeftArrow,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_LEFTARROW),
        std::make_tuple(environment::map::OsiSupplementarySignType::kLeftBendArrow,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_LEFTBENDARROW),
        std::make_tuple(environment::map::OsiSupplementarySignType::kRightArrow,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_RIGHTARROW),
        std::make_tuple(environment::map::OsiSupplementarySignType::kRightBendArrow,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_RIGHTBENDARROW),
        std::make_tuple(environment::map::OsiSupplementarySignType::kAccident,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_ACCIDENT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kSnow,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_SNOW),
        std::make_tuple(environment::map::OsiSupplementarySignType::kFog,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_FOG),
        std::make_tuple(environment::map::OsiSupplementarySignType::kRollingHighwayInformation,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_ROLLINGHIGHWAYINFORMATION),
        std::make_tuple(environment::map::OsiSupplementarySignType::kServices,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_SERVICES),
        std::make_tuple(environment::map::OsiSupplementarySignType::kTimeRange,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_TIMERANGE),
        std::make_tuple(environment::map::OsiSupplementarySignType::kParkingDiscTimeRestriction,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PARKINGDISCTIMERESTRICTION),
        std::make_tuple(environment::map::OsiSupplementarySignType::kWeight,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_WEIGHT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kWet,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_WET),
        std::make_tuple(environment::map::OsiSupplementarySignType::kParkingConstraint,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_PARKINGCONSTRAINT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kNoWaitingSideStripes,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_NOWAITINGSIDESTRIPES),
        std::make_tuple(environment::map::OsiSupplementarySignType::kRain,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_RAIN),
        std::make_tuple(environment::map::OsiSupplementarySignType::kSnowRain,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_SNOWRAIN),
        std::make_tuple(environment::map::OsiSupplementarySignType::kNight,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_NIGHT),
        std::make_tuple(environment::map::OsiSupplementarySignType::kStop4Way,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_STOP4WAY),
        std::make_tuple(environment::map::OsiSupplementarySignType::kTruck,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_TRUCK),
        std::make_tuple(environment::map::OsiSupplementarySignType::kTractorsMayBePassed,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_TRACTORSMAYBEPASSED),
        std::make_tuple(environment::map::OsiSupplementarySignType::kHazardous,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_HAZARDOUS),
        std::make_tuple(environment::map::OsiSupplementarySignType::kTrailer,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_TRAILER),
        std::make_tuple(environment::map::OsiSupplementarySignType::kZone,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_ZONE),
        std::make_tuple(environment::map::OsiSupplementarySignType::kMotorcycle,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_MOTORCYCLE),
        std::make_tuple(environment::map::OsiSupplementarySignType::kMotorcycleAllowed,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_MOTORCYCLEALLOWED),
        std::make_tuple(environment::map::OsiSupplementarySignType::kCar,
                        messages::map::SupplementarySignType::SUPPLEMENTARY_TYPE_CAR)}));

TEST_P(SupplementarySignConverterTestFixture, GivenSupplementartySign_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    environment::map::OsiSupplementarySignType astas_type = std::get<0>(GetParam());
    messages::map::SupplementarySignType expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoSupplementarySign(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

}  // namespace astas::communication
