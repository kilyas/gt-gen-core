/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/conversion_test_helper.h"

#include "Core/Communication/Serialization/Internal/map_type_conversions.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"

#include <gtest/gtest.h>

namespace astas::communication
{

void CheckVector3(const mantle_api::Vec3<units::length::meter_t>& map_vector,
                  const messages::map::Vector3d& proto_vector)
{
    EXPECT_DOUBLE_EQ(proto_vector.x(), map_vector.x());
    EXPECT_DOUBLE_EQ(proto_vector.y(), map_vector.y());
    EXPECT_DOUBLE_EQ(proto_vector.z(), map_vector.z());
}

void CheckDimension3(const mantle_api::Dimension3& astas_dimension, const messages::map::Dimension3d& proto_dimension)
{
    EXPECT_DOUBLE_EQ(proto_dimension.width(), astas_dimension.width());
    EXPECT_DOUBLE_EQ(proto_dimension.length(), astas_dimension.length());
    EXPECT_DOUBLE_EQ(proto_dimension.height(), astas_dimension.height());
}

void CheckOrientation3(const mantle_api::Orientation3<units::angle::radian_t>& astas_orientation,
                       const messages::map::Orientation3d& proto_orientation)
{
    EXPECT_DOUBLE_EQ(proto_orientation.yaw(), astas_orientation.yaw());
    EXPECT_DOUBLE_EQ(proto_orientation.pitch(), astas_orientation.pitch());
    EXPECT_DOUBLE_EQ(proto_orientation.roll(), astas_orientation.roll());
}

void CheckPose(const mantle_api::Pose& astas_pose, const messages::map::Pose& proto_pose)
{
    CheckVector3(astas_pose.position, proto_pose.position());
    CheckOrientation3(astas_pose.orientation, proto_pose.orientation());
}

void CheckRoadObject(const environment::map::RoadObject& astas_object, const messages::map::RoadObject& proto_object)
{
    EXPECT_EQ(proto_object.id(), astas_object.id);
    EXPECT_EQ(proto_object.name(), astas_object.name);
    EXPECT_DOUBLE_EQ(proto_object.height(), astas_object.dimensions.height());
    EXPECT_EQ(proto_object.type(), AstasToProtoRoadObjectType(astas_object.type));
    CheckPose(astas_object.pose, proto_object.pose());
    CheckDimension3(astas_object.dimensions, proto_object.dimensions());
}

void CheckSupplementarySign(const environment::map::MountedSign::SupplementarySign& astas_sign,
                            const messages::map::TrafficSign& proto_sign)
{
    CheckPose(astas_sign.pose, proto_sign.pose());
    CheckDimension3(astas_sign.dimensions, proto_sign.dimension());

    ASSERT_FALSE(proto_sign.value_information().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_sign.value_information_size()), astas_sign.value_information.size());
    EXPECT_EQ(proto_sign.value_information(0).text(), astas_sign.value_information[0].text);
    EXPECT_DOUBLE_EQ(proto_sign.value_information(0).value(), astas_sign.value_information[0].value);
    EXPECT_EQ(proto_sign.value_information(0).unit(), AstasToProtoSignUnit(astas_sign.value_information[0].value_unit));

    EXPECT_EQ(proto_sign.sign_variability(), AstasToProtoSignVariability(astas_sign.variability));
    EXPECT_EQ(proto_sign.supplementary_sign_type(), AstasToProtoSupplementarySign(astas_sign.type));
}

void CheckTrafficSign(const environment::map::TrafficSign& astas_sign, const messages::map::TrafficSign& proto_sign)
{
    EXPECT_EQ(proto_sign.id(), astas_sign.id);
    EXPECT_EQ(proto_sign.stvo_id(), astas_sign.stvo_id);
    EXPECT_EQ(proto_sign.sign_variability(), AstasToProtoSignVariability(astas_sign.variability));
    EXPECT_EQ(proto_sign.direction_scope(), AstasToProtoDirectionScope(astas_sign.direction_scope));
    EXPECT_EQ(proto_sign.main_sign_type(), AstasToProtoMainSign(astas_sign.type));

    ASSERT_FALSE(proto_sign.value_information().empty());
    EXPECT_EQ(proto_sign.value_information(0).text(), astas_sign.value_information.text);
    EXPECT_DOUBLE_EQ(proto_sign.value_information(0).value(), astas_sign.value_information.value);
    EXPECT_EQ(proto_sign.value_information(0).unit(), AstasToProtoSignUnit(astas_sign.value_information.value_unit));

    ASSERT_FALSE(proto_sign.assigned_lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_sign.assigned_lanes_size()), astas_sign.assigned_lanes.size());
    EXPECT_EQ(proto_sign.assigned_lanes(0), astas_sign.assigned_lanes[0]);

    CheckPose(astas_sign.pose, proto_sign.pose());
    CheckDimension3(astas_sign.dimensions, proto_sign.dimension());
}

void CheckMountedSign(const environment::map::MountedSign& astas_sign, const messages::map::MountedSign& proto_sign)
{
    CheckTrafficSign(astas_sign, proto_sign.sign());

    ASSERT_FALSE(proto_sign.supplementary_signs().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_sign.supplementary_signs_size()), astas_sign.supplementary_signs.size());
    CheckSupplementarySign(astas_sign.supplementary_signs[0], proto_sign.supplementary_signs(0));
}

void CheckGroundSign(const environment::map::GroundSign& astas_sign, const messages::map::GroundSign& proto_sign)
{
    CheckTrafficSign(astas_sign, proto_sign.sign());

    EXPECT_EQ(proto_sign.road_marking_type(), AstasToProtoRoadMarkingType(astas_sign.marking_type));
    EXPECT_EQ(proto_sign.road_marking_color(), AstasToProtoRoadMarkingColor(astas_sign.marking_color));
}

void CheckTrafficLightBulb(const environment::map::TrafficLightBulb& astas_traffic_light_bulb,
                           const messages::map::TrafficLight_LightBulb& proto_traffic_light_bulb)
{
    EXPECT_EQ(proto_traffic_light_bulb.id(), astas_traffic_light_bulb.id);
    EXPECT_EQ(proto_traffic_light_bulb.color(), AstasToProtoOsiTrafficLightColor(astas_traffic_light_bulb.color));
    EXPECT_EQ(proto_traffic_light_bulb.icon(), AstasToProtoOsiTrafficLightIcon(astas_traffic_light_bulb.icon));
    EXPECT_EQ(proto_traffic_light_bulb.mode(), AstasToProtoOsiTrafficLightMode(astas_traffic_light_bulb.mode));

    if (astas_traffic_light_bulb.mode == environment::map::OsiTrafficLightMode::kCounting)
    {
        EXPECT_DOUBLE_EQ(proto_traffic_light_bulb.value(), astas_traffic_light_bulb.count);
    }

    ASSERT_EQ(static_cast<std::size_t>(proto_traffic_light_bulb.assigned_lanes().size()),
              astas_traffic_light_bulb.assigned_lanes.size());
    for (std::size_t i{0}; i < astas_traffic_light_bulb.assigned_lanes.size(); ++i)
    {
        EXPECT_EQ(proto_traffic_light_bulb.assigned_lanes(static_cast<int>(i)),
                  astas_traffic_light_bulb.assigned_lanes[i]);
    }

    CheckPose(astas_traffic_light_bulb.pose, proto_traffic_light_bulb.pose());
    CheckDimension3(astas_traffic_light_bulb.dimensions, proto_traffic_light_bulb.dimension());
}

void CheckTrafficLight(const environment::map::TrafficLight& astas_traffic_light,
                       const messages::map::TrafficLight& proto_traffic_light)
{
    EXPECT_EQ(proto_traffic_light.id(), astas_traffic_light.id);

    ASSERT_EQ(astas_traffic_light.light_bulbs.size(),
              static_cast<std::size_t>(proto_traffic_light.light_bulbs().size()));
    for (std::size_t i{0}; i < astas_traffic_light.light_bulbs.size(); ++i)
    {
        CheckTrafficLightBulb(astas_traffic_light.light_bulbs[i], proto_traffic_light.light_bulbs(static_cast<int>(i)));
    }
}

void CheckLaneBoundary(const environment::map::LaneBoundary& astas_boundary,
                       const messages::map::LaneBoundary& proto_boundary)
{
    EXPECT_EQ(proto_boundary.id(), astas_boundary.id);
    EXPECT_EQ(proto_boundary.mirrored_from(), astas_boundary.mirrored_from);
    EXPECT_EQ(proto_boundary.parent_lane_group(), astas_boundary.parent_lane_group_id);
    EXPECT_EQ(proto_boundary.color(), AstasToProtoBoundaryColor(astas_boundary.color));
    EXPECT_EQ(proto_boundary.type(), AstasToProtoBoundaryType(astas_boundary.type));

    ASSERT_FALSE(proto_boundary.points().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_boundary.points_size()), astas_boundary.points.size());
    for (std::size_t i = 0; i < astas_boundary.points.size(); ++i)
    {
        const auto& astas_boundary_point = astas_boundary.points[i];
        const auto& proto_boundary_point = proto_boundary.points(static_cast<int>(i));
        EXPECT_DOUBLE_EQ(proto_boundary_point.width(), astas_boundary_point.width);
        EXPECT_DOUBLE_EQ(proto_boundary_point.height(), astas_boundary_point.height);
        CheckVector3(astas_boundary_point.position, proto_boundary_point.position());
    }
}

void CheckLane(const environment::map::Lane& astas_lane, const messages::map::Lane& proto_lane)
{
    EXPECT_EQ(proto_lane.id(), astas_lane.id);
    EXPECT_EQ(proto_lane.type(), AstasToProtoLaneType(astas_lane.flags));
    EXPECT_EQ(proto_lane.parent_lane_group(), astas_lane.parent_lane_group_id);

    ASSERT_FALSE(proto_lane.hd_center_line().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.hd_center_line_size()), astas_lane.center_line.size());
    for (std::size_t i = 0; i < astas_lane.center_line.size(); ++i)
    {
        CheckVector3(astas_lane.center_line[i], proto_lane.hd_center_line(static_cast<int>(i)));
    }

    ASSERT_FALSE(proto_lane.predecessors().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.predecessors_size()), astas_lane.predecessors.size());
    for (std::size_t i = 0; i < astas_lane.predecessors.size(); ++i)
    {
        EXPECT_EQ(proto_lane.predecessors(static_cast<int>(i)), astas_lane.predecessors[i]);
    }
    ASSERT_FALSE(proto_lane.successors().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.successors_size()), astas_lane.successors.size());
    for (std::size_t i = 0; i < astas_lane.successors.size(); ++i)
    {
        EXPECT_EQ(proto_lane.successors(static_cast<int>(i)), astas_lane.successors[i]);
    }

    ASSERT_FALSE(proto_lane.left_adjacent_lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.left_adjacent_lanes_size()), astas_lane.left_adjacent_lanes.size());
    for (std::size_t i = 0; i < astas_lane.left_adjacent_lanes.size(); ++i)
    {
        EXPECT_EQ(proto_lane.left_adjacent_lanes(static_cast<int>(i)), astas_lane.left_adjacent_lanes[i]);
    }
    ASSERT_FALSE(proto_lane.right_adjacent_lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.right_adjacent_lanes_size()), astas_lane.right_adjacent_lanes.size());
    for (std::size_t i = 0; i < astas_lane.right_adjacent_lanes.size(); ++i)
    {
        EXPECT_EQ(proto_lane.right_adjacent_lanes(static_cast<int>(i)), astas_lane.right_adjacent_lanes[i]);
    }

    ASSERT_FALSE(proto_lane.left_lane_boundaries().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.left_lane_boundaries_size()), astas_lane.left_lane_boundaries.size());
    for (std::size_t i = 0; i < astas_lane.left_lane_boundaries.size(); ++i)
    {
        EXPECT_EQ(proto_lane.left_lane_boundaries(static_cast<int>(i)), astas_lane.left_lane_boundaries[i]);
    }
    ASSERT_FALSE(proto_lane.right_lane_boundaries().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_lane.right_lane_boundaries_size()),
              astas_lane.right_lane_boundaries.size());
    for (std::size_t i = 0; i < astas_lane.right_lane_boundaries.size(); ++i)
    {
        EXPECT_EQ(proto_lane.right_lane_boundaries(static_cast<int>(i)), astas_lane.right_lane_boundaries[i]);
    }
}

void CheckLaneGroup(const environment::map::AstasMap& astas_map,
                    const environment::map::LaneGroup& astas_group,
                    const messages::map::LaneGroup& proto_group)
{
    EXPECT_EQ(proto_group.id(), astas_group.id);
    EXPECT_EQ(proto_group.type(), AstasToProtoLaneGroupType(astas_group.type));

    ASSERT_FALSE(proto_group.lane_boundaries().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_group.lane_boundaries_size()), astas_group.lane_boundary_ids.size());
    const auto& astas_boundary = astas_map.GetLaneBoundary(astas_group.lane_boundary_ids[0]);
    CheckLaneBoundary(astas_boundary, proto_group.lane_boundaries(0));

    ASSERT_FALSE(proto_group.lanes().empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_group.lanes_size()), astas_group.lane_ids.size());
    const auto& astas_lane = astas_map.GetLane(astas_group.lane_ids[0]);
    CheckLane(astas_lane, proto_group.lanes(0));
}

}  // namespace astas::communication
