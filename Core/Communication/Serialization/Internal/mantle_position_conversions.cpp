/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/mantle_position_conversions.h"

#include "Core/Service/Logging/logging.h"

namespace astas::communication
{

void FillLatLonProtoPosition(const mantle_api::Position& position, messages::scenario::LatLonPosition* proto_position)
{
    auto nds_pos = std::get<mantle_api::LatLonPosition>(position);
    proto_position->set_latitude(units::angle::degree_t(nds_pos.latitude)());
    proto_position->set_longitude(units::angle::degree_t(nds_pos.longitude)());
}

void FillOdrRoadProtoPosition(const mantle_api::Position& position,
                              messages::scenario::OpenDrivePosition* proto_position)
{
    auto odr_road_pos = std::get<mantle_api::OpenDriveRoadPosition>(position);
    proto_position->set_road_id(odr_road_pos.road == "" ? 0 : std::stoi(odr_road_pos.road));
    proto_position->set_lane_id(0);  // center lane
    proto_position->set_s_offset(odr_road_pos.s_offset());
    proto_position->set_t_offset(odr_road_pos.t_offset());
}

void FillOdrLaneProtoPosition(const mantle_api::Position& position,
                              messages::scenario::OpenDrivePosition* proto_position)
{
    auto odr_lane_pos = std::get<mantle_api::OpenDriveLanePosition>(position);
    proto_position->set_road_id(odr_lane_pos.road == "" ? 0 : std::stoi(odr_lane_pos.road));
    proto_position->set_lane_id(odr_lane_pos.lane);
    proto_position->set_s_offset(odr_lane_pos.s_offset());
    proto_position->set_t_offset(odr_lane_pos.t_offset());
}

void FillPosition(const mantle_api::Position& position, messages::scenario::AstasPosition* proto_position)
{
    if (std::holds_alternative<mantle_api::OpenDriveRoadPosition>(position))
    {
        auto* proto_odr_pos = proto_position->mutable_open_drive_position();
        FillOdrRoadProtoPosition(position, proto_odr_pos);
    }
    else if (std::holds_alternative<mantle_api::OpenDriveLanePosition>(position))
    {
        auto* proto_odr_pos = proto_position->mutable_open_drive_position();
        FillOdrLaneProtoPosition(position, proto_odr_pos);
    }
    else if (std::holds_alternative<mantle_api::LatLonPosition>(position))
    {
        auto* proto_latlon_pos = proto_position->mutable_lat_lon_position();
        FillLatLonProtoPosition(position, proto_latlon_pos);
    }
    else
    {
        ASSERT(false && "Called FillPosition(mantle_api::Position), but no valid variant is set!")
    }
}

void FillEditorResponsePosition(const mantle_api::Position& position, messages::editor::EditorApi* editor_api)
{
    if (std::holds_alternative<mantle_api::OpenDriveRoadPosition>(position))
    {
        editor_api->set_response(messages::editor::EDITOR_RESPONSE_ODR_POSITION);
        auto* proto_position = editor_api->mutable_odr_position();
        FillOdrRoadProtoPosition(position, proto_position);
    }
    else if (std::holds_alternative<mantle_api::OpenDriveLanePosition>(position))
    {
        editor_api->set_response(messages::editor::EDITOR_RESPONSE_ODR_POSITION);
        auto* proto_position = editor_api->mutable_odr_position();
        FillOdrLaneProtoPosition(position, proto_position);
    }
    else if (std::holds_alternative<mantle_api::LatLonPosition>(position))
    {
        editor_api->set_response(messages::editor::EDITOR_RESPONSE_NDS_POSITION);
        auto* proto_position = editor_api->mutable_lat_lon_position();
        FillLatLonProtoPosition(position, proto_position);
    }
    else
    {
        // if you want to send a world position, use the constructor that provides a pose!
        ASSERT(false && "Called EditorApiResponse(service::scenario::Position), but no valid variant is set!")
    }
}

}  // namespace astas::communication
