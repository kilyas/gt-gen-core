/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Communication/Serialization/Internal/map_proto_conversion.h"

#include "Core/Communication/Serialization/Internal/map_type_conversions.h"
#include "Core/Communication/Serialization/Internal/sign_type_conversions.h"
#include "Core/Communication/Serialization/Internal/traffic_light_bulb_type_conversions.h"
#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"

#include <MantleAPI/Traffic/i_entity_repository.h>
#include <MantleAPI/Traffic/traffic_light_properties.h>

namespace astas::communication
{

using google::protobuf::RepeatedPtrField;

void FillLaneBoundary(const environment::map::LaneBoundary& astas_boundary, messages::map::LaneBoundary* proto_boundary)
{
    proto_boundary->set_id(astas_boundary.id);
    proto_boundary->set_parent_lane_group(astas_boundary.parent_lane_group_id);
    proto_boundary->set_mirrored_from(astas_boundary.mirrored_from);

    for (const auto& boundary_point : astas_boundary.points)
    {
        auto* proto_point = proto_boundary->mutable_points()->Add();
        service::gt_conversion::FillProtoObject(boundary_point.position, proto_point->mutable_position());
        proto_point->set_width(boundary_point.width);
        proto_point->set_height(boundary_point.height);
    }

    proto_boundary->set_color(AstasToProtoBoundaryColor(astas_boundary.color));
    proto_boundary->set_type(AstasToProtoBoundaryType(astas_boundary.type));
}

void FillLane(const environment::map::Lane& astas_lane, messages::map::Lane* proto_lane)
{
    proto_lane->set_id(astas_lane.id);
    proto_lane->set_center_line_is_driving_direction(astas_lane.flags.IsDrivable());
    proto_lane->set_parent_lane_group(astas_lane.parent_lane_group_id);
    proto_lane->set_type(AstasToProtoLaneType(astas_lane.flags));

    for (const auto& center_line_point : astas_lane.center_line)
    {
        auto* proto_point = proto_lane->mutable_hd_center_line()->Add();
        service::gt_conversion::FillProtoObject(center_line_point, proto_point);
    }

    for (const auto& lane : astas_lane.predecessors)
    {
        proto_lane->add_predecessors(lane);
    }
    for (const auto& lane : astas_lane.successors)
    {
        proto_lane->add_successors(lane);
    }
    for (const auto& lane : astas_lane.left_adjacent_lanes)
    {
        proto_lane->add_left_adjacent_lanes(lane);
    }
    for (const auto& lane : astas_lane.right_adjacent_lanes)
    {
        proto_lane->add_right_adjacent_lanes(lane);
    }
    for (const auto& lane : astas_lane.left_lane_boundaries)
    {
        proto_lane->add_left_lane_boundaries(lane);
    }
    for (const auto& lane : astas_lane.right_lane_boundaries)
    {
        proto_lane->add_right_lane_boundaries(lane);
    }
}

void FillLaneGroups(const environment::map::AstasMap& astas_map,
                    RepeatedPtrField<messages::map::LaneGroup>* proto_lane_groups)
{
    for (const auto& lane_group : astas_map.GetLaneGroups())
    {
        auto* proto_lane_group = proto_lane_groups->Add();
        proto_lane_group->set_id(lane_group.id);
        proto_lane_group->set_type(AstasToProtoLaneGroupType(lane_group.type));

        for (const auto& lane_boundary_id : lane_group.lane_boundary_ids)
        {
            const auto& astas_lane_boundary = astas_map.GetLaneBoundary(lane_boundary_id);
            auto* proto_lane_boundary = proto_lane_group->mutable_lane_boundaries()->Add();
            FillLaneBoundary(astas_lane_boundary, proto_lane_boundary);
        }

        for (const auto& lane_id : lane_group.lane_ids)
        {
            const auto& astas_lane = astas_map.GetLane(lane_id);
            auto* proto_lane = proto_lane_group->mutable_lanes()->Add();
            FillLane(astas_lane, proto_lane);
        }
    }
}

void FillRoadObjects(const environment::map::RoadObjects& astas_road_objects,
                     RepeatedPtrField<messages::map::RoadObject>* proto_road_objects)
{
    for (const auto& road_object : astas_road_objects)
    {
        auto* proto_road_object = proto_road_objects->Add();
        proto_road_object->set_id(road_object.id);
        proto_road_object->set_name(road_object.name);
        proto_road_object->set_height(road_object.dimensions.height());
        proto_road_object->set_type(AstasToProtoRoadObjectType(road_object.type));
        service::gt_conversion::FillProtoObject(road_object.pose.position,
                                                proto_road_object->mutable_pose()->mutable_position());
        service::gt_conversion::FillProtoObject(road_object.pose.orientation,
                                                proto_road_object->mutable_pose()->mutable_orientation());
        service::gt_conversion::FillProtoObject(road_object.dimensions, proto_road_object->mutable_dimensions());

        for (const auto& point : road_object.base_polygon)
        {
            auto* proto_point = proto_road_object->mutable_base_polygon()->Add();
            service::gt_conversion::FillProtoObject(point, proto_point);
        }
    }
}

void FillSupplementarySign(const environment::map::MountedSign::SupplementarySign& astas_sign,
                           messages::map::TrafficSign* proto_sign)
{
    service::gt_conversion::FillProtoObject(astas_sign.pose.position, proto_sign->mutable_pose()->mutable_position());
    service::gt_conversion::FillProtoObject(astas_sign.pose.orientation,
                                            proto_sign->mutable_pose()->mutable_orientation());
    service::gt_conversion::FillProtoObject(astas_sign.dimensions, proto_sign->mutable_dimension());

    for (const auto& value_info : astas_sign.value_information)
    {
        auto* value_information = proto_sign->mutable_value_information()->Add();
        value_information->set_text(value_info.text);
        value_information->set_value(value_info.value);
        value_information->set_unit(AstasToProtoSignUnit(value_info.value_unit));
    }

    proto_sign->set_sign_variability(AstasToProtoSignVariability(astas_sign.variability));
    proto_sign->set_supplementary_sign_type(AstasToProtoSupplementarySign(astas_sign.type));
}

void FillTrafficSign(const environment::map::TrafficSign& astas_sign, messages::map::TrafficSign* proto_sign)
{
    proto_sign->set_id(astas_sign.id);
    proto_sign->set_stvo_id(astas_sign.stvo_id);

    service::gt_conversion::FillProtoObject(astas_sign.pose.position, proto_sign->mutable_pose()->mutable_position());
    service::gt_conversion::FillProtoObject(astas_sign.pose.orientation,
                                            proto_sign->mutable_pose()->mutable_orientation());
    service::gt_conversion::FillProtoObject(astas_sign.dimensions, proto_sign->mutable_dimension());

    for (const auto assigned_lane : astas_sign.assigned_lanes)
    {
        proto_sign->add_assigned_lanes(assigned_lane);
    }

    auto* value_information = proto_sign->mutable_value_information()->Add();
    value_information->set_text(astas_sign.value_information.text);
    value_information->set_value(astas_sign.value_information.value);
    value_information->set_unit(AstasToProtoSignUnit(astas_sign.value_information.value_unit));

    proto_sign->set_sign_variability(AstasToProtoSignVariability(astas_sign.variability));
    proto_sign->set_direction_scope(AstasToProtoDirectionScope(astas_sign.direction_scope));
    proto_sign->set_main_sign_type(AstasToProtoMainSign(astas_sign.type));
}

void FillGroundSigns(const environment::map::GroundSigns& astas_ground_signs,
                     RepeatedPtrField<messages::map::GroundSign>* proto_ground_signs)
{
    for (const auto& astas_sign : astas_ground_signs)
    {
        auto* proto_ground_sign = proto_ground_signs->Add();
        proto_ground_sign->set_road_marking_type(AstasToProtoRoadMarkingType(astas_sign->marking_type));
        proto_ground_sign->set_road_marking_color(AstasToProtoRoadMarkingColor(astas_sign->marking_color));
        FillTrafficSign(*astas_sign, proto_ground_sign->mutable_sign());
    }
}

void FillFixedFrictionPatches(const environment::map::FixedFrictionPatches& astas_fixed_friction_patches,
                              RepeatedPtrField<messages::map::FixedFrictionPatch>* proto_fixed_friction_patches)
{
    for (const auto& astas_friction_patch : astas_fixed_friction_patches)
    {
        auto* proto_friction_patch = proto_fixed_friction_patches->Add();
        proto_friction_patch->set_mue(astas_friction_patch->mue);

        for (const auto& shape_3d_point : astas_friction_patch->shape_3d.points)
        {
            auto proto_patch_shape = proto_friction_patch->mutable_patch()->add_shape_3d();
            proto_patch_shape->set_x(shape_3d_point.x());
            proto_patch_shape->set_y(shape_3d_point.y());
            proto_patch_shape->set_z(shape_3d_point.z());
        }
    }
}

void FillMountedSigns(const environment::map::MountedSigns& astas_mounted_signs,
                      RepeatedPtrField<messages::map::MountedSign>* proto_mounted_signs)
{
    for (const auto& astas_sign : astas_mounted_signs)
    {
        auto* proto_mounted_sign = proto_mounted_signs->Add();
        FillTrafficSign(*astas_sign, proto_mounted_sign->mutable_sign());

        for (const auto& supplementary_sign : astas_sign->supplementary_signs)
        {
            auto* proto_supplementary_sign = proto_mounted_sign->mutable_supplementary_signs()->Add();
            FillSupplementarySign(supplementary_sign, proto_supplementary_sign);
        }
    }
}

void FillTrafficLightBulb(const mantle_ext::TrafficLightBulbProperties& bulb_properties,
                          const environment::entities::TrafficLightEntity* entity,
                          messages::map::TrafficLight_LightBulb* proto_traffic_light_bulb)
{
    proto_traffic_light_bulb->set_id(bulb_properties.id);

    auto bulb_position = entity->GetBulbPosition(bulb_properties.id);

    service::gt_conversion::FillProtoObject(bulb_position,
                                            proto_traffic_light_bulb->mutable_pose()->mutable_position());
    service::gt_conversion::FillProtoObject(entity->GetOrientation(),
                                            proto_traffic_light_bulb->mutable_pose()->mutable_orientation());
    service::gt_conversion::FillProtoObject(bulb_properties.bounding_box.dimension,
                                            proto_traffic_light_bulb->mutable_dimension());

    proto_traffic_light_bulb->set_color(static_cast<messages::map::TrafficLightColor>(bulb_properties.color));
    proto_traffic_light_bulb->set_icon(static_cast<messages::map::TrafficLightIcon>(bulb_properties.icon));
    proto_traffic_light_bulb->set_mode(static_cast<messages::map::TrafficLightMode>(bulb_properties.mode));

    if (bulb_properties.mode == mantle_api::TrafficLightBulbMode::kCounting)
    {
        proto_traffic_light_bulb->set_value(static_cast<std::int32_t>(bulb_properties.count));
    }

    for (const mantle_api::UniqueId astas_lane_id : entity->GetAssignedLaneIds())
    {
        proto_traffic_light_bulb->mutable_assigned_lanes()->Add(astas_lane_id);
    }
}

void FillTrafficLightBulb(const environment::map::TrafficLightBulb& astas_traffic_light_bulb,
                          messages::map::TrafficLight_LightBulb* proto_traffic_light_bulb)
{
    proto_traffic_light_bulb->set_id(astas_traffic_light_bulb.id);
    service::gt_conversion::FillProtoObject(astas_traffic_light_bulb.pose.position,
                                            proto_traffic_light_bulb->mutable_pose()->mutable_position());
    service::gt_conversion::FillProtoObject(astas_traffic_light_bulb.pose.orientation,
                                            proto_traffic_light_bulb->mutable_pose()->mutable_orientation());
    service::gt_conversion::FillProtoObject(astas_traffic_light_bulb.dimensions,
                                            proto_traffic_light_bulb->mutable_dimension());
    proto_traffic_light_bulb->set_color(AstasToProtoOsiTrafficLightColor(astas_traffic_light_bulb.color));
    proto_traffic_light_bulb->set_icon(AstasToProtoOsiTrafficLightIcon(astas_traffic_light_bulb.icon));
    proto_traffic_light_bulb->set_mode(AstasToProtoOsiTrafficLightMode(astas_traffic_light_bulb.mode));

    if (astas_traffic_light_bulb.mode == environment::map::OsiTrafficLightMode::kCounting)
    {
        proto_traffic_light_bulb->set_value(static_cast<std::int32_t>(astas_traffic_light_bulb.count));
    }

    for (const mantle_api::UniqueId astas_lane_id : astas_traffic_light_bulb.assigned_lanes)
    {
        proto_traffic_light_bulb->mutable_assigned_lanes()->Add(astas_lane_id);
    }
}

void FillTrafficLights(const mantle_api::IEntityRepository* entity_repository,
                       RepeatedPtrField<messages::map::TrafficLight>* proto_traffic_lights)
{
    for (const auto& entity : entity_repository->GetEntities())
    {
        if (entity->GetProperties()->type == mantle_api::EntityType::kStatic)
        {
            const auto* traffic_light_entity = dynamic_cast<environment::entities::TrafficLightEntity*>(entity.get());
            if (traffic_light_entity != nullptr)
            {
                auto* proto_traffic_light = proto_traffic_lights->Add();
                proto_traffic_light->set_id(entity->GetUniqueId());

                const auto* properties = traffic_light_entity->GetProperties();
                for (const auto& bulb : properties->bulbs)
                {
                    FillTrafficLightBulb(bulb, traffic_light_entity, proto_traffic_light->mutable_light_bulbs()->Add());
                }
            }
        }
    }
}

void FillTrafficLights(const environment::map::TrafficLights& astas_traffic_lights,
                       RepeatedPtrField<messages::map::TrafficLight>* proto_traffic_lights)
{
    for (const environment::map::TrafficLight& astas_traffic_light : astas_traffic_lights)
    {
        auto* proto_traffic_light = proto_traffic_lights->Add();
        proto_traffic_light->set_id(astas_traffic_light.id);

        for (const environment::map::TrafficLightBulb& astas_traffic_light_bulb : astas_traffic_light.light_bulbs)
        {
            FillTrafficLightBulb(astas_traffic_light_bulb, proto_traffic_light->mutable_light_bulbs()->Add());
        }
    }
}

void FillMapProtoRepresentation(const environment::map::AstasMap& map,
                                const mantle_api::IEntityRepository* entity_repository,
                                messages::map::Map* proto_message)
{
    proto_message->set_is_odr(map.IsOpenDrive());
    proto_message->set_map_path(map.path);
    proto_message->set_projection_string(map.projection_string);

    FillLaneGroups(map, proto_message->mutable_lane_groups());
    FillRoadObjects(map.road_objects, proto_message->mutable_road_objects());
    FillMountedSigns(map.GetMountedSigns(), proto_message->mutable_mounted_signs());
    FillGroundSigns(map.GetGroundSigns(), proto_message->mutable_ground_signs());
    FillFixedFrictionPatches(map.GetFixedFrictionPatches(), proto_message->mutable_fixed_friction_patches());
    FillTrafficLights(map.traffic_lights, proto_message->mutable_traffic_lights());
    FillTrafficLights(entity_repository, proto_message->mutable_traffic_lights());
}

void FillChunkingProtoRepresentation(const environment::chunking::StaticChunkList& chunks,
                                     messages::map::Map* proto_message)
{
    auto* proto_chunks = proto_message->mutable_world_chunks();

    for (const auto& [key, chunk] : chunks)
    {
        auto* proto_chunk = proto_chunks->Add();
        proto_chunk->mutable_key()->set_i(key.i);
        proto_chunk->mutable_key()->set_j(key.j);

        proto_chunk->mutable_origin()->set_x(chunk.lower_left.x());
        proto_chunk->mutable_origin()->set_y(chunk.lower_left.y());

        for (const auto& lane_group : chunk.lane_groups)
        {
            proto_chunk->mutable_lane_groups()->Add(lane_group->id);
        }
        for (const auto& traffic_sign : chunk.traffic_signs)
        {
            proto_chunk->mutable_traffic_signs()->Add(traffic_sign->id);
        }
        for (const auto& road_object : chunk.road_objects)
        {
            proto_chunk->mutable_road_objects()->Add(road_object->id);
        }
        for (const auto& traffic_light : chunk.traffic_lights)
        {
            proto_chunk->mutable_traffic_lights()->Add(traffic_light->id);
        }
    }
}

}  // namespace astas::communication
