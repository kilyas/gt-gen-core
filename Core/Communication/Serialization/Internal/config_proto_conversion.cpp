/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/config_proto_conversion.h"

#include "Core/Service/Logging/logging.h"

namespace astas::communication
{
namespace detail
{

messages::config::Movement AstasToProtoHostMovement(const service::user_settings::HostControlMode host_control_mode)
{
    switch (host_control_mode)
    {
        case service::user_settings::HostControlMode::kInternal:
        {
            return messages::config::Movement::HOST_MOVEMENT_ASTAS_MODE;
        }
        case service::user_settings::HostControlMode::kExternal:
        default:
        {
            return messages::config::Movement::HOST_MOVEMENT_EXTERNAL_VEHICLE;
        }
    }
}

messages::config::LogLevel AstasToProtoLogLevel(const std::string& astas_level)
{
    using proto_type = messages::config::LogLevel;

    if (astas_level == "Trace")
    {
        return proto_type::LOG_LEVEL_TRACE;
    }
    else if (astas_level == "Debug")
    {
        return proto_type::LOG_LEVEL_DEBUG;
    }
    else if (astas_level == "Info")
    {
        return proto_type::LOG_LEVEL_INFO;
    }
    else if (astas_level == "Warn")
    {
        return proto_type::LOG_LEVEL_WARN;
    }
    else if (astas_level == "Error")
    {
        return proto_type::LOG_LEVEL_ERROR;
    }
    else if (astas_level == "Off")
    {
        return proto_type::LOG_LEVEL_OFF;
    }
    else
    {
        Warn("Couldn't convert log level {} to proto representation for GUI. Will use INFO as default.");
        return proto_type::LOG_LEVEL_INFO;
    }
}

void FillGeneralSection(const service::user_settings::UserSettings& user_settings,
                        const std::string& user_settings_path,
                        messages::config::General* proto_message)
{
    proto_message->set_settings_path(user_settings_path);

    /// TODO: remove the console log level and remove it also from proto definition + change ASTAS_GUI accordingly
    proto_message->set_console_log_level(AstasToProtoLogLevel("Info"));
    proto_message->set_file_log_level(AstasToProtoLogLevel(user_settings.file_logging.log_level));
}

void FillGroundTruthSection(const service::user_settings::GroundTruth& astas_ground_truth,
                            messages::config::GroundTruth* proto_message)
{
    proto_message->set_lane_marking_distance(static_cast<float>(astas_ground_truth.lane_marking_distance_in_m));
    proto_message->set_allow_invalid_lane_locations(astas_ground_truth.allow_invalid_lane_locations);
}

void FillHostVehicleSection(const service::user_settings::HostVehicle& astas_host_vehicle,
                            messages::config::HostVehicle* proto_message)
{
    proto_message->set_blocking_communication(astas_host_vehicle.blocking_communication);
    proto_message->set_recovery_mode(astas_host_vehicle.recovery_mode);
    proto_message->set_time_scale(static_cast<float>(astas_host_vehicle.time_scale));
    proto_message->set_movement(AstasToProtoHostMovement(astas_host_vehicle.host_control_mode));
}

void FillMapSection(const service::user_settings::Map& astas_map_config, messages::config::Map* proto_message)
{
    // TODO: remove this line, when removed from the proto definition and astas_gui has been fixed, too
    proto_message->set_ignore_road_elevation(false);
    proto_message->set_include_obstacles(astas_map_config.include_obstacles);
}

void FillMapChunkingSection(const service::user_settings::MapChunking& astas_map_chunking,
                            messages::config::MapChunking* proto_message)
{
    proto_message->set_cells_per_direction(astas_map_chunking.cells_per_direction);
    proto_message->set_chunk_grid_size(astas_map_chunking.chunk_grid_size);
}

}  // namespace detail

void FillConfigProtoRepresentation(const service::user_settings::UserSettings& user_settings,
                                   const std::string& user_settings_path,
                                   messages::config::Config* proto_message)
{
    detail::FillGeneralSection(user_settings, user_settings_path, proto_message->mutable_general());
    detail::FillGroundTruthSection(user_settings.ground_truth, proto_message->mutable_ground_truth());
    detail::FillHostVehicleSection(user_settings.host_vehicle, proto_message->mutable_host_vehicle());
    detail::FillMapSection(user_settings.map, proto_message->mutable_map());
    detail::FillMapChunkingSection(user_settings.map_chunking, proto_message->mutable_map_chunking());
}

}  // namespace astas::communication
