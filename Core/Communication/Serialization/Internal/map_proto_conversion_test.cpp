/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/map_proto_conversion.h"

#include "Core/Communication/Serialization/Internal/conversion_test_helper.h"
#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MapUtils/astas_map_builder.h"

#include <gtest/gtest.h>

namespace astas::communication
{
using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_deg;

mantle_api::Pose CreateTestPose()
{
    mantle_api::Vec3<units::length::meter_t> position{1.1_m, -2.2_m, 3.3_m};
    mantle_api::Orientation3<units::angle::radian_t> orientation{3.14_rad, 1.57_rad, -1_rad};
    return {position, orientation};
}

void FillTestSignValues(environment::map::TrafficSign& sign)
{
    sign.id = 69;
    sign.stvo_id = "what_if";
    sign.pose = CreateTestPose();
    sign.dimensions = {1.1_m, 2.2_m, 3.3_m};
    sign.assigned_lanes = {{12, 8338}};
    sign.variability = osi::OsiTrafficSignVariability::kFixed;
    sign.direction_scope = osi::OsiTrafficSignDirectionScope::kLeftRight;
    sign.type = osi::OsiTrafficSignType::kHorseRidersOnly;
    sign.value_information.value_unit = osi::OsiTrafficSignValueUnit::kPercentage;
    sign.value_information.value = 1234567;
    sign.value_information.text = "I take the blue pill";
}

void FillTestTrafficLightValues(environment::map::TrafficLight& traffic_light)
{
    traffic_light.id = 107;

    auto& white_light_bulb = traffic_light.light_bulbs.emplace_back();
    white_light_bulb.id = 19;
    white_light_bulb.dimensions = {1.1_m, 2.2_m, 3.3_m};
    white_light_bulb.pose = CreateTestPose();
    white_light_bulb.color = environment::map::OsiTrafficLightColor::kWhite;
    white_light_bulb.icon = environment::map::OsiTrafficLightIcon::kTram;
    white_light_bulb.mode = environment::map::OsiTrafficLightMode::kConstant;
    white_light_bulb.assigned_lanes = {4323};

    auto& blue_light_bulb = traffic_light.light_bulbs.emplace_back();
    blue_light_bulb.id = 88;
    blue_light_bulb.dimensions = {1.1_m, 2.2_m, 3.3_m};
    blue_light_bulb.pose = CreateTestPose();
    blue_light_bulb.color = environment::map::OsiTrafficLightColor::kBlue;
    blue_light_bulb.icon = environment::map::OsiTrafficLightIcon::kCountdownSeconds;
    blue_light_bulb.mode = environment::map::OsiTrafficLightMode::kCounting;
    blue_light_bulb.count = 45.;
    blue_light_bulb.assigned_lanes = {1991};
}

void CreateTestLaneGroup(test_utils::AstasMapBuilder& map_builder)
{
    const mantle_api::UniqueId lane_group_id = 1;
    const mantle_api::UniqueId lane_id = 2;

    environment::map::LaneBoundary::Points boundary_points{{{7_m, 8_m, 9_m}, 0, 0}, {{10_m, 11_m, 12_m}, 0, 0}};
    environment::map::LaneBoundary::Points more_boundary_points{{{13_m, 14_m, 15_m}, 0, 0}, {{16_m, 17_m, 18_m}, 0, 0}};

    map_builder.SetIds(lane_group_id, lane_id);
    map_builder.SetCenterLine({{1_m, 2_m, 3_m}, {4_m, 5_m, 6_m}, {7_m, 8_m, 9_m}});
    map_builder.SetDrivable(true);
    map_builder.SetPredecessors({3, 5});
    map_builder.SetSuccessors({2, 4});
    map_builder.SetLeftAdjacentLanes({6, 8});
    map_builder.SetRightAdjacentLanes({7, 9});
    map_builder.SetLeftLaneBoundaries({42, 69});
    map_builder.SetRightBoundaries({boundary_points, more_boundary_points});
    map_builder.CreateLane();
}

TEST(MapToProtoConversionTest,
     GivenAstasMapWithAllSupportedObjects_WhenConvertedToProtoRepresentation_ThenAllObjectsExistInProtoMap)
{
    const auto full_map_path = "dummy/path/map.xodr";

    service::utility::UniqueIdProvider id_provider{};
    environment::api::EntityRepository entity_repository{&id_provider};

    std::unique_ptr<environment::map::AstasMap> astas_map_ptr{
        test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints()};
    auto& astas_map = *astas_map_ptr;

    // Fill in with dummy data just for testing conversion
    astas_map.SetSourceMapType(environment::map::AstasMap::SourceMapType::kOpenDrive);
    astas_map.path = full_map_path;
    astas_map.projection_string = "+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs";

    astas_map.road_objects.push_back(environment::map::RoadObject{.id = 1001});
    astas_map.road_objects.push_back(environment::map::RoadObject{.id = 1002});

    astas_map.traffic_signs.push_back(std::make_shared<environment::map::GroundSign>());
    astas_map.traffic_signs.back()->id = 1003;
    astas_map.traffic_signs.push_back(std::make_shared<environment::map::GroundSign>());
    astas_map.traffic_signs.back()->id = 1004;

    astas_map.traffic_signs.push_back(std::make_shared<environment::map::MountedSign>());
    astas_map.traffic_signs.back()->id = 1005;
    astas_map.traffic_signs.push_back(std::make_shared<environment::map::MountedSign>());
    astas_map.traffic_signs.back()->id = 1006;

    messages::map::Map proto_map{};
    FillMapProtoRepresentation(astas_map, &entity_repository, &proto_map);

    EXPECT_TRUE(proto_map.is_odr());
    EXPECT_EQ(proto_map.map_path(), full_map_path);
    EXPECT_EQ(proto_map.projection_string(), astas_map.projection_string);

    ASSERT_FALSE(astas_map.road_objects.empty());  // otherwise the next check is useless
    EXPECT_EQ(static_cast<std::size_t>(proto_map.road_objects_size()), astas_map.road_objects.size());

    ASSERT_FALSE(astas_map.GetGroundSigns().empty());
    EXPECT_EQ(static_cast<std::size_t>(proto_map.ground_signs_size()), astas_map.GetGroundSigns().size());

    // following tests assume that repeated proto fields have been filled in the same order
    // as they appear in their astas map counterpart

    const auto astas_mounted_signs = astas_map.GetMountedSigns();
    ASSERT_FALSE(astas_mounted_signs.empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_map.mounted_signs_size()), astas_mounted_signs.size());
    for (std::size_t i = 0; i < astas_mounted_signs.size(); ++i)
    {
        EXPECT_EQ(static_cast<std::size_t>(proto_map.mounted_signs(static_cast<int>(i)).supplementary_signs_size()),
                  astas_mounted_signs[i]->supplementary_signs.size());
    }

    const auto astas_lane_groups = astas_map.GetLaneGroups();
    ASSERT_FALSE(astas_lane_groups.empty());
    ASSERT_EQ(static_cast<std::size_t>(proto_map.lane_groups_size()), astas_lane_groups.size());
    for (std::size_t i = 0; i < astas_lane_groups.size(); ++i)
    {
        EXPECT_EQ(static_cast<std::size_t>(proto_map.lane_groups(static_cast<int>(i)).lanes_size()),
                  astas_lane_groups[i].lane_ids.size());
        EXPECT_EQ(static_cast<std::size_t>(proto_map.lane_groups(static_cast<int>(i)).lane_boundaries_size()),
                  astas_lane_groups[i].lane_boundary_ids.size());
    }

    std::vector<char> data(proto_map.ByteSize());
    EXPECT_NO_THROW(proto_map.SerializeToArray(data.data(), proto_map.ByteSize()));
}

TEST(MapToProtoConversionTest,
     GivenMapWithFrictionPatch_WhenConvertingToProtoRepresentation_ThenFrictionPatchesAreFilledCorrectly)
{
    environment::map::FixedFrictionPatch astas_fixed_friction_patch{
        42, {{{0_m, 0_m, 0_m}, {5_m, 0_m, 0_m}, {5_m, 3_m, 0_m}, {0_m, 3_m, 0_m}, {0_m, 0_m, 0_m}}}, 0.5};
    test_utils::AstasMapBuilder map_builder{};
    map_builder.AddPatch(std::make_shared<environment::map::FixedFrictionPatch>(astas_fixed_friction_patch));

    environment::api::EntityRepository entity_repository{nullptr};
    messages::map::Map proto_map{};
    FillMapProtoRepresentation(map_builder.astas_map, &entity_repository, &proto_map);

    EXPECT_EQ(proto_map.fixed_friction_patches().size(), 1);

    const auto& proto_friction_patch = proto_map.fixed_friction_patches(0);
    EXPECT_DOUBLE_EQ(astas_fixed_friction_patch.mue, proto_friction_patch.mue());

    EXPECT_TRUE(proto_friction_patch.has_patch());
    const auto& proto_patch_shape = proto_friction_patch.patch().shape_3d();
    for (int i = 0; i < proto_patch_shape.size(); ++i)
    {
        EXPECT_DOUBLE_EQ(astas_fixed_friction_patch.shape_3d.points.at(static_cast<std::size_t>(i)).x(),
                         proto_patch_shape.Get(i).x());
        EXPECT_DOUBLE_EQ(astas_fixed_friction_patch.shape_3d.points.at(static_cast<std::size_t>(i)).y(),
                         proto_patch_shape.Get(i).y());
        EXPECT_DOUBLE_EQ(astas_fixed_friction_patch.shape_3d.points.at(static_cast<std::size_t>(i)).z(),
                         proto_patch_shape.Get(i).z());
    }
}

TEST(MapToProtoConversionTest, GivenMapWithRoadObject_WhenConvertedToProtoRepresentation_ThenRoadObjectFilledCorrectly)
{
    environment::api::EntityRepository entity_repository{nullptr};
    environment::map::RoadObject astas_road_object{{},
                                                   "myBeautifulObject",
                                                   CreateTestPose(),
                                                   {11.1_m, 12.2_m, 13.3_m},
                                                   osi::StationaryObjectEntityType::kPylon,
                                                   osi::StationaryObjectEntityMaterial::kOther,
                                                   42};
    test_utils::AstasMapBuilder builder{};
    builder.AddRoadObject(astas_road_object);

    messages::map::Map proto_map{};
    FillMapProtoRepresentation(builder.astas_map, &entity_repository, &proto_map);

    ASSERT_EQ(proto_map.road_objects_size(), 1);
    CheckRoadObject(astas_road_object, proto_map.road_objects(0));
}

TEST(MapToProtoConversionTest,
     GivenMapWithMountedSign_WhenConvertedToProtoRepresentation_ThenMountedSignFilledCorrectly)
{
    environment::api::EntityRepository entity_repository{nullptr};
    environment::map::MountedSign astas_mounted_sign{};
    FillTestSignValues(astas_mounted_sign);
    environment::map::MountedSign::SupplementarySign suppl_sign{};
    suppl_sign.variability = osi::OsiTrafficSignVariability::kVariable;
    suppl_sign.type = environment::map::OsiSupplementarySignType::kHazardous;
    suppl_sign.value_information = {{"I take the red pill", 7654321, osi::OsiTrafficSignValueUnit::kMinutes}};
    suppl_sign.dimensions = {42.2_m, 43.3_m, 44.4_m};
    suppl_sign.pose = CreateTestPose();
    astas_mounted_sign.supplementary_signs = {suppl_sign};

    test_utils::AstasMapBuilder builder{};
    builder.AddTrafficSign(std::make_shared<environment::map::MountedSign>(astas_mounted_sign));

    messages::map::Map proto_map{};
    FillMapProtoRepresentation(builder.astas_map, &entity_repository, &proto_map);

    ASSERT_EQ(proto_map.mounted_signs_size(), 1);
    CheckMountedSign(astas_mounted_sign, proto_map.mounted_signs(0));
}

TEST(MapToProtoConversionTest, GivenMapWithGroundSign_WhenConvertedToProtoRepresentation_ThenGroundSignFilledCorrectly)
{
    environment::api::EntityRepository entity_repository{nullptr};
    environment::map::GroundSign astas_ground_sign{};
    FillTestSignValues(astas_ground_sign);
    astas_ground_sign.marking_color = osi::OsiRoadMarkingColor::kViolet;
    astas_ground_sign.marking_type = osi::OsiRoadMarkingsType::kSymbolicTrafficSign;

    test_utils::AstasMapBuilder builder{};
    builder.AddTrafficSign(std::make_shared<environment::map::GroundSign>(astas_ground_sign));

    messages::map::Map proto_map{};
    FillMapProtoRepresentation(builder.astas_map, &entity_repository, &proto_map);

    ASSERT_EQ(proto_map.ground_signs_size(), 1);
    CheckGroundSign(astas_ground_sign, proto_map.ground_signs(0));
}

TEST(MapToProtoConversionTest,
     GivenMapWithTrafficLight_WhenConvertedToProtoRepresentation_ThenTrafficLightIsFilledCorrectly)
{
    environment::api::EntityRepository entity_repository{nullptr};
    environment::map::TrafficLight astas_traffic_light{};
    FillTestTrafficLightValues(astas_traffic_light);

    test_utils::AstasMapBuilder builder{};
    builder.AddTrafficLight(astas_traffic_light);

    messages::map::Map proto_map{};
    FillMapProtoRepresentation(builder.astas_map, &entity_repository, &proto_map);

    ASSERT_EQ(proto_map.traffic_lights().size(), 1);
    CheckTrafficLight(astas_traffic_light, proto_map.traffic_lights(0));
}

TEST(MapToProtoConversionTest, GivenValidMap_WhenConvertedToProtoRepresentation_ThenLaneGroupsFilledCorrectly)
{
    environment::api::EntityRepository entity_repository{nullptr};
    test_utils::AstasMapBuilder builder{};
    CreateTestLaneGroup(builder);

    messages::map::Map proto_map{};
    const auto& astas_map = builder.astas_map;
    FillMapProtoRepresentation(astas_map, &entity_repository, &proto_map);

    ASSERT_EQ(proto_map.lane_groups_size(), 1);
    CheckLaneGroup(astas_map, astas_map.GetLaneGroups()[0], proto_map.lane_groups(0));
}

TEST(MapToProtoConversionTest,
     GivenFilledStaticChunks_WhenConvertedToProtoRepresentation_ThenWorldChunksFilledCorrectly)
{
    environment::map::LaneGroup test_lanegroup{42, environment::map::LaneGroup::Type::kJunction};
    environment::map::RoadObject test_roadobject{};
    test_roadobject.id = 1337;
    environment::map::TrafficSign test_trafficsign{};
    test_trafficsign.id = 666;
    environment::map::TrafficLight test_trafficlight{};
    test_trafficlight.id = 69;

    environment::chunking::StaticChunkList chunks{};
    environment::chunking::MapChunk chunk_1{{1, 2}, {1.1_m, 1.2_m, 1.3_m}};
    environment::chunking::MapChunk chunk_2{{-1, -2}, {-1.1_m, -1.2_m, -1.3_m}};

    chunk_1.lane_groups.push_back(&test_lanegroup);
    chunk_1.road_objects.push_back(&test_roadobject);
    chunk_2.traffic_signs.push_back(&test_trafficsign);
    chunk_2.traffic_lights.push_back(&test_trafficlight);

    chunks.emplace(chunk_1.key, chunk_1);
    chunks.emplace(chunk_2.key, chunk_2);

    messages::map::Map proto_map{};
    FillChunkingProtoRepresentation(chunks, &proto_map);

    ASSERT_EQ(proto_map.world_chunks_size(), 2);
    auto& proto_chunk1 = proto_map.world_chunks(1);
    auto& proto_chunk2 = proto_map.world_chunks(0);

    ASSERT_EQ(proto_chunk1.lane_groups_size(), 1);
    ASSERT_EQ(proto_chunk1.road_objects_size(), 1);
    EXPECT_EQ(proto_chunk1.lane_groups(0), test_lanegroup.id);
    EXPECT_EQ(proto_chunk1.road_objects(0), test_roadobject.id);

    ASSERT_EQ(proto_chunk2.traffic_signs_size(), 1);
    ASSERT_EQ(proto_chunk2.traffic_lights_size(), 1);
    EXPECT_EQ(proto_chunk2.traffic_signs(0), test_trafficsign.id);
    EXPECT_EQ(proto_chunk2.traffic_lights(0), test_trafficlight.id);
}

}  // namespace astas::communication
