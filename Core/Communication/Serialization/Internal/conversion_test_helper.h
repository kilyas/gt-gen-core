/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONVERSIONTESTHELPER_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONVERSIONTESTHELPER_H

#include "Core/Communication/Serialization/Internal/map_type_conversions.h"
#include "Core/Communication/Serialization/Internal/sign_type_conversions.h"
#include "Core/Communication/Serialization/Internal/traffic_light_bulb_type_conversions.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "astas_map.pb.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::communication
{

void CheckVector3(const mantle_api::Vec3<units::length::meter_t>& map_vector,
                  const messages::map::Vector3d& proto_vector);

void CheckDimension3(const mantle_api::Dimension3& astas_dimension, const messages::map::Dimension3d& proto_dimension);

void CheckOrientation3(const mantle_api::Orientation3<units::angle::radian_t>& astas_orientation,
                       const messages::map::Orientation3d& proto_orientation);

void CheckPose(const mantle_api::Pose& astas_pose, const messages::map::Pose& proto_pose);

void CheckRoadObject(const environment::map::RoadObject& astas_object, const messages::map::RoadObject& proto_object);

void CheckSupplementarySign(const environment::map::MountedSign::SupplementarySign& astas_sign,
                            const messages::map::TrafficSign& proto_sign);

void CheckTrafficSign(const environment::map::TrafficSign& astas_sign, const messages::map::TrafficSign& proto_sign);

void CheckMountedSign(const environment::map::MountedSign& astas_sign, const messages::map::MountedSign& proto_sign);

void CheckGroundSign(const environment::map::GroundSign& astas_sign, const messages::map::GroundSign& proto_sign);

void CheckTrafficLightBulb(const environment::map::TrafficLightBulb& astas_traffic_light_bulb,
                           const messages::map::TrafficLight_LightBulb& proto_traffic_light_bulb);

void CheckTrafficLight(const environment::map::TrafficLight& astas_traffic_light,
                       const messages::map::TrafficLight& proto_traffic_light);

void CheckLaneBoundary(const environment::map::LaneBoundary& astas_boundary,
                       const messages::map::LaneBoundary& proto_boundary);

void CheckLane(const environment::map::Lane& astas_lane, const messages::map::Lane& proto_lane);

void CheckLaneGroup(const environment::map::AstasMap& astas_map,
                    const environment::map::LaneGroup& astas_group,
                    const messages::map::LaneGroup& proto_group);

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONVERSIONTESTHELPER_H
