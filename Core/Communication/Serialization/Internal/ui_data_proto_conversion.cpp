/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/ui_data_proto_conversion.h"

namespace astas::communication
{

void FillUiDataProtoRepresentation(const service::gui::UiData& ui_data, messages::ui::UiData* proto_message)
{
    proto_message->set_ad_state(ui_data.ad_state);
    proto_message->mutable_driver_related_data()->CopyFrom(ui_data.driver_related_data);

    for (const auto& marker : ui_data.markers)
    {
        auto* proto_marker = proto_message->mutable_marker()->Add();
        proto_marker->CopyFrom(marker);
    }
}

}  // namespace astas::communication
