/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_SIGNTYPECONVERSIONS_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_SIGNTYPECONVERSIONS_H

#include "Core/Environment/Map/AstasMap/signs.h"
#include "astas_map.pb.h"

namespace astas::communication
{

inline messages::map::RoadMarkingType AstasToProtoRoadMarkingType(osi::OsiRoadMarkingsType astas_marking_type)
{
    using astas_type = osi::OsiRoadMarkingsType;
    using proto_type = messages::map::RoadMarkingType;

    switch (astas_marking_type)
    {
        case astas_type::kOther:
        {
            return proto_type::ROAD_MARKING_TYPE_OTHER;
        }
        case astas_type::kPaintedTrafficSign:
        {
            return proto_type::ROAD_MARKING_TYPE_PAINTEDTRAFFICSIGN;
        }
        case astas_type::kSymbolicTrafficSign:
        {
            return proto_type::ROAD_MARKING_TYPE_SYMBOLICTRAFFICSIGN;
        }
        case astas_type::kTextualTrafficSign:
        {
            return proto_type::ROAD_MARKING_TYPE_TEXTUALTRAFFICSIGN;
        }
        case astas_type::kGenericSymbol:
        {
            return proto_type::ROAD_MARKING_TYPE_GENERICSYMBOL;
        }
        case astas_type::kGenericLine:
        {
            return proto_type::ROAD_MARKING_TYPE_GENERICLINE;
        }
        case astas_type::kGenericText:
        {
            return proto_type::ROAD_MARKING_TYPE_GENERICTEXT;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::ROAD_MARKING_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::RoadMarkingColor AstasToProtoRoadMarkingColor(osi::OsiRoadMarkingColor astas_marking_color)
{
    using astas_type = osi::OsiRoadMarkingColor;
    using proto_type = messages::map::RoadMarkingColor;

    switch (astas_marking_color)
    {
        case astas_type::kOther:
        {
            return proto_type::ROAD_MARKING_COLOR_OTHER;
        }
        case astas_type::kWhite:
        {
            return proto_type::ROAD_MARKING_COLOR_WHITE;
        }
        case astas_type::kYellow:
        {
            return proto_type::ROAD_MARKING_COLOR_YELLOW;
        }
        case astas_type::kBlue:
        {
            return proto_type::ROAD_MARKING_COLOR_BLUE;
        }
        case astas_type::kRed:
        {
            return proto_type::ROAD_MARKING_COLOR_RED;
        }
        case astas_type::kGreen:
        {
            return proto_type::ROAD_MARKING_COLOR_GREEN;
        }
        case astas_type::kViolet:
        {
            return proto_type::ROAD_MARKING_COLOR_VIOLET;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::ROAD_MARKING_COLOR_UNKNOWN;
        }
    }
}

inline messages::map::SignVariability AstasToProtoSignVariability(osi::OsiTrafficSignVariability astas_variability)
{
    using astas_type = osi::OsiTrafficSignVariability;
    using proto_type = messages::map::SignVariability;

    switch (astas_variability)
    {
        case astas_type::kOther:
        {
            return proto_type::SIGNVARIABILITY_OTHER;
        }
        case astas_type::kFixed:
        {
            return proto_type::SIGNVARIABILITY_FIXED;
        }
        case astas_type::kVariable:
        {
            return proto_type::SIGNVARIABILITY_VARIABLE;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::SIGNVARIABILITY_UNKNOWN;
        }
    }
}

inline messages::map::DirectionScope AstasToProtoDirectionScope(osi::OsiTrafficSignDirectionScope astas_direction_scope)
{
    using astas_type = osi::OsiTrafficSignDirectionScope;
    using proto_type = messages::map::DirectionScope;

    switch (astas_direction_scope)
    {
        case astas_type::kOther:
        {
            return proto_type::DIRECTION_SCOPE_OTHER;
        }
        case astas_type::kNoDirection:
        {
            return proto_type::DIRECTION_SCOPE_NODIRECTION;
        }
        case astas_type::kLeft:
        {
            return proto_type::DIRECTION_SCOPE_LEFT;
        }
        case astas_type::kRight:
        {
            return proto_type::DIRECTION_SCOPE_RIGHT;
        }
        case astas_type::kLeftRight:
        {
            return proto_type::DIRECTION_SCOPE_LEFTRIGHT;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::DIRECTION_SCOPE_UNKNOWN;
        }
    }
}

inline messages::map::TrafficSignUnit AstasToProtoSignUnit(osi::OsiTrafficSignValueUnit astas_unit)
{
    using astas_type = osi::OsiTrafficSignValueUnit;
    using proto_type = messages::map::TrafficSignUnit;

    switch (astas_unit)
    {
        case astas_type::kOther:
        {
            return proto_type::SIGN_UNIT_OTHER;
        }
        case astas_type::kNoUnit:
        {
            return proto_type::SIGN_UNIT_NOUNIT;
        }
        case astas_type::kKilometerPerHour:
        {
            return proto_type::SIGN_UNIT_KILOMETERPERHOUR;
        }
        case astas_type::kMilePerHour:
        {
            return proto_type::SIGN_UNIT_MILEPERHOUR;
        }
        case astas_type::kMeter:
        {
            return proto_type::SIGN_UNIT_METER;
        }
        case astas_type::kKilometer:
        {
            return proto_type::SIGN_UNIT_KILOMETER;
        }
        case astas_type::kFeet:
        {
            return proto_type::SIGN_UNIT_FEET;
        }
        case astas_type::kMile:
        {
            return proto_type::SIGN_UNIT_MILE;
        }
        case astas_type::kMetricTon:
        {
            return proto_type::SIGN_UNIT_METRICTON;
        }
        case astas_type::kLongTon:
        {
            return proto_type::SIGN_UNIT_LONGTON;
        }
        case astas_type::kShortTon:
        {
            return proto_type::SIGN_UNIT_SHORTTON;
        }
        case astas_type::kMinutes:
        {
            return proto_type::SIGN_UNIT_MINUTES;
        }
        case astas_type::kDay:
        {
            return proto_type::SIGN_UNIT_DAY;
        }
        case astas_type::kPercentage:
        {
            return proto_type::SIGN_UNIT_PERCENTAGE;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::SIGN_UNIT_UNKNOWN;
        }
    }
}

inline messages::map::MainTrafficSignType AstasToProtoMainSign(osi::OsiTrafficSignType astas_sign_type)
{
    using astas_type = osi::OsiTrafficSignType;
    using proto_type = messages::map::MainTrafficSignType;

    switch (astas_sign_type)
    {
        case astas_type::kOther:
        {
            return proto_type::MAIN_SIGN_TYPE_OTHER;
        }
        case astas_type::kDangerSpot:
        {
            return proto_type::MAIN_SIGN_TYPE_DANGERSPOT;
        }
        case astas_type::kZebraCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_ZEBRACROSSING;
        }
        case astas_type::kFlight:
        {
            return proto_type::MAIN_SIGN_TYPE_FLIGHT;
        }
        case astas_type::kCattle:
        {
            return proto_type::MAIN_SIGN_TYPE_CATTLE;
        }
        case astas_type::kHorseRiders:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSERIDERS;
        }
        case astas_type::kAmphibians:
        {
            return proto_type::MAIN_SIGN_TYPE_AMPHIBIANS;
        }
        case astas_type::kFallingRocks:
        {
            return proto_type::MAIN_SIGN_TYPE_FALLINGROCKS;
        }
        case astas_type::kSnowOrIce:
        {
            return proto_type::MAIN_SIGN_TYPE_SNOWORICE;
        }
        case astas_type::kLooseGravel:
        {
            return proto_type::MAIN_SIGN_TYPE_LOOSEGRAVEL;
        }
        case astas_type::kWaterside:
        {
            return proto_type::MAIN_SIGN_TYPE_WATERSIDE;
        }
        case astas_type::kClearance:
        {
            return proto_type::MAIN_SIGN_TYPE_CLEARANCE;
        }
        case astas_type::kMovableBridge:
        {
            return proto_type::MAIN_SIGN_TYPE_MOVABLEBRIDGE;
        }
        case astas_type::kRightBeforeLeftNextIntersection:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTBEFORELEFTNEXTINTERSECTION;
        }
        case astas_type::kTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_TURNLEFT;
        }
        case astas_type::kTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_TURNRIGHT;
        }
        case astas_type::kDoubleTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DOUBLETURNLEFT;
        }
        case astas_type::kDoubleTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DOUBLETURNRIGHT;
        }
        case astas_type::kHillDownwards:
        {
            return proto_type::MAIN_SIGN_TYPE_HILLDOWNWARDS;
        }
        case astas_type::kHillUpwards:
        {
            return proto_type::MAIN_SIGN_TYPE_HILLUPWARDS;
        }
        case astas_type::kUnevenRoad:
        {
            return proto_type::MAIN_SIGN_TYPE_UNEVENROAD;
        }
        case astas_type::kRoadSlipperyWetOrDirty:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADSLIPPERYWETORDIRTY;
        }
        case astas_type::kSideWinds:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWINDS;
        }
        case astas_type::kRoadNarrowing:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADNARROWING;
        }
        case astas_type::kRoadNarrowingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADNARROWINGRIGHT;
        }
        case astas_type::kRoadNarrowingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADNARROWINGLEFT;
        }
        case astas_type::kRoadWorks:
        {
            return proto_type::MAIN_SIGN_TYPE_ROADWORKS;
        }
        case astas_type::kTrafficQueues:
        {
            return proto_type::MAIN_SIGN_TYPE_TRAFFICQUEUES;
        }
        case astas_type::kTwoWayTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_TWOWAYTRAFFIC;
        }
        case astas_type::kAttentionTrafficLight:
        {
            return proto_type::MAIN_SIGN_TYPE_ATTENTIONTRAFFICLIGHT;
        }
        case astas_type::kPedestrians:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANS;
        }
        case astas_type::kChildrenCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_CHILDRENCROSSING;
        }
        case astas_type::kCycleRoute:
        {
            return proto_type::MAIN_SIGN_TYPE_CYCLEROUTE;
        }
        case astas_type::kDeerCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_DEERCROSSING;
        }
        case astas_type::kUngatedLevelCrossing:
        {
            return proto_type::MAIN_SIGN_TYPE_UNGATEDLEVELCROSSING;
        }
        case astas_type::kLevelCrossingMarker:
        {
            return proto_type::MAIN_SIGN_TYPE_LEVELCROSSINGMARKER;
        }
        case astas_type::kRailwayTrafficPriority:
        {
            return proto_type::MAIN_SIGN_TYPE_RAILWAYTRAFFICPRIORITY;
        }
        case astas_type::kGiveWay:
        {
            return proto_type::MAIN_SIGN_TYPE_GIVEWAY;
        }
        case astas_type::kStop:
        {
            return proto_type::MAIN_SIGN_TYPE_STOP;
        }
        case astas_type::kPriorityToOppositeDirection:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYTOOPPOSITEDIRECTION;
        }
        case astas_type::kPriorityToOppositeDirectionUpsideDown:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYTOOPPOSITEDIRECTIONUPSIDEDOWN;
        }
        case astas_type::kPrescribedLeftTurn:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURN;
        }
        case astas_type::kPrescribedRightTurn:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTTURN;
        }
        case astas_type::kPrescribedStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDSTRAIGHT;
        }
        case astas_type::kPrescribedRightWay:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTWAY;
        }
        case astas_type::kPrescribedLeftWay:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTWAY;
        }
        case astas_type::kPrescribedRightTurnAndStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDRIGHTTURNANDSTRAIGHT;
        }
        case astas_type::kPrescribedLeftTurnAndStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURNANDSTRAIGHT;
        }
        case astas_type::kPrescribedLeftTurnAndRightTurn:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURNANDRIGHTTURN;
        }
        case astas_type::kPrescribedLeftTurnRightTurnAndStraight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDLEFTTURNRIGHTTURNANDSTRAIGHT;
        }
        case astas_type::kRoundabout:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUNDABOUT;
        }
        case astas_type::kOnewayLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_ONEWAYLEFT;
        }
        case astas_type::kOnewayRight:
        {
            return proto_type::MAIN_SIGN_TYPE_ONEWAYRIGHT;
        }
        case astas_type::kPassLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_PASSLEFT;
        }
        case astas_type::kPassRight:
        {
            return proto_type::MAIN_SIGN_TYPE_PASSRIGHT;
        }
        case astas_type::kSideLaneOpenForTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDELANEOPENFORTRAFFIC;
        }
        case astas_type::kSideLaneClosedForTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDELANECLOSEDFORTRAFFIC;
        }
        case astas_type::kSideLaneClosingForTraffic:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDELANECLOSINGFORTRAFFIC;
        }
        case astas_type::kBusStop:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSSTOP;
        }
        case astas_type::kTaxiStand:
        {
            return proto_type::MAIN_SIGN_TYPE_TAXISTAND;
        }
        case astas_type::kBicyclesOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESONLY;
        }
        case astas_type::kHorseRidersOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSERIDERSONLY;
        }
        case astas_type::kPedestriansOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANSONLY;
        }
        case astas_type::kBicyclesPedestriansSharedOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSHAREDONLY;
        }
        case astas_type::kBicyclesPedestriansSeparatedLeftOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSEPARATEDLEFTONLY;
        }
        case astas_type::kBicyclesPedestriansSeparatedRightOnly:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPEDESTRIANSSEPARATEDRIGHTONLY;
        }
        case astas_type::kPedestrianZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANZONEBEGIN;
        }
        case astas_type::kPedestrianZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANZONEEND;
        }
        case astas_type::kBicycleRoadBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLEROADBEGIN;
        }
        case astas_type::kBicycleRoadEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLEROADEND;
        }
        case astas_type::kBusLane:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSLANE;
        }
        case astas_type::kBusLaneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSLANEBEGIN;
        }
        case astas_type::kBusLaneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSLANEEND;
        }
        case astas_type::kAllProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_ALLPROHIBITED;
        }
        case astas_type::kMotorizedMultitrackProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOTORIZEDMULTITRACKPROHIBITED;
        }
        case astas_type::kTrucksProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_TRUCKSPROHIBITED;
        }
        case astas_type::kBicyclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_BICYCLESPROHIBITED;
        }
        case astas_type::kMotorcyclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOTORCYCLESPROHIBITED;
        }
        case astas_type::kMopedsProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOPEDSPROHIBITED;
        }
        case astas_type::kHorseRidersProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSERIDERSPROHIBITED;
        }
        case astas_type::kHorseCarriagesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_HORSECARRIAGESPROHIBITED;
        }
        case astas_type::kCattleProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_CATTLEPROHIBITED;
        }
        case astas_type::kBusesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_BUSESPROHIBITED;
        }
        case astas_type::kCarsProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_CARSPROHIBITED;
        }
        case astas_type::kCarsTrailersProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_CARSTRAILERSPROHIBITED;
        }
        case astas_type::kTrucksTrailersProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_TRUCKSTRAILERSPROHIBITED;
        }
        case astas_type::kTractorsProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_TRACTORSPROHIBITED;
        }
        case astas_type::kPedestriansProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANSPROHIBITED;
        }
        case astas_type::kMotorVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_MOTORVEHICLESPROHIBITED;
        }
        case astas_type::kHazardousGoodsVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_HAZARDOUSGOODSVEHICLESPROHIBITED;
        }
        case astas_type::kOverWeightVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERWEIGHTVEHICLESPROHIBITED;
        }
        case astas_type::kVehiclesAxleOverWeightProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESAXLEOVERWEIGHTPROHIBITED;
        }
        case astas_type::kVehiclesExcessWidthProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESEXCESSWIDTHPROHIBITED;
        }
        case astas_type::kVehiclesExcessHeightProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESEXCESSHEIGHTPROHIBITED;
        }
        case astas_type::kVehiclesExcessLengthProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_VEHICLESEXCESSLENGTHPROHIBITED;
        }
        case astas_type::kDoNotEnter:
        {
            return proto_type::MAIN_SIGN_TYPE_DONOTENTER;
        }
        case astas_type::kSnowChainsRequired:
        {
            return proto_type::MAIN_SIGN_TYPE_SNOWCHAINSREQUIRED;
        }
        case astas_type::kWaterPollutantVehiclesProhibited:
        {
            return proto_type::MAIN_SIGN_TYPE_WATERPOLLUTANTVEHICLESPROHIBITED;
        }
        case astas_type::kEnvironmentalZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ENVIRONMENTALZONEBEGIN;
        }
        case astas_type::kEnvironmentalZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ENVIRONMENTALZONEEND;
        }
        case astas_type::kNoUTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_NOUTURNLEFT;
        }
        case astas_type::kNoUTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_NOUTURNRIGHT;
        }
        case astas_type::kPrescribedUTurnLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDUTURNLEFT;
        }
        case astas_type::kPrescribedUTurnRight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRESCRIBEDUTURNRIGHT;
        }
        case astas_type::kMinimumDistanceForTrucks:
        {
            return proto_type::MAIN_SIGN_TYPE_MINIMUMDISTANCEFORTRUCKS;
        }
        case astas_type::kSpeedLimitBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITBEGIN;
        }
        case astas_type::kSpeedLimitZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITZONEBEGIN;
        }
        case astas_type::kSpeedLimitZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITZONEEND;
        }
        case astas_type::kMinimumSpeedBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_MINIMUMSPEEDBEGIN;
        }
        case astas_type::kOvertakingBanBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANBEGIN;
        }
        case astas_type::kOvertakingBanForTrucksBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANFORTRUCKSBEGIN;
        }
        case astas_type::kSpeedLimitEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_SPEEDLIMITEND;
        }
        case astas_type::kMinimumSpeedEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_MINIMUMSPEEDEND;
        }
        case astas_type::kOvertakingBanEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANEND;
        }
        case astas_type::kOvertakingBanForTrucksEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_OVERTAKINGBANFORTRUCKSEND;
        }
        case astas_type::kAllRestrictionsEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ALLRESTRICTIONSEND;
        }
        case astas_type::kNoStopping:
        {
            return proto_type::MAIN_SIGN_TYPE_NOSTOPPING;
        }
        case astas_type::kNoParking:
        {
            return proto_type::MAIN_SIGN_TYPE_NOPARKING;
        }
        case astas_type::kNoParkingZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_NOPARKINGZONEBEGIN;
        }
        case astas_type::kNoParkingZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_NOPARKINGZONEEND;
        }
        case astas_type::kRightOfWayNextIntersection:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTOFWAYNEXTINTERSECTION;
        }
        case astas_type::kRightOfWayBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTOFWAYBEGIN;
        }
        case astas_type::kRightOfWayEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_RIGHTOFWAYEND;
        }
        case astas_type::kPriorityOverOppositeDirection:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYOVEROPPOSITEDIRECTION;
        }
        case astas_type::kPriorityOverOppositeDirectionUpsideDown:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIORITYOVEROPPOSITEDIRECTIONUPSIDEDOWN;
        }
        case astas_type::kTownBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_TOWNBEGIN;
        }
        case astas_type::kTownEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_TOWNEND;
        }
        case astas_type::kCarParking:
        {
            return proto_type::MAIN_SIGN_TYPE_CARPARKING;
        }
        case astas_type::kCarParkingZoneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_CARPARKINGZONEBEGIN;
        }
        case astas_type::kCarParkingZoneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_CARPARKINGZONEEND;
        }
        case astas_type::kSidewalkHalfParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKHALFPARKINGLEFT;
        }
        case astas_type::kSidewalkHalfParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKHALFPARKINGRIGHT;
        }
        case astas_type::kSidewalkParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPARKINGLEFT;
        }
        case astas_type::kSidewalkParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPARKINGRIGHT;
        }
        case astas_type::kSidewalkPerpendicularHalfParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARHALFPARKINGLEFT;
        }
        case astas_type::kSidewalkPerpendicularHalfParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARHALFPARKINGRIGHT;
        }
        case astas_type::kSidewalkPerpendicularParkingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARPARKINGLEFT;
        }
        case astas_type::kSidewalkPerpendicularParkingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SIDEWALKPERPENDICULARPARKINGRIGHT;
        }
        case astas_type::kLivingStreetBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_LIVINGSTREETBEGIN;
        }
        case astas_type::kLivingStreetEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_LIVINGSTREETEND;
        }
        case astas_type::kTunnel:
        {
            return proto_type::MAIN_SIGN_TYPE_TUNNEL;
        }
        case astas_type::kEmergencyStoppingLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_EMERGENCYSTOPPINGLEFT;
        }
        case astas_type::kEmergencyStoppingRight:
        {
            return proto_type::MAIN_SIGN_TYPE_EMERGENCYSTOPPINGRIGHT;
        }
        case astas_type::kHighwayBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYBEGIN;
        }
        case astas_type::kHighwayEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYEND;
        }
        case astas_type::kExpresswayBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_EXPRESSWAYBEGIN;
        }
        case astas_type::kExpresswayEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_EXPRESSWAYEND;
        }
        case astas_type::kNamedHighwayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_NAMEDHIGHWAYEXIT;
        }
        case astas_type::kNamedExpresswayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_NAMEDEXPRESSWAYEXIT;
        }
        case astas_type::kNamedRoadExit:
        {
            return proto_type::MAIN_SIGN_TYPE_NAMEDROADEXIT;
        }
        case astas_type::kHighwayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYEXIT;
        }
        case astas_type::kExpresswayExit:
        {
            return proto_type::MAIN_SIGN_TYPE_EXPRESSWAYEXIT;
        }
        case astas_type::kOnewayStreet:
        {
            return proto_type::MAIN_SIGN_TYPE_ONEWAYSTREET;
        }
        case astas_type::kCrossingGuards:
        {
            return proto_type::MAIN_SIGN_TYPE_CROSSINGGUARDS;
        }
        case astas_type::kDeadend:
        {
            return proto_type::MAIN_SIGN_TYPE_DEADEND;
        }
        case astas_type::kDeadendExcludingDesignatedActors:
        {
            return proto_type::MAIN_SIGN_TYPE_DEADENDEXCLUDINGDESIGNATEDACTORS;
        }
        case astas_type::kFirstAidStation:
        {
            return proto_type::MAIN_SIGN_TYPE_FIRSTAIDSTATION;
        }
        case astas_type::kPoliceStation:
        {
            return proto_type::MAIN_SIGN_TYPE_POLICESTATION;
        }
        case astas_type::kTelephone:
        {
            return proto_type::MAIN_SIGN_TYPE_TELEPHONE;
        }
        case astas_type::kFillingStation:
        {
            return proto_type::MAIN_SIGN_TYPE_FILLINGSTATION;
        }
        case astas_type::kHotel:
        {
            return proto_type::MAIN_SIGN_TYPE_HOTEL;
        }
        case astas_type::kInn:
        {
            return proto_type::MAIN_SIGN_TYPE_INN;
        }
        case astas_type::kKiosk:
        {
            return proto_type::MAIN_SIGN_TYPE_KIOSK;
        }
        case astas_type::kToilet:
        {
            return proto_type::MAIN_SIGN_TYPE_TOILET;
        }
        case astas_type::kChapel:
        {
            return proto_type::MAIN_SIGN_TYPE_CHAPEL;
        }
        case astas_type::kTouristInfo:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTINFO;
        }
        case astas_type::kRepairService:
        {
            return proto_type::MAIN_SIGN_TYPE_REPAIRSERVICE;
        }
        case astas_type::kPedestrianUnderpass:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANUNDERPASS;
        }
        case astas_type::kPedestrianBridge:
        {
            return proto_type::MAIN_SIGN_TYPE_PEDESTRIANBRIDGE;
        }
        case astas_type::kCamperPlace:
        {
            return proto_type::MAIN_SIGN_TYPE_CAMPERPLACE;
        }
        case astas_type::kAdvisorySpeedLimitBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ADVISORYSPEEDLIMITBEGIN;
        }
        case astas_type::kAdvisorySpeedLimitEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ADVISORYSPEEDLIMITEND;
        }
        case astas_type::kPlaceName:
        {
            return proto_type::MAIN_SIGN_TYPE_PLACENAME;
        }
        case astas_type::kTouristAttraction:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTATTRACTION;
        }
        case astas_type::kTouristRoute:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTROUTE;
        }
        case astas_type::kTouristArea:
        {
            return proto_type::MAIN_SIGN_TYPE_TOURISTAREA;
        }
        case astas_type::kShoulderNotPassableMotorVehicles:
        {
            return proto_type::MAIN_SIGN_TYPE_SHOULDERNOTPASSABLEMOTORVEHICLES;
        }
        case astas_type::kShoulderUnsafeTrucksTractors:
        {
            return proto_type::MAIN_SIGN_TYPE_SHOULDERUNSAFETRUCKSTRACTORS;
        }
        case astas_type::kTollBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_TOLLBEGIN;
        }
        case astas_type::kTollEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_TOLLEND;
        }
        case astas_type::kTollRoad:
        {
            return proto_type::MAIN_SIGN_TYPE_TOLLROAD;
        }
        case astas_type::kCustoms:
        {
            return proto_type::MAIN_SIGN_TYPE_CUSTOMS;
        }
        case astas_type::kInternationalBorderInfo:
        {
            return proto_type::MAIN_SIGN_TYPE_INTERNATIONALBORDERINFO;
        }
        case astas_type::kStreetlightRedBand:
        {
            return proto_type::MAIN_SIGN_TYPE_STREETLIGHTREDBAND;
        }
        case astas_type::kFederalHighwayRouteNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_FEDERALHIGHWAYROUTENUMBER;
        }
        case astas_type::kHighwayRouteNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYROUTENUMBER;
        }
        case astas_type::kHighwayInterchangeNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYINTERCHANGENUMBER;
        }
        case astas_type::kEuropeanRouteNumber:
        {
            return proto_type::MAIN_SIGN_TYPE_EUROPEANROUTENUMBER;
        }
        case astas_type::kFederalHighwayDirectionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_FEDERALHIGHWAYDIRECTIONLEFT;
        }
        case astas_type::kFederalHighwayDirectionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_FEDERALHIGHWAYDIRECTIONRIGHT;
        }
        case astas_type::kPrimaryRoadDirectionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIMARYROADDIRECTIONLEFT;
        }
        case astas_type::kPrimaryRoadDirectionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_PRIMARYROADDIRECTIONRIGHT;
        }
        case astas_type::kSecondaryRoadDirectionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_SECONDARYROADDIRECTIONLEFT;
        }
        case astas_type::kSecondaryRoadDirectionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_SECONDARYROADDIRECTIONRIGHT;
        }
        case astas_type::kDirectionDesignatedActorsLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONDESIGNATEDACTORSLEFT;
        }
        case astas_type::kDirectionDesignatedActorsRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONDESIGNATEDACTORSRIGHT;
        }
        case astas_type::kRoutingDesignatedActors:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUTINGDESIGNATEDACTORS;
        }
        case astas_type::kDirectionToHighwayLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOHIGHWAYLEFT;
        }
        case astas_type::kDirectionToHighwayRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOHIGHWAYRIGHT;
        }
        case astas_type::kDirectionToLocalDestinationLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOLOCALDESTINATIONLEFT;
        }
        case astas_type::kDirectionToLocalDestinationRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONTOLOCALDESTINATIONRIGHT;
        }
        case astas_type::kConsolidatedDirections:
        {
            return proto_type::MAIN_SIGN_TYPE_CONSOLIDATEDDIRECTIONS;
        }
        case astas_type::kStreetName:
        {
            return proto_type::MAIN_SIGN_TYPE_STREETNAME;
        }
        case astas_type::kDirectionPreannouncement:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENT;
        }
        case astas_type::kDirectionPreannouncementLaneConfig:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENTLANECONFIG;
        }
        case astas_type::kDirectionPreannouncementHighwayEntries:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONPREANNOUNCEMENTHIGHWAYENTRIES;
        }
        case astas_type::kHighwayAnnouncement:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYANNOUNCEMENT;
        }
        case astas_type::kOtherRoadAnnouncement:
        {
            return proto_type::MAIN_SIGN_TYPE_OTHERROADANNOUNCEMENT;
        }
        case astas_type::kHighwayAnnouncementTruckStop:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYANNOUNCEMENTTRUCKSTOP;
        }
        case astas_type::kHighwayPreannouncementDirections:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYPREANNOUNCEMENTDIRECTIONS;
        }
        case astas_type::kPoleExit:
        {
            return proto_type::MAIN_SIGN_TYPE_POLEEXIT;
        }
        case astas_type::kHighwayDistanceBoard:
        {
            return proto_type::MAIN_SIGN_TYPE_HIGHWAYDISTANCEBOARD;
        }
        case astas_type::kDetourLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURLEFT;
        }
        case astas_type::kDetourRight:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURRIGHT;
        }
        case astas_type::kNumberedDetour:
        {
            return proto_type::MAIN_SIGN_TYPE_NUMBEREDDETOUR;
        }
        case astas_type::kDetourBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURBEGIN;
        }
        case astas_type::kDetourEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOUREND;
        }
        case astas_type::kDetourRoutingBoard:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURROUTINGBOARD;
        }
        case astas_type::kOptionalDetour:
        {
            return proto_type::MAIN_SIGN_TYPE_OPTIONALDETOUR;
        }
        case astas_type::kOptionalDetourRouting:
        {
            return proto_type::MAIN_SIGN_TYPE_OPTIONALDETOURROUTING;
        }
        case astas_type::kRouteRecommendation:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUTERECOMMENDATION;
        }
        case astas_type::kRouteRecommendationEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ROUTERECOMMENDATIONEND;
        }
        case astas_type::kAnnounceLaneTransitionLeft:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELANETRANSITIONLEFT;
        }
        case astas_type::kAnnounceLaneTransitionRight:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELANETRANSITIONRIGHT;
        }
        case astas_type::kAnnounceRightLaneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCERIGHTLANEEND;
        }
        case astas_type::kAnnounceLeftLaneEnd:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELEFTLANEEND;
        }
        case astas_type::kAnnounceRightLaneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCERIGHTLANEBEGIN;
        }
        case astas_type::kAnnounceLeftLaneBegin:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELEFTLANEBEGIN;
        }
        case astas_type::kAnnounceLaneConsolidation:
        {
            return proto_type::MAIN_SIGN_TYPE_ANNOUNCELANECONSOLIDATION;
        }
        case astas_type::kDetourCityBlock:
        {
            return proto_type::MAIN_SIGN_TYPE_DETOURCITYBLOCK;
        }
        case astas_type::kGate:
        {
            return proto_type::MAIN_SIGN_TYPE_GATE;
        }
        case astas_type::kPoleWarning:
        {
            return proto_type::MAIN_SIGN_TYPE_POLEWARNING;
        }
        case astas_type::kTrafficCone:
        {
            return proto_type::MAIN_SIGN_TYPE_TRAFFICCONE;
        }
        case astas_type::kMobileLaneClosure:
        {
            return proto_type::MAIN_SIGN_TYPE_MOBILELANECLOSURE;
        }
        case astas_type::kReflectorPost:
        {
            return proto_type::MAIN_SIGN_TYPE_REFLECTORPOST;
        }
        case astas_type::kDirectionalBoardWarning:
        {
            return proto_type::MAIN_SIGN_TYPE_DIRECTIONALBOARDWARNING;
        }
        case astas_type::kGuidingPlate:
        {
            return proto_type::MAIN_SIGN_TYPE_GUIDINGPLATE;
        }
        case astas_type::kGuidingPlateWedges:
        {
            return proto_type::MAIN_SIGN_TYPE_GUIDINGPLATEWEDGES;
        }
        case astas_type::kParkingHazard:
        {
            return proto_type::MAIN_SIGN_TYPE_PARKINGHAZARD;
        }
        case astas_type::kTrafficLightGreenArrow:
        {
            return proto_type::MAIN_SIGN_TYPE_TRAFFICLIGHTGREENARROW;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::MAIN_SIGN_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::SupplementarySignType AstasToProtoSupplementarySign(
    environment::map::OsiSupplementarySignType astas_sign_type)
{
    using astas_type = environment::map::OsiSupplementarySignType;
    using proto_type = messages::map::SupplementarySignType;

    switch (astas_sign_type)
    {
        case astas_type::kOther:
        {
            return proto_type::SUPPLEMENTARY_TYPE_OTHER;
        }
        case astas_type::kNoSign:
        {
            return proto_type::SUPPLEMENTARY_TYPE_NOSIGN;
        }
        case astas_type::kText:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TEXT;
        }
        case astas_type::kSpace:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SPACE;
        }
        case astas_type::kTime:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TIME;
        }
        case astas_type::kArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ARROW;
        }
        case astas_type::kConstrainedTo:
        {
            return proto_type::SUPPLEMENTARY_TYPE_CONSTRAINEDTO;
        }
        case astas_type::kExcept:
        {
            return proto_type::SUPPLEMENTARY_TYPE_EXCEPT;
        }
        case astas_type::kValidForDistance:
        {
            return proto_type::SUPPLEMENTARY_TYPE_VALIDFORDISTANCE;
        }
        case astas_type::kPriorityRoadBottomLeftFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTFOURWAY;
        }
        case astas_type::kPriorityRoadTopLeftFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPLEFTFOURWAY;
        }
        case astas_type::kPriorityRoadBottomLeftThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTTHREEWAYSTRAIGHT;
        }
        case astas_type::kPriorityRoadBottomLeftThreeWaySideways:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMLEFTTHREEWAYSIDEWAYS;
        }
        case astas_type::kPriorityRoadTopLeftThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPLEFTTHREEWAYSTRAIGHT;
        }
        case astas_type::kPriorityRoadBottomRightFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTFOURWAY;
        }
        case astas_type::kPriorityRoadTopRightFourWay:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPRIGHTFOURWAY;
        }
        case astas_type::kPriorityRoadBottomRightThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTTHREEWAYSTRAIGHT;
        }
        case astas_type::kPriorityRoadBottomRightThreeWaySideway:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADBOTTOMRIGHTTHREEWAYSIDEWAY;
        }
        case astas_type::kPriorityRoadTopRightThreeWayStraight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PRIORITYROADTOPRIGHTTHREEWAYSTRAIGHT;
        }
        case astas_type::kValidInDistance:
        {
            return proto_type::SUPPLEMENTARY_TYPE_VALIDINDISTANCE;
        }
        case astas_type::kStopIn:
        {
            return proto_type::SUPPLEMENTARY_TYPE_STOPIN;
        }
        case astas_type::kLeftArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_LEFTARROW;
        }
        case astas_type::kLeftBendArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_LEFTBENDARROW;
        }
        case astas_type::kRightArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_RIGHTARROW;
        }
        case astas_type::kRightBendArrow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_RIGHTBENDARROW;
        }
        case astas_type::kAccident:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ACCIDENT;
        }
        case astas_type::kSnow:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SNOW;
        }
        case astas_type::kFog:
        {
            return proto_type::SUPPLEMENTARY_TYPE_FOG;
        }
        case astas_type::kRollingHighwayInformation:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ROLLINGHIGHWAYINFORMATION;
        }
        case astas_type::kServices:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SERVICES;
        }
        case astas_type::kTimeRange:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TIMERANGE;
        }
        case astas_type::kParkingDiscTimeRestriction:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PARKINGDISCTIMERESTRICTION;
        }
        case astas_type::kWeight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_WEIGHT;
        }
        case astas_type::kWet:
        {
            return proto_type::SUPPLEMENTARY_TYPE_WET;
        }
        case astas_type::kParkingConstraint:
        {
            return proto_type::SUPPLEMENTARY_TYPE_PARKINGCONSTRAINT;
        }
        case astas_type::kNoWaitingSideStripes:
        {
            return proto_type::SUPPLEMENTARY_TYPE_NOWAITINGSIDESTRIPES;
        }
        case astas_type::kRain:
        {
            return proto_type::SUPPLEMENTARY_TYPE_RAIN;
        }
        case astas_type::kSnowRain:
        {
            return proto_type::SUPPLEMENTARY_TYPE_SNOWRAIN;
        }
        case astas_type::kNight:
        {
            return proto_type::SUPPLEMENTARY_TYPE_NIGHT;
        }
        case astas_type::kStop4Way:
        {
            return proto_type::SUPPLEMENTARY_TYPE_STOP4WAY;
        }
        case astas_type::kTruck:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TRUCK;
        }
        case astas_type::kTractorsMayBePassed:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TRACTORSMAYBEPASSED;
        }
        case astas_type::kHazardous:
        {
            return proto_type::SUPPLEMENTARY_TYPE_HAZARDOUS;
        }
        case astas_type::kTrailer:
        {
            return proto_type::SUPPLEMENTARY_TYPE_TRAILER;
        }
        case astas_type::kZone:
        {
            return proto_type::SUPPLEMENTARY_TYPE_ZONE;
        }
        case astas_type::kMotorcycle:
        {
            return proto_type::SUPPLEMENTARY_TYPE_MOTORCYCLE;
        }
        case astas_type::kMotorcycleAllowed:
        {
            return proto_type::SUPPLEMENTARY_TYPE_MOTORCYCLEALLOWED;
        }
        case astas_type::kCar:
        {
            return proto_type::SUPPLEMENTARY_TYPE_CAR;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::SUPPLEMENTARY_TYPE_UNKNOWN;
        }
    }
}

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_SIGNTYPECONVERSIONS_H
