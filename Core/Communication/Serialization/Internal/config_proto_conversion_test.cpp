/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/config_proto_conversion.h"

#include <gtest/gtest.h>

namespace astas::communication
{

TEST(ConfigToProtoConversionTest, GivenDefaultIniAllBoolsTrue_WhenConvertedToProtoRepresentation_ThenCorrectlyConverted)
{
    // take default ini and set all booleans that are false by default to true, so the test makes some sense
    service::user_settings::UserSettings user_settings{};
    user_settings.host_vehicle.blocking_communication = true;
    user_settings.host_vehicle.recovery_mode = true;
    user_settings.map.include_obstacles = true;

    messages::config::Config proto_config{};
    FillConfigProtoRepresentation(user_settings, "aoe2/aegis", &proto_config);

    EXPECT_EQ("aoe2/aegis", proto_config.general().settings_path());
    EXPECT_EQ(messages::config::LOG_LEVEL_DEBUG, proto_config.general().file_log_level());

    EXPECT_EQ(static_cast<float>(user_settings.ground_truth.lane_marking_distance_in_m),
              proto_config.ground_truth().lane_marking_distance());
    EXPECT_EQ(user_settings.ground_truth.allow_invalid_lane_locations,
              proto_config.ground_truth().allow_invalid_lane_locations());

    EXPECT_EQ(messages::config::Movement::HOST_MOVEMENT_EXTERNAL_VEHICLE, proto_config.host_vehicle().movement());
    EXPECT_EQ(static_cast<float>(user_settings.host_vehicle.time_scale), proto_config.host_vehicle().time_scale());
    EXPECT_EQ(user_settings.host_vehicle.blocking_communication, proto_config.host_vehicle().blocking_communication());
    EXPECT_EQ(user_settings.host_vehicle.recovery_mode, proto_config.host_vehicle().recovery_mode());

    EXPECT_EQ(user_settings.map.include_obstacles, proto_config.map().include_obstacles());

    EXPECT_EQ(user_settings.map_chunking.chunk_grid_size, proto_config.map_chunking().chunk_grid_size());
    EXPECT_EQ(user_settings.map_chunking.cells_per_direction, proto_config.map_chunking().cells_per_direction());
}

}  // namespace astas::communication
