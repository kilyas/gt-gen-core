/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_TRAFFICLIGHTBULBTYPECONVERSIONS_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_TRAFFICLIGHTBULBTYPECONVERSIONS_H

#include "Core/Environment/Map/AstasMap/Internal/traffic_light.h"
#include "astas_map.pb.h"

namespace astas::communication
{

inline messages::map::TrafficLightColor AstasToProtoOsiTrafficLightColor(
    environment::map::OsiTrafficLightColor astas_colour_type)
{
    using astas_type = environment::map::OsiTrafficLightColor;
    using proto_type = messages::map::TrafficLightColor;

    switch (astas_colour_type)
    {
        case astas_type::kOther:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_OTHER;
        }
        case astas_type::kRed:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_RED;
        }
        case astas_type::kYellow:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_YELLOW;
        }
        case astas_type::kGreen:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_GREEN;
        }
        case astas_type::kBlue:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_BLUE;
        }
        case astas_type::kWhite:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_WHITE;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::TRAFFIC_LIGHT_COLOR_UNKNOWN;
        }
    }
}

inline messages::map::TrafficLightIcon AstasToProtoOsiTrafficLightIcon(
    environment::map::OsiTrafficLightIcon astas_icon_type)
{
    using astas_type = environment::map::OsiTrafficLightIcon;
    using proto_type = messages::map::TrafficLightIcon;

    switch (astas_icon_type)
    {
        case astas_type::kOther:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_OTHER;
        }
        case astas_type::kNone:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_NONE;
        }
        case astas_type::kArrowStraightAhead:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEAD;
        }
        case astas_type::kArrowLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWLEFT;
        }
        case astas_type::kArrowDiagonalLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDIAGONALLEFT;
        }
        case astas_type::kArrowStraightAheadLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEADLEFT;
        }
        case astas_type::kArrowRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWRIGHT;
        }
        case astas_type::kArrowDiagonalRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDIAGONALRIGHT;
        }
        case astas_type::kArrowStraightAheadRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWSTRAIGHTAHEADRIGHT;
        }
        case astas_type::kArrowLeftRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWLEFTRIGHT;
        }
        case astas_type::kArrowDown:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDOWN;
        }
        case astas_type::kArrowDownLeft:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDOWNLEFT;
        }
        case astas_type::kArrowDownRight:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWDOWNRIGHT;
        }
        case astas_type::kArrowCross:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_ARROWCROSS;
        }
        case astas_type::kPedestrian:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_PEDESTRIAN;
        }
        case astas_type::kWalk:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_WALK;
        }
        case astas_type::kDontWalk:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_DONTWALK;
        }
        case astas_type::kBicycle:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_BICYCLE;
        }
        case astas_type::kPedestrianAndBicycle:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_PEDESTRIANANDBICYCLE;
        }
        case astas_type::kCountdownSeconds:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_COUNTDOWNSECONDS;
        }
        case astas_type::kCountdownPercent:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_COUNTDOWNPERCENT;
        }
        case astas_type::kTram:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_TRAM;
        }
        case astas_type::kBus:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_BUS;
        }
        case astas_type::kBusAndTram:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_BUSANDTRAM;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::TRAFFIC_LIGHT_ICON_UNKNOWN;
        }
    }
}

inline messages::map::TrafficLightMode AstasToProtoOsiTrafficLightMode(
    environment::map::OsiTrafficLightMode astas_mode_type)
{
    using astas_type = environment::map::OsiTrafficLightMode;
    using proto_type = messages::map::TrafficLightMode;

    switch (astas_mode_type)
    {
        case astas_type::kOther:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_OTHER;
        }
        case astas_type::kOff:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_OFF;
        }
        case astas_type::kConstant:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_CONSTANT;
        }
        case astas_type::kFlashing:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_FLASHING;
        }
        case astas_type::kCounting:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_COUNTING;
        }
        case astas_type::kUnknown:
        default:
        {
            return proto_type::TRAFFIC_LIGHT_MODE_UNKNOWN;
        }
    }
}

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_TRAFFICLIGHTBULBTYPECONVERSIONS_H
