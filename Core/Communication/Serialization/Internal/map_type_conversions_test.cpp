/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/Internal/map_type_conversions.h"

#include <gtest/gtest.h>

#include <utility>

namespace astas::communication
{

//
// LaneGroup
//
class LaneGroupConverterTestFixture
    : public testing::TestWithParam<std::tuple<environment::map::LaneGroup::Type, messages::map::LaneGroupType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    MapTypeConverter,
    LaneGroupConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<environment::map::LaneGroup::Type, messages::map::LaneGroupType>>{
        std::make_tuple(environment::map::LaneGroup::Type::kUnknown,
                        messages::map::LaneGroupType::LANE_GROUP_TYPE_UNKNOWN),
        std::make_tuple(environment::map::LaneGroup::Type::kJunction,
                        messages::map::LaneGroupType::LANE_GROUP_TYPE_JUNCTION),
        std::make_tuple(environment::map::LaneGroup::Type::kOneWay,
                        messages::map::LaneGroupType::LANE_GROUP_TYPE_ONE_WAY),
        std::make_tuple(environment::map::LaneGroup::Type::kOther,
                        messages::map::LaneGroupType::LANE_GROUP_TYPE_OTHER)}));

TEST_P(LaneGroupConverterTestFixture, GivenLaneGroup_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    environment::map::LaneGroup::Type astas_type = std::get<0>(GetParam());
    messages::map::LaneGroupType expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoLaneGroupType(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// BoundaryColor
//
class LaneBoundaryColorConverterTestFixture
    : public testing::TestWithParam<std::tuple<environment::map::LaneBoundary::Color, messages::map::LaneBoundaryColor>>
{
};

INSTANTIATE_TEST_SUITE_P(
    MapTypeConverter,
    LaneBoundaryColorConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<environment::map::LaneBoundary::Color, messages::map::LaneBoundaryColor>>{
        std::make_tuple(environment::map::LaneBoundary::Color::kUnknown,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_UNKNOWN),
        std::make_tuple(environment::map::LaneBoundary::Color::kOther,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_OTHER),
        std::make_tuple(environment::map::LaneBoundary::Color::kNone,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_NONE),
        std::make_tuple(environment::map::LaneBoundary::Color::kWhite,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_WHITE),
        std::make_tuple(environment::map::LaneBoundary::Color::kYellow,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_YELLOW),
        std::make_tuple(environment::map::LaneBoundary::Color::kRed,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_RED),
        std::make_tuple(environment::map::LaneBoundary::Color::kBlue,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_BLUE),
        std::make_tuple(environment::map::LaneBoundary::Color::kGreen,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_GREEN),
        std::make_tuple(environment::map::LaneBoundary::Color::kViolet,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_VIOLET),
        std::make_tuple(environment::map::LaneBoundary::Color::kLightGray,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_LIGHTGRAY),
        std::make_tuple(environment::map::LaneBoundary::Color::kGray,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_GRAY),
        std::make_tuple(environment::map::LaneBoundary::Color::kDarkGray,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_DARKGRAY),
        std::make_tuple(environment::map::LaneBoundary::Color::kBlack,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_BLACK),
        std::make_tuple(environment::map::LaneBoundary::Color::kCyan,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_CYAN),
        std::make_tuple(environment::map::LaneBoundary::Color::kOrange,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_ORANGE),
        std::make_tuple(environment::map::LaneBoundary::Color::kStandard,
                        messages::map::LaneBoundaryColor::BOUNDARY_COLOR_STANDARD)}));

TEST_P(LaneBoundaryColorConverterTestFixture, GivenLaneBoundaryColor_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    environment::map::LaneBoundary::Color astas_type = std::get<0>(GetParam());
    messages::map::LaneBoundaryColor expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoBoundaryColor(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// BoundaryType
//
class LaneBoundaryTypeConverterTestFixture
    : public testing::TestWithParam<std::tuple<environment::map::LaneBoundary::Type, messages::map::LaneBoundaryType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    MapTypeConverter,
    LaneBoundaryTypeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<environment::map::LaneBoundary::Type, messages::map::LaneBoundaryType>>{
        std::make_tuple(environment::map::LaneBoundary::Type::kUnknown,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_UNKNOWN),
        std::make_tuple(environment::map::LaneBoundary::Type::kOther,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_OTHER),
        std::make_tuple(environment::map::LaneBoundary::Type::kNoLine,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_NOLINE),
        std::make_tuple(environment::map::LaneBoundary::Type::kSolidLine,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_SOLIDLINE),
        std::make_tuple(environment::map::LaneBoundary::Type::kDashedLine,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_DASHEDLINE),
        std::make_tuple(environment::map::LaneBoundary::Type::kBottsDots,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_BOTTSDOTS),
        std::make_tuple(environment::map::LaneBoundary::Type::kRoadEdge,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_ROADEDGE),
        std::make_tuple(environment::map::LaneBoundary::Type::kSnowEdge,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_SNOWEDGE),
        std::make_tuple(environment::map::LaneBoundary::Type::kGrassEdge,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_GRASSEDGE),
        std::make_tuple(environment::map::LaneBoundary::Type::kGravelEdge,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_GRAVELEDGE),
        std::make_tuple(environment::map::LaneBoundary::Type::kSoilEdge,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_SOILEDGE),
        std::make_tuple(environment::map::LaneBoundary::Type::kGuardRail,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_GUARDRAIL),
        std::make_tuple(environment::map::LaneBoundary::Type::kCurb,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_CURB),
        std::make_tuple(environment::map::LaneBoundary::Type::kStructure,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_STRUCTURE),
        std::make_tuple(environment::map::LaneBoundary::Type::kBarrier,
                        messages::map::LaneBoundaryType::BOUNDARY_TYPE_BARRIER)}));

TEST_P(LaneBoundaryTypeConverterTestFixture, GivenLaneBoundaryType_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    environment::map::LaneBoundary::Type astas_type = std::get<0>(GetParam());
    messages::map::LaneBoundaryType expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoBoundaryType(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoLaneType
//
TEST(AstasToProtoLaneTypeTest, GivenLaneFlagDriving_WhenAstasToProtoLaneType_ThenProtoLaneTypeDrivingReturned)
{
    messages::map::LaneType expected_proto_type{messages::map::LaneType::LANE_TYPE_DRIVING};
    environment::map::LaneFlags flags;
    flags.SetDrivable();

    auto actual_proto_type = AstasToProtoLaneType(flags);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

TEST(AstasToProtoLaneTypeTest, GivenLaneFlagNonDriving_WhenAstasToProtoLaneType_ThenProtoLaneTypeNonDrivingReturned)
{
    messages::map::LaneType expected_proto_type{messages::map::LaneType::LANE_TYPE_NON_DRIVING};
    environment::map::LaneFlags flags;
    flags.SetNonDrivable();

    auto actual_proto_type = AstasToProtoLaneType(flags);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// AstasToProtoRoadObjectType
//
class RoadObjectTypeConverterTestFixture
    : public testing::TestWithParam<std::tuple<osi::StationaryObjectEntityType, messages::map::RoadObjectType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    MapTypeConverter,
    RoadObjectTypeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<osi::StationaryObjectEntityType, messages::map::RoadObjectType>>{
        std::make_tuple(osi::StationaryObjectEntityType::kUnknown,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_UNKNOWN),
        std::make_tuple(osi::StationaryObjectEntityType::kOther, messages::map::RoadObjectType::ROAD_OBJECT_TYPE_OTHER),
        std::make_tuple(osi::StationaryObjectEntityType::kBridge,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_BRIDGE),
        std::make_tuple(osi::StationaryObjectEntityType::kBuilding,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_BUILDING),
        std::make_tuple(osi::StationaryObjectEntityType::kPole, messages::map::RoadObjectType::ROAD_OBJECT_TYPE_POLE),
        std::make_tuple(osi::StationaryObjectEntityType::kPylon, messages::map::RoadObjectType::ROAD_OBJECT_TYPE_PYLON),
        std::make_tuple(osi::StationaryObjectEntityType::kDelineator,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_DELINEATOR),
        std::make_tuple(osi::StationaryObjectEntityType::kTree, messages::map::RoadObjectType::ROAD_OBJECT_TYPE_TREE),
        std::make_tuple(osi::StationaryObjectEntityType::kBarrier,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_BARRIER),
        std::make_tuple(osi::StationaryObjectEntityType::kVegetation,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_VEGETATION),
        std::make_tuple(osi::StationaryObjectEntityType::kCurbstone,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_CURBSTONE),
        std::make_tuple(osi::StationaryObjectEntityType::kWall, messages::map::RoadObjectType::ROAD_OBJECT_TYPE_WALL),
        std::make_tuple(osi::StationaryObjectEntityType::kVerticalStructure,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_VERTICALSTRUCTURE),
        std::make_tuple(osi::StationaryObjectEntityType::kRectangularStructure,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_RECTANGULARSTRUCTURE),
        std::make_tuple(osi::StationaryObjectEntityType::kOverheadStructure,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_OVERHEADSTRUCTURE),
        std::make_tuple(osi::StationaryObjectEntityType::kReflectiveStructure,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_REFLECTIVESTRUCTURE),
        std::make_tuple(osi::StationaryObjectEntityType::kConstructionSiteElement,
                        messages::map::RoadObjectType::ROAD_OBJECT_TYPE_CONSTRUCTIONSITEELEMENT)}));

TEST_P(RoadObjectTypeConverterTestFixture, GivenRoadObjectType_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    osi::StationaryObjectEntityType astas_type = std::get<0>(GetParam());
    messages::map::RoadObjectType expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = AstasToProtoRoadObjectType(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

}  // namespace astas::communication
