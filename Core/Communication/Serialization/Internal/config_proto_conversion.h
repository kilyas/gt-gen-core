/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONFIGPROTOCONVERSION_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONFIGPROTOCONVERSION_H

#include "Core/Service/UserSettings/user_settings.h"
#include "config.pb.h"

#include <string>

namespace astas::communication
{

/// @brief Converts provided UserSettings to corresponding proto message
/// @param user_settings UserSettings to convert
/// @param user_settings_path Full path to UserSettings that are converted
/// @param proto_message Proto message that should be filled
void FillConfigProtoRepresentation(const service::user_settings::UserSettings& user_settings,
                                   const std::string& user_settings_path,
                                   messages::config::Config* proto_message);

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_CONFIGPROTOCONVERSION_H
