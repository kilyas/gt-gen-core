/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPPROTOCONVERSION_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPPROTOCONVERSION_H

#include "Core/Environment/Chunking/world_chunk.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "astas_map.pb.h"

#include <MantleAPI/Traffic/i_entity_repository.h>

namespace astas::communication
{

/// @brief Converts provided AstasMap to corresponding proto message
/// @param map Map to convert
/// @param proto_message Proto message that should be filled
void FillMapProtoRepresentation(const environment::map::AstasMap& map,
                                const mantle_api::IEntityRepository* entity_repository,
                                messages::map::Map* proto_message);

/// @brief Converts provided static chunks to corresponding proto message
/// @param chunks List of static chunks to convert
/// @param proto_message Proto message that should be filled
void FillChunkingProtoRepresentation(const environment::chunking::StaticChunkList& chunks,
                                     messages::map::Map* proto_message);

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPPROTOCONVERSION_H
