/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_MESSAGECONVERTER_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_MESSAGECONVERTER_H

#include "Core/Environment/Chunking/world_chunk.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Service/Gui/ui_data.h"
#include "Core/Service/UserSettings/user_settings.h"
#include "message_size.pb.h"
#include "message_wrapper.pb.h"

#include <MantleAPI/Traffic/i_entity_repository.h>

#include <memory>
#include <optional>

namespace astas::communication
{

struct MessageContainer
{
    std::optional<int> Serialize(std::ostream& output_stream);
    messages::gui::MessageWrapper message_wrapper;
};

struct CoreConnectionAcknowledgement : MessageContainer
{
    explicit CoreConnectionAcknowledgement(std::int32_t pid, std::uint16_t sim_control_port = 0);
};

struct ControllerConnectionAcknowledgement : MessageContainer
{
    explicit ControllerConnectionAcknowledgement(std::int32_t pid);
};

struct JustPing : MessageContainer
{
    explicit JustPing(const std::string& message);
};

struct CommandAcknowledgement : MessageContainer
{
    explicit CommandAcknowledgement(messages::gui::MessageType ack_type);
};

struct ScenarioData : MessageContainer
{
    /// @brief used by EditorApi, when no scenario has been loaded
    ScenarioData(const environment::map::AstasMap& astas_map,
                 const service::user_settings::UserSettings& user_settings,
                 const std::string& user_settings_path,
                 const mantle_api::IEntityRepository* entity_repository);

    /// @brief used when none of the scenario data is available, messages will be default initialized
    ScenarioData();
};

struct EditorApiResponse : MessageContainer
{
    explicit EditorApiResponse(const mantle_api::Position& position);
    explicit EditorApiResponse(const mantle_api::Pose& world_pose);
    explicit EditorApiResponse(const std::string& error_message);
};

struct GroundTruth : MessageContainer
{
    explicit GroundTruth(const astas_osi3::GroundTruth& ground_truth);
};

struct SimGuiData : MessageContainer
{
    SimGuiData(const astas_osi3::GroundTruth& ground_truth, const service::gui::UiDataProvider& ui_data_provider);
};

/// @brief Converts byte arrays into proto messages and the other way round
class MessageConverter
{
  public:
    /// @brief Tries to convert received data into MessageWrapper proto message
    /// @param data Received bytes
    /// @param bytes_transferred Number of received bytes
    /// @return Const ptr to deserialized MessageWrapper, if conversion was successful, otherwise nullptr
    const messages::gui::MessageWrapper* TryGetProtoMessageWrapper(const char* data, std::size_t bytes_transferred);

    /// @brief Gets message size from a received MessageSize proto message. Used to determine the number of bytes that
    /// have to be read in order to completely receive the following MessageWrapper
    /// @param msg_size_buffer Contains the received MessageSize message
    /// @param bytes_transferred Number of received bytes
    /// @return Size extracted from the proto message
    std::uint32_t GetMessageSize(const char* msg_size_buffer, std::size_t bytes_transferred);

    /// @brief Creates a message of type T, if it can be constructed with the supplied args (compile-time check)
    /// @tparam T Type of message that should be created
    /// @param args Arguments given to the constructor of T
    /// @return Unique pointer to the created message
    template <class T, class... Args>
    std::enable_if_t<std::is_constructible<T, Args&&...>::value, std::unique_ptr<T>> CreateMessage(Args&&... args)
    {
        return std::make_unique<T>(std::forward<Args>(args)...);
    }

  private:
    messages::gui::MessageWrapper last_received_message_;
    messages::gui::MessageSize msg_size_;
};

}  // namespace astas::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_MESSAGECONVERTER_H
