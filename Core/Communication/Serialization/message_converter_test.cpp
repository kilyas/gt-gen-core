/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/message_converter.h"

#include <boost/asio/streambuf.hpp>
#include <gtest/gtest.h>

#include <array>

namespace astas::communication
{

using ProtoEditorApi = messages::editor::EditorApi;

class MessageConverterTest : public testing::Test
{
  public:
    MessageConverterTest() : output_stream_{&send_buffer_} {}

    const messages::gui::MessageWrapper* GetDeserializedMessage(int number_of_bytes)
    {
        const auto* sent_data = boost::asio::buffer_cast<const char*>(send_buffer_.data());
        return converter_.TryGetProtoMessageWrapper(sent_data, static_cast<std::size_t>(number_of_bytes));
    }

  protected:
    MessageConverter converter_{};

    boost::asio::streambuf send_buffer_;
    std::ostream output_stream_;
};

TEST_F(MessageConverterTest,
       GivenCreateCoreAcknowledgementMessage_WhenSerialize_ThenCanBeConvertedBackToProtoMessageWrapper)
{
    std::int32_t pid = 12345;

    auto message = converter_.CreateMessage<CoreConnectionAcknowledgement>(pid);

    auto number_of_serialized_bytes = message->Serialize(output_stream_);
    auto deserialized_message = GetDeserializedMessage(number_of_serialized_bytes.value());

    EXPECT_EQ(deserialized_message->message_type_identifier(), messages::gui::IAMACORE);
}

TEST_F(MessageConverterTest,
       GivenCreateCoreAcknowledgementMessage_WhenSerializeAndDeserializeAgain_ThenContentIsCorrect)
{
    std::int32_t pid = 12345;

    auto message = converter_.CreateMessage<CoreConnectionAcknowledgement>(pid);

    auto number_of_serialized_bytes = message->Serialize(output_stream_);
    auto deserialized_message = GetDeserializedMessage(number_of_serialized_bytes.value());

    EXPECT_EQ(deserialized_message->core_info().pid(), pid);
}

TEST_F(MessageConverterTest, GivenCreatePingMessage_WhenSerialize_ThenCanBeConvertedBackToProtoMessageWrapper)
{
    auto message = converter_.CreateMessage<JustPing>("hello");

    auto number_of_serialized_bytes = message->Serialize(output_stream_);
    auto deserialized_message = GetDeserializedMessage(number_of_serialized_bytes.value());

    EXPECT_EQ(deserialized_message->message_type_identifier(), messages::gui::JUSTPING);
}

TEST_F(MessageConverterTest,
       GivenNoScenarioDataProvided_WhenCreateScenarioDataMessage_ThenDefaultInitializedWithoutContent)
{
    auto message = converter_.CreateMessage<ScenarioData>();

    auto number_of_serialized_bytes = message->Serialize(output_stream_);
    auto deserialized_message = GetDeserializedMessage(number_of_serialized_bytes.value());

    EXPECT_EQ(deserialized_message->message_type_identifier(), messages::gui::HERESCEN);

    ASSERT_TRUE(deserialized_message->has_scenario_data());
    EXPECT_FALSE(deserialized_message->scenario_data().has_astas_map());
    EXPECT_FALSE(deserialized_message->scenario_data().has_scenario());
    EXPECT_FALSE(deserialized_message->scenario_data().has_config());
}

TEST_F(MessageConverterTest,
       GivenEditorApiResponseWithErrorMessage_WhenSerializeAndDeserializeAgain_ThenContentIsCorrect)
{
    std::string error_message = "You shall not pass!";

    auto message = converter_.CreateMessage<EditorApiResponse>(error_message);

    auto number_of_serialized_bytes = message->Serialize(output_stream_);
    auto deserialized_message = GetDeserializedMessage(number_of_serialized_bytes.value());

    ASSERT_TRUE(deserialized_message->has_editor_api());
    ASSERT_EQ(deserialized_message->editor_api().payload_case(), ProtoEditorApi::PayloadCase::kErrorMessage);

    EXPECT_EQ(deserialized_message->editor_api().error_message(), error_message);
}

TEST_F(MessageConverterTest, GivenEditorApiResponseWithOdrPos_WhenConstructed_ThenProtoOdrPosIsSet)
{
    mantle_api::OpenDriveLanePosition dummy_pos{};
    auto message = converter_.CreateMessage<EditorApiResponse>(dummy_pos);
    const auto& message_wrapper = message->message_wrapper;

    ASSERT_TRUE(message_wrapper.has_editor_api());
    EXPECT_EQ(message_wrapper.editor_api().payload_case(), ProtoEditorApi::PayloadCase::kOdrPosition);
}

TEST_F(MessageConverterTest, GivenEditorApiResponseWithNdsPos_WhenConstructed_ThenProtoLatLonPosIsSet)
{
    mantle_api::LatLonPosition dummy_pos{};
    auto message = converter_.CreateMessage<EditorApiResponse>(dummy_pos);
    const auto& message_wrapper = message->message_wrapper;

    ASSERT_TRUE(message_wrapper.has_editor_api());
    EXPECT_EQ(message_wrapper.editor_api().payload_case(), ProtoEditorApi::PayloadCase::kLatLonPosition);
}

TEST_F(MessageConverterTest, GivenEditorApiResponseWithPose_WhenConstructed_ThenProtoPoseIsSet)
{
    mantle_api::Pose dummy_pose{};
    auto message = converter_.CreateMessage<EditorApiResponse>(dummy_pose);
    const auto& message_wrapper = message->message_wrapper;

    ASSERT_TRUE(message_wrapper.has_editor_api());
    EXPECT_EQ(message_wrapper.editor_api().payload_case(), ProtoEditorApi::PayloadCase::kWorldPose);
}

TEST_F(MessageConverterTest, GivenInvalidOstream_WhenSerializeMessage_ThenDoesntThrowAndReturnsNullOpt)
{
    struct MyStream : public std::ostream  // NOLINT(fuchsia-multiple-inheritance)
    {
    };

    auto message = converter_.CreateMessage<JustPing>("Beta is Latin for still doesn’t work.");

    MyStream my_stream;
    std::optional<int> serialized_bytes{};
    EXPECT_NO_THROW(serialized_bytes = message->Serialize(my_stream));
    EXPECT_EQ(serialized_bytes, std::nullopt);
}

TEST_F(MessageConverterTest, GivenInvalidByteData_WhenTryGetProtoMessageWrapper_ThenReturnsNullPtr)
{
    std::array<const char, 10> invalid_data{};
    auto result = converter_.TryGetProtoMessageWrapper(invalid_data.data(), static_cast<std::size_t>(10));
    EXPECT_EQ(result, nullptr);
}

TEST_F(MessageConverterTest, GivenInvalidByteData_WhenGetMessageSize_ThenReturnsZero)
{
    std::array<const char, 10> invalid_data{};
    auto result = converter_.GetMessageSize(invalid_data.data(), static_cast<std::size_t>(10));
    EXPECT_EQ(result, 0);
}

}  // namespace astas::communication
