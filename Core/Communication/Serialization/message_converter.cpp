/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Communication/Serialization/message_converter.h"

#include "Core/Communication/Serialization/Internal/config_proto_conversion.h"
#include "Core/Communication/Serialization/Internal/mantle_position_conversions.h"
#include "Core/Communication/Serialization/Internal/map_proto_conversion.h"
#include "Core/Communication/Serialization/Internal/ui_data_proto_conversion.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"

namespace astas::communication
{

std::optional<int> MessageContainer::Serialize(std::ostream& output_stream)
{
    if (const bool success = message_wrapper.SerializeToOstream(&output_stream); success)
    {
        return message_wrapper.ByteSize();
    }

    return std::nullopt;
}

const messages::gui::MessageWrapper* MessageConverter::TryGetProtoMessageWrapper(const char* data,
                                                                                 std::size_t bytes_transferred)
{
    if (const bool success = last_received_message_.ParseFromArray(data, static_cast<int>(bytes_transferred)); success)
    {
        return &last_received_message_;
    }

    return nullptr;
}

std::uint32_t MessageConverter::GetMessageSize(const char* msg_size_buffer, std::size_t bytes_transferred)
{
    if (const bool success = msg_size_.ParseFromArray(msg_size_buffer, static_cast<int>(bytes_transferred)); success)
    {
        return msg_size_.num_bytes();
    }

    return 0;
}

CoreConnectionAcknowledgement::CoreConnectionAcknowledgement(std::int32_t pid, std::uint16_t sim_control_port)
{
    message_wrapper.set_message_type_identifier(messages::gui::IAMACORE);
    message_wrapper.mutable_core_info()->set_pid(pid);
    message_wrapper.mutable_core_info()->set_sim_control_port(sim_control_port);
}

ControllerConnectionAcknowledgement::ControllerConnectionAcknowledgement(std::int32_t pid)
{
    message_wrapper.set_message_type_identifier(messages::gui::ICONTROL);
    message_wrapper.set_message_string("SimulationController at PID " + std::to_string(pid));
}

JustPing::JustPing(const std::string& message)
{
    message_wrapper.set_message_type_identifier(messages::gui::JUSTPING);
    message_wrapper.set_message_string(message);
}

CommandAcknowledgement::CommandAcknowledgement(const messages::gui::MessageType ack_type)
{
    message_wrapper.set_message_type_identifier(ack_type);
}

ScenarioData::ScenarioData(const environment::map::AstasMap& astas_map,
                           const service::user_settings::UserSettings& user_settings,
                           const std::string& user_settings_path,
                           const mantle_api::IEntityRepository* entity_repository)
{
    message_wrapper.set_message_type_identifier(messages::gui::HERESCEN);
    message_wrapper.mutable_scenario_data()->mutable_scenario()->set_map(astas_map.path);
    FillMapProtoRepresentation(
        astas_map, entity_repository, message_wrapper.mutable_scenario_data()->mutable_astas_map());
    FillConfigProtoRepresentation(
        user_settings, user_settings_path, message_wrapper.mutable_scenario_data()->mutable_config());
}

ScenarioData::ScenarioData()
{
    message_wrapper.set_message_type_identifier(messages::gui::HERESCEN);
    messages::gui::ScenarioData empty_data{};
    message_wrapper.mutable_scenario_data()->CopyFrom(empty_data);
}

void SetGroundTruth(const astas_osi3::GroundTruth& ground_truth, messages::gui::MessageWrapper& message_wrapper)
{
    message_wrapper.set_message_type_identifier(messages::gui::GROUNDTR);
    // we have to copy here, otherwise we'd have to lock the core during the whole serialization as well
    *message_wrapper.mutable_ground_truth() = ground_truth;
}

GroundTruth::GroundTruth(const astas_osi3::GroundTruth& ground_truth)
{
    SetGroundTruth(ground_truth, message_wrapper);
}

SimGuiData::SimGuiData(const astas_osi3::GroundTruth& ground_truth,
                       const service::gui::UiDataProvider& ui_data_provider)
{
    SetGroundTruth(ground_truth, message_wrapper);
    FillUiDataProtoRepresentation(ui_data_provider.GetUiData(), message_wrapper.mutable_additional_ui());
}

EditorApiResponse::EditorApiResponse(const std::string& error_message)
{
    message_wrapper.set_message_type_identifier(messages::gui::SCENEDIT);
    message_wrapper.mutable_editor_api()->set_error_message(error_message);
    message_wrapper.mutable_editor_api()->set_response(messages::editor::EDITOR_RESPONSE_ERROR);
}

EditorApiResponse::EditorApiResponse(const mantle_api::Position& position)
{
    message_wrapper.set_message_type_identifier(messages::gui::SCENEDIT);
    auto* editor_api = message_wrapper.mutable_editor_api();

    FillEditorResponsePosition(position, editor_api);
}

EditorApiResponse::EditorApiResponse(const mantle_api::Pose& world_pose)
{
    message_wrapper.set_message_type_identifier(messages::gui::SCENEDIT);

    auto* editor_api = message_wrapper.mutable_editor_api();
    editor_api->set_response(messages::editor::EDITOR_RESPONSE_WORLD_POSE);

    auto* proto_pose = editor_api->mutable_world_pose();
    service::gt_conversion::FillProtoObject(world_pose.position, proto_pose->mutable_position());
    service::gt_conversion::FillProtoObject(world_pose.orientation, proto_pose->mutable_orientation());
}

}  // namespace astas::communication
