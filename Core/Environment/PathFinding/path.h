/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_PATHFINDING_PATH_H
#define GTGEN_CORE_ENVIRONMENT_PATHFINDING_PATH_H

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/PathFinding/Internal/lane_graph_types.h"

#include <deque>
#include <memory>

namespace astas::environment::path_finding
{

/// @brief Path representing the route of a route vehicle
class Path : public std::deque<std::shared_ptr<PathEntry>>
{
  public:
    void EnableClosedPath();

    bool IsClosedPath() const;

    const environment::map::Lane* GetCurrentLane() const;
    void PopCurrentLane();

  private:
    bool IsLastPathLaneNotEqualTo(const environment::map::Lane* lane) const;

    bool is_closed_path_{false};
};

}  // namespace astas::environment::path_finding
#endif  // GTGEN_CORE_ENVIRONMENT_PATHFINDING_PATH_H
