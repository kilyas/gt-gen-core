/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/path_finder.h"

#include "Core/Environment/Map/Common/exceptions.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_lane_builder.h"

#include <gtest/gtest.h>

namespace astas::environment::path_finding
{
using units::literals::operator""_m;

class PathFinderTest : public testing::Test
{
  protected:
    environment::map::AstasMap astas_map_;
};

environment::map::AstasMap GetAstasMapWithNonDrivingLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetNonDrivable();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithDrivableLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetDrivable();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithShoulderLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetShoulderLane();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithMergeLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetMergeLane();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithSplitLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetSplitLane();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithNormalLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetNormalLane();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithEntryLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetEntryLane();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithExitLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetExitLane();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithParkingLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetParking();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithBicycleLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetBicycle();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithOneLaneAndOneShoulderLane()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithTwoEastingLanes(map_builder, {0_m, 0_m, 0_m});
    map_builder.GetLastAddedLaneHandle().flags.SetShoulderLane();

    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithDoubleSplitAndDoubleMerge()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {0_m, 4_m, 0_m});
    test_utils::LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanesSplitBoth(map_builder, {99_m, 0_m, 0_m});
    test_utils::LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanesMergeBoth(map_builder, {198_m, 0_m, 0_m});
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {297_m, 4_m, 0_m});
    return std::move(*map_builder.Build());
}

environment::map::AstasMap GetAstasMapWithDoubleMergeAndDoubleSplit()
{
    test_utils::RawMapBuilder map_builder;
    test_utils::LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanes(map_builder, {0_m, 0_m, 0_m});
    test_utils::LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanesMergeBoth(map_builder, {99_m, 0_m, 0_m});
    test_utils::LaneGroupCatalogue::AddLaneGroupWithOneEastingLane(map_builder, {198_m, 4_m, 0_m});
    test_utils::LaneGroupCatalogue::AddLaneGroupWithThreeEastingLanesSplitBoth(map_builder, {297_m, 0_m, 0_m});

    return std::move(*map_builder.Build());
}

TEST_F(PathFinderTest, GivenMapWithNonDrivingLane_WhenLaneGraphConstructed_ThenTheseLanesAreNotAdded)
{
    PathFinder path_finder{GetAstasMapWithNonDrivingLane()};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    EXPECT_EQ(0, lane_graph.nodes.size());
    EXPECT_EQ(0, lane_graph.edges.size());
}

TEST_F(PathFinderTest, GivenMapWithShoulderLane_WhenLaneGraphConstructed_ThenTheseLanesAreNotAdded)
{
    PathFinder path_finder{GetAstasMapWithShoulderLane()};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    EXPECT_EQ(0, lane_graph.nodes.size());
    EXPECT_EQ(0, lane_graph.edges.size());
}

TEST_F(PathFinderTest, GivenMapWithOneLaneAndOneShoulderLane_WhenLaneGraphConstructed_ThenOnlyDrivableLaneIsContained)
{
    PathFinder path_finder{GetAstasMapWithOneLaneAndOneShoulderLane()};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    EXPECT_EQ(1, lane_graph.nodes.size());
    EXPECT_EQ(0, lane_graph.edges.size());
}

TEST_F(PathFinderTest, GivenEmptyMap_WhenRouteFromWaypointsRequested_ThenNoPathCalculated)
{
    auto astas_map = test_utils::MapCatalogue::EmptyMap();
    PathFinder path_finder{*astas_map};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {1000.0_m, 1111.0_m, 42.0_m}};
    EXPECT_FALSE(path_finder.ComputeRoute(route).has_value());
}

TEST_F(PathFinderTest, GivenMapWithDrivableLane_WhenRouteOnDrivableLaneIsRequested_ThenNoExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithDrivableLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_NO_THROW(path_finder.ComputeRoute(route));
}

TEST_F(PathFinderTest, GivenMapWithShoulderLane_WhenRouteOnShoulderLaneIsRequested_ThenExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithShoulderLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_THROW(path_finder.ComputeRoute(route), environment::map::exception::MapException);
}

TEST_F(PathFinderTest, GivenMapWithMergeLane_WhenRouteOnMergeLaneIsRequested_ThenNoExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithMergeLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_NO_THROW(path_finder.ComputeRoute(route));
}

TEST_F(PathFinderTest, GivenMapWithSplitLane_WhenRouteOnSplitLaneIsRequested_ThenNoExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithSplitLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_NO_THROW(path_finder.ComputeRoute(route));
}

TEST_F(PathFinderTest, GivenMapWithNormalLane_WhenRouteOnNormalLaneIsRequested_ThenNoExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithNormalLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_NO_THROW(path_finder.ComputeRoute(route));
}

TEST_F(PathFinderTest, GivenMapWithEntryLane_WhenRouteOnEntryLaneIsRequested_ThenNoExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithEntryLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_NO_THROW(path_finder.ComputeRoute(route));
}

TEST_F(PathFinderTest, GivenMapWithExitLane_WhenRouteOnExitLaneIsRequested_ThenNoExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithExitLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_NO_THROW(path_finder.ComputeRoute(route));
}

TEST_F(PathFinderTest, GivenMapWithParkingLane_WhenRouteOnParkingLaneIsRequested_ThenExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithParkingLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_THROW(path_finder.ComputeRoute(route), environment::map::exception::MapException);
}

TEST_F(PathFinderTest, GivenMapWithBicycleLane_WhenRouteOnBicycleLaneIsRequested_ThenExceptionIsThrown)
{
    astas_map_ = GetAstasMapWithBicycleLane();
    PathFinder path_finder{astas_map_};

    std::vector<mantle_api::Vec3<units::length::meter_t>> route{{0.0_m, 0.0_m, 0.0_m}, {99.0_m, 0.0_m, 0.0_m}};
    EXPECT_THROW(path_finder.ComputeRoute(route), environment::map::exception::MapException);
}

TEST_F(PathFinderTest, GivenSingleLane_WhenPathFromBeginToEndRequested_ThenPathContainsOnlyOneEntry)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(1, lane_graph.nodes.size());
    ASSERT_EQ(0, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{99.0_m, 0.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(1, path.size());
    EXPECT_EQ(astas_map_.GetLanes().front().id, path.front()->lane->id);
}

TEST_F(PathFinderTest, GivenLaneWithOneSuccessorLane_WhenPathIsRequestedFromOneLaneToTheOther_ThenPathContainsBothLanes)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(2, lane_graph.nodes.size());
    ASSERT_EQ(1, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{198.0_m, 0.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(2, path.size());
    EXPECT_EQ(astas_map_.GetLanes().front().id, path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLanes().back().id, path.at(1)->lane->id);
}

TEST_F(PathFinderTest,
       GivenThreeConsecutiveLanes_WhenRouteRequestedWithOnePointOnEachLane_ThenPathContainsEachLaneAsNode)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(3, lane_graph.nodes.size());
    ASSERT_EQ(2, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from = {1.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> via = {142_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to = {297_m, 0.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, via, to});

    ASSERT_EQ(3, path.size());
    EXPECT_EQ(astas_map_.GetLanes().at(0).id, path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLanes().at(1).id, path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLanes().at(2).id, path.at(2)->lane->id);
}

TEST_F(PathFinderTest,
       Given3x3ConnectedLanes_WhenPathRequestedUpperLowerLeftToLowerRight_ThenPathGoesStraightAndThenChangesLanesTwice)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(9, lane_graph.nodes.size());
    ASSERT_EQ(18, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{297.0_m, 8.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(5, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(0), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(0), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(0), path.at(2)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(1), path.at(3)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(2), path.at(4)->lane->id);
}

TEST_F(PathFinderTest,
       Given3x3ConnectedLanes_WhenPathRequestedFromUpperLeftToLowerRightViaMiddleLane_ThenPathGoesViaMiddleLane)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(9, lane_graph.nodes.size());
    ASSERT_EQ(18, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> via{198.0_m, 4.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{297.0_m, 8.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, via, to});

    ASSERT_EQ(5, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(0), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(0), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(1), path.at(2)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(1), path.at(3)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(2), path.at(4)->lane->id);
}

TEST_F(PathFinderTest, GivenATwoLanesToOneLaneMerge_WhenPathRequestedFromRightLaneOverMerge_ThenPathIsFound)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::MapMergeTwoLanesToTheRightIntoOneLane());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(5, lane_graph.nodes.size());
    ASSERT_EQ(8, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 4.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{297.0_m, 0.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(3, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(1), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(1), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(0), path.at(2)->lane->id);
}

TEST_F(PathFinderTest, GivenATwoLanesToOneLaneMerge_WhenPathRequestedFromLeftLaneOverMerge_ThenPathIsFound)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::MapMergeTwoLanesToTheLeftIntoOneLane());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(5, lane_graph.nodes.size());
    ASSERT_EQ(8, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{297.0_m, 4.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(3, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(0), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(0), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(0), path.at(2)->lane->id);
}

TEST_F(PathFinderTest, GivenALaneSplit_WhenPathRequestedToChangeToRightLaneAfterSplit_ThenPathIsFound)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(5, lane_graph.nodes.size());
    ASSERT_EQ(8, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{297.0_m, 4.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(3, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(0), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(1), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(1), path.at(2)->lane->id);
}

TEST_F(PathFinderTest, GivenALaneSplit_WhenPathRequestedToChangeToLeftLaneAfterSplit_ThenPathIsFound)
{
    astas_map_ = std::move(*test_utils::MapCatalogue::MapSplitOneLaneToTheRightIntoTwoLanes());
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(5, lane_graph.nodes.size());
    ASSERT_EQ(8, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 4.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{297.0_m, 0.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(3, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(0), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(0), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(0), path.at(2)->lane->id);
}

TEST_F(PathFinderTest, GivenALaneDoubleSplitAndDoubleMerge_WhenStartingAndEndingOnTheMiddleLane_ThenStraightPathIsFound)
{
    /// @verbatim
    ///
    ///                     /---------------|--------------\
    ///                    /x x x x x x x x | x x x x x x x \ y: 8
    /// ------------------|-----------------|------------------------------------
    /// | >>>>>>>>>>>>>>> | >>>>>>>>>>>>>>> | >>>>>>>>>>>>>> | >>>>>>>>>>>>>>>> | y: 4
    /// ------------------|-----------------|------------------------------------
    /// ^                  \ x x x  x x x x | x x x x x x x / y: 0              ^
    /// (Start: 0;4)        \---------------|--------------/                (End: 396;4)
    ///
    /// @endverbatim
    astas_map_ = GetAstasMapWithDoubleSplitAndDoubleMerge();
    PathFinder path_finder{astas_map_};

    EXPECT_EQ(astas_map_.GetLanes().at(3).center_line.front().y, 4_m);
    EXPECT_EQ(astas_map_.GetLanes().at(3).center_line.back().y, 8_m);

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(8, lane_graph.nodes.size());
    ASSERT_EQ(17, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 4.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{396.0_m, 4.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(4, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(0), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(1), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(1), path.at(2)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(3).lane_ids.at(0), path.at(3)->lane->id);
}

TEST_F(PathFinderTest,
       GivenALaneDoubleMergeAndDoubleSplit_WhenStartingLowerLeftAndEndingUpperRightLane_ThenDiagonalPathIsFound)
{
    /// @verbatim
    ///                                                                     End (396;8)
    ///                                                                         v
    /// ---------------------------------\                      /----------------
    /// | x x x x x x x x | x x x x x x x \                    / >>>>>>>>>>>>>> | y: 8
    /// -------------------------------------------------------------------------
    /// | x x x x x x x x | x x x x x x x x | >>>>>>>>>>>>>>> | x x x x x x x x | y: 4
    /// -------------------------------------------------------------------------
    /// | >>>>>>>>>>>>>>> | >>>>>>>>>>>>> /                    \ x x x  x x x x | y: 0
    /// ---------------------------------/                      \----------------
    /// ^
    /// Start (0;0)
    ///
    /// @endverbatim
    astas_map_ = GetAstasMapWithDoubleMergeAndDoubleSplit();
    PathFinder path_finder{astas_map_};

    const LaneGraph& lane_graph = path_finder.GetLaneGraph();
    ASSERT_EQ(10, lane_graph.nodes.size());
    ASSERT_EQ(21, lane_graph.edges.size());

    mantle_api::Vec3<units::length::meter_t> from{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> to{396.0_m, 8.0_m, 0.0_m};
    Path path = *path_finder.ComputeRoute({from, to});

    ASSERT_EQ(4, path.size());
    EXPECT_EQ(astas_map_.GetLaneGroups().at(0).lane_ids.at(0), path.at(0)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(1).lane_ids.at(0), path.at(1)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(2).lane_ids.at(0), path.at(2)->lane->id);
    EXPECT_EQ(astas_map_.GetLaneGroups().at(3).lane_ids.at(2), path.at(3)->lane->id);
}

}  // namespace astas::environment::path_finding
