/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_ASTARALGORITHM_H
#define GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_ASTARALGORITHM_H

#include "Core/Environment/PathFinding/Internal/path_finding_list.h"
#include "Core/Environment/PathFinding/path.h"

#include <optional>

namespace astas::environment::path_finding
{

/// @brief Implementation of the A-Star shortest path finding algorithm
class AStarAlgorithm
{
  public:
    /// @brief Computes the shortest path between at least two given lane graph nodes
    /// @return Shortest path, if any path exists
    std::optional<Path> ShortestPath(const std::vector<SharedNode>& nodes);

  private:
    Path InitPath(const std::vector<SharedNode>& nodes) const;
    void AppendToOverallPath(Path& overall_path, const Path& path_to_append) const;

    /// @brief Computes the shortest path between two nodes
    std::optional<Path> ShortestPathBetweenTwoNodes(const SharedNode& start_node, const SharedNode& target_node);

    /// @brief Computes a list of connections, which represent the shortest path
    std::optional<Path> CalculateShortestPath();

    /// @brief Traverses the graph until all nodes have been processed or the target is reached
    RecordPtr TraverseNodesToTarget();

    /// @brief Processes all the successor nodes for the current node
    void ProcessSuccessors(const RecordPtr& current_record);

    /// @brief Constructs the path by going backwards from target until the start is reached
    std::optional<Path> ConvertInternalStructureToPath(RecordPtr& to_node, const SharedNode& from_node);

    /// @brief Evaluates if a given node was already visited and thus handled
    bool CanNodeBeSkipped(const SharedNode& successor_node) const;

    /// @brief Evaluates if a previous added node to the open list has a better cost than the current path to it
    bool AlreadyVisitedNodeIsBetter(const RecordPtr& already_visited_record, double cost_to_successor) const;

    /// @brief Computes euclidean distance between from_node and to_node as heuristic
    double DistanceBetweenNodes(const SharedNode& from_node, const SharedNode& to_node) const;

    /// @brief Converts the start node to a record and puts it into the open list
    void CreateStartRecord();

    /// @brief Adds a new node to the open list
    void AddOpenListRecord(const SharedNode& successor_node,
                           const RecordPtr& current,
                           double cost_to_successor,
                           EdgeType edge_type);
    /// @brief Updates an existing node in the open list when the current path to it is cheaper
    void UpdateOpenListRecord(const RecordPtr& open_list_end_node_record,
                              const SharedNode& successor_node,
                              const RecordPtr& current,
                              double cost_to_successor,
                              EdgeType edge_type);

    /// @brief PriorityQueue with the not yet visited nodes. The node with the smallest cost is always returned
    PathFindingList open_list_{};
    /// @brief List of already visited nodes
    PathFindingList closed_list_{};

    /// @brief The node to start the path
    SharedNode start_node_{nullptr};
    /// @brief The node to find the shortest path to
    SharedNode target_node_{nullptr};
};

}  // namespace astas::environment::path_finding

#endif  // GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_ASTARALGORITHM_H
