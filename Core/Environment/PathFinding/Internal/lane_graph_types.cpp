/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/Internal/lane_graph_types.h"

namespace astas::environment::path_finding
{

//////////////////////////////////////
/// Node
Node::Node(const environment::map::Lane* l) : lane{l}, world_position{l->center_line[l->center_line.size() / 2]} {}

//////////////////////////////////////
/// Edge
Edge::Edge(const WeakNode& from, const WeakNode& to, double w, EdgeType t)
    : from_node{from}, to_node{to}, weight{w}, type{t}
{
}

//////////////////////////////////////
/// PathEntry
PathEntry::PathEntry(const environment::map::Lane* l, EdgeType t) : lane{l}, edge_type{t} {}

}  // namespace astas::environment::path_finding
