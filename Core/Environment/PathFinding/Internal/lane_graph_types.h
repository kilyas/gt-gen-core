/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_LANEGRAPHTYPES_H
#define GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_LANEGRAPHTYPES_H

#include "Core/Environment/Map/AstasMap/lane.h"

#include <deque>
#include <memory>
#include <stack>
#include <vector>

namespace astas::environment::path_finding
{
struct PathEntry;
struct Node;
using WeakNode = std::weak_ptr<Node>;
using SharedNode = std::shared_ptr<Node>;
using SharedNodes = std::vector<SharedNode>;

struct Edge;
using SharedEdge = std::shared_ptr<Edge>;
using WeakEdge = std::weak_ptr<Edge>;
using SharedEdges = std::vector<SharedEdge>;
using WeakEdges = std::vector<WeakEdge>;

enum class EdgeType
{
    kFollow,
    kChange
};

//////////////////////////////////////
/// @brief LaneGraph node, representing a GTGen map lane
struct Node
{
    /// @brief Constructs a node form a given lane
    explicit Node(const environment::map::Lane* l);

    const environment::map::Lane* lane;
    mantle_api::Vec3<units::length::meter_t> world_position;
    WeakEdges edges{};
};

//////////////////////////////////////
/// @brief LaneGraph edge, representing a connection from one GTGen map lane to another
struct Edge
{
    /// @brief Constructs an edge between to nodes with a given weight
    Edge(const WeakNode& from, const WeakNode& to, double w, EdgeType t);

    WeakNode from_node;
    WeakNode to_node;
    double weight;
    EdgeType type;
};

//////////////////////////////////////
/// @brief PathEntry representing an entry in the path object
struct PathEntry
{
    PathEntry(const environment::map::Lane* l, EdgeType t);
    const environment::map::Lane* lane;
    EdgeType edge_type;
};

}  // namespace astas::environment::path_finding

#endif  // GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_LANEGRAPHTYPES_H
