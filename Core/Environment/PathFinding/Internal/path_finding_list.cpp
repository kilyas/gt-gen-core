/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/Internal/path_finding_list.h"

#include <algorithm>

namespace astas::environment::path_finding
{
RecordPtr PathFindingList::TopAndPop()
{
    auto smallest = top();
    pop();
    return smallest;
}

bool PathFindingList::Contains(const SharedNode& node) const
{
    return FindNode(node) != c.end();
}

RecordPtr PathFindingList::GetIfContained(const SharedNode& node) const
{
    auto it = FindNode(node);
    if (it != c.end())
    {
        return *it;
    }

    return nullptr;
}

PathFindingList::container_type::const_iterator PathFindingList::FindNode(const SharedNode& node) const
{
    return std::find_if(c.begin(), c.end(), [node](const RecordPtr& record) {
        return node->lane->id == record->node.lock()->lane->id;
    });
}

}  // namespace astas::environment::path_finding
