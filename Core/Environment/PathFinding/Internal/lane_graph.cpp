/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/Internal/lane_graph.h"

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Service/Logging/logging.h"

namespace astas::environment::path_finding
{
bool LaneGraph::ContainsNode(const environment::map::Lane* lane) const
{
    auto it =
        std::find_if(nodes.begin(), nodes.end(), [&lane](const SharedNode& n) { return n->lane->id == lane->id; });
    return it != nodes.end();
}

bool LaneGraph::ContainsEdge(const SharedNode& from_node, const SharedNode& to_node, double weight, EdgeType type) const
{
    auto it = std::find_if(edges.begin(), edges.end(), [&](const SharedEdge& e) {
        return e->from_node.lock() == from_node && e->to_node.lock() == to_node &&
               fabs(e->weight - weight) < std::numeric_limits<double>::epsilon() && e->type == type;
    });
    return it != edges.end();
}

void LaneGraph::AddNode(const environment::map::Lane* lane)
{
    ASSERT(!lane->center_line.empty() && "Cannot add a node to lane graph with empty center line");
    if (!ContainsNode(lane))
    {
        nodes.emplace_back(std::make_shared<Node>(lane));
    }
}

void LaneGraph::AddEdge(const environment::map::Lane* from_lane,
                        const environment::map::Lane* to_lane,
                        double weight,
                        EdgeType type)
{
    ASSERT(from_lane != nullptr && "Starting lane must be contained in the graph before an edge can be added");
    ASSERT(to_lane != nullptr && "Ending lane must be contained in the graph before an edge can be added");

    SharedNode from_node = GetNode(from_lane);
    SharedNode to_node = GetNode(to_lane);

    if (!ContainsEdge(from_node, to_node, weight, type))
    {
        SharedEdge e{std::make_shared<Edge>(from_node, to_node, ComputeEdgeWeight(weight, type), type)};
        edges.push_back(e);
        from_node->edges.push_back(e);
    }
}

SharedNode LaneGraph::GetNode(const environment::map::Lane* lane) const
{
    auto it = std::find_if(nodes.begin(), nodes.end(), [lane](const SharedNode& n) { return n->lane->id == lane->id; });
    if (it == nodes.end())
    {
        return nullptr;
    }
    return *it;
}

double LaneGraph::ComputeEdgeWeight(double weight, EdgeType type) const
{
    double lane_change_penalty{9999};  // arbitrary value, must be big
    if (type == EdgeType::kChange)
    {
        return weight * lane_change_penalty;
    }
    return weight;
}

}  // namespace astas::environment::path_finding
