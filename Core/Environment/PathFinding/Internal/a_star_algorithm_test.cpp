/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/Internal/a_star_algorithm.h"

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/PathFinding/Internal/lane_graph.h"
#include "Core/Service/Logging/logging.h"

#include <gtest/gtest.h>

namespace astas::environment::path_finding
{
using units::literals::operator""_m;

TEST(AStarAlgorithmTest, GivenLaneGraphWithOneNode_WhenShortestPath_ThenPathWithOneNodeReturned)
{
    Info("GivenLaneGraphWithOneNode_WhenShortestPath_ThenPathWithOneNodeReturned");

    // Lane Graph:        Path:
    // L0           ==>   L0
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane0);

    Path path = *a_star.ShortestPath({from_node, to_node});
    EXPECT_EQ(1, path.size());
    EXPECT_EQ(path.front()->lane->id, from_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(AStarAlgorithmTest,
     GivenLaneGraphWithTwoConnectedLanes_WhenShortestPathFromLaneZeroToLaneZero_ThenPathContainsOneNode)
{
    Info("LaneGraphTwoNodesOneEdge_StartAndEndEquals_PathWithOneNode");

    // Lane Graph:       Path:
    // L0 ------ L1  ==> L0
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({10.0_m, 0.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane1);
    graph.AddEdge(&lane0, &lane1, 10, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane0);

    Path path = *a_star.ShortestPath({from_node, to_node});
    EXPECT_EQ(1, path.size());
    EXPECT_EQ(path.front()->lane->id, from_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(AStarAlgorithmTest,
     GivenLaneGraphWithTwoConnectedLanes_WhenShortestPathFromLaneZeroToLaneOne_ThenPathContainsOneNode)
{
    Info("LaneGraphTwoNodesOneEdge_FromLane0ToLane1_PathWithTwoNodesFound");

    // Lane Graph:       Path:
    // L0 ------ L1  ==> L0 ------ L1
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({10.0_m, 0.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane1);
    graph.AddEdge(&lane0, &lane1, 10, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane1);

    Path path = *a_star.ShortestPath({from_node, to_node});
    EXPECT_EQ(2, path.size());
    EXPECT_EQ(path.front()->lane->id, from_node->lane->id);
    EXPECT_EQ(path.back()->lane->id, to_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(AStarAlgorithmTest,
     GivenLaneGraphWithTreeConnectedLanes_WhenShortestPathFromLaneZeroToLaneTwo_ThenPathWithThreeNodesFound)
{
    Info("LaneGraphWithThreeNodes_FromLane0ToLane2_PathWithThreeNodesFound");

    // Lane Graph:      Path:
    //         L2               L2
    //         |                |
    //         |   ==>          |
    //         |                |
    // L0 ---- L1       L0 ---- L1
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({10.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({10.0_m, 10.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane1);
    graph.AddNode(&lane2);
    graph.AddEdge(&lane0, &lane1, 10, EdgeType::kFollow);
    graph.AddEdge(&lane1, &lane2, 10, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane2);

    Path path = *a_star.ShortestPath({from_node, to_node});
    EXPECT_EQ(3, path.size());
    EXPECT_EQ(path.at(0)->lane->id, from_node->lane->id);
    EXPECT_EQ(path.at(1)->lane->id, 1);
    EXPECT_EQ(path.at(2)->lane->id, to_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(AStarAlgorithmTest, GivenLaneGraphWithTwoPaths_WhenShortestPathFromLaneZeroToLaneTwo_ThenDiagonalShortestPathTaken)
{
    // Lane Graph:     Path:
    //         L2          L2
    //       / |          /
    //     L3  |   ==>   L3
    //    /    |        /
    // L0 ---- L1      L0
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({10.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({10.0_m, 5.0_m, 0.0_m});

    environment::map::Lane lane3{3};
    lane3.center_line.push_back({4.5_m, 4.5_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane1);
    graph.AddNode(&lane2);
    graph.AddNode(&lane3);
    graph.AddEdge(&lane0, &lane1, 10, EdgeType::kFollow);
    graph.AddEdge(&lane1, &lane2, 10, EdgeType::kFollow);
    graph.AddEdge(&lane0, &lane3, 7, EdgeType::kFollow);
    graph.AddEdge(&lane3, &lane2, 7, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane2);

    Path path = *a_star.ShortestPath({from_node, to_node});
    EXPECT_EQ(3, path.size());
    EXPECT_EQ(path.at(0)->lane->id, from_node->lane->id);
    EXPECT_EQ(path.at(1)->lane->id, 3);
    EXPECT_EQ(path.at(2)->lane->id, to_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(AStarAlgorithmTest,
     GivenLaneGraphWithSixNodes_WhenShortestPathFromLaneThreeToLaneTwo_ThenShortestDiagonalPathFound)
{
    // The diagonal path is slightly shorter
    //
    // Lane Graph:            Path:
    // L0 ----- L1--L2                 L1--L2
    //  |     /     |     ==>         /
    //  |    /      |                /
    // L3--L4 ----- L5         L3--L4
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});
    environment::map::Lane lane1{1};
    lane1.center_line.push_back({5.0_m, 0.0_m, 0.0_m});
    environment::map::Lane lane2{2};
    lane2.center_line.push_back({6.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane3{3};
    lane3.center_line.push_back({0.0_m, 1.0_m, 0.0_m});
    environment::map::Lane lane4{4};
    lane4.center_line.push_back({1.0_m, 1.0_m, 0.0_m});
    environment::map::Lane lane5{5};
    lane5.center_line.push_back({6.0_m, 1.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane1);
    graph.AddNode(&lane2);

    graph.AddNode(&lane3);
    graph.AddNode(&lane4);
    graph.AddNode(&lane5);

    graph.AddEdge(&lane0, &lane1, 5, EdgeType::kFollow);
    graph.AddEdge(&lane1, &lane2, 1, EdgeType::kFollow);

    graph.AddEdge(&lane3, &lane4, 1, EdgeType::kFollow);
    graph.AddEdge(&lane4, &lane5, 5, EdgeType::kFollow);

    graph.AddEdge(&lane3, &lane0, 1, EdgeType::kFollow);
    graph.AddEdge(&lane4, &lane1, 4.123, EdgeType::kFollow);
    graph.AddEdge(&lane5, &lane2, 1, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane3);
    SharedNode to_node = graph.GetNode(&lane2);

    Path path = *a_star.ShortestPath({from_node, to_node});
    EXPECT_EQ(4, path.size());
    EXPECT_EQ(path.at(0)->lane->id, from_node->lane->id);
    EXPECT_EQ(path.at(1)->lane->id, 4);
    EXPECT_EQ(path.at(2)->lane->id, 1);
    EXPECT_EQ(path.at(3)->lane->id, to_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(
    AStarAlgorithmTest,
    GivenLaneGraphWithFourNodesAndSomeEdgesWithSmallWeight_WhenShortestPathFromLaneZeroToLaneOne_ThenPathViaLaneTwoandLaneThreeFound)
{
    // Find the path over the edges with weight 1
    //
    // Lane Graph:       Path:
    // L0 --5-- L1        L0       L1
    //  |       |          |       |
    //  1       1    ==>   1       1
    //  |       |          |       |
    // L2 --1-- L3        L2 --1-- L3
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({5.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({0.0_m, 5.0_m, 0.0_m});

    environment::map::Lane lane3{3};
    lane3.center_line.push_back({5.0_m, 5.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);

    graph.AddNode(&lane2);
    graph.AddNode(&lane1);
    graph.AddNode(&lane3);

    graph.AddEdge(&lane0, &lane1, 5, EdgeType::kFollow);
    graph.AddEdge(&lane2, &lane3, 1, EdgeType::kFollow);

    graph.AddEdge(&lane0, &lane2, 1, EdgeType::kFollow);
    graph.AddEdge(&lane3, &lane1, 1, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane1);

    Path path = *a_star.ShortestPath({from_node, to_node});

    EXPECT_EQ(4, path.size());
    EXPECT_EQ(path.at(0)->lane->id, from_node->lane->id);
    EXPECT_EQ(path.at(1)->lane->id, 2);
    EXPECT_EQ(path.at(2)->lane->id, 3);
    EXPECT_EQ(path.at(3)->lane->id, to_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(AStarAlgorithmTest,
     GivenLaneGraphWith8NodesAndSomeEdgesWithSmallWeight_WhenShortestPathFromLaneZeroToLaneThree_ThenZickZackPathFound)
{
    Info("LaneGraphWith8NodesAndSomeEdgesWithSmallWeight_FromLane0ToLane3_ZickZackPathFound");

    // Find the zig-zack path with the 1-weight edges
    //
    // Lane Graph:                        Path:
    // L0 --5-- L1 --1-- L2 --5-- L3      L0       L1 --1-- L2       L3
    //  |       |        |        |        |       |        |        |
    //  1       1        1        1   ==>  1       1        1        1
    //  |       |        |        |        |       |        |        |
    // L4 --1-- L5 --5-- L6 --1-- L7      L4 --1-- L5       L6 --1-- L7
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({5.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({10.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane3{3};
    lane3.center_line.push_back({15.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane4{4};
    lane4.center_line.push_back({0.0_m, 5.0_m, 0.0_m});

    environment::map::Lane lane5{5};
    lane5.center_line.push_back({5.0_m, 5.0_m, 0.0_m});

    environment::map::Lane lane6{6};
    lane6.center_line.push_back({10.0_m, 5.0_m, 0.0_m});

    environment::map::Lane lane7{7};
    lane7.center_line.push_back({15.0_m, 5.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane1);
    graph.AddNode(&lane2);
    graph.AddNode(&lane3);
    graph.AddNode(&lane4);
    graph.AddNode(&lane5);
    graph.AddNode(&lane6);
    graph.AddNode(&lane7);

    graph.AddEdge(&lane0, &lane1, 5, EdgeType::kFollow);
    graph.AddEdge(&lane1, &lane2, 1, EdgeType::kFollow);
    graph.AddEdge(&lane2, &lane3, 5, EdgeType::kFollow);

    graph.AddEdge(&lane4, &lane5, 1, EdgeType::kFollow);
    graph.AddEdge(&lane5, &lane6, 5, EdgeType::kFollow);
    graph.AddEdge(&lane6, &lane7, 1, EdgeType::kFollow);

    graph.AddEdge(&lane0, &lane4, 1, EdgeType::kFollow);
    graph.AddEdge(&lane1, &lane5, 1, EdgeType::kFollow);
    graph.AddEdge(&lane2, &lane6, 1, EdgeType::kFollow);
    graph.AddEdge(&lane3, &lane7, 1, EdgeType::kFollow);

    graph.AddEdge(&lane4, &lane0, 1, EdgeType::kFollow);
    graph.AddEdge(&lane5, &lane1, 1, EdgeType::kFollow);
    graph.AddEdge(&lane6, &lane2, 1, EdgeType::kFollow);
    graph.AddEdge(&lane7, &lane3, 1, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane3);

    Path path = *a_star.ShortestPath({from_node, to_node});

    EXPECT_EQ(8, path.size());
    EXPECT_EQ(path.at(0)->lane->id, from_node->lane->id);
    EXPECT_EQ(path.at(1)->lane->id, 4);
    EXPECT_EQ(path.at(2)->lane->id, 5);
    EXPECT_EQ(path.at(3)->lane->id, 1);
    EXPECT_EQ(path.at(4)->lane->id, 2);
    EXPECT_EQ(path.at(5)->lane->id, 6);
    EXPECT_EQ(path.at(6)->lane->id, 7);
    EXPECT_EQ(path.at(7)->lane->id, to_node->lane->id);
    EXPECT_FALSE(path.IsClosedPath());
}

TEST(AStarAlgorithmTest, GivenLaneGraphWithNonConnectedLanes_WhenShortestPathFromLaneZeroToLaneTwo_ThenNoPathReturned)
{
    Info("LaneGraphWithNonConnectedLanes_FromLane0ToLane2_NoPathReturned");

    // No connection to lane 2
    //
    // Lane Graph:      Path:
    //         L2
    //
    //            ==>   no connection
    //
    // L0 ---- L1
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({10.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({10.0_m, 10.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane1);
    graph.AddNode(&lane2);
    graph.AddEdge(&lane0, &lane1, 10, EdgeType::kFollow);

    AStarAlgorithm a_star;

    SharedNode from_node = graph.GetNode(&lane0);
    SharedNode to_node = graph.GetNode(&lane2);

    EXPECT_FALSE(a_star.ShortestPath({from_node, to_node}));
}

TEST(AStarAlgorithmTest,
     GivenLaneGraphWithFourNodesConnectedInAnRectangle_WhenPathOverallCornersRequested_ThenPathIsClosed)
{
    // Find a closed path (L0 is start and end)
    //
    // Lane Graph:       Path:
    // L3 --5-- L2         L3  --5-- 2
    //  |       |          |         |
    //  5       5    ==>   5         5
    //  |       |          |         |
    // L0 --5-- L1        L0/L0--5-- L1
    //
    environment::map::Lane lane0{0};
    lane0.center_line.push_back({0.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane1{1};
    lane1.center_line.push_back({5.0_m, 0.0_m, 0.0_m});

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({5.0_m, 5.0_m, 0.0_m});

    environment::map::Lane lane3{3};
    lane3.center_line.push_back({0.0_m, 5.0_m, 0.0_m});

    LaneGraph graph;
    graph.AddNode(&lane0);
    graph.AddNode(&lane2);
    graph.AddNode(&lane1);
    graph.AddNode(&lane3);

    graph.AddEdge(&lane0, &lane1, 5, EdgeType::kFollow);
    graph.AddEdge(&lane1, &lane2, 5, EdgeType::kFollow);

    graph.AddEdge(&lane2, &lane3, 5, EdgeType::kFollow);
    graph.AddEdge(&lane3, &lane0, 5, EdgeType::kFollow);

    ASSERT_EQ(4, graph.nodes.size());
    ASSERT_EQ(4, graph.edges.size());

    SharedNodes nodes = {graph.GetNode(&lane0), graph.GetNode(&lane3), graph.GetNode(&lane0)};

    AStarAlgorithm a_star;
    Path path = *a_star.ShortestPath(nodes);

    EXPECT_EQ(5, path.size());
    EXPECT_EQ(path.at(0)->lane->id, 0);
    EXPECT_EQ(path.at(1)->lane->id, 1);
    EXPECT_EQ(path.at(2)->lane->id, 2);
    EXPECT_EQ(path.at(3)->lane->id, 3);
    EXPECT_EQ(path.at(4)->lane->id, 0);
    EXPECT_TRUE(path.IsClosedPath());
}

}  // namespace astas::environment::path_finding
