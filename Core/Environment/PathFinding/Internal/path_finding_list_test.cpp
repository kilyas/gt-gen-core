/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/Internal/path_finding_list.h"

#include "Core/Environment/Map/AstasMap/lane.h"

#include <gtest/gtest.h>

namespace astas::environment::path_finding
{
using units::literals::operator""_m;

TEST(PathFindingListTest, GivenEmptyList_WhenPushingUnorderedElements_ThenTopAndPopAlwaysTheSmallestElement)
{
    PathFindingList path_finding_list;

    auto record1 = std::make_shared<Record>();
    auto record2 = std::make_shared<Record>();
    auto record3 = std::make_shared<Record>();
    auto record4 = std::make_shared<Record>();
    auto record5 = std::make_shared<Record>();
    auto record6 = std::make_shared<Record>();
    auto record7 = std::make_shared<Record>();
    auto record8 = std::make_shared<Record>();

    record1->cost_so_far = 1;
    record2->cost_so_far = 2;
    record3->cost_so_far = 3;
    record4->cost_so_far = 4;
    record5->cost_so_far = 5;
    record6->cost_so_far = 6;
    record7->cost_so_far = 7;
    record8->cost_so_far = 8;

    path_finding_list.push(record3);
    path_finding_list.push(record8);
    path_finding_list.push(record7);
    path_finding_list.push(record2);
    path_finding_list.push(record5);
    path_finding_list.push(record4);
    path_finding_list.push(record6);
    path_finding_list.push(record1);

    EXPECT_EQ(8, path_finding_list.size());

    // Remove one after another element to check their order
    EXPECT_EQ(record1, path_finding_list.TopAndPop());
    EXPECT_EQ(7, path_finding_list.size());
    EXPECT_EQ(record2, path_finding_list.TopAndPop());
    EXPECT_EQ(6, path_finding_list.size());
    EXPECT_EQ(record3, path_finding_list.TopAndPop());
    EXPECT_EQ(5, path_finding_list.size());
    EXPECT_EQ(record4, path_finding_list.TopAndPop());
    EXPECT_EQ(4, path_finding_list.size());
    EXPECT_EQ(record5, path_finding_list.TopAndPop());
    EXPECT_EQ(3, path_finding_list.size());
    EXPECT_EQ(record6, path_finding_list.TopAndPop());
    EXPECT_EQ(2, path_finding_list.size());
    EXPECT_EQ(record7, path_finding_list.TopAndPop());
    EXPECT_EQ(1, path_finding_list.size());
    EXPECT_EQ(record8, path_finding_list.TopAndPop());
    EXPECT_EQ(0, path_finding_list.size());
}

TEST(PathFindingListTest, GivenEmptyList_WhenPushRecored_ThenRecordIsContained)
{
    environment::map::Lane lane1{1};
    lane1.center_line.push_back({0.0_m, 0.0_m, 0.0_m});
    SharedNode node1 = std::make_shared<Node>(&lane1);
    RecordPtr record1 = std::make_shared<Record>();
    record1->node = node1;

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({10.0_m, 0.0_m, 0.0_m});
    SharedNode node2 = std::make_shared<Node>(&lane2);

    PathFindingList path_finding_list;
    EXPECT_FALSE(path_finding_list.Contains(node1));
    EXPECT_FALSE(path_finding_list.Contains(node2));

    path_finding_list.push(record1);

    EXPECT_TRUE(path_finding_list.Contains(node1));
    EXPECT_FALSE(path_finding_list.Contains(node2));
}

TEST(PathFindingListTest, GivenListWithElements_WhenGetIfContained_ThenContainedObjectIsReturnedOrNullPtr)
{
    environment::map::Lane lane1{1};
    lane1.center_line.push_back({0.0_m, 0.0_m, 0.0_m});
    SharedNode node1 = std::make_shared<Node>(&lane1);
    RecordPtr record1 = std::make_shared<Record>();
    record1->node = node1;

    environment::map::Lane lane2{2};
    lane2.center_line.push_back({0.0_m, 0.0_m, 0.0_m});
    SharedNode node2 = std::make_shared<Node>(&lane2);
    RecordPtr record2 = std::make_shared<Record>();
    record2->node = node2;

    PathFindingList path_finding_list;
    EXPECT_EQ(nullptr, path_finding_list.GetIfContained(node1));
    EXPECT_EQ(nullptr, path_finding_list.GetIfContained(node2));

    path_finding_list.push(record1);
    EXPECT_EQ(record1, path_finding_list.GetIfContained(node1));
    EXPECT_EQ(nullptr, path_finding_list.GetIfContained(node2));

    path_finding_list.push(record2);
    EXPECT_EQ(record1, path_finding_list.GetIfContained(node1));
    EXPECT_EQ(record2, path_finding_list.GetIfContained(node2));
}

}  // namespace astas::environment::path_finding
