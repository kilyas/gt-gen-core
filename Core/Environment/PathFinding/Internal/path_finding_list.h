/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_PATHFINDINGLIST_H
#define GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_PATHFINDINGLIST_H

#include "Core/Environment/PathFinding/Internal/lane_graph_types.h"

#include <queue>

namespace astas::environment::path_finding
{
/// @brief A-Star record data structure. Needed to indicate where in the graph traversal the algorithm currently is
struct Record
{
    /// @brief The node representing a lane
    WeakNode node;

    /// @brief The next record to process
    std::weak_ptr<Record> from_node_record;
    /// @brief This is where we come from
    std::weak_ptr<Record> to_node_record;

    ///  @brief The costs so far to get to this node
    double cost_so_far{0.0};
    /// @brief The estimated cost to the target (uses the eucleadian distance as heuristic)
    double estimated_total_cost{0.0};
    /// @brief Either follows a lane or changes a lane
    EdgeType edge_type{EdgeType::kFollow};
};
using RecordPtr = std::shared_ptr<Record>;

/// @brief Compare function in order to use this struct in the priority_queue
inline bool operator>(const RecordPtr& left, const RecordPtr& right)
{
    return left->cost_so_far > right->cost_so_far;
}

/// @brief Priority queue with find access. Provides access to the node with the smallest cost so far
class PathFindingList : public std::priority_queue<RecordPtr, std::vector<RecordPtr>, std::greater<RecordPtr>>
{
  public:
    /// @brief Returns the smallest element and pops it form the priority_queue
    RecordPtr TopAndPop();
    /// @brief Returns true if a given node is contained in the priority list, false otherwise
    bool Contains(const SharedNode& node) const;
    /// Returns the element for the given node if it is contained, nullptr otherwise
    RecordPtr GetIfContained(const SharedNode& node) const;

  private:
    /// @brief Accesses the underlying container to search for the given node and returns the iterator to it
    container_type::const_iterator FindNode(const SharedNode& node) const;
};

}  // namespace astas::environment::path_finding

#endif  // GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_PATHFINDINGLIST_H
