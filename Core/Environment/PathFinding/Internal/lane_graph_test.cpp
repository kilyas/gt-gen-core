/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/Internal/lane_graph.h"

#include "Core/Environment/Map/AstasMap/lane.h"

#include <gtest/gtest.h>

namespace astas::environment::path_finding
{
using units::literals::operator""_m;

class LaneGraphTest : public testing::Test
{
  protected:
    LaneGraphTest()
    {
        lane1_.center_line.push_back({0.0_m, 0.0_m, 0.0_m});
        lane2_.center_line.push_back({1.0_m, 0.0_m, 0.0_m});
        lane3_.center_line.push_back({3.0_m, 0.0_m, 0.0_m});
    }
    environment::map::Lane lane1_{1};
    environment::map::Lane lane2_{2};
    environment::map::Lane lane3_{3};
};

TEST_F(LaneGraphTest, GivenLaneGraph_WhenConstructed_ThenSizeOfNodesIsZero)
{
    LaneGraph lane_graph;

    EXPECT_EQ(0, lane_graph.nodes.size());
}

TEST_F(LaneGraphTest, GivenLaneGraph_WhenConstructed_ThenSizeOfEdgesIsZero)
{
    LaneGraph lane_graph;

    EXPECT_EQ(0, lane_graph.edges.size());
}

TEST_F(LaneGraphTest, GivenEmptyLaneGraph_WhenAddOneNode_ThenSizeOfNodesIsOne)
{
    LaneGraph lane_graph;
    ASSERT_EQ(0, lane_graph.nodes.size());

    lane_graph.AddNode(&lane1_);

    EXPECT_EQ(1, lane_graph.nodes.size());
}

TEST_F(LaneGraphTest, GivenLaneGraphContainsOneNode_WhenAddOneNode_ThenSizeOfNodesIsTwo)
{
    LaneGraph lane_graph;
    lane_graph.AddNode(&lane1_);
    ASSERT_EQ(1, lane_graph.nodes.size());

    lane_graph.AddNode(&lane2_);

    EXPECT_EQ(2, lane_graph.nodes.size());
}

TEST_F(LaneGraphTest, GivenLaneGraphContainsOneNode_WhenAddRedundantNode_ThenNodeIsNotAdded)
{
    LaneGraph lane_graph;
    lane_graph.AddNode(&lane1_);
    ASSERT_EQ(1, lane_graph.nodes.size());

    lane_graph.AddNode(&lane1_);

    EXPECT_EQ(1, lane_graph.nodes.size());
}

TEST_F(LaneGraphTest, GivenEmptyLaneGraph_WhenGetNode_ThenNodeReturnedIsNull)
{
    LaneGraph lane_graph;

    auto node_ptr = lane_graph.GetNode(&lane1_);

    EXPECT_EQ(nullptr, node_ptr);
}

TEST_F(LaneGraphTest, GivenLaneGraphContainsNodeOne_WhenGetNodeOne_ThenNodeOneIsReturned)
{
    LaneGraph lane_graph;
    lane_graph.AddNode(&lane1_);

    auto node_ptr = lane_graph.GetNode(&lane1_);

    ASSERT_NE(nullptr, node_ptr);
    EXPECT_EQ(lane1_.id, node_ptr->lane->id);
}

TEST_F(LaneGraphTest, GivenLaneGraphContainsTwoNodes_WhenGetNode_ThenEachNodeIsReturned)
{
    LaneGraph lane_graph;
    lane_graph.AddNode(&lane1_);
    lane_graph.AddNode(&lane2_);
    ASSERT_EQ(2, lane_graph.nodes.size());

    auto node1_ptr = lane_graph.GetNode(&lane1_);
    auto node2_ptr = lane_graph.GetNode(&lane2_);

    ASSERT_NE(nullptr, node1_ptr);
    ASSERT_NE(nullptr, node2_ptr);
    EXPECT_EQ(lane1_.id, node1_ptr->lane->id);
    EXPECT_EQ(lane2_.id, node2_ptr->lane->id);
}

TEST_F(LaneGraphTest, GivenLaneGraphContainsTwoNodes_WhenAddEdge_ThenSizeOfEdgesIsOne)
{
    LaneGraph lane_graph;
    lane_graph.AddNode(&lane1_);
    lane_graph.AddNode(&lane2_);
    ASSERT_EQ(0, lane_graph.edges.size());

    lane_graph.AddEdge(&lane1_, &lane2_, 1, EdgeType::kFollow);

    EXPECT_EQ(1, lane_graph.edges.size());
}

TEST_F(LaneGraphTest, GivenLaneGraphContainsTwoNodesWithOneEdge_WhenAddRedundantEdge_ThenSizeOfEdgesIsOne)
{
    LaneGraph lane_graph;
    lane_graph.AddNode(&lane1_);
    lane_graph.AddNode(&lane2_);
    lane_graph.AddEdge(&lane1_, &lane2_, 1, EdgeType::kFollow);
    ASSERT_EQ(1, lane_graph.edges.size());

    lane_graph.AddEdge(&lane1_, &lane2_, 1, EdgeType::kFollow);

    EXPECT_EQ(1, lane_graph.edges.size());
}

}  // namespace astas::environment::path_finding
