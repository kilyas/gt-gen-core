/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_LANEGRAPH_H
#define GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_LANEGRAPH_H

#include "Core/Environment/PathFinding/Internal/lane_graph_types.h"

namespace astas::environment::path_finding
{
//////////////////////////////////////
/// @brief LaneGraph manages nodes and edges. A node represents a lane, an edge represents the connection of two lanes
class LaneGraph
{
  public:
    /// @brief List of nodes
    SharedNodes nodes{};
    /// @brief List of edges
    SharedEdges edges{};

    /// @brief Adds a node to the graph, which represents the given lane
    void AddNode(const environment::map::Lane* lane);
    /// @brief Adds a edge to the graph, which represents the connection of two given lanes
    void AddEdge(const environment::map::Lane* from_lane,
                 const environment::map::Lane* to_lane,
                 double weight,
                 EdgeType type);

    /// @brief Returns the node for a given lane if it is contained in the graph, nullptr otherwise
    SharedNode GetNode(const environment::map::Lane* lane) const;

  private:
    /// @brief Returns true if a Node exists in the graph for the given lane, false otherwise
    bool ContainsNode(const environment::map::Lane* lane) const;
    /// @brief Returns true if a Edge exists in the graph for the given lanes, false otherwise
    bool ContainsEdge(const SharedNode& from_node, const SharedNode& to_node, double weight, EdgeType type) const;

    double ComputeEdgeWeight(double weight, EdgeType type) const;
};

}  // namespace astas::environment::path_finding

#endif  // GTGEN_CORE_ENVIRONMENT_PATHFINDING_INTERNAL_LANEGRAPH_H
