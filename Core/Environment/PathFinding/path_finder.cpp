/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/path_finder.h"

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Environment/Map/Common/exceptions.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Environment/PathFinding/Internal/a_star_algorithm.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/MantleApiExtension/formatting.h"

namespace astas::environment::path_finding
{

PathFinder::PathFinder(const environment::map::AstasMap& map) : map_{map}
{
    GenerateGraph();
}

void PathFinder::GenerateGraph()
{
    for (const auto& lane : map_.GetLanes())
    {
        CreateNodeIfDrivingLane(&lane);
        CreateEdgesForDrivingLane(&lane);
    }
}

bool AllWaypointsPlacedOnRoad(const environment::map::LaneLocationProvider& lane_location_provider,
                              const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints)
{
    if (waypoints.size() < 2)
    {
        Error("Routes require at least 2 waypoints!");
        return false;
    }

    std::size_t error{0};
    for (std::size_t i = 0; i < waypoints.size(); ++i)
    {
        const auto lane_location = lane_location_provider.GetLaneLocation(waypoints[i]);
        if (!lane_location.IsValid())
        {
            Error("Waypoint number {} is not on the road.\nWorld-Coordinate: {}\nMap-Coordinate: {}",
                  i,
                  waypoints[i],
                  environment::map::GetMapCoordinateString(
                      waypoints[i], lane_location_provider.GetAstasMap().coordinate_converter.get()));
            error++;
        }
    }
    return error == 0;
}

std::optional<Path> PathFinder::ComputeRoute(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints) const
{
    environment::map::LaneLocationProvider lane_location_provider{map_};

    if (!AllWaypointsPlacedOnRoad(lane_location_provider, waypoints))
    {
        return std::nullopt;
    }

    auto nodes = WayPointsToNodes(lane_location_provider, waypoints);
    AStarAlgorithm a_star_algorithm;
    auto path = a_star_algorithm.ShortestPath(nodes);
    if (!path)
    {
        std::string waypoint_to_lane_list{};
        for (std::uint64_t i = 0; i < nodes.size(); ++i)
        {
            waypoint_to_lane_list += "\n#" + std::to_string(i) + "\t" + std::to_string(nodes[i]->lane->id);
        }
        Error("These are the lanes corresponding to the route:\nWaypoint\tLane{}", waypoint_to_lane_list);

        return std::nullopt;
    }

    return path;
}

mantle_api::Vec3<units::length::meter_t> PathFinder::GetCenterOfLane(const environment::map::Lane* lane) const
{
    return lane->center_line[lane->center_line.size() / 2];
}

double PathFinder::DistanceBetweenLanes(const environment::map::Lane* left, const environment::map::Lane* right) const
{
    const auto left_center = GetCenterOfLane(left);
    const auto right_center = GetCenterOfLane(right);
    return service::glmwrapper::Distance(right_center, left_center);
}

void PathFinder::CreateNodeIfDrivingLane(const environment::map::Lane* lane)
{
    if (IsLaneFeasibleForRouting(lane))
    {
        lane_graph_.AddNode(lane);
    }
}

void PathFinder::CreateEdgesForDrivingLane(const environment::map::Lane* from_lane)
{
    ASSERT(from_lane != nullptr &&
           "Contact GTGen support: Cannot add nullptr edge. Something is messed up with the map structure.")

    if (!IsLaneFeasibleForRouting(from_lane))
    {
        return;
    }

    // Note: Do not consider the predecessors since the lanes have a driving direction

    for (const auto& successor : from_lane->successors)
    {
        CreateEdge(from_lane, successor, EdgeType::kFollow);
    }

    CreateEdgesForNeighbouredLanes(from_lane);
}

void PathFinder::CreateEdgesForNeighbouredLanes(const environment::map::Lane* from_lane)
{
    for (const auto& left_adjacent_lane : from_lane->left_adjacent_lanes)
    {
        CreateEdge(from_lane, left_adjacent_lane, EdgeType::kChange);
    }

    for (const auto& right_adjacent_lane : from_lane->right_adjacent_lanes)
    {
        CreateEdge(from_lane, right_adjacent_lane, EdgeType::kChange);
    }
}

void PathFinder::CreateEdge(const environment::map::Lane* from_lane,
                            const mantle_api::UniqueId to_lane_id,
                            EdgeType edge_type)
{
    const environment::map::Lane* to_lane = map_.FindLane(to_lane_id);

    // For NDS it's possible that a lane references another lane, but this lane is not part of the loaded map,
    // therefore it could be that we get a nullptr. Do not ASSERT, instead just skip the edge for a nullptr to_lane
    if (IsLaneFeasibleForRouting(to_lane))
    {
        lane_graph_.AddNode(to_lane);
        lane_graph_.AddEdge(from_lane, to_lane, DistanceBetweenLanes(from_lane, to_lane), edge_type);
    }
}

SharedNode PathFinder::WayPointToNode(const environment::map::LaneLocationProvider& lane_location_provider,
                                      const mantle_api::Vec3<units::length::meter_t>& waypoint) const
{
    auto lane = lane_location_provider.FindBestLaneCandidate(waypoint);
    if (lane == nullptr)
    {
        LogAndThrow(environment::map::exception::LaneNotFound("Could not find any lane for waypoint {}", waypoint));
    }
    if (lane->flags.IsShoulderLane() || lane->flags.IsParking() || lane->flags.IsBicycle())
    {
        LogAndThrow(
            environment::map::exception::MapException("Waypoint {} is located on a shoulder, parking or bicycle "
                                                      "lane. Only drivable lanes are supported.",
                                                      waypoint));
    }

    SharedNode node = lane_graph_.GetNode(lane);
    ASSERT(node != nullptr && "Lane exists in the map, but is not contained in the lane graph - contact GTGen support")
    return node;
}

SharedNodes PathFinder::WayPointsToNodes(const environment::map::LaneLocationProvider& lane_location_provider,
                                         const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints) const
{
    std::vector<SharedNode> nodes;
    for (const auto& waypoint : waypoints)
    {
        SharedNode node = WayPointToNode(lane_location_provider, waypoint);
        nodes.push_back(node);
    }

    return nodes;
}
bool PathFinder::IsLaneFeasibleForRouting(const environment::map::Lane* lane) const
{
    return lane != nullptr && lane->flags.IsDrivable() && !lane->flags.IsShoulderLane();
}

}  // namespace astas::environment::path_finding
