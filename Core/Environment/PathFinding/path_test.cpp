/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/path.h"

#include "Core/Environment/Exception/exception.h"

#include <gtest/gtest.h>

#include <memory>

namespace astas::environment::path_finding
{
TEST(PathTest, GivenPath_WhenEnableClosedPath_ThenIsClosedPath)
{
    Path path;
    ASSERT_FALSE(path.IsClosedPath());

    path.EnableClosedPath();

    EXPECT_TRUE(path.IsClosedPath());
}

TEST(PathTest, GivenEmptyPath_WhenGetCurrentLane_ThenNull)
{
    Path path;

    const environment::map::Lane* current_lane = path.GetCurrentLane();

    EXPECT_EQ(current_lane, nullptr);
}

TEST(PathTest, GivenOneLaneInPath_WhenGetCurrentLane_ThenReturnThisLane)
{
    Path path;
    map::Lane lane{0};
    path.push_back(std::make_shared<PathEntry>(&lane, EdgeType::kFollow));

    const environment::map::Lane* current_lane = path.GetCurrentLane();

    EXPECT_EQ(current_lane, &lane);
}

TEST(PathTest, GivenTwoLanesInPath_WhenGetCurrentLane_ThenReturnFrontLane)
{
    Path path;
    map::Lane lane0{0};
    map::Lane lane1{1};
    path.push_back(std::make_shared<PathEntry>(&lane0, EdgeType::kFollow));
    path.push_back(std::make_shared<PathEntry>(&lane1, EdgeType::kFollow));

    const environment::map::Lane* current_lane = path.GetCurrentLane();

    EXPECT_EQ(current_lane, &lane0);
    EXPECT_NE(current_lane, &lane1);
}

TEST(PathTest, GivenEmptyPath_WhenPopCurrentLane_ThenThrowException)
{
    Path path;

    EXPECT_THROW(path.PopCurrentLane(), environment::EnvironmentException);
}

TEST(PathTest, GivenOneLaneInPath_WhenPopCurrentLane_ThenPathIsEmpty)
{
    Path path;
    map::Lane lane{0};
    path.push_back(std::make_shared<PathEntry>(&lane, EdgeType::kFollow));

    path.PopCurrentLane();

    EXPECT_TRUE(path.empty());
}

TEST(PathTest, GivenTwoLanesInPath_WhenPopCurrentLane_ThenPathContainsOnlyOtherLane)
{
    Path path;
    map::Lane lane0{0};
    map::Lane lane1{1};
    path.push_back(std::make_shared<PathEntry>(&lane0, EdgeType::kFollow));
    path.push_back(std::make_shared<PathEntry>(&lane1, EdgeType::kFollow));

    path.PopCurrentLane();
    const environment::map::Lane* current_lane = path.GetCurrentLane();

    EXPECT_EQ(current_lane, &lane1);
}

TEST(PathTest, GivenThreeLanesInClosedPath_WhenPopCurrentLane_ThenPoppedLaneIsAppendedToPath)
{
    Path path;
    path.EnableClosedPath();
    map::Lane lane0{0};
    map::Lane lane1{1};
    map::Lane lane2{2};
    path.push_back(std::make_shared<PathEntry>(&lane0, EdgeType::kFollow));
    path.push_back(std::make_shared<PathEntry>(&lane1, EdgeType::kFollow));
    path.push_back(std::make_shared<PathEntry>(&lane2, EdgeType::kFollow));

    path.PopCurrentLane();

    ASSERT_EQ(path.size(), 3);
    EXPECT_EQ(path.back()->lane, &lane0);
}

TEST(PathTest, GivenFirstAndLastLaneAreEqualInClosedPath_WhenPopCurrentLane_ThenPoppedLaneIsNotAppendedToPath)
{
    Path path;
    path.EnableClosedPath();
    map::Lane lane0{0};
    map::Lane lane1{1};
    path.push_back(std::make_shared<PathEntry>(&lane0, EdgeType::kFollow));
    path.push_back(std::make_shared<PathEntry>(&lane1, EdgeType::kFollow));
    path.push_back(std::make_shared<PathEntry>(&lane0, EdgeType::kFollow));

    path.PopCurrentLane();

    EXPECT_EQ(path.size(), 2);
}

TEST(PathTest, GivenOneLaneInClosedPath_WhenPopCurrentLane_ThenLaneIsNotPopped)
{
    Path path;
    path.EnableClosedPath();
    map::Lane lane0{0};
    path.push_back(std::make_shared<PathEntry>(&lane0, EdgeType::kFollow));

    path.PopCurrentLane();

    EXPECT_EQ(path.size(), 1);
}

}  // namespace astas::environment::path_finding
