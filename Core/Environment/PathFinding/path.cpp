/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/PathFinding/path.h"

#include "Core/Environment/Exception/exception.h"

namespace astas::environment::path_finding
{

void Path::EnableClosedPath()
{
    is_closed_path_ = true;
}

bool Path::IsClosedPath() const
{
    return is_closed_path_;
}

const environment::map::Lane* Path::GetCurrentLane() const
{
    if (empty())
    {
        return nullptr;
    }
    return front()->lane;
}

void Path::PopCurrentLane()
{
    if (empty())
    {
        throw environment::EnvironmentException("Pop on empty Path not supported.");
    }

    auto current_path_node = front();

    if (size() > 1 || !is_closed_path_)
    {
        pop_front();
    }

    if (is_closed_path_ && IsLastPathLaneNotEqualTo(current_path_node->lane))
    {
        push_back(current_path_node);
    }
}

bool Path::IsLastPathLaneNotEqualTo(const environment::map::Lane* lane) const
{
    return lane->id != back()->lane->id;
}

}  // namespace astas::environment::path_finding
