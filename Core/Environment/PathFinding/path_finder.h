/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_PATHFINDING_PATHFINDER_H
#define GTGEN_CORE_ENVIRONMENT_PATHFINDING_PATHFINDER_H

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Environment/PathFinding/Internal/lane_graph.h"
#include "Core/Environment/PathFinding/path.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <optional>

namespace astas::environment::path_finding
{

/// @brief Translates the GTGen map to a lane graph, where the a-star routing algorithm is executed on
class PathFinder
{
  public:
    /// @brief Constructs the path finder
    /// @param map to be transferred to a lane graph
    explicit PathFinder(const environment::map::AstasMap& map);

    /// @brief Returns the shortest path for a variable number of waypoints (at least 2)
    /// @param waypoints The world positions to find a path between
    /// @return Path represented as deque of nodes, if a valid path has been found
    std::optional<Path> ComputeRoute(const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints) const;

    /// @brief Returns the constructed lane graph - @TODO: currently only used for testing, check if needed
    const LaneGraph& GetLaneGraph() const { return lane_graph_; }

  private:
    void GenerateGraph();

    bool IsLaneFeasibleForRouting(const environment::map::Lane* lane) const;

    mantle_api::Vec3<units::length::meter_t> GetCenterOfLane(const environment::map::Lane* lane) const;
    double DistanceBetweenLanes(const environment::map::Lane* left, const environment::map::Lane* right) const;

    void CreateNodeIfDrivingLane(const environment::map::Lane* lane);
    void CreateEdgesForDrivingLane(const environment::map::Lane* from_lane);
    void CreateEdgesForNeighbouredLanes(const environment::map::Lane* from_lane);
    void CreateEdge(const environment::map::Lane* from_lane, mantle_api::UniqueId to_lane_id, EdgeType edge_type);

    SharedNode WayPointToNode(const environment::map::LaneLocationProvider& lane_location_provider,
                              const mantle_api::Vec3<units::length::meter_t>& waypoint) const;
    SharedNodes WayPointsToNodes(const environment::map::LaneLocationProvider& lane_location_provider,
                                 const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints) const;

  protected:
    const environment::map::AstasMap& map_;
    LaneGraph lane_graph_;
};

}  // namespace astas::environment::path_finding

#endif  // GTGEN_CORE_ENVIRONMENT_PATHFINDING_PATHFINDER_H
