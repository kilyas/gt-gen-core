/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Host/host_vehicle_interface.h"

namespace astas::environment::host
{

void HostVehicleInterface::SetHostPoseRecoveredInLastStep(bool recovered)
{
    recovered_ = recovered;
}

bool HostVehicleInterface::HostPoseRecoveredInLastStep() const
{
    return recovered_;
}

}  // namespace astas::environment::host
