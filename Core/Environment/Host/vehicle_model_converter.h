/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELCONVERTER_H

#include "Core/Environment/Host/vehicle_model_in.h"
#include "Core/Environment/Host/vehicle_model_out.h"
#include "astas_osi_trafficupdate.pb.h"

namespace astas::environment::host
{

VehicleModelOut ConvertProtoToVehicleModelOut(const astas_osi3::TrafficUpdate& proto_traffic_update);

}  // namespace astas::environment::host

#endif  // GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELCONVERTER_H
