/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELOUT_H
#define GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELOUT_H

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Traffic/entity_properties.h>

namespace astas::environment::host
{

struct VehicleModelOut
{
    mantle_api::Vec3<units::length::meter_t> position{};
    mantle_api::Orientation3<units::angle::radian_t> orientation{};
    mantle_api::Vec3<units::velocity::meters_per_second_t> velocity{};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> acceleration{};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> orientation_rate{};

    mantle_api::IndicatorState indicator_state{mantle_api::IndicatorState::kOff};
    mantle_api::ExternalControlState had_control_state{mantle_api::ExternalControlState::kOff};

    mantle_api::Time time_stamp{-1};
};

}  // namespace astas::environment::host

#endif  // GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELOUT_H
