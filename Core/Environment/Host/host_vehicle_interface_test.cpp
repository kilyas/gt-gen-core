/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Host/host_vehicle_interface.h"

#include <gtest/gtest.h>

namespace astas::environment::host
{

TEST(HostVehicleInterfaceTest, GivenInterface_WhenConstructed_ThenHostIsNotRecovered)
{
    HostVehicleInterface interface;
    EXPECT_FALSE(interface.HostPoseRecoveredInLastStep());
}

TEST(HostVehicleInterfaceTest, GivenInterface_WhenHostRecovered_ThenFlagIsSet)
{
    HostVehicleInterface interface;
    interface.SetHostPoseRecoveredInLastStep(true);
    EXPECT_TRUE(interface.HostPoseRecoveredInLastStep());
}

TEST(HostVehicleInterfaceTest, GivenInterface_WhenHostNotRecovered_ThenFlagIsNotSet)
{
    HostVehicleInterface interface;
    interface.SetHostPoseRecoveredInLastStep(false);
    EXPECT_FALSE(interface.HostPoseRecoveredInLastStep());
}

TEST(HostVehicleInterfaceTest, GivenInterface_WhenSettingHostRecovered_ThenRecoveryFlagIsSet)
{
    HostVehicleInterface interface;
    ASSERT_FALSE(interface.HostPoseRecoveredInLastStep());

    interface.SetHostPoseRecoveredInLastStep(true);
    EXPECT_TRUE(interface.HostPoseRecoveredInLastStep());

    interface.SetHostPoseRecoveredInLastStep(false);
    EXPECT_FALSE(interface.HostPoseRecoveredInLastStep());
}

}  // namespace astas::environment::host
