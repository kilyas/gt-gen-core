/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELIN_H
#define GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELIN_H

#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::host
{

struct WheelStates
{
    double front_right_mue{1.0};
    double front_left_mue{1.0};
    double rear_right_mue{1.0};
    double rear_left_mue{1.0};
};

struct VehicleModelIn
{
    mantle_api::Time time_stamp{};
    WheelStates wheel_states{};
};

}  // namespace astas::environment::host

#endif  // GTGEN_CORE_ENVIRONMENT_HOST_VEHICLEMODELIN_H
