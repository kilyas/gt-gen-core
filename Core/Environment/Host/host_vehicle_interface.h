/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHICLEINTERFACE_H
#define GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHICLEINTERFACE_H

#include "Core/Export/i_host_vehicle_interface.h"

#include <MantleAPI/Traffic/i_entity.h>

#include <cstdint>
#include <vector>

namespace astas::environment::host
{

/// @brief Provides access to host vehicle related data
class HostVehicleInterface : public IHostVehicleInterface
{
  public:
    void SetHostPoseRecoveredInLastStep(bool recovered);

    /// @brief Whether the host vehicle left the road and has been recovered to the last valid drivable lane
    /// in the last completed Step
    bool HostPoseRecoveredInLastStep() const override;

  private:
    bool recovered_{false};
};

}  // namespace astas::environment::host

#endif  // GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHICLEINTERFACE_H
