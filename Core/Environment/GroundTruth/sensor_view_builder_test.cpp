/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/sensor_view_builder.h"

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"
#include "Core/Tests/TestUtils/exception_testing.h"

#include <google/protobuf/util/message_differencer.h>
#include <gtest/gtest.h>
#include <units.h>

namespace astas::environment::proto_groundtruth
{
class SensorViewBuilderTest : public testing::Test
{
  protected:
    void SetUp() override
    {
        sensor_view_builder_.Init();  // needed for the chunking  to have the correct map
    }

    std::vector<std::unique_ptr<mantle_api::IEntity>> CreateHostEntityOnly() const
    {
        std::vector<std::unique_ptr<mantle_api::IEntity>> entities;
        entities.push_back(std::make_unique<environment::entities::VehicleEntity>(0, "host"));
        entities.back()->SetProperties(std::move(CreateHostVehicleProperties()));
        return entities;
    }

    std::vector<std::unique_ptr<mantle_api::IEntity>> CreateTwoVehiclesAndTwoPedestrians() const
    {
        std::vector<std::unique_ptr<mantle_api::IEntity>> entities;
        entities.push_back(std::make_unique<environment::entities::VehicleEntity>(0, "host"));
        entities.back()->SetProperties(std::move(CreateHostVehicleProperties()));
        entities.push_back(std::make_unique<environment::entities::VehicleEntity>(1, "vehicle1"));
        entities.back()->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
        entities.push_back(std::make_unique<environment::entities::PedestrianEntity>(2, "pedestrian2"));
        entities.back()->SetProperties(std::make_unique<mantle_api::PedestrianProperties>());
        entities.push_back(std::make_unique<environment::entities::PedestrianEntity>(3, "pedestrian3"));
        entities.back()->SetProperties(std::make_unique<mantle_api::PedestrianProperties>());
        return entities;
    }

    std::unique_ptr<mantle_api::VehicleProperties> CreateHostVehicleProperties() const
    {
        auto host_vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
        host_vehicle_properties->is_host = true;
        return host_vehicle_properties;
    }

    void CreateLaneGroupWithoutLaneBoundaries(map::AstasMap& astas_map)
    {
        using units::literals::operator""_m;
        map::Lane lane{1};
        lane.center_line = {{-25.0_m, 25.0_m, 0.0_m}, {-175.0_m, 175.0_m, 0.0_m}};

        map::LaneGroup lane_group1{1001, map::LaneGroup::Type::kUnknown};
        astas_map.AddLaneGroup(lane_group1);
        astas_map.AddLane(1001, lane);
    }

    std::unique_ptr<map::AstasMap> astas_map_{
        test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach()};

    SensorViewBuilder sensor_view_builder_{*astas_map_, mantle_api::Time{10}, service::user_settings::MapChunking{}};
};

TEST_F(SensorViewBuilderTest, GivenMapWithTwoLanes_WhenGetSensorViewAfterInitStep_ThenSensorViewHasGlobalGroundTruth)
{
    auto entities = CreateHostEntityOnly();
    sensor_view_builder_.Step(entities, entities.front().get());

    ASSERT_TRUE(sensor_view_builder_.GetSensorView().has_global_ground_truth());
}

TEST_F(SensorViewBuilderTest,
       GivenMapWithTwoLanes_WhenGetSensorViewAfterInitStep_ThenVersionInGroundTruthAndSensorViewAreEqual)
{
    auto entities = CreateHostEntityOnly();
    sensor_view_builder_.Step(entities, entities.front().get());

    std::lock_guard<std::mutex> lock(sensor_view_builder_.gt_mutex);
    const astas_osi3::SensorView& sensor_view = sensor_view_builder_.GetSensorView();

    EXPECT_TRUE(google::protobuf::util::MessageDifferencer::Equals(sensor_view.version(),
                                                                   sensor_view.global_ground_truth().version()));
}

TEST_F(SensorViewBuilderTest,
       GivenMapWithTwoLanes_WhenGetSensorViewAfterInitStep_ThenTimestampInGroundTruthAndSensorViewAreEqual)
{
    auto entities = CreateHostEntityOnly();
    sensor_view_builder_.Step(entities, entities.front().get());

    std::lock_guard<std::mutex> lock(sensor_view_builder_.gt_mutex);
    const astas_osi3::SensorView& sensor_view = sensor_view_builder_.GetSensorView();

    EXPECT_TRUE(google::protobuf::util::MessageDifferencer::Equals(sensor_view.timestamp(),
                                                                   sensor_view.global_ground_truth().timestamp()));
}

TEST_F(SensorViewBuilderTest, GivenMapWithTwoLanes_WhenInitStep_ThenTwoLanesInGroundTruth)
{
    auto entities = CreateHostEntityOnly();

    sensor_view_builder_.Step(entities, entities.front().get());

    std::lock_guard<std::mutex> lock(sensor_view_builder_.gt_mutex);

    ASSERT_EQ(2, sensor_view_builder_.GetSensorView().global_ground_truth().lane().size());
}

TEST_F(SensorViewBuilderTest,
       GivenVehiclesAndPedestrians_WhenGroundTruthAfterStep_ThenHostVehicleIdIsSetToTheCenterEntity)
{
    auto entities = CreateTwoVehiclesAndTwoPedestrians();
    sensor_view_builder_.Step(entities, entities.front().get());

    astas_osi3::GroundTruth gt = sensor_view_builder_.GetSensorView().global_ground_truth();

    ASSERT_TRUE(sensor_view_builder_.GetSensorView().global_ground_truth().has_host_vehicle_id());
    EXPECT_EQ(entities.front()->GetUniqueId(),
              sensor_view_builder_.GetSensorView().global_ground_truth().host_vehicle_id().value());
}

TEST_F(SensorViewBuilderTest,
       GivenVehiclesAndPedestrians_WhenGetGroundTruthAfterStep_ThenVehiclesAndPedestriansInGroundTruth)
{
    auto entities = CreateTwoVehiclesAndTwoPedestrians();
    sensor_view_builder_.Step(entities, entities.front().get());

    astas_osi3::GroundTruth gt = sensor_view_builder_.GetSensorView().global_ground_truth();

    const auto& vehicles = test_utils::GetObjectsByType(gt, astas_osi3::MovingObject::TYPE_VEHICLE);
    const auto& pedestrians = test_utils::GetObjectsByType(gt, astas_osi3::MovingObject::TYPE_PEDESTRIAN);
    ASSERT_EQ(2, vehicles.size());
    ASSERT_EQ(2, pedestrians.size());
}

TEST_F(SensorViewBuilderTest, GivenSimulationTime_WhenGetGroundTruthAfterStep_ThenTimeStampSetCorrect)
{
    using units::literals::operator""_ms;

    service::utility::Clock::Instance().SetNow(1500_ms);
    auto entities = CreateHostEntityOnly();
    sensor_view_builder_.Step(entities, entities.front().get());

    astas_osi3::GroundTruth gt = sensor_view_builder_.GetSensorView().global_ground_truth();

    EXPECT_EQ(gt.timestamp().seconds(), 1);
    EXPECT_EQ(gt.timestamp().nanos(), 500'000'000);
    EXPECT_EQ(sensor_view_builder_.GetSensorView().timestamp().seconds(), 1);
    EXPECT_EQ(sensor_view_builder_.GetSensorView().timestamp().nanos(), 500'000'000);
}

TEST_F(SensorViewBuilderTest, GivenDateTimeOneMinPastMidnight_WhenStepWithOneMinuteStepSize_ThenTimeOfDayIsTwoMinutes)
{
    auto entities = CreateHostEntityOnly();
    SensorViewBuilder sensor_view_builder{*astas_map_, mantle_api::Time{60'000}, service::user_settings::MapChunking{}};
    sensor_view_builder.Init();
    sensor_view_builder.SetDateTime(mantle_api::Time(60'000));
    sensor_view_builder.Step(entities, entities.front().get());

    EXPECT_EQ(sensor_view_builder.GetSensorView()
                  .global_ground_truth()
                  .environmental_conditions()
                  .time_of_day()
                  .seconds_since_midnight(),
              120);
}

TEST_F(
    SensorViewBuilderTest,
    GivenTwoVehiclesAndTwoPedestrians_WhenIgnoringVehicleWithIdOneAndGroundTruthAfterStep_ThenVehicleIsNotInGroundTruth)
{
    auto entities = CreateTwoVehiclesAndTwoPedestrians();
    sensor_view_builder_.AddEntityIdToIgnoreList(1);
    sensor_view_builder_.Step(entities, entities.front().get());

    astas_osi3::GroundTruth gt = sensor_view_builder_.GetSensorView().global_ground_truth();

    const auto& vehicles = test_utils::GetObjectsByType(gt, astas_osi3::MovingObject::TYPE_VEHICLE);
    const auto& pedestrians = test_utils::GetObjectsByType(gt, astas_osi3::MovingObject::TYPE_PEDESTRIAN);
    ASSERT_EQ(1, vehicles.size());
    ASSERT_EQ(2, pedestrians.size());
}

TEST_F(SensorViewBuilderTest, GivenSystemClock_WhenGroundTruthAfterStep_ThenGroundTruthIsStampedWithSystemClock)
{
    service::utility::Clock::Instance().UseSystemClock();
    auto now = std::chrono::system_clock::now();

    auto entities = CreateHostEntityOnly();
    sensor_view_builder_.Step(entities, entities.front().get());

    astas_osi3::GroundTruth gt = sensor_view_builder_.GetSensorView().global_ground_truth();

    EXPECT_GE(gt.timestamp().seconds(),
              std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count());
}

TEST_F(SensorViewBuilderTest, GivenInvalidCenterEntity_WhenStep_ThenExpectedEnvironmentExceptionThrow)
{
    auto entities = CreateHostEntityOnly();

    EXPECT_THROW(sensor_view_builder_.Step(entities, nullptr), EnvironmentException);
}

TEST_F(SensorViewBuilderTest, GivenEmptyEntities_WhenStep_ThenExpectedEnvironmentExceptionThrow)
{
    std::vector<std::unique_ptr<mantle_api::IEntity>> empty_entities{};
    auto entities = CreateHostEntityOnly();

    EXPECT_THROW(sensor_view_builder_.Step(empty_entities, entities.front().get()), EnvironmentException);
}

TEST_F(SensorViewBuilderTest, GivenGroundTruthWithNoLane_WhenStep_ThenExpectedEnvironmentExceptionExceptionIsThrown)
{
    std::vector<std::unique_ptr<mantle_api::IEntity>> empty_entities{};
    std::unique_ptr<map::AstasMap> astas_map{
        std::move(test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach())};
    astas_map->GetLanes().clear();
    SensorViewBuilder sensor_view_builder{*astas_map, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    auto entities = CreateHostEntityOnly();

    EXPECT_TRUE(ExpectExceptionContains<EnvironmentException>(
        [&]() { sensor_view_builder.Step(entities, entities.front().get()); },
        "SensorViewBuilder: Lane is not set in the generated OSI Ground Truth."));
}

TEST_F(SensorViewBuilderTest, GivenGroundTruthWithNoLaneBoundaries_WhenStep_ThenExpectedEnvironmentExceptionIsThrown)
{
    std::vector<std::unique_ptr<mantle_api::IEntity>> empty_entities{};
    auto astas_map = std::move(test_utils::MapCatalogue::EmptyMap());
    CreateLaneGroupWithoutLaneBoundaries(*astas_map);
    SensorViewBuilder sensor_view_builder{*astas_map, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    sensor_view_builder.Init();
    auto entities = CreateHostEntityOnly();

    EXPECT_TRUE(ExpectExceptionContains<EnvironmentException>(
        [&]() { sensor_view_builder.Step(entities, entities.front().get()); },
        "SensorViewBuilder: minimum of Lane boundaries is 2 in the generated OSI Ground Truth."));
}

// Driving Function State Parametrized Tests

struct ExternalControlStateTestType
{
    mantle_api::ExternalControlState external_control_state;
    astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::State expected_driving_function_state;
};

class SensorViewBuilderDrivingFunctionTest  // NOLINT(fuchsia-multiple-inheritance)
    : public SensorViewBuilderTest,
      public testing::WithParamInterface<ExternalControlStateTestType>
{
};

std::vector<ExternalControlStateTestType> GetTestExternalControlStates()
{
    return {{mantle_api::ExternalControlState::kOff,
             astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_UNAVAILABLE},
            {mantle_api::ExternalControlState::kFull,
             astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE},
            {mantle_api::ExternalControlState::kLateralOnly,
             astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE},
            {mantle_api::ExternalControlState::kLongitudinalOnly,
             astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE}};
}

TEST_P(SensorViewBuilderDrivingFunctionTest,
       GivenMapWithHostVehicleWithExternalState_WhenInitStep_ThenSensorViewContainsExpectedDrivingFunction)
{

    auto entities = CreateHostEntityOnly();
    const auto test_data = GetParam();
    dynamic_cast<entities::VehicleEntity*>(entities.front().get())
        ->SetHADControlState(test_data.external_control_state);

    sensor_view_builder_.Step(entities, entities.front().get());

    const auto host_driving_functions{
        sensor_view_builder_.GetSensorView().host_vehicle_data().vehicle_automated_driving_function()};

    ASSERT_EQ(1, host_driving_functions.size());
    EXPECT_EQ(astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_HIGHWAY_AUTOPILOT,
              host_driving_functions[0].name());

    EXPECT_EQ(test_data.expected_driving_function_state, host_driving_functions[0].state());
}

INSTANTIATE_TEST_SUITE_P(ExternalControlStateParameters,
                         SensorViewBuilderDrivingFunctionTest,
                         ::testing::ValuesIn(GetTestExternalControlStates()));

}  // namespace astas::environment::proto_groundtruth
