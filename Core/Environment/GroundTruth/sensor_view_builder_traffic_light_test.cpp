/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/GroundTruth/sensor_view_builder_test_common.h"

#include <gtest/gtest.h>
#include <units.h>

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_deg;

namespace astas::environment::proto_groundtruth
{

using ProtoLightColor = astas_osi3::TrafficLight::Classification;
ProtoLightColor::Color ConvertAstasTrafficLightColorToProto(map::OsiTrafficLightColor color)
{
    switch (color)
    {
        case map::OsiTrafficLightColor::kUnknown:
        {
            return ProtoLightColor::COLOR_UNKNOWN;
        }
        case map::OsiTrafficLightColor::kOther:
        {
            return ProtoLightColor::COLOR_OTHER;
        }
        case map::OsiTrafficLightColor::kRed:
        {
            return ProtoLightColor::COLOR_RED;
        }
        case map::OsiTrafficLightColor::kYellow:
        {
            return ProtoLightColor::COLOR_YELLOW;
        }
        case map::OsiTrafficLightColor::kGreen:
        {
            return ProtoLightColor::COLOR_GREEN;
        }
        case map::OsiTrafficLightColor::kBlue:
        {
            return ProtoLightColor::COLOR_BLUE;
        }
        case map::OsiTrafficLightColor::kWhite:
        {
            return ProtoLightColor::COLOR_WHITE;
        }
        default:
        {
            ASSERT(false && "Could not convert the passed in Astas traffic light color to proto traffic light color")
        }
    }
}

using ProtoLightIcon = astas_osi3::TrafficLight::Classification;
ProtoLightIcon::Icon ConvertAstasTrafficLightIconToProto(map::OsiTrafficLightIcon icon)
{
    switch (icon)
    {
        case map::OsiTrafficLightIcon::kUnknown:
        {
            return ProtoLightIcon::ICON_UNKNOWN;
        }
        case map::OsiTrafficLightIcon::kOther:
        {
            return ProtoLightIcon::ICON_OTHER;
        }
        case map::OsiTrafficLightIcon::kNone:
        {
            return ProtoLightIcon::ICON_NONE;
        }
        case map::OsiTrafficLightIcon::kArrowStraightAhead:
        {
            return ProtoLightIcon::ICON_ARROW_STRAIGHT_AHEAD;
        }
        case map::OsiTrafficLightIcon::kArrowLeft:
        {
            return ProtoLightIcon::ICON_ARROW_LEFT;
        }
        case map::OsiTrafficLightIcon::kArrowDiagonalLeft:
        {
            return ProtoLightIcon::ICON_ARROW_DIAG_LEFT;
        }
        case map::OsiTrafficLightIcon::kArrowStraightAheadLeft:
        {
            return ProtoLightIcon::ICON_ARROW_STRAIGHT_AHEAD_LEFT;
        }
        case map::OsiTrafficLightIcon::kArrowRight:
        {
            return ProtoLightIcon::ICON_ARROW_RIGHT;
        }
        case map::OsiTrafficLightIcon::kArrowDiagonalRight:
        {
            return ProtoLightIcon::ICON_ARROW_DIAG_RIGHT;
        }
        case map::OsiTrafficLightIcon::kArrowStraightAheadRight:
        {
            return ProtoLightIcon::ICON_ARROW_STRAIGHT_AHEAD_RIGHT;
        }
        case map::OsiTrafficLightIcon::kArrowLeftRight:
        {
            return ProtoLightIcon::ICON_ARROW_LEFT_RIGHT;
        }
        case map::OsiTrafficLightIcon::kArrowDown:
        {
            return ProtoLightIcon::ICON_ARROW_DOWN;
        }
        case map::OsiTrafficLightIcon::kArrowDownLeft:
        {
            return ProtoLightIcon::ICON_ARROW_DOWN_LEFT;
        }
        case map::OsiTrafficLightIcon::kArrowDownRight:
        {
            return ProtoLightIcon::ICON_ARROW_DOWN_RIGHT;
        }
        case map::OsiTrafficLightIcon::kArrowCross:
        {
            return ProtoLightIcon::ICON_ARROW_CROSS;
        }
        case map::OsiTrafficLightIcon::kPedestrian:
        {
            return ProtoLightIcon::ICON_PEDESTRIAN;
        }
        case map::OsiTrafficLightIcon::kWalk:
        {
            return ProtoLightIcon::ICON_WALK;
        }
        case map::OsiTrafficLightIcon::kDontWalk:
        {
            return ProtoLightIcon::ICON_DONT_WALK;
        }
        case map::OsiTrafficLightIcon::kBicycle:
        {
            return ProtoLightIcon::ICON_BICYCLE;
        }
        case map::OsiTrafficLightIcon::kPedestrianAndBicycle:
        {
            return ProtoLightIcon::ICON_PEDESTRIAN_AND_BICYCLE;
        }
        case map::OsiTrafficLightIcon::kCountdownSeconds:
        {
            return ProtoLightIcon::ICON_COUNTDOWN_SECONDS;
        }
        case map::OsiTrafficLightIcon::kCountdownPercent:
        {
            return ProtoLightIcon::ICON_COUNTDOWN_PERCENT;
        }
        case map::OsiTrafficLightIcon::kTram:
        {
            return ProtoLightIcon::ICON_TRAM;
        }
        case map::OsiTrafficLightIcon::kBus:
        {
            return ProtoLightIcon::ICON_BUS;
        }
        case map::OsiTrafficLightIcon::kBusAndTram:
        {
            return ProtoLightIcon::ICON_BUS_AND_TRAM;
        }
        default:
        {
            ASSERT(false && "Could not convert the passed in Astas traffic light icon to proto traffic light icon")
        }
    }
}

using ProtoLightMode = astas_osi3::TrafficLight::Classification;
ProtoLightMode::Mode ConvertAstasTrafficLightModeToProto(map::OsiTrafficLightMode mode)
{
    switch (mode)
    {
        case map::OsiTrafficLightMode::kUnknown:
        {
            return ProtoLightMode::MODE_UNKNOWN;
        }
        case map::OsiTrafficLightMode::kOther:
        {
            return ProtoLightMode::MODE_OTHER;
        }
        case map::OsiTrafficLightMode::kOff:
        {
            return ProtoLightMode::MODE_OFF;
        }
        case map::OsiTrafficLightMode::kConstant:
        {
            return ProtoLightMode::MODE_CONSTANT;
        }
        case map::OsiTrafficLightMode::kFlashing:
        {
            return ProtoLightMode::MODE_FLASHING;
        }
        case map::OsiTrafficLightMode::kCounting:
        {
            return ProtoLightMode::MODE_COUNTING;
        }
        default:
        {
            ASSERT(false && "Could not convert the passed in Astas traffic light mode to proto traffic light mode")
        }
    }
}

class TrafficLightTest : public BaseSensorViewBuilderObjectGenerationTest
{
  protected:
    virtual void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) = 0;

    void AddObjectsToMap() override
    {
        map::TrafficLight traffic_light{.id{GetNewId()}, .light_bulbs{{.id{GetNewId()}}}};
        SetTrafficLightMapObjectProperties(traffic_light);

        astas_map_->traffic_lights.push_back(traffic_light);
    }

    virtual void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) = 0;

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::TrafficLightEntity>(GetNewId(), "traffic_light"));
        auto traffic_light_properties = std::make_unique<mantle_ext::TrafficLightProperties>();
        traffic_light_properties->type = mantle_api::EntityType::kStatic;
        constexpr std::size_t bulbs_per_light = 1;
        // If a class/struct inherits from something, cannot use initializer lists as if it were a PODS
        std::vector<mantle_ext::TrafficLightBulbProperties> bulbs(bulbs_per_light);
        bulbs[0].id = GetNewId();
        traffic_light_properties->bulbs = std::move(bulbs);
        SetTrafficLightEntityProperties(*traffic_light_properties);

        entities_.back()->SetProperties(std::move(traffic_light_properties));
        entities_.back()->SetPosition(position_);
    }

    mantle_api::Vec3<units::length::meter_t> position_{};
};

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightIdTestParameterized : public TrafficLightTest,
                                        public testing::WithParamInterface<mantle_api::UniqueId>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].id = GetParam();
        traffic_light.light_bulbs.push_back(map::TrafficLightBulb{.id{GetParam() + 1}});
        traffic_light.light_bulbs.push_back(map::TrafficLightBulb{.id{GetParam() + 2}});
    }

    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        traffic_light_properties.bulbs[0].id = GetParam() + 3;
        traffic_light_properties.bulbs.push_back(mantle_ext::TrafficLightBulbProperties{});
        traffic_light_properties.bulbs[1].id = GetParam() + 4;
        traffic_light_properties.bulbs.push_back(mantle_ext::TrafficLightBulbProperties{});
        traffic_light_properties.bulbs[2].id = GetParam() + 5;
    }
};

TEST_P(TrafficLightIdTestParameterized, GivenMapWithTwoLanes_WhenStep_ThenTrafficLightIdHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    mantle_api::UniqueId expected_id = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    EXPECT_EQ(expected_id, ground_truth.traffic_light(0).id().value());
    EXPECT_EQ(expected_id + 1, ground_truth.traffic_light(1).id().value());
    EXPECT_EQ(expected_id + 2, ground_truth.traffic_light(2).id().value());
    EXPECT_EQ(expected_id + 3, ground_truth.traffic_light(3).id().value());
    EXPECT_EQ(expected_id + 4, ground_truth.traffic_light(4).id().value());
    EXPECT_EQ(expected_id + 5, ground_truth.traffic_light(5).id().value());
}

INSTANTIATE_TEST_SUITE_P(TrafficLightIdTest, TrafficLightIdTestParameterized, testing::Values(0, 1, 2, 3));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightColorTestParameterized : public TrafficLightTest,
                                           public testing::WithParamInterface<map::OsiTrafficLightColor>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].color = GetParam();
    }

    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        traffic_light_properties.bulbs[0].color = static_cast<mantle_api::TrafficLightBulbColor>(GetParam());
    }
};

TEST_P(TrafficLightColorTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectColor)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_color = ConvertAstasTrafficLightColorToProto(GetParam());
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    EXPECT_EQ(expected_color, ground_truth.traffic_light(0).classification().color());
    EXPECT_EQ(expected_color, ground_truth.traffic_light(1).classification().color());
}

INSTANTIATE_TEST_SUITE_P(TrafficLightColorTest,
                         TrafficLightColorTestParameterized,
                         testing::Values(map::OsiTrafficLightColor::kUnknown,
                                         map::OsiTrafficLightColor::kOther,
                                         map::OsiTrafficLightColor::kRed,
                                         map::OsiTrafficLightColor::kYellow,
                                         map::OsiTrafficLightColor::kGreen,
                                         map::OsiTrafficLightColor::kBlue,
                                         map::OsiTrafficLightColor::kWhite));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightIconTestParameterized : public TrafficLightTest,
                                          public testing::WithParamInterface<map::OsiTrafficLightIcon>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].icon = GetParam();
    }

    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        traffic_light_properties.bulbs[0].icon = static_cast<osi::OsiTrafficLightIcon>(GetParam());
    }
};

TEST_P(TrafficLightIconTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectIcon)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_icon = ConvertAstasTrafficLightIconToProto(GetParam());
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    EXPECT_EQ(expected_icon, ground_truth.traffic_light(0).classification().icon());
    EXPECT_EQ(expected_icon, ground_truth.traffic_light(1).classification().icon());
}

INSTANTIATE_TEST_SUITE_P(TrafficLightIconTest,
                         TrafficLightIconTestParameterized,
                         testing::Values(map::OsiTrafficLightIcon::kUnknown,
                                         map::OsiTrafficLightIcon::kOther,
                                         map::OsiTrafficLightIcon::kNone,
                                         map::OsiTrafficLightIcon::kArrowStraightAhead,
                                         map::OsiTrafficLightIcon::kArrowLeft,
                                         map::OsiTrafficLightIcon::kArrowDiagonalLeft,
                                         map::OsiTrafficLightIcon::kArrowStraightAheadLeft,
                                         map::OsiTrafficLightIcon::kArrowRight,
                                         map::OsiTrafficLightIcon::kArrowDiagonalRight,
                                         map::OsiTrafficLightIcon::kArrowStraightAheadRight,
                                         map::OsiTrafficLightIcon::kArrowLeftRight,
                                         map::OsiTrafficLightIcon::kArrowDown,
                                         map::OsiTrafficLightIcon::kArrowDownLeft,
                                         map::OsiTrafficLightIcon::kArrowDownRight,
                                         map::OsiTrafficLightIcon::kArrowCross,
                                         map::OsiTrafficLightIcon::kPedestrian,
                                         map::OsiTrafficLightIcon::kWalk,
                                         map::OsiTrafficLightIcon::kDontWalk,
                                         map::OsiTrafficLightIcon::kBicycle,
                                         map::OsiTrafficLightIcon::kPedestrianAndBicycle,
                                         map::OsiTrafficLightIcon::kCountdownSeconds,
                                         map::OsiTrafficLightIcon::kCountdownPercent,
                                         map::OsiTrafficLightIcon::kTram,
                                         map::OsiTrafficLightIcon::kBus,
                                         map::OsiTrafficLightIcon::kBusAndTram));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightModeTestParameterized : public TrafficLightTest,
                                          public testing::WithParamInterface<map::OsiTrafficLightMode>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].mode = GetParam();
    }

    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        traffic_light_properties.bulbs[0].mode = static_cast<mantle_api::TrafficLightBulbMode>(GetParam());
    }
};

TEST_P(TrafficLightModeTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectMode)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_mode = ConvertAstasTrafficLightModeToProto(GetParam());
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    EXPECT_EQ(expected_mode, ground_truth.traffic_light(0).classification().mode());
    EXPECT_EQ(expected_mode, ground_truth.traffic_light(1).classification().mode());
}

INSTANTIATE_TEST_SUITE_P(TrafficLightModeTest,
                         TrafficLightModeTestParameterized,
                         testing::Values(map::OsiTrafficLightMode::kUnknown,
                                         map::OsiTrafficLightMode::kOther,
                                         map::OsiTrafficLightMode::kOff,
                                         map::OsiTrafficLightMode::kConstant,
                                         map::OsiTrafficLightMode::kFlashing,
                                         map::OsiTrafficLightMode::kCounting));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightCounterTestParameterized : public TrafficLightTest, public testing::WithParamInterface<double>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].mode = map::OsiTrafficLightMode::kCounting;
        traffic_light.light_bulbs[0].count = GetParam();
    }

    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        traffic_light_properties.bulbs[0].mode = mantle_api::TrafficLightBulbMode::kCounting;
        traffic_light_properties.bulbs[0].count = GetParam();
    }
};

TEST_P(TrafficLightCounterTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectCount)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_count = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    EXPECT_EQ(expected_count, ground_truth.traffic_light(0).classification().counter());
    EXPECT_EQ(expected_count, ground_truth.traffic_light(1).classification().counter());
}

// TODO: what values should be tested?
INSTANTIATE_TEST_SUITE_P(TrafficLightCounterTest,
                         TrafficLightCounterTestParameterized,
                         testing::Values(0.0, 1.0, 3.14159, -1.0));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightAssignedLaneIdTestParameterized
    : public TrafficLightTest,
      public testing::WithParamInterface<std::vector<mantle_api::UniqueId>>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].assigned_lanes = GetParam();
    }

    // NOLINTNEXTLINE(misc-unused-parameters)
    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        entities_.back()->SetAssignedLaneIds(GetParam());
    }
};

TEST_P(TrafficLightAssignedLaneIdTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectAssignedLaneId)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_lane_ids = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    ASSERT_EQ(expected_lane_ids.size(), ground_truth.traffic_light(0).classification().assigned_lane_id_size());
    ASSERT_EQ(expected_lane_ids.size(), ground_truth.traffic_light(1).classification().assigned_lane_id_size());

    for (int i = 0; i < expected_lane_ids.size(); i++)
    {
        EXPECT_EQ(expected_lane_ids[i], ground_truth.traffic_light(0).classification().assigned_lane_id(i).value());
        EXPECT_EQ(expected_lane_ids[i], ground_truth.traffic_light(1).classification().assigned_lane_id(i).value());
    }
}

INSTANTIATE_TEST_SUITE_P(TrafficLightAssignedLaneIdTest,
                         TrafficLightAssignedLaneIdTestParameterized,
                         testing::Values(std::vector<mantle_api::UniqueId>{0, 1},
                                         std::vector<mantle_api::UniqueId>{1, 0},
                                         std::vector<mantle_api::UniqueId>{0},
                                         std::vector<mantle_api::UniqueId>{1}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightPositionTestParameterized
    : public TrafficLightTest,
      public testing::WithParamInterface<mantle_api::Vec3<units::length::meter_t>>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].pose.position = GetParam();
    }

    // NOLINTNEXTLINE(misc-unused-parameters)
    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        position_ = GetParam();
        // This happens in the base class, not possible here, because properties always needs to be set before position.
        // entities_.back()->SetPosition(position_);
    }
};

TEST_P(TrafficLightPositionTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectPosition)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_position = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    for (int light = 0; light < ground_truth.traffic_light_size(); light++)
    {
        auto& position = ground_truth.traffic_light(light).base().position();
        EXPECT_EQ(expected_position.x.value(), position.x());
        EXPECT_EQ(expected_position.y.value(), position.y());
        EXPECT_EQ(expected_position.z.value(), position.z());
    }
}

INSTANTIATE_TEST_SUITE_P(TrafficLightPositionTest,
                         TrafficLightPositionTestParameterized,
                         testing::Values(mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 1.0_m},
                                         mantle_api::Vec3<units::length::meter_t>{-1.0_m, -1.0_m, -1.0_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightOrientationTestParameterized
    : public TrafficLightTest,
      public testing::WithParamInterface<mantle_api::Orientation3<units::angle::radian_t>>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].pose.orientation = GetParam();
    }

    // NOLINTNEXTLINE(misc-unused-parameters)
    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        entities_.back()->SetOrientation(GetParam());
    }
};

TEST_P(TrafficLightOrientationTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectOrientation)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_orientation = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    for (int light = 0; light < ground_truth.traffic_light_size(); light++)
    {
        auto& orientation = ground_truth.traffic_light(light).base().orientation();
        EXPECT_EQ(expected_orientation.yaw.value(), orientation.yaw());
        EXPECT_EQ(expected_orientation.pitch.value(), orientation.pitch());
        EXPECT_EQ(expected_orientation.roll.value(), orientation.roll());
    }
}

INSTANTIATE_TEST_SUITE_P(
    TrafficLightOrientationTest,
    TrafficLightOrientationTestParameterized,
    testing::Values(mantle_api::Orientation3<units::angle::radian_t>(0.0_rad, 0.0_rad, 0.0_rad),
                    mantle_api::Orientation3<units::angle::radian_t>(1.0_rad, 1.0_rad, 1.0_rad),
                    mantle_api::Orientation3<units::angle::radian_t>(-1.0_rad, -1.0_rad, -1.0_rad)));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficLightDimensionTestParameterized : public TrafficLightTest,
                                               public testing::WithParamInterface<mantle_api::Dimension3>
{
  protected:
    void SetTrafficLightMapObjectProperties(map::TrafficLight& traffic_light) override
    {
        traffic_light.light_bulbs[0].dimensions = GetParam();
    }

    void SetTrafficLightEntityProperties(mantle_ext::TrafficLightProperties& traffic_light_properties) override
    {
        traffic_light_properties.bulbs[0].bounding_box.dimension = GetParam();
    }
};

TEST_P(TrafficLightDimensionTestParameterized,
       GivenMapWithTwoLanesAndTrafficLight_WhenStep_ThenGroundTruthTrafficLightHasCorrectDimension)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_dimension = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    for (int light = 0; light < ground_truth.traffic_light_size(); light++)
    {
        auto& dimension = ground_truth.traffic_light(light).base().dimension();
        EXPECT_EQ(expected_dimension.length.value(), dimension.length());
        EXPECT_EQ(expected_dimension.width.value(), dimension.width());
        EXPECT_EQ(expected_dimension.height.value(), dimension.height());
    }
}

INSTANTIATE_TEST_SUITE_P(TrafficLightDimensionTest,
                         TrafficLightDimensionTestParameterized,
                         testing::Values(mantle_api::Dimension3{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Dimension3{0.1_m, 0.02_m, 0.3_m}));

}  // namespace astas::environment::proto_groundtruth
