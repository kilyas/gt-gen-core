/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/static_object_entity.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/GroundTruth/sensor_view_builder_test_common.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"

#include <gtest/gtest.h>
#include <units.h>

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_deg;

namespace astas::environment::proto_groundtruth
{
class TrafficSignBaseTest : public BaseSensorViewBuilderObjectGenerationTest
{
  protected:
    void SetUp() override
    {
        BaseSensorViewBuilderObjectGenerationTest::SetUp();
        sensor_view_builder_.Step(entities_, entities_.front().get());
        gt_ = sensor_view_builder_.GetSensorView().global_ground_truth();
    }

    astas_osi3::GroundTruth gt_;
};

auto GetTrafficSignValueUnitTuples()
{
    static auto traffic_sign_value_units_testing = {
        std::make_tuple(osi::OsiTrafficSignValueUnit::kUnknown,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kOther,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_OTHER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kNoUnit,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_NO_UNIT),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kKilometerPerHour,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER_PER_HOUR),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMilePerHour,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MILE_PER_HOUR),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMeter,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_METER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kKilometer,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kFeet,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_FEET),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMile,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MILE),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMetricTon,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_METRIC_TON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kLongTon,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_LONG_TON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kShortTon,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_SHORT_TON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMinutes,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MINUTES),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kDay,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_DAY),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kPercentage,
                        astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_PERCENTAGE)};

    return traffic_sign_value_units_testing;
}

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficSignValueTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<std::tuple<osi::OsiTrafficSignValueUnit, astas_osi3::TrafficSignValue_Unit>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::SignValueInformation value_information;
        value_information.text = "foo";
        value_information.value = 120.0;
        value_information.value_unit = std::get<0>(GetParam());

        auto traffic_sign = std::make_shared<map::MountedSign>();
        traffic_sign->value_information = value_information;
        traffic_sign->id = GetNewId();
        astas_map_->traffic_signs.push_back(traffic_sign);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign"));
        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        traffic_sign_properties->text = "foo";
        traffic_sign_properties->value = 120.0;
        traffic_sign_properties->unit = std::get<0>(GetParam());
        entities_.back()->SetProperties(std::move(traffic_sign_properties));
    }
};

TEST_P(TrafficSignValueTestParameterized,
       GivenTrafficSignWithSpecificValue_WhenStep_ThenExpectedTrafficSignHasRightValue)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    const auto& actual_map_sign_value = gt_.traffic_sign(0).main_sign().classification().value();

    EXPECT_EQ(actual_map_sign_value.text(), "foo");
    EXPECT_EQ(actual_map_sign_value.value(), 120.0);
    EXPECT_EQ(actual_map_sign_value.value_unit(), std::get<1>(GetParam()));

    const auto& actual_entity_sign_value = gt_.traffic_sign(1).main_sign().classification().value();

    EXPECT_EQ(actual_entity_sign_value.text(), "foo");
    EXPECT_EQ(actual_entity_sign_value.value(), 120.0);
    EXPECT_EQ(actual_entity_sign_value.value_unit(), std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(TrafficSignValueTest,
                         TrafficSignValueTestParameterized,
                         testing::ValuesIn(GetTrafficSignValueUnitTuples()));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficSignTypeTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<
          std::tuple<osi::OsiTrafficSignType, astas_osi3::TrafficSign_MainSign_Classification_Type>>
{
  protected:
    void AddObjectsToMap() override
    {
        auto traffic_sign = std::make_shared<map::MountedSign>();
        traffic_sign->type = std::get<0>(GetParam());
        traffic_sign->id = GetNewId();
        astas_map_->traffic_signs.push_back(traffic_sign);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign"));
        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        traffic_sign_properties->sign_type = std::get<0>(GetParam());
        entities_.back()->SetProperties(std::move(traffic_sign_properties));
    }
};

TEST_P(TrafficSignTypeTestParameterized, GivenTrafficSignOfSpecificType_WhenStep_ThenExpectedTrafficSignHasRightType)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    EXPECT_EQ(gt_.traffic_sign(0).main_sign().classification().type(), std::get<1>(GetParam()));
    EXPECT_EQ(gt_.traffic_sign(1).main_sign().classification().type(), std::get<1>(GetParam()));
}

// clang-format off

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CREATE_TRAFFIC_SIGN_TYPE_TUPLE(custom_type, osi_type) \
    std::make_tuple(osi::OsiTrafficSignType::custom_type,     \
                    astas_osi3::TrafficSign_MainSign_Classification_Type::osi_type)

INSTANTIATE_TEST_SUITE_P(
    TrafficSignTypeTest,
    TrafficSignTypeTestParameterized,
    testing::Values(
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kUnknown, TrafficSign_MainSign_Classification_Type_TYPE_UNKNOWN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOther, TrafficSign_MainSign_Classification_Type_TYPE_OTHER),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDangerSpot, TrafficSign_MainSign_Classification_Type_TYPE_DANGER_SPOT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kZebraCrossing, TrafficSign_MainSign_Classification_Type_TYPE_ZEBRA_CROSSING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kFlight, TrafficSign_MainSign_Classification_Type_TYPE_FLIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCattle, TrafficSign_MainSign_Classification_Type_TYPE_CATTLE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHorseRiders, TrafficSign_MainSign_Classification_Type_TYPE_HORSE_RIDERS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAmphibians, TrafficSign_MainSign_Classification_Type_TYPE_AMPHIBIANS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kFallingRocks, TrafficSign_MainSign_Classification_Type_TYPE_FALLING_ROCKS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSnowOrIce, TrafficSign_MainSign_Classification_Type_TYPE_SNOW_OR_ICE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kLooseGravel, TrafficSign_MainSign_Classification_Type_TYPE_LOOSE_GRAVEL),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kWaterside, TrafficSign_MainSign_Classification_Type_TYPE_WATERSIDE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kClearance, TrafficSign_MainSign_Classification_Type_TYPE_CLEARANCE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMovableBridge, TrafficSign_MainSign_Classification_Type_TYPE_MOVABLE_BRIDGE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRightBeforeLeftNextIntersection,TrafficSign_MainSign_Classification_Type_TYPE_RIGHT_BEFORE_LEFT_NEXT_INTERSECTION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTurnLeft, TrafficSign_MainSign_Classification_Type_TYPE_TURN_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTurnRight, TrafficSign_MainSign_Classification_Type_TYPE_TURN_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDoubleTurnLeft, TrafficSign_MainSign_Classification_Type_TYPE_DOUBLE_TURN_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDoubleTurnRight,TrafficSign_MainSign_Classification_Type_TYPE_DOUBLE_TURN_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHillDownwards, TrafficSign_MainSign_Classification_Type_TYPE_HILL_DOWNWARDS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHillUpwards, TrafficSign_MainSign_Classification_Type_TYPE_HILL_UPWARDS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kUnevenRoad, TrafficSign_MainSign_Classification_Type_TYPE_UNEVEN_ROAD),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRoadSlipperyWetOrDirty,TrafficSign_MainSign_Classification_Type_TYPE_ROAD_SLIPPERY_WET_OR_DIRTY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSideWinds, TrafficSign_MainSign_Classification_Type_TYPE_SIDE_WINDS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRoadNarrowing, TrafficSign_MainSign_Classification_Type_TYPE_ROAD_NARROWING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRoadNarrowingRight,TrafficSign_MainSign_Classification_Type_TYPE_ROAD_NARROWING_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRoadNarrowingLeft,TrafficSign_MainSign_Classification_Type_TYPE_ROAD_NARROWING_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRoadWorks, TrafficSign_MainSign_Classification_Type_TYPE_ROAD_WORKS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTrafficQueues, TrafficSign_MainSign_Classification_Type_TYPE_TRAFFIC_QUEUES),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTwoWayTraffic, TrafficSign_MainSign_Classification_Type_TYPE_TWO_WAY_TRAFFIC),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAttentionTrafficLight,TrafficSign_MainSign_Classification_Type_TYPE_ATTENTION_TRAFFIC_LIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPedestrians, TrafficSign_MainSign_Classification_Type_TYPE_PEDESTRIANS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kChildrenCrossing,TrafficSign_MainSign_Classification_Type_TYPE_CHILDREN_CROSSING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCycleRoute, TrafficSign_MainSign_Classification_Type_TYPE_CYCLE_ROUTE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDeerCrossing, TrafficSign_MainSign_Classification_Type_TYPE_DEER_CROSSING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kUngatedLevelCrossing,TrafficSign_MainSign_Classification_Type_TYPE_UNGATED_LEVEL_CROSSING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kLevelCrossingMarker,TrafficSign_MainSign_Classification_Type_TYPE_LEVEL_CROSSING_MARKER),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRailwayTrafficPriority,TrafficSign_MainSign_Classification_Type_TYPE_RAILWAY_TRAFFIC_PRIORITY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kGiveWay, TrafficSign_MainSign_Classification_Type_TYPE_GIVE_WAY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kStop, TrafficSign_MainSign_Classification_Type_TYPE_STOP),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPriorityToOppositeDirection,TrafficSign_MainSign_Classification_Type_TYPE_PRIORITY_TO_OPPOSITE_DIRECTION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPriorityToOppositeDirectionUpsideDown,TrafficSign_MainSign_Classification_Type_TYPE_PRIORITY_TO_OPPOSITE_DIRECTION_UPSIDE_DOWN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedLeftTurn,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_LEFT_TURN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedRightTurn,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_RIGHT_TURN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedStraight,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_STRAIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedRightWay,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_RIGHT_WAY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedLeftWay,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_LEFT_WAY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedRightTurnAndStraight,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_RIGHT_TURN_AND_STRAIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedLeftTurnAndStraight,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_LEFT_TURN_AND_STRAIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedLeftTurnAndRightTurn,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_LEFT_TURN_AND_RIGHT_TURN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedLeftTurnRightTurnAndStraight,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_LEFT_TURN_RIGHT_TURN_AND_STRAIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRoundabout, TrafficSign_MainSign_Classification_Type_TYPE_ROUNDABOUT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOnewayLeft, TrafficSign_MainSign_Classification_Type_TYPE_ONEWAY_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOnewayRight, TrafficSign_MainSign_Classification_Type_TYPE_ONEWAY_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPassLeft, TrafficSign_MainSign_Classification_Type_TYPE_PASS_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPassRight, TrafficSign_MainSign_Classification_Type_TYPE_PASS_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSideLaneOpenForTraffic,TrafficSign_MainSign_Classification_Type_TYPE_SIDE_LANE_OPEN_FOR_TRAFFIC),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSideLaneClosedForTraffic,TrafficSign_MainSign_Classification_Type_TYPE_SIDE_LANE_CLOSED_FOR_TRAFFIC),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSideLaneClosingForTraffic,TrafficSign_MainSign_Classification_Type_TYPE_SIDE_LANE_CLOSING_FOR_TRAFFIC),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBusStop, TrafficSign_MainSign_Classification_Type_TYPE_BUS_STOP),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTaxiStand, TrafficSign_MainSign_Classification_Type_TYPE_TAXI_STAND),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBicyclesOnly, TrafficSign_MainSign_Classification_Type_TYPE_BICYCLES_ONLY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHorseRidersOnly,TrafficSign_MainSign_Classification_Type_TYPE_HORSE_RIDERS_ONLY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPedestriansOnly,TrafficSign_MainSign_Classification_Type_TYPE_PEDESTRIANS_ONLY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBicyclesPedestriansSharedOnly,TrafficSign_MainSign_Classification_Type_TYPE_BICYCLES_PEDESTRIANS_SHARED_ONLY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBicyclesPedestriansSeparatedLeftOnly,TrafficSign_MainSign_Classification_Type_TYPE_BICYCLES_PEDESTRIANS_SEPARATED_LEFT_ONLY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBicyclesPedestriansSeparatedRightOnly,TrafficSign_MainSign_Classification_Type_TYPE_BICYCLES_PEDESTRIANS_SEPARATED_RIGHT_ONLY),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPedestrianZoneBegin,TrafficSign_MainSign_Classification_Type_TYPE_PEDESTRIAN_ZONE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPedestrianZoneEnd,TrafficSign_MainSign_Classification_Type_TYPE_PEDESTRIAN_ZONE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBicycleRoadBegin,TrafficSign_MainSign_Classification_Type_TYPE_BICYCLE_ROAD_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBicycleRoadEnd, TrafficSign_MainSign_Classification_Type_TYPE_BICYCLE_ROAD_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBusLane, TrafficSign_MainSign_Classification_Type_TYPE_BUS_LANE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBusLaneBegin, TrafficSign_MainSign_Classification_Type_TYPE_BUS_LANE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBusLaneEnd, TrafficSign_MainSign_Classification_Type_TYPE_BUS_LANE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAllProhibited, TrafficSign_MainSign_Classification_Type_TYPE_ALL_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMotorizedMultitrackProhibited,TrafficSign_MainSign_Classification_Type_TYPE_MOTORIZED_MULTITRACK_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTrucksProhibited,TrafficSign_MainSign_Classification_Type_TYPE_TRUCKS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBicyclesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_BICYCLES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMotorcyclesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_MOTORCYCLES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMopedsProhibited,TrafficSign_MainSign_Classification_Type_TYPE_MOPEDS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHorseRidersProhibited,TrafficSign_MainSign_Classification_Type_TYPE_HORSE_RIDERS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHorseCarriagesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_HORSE_CARRIAGES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCattleProhibited,TrafficSign_MainSign_Classification_Type_TYPE_CATTLE_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kBusesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_BUSES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCarsProhibited, TrafficSign_MainSign_Classification_Type_TYPE_CARS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCarsTrailersProhibited,TrafficSign_MainSign_Classification_Type_TYPE_CARS_TRAILERS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTrucksTrailersProhibited,TrafficSign_MainSign_Classification_Type_TYPE_TRUCKS_TRAILERS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTractorsProhibited,TrafficSign_MainSign_Classification_Type_TYPE_TRACTORS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPedestriansProhibited,TrafficSign_MainSign_Classification_Type_TYPE_PEDESTRIANS_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMotorVehiclesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_MOTOR_VEHICLES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHazardousGoodsVehiclesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_HAZARDOUS_GOODS_VEHICLES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOverWeightVehiclesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_OVER_WEIGHT_VEHICLES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kVehiclesAxleOverWeightProhibited,TrafficSign_MainSign_Classification_Type_TYPE_VEHICLES_AXLE_OVER_WEIGHT_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kVehiclesExcessWidthProhibited,TrafficSign_MainSign_Classification_Type_TYPE_VEHICLES_EXCESS_WIDTH_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kVehiclesExcessHeightProhibited,TrafficSign_MainSign_Classification_Type_TYPE_VEHICLES_EXCESS_HEIGHT_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kVehiclesExcessLengthProhibited,TrafficSign_MainSign_Classification_Type_TYPE_VEHICLES_EXCESS_LENGTH_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDoNotEnter, TrafficSign_MainSign_Classification_Type_TYPE_DO_NOT_ENTER),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSnowChainsRequired,TrafficSign_MainSign_Classification_Type_TYPE_SNOW_CHAINS_REQUIRED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kWaterPollutantVehiclesProhibited,TrafficSign_MainSign_Classification_Type_TYPE_WATER_POLLUTANT_VEHICLES_PROHIBITED),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kEnvironmentalZoneBegin,TrafficSign_MainSign_Classification_Type_TYPE_ENVIRONMENTAL_ZONE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kEnvironmentalZoneEnd,TrafficSign_MainSign_Classification_Type_TYPE_ENVIRONMENTAL_ZONE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNoUTurnLeft, TrafficSign_MainSign_Classification_Type_TYPE_NO_U_TURN_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNoUTurnRight, TrafficSign_MainSign_Classification_Type_TYPE_NO_U_TURN_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedUTurnLeft,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_U_TURN_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrescribedUTurnRight,TrafficSign_MainSign_Classification_Type_TYPE_PRESCRIBED_U_TURN_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMinimumDistanceForTrucks,TrafficSign_MainSign_Classification_Type_TYPE_MINIMUM_DISTANCE_FOR_TRUCKS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSpeedLimitBegin,TrafficSign_MainSign_Classification_Type_TYPE_SPEED_LIMIT_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSpeedLimitZoneBegin,TrafficSign_MainSign_Classification_Type_TYPE_SPEED_LIMIT_ZONE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSpeedLimitZoneEnd,TrafficSign_MainSign_Classification_Type_TYPE_SPEED_LIMIT_ZONE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMinimumSpeedBegin,TrafficSign_MainSign_Classification_Type_TYPE_MINIMUM_SPEED_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOvertakingBanBegin,TrafficSign_MainSign_Classification_Type_TYPE_OVERTAKING_BAN_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOvertakingBanForTrucksBegin,TrafficSign_MainSign_Classification_Type_TYPE_OVERTAKING_BAN_FOR_TRUCKS_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSpeedLimitEnd, TrafficSign_MainSign_Classification_Type_TYPE_SPEED_LIMIT_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMinimumSpeedEnd,TrafficSign_MainSign_Classification_Type_TYPE_MINIMUM_SPEED_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOvertakingBanEnd,TrafficSign_MainSign_Classification_Type_TYPE_OVERTAKING_BAN_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOvertakingBanForTrucksEnd,TrafficSign_MainSign_Classification_Type_TYPE_OVERTAKING_BAN_FOR_TRUCKS_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAllRestrictionsEnd,TrafficSign_MainSign_Classification_Type_TYPE_ALL_RESTRICTIONS_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNoStopping, TrafficSign_MainSign_Classification_Type_TYPE_NO_STOPPING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNoParking, TrafficSign_MainSign_Classification_Type_TYPE_NO_PARKING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNoParkingZoneBegin,TrafficSign_MainSign_Classification_Type_TYPE_NO_PARKING_ZONE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNoParkingZoneEnd,TrafficSign_MainSign_Classification_Type_TYPE_NO_PARKING_ZONE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRightOfWayNextIntersection,TrafficSign_MainSign_Classification_Type_TYPE_RIGHT_OF_WAY_NEXT_INTERSECTION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRightOfWayBegin,TrafficSign_MainSign_Classification_Type_TYPE_RIGHT_OF_WAY_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRightOfWayEnd, TrafficSign_MainSign_Classification_Type_TYPE_RIGHT_OF_WAY_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPriorityOverOppositeDirection,TrafficSign_MainSign_Classification_Type_TYPE_PRIORITY_OVER_OPPOSITE_DIRECTION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPriorityOverOppositeDirectionUpsideDown,TrafficSign_MainSign_Classification_Type_TYPE_PRIORITY_OVER_OPPOSITE_DIRECTION_UPSIDE_DOWN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTownBegin, TrafficSign_MainSign_Classification_Type_TYPE_TOWN_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTownEnd, TrafficSign_MainSign_Classification_Type_TYPE_TOWN_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCarParking, TrafficSign_MainSign_Classification_Type_TYPE_CAR_PARKING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCarParkingZoneBegin,TrafficSign_MainSign_Classification_Type_TYPE_CAR_PARKING_ZONE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCarParkingZoneEnd,TrafficSign_MainSign_Classification_Type_TYPE_CAR_PARKING_ZONE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkHalfParkingLeft,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_HALF_PARKING_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkHalfParkingRight,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_HALF_PARKING_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkParkingLeft,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_PARKING_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkParkingRight,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_PARKING_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkPerpendicularHalfParkingLeft,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_PERPENDICULAR_HALF_PARKING_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkPerpendicularHalfParkingRight,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_PERPENDICULAR_HALF_PARKING_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkPerpendicularParkingLeft,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_PERPENDICULAR_PARKING_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSidewalkPerpendicularParkingRight,TrafficSign_MainSign_Classification_Type_TYPE_SIDEWALK_PERPENDICULAR_PARKING_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kLivingStreetBegin,TrafficSign_MainSign_Classification_Type_TYPE_LIVING_STREET_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kLivingStreetEnd,TrafficSign_MainSign_Classification_Type_TYPE_LIVING_STREET_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTunnel, TrafficSign_MainSign_Classification_Type_TYPE_TUNNEL),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kEmergencyStoppingLeft,TrafficSign_MainSign_Classification_Type_TYPE_EMERGENCY_STOPPING_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kEmergencyStoppingRight,TrafficSign_MainSign_Classification_Type_TYPE_EMERGENCY_STOPPING_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayBegin, TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayEnd, TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kExpresswayBegin,TrafficSign_MainSign_Classification_Type_TYPE_EXPRESSWAY_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kExpresswayEnd, TrafficSign_MainSign_Classification_Type_TYPE_EXPRESSWAY_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNamedHighwayExit,TrafficSign_MainSign_Classification_Type_TYPE_NAMED_HIGHWAY_EXIT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNamedExpresswayExit,TrafficSign_MainSign_Classification_Type_TYPE_NAMED_EXPRESSWAY_EXIT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNamedRoadExit, TrafficSign_MainSign_Classification_Type_TYPE_NAMED_ROAD_EXIT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayExit, TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_EXIT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kExpresswayExit, TrafficSign_MainSign_Classification_Type_TYPE_EXPRESSWAY_EXIT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOnewayStreet, TrafficSign_MainSign_Classification_Type_TYPE_ONEWAY_STREET),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCrossingGuards, TrafficSign_MainSign_Classification_Type_TYPE_CROSSING_GUARDS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDeadend, TrafficSign_MainSign_Classification_Type_TYPE_DEADEND),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDeadendExcludingDesignatedActors,TrafficSign_MainSign_Classification_Type_TYPE_DEADEND_EXCLUDING_DESIGNATED_ACTORS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kFirstAidStation,TrafficSign_MainSign_Classification_Type_TYPE_FIRST_AID_STATION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPoliceStation, TrafficSign_MainSign_Classification_Type_TYPE_POLICE_STATION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTelephone, TrafficSign_MainSign_Classification_Type_TYPE_TELEPHONE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kFillingStation, TrafficSign_MainSign_Classification_Type_TYPE_FILLING_STATION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHotel, TrafficSign_MainSign_Classification_Type_TYPE_HOTEL),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kInn, TrafficSign_MainSign_Classification_Type_TYPE_INN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kKiosk, TrafficSign_MainSign_Classification_Type_TYPE_KIOSK),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kToilet, TrafficSign_MainSign_Classification_Type_TYPE_TOILET),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kChapel, TrafficSign_MainSign_Classification_Type_TYPE_CHAPEL),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTouristInfo, TrafficSign_MainSign_Classification_Type_TYPE_TOURIST_INFO),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRepairService, TrafficSign_MainSign_Classification_Type_TYPE_REPAIR_SERVICE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPedestrianUnderpass,TrafficSign_MainSign_Classification_Type_TYPE_PEDESTRIAN_UNDERPASS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPedestrianBridge,TrafficSign_MainSign_Classification_Type_TYPE_PEDESTRIAN_BRIDGE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCamperPlace, TrafficSign_MainSign_Classification_Type_TYPE_CAMPER_PLACE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAdvisorySpeedLimitBegin,TrafficSign_MainSign_Classification_Type_TYPE_ADVISORY_SPEED_LIMIT_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAdvisorySpeedLimitEnd,TrafficSign_MainSign_Classification_Type_TYPE_ADVISORY_SPEED_LIMIT_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPlaceName, TrafficSign_MainSign_Classification_Type_TYPE_PLACE_NAME),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTouristAttraction,TrafficSign_MainSign_Classification_Type_TYPE_TOURIST_ATTRACTION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTouristRoute, TrafficSign_MainSign_Classification_Type_TYPE_TOURIST_ROUTE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTouristArea, TrafficSign_MainSign_Classification_Type_TYPE_TOURIST_AREA),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kShoulderNotPassableMotorVehicles,TrafficSign_MainSign_Classification_Type_TYPE_SHOULDER_NOT_PASSABLE_MOTOR_VEHICLES),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kShoulderUnsafeTrucksTractors,TrafficSign_MainSign_Classification_Type_TYPE_SHOULDER_UNSAFE_TRUCKS_TRACTORS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTollBegin, TrafficSign_MainSign_Classification_Type_TYPE_TOLL_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTollEnd, TrafficSign_MainSign_Classification_Type_TYPE_TOLL_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTollRoad, TrafficSign_MainSign_Classification_Type_TYPE_TOLL_ROAD),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kCustoms, TrafficSign_MainSign_Classification_Type_TYPE_CUSTOMS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kInternationalBorderInfo,TrafficSign_MainSign_Classification_Type_TYPE_INTERNATIONAL_BORDER_INFO),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kStreetlightRedBand,TrafficSign_MainSign_Classification_Type_TYPE_STREETLIGHT_RED_BAND),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kFederalHighwayRouteNumber,TrafficSign_MainSign_Classification_Type_TYPE_FEDERAL_HIGHWAY_ROUTE_NUMBER),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayRouteNumber,TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_ROUTE_NUMBER),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayInterchangeNumber,TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_INTERCHANGE_NUMBER),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kEuropeanRouteNumber,TrafficSign_MainSign_Classification_Type_TYPE_EUROPEAN_ROUTE_NUMBER),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kFederalHighwayDirectionLeft,TrafficSign_MainSign_Classification_Type_TYPE_FEDERAL_HIGHWAY_DIRECTION_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kFederalHighwayDirectionRight,TrafficSign_MainSign_Classification_Type_TYPE_FEDERAL_HIGHWAY_DIRECTION_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrimaryRoadDirectionLeft,TrafficSign_MainSign_Classification_Type_TYPE_PRIMARY_ROAD_DIRECTION_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPrimaryRoadDirectionRight,TrafficSign_MainSign_Classification_Type_TYPE_PRIMARY_ROAD_DIRECTION_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSecondaryRoadDirectionLeft,TrafficSign_MainSign_Classification_Type_TYPE_SECONDARY_ROAD_DIRECTION_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kSecondaryRoadDirectionRight,TrafficSign_MainSign_Classification_Type_TYPE_SECONDARY_ROAD_DIRECTION_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionDesignatedActorsLeft,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_DESIGNATED_ACTORS_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionDesignatedActorsRight,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_DESIGNATED_ACTORS_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRoutingDesignatedActors,TrafficSign_MainSign_Classification_Type_TYPE_ROUTING_DESIGNATED_ACTORS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionToHighwayLeft,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_TO_HIGHWAY_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionToHighwayRight,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_TO_HIGHWAY_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionToLocalDestinationLeft,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_TO_LOCAL_DESTINATION_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionToLocalDestinationRight,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_TO_LOCAL_DESTINATION_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kConsolidatedDirections,TrafficSign_MainSign_Classification_Type_TYPE_CONSOLIDATED_DIRECTIONS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kStreetName, TrafficSign_MainSign_Classification_Type_TYPE_STREET_NAME),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionPreannouncement,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_PREANNOUNCEMENT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionPreannouncementLaneConfig,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_PREANNOUNCEMENT_LANE_CONFIG),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionPreannouncementHighwayEntries,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTION_PREANNOUNCEMENT_HIGHWAY_ENTRIES),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayAnnouncement,TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_ANNOUNCEMENT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOtherRoadAnnouncement,TrafficSign_MainSign_Classification_Type_TYPE_OTHER_ROAD_ANNOUNCEMENT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayAnnouncementTruckStop,TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_ANNOUNCEMENT_TRUCK_STOP),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayPreannouncementDirections,TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_PREANNOUNCEMENT_DIRECTIONS),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPoleExit, TrafficSign_MainSign_Classification_Type_TYPE_POLE_EXIT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kHighwayDistanceBoard,TrafficSign_MainSign_Classification_Type_TYPE_HIGHWAY_DISTANCE_BOARD),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDetourLeft, TrafficSign_MainSign_Classification_Type_TYPE_DETOUR_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDetourRight, TrafficSign_MainSign_Classification_Type_TYPE_DETOUR_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kNumberedDetour, TrafficSign_MainSign_Classification_Type_TYPE_NUMBERED_DETOUR),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDetourBegin, TrafficSign_MainSign_Classification_Type_TYPE_DETOUR_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDetourEnd, TrafficSign_MainSign_Classification_Type_TYPE_DETOUR_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDetourRoutingBoard,TrafficSign_MainSign_Classification_Type_TYPE_DETOUR_ROUTING_BOARD),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOptionalDetour, TrafficSign_MainSign_Classification_Type_TYPE_OPTIONAL_DETOUR),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kOptionalDetourRouting,TrafficSign_MainSign_Classification_Type_TYPE_OPTIONAL_DETOUR_ROUTING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRouteRecommendation,TrafficSign_MainSign_Classification_Type_TYPE_ROUTE_RECOMMENDATION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kRouteRecommendationEnd,TrafficSign_MainSign_Classification_Type_TYPE_ROUTE_RECOMMENDATION_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAnnounceLaneTransitionLeft,TrafficSign_MainSign_Classification_Type_TYPE_ANNOUNCE_LANE_TRANSITION_LEFT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAnnounceLaneTransitionRight,TrafficSign_MainSign_Classification_Type_TYPE_ANNOUNCE_LANE_TRANSITION_RIGHT),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAnnounceRightLaneEnd,TrafficSign_MainSign_Classification_Type_TYPE_ANNOUNCE_RIGHT_LANE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAnnounceLeftLaneEnd,TrafficSign_MainSign_Classification_Type_TYPE_ANNOUNCE_LEFT_LANE_END),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAnnounceRightLaneBegin,TrafficSign_MainSign_Classification_Type_TYPE_ANNOUNCE_RIGHT_LANE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAnnounceLeftLaneBegin,TrafficSign_MainSign_Classification_Type_TYPE_ANNOUNCE_LEFT_LANE_BEGIN),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kAnnounceLaneConsolidation,TrafficSign_MainSign_Classification_Type_TYPE_ANNOUNCE_LANE_CONSOLIDATION),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDetourCityBlock,TrafficSign_MainSign_Classification_Type_TYPE_DETOUR_CITY_BLOCK),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kGate, TrafficSign_MainSign_Classification_Type_TYPE_GATE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kPoleWarning, TrafficSign_MainSign_Classification_Type_TYPE_POLE_WARNING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTrafficCone, TrafficSign_MainSign_Classification_Type_TYPE_TRAFFIC_CONE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kMobileLaneClosure,TrafficSign_MainSign_Classification_Type_TYPE_MOBILE_LANE_CLOSURE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kReflectorPost, TrafficSign_MainSign_Classification_Type_TYPE_REFLECTOR_POST),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kDirectionalBoardWarning,TrafficSign_MainSign_Classification_Type_TYPE_DIRECTIONAL_BOARD_WARNING),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kGuidingPlate, TrafficSign_MainSign_Classification_Type_TYPE_GUIDING_PLATE),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kGuidingPlateWedges,TrafficSign_MainSign_Classification_Type_TYPE_GUIDING_PLATE_WEDGES),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kParkingHazard, TrafficSign_MainSign_Classification_Type_TYPE_PARKING_HAZARD),
        CREATE_TRAFFIC_SIGN_TYPE_TUPLE(kTrafficLightGreenArrow,TrafficSign_MainSign_Classification_Type_TYPE_TRAFFIC_LIGHT_GREEN_ARROW)));

// clang-format on

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class SupplementarySignValueTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<std::tuple<osi::OsiTrafficSignValueUnit, astas_osi3::TrafficSignValue_Unit>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::SignValueInformation value_information;
        value_information.text = "foo";
        value_information.value = 120.0;
        value_information.value_unit = std::get<0>(GetParam());

        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.value_information.push_back(std::move(value_information));

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_P(SupplementarySignValueTestParameterized,
       GivenSupplementarySignWithSpecificValue_WhenStep_ThenExpectedTrafficSignHasRightValue)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    const auto& actual_sign_value = gt_.traffic_sign(0).supplementary_sign(0).classification().value();

    EXPECT_EQ(actual_sign_value[0].text(), "foo");
    EXPECT_EQ(actual_sign_value[0].value(), 120.0);
    EXPECT_EQ(actual_sign_value[0].value_unit(), std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(SupplementarySignValueTest,
                         SupplementarySignValueTestParameterized,
                         testing::ValuesIn(GetTrafficSignValueUnitTuples()));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class SupplementarySignTypeTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<
          std::tuple<map::OsiSupplementarySignType, astas_osi3::TrafficSign_SupplementarySign_Classification_Type>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.type = std::get<0>(GetParam());

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_P(SupplementarySignTypeTestParameterized,
       GivenSupplementarySignWithSpecificType_WhenStep_ThenExpectedTrafficSignHasRightSubType)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    EXPECT_EQ(gt_.traffic_sign(0).supplementary_sign(0).classification().type(), std::get<1>(GetParam()));
}

// clang-format off

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(custom_type, osi_type) \
    std::make_tuple(map::OsiSupplementarySignType::custom_type,             \
                    astas_osi3::TrafficSign_SupplementarySign_Classification_Type::osi_type)

INSTANTIATE_TEST_SUITE_P(
    SupplementarySignTypeTest,
    SupplementarySignTypeTestParameterized,
    testing::Values(
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kUnknown, TrafficSign_SupplementarySign_Classification_Type_TYPE_UNKNOWN),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kOther,  TrafficSign_SupplementarySign_Classification_Type_TYPE_OTHER),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kNoSign, TrafficSign_SupplementarySign_Classification_Type_TYPE_NO_SIGN),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kText, TrafficSign_SupplementarySign_Classification_Type_TYPE_TEXT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kSpace, TrafficSign_SupplementarySign_Classification_Type_TYPE_SPACE),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kTime, TrafficSign_SupplementarySign_Classification_Type_TYPE_TIME),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kArrow, TrafficSign_SupplementarySign_Classification_Type_TYPE_ARROW),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kConstrainedTo, TrafficSign_SupplementarySign_Classification_Type_TYPE_CONSTRAINED_TO),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kExcept, TrafficSign_SupplementarySign_Classification_Type_TYPE_EXCEPT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kValidForDistance, TrafficSign_SupplementarySign_Classification_Type_TYPE_VALID_FOR_DISTANCE),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadBottomLeftFourWay, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_LEFT_FOUR_WAY),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadTopLeftFourWay, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_LEFT_FOUR_WAY),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadBottomLeftThreeWayStraight, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_LEFT_THREE_WAY_STRAIGHT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadBottomLeftThreeWaySideways, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_LEFT_THREE_WAY_SIDEWAYS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadTopLeftThreeWayStraight, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_LEFT_THREE_WAY_STRAIGHT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadBottomRightFourWay, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_RIGHT_FOUR_WAY),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadTopRightFourWay, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_RIGHT_FOUR_WAY),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadBottomRightThreeWayStraight, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_RIGHT_THREE_WAY_STRAIGHT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadBottomRightThreeWaySideway, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_BOTTOM_RIGHT_THREE_WAY_SIDEWAY),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kPriorityRoadTopRightThreeWayStraight, TrafficSign_SupplementarySign_Classification_Type_TYPE_PRIORITY_ROAD_TOP_RIGHT_THREE_WAY_STRAIGHT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kValidInDistance, TrafficSign_SupplementarySign_Classification_Type_TYPE_VALID_IN_DISTANCE),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kStopIn, TrafficSign_SupplementarySign_Classification_Type_TYPE_STOP_IN),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kLeftArrow, TrafficSign_SupplementarySign_Classification_Type_TYPE_LEFT_ARROW),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kLeftBendArrow, TrafficSign_SupplementarySign_Classification_Type_TYPE_LEFT_BEND_ARROW),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kRightArrow, TrafficSign_SupplementarySign_Classification_Type_TYPE_RIGHT_ARROW),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kRightBendArrow, TrafficSign_SupplementarySign_Classification_Type_TYPE_RIGHT_BEND_ARROW),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kAccident, TrafficSign_SupplementarySign_Classification_Type_TYPE_ACCIDENT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kSnow, TrafficSign_SupplementarySign_Classification_Type_TYPE_SNOW),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kFog, TrafficSign_SupplementarySign_Classification_Type_TYPE_FOG),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kRollingHighwayInformation, TrafficSign_SupplementarySign_Classification_Type_TYPE_ROLLING_HIGHWAY_INFORMATION),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kServices, TrafficSign_SupplementarySign_Classification_Type_TYPE_SERVICES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kTimeRange, TrafficSign_SupplementarySign_Classification_Type_TYPE_TIME_RANGE),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kParkingDiscTimeRestriction, TrafficSign_SupplementarySign_Classification_Type_TYPE_PARKING_DISC_TIME_RESTRICTION),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kWeight, TrafficSign_SupplementarySign_Classification_Type_TYPE_WEIGHT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kWet, TrafficSign_SupplementarySign_Classification_Type_TYPE_WET),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kParkingConstraint, TrafficSign_SupplementarySign_Classification_Type_TYPE_PARKING_CONSTRAINT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kNoWaitingSideStripes, TrafficSign_SupplementarySign_Classification_Type_TYPE_NO_WAITING_SIDE_STRIPES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kRain, TrafficSign_SupplementarySign_Classification_Type_TYPE_RAIN),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kSnowRain, TrafficSign_SupplementarySign_Classification_Type_TYPE_SNOW_RAIN),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kNight, TrafficSign_SupplementarySign_Classification_Type_TYPE_NIGHT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kStop4Way, TrafficSign_SupplementarySign_Classification_Type_TYPE_STOP_4_WAY),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kTruck, TrafficSign_SupplementarySign_Classification_Type_TYPE_TRUCK),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kTractorsMayBePassed, TrafficSign_SupplementarySign_Classification_Type_TYPE_TRACTORS_MAY_BE_PASSED),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kHazardous, TrafficSign_SupplementarySign_Classification_Type_TYPE_HAZARDOUS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kTrailer, TrafficSign_SupplementarySign_Classification_Type_TYPE_TRAILER),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kZone, TrafficSign_SupplementarySign_Classification_Type_TYPE_ZONE),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kMotorcycle, TrafficSign_SupplementarySign_Classification_Type_TYPE_MOTORCYCLE),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kMotorcycleAllowed, TrafficSign_SupplementarySign_Classification_Type_TYPE_MOTORCYCLE_ALLOWED),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_TYPE_TUPLE(kCar, TrafficSign_SupplementarySign_Classification_Type_TYPE_CAR)));

// clang-format on
class SupplementarySignMultipleValuesFixture : public TrafficSignBaseTest
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.value_information.push_back(
            {"foo", 100.0, osi::OsiTrafficSignValueUnit::kKilometerPerHour});
        supplementary_traffic_sign.value_information.push_back({"bar", 30.0, osi::OsiTrafficSignValueUnit::kMinutes});

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_F(SupplementarySignMultipleValuesFixture,
       GivenSupplementarySignWithMultipleValues_WhenStep_ThenExpectAllValuesReturnedInCorrectOrder)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    const auto& actual_sign_value = gt_.traffic_sign(0).supplementary_sign(0).classification().value();

    EXPECT_EQ(actual_sign_value[0].text(), "foo");
    EXPECT_EQ(actual_sign_value[0].value(), 100.0);
    EXPECT_EQ(actual_sign_value[0].value_unit(),
              astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER_PER_HOUR);

    EXPECT_EQ(actual_sign_value[1].text(), "bar");
    EXPECT_EQ(actual_sign_value[1].value(), 30.0);
    EXPECT_EQ(actual_sign_value[1].value_unit(), astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MINUTES);
}

class MultipleSupplementarySignsFixture : public TrafficSignBaseTest
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign first_supplementary_traffic_sign;
        first_supplementary_traffic_sign.value_information.push_back(
            {"foo", 100.0, osi::OsiTrafficSignValueUnit::kKilometerPerHour});

        map::MountedSign::SupplementarySign second_supplementary_traffic_sign;
        second_supplementary_traffic_sign.value_information.push_back(
            {"bar", 30.0, osi::OsiTrafficSignValueUnit::kMinutes});

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        first_supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        second_supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(first_supplementary_traffic_sign));
        main_traffic_sign->supplementary_signs.push_back(std::move(second_supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_F(MultipleSupplementarySignsFixture,
       GivenMultipleSupplementarySigns_WhenStep_ThenExpectAllSignsReturnedInCorrectOrder)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_EQ(gt_.traffic_sign(0).supplementary_sign_size(), 2);

    {
        const auto& actual_first_sign_value = gt_.traffic_sign(0).supplementary_sign(0).classification().value();

        EXPECT_EQ(actual_first_sign_value[0].text(), "foo");
        EXPECT_EQ(actual_first_sign_value[0].value(), 100.0);
        EXPECT_EQ(actual_first_sign_value[0].value_unit(),
                  astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER_PER_HOUR);
    }

    {
        const auto& actual_second_sign_value = gt_.traffic_sign(0).supplementary_sign(1).classification().value();

        EXPECT_EQ(actual_second_sign_value[0].text(), "bar");
        EXPECT_EQ(actual_second_sign_value[0].value(), 30.0);
        EXPECT_EQ(actual_second_sign_value[0].value_unit(),
                  astas_osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MINUTES);
    }
}

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficSignPoseTestParameterized : public TrafficSignBaseTest,
                                         public testing::WithParamInterface<mantle_api::Pose>
{
  protected:
    void AddObjectsToMap() override
    {
        auto traffic_sign = std::make_shared<map::MountedSign>();
        traffic_sign->pose = GetParam();
        traffic_sign->id = GetNewId();
        astas_map_->traffic_signs.push_back(traffic_sign);
    }

    void AddEntities() override
    {
        auto traffic_sign_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign");
        traffic_sign_entity->SetPosition(GetParam().position);
        traffic_sign_entity->SetOrientation(GetParam().orientation);

        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        traffic_sign_entity->SetProperties(std::move(traffic_sign_properties));

        entities_.push_back(std::move(traffic_sign_entity));
    }
};

TEST_P(TrafficSignPoseTestParameterized,
       GivenTrafficSignWithSpecificPose_WhenStep_ThenTrafficSignHasCorrectPositionOrientation)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    const auto& actual_map_sign_position = gt_.traffic_sign(0).main_sign().base().position();
    const auto& actual_map_sign_orientation = gt_.traffic_sign(0).main_sign().base().orientation();

    const mantle_api::Pose ref_pose(GetParam());

    EXPECT_EQ(actual_map_sign_position.x(), ref_pose.position.x.value());
    EXPECT_EQ(actual_map_sign_position.y(), ref_pose.position.y.value());
    EXPECT_EQ(actual_map_sign_position.z(), ref_pose.position.z.value());

    EXPECT_EQ(actual_map_sign_orientation.pitch(), ref_pose.orientation.pitch.value());
    EXPECT_EQ(actual_map_sign_orientation.roll(), ref_pose.orientation.roll.value());
    EXPECT_EQ(actual_map_sign_orientation.yaw(), ref_pose.orientation.yaw.value());

    const auto& actual_entity_sign_position = gt_.traffic_sign(1).main_sign().base().position();
    const auto& actual_entity_sign_orientation = gt_.traffic_sign(1).main_sign().base().orientation();

    EXPECT_EQ(actual_entity_sign_position.x(), ref_pose.position.x.value());
    EXPECT_EQ(actual_entity_sign_position.y(), ref_pose.position.y.value());
    EXPECT_EQ(actual_entity_sign_position.z(), ref_pose.position.z.value());

    EXPECT_EQ(actual_entity_sign_orientation.pitch(), ref_pose.orientation.pitch.value());
    EXPECT_EQ(actual_entity_sign_orientation.roll(), ref_pose.orientation.roll.value());
    EXPECT_EQ(actual_entity_sign_orientation.yaw(), ref_pose.orientation.yaw.value());
}

INSTANTIATE_TEST_SUITE_P(TrafficSignPoseTest,
                         TrafficSignPoseTestParameterized,
                         testing::Values(mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {1.0_rad, 2.0_rad, 3.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {90.0_deg, 30.0_deg, 180_deg}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {90.0_deg, 30.0_deg, 180_deg}}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class SupplementarySignPoseTestParameterized : public TrafficSignBaseTest,
                                               public testing::WithParamInterface<mantle_api::Pose>
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.pose = GetParam();

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_P(SupplementarySignPoseTestParameterized,
       GivenSupplementarySignWithSpecificPose_WhenStep_ThenTrafficSignHasCorrectPositionOrientation)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    const auto& actual_sign_position = gt_.traffic_sign(0).supplementary_sign(0).base().position();
    const auto& actual_sign_orientation = gt_.traffic_sign(0).supplementary_sign(0).base().orientation();

    const mantle_api::Pose ref_pose(GetParam());

    EXPECT_EQ(actual_sign_position.x(), ref_pose.position.x.value());
    EXPECT_EQ(actual_sign_position.y(), ref_pose.position.y.value());
    EXPECT_EQ(actual_sign_position.z(), ref_pose.position.z.value());

    EXPECT_EQ(actual_sign_orientation.pitch(), ref_pose.orientation.pitch.value());
    EXPECT_EQ(actual_sign_orientation.roll(), ref_pose.orientation.roll.value());
    EXPECT_EQ(actual_sign_orientation.yaw(), ref_pose.orientation.yaw.value());
}

INSTANTIATE_TEST_SUITE_P(SupplementarySignPoseTest,
                         SupplementarySignPoseTestParameterized,
                         testing::Values(mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {1.0_rad, 2.0_rad, 3.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {90.0_deg, 30.0_deg, 180_deg}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {90.0_deg, 30.0_deg, 180_deg}}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficSignDimensionsTestParameterized : public TrafficSignBaseTest,
                                               public testing::WithParamInterface<mantle_api::Dimension3>
{
  protected:
    void AddObjectsToMap() override
    {
        auto traffic_sign = std::make_shared<map::MountedSign>();
        traffic_sign->dimensions = GetParam();
        traffic_sign->id = GetNewId();
        astas_map_->traffic_signs.push_back(traffic_sign);
    }

    void AddEntities() override
    {
        auto traffic_sign_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign");

        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        traffic_sign_properties->bounding_box.dimension = GetParam();
        traffic_sign_entity->SetProperties(std::move(traffic_sign_properties));

        entities_.push_back(std::move(traffic_sign_entity));
    }
};

TEST_P(TrafficSignDimensionsTestParameterized,
       GivenTrafficSignWithSpecificDimensions_WhenStep_ThenTrafficSignHasCorrectDimensions)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    const auto& actual_map_sign_dimension = gt_.traffic_sign(0).main_sign().base().dimension();
    const auto& actual_entity_sign_dimension = gt_.traffic_sign(1).main_sign().base().dimension();

    const mantle_api::Dimension3 ref_dimension(GetParam());

    EXPECT_EQ(actual_map_sign_dimension.length(), ref_dimension.length.value());
    EXPECT_EQ(actual_map_sign_dimension.width(), ref_dimension.width.value());
    EXPECT_EQ(actual_map_sign_dimension.height(), ref_dimension.height.value());

    EXPECT_EQ(actual_entity_sign_dimension.length(), ref_dimension.length.value());
    EXPECT_EQ(actual_entity_sign_dimension.width(), ref_dimension.width.value());
    EXPECT_EQ(actual_entity_sign_dimension.height(), ref_dimension.height.value());
}

INSTANTIATE_TEST_SUITE_P(TrafficSignDimensionsTest,
                         TrafficSignDimensionsTestParameterized,
                         testing::Values(mantle_api::Dimension3{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Dimension3{0.1_m, 0.02_m, 0.3_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class SupplementarySignDimensionsTestParameterized : public TrafficSignBaseTest,
                                                     public testing::WithParamInterface<mantle_api::Dimension3>
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.dimensions = GetParam();

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_P(SupplementarySignDimensionsTestParameterized,
       GivenSupplementarySignWithSpecificDimensions_WhenStep_ThenSignHasCorrectDimensions)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    const auto& actual_sign_dimension = gt_.traffic_sign(0).supplementary_sign(0).base().dimension();

    const mantle_api::Dimension3 ref_dimension(GetParam());

    EXPECT_EQ(actual_sign_dimension.length(), ref_dimension.length.value());
    EXPECT_EQ(actual_sign_dimension.width(), ref_dimension.width.value());
    EXPECT_EQ(actual_sign_dimension.height(), ref_dimension.height.value());
}

INSTANTIATE_TEST_SUITE_P(SupplementarySignDimensionsTest,
                         SupplementarySignDimensionsTestParameterized,
                         testing::Values(mantle_api::Dimension3{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Dimension3{0.1_m, 0.02_m, 0.3_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficSignVariabilityTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<
          std::tuple<osi::OsiTrafficSignVariability, astas_osi3::TrafficSign_Variability>>
{
  protected:
    void AddObjectsToMap() override
    {
        auto traffic_sign = std::make_shared<map::MountedSign>();
        traffic_sign->variability = std::get<0>(GetParam());
        traffic_sign->id = GetNewId();
        astas_map_->traffic_signs.push_back(traffic_sign);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign"));
        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        traffic_sign_properties->variability = std::get<0>(GetParam());
        entities_.back()->SetProperties(std::move(traffic_sign_properties));
    }
};

TEST_P(TrafficSignVariabilityTestParameterized,
       GivenTrafficSignWithVariability_WhenStep_ThenTrafficSignHasCorrectVariability)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    const auto& actual_map_sign_variability = gt_.traffic_sign(0).main_sign().classification().variability();
    const auto& actual_entity_sign_variability = gt_.traffic_sign(0).main_sign().classification().variability();

    EXPECT_EQ(actual_map_sign_variability, std::get<1>(GetParam()));
    EXPECT_EQ(actual_entity_sign_variability, std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(
    TrafficSignVariabilityTest,
    TrafficSignVariabilityTestParameterized,
    testing::Values(
        std::make_tuple(osi::OsiTrafficSignVariability::kUnknown,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignVariability::kOther,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_OTHER),
        std::make_tuple(osi::OsiTrafficSignVariability::kFixed,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_FIXED),
        std::make_tuple(osi::OsiTrafficSignVariability::kVariable,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_VARIABLE)));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class SupplementarySignVariabilityTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<
          std::tuple<osi::OsiTrafficSignVariability, astas_osi3::TrafficSign_Variability>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.variability = std::get<0>(GetParam());

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_P(SupplementarySignVariabilityTestParameterized,
       GivenSupplementarySignWithVariability_WhenStep_ThenTrafficSignHasCorrectVariability)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    const auto& actual_sign_variability = gt_.traffic_sign(0).supplementary_sign(0).classification().variability();

    EXPECT_EQ(actual_sign_variability, std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(
    SupplementarySignVariabilityTest,
    SupplementarySignVariabilityTestParameterized,
    testing::Values(
        std::make_tuple(osi::OsiTrafficSignVariability::kUnknown,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignVariability::kOther,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_OTHER),
        std::make_tuple(osi::OsiTrafficSignVariability::kFixed,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_FIXED),
        std::make_tuple(osi::OsiTrafficSignVariability::kVariable,
                        astas_osi3::TrafficSign_Variability::TrafficSign_Variability_VARIABILITY_VARIABLE)));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class TrafficSignDirectionScopeTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<
          std::tuple<osi::OsiTrafficSignDirectionScope, astas_osi3::TrafficSign_MainSign_Classification_DirectionScope>>
{
  protected:
    void AddObjectsToMap() override
    {
        auto traffic_sign = std::make_shared<map::MountedSign>();
        traffic_sign->direction_scope = std::get<0>(GetParam());
        traffic_sign->id = GetNewId();
        astas_map_->traffic_signs.push_back(traffic_sign);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign"));
        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        traffic_sign_properties->direction = std::get<0>(GetParam());
        entities_.back()->SetProperties(std::move(traffic_sign_properties));
    }
};

TEST_P(TrafficSignDirectionScopeTestParameterized,
       GivenTrafficSignWithDirectionScope_WhenStep_ThenTrafficSignHasCorrectDirectionScope)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    const auto& actual_map_sign_direction_scope = gt_.traffic_sign(0).main_sign().classification().direction_scope();
    const auto& actual_entity_sign_direction_scope = gt_.traffic_sign(1).main_sign().classification().direction_scope();

    EXPECT_EQ(actual_map_sign_direction_scope, std::get<1>(GetParam()));
    EXPECT_EQ(actual_entity_sign_direction_scope, std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(
    TrafficSignDirectionScopeTest,
    TrafficSignDirectionScopeTestParameterized,
    testing::Values(
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kUnknown,
                        astas_osi3::TrafficSign_MainSign_Classification_DirectionScope::
                            TrafficSign_MainSign_Classification_DirectionScope_DIRECTION_SCOPE_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kOther,
                        astas_osi3::TrafficSign_MainSign_Classification_DirectionScope::
                            TrafficSign_MainSign_Classification_DirectionScope_DIRECTION_SCOPE_OTHER),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kNoDirection,
                        astas_osi3::TrafficSign_MainSign_Classification_DirectionScope::
                            TrafficSign_MainSign_Classification_DirectionScope_DIRECTION_SCOPE_NO_DIRECTION),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kLeft,
                        astas_osi3::TrafficSign_MainSign_Classification_DirectionScope::
                            TrafficSign_MainSign_Classification_DirectionScope_DIRECTION_SCOPE_LEFT),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kRight,
                        astas_osi3::TrafficSign_MainSign_Classification_DirectionScope::
                            TrafficSign_MainSign_Classification_DirectionScope_DIRECTION_SCOPE_RIGHT),
        std::make_tuple(osi::OsiTrafficSignDirectionScope::kLeftRight,
                        astas_osi3::TrafficSign_MainSign_Classification_DirectionScope::
                            TrafficSign_MainSign_Classification_DirectionScope_DIRECTION_SCOPE_LEFT_RIGHT)));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class MainSignAssignedLaneIdTestParameterized : public TrafficSignBaseTest,
                                                public testing::WithParamInterface<std::vector<mantle_api::UniqueId>>
{
  protected:
    void AddObjectsToMap() override
    {
        auto traffic_sign = std::make_shared<map::MountedSign>();
        traffic_sign->assigned_lanes = GetParam();
        traffic_sign->id = GetNewId();
        astas_map_->traffic_signs.push_back(std::move(traffic_sign));
    }

    void AddEntities() override
    {
        auto traffic_sign_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign");
        traffic_sign_entity->SetAssignedLaneIds(GetParam());

        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        traffic_sign_entity->SetProperties(std::move(traffic_sign_properties));

        entities_.push_back(std::move(traffic_sign_entity));
    }
};

TEST_P(MainSignAssignedLaneIdTestParameterized,
       GivenMainSignWithAssignedLanes_WhenStep_ThenExpectRoadMarkingHasCorrectAssignedLanes)
{
    ASSERT_TRUE(gt_.traffic_sign_size());

    const auto& actual_map_sign_classification = gt_.traffic_sign(0).main_sign().classification();
    const auto& actual_entity_sign_classification = gt_.traffic_sign(1).main_sign().classification();

    auto expected_lane_ids = GetParam();
    ASSERT_EQ(expected_lane_ids.size(), actual_map_sign_classification.assigned_lane_id_size());
    ASSERT_EQ(expected_lane_ids.size(), actual_entity_sign_classification.assigned_lane_id_size());

    for (int i = 0; i < expected_lane_ids.size(); i++)
    {
        EXPECT_EQ(expected_lane_ids[i], actual_map_sign_classification.assigned_lane_id(i).value());
        EXPECT_EQ(expected_lane_ids[i], actual_entity_sign_classification.assigned_lane_id(i).value());
    }
}

INSTANTIATE_TEST_SUITE_P(MainSignAssignedLaneIdTest,
                         MainSignAssignedLaneIdTestParameterized,
                         testing::Values(std::vector<mantle_api::UniqueId>{0, 1},
                                         std::vector<mantle_api::UniqueId>{1, 0},
                                         std::vector<mantle_api::UniqueId>{0},
                                         std::vector<mantle_api::UniqueId>{1}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class SupplementarySignActorsTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<std::tuple<map::OsiSupplementaryTrafficSignActor,
                                                    astas_osi3::TrafficSign_SupplementarySign_Classification_Actor>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.actors.push_back(std::get<0>(GetParam()));

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_P(SupplementarySignActorsTestParameterized,
       GivenSupplementarySignWithActors_WhenStep_ThenTrafficSignHasCorrectActors)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    const auto& actual_sign_actor = gt_.traffic_sign(0).supplementary_sign(0).classification().actor()[0];

    EXPECT_EQ(actual_sign_actor, std::get<1>(GetParam()));
}

// clang-format off

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(custom_type, osi_type) \
    std::make_tuple(map::OsiSupplementaryTrafficSignActor::custom_type,      \
                    astas_osi3::TrafficSign_SupplementarySign_Classification_Actor::osi_type)

INSTANTIATE_TEST_SUITE_P(
    SupplementarySignActorsTest,
    SupplementarySignActorsTestParameterized,
    testing::Values(
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kUnknown, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_UNKNOWN),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kOther, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_OTHER),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kNoActor, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_NO_ACTOR),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kAgriculturalVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_AGRICULTURAL_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kBicycles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_BICYCLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kBuses, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_BUSES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kCampers, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CAMPERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kCaravans, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARAVANS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kCars, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kCarsWithCaravans, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS_WITH_CARAVANS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kCarsWithTrailers, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS_WITH_TRAILERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kCattle, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CATTLE),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kChildren, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CHILDREN),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kConstructionVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CONSTRUCTION_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kDeliveryVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_DELIVERY_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kDisabledPersons, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_DISABLED_PERSONS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kEbikes, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_EBIKES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kElectricVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_ELECTRIC_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kEmergencyVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_EMERGENCY_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kFerryUsers, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_FERRY_USERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kForestryVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_FORESTRY_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kHazardousGoodsVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_HAZARDOUS_GOODS_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kHorseCarriages, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_HORSE_CARRIAGES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kHorseRiders, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_HORSE_RIDERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kInlineSkaters, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_INLINE_SKATERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kMedicalVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MEDICAL_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kMilitaryVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MILITARY_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kMopeds, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MOPEDS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kMotorcycles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MOTORCYCLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kMotorizedMultitrackVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_MOTORIZED_MULTITRACK_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kOperationalAndUtilityVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_OPERATIONAL_AND_UTILITY_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kPedestrians, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_PEDESTRIANS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kPublicTransportVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_PUBLIC_TRANSPORT_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kRailroadTraffic, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_RAILROAD_TRAFFIC),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kResidents, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_RESIDENTS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kSlurryTransport, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_SLURRY_TRANSPORT),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kTaxis, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TAXIS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kTractors, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRACTORS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kTrailers, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRAILERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kTrams, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRAMS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kTrucks, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kTrucksWithSemitrailers, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS_WITH_SEMITRAILERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kTrucksWithTrailers, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS_WITH_TRAILERS),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kVehiclesWithGreenBadges, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_VEHICLES_WITH_GREEN_BADGES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kVehiclesWithRedBadges, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_VEHICLES_WITH_RED_BADGES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kVehiclesWithYellowBadges, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_VEHICLES_WITH_YELLOW_BADGES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kWaterPollutantVehicles, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_WATER_POLLUTANT_VEHICLES),
        CREATE_TRAFFIC_SIGN_SUPPLEMENTARY_ACTOR_TUPLE(kWinterSportspeople, TrafficSign_SupplementarySign_Classification_Actor_ACTOR_WINTER_SPORTSPEOPLE)));

// clang-format on

class SupplementarySignMultipleActorsFixture : public TrafficSignBaseTest
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;
        supplementary_traffic_sign.actors = {map::OsiSupplementaryTrafficSignActor::kCars,
                                             map::OsiSupplementaryTrafficSignActor::kChildren};

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_F(SupplementarySignMultipleActorsFixture,
       GivenSupplementarySignWithMultipleActors_WhenStep_ThenExpectAllActorsReturnedInCorrectOrder)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    const auto& actual_sign_actors = gt_.traffic_sign(0).supplementary_sign(0).classification().actor();

    EXPECT_EQ(actual_sign_actors[0],
              astas_osi3::TrafficSign_SupplementarySign_Classification_Actor::
                  TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS);
    EXPECT_EQ(actual_sign_actors[1],
              astas_osi3::TrafficSign_SupplementarySign_Classification_Actor::
                  TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CHILDREN);
}

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class SupplementarySignAssignedLaneIdTestParameterized
    : public TrafficSignBaseTest,
      public testing::WithParamInterface<std::vector<mantle_api::UniqueId>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::MountedSign::SupplementarySign supplementary_traffic_sign;

        auto main_traffic_sign = std::make_shared<map::MountedSign>();
        supplementary_traffic_sign.main_sign = main_traffic_sign.get();
        main_traffic_sign->supplementary_signs.push_back(std::move(supplementary_traffic_sign));
        main_traffic_sign->assigned_lanes = GetParam();
        astas_map_->traffic_signs.push_back(std::move(main_traffic_sign));
    }
};

TEST_P(SupplementarySignAssignedLaneIdTestParameterized,
       GivenSupplementarySignWithAssignedLanes_WhenStep_ThenExpectRoadMarkingHasCorrectAssignedLanes)
{
    ASSERT_TRUE(gt_.traffic_sign_size());
    ASSERT_TRUE(gt_.traffic_sign(0).supplementary_sign_size());

    const auto& actual_sign_classification = gt_.traffic_sign(0).supplementary_sign(0).classification();

    auto expected_lane_ids = GetParam();
    ASSERT_EQ(expected_lane_ids.size(), actual_sign_classification.assigned_lane_id_size());

    for (int i = 0; i < expected_lane_ids.size(); i++)
    {
        EXPECT_EQ(expected_lane_ids[i], actual_sign_classification.assigned_lane_id(i).value());
    }
}

INSTANTIATE_TEST_SUITE_P(SupplementarySignAssignedLaneIdTest,
                         SupplementarySignAssignedLaneIdTestParameterized,
                         testing::Values(std::vector<mantle_api::UniqueId>{0, 1},
                                         std::vector<mantle_api::UniqueId>{1, 0},
                                         std::vector<mantle_api::UniqueId>{0},
                                         std::vector<mantle_api::UniqueId>{1}));
}  // namespace astas::environment::proto_groundtruth
