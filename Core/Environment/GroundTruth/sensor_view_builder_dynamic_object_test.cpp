/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_converter_utils.h"
#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter_utils.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/GroundTruth/sensor_view_builder_test_common.h"

#include <gtest/gtest.h>
#include <units.h>

using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;
using units::literals::operator""_deg;

namespace astas::environment::proto_groundtruth
{

class DynamicObjectTest : public BaseSensorViewBuilderObjectGenerationTest
{
  protected:
    void AddObjectsToMap() override
    {
        // Moving Objects cannot exist as part of the map file.
    }

    virtual void SetVehicle(environment::entities::VehicleEntity& vehicle) { std::ignore = vehicle; }

    virtual void SetVehicleProperties(mantle_api::VehicleProperties& vehicle_properties)
    {
        std::ignore = vehicle_properties;
    }

    virtual void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) { std::ignore = pedestrian; }

    virtual void SetPedestrianProperties(mantle_api::PedestrianProperties& pedestrian_properties)
    {
        std::ignore = pedestrian_properties;
    }

    void AddEntities() override
    {
        auto vehicle = std::make_unique<environment::entities::VehicleEntity>(GetNewId(), "vehicle");
        auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
        SetVehicle(*vehicle);
        SetVehicleProperties(*vehicle_properties);

        entities_.push_back(std::move(vehicle));
        entities_.back()->SetProperties(std::move(vehicle_properties));

        auto pedestrian = std::make_unique<environment::entities::PedestrianEntity>(GetNewId(), "pedestrian");
        auto pedestrian_properties = std::make_unique<mantle_api::PedestrianProperties>();
        SetPedestrian(*pedestrian);
        SetPedestrianProperties(*pedestrian_properties);

        entities_.push_back(std::move(pedestrian));
        entities_.back()->SetProperties(std::move(pedestrian_properties));
    }

    static constexpr int num_non_host_moving_objects = 2;
};

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectIdTestParameterized : public DynamicObjectTest,
                                         public testing::WithParamInterface<mantle_api::UniqueId>
{
  protected:
    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::VehicleEntity>(GetParam(), "vehicle"));
        entities_.back()->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

        entities_.push_back(std::make_unique<environment::entities::PedestrianEntity>(GetParam() + 1, "pedestrian"));
        entities_.back()->SetProperties(std::make_unique<mantle_api::PedestrianProperties>());
    }
};

TEST_P(DynamicObjectIdTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectIdHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    mantle_api::UniqueId expected_id = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    EXPECT_EQ(expected_id, ground_truth.moving_object(1).id().value());
    EXPECT_EQ(expected_id + 1, ground_truth.moving_object(2).id().value());
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectIdTest, DynamicObjectIdTestParameterized, testing::Values(1, 2, 3));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectPositionTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<mantle_api::Vec3<units::length::meter_t>>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetPosition(GetParam()); }

    void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) override
    {
        pedestrian.SetPosition(GetParam());
    }
};

TEST_P(DynamicObjectPositionTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectPositionHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    mantle_api::Vec3<units::length::meter_t> expected_position = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    for (int obj = 1; obj <= num_non_host_moving_objects; obj++)
    {
        auto& position = ground_truth.moving_object(obj).base().position();
        EXPECT_EQ(expected_position.x.value(), position.x());
        EXPECT_EQ(expected_position.y.value(), position.y());
        EXPECT_EQ(expected_position.z.value(), position.z());
    }
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectPositionTest,
                         DynamicObjectPositionTestParameterized,
                         testing::Values(mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 1.0_m},
                                         mantle_api::Vec3<units::length::meter_t>{-1.0_m, -1.0_m, -1.0_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectVelocityTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<mantle_api::Vec3<units::velocity::meters_per_second_t>>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetVelocity(GetParam()); }

    void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) override
    {
        pedestrian.SetVelocity(GetParam());
    }
};

TEST_P(DynamicObjectVelocityTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectVelocityHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    mantle_api::Vec3<units::velocity::meters_per_second_t> expected_velocity = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    for (int obj = 1; obj <= num_non_host_moving_objects; obj++)
    {
        auto& velocity = ground_truth.moving_object(obj).base().velocity();
        EXPECT_EQ(expected_velocity.x.value(), velocity.x());
        EXPECT_EQ(expected_velocity.y.value(), velocity.y());
        EXPECT_EQ(expected_velocity.z.value(), velocity.z());
    }
}

INSTANTIATE_TEST_SUITE_P(
    DynamicObjectVelocityTest,
    DynamicObjectVelocityTestParameterized,
    testing::Values(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 0.0_mps, 0.0_mps},
                    mantle_api::Vec3<units::velocity::meters_per_second_t>{1.0_mps, 1.0_mps, 1.0_mps},
                    mantle_api::Vec3<units::velocity::meters_per_second_t>{-1.0_mps, -1.0_mps, -1.0_mps}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectAccelerationTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetAcceleration(GetParam()); }

    void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) override
    {
        pedestrian.SetAcceleration(GetParam());
    }
};

TEST_P(DynamicObjectAccelerationTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectAccelerationHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> expected_acceleration = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    for (int obj = 1; obj <= num_non_host_moving_objects; obj++)
    {
        auto& acceleration = ground_truth.moving_object(obj).base().acceleration();
        EXPECT_EQ(expected_acceleration.x.value(), acceleration.x());
        EXPECT_EQ(expected_acceleration.y.value(), acceleration.y());
        EXPECT_EQ(expected_acceleration.z.value(), acceleration.z());
    }
}

INSTANTIATE_TEST_SUITE_P(
    DynamicObjectAccelerationTest,
    DynamicObjectAccelerationTestParameterized,
    testing::Values(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq},
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{1.0_mps_sq, 1.0_mps_sq, 1.0_mps_sq},
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{-1.0_mps_sq, -1.0_mps_sq, -1.0_mps_sq}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectDimensionTestParameterized : public DynamicObjectTest,
                                                public testing::WithParamInterface<mantle_api::Dimension3>
{
  protected:
    void SetVehicleProperties(mantle_api::VehicleProperties& vehicle_properties) override
    {
        vehicle_properties.bounding_box.dimension = GetParam();
    }

    void SetPedestrianProperties(mantle_api::PedestrianProperties& pedestrian_properties) override
    {
        pedestrian_properties.bounding_box.dimension = GetParam();
    }
};

TEST_P(DynamicObjectDimensionTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectDimensionHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_dimension = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    for (int obj = 1; obj <= num_non_host_moving_objects; obj++)
    {
        auto& dimension = ground_truth.moving_object(obj).base().dimension();
        EXPECT_EQ(expected_dimension.height.value(), dimension.height());
        EXPECT_EQ(expected_dimension.length.value(), dimension.length());
        EXPECT_EQ(expected_dimension.width.value(), dimension.width());
    }
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectDimensionTest,
                         DynamicObjectDimensionTestParameterized,
                         testing::Values(mantle_api::Dimension3{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Dimension3{0.1_m, 0.02_m, 0.3_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectOrientationTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<mantle_api::Orientation3<units::angle::radian_t>>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetOrientation(GetParam()); }

    void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) override
    {
        pedestrian.SetOrientation(GetParam());
    }
};

TEST_P(DynamicObjectOrientationTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectOrientationHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_orientation = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    for (int obj = 1; obj <= num_non_host_moving_objects; obj++)
    {
        auto& orientation = ground_truth.moving_object(obj).base().orientation();
        EXPECT_EQ(expected_orientation.pitch.value(), orientation.pitch());
        EXPECT_EQ(expected_orientation.roll.value(), orientation.roll());
        EXPECT_EQ(expected_orientation.yaw.value(), orientation.yaw());
    }
}

INSTANTIATE_TEST_SUITE_P(
    DynamicObjectOrientationTest,
    DynamicObjectOrientationTestParameterized,
    testing::Values(mantle_api::Orientation3<units::angle::radian_t>{0.0_rad, 0.0_rad, 0.0_rad},
                    mantle_api::Orientation3<units::angle::radian_t>{1.0_rad, 1.0_rad, 1.0_rad},
                    mantle_api::Orientation3<units::angle::radian_t>{-1.0_rad, -1.0_rad, -1.0_rad}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectOrientationRateTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetOrientationRate(GetParam()); }

    void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) override
    {
        pedestrian.SetOrientationRate(GetParam());
    }
};

TEST_P(DynamicObjectOrientationRateTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectOrientationRateHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_orientation_rate = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    for (int obj = 1; obj <= num_non_host_moving_objects; obj++)
    {
        auto& orientation_rate = ground_truth.moving_object(obj).base().orientation_rate();
        EXPECT_EQ(expected_orientation_rate.pitch.value(), orientation_rate.pitch());
        EXPECT_EQ(expected_orientation_rate.roll.value(), orientation_rate.roll());
        EXPECT_EQ(expected_orientation_rate.yaw.value(), orientation_rate.yaw());
    }
}

INSTANTIATE_TEST_SUITE_P(
    DynamicObjectOrientationRateTest,
    DynamicObjectOrientationRateTestParameterized,
    testing::Values(mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{0.0_rad_per_s,
                                                                                            0.0_rad_per_s,
                                                                                            0.0_rad_per_s},
                    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{1.0_rad_per_s,
                                                                                            1.0_rad_per_s,
                                                                                            1.0_rad_per_s},
                    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>{-1.0_rad_per_s,
                                                                                            -1.0_rad_per_s,
                                                                                            -1.0_rad_per_s}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectOrientationAccelerationTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<
          mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override
    {
        vehicle.SetOrientationAcceleration(GetParam());
    }

    void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) override
    {
        pedestrian.SetOrientationAcceleration(GetParam());
    }
};

TEST_P(DynamicObjectOrientationAccelerationTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectOrientationAccelerationHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_orientation_acceleration = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    for (int obj = 1; obj <= num_non_host_moving_objects; obj++)
    {
        auto& orientation_acceleration = ground_truth.moving_object(obj).base().orientation_acceleration();
        EXPECT_EQ(expected_orientation_acceleration.pitch.value(), orientation_acceleration.pitch());
        EXPECT_EQ(expected_orientation_acceleration.roll.value(), orientation_acceleration.roll());
        EXPECT_EQ(expected_orientation_acceleration.yaw.value(), orientation_acceleration.yaw());
    }
}

INSTANTIATE_TEST_SUITE_P(
    DynamicObjectOrientationAccelerationTest,
    DynamicObjectOrientationAccelerationTestParameterized,
    testing::Values(
        mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{0.0_rad_per_s_sq,
                                                                                            0.0_rad_per_s_sq,
                                                                                            0.0_rad_per_s_sq},
        mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{1.0_rad_per_s_sq,
                                                                                            1.0_rad_per_s_sq,
                                                                                            1.0_rad_per_s_sq},
        mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{-1.0_rad_per_s_sq,
                                                                                            -1.0_rad_per_s_sq,
                                                                                            -1.0_rad_per_s_sq}));

class DynamicObjectModelTestParameterized : public DynamicObjectTest
{
  protected:
    void SetVehicleProperties(mantle_api::VehicleProperties& vehicle_properties) override
    {
        vehicle_properties.model = "vehicle";
    }

    void SetPedestrianProperties(mantle_api::PedestrianProperties& pedestrian_properties) override
    {
        pedestrian_properties.model = "pedestrian";
    }
};

TEST_F(DynamicObjectModelTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectModelHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    EXPECT_EQ("vehicle", ground_truth.moving_object(1).model_reference());
    EXPECT_EQ("pedestrian", ground_truth.moving_object(2).model_reference());
}

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectAssignedLaneIdTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<std::vector<mantle_api::UniqueId>>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetAssignedLaneIds(GetParam()); }

    void SetPedestrian(environment::entities::PedestrianEntity& pedestrian) override
    {
        pedestrian.SetAssignedLaneIds(GetParam());
    }
};

TEST_P(DynamicObjectAssignedLaneIdTestParameterized,
       GivenMapWithTwoLanesAndDynamicObject_WhenStep_ThenGroundTruthDynamicObjectHasCorrectAssignedLaneId)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_lane_ids = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    const auto& vehicle = ground_truth.moving_object(1);
    const auto& pedestrian = ground_truth.moving_object(2);
    ASSERT_EQ(expected_lane_ids.size(), vehicle.moving_object_classification().assigned_lane_id_size());
    ASSERT_EQ(expected_lane_ids.size(), pedestrian.moving_object_classification().assigned_lane_id_size());
    ASSERT_EQ(expected_lane_ids.size(), vehicle.assigned_lane_id_size());
    ASSERT_EQ(expected_lane_ids.size(), pedestrian.assigned_lane_id_size());

    for (int i = 0; i < expected_lane_ids.size(); i++)
    {
        EXPECT_EQ(expected_lane_ids[i], vehicle.moving_object_classification().assigned_lane_id(i).value());
        EXPECT_EQ(expected_lane_ids[i], pedestrian.moving_object_classification().assigned_lane_id(i).value());
        EXPECT_EQ(expected_lane_ids[i], vehicle.assigned_lane_id(i).value());
        EXPECT_EQ(expected_lane_ids[i], pedestrian.assigned_lane_id(i).value());
    }
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectAssignedLaneIdTest,
                         DynamicObjectAssignedLaneIdTestParameterized,
                         testing::Values(std::vector<mantle_api::UniqueId>{0, 1},
                                         std::vector<mantle_api::UniqueId>{1, 0},
                                         std::vector<mantle_api::UniqueId>{0},
                                         std::vector<mantle_api::UniqueId>{1}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectIndicatorStateTestParameterized : public DynamicObjectTest,
                                                     public testing::WithParamInterface<mantle_api::IndicatorState>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetIndicatorState(GetParam()); }
};

TEST_P(DynamicObjectIndicatorStateTestParameterized,
       GivenMapWithTwoLanesAndDynamicObject_WhenStep_ThenGroundTruthDynamicObjectHasCorrectIndicatorState)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_indicator_state = ConvertAstasIndicatorStateToProto(GetParam());
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    const auto& vehicle = ground_truth.moving_object(1);
    EXPECT_EQ(expected_indicator_state, vehicle.vehicle_classification().light_state().indicator_state());
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectIndicatorStateTest,
                         DynamicObjectIndicatorStateTestParameterized,
                         testing::Values(mantle_api::IndicatorState::kUnknown,
                                         mantle_api::IndicatorState::kOther,
                                         mantle_api::IndicatorState::kOff,
                                         mantle_api::IndicatorState::kLeft,
                                         mantle_api::IndicatorState::kRight,
                                         mantle_api::IndicatorState::kWarning));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectVehicleTypeTestParameterized : public DynamicObjectTest,
                                                  public testing::WithParamInterface<mantle_api::VehicleClass>
{
  protected:
    void SetVehicleProperties(mantle_api::VehicleProperties& vehicle_properties) override
    {
        vehicle_properties.classification = GetParam();
    }
};

TEST_P(DynamicObjectVehicleTypeTestParameterized,
       GivenMapWithTwoLanesAndDynamicObject_WhenStep_ThenGroundTruthDynamicObjectHasCorrectVehicleType)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_vehicle_type = ConvertVehicleTypeToProtoVehicleType(GetParam());
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    const auto& vehicle = ground_truth.moving_object(1);
    EXPECT_EQ(expected_vehicle_type, vehicle.vehicle_classification().type());
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectVehicleTypeTest,
                         DynamicObjectVehicleTypeTestParameterized,
                         testing::Values(mantle_api::VehicleClass::kOther,
                                         mantle_api::VehicleClass::kSmall_car,
                                         mantle_api::VehicleClass::kCompact_car,
                                         mantle_api::VehicleClass::kMedium_car,
                                         mantle_api::VehicleClass::kLuxury_car,
                                         mantle_api::VehicleClass::kDelivery_van,
                                         mantle_api::VehicleClass::kHeavy_truck,
                                         mantle_api::VehicleClass::kSemitrailer,
                                         mantle_api::VehicleClass::kTrailer,
                                         mantle_api::VehicleClass::kMotorbike,
                                         mantle_api::VehicleClass::kBicycle,
                                         mantle_api::VehicleClass::kBus,
                                         mantle_api::VehicleClass::kTram,
                                         mantle_api::VehicleClass::kTrain,
                                         mantle_api::VehicleClass::kWheelchair));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectAxleBoundingBoxTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<mantle_api::Vec3<units::length::meter_t>>
{
  protected:
    void SetVehicleProperties(mantle_api::VehicleProperties& vehicle_properties) override
    {
        vehicle_properties.rear_axle.bb_center_to_axle_center = GetParam();
        vehicle_properties.front_axle.bb_center_to_axle_center = GetParam();
    }
};

TEST_P(DynamicObjectAxleBoundingBoxTestParameterized,
       GivenMapWithTwoLanesAndDynamicObjects_WhenStep_ThenDynamicObjectAxleBoundingBoxHasCorrectValue)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    mantle_api::Vec3<units::length::meter_t> expected_bb_center_to_axle_center = GetParam();
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();

    auto& rear_axle_bb_center_to_axle_center = ground_truth.moving_object(1).vehicle_attributes().bbcenter_to_rear();
    EXPECT_EQ(expected_bb_center_to_axle_center.x.value(), rear_axle_bb_center_to_axle_center.x());
    EXPECT_EQ(expected_bb_center_to_axle_center.y.value(), rear_axle_bb_center_to_axle_center.y());
    EXPECT_EQ(expected_bb_center_to_axle_center.z.value(), rear_axle_bb_center_to_axle_center.z());

    auto& front_axle_bb_center_to_axle_center = ground_truth.moving_object(1).vehicle_attributes().bbcenter_to_front();
    EXPECT_EQ(expected_bb_center_to_axle_center.x.value(), front_axle_bb_center_to_axle_center.x());
    EXPECT_EQ(expected_bb_center_to_axle_center.y.value(), front_axle_bb_center_to_axle_center.y());
    EXPECT_EQ(expected_bb_center_to_axle_center.z.value(), front_axle_bb_center_to_axle_center.z());
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectAxleBoundingBoxTest,
                         DynamicObjectAxleBoundingBoxTestParameterized,
                         testing::Values(mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Vec3<units::length::meter_t>{1.0_m, 1.0_m, 1.0_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectExternalControlStateTestParameterized
    : public DynamicObjectTest,
      public testing::WithParamInterface<mantle_api::ExternalControlState>
{
  protected:
    void CreateHostEntityOnly() override
    {
        auto host_entity = std::make_unique<environment::entities::VehicleEntity>(GetNewId(), "host");
        host_entity->SetHADControlState(GetParam());
        entities_.push_back(std::move(host_entity));
        entities_.back()->SetProperties(std::move(CreateHostVehicleProperties()));
    }

    std::unique_ptr<mantle_api::VehicleProperties> CreateHostVehicleProperties() const override
    {
        auto host_vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
        host_vehicle_properties->model = "host";
        host_vehicle_properties->is_host = true;
        return host_vehicle_properties;
    }
};

TEST_P(DynamicObjectExternalControlStateTestParameterized,
       GivenMapWithTwoLanesAndDynamicObject_WhenStep_ThenGroundTruthDynamicObjectHasCorrectExternalControlState)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_external_control_state =
        entities_.front().get()->GetProperties()->model + ";" + EncodeHADControlState(GetParam());
    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    const auto& host = ground_truth.moving_object(0);
    EXPECT_EQ(expected_external_control_state, host.model_reference());
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectExternalControlStateTest,
                         DynamicObjectExternalControlStateTestParameterized,
                         testing::Values(mantle_api::ExternalControlState::kOff,
                                         mantle_api::ExternalControlState::kFull,
                                         mantle_api::ExternalControlState::kLateralOnly,
                                         mantle_api::ExternalControlState::kLongitudinalOnly));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class DynamicObjectWheelDataTestParameterized : public DynamicObjectTest,
                                                public testing::WithParamInterface<entities::WheelStates>
{
  protected:
    void SetVehicle(environment::entities::VehicleEntity& vehicle) override { vehicle.SetWheelStates(GetParam()); }
};

TEST_P(DynamicObjectWheelDataTestParameterized,
       GivenMapWithTwoLanesAndDynamicObject_WhenStep_ThenGroundTruthDynamicObjectHasCorrectWheelData)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    auto expected_wheel_data = GetParam();

    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    const auto& vehicle = ground_truth.moving_object(1);
    const auto& front_right_wheel = vehicle.vehicle_attributes().wheel_data(0);
    const auto& front_left_wheel = vehicle.vehicle_attributes().wheel_data(1);
    const auto& rear_right_wheel = vehicle.vehicle_attributes().wheel_data(2);
    const auto& rear_left_wheel = vehicle.vehicle_attributes().wheel_data(3);

    EXPECT_EQ(front_right_wheel.axle(), 0);
    EXPECT_EQ(front_left_wheel.axle(), 0);
    EXPECT_EQ(rear_right_wheel.axle(), 1);
    EXPECT_EQ(rear_left_wheel.axle(), 1);

    EXPECT_EQ(front_right_wheel.index(), 0);
    EXPECT_EQ(front_left_wheel.index(), 1);
    EXPECT_EQ(rear_right_wheel.index(), 0);
    EXPECT_EQ(rear_left_wheel.index(), 1);

    EXPECT_EQ(front_right_wheel.friction_coefficient(), expected_wheel_data.front_right_mue);
    EXPECT_EQ(front_left_wheel.friction_coefficient(), expected_wheel_data.front_left_mue);
    EXPECT_EQ(rear_right_wheel.friction_coefficient(), expected_wheel_data.rear_right_mue);
    EXPECT_EQ(rear_left_wheel.friction_coefficient(), expected_wheel_data.rear_left_mue);
}

INSTANTIATE_TEST_SUITE_P(DynamicObjectWheelDataTest,
                         DynamicObjectWheelDataTestParameterized,
                         testing::Values(entities::WheelStates{.front_right_mue{1.0},
                                                               .front_left_mue{2.0},
                                                               .rear_right_mue{3.0},
                                                               .rear_left_mue{4.0}}));

TEST_F(DynamicObjectTest,
       GivenMapWithTwoLanesAndDynamicObject_WhenStep_ThenGroundTruthDynamicObjectHasCorrectTypeAndSourceReference)
{
    sensor_view_builder_.Step(entities_, entities_.front().get());

    const auto& ground_truth = sensor_view_builder_.GetSensorView().global_ground_truth();
    const auto& vehicle = ground_truth.moving_object(1);
    const auto& pedestrian = ground_truth.moving_object(2);

    EXPECT_EQ(vehicle.type(), astas_osi3::MovingObject::TYPE_VEHICLE);
    EXPECT_EQ(vehicle.source_reference(0).identifier(0), "Vehicle");
    EXPECT_EQ(vehicle.source_reference(0).identifier(1), "vehicle");
    EXPECT_EQ(pedestrian.type(), astas_osi3::MovingObject::TYPE_PEDESTRIAN);
    EXPECT_EQ(pedestrian.source_reference(0).identifier(0), "Pedestrian");
    EXPECT_EQ(pedestrian.source_reference(0).identifier(1), "pedestrian");
}

}  // namespace astas::environment::proto_groundtruth
