/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWSANITYCHECKER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWSANITYCHECKER_H

#include "Core/Environment/Chunking/world_chunk.h"
#include "astas_osi_groundtruth.pb.h"
#include "astas_osi_sensorview.pb.h"

#include <list>

namespace astas::environment::proto_groundtruth
{

class SensorViewSanityChecker
{
  public:
    SensorViewSanityChecker(const astas_osi3::SensorView& sensor_view,
                            const chunking::WorldChunks& world_chunks,
                            const std::list<mantle_api::UniqueId>& static_entities_to_ignore,
                            const std::set<mantle_api::UniqueId>& dynamic_entities_to_ignore);

    void PerformSanityCheck();
    void CollectIdsFromChunk();
    void CheckNumberObjects();

  private:
    void CollectIdsFromRoadObjects(const chunking::WorldChunk& map_chunk);
    void CollectIdsFromTrafficSigns(const chunking::WorldChunk& map_chunk);
    void CollectIdsFromTrafficLights(const chunking::WorldChunk& map_chunk);
    void CollectIdsFromEntities(const chunking::WorldChunk& map_chunk);
    void ProcessStaticEntity(const mantle_api::IEntity& entity);
    void ProcessDynamicEntity(const mantle_api::IEntity& entity);

    bool IsStaticEntity(const mantle_api::IEntity& entity) const;
    bool IsDynamicEntity(const mantle_api::IEntity& entity) const;
    bool IsEntityTrafficSign(const mantle_api::IEntity& entity) const;
    bool IsEntityTrafficLight(const mantle_api::IEntity& entity) const;
    bool IsIdIgnored(const mantle_api::UniqueId& id) const;

    using ObjectIds = std::unordered_set<mantle_api::UniqueId>;
    ObjectIds traffic_light_ids_{};
    ObjectIds traffic_sign_ids_{};
    ObjectIds stationary_object_ids_{};
    ObjectIds moving_object_ids_{};

    const astas_osi3::SensorView& sensor_view_;
    const chunking::WorldChunks& world_chunks_;
    const std::list<mantle_api::UniqueId>& static_entities_to_ignore_;
    const std::set<mantle_api::UniqueId>& dynamic_entities_to_ignore_;
};

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWSANITYCHECKER_H
