/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWBUILDER_H

#include "Core/Environment/Chunking/chunking.h"
#include "Core/Environment/Chunking/world_chunk.h"
#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_ground_truth_builder.h"
#include "Core/Environment/GroundTruth/Internal/Environment/environment_proto_ground_truth_builder.h"
#include "Core/Environment/GroundTruth/Internal/Static/static_proto_ground_truth_builder.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/UserSettings/user_settings.h"
#include "astas_osi_groundtruth.pb.h"
#include "astas_osi_sensorview.pb.h"

#include <mutex>

namespace astas::environment::proto_groundtruth
{

class SensorViewBuilder
{
  public:
    SensorViewBuilder(const environment::map::AstasMap& astas_map,
                      const mantle_api::Time& step_size,
                      const service::user_settings::MapChunking& chunking_settings);

    void Init();

    /// @brief Gathers all necessary information and builds GroundTruth that can be published
    void Step(const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities, mantle_api::IEntity* central_entity);

    /// @brief Set an entity, via its unique identifier, to be ignored when building GroundTruth
    /// @param entity_id ID of the entity which not be built into the GroundTruth
    void AddEntityIdToIgnoreList(mantle_api::UniqueId entity_id);

    /// @return Const reference to astas_osi3::SensorView
    const astas_osi3::SensorView& GetSensorView() const;

    mutable std::mutex gt_mutex{};

    void SetWeather(const mantle_api::Weather& weather);
    void SetDateTime(const mantle_api::Time& date_time);
    mantle_api::Time GetDateTime() const;

    chunking::StaticChunkList GetStaticChunks() const;
    chunking::WorldChunks GetChunksAroundEntity(const mantle_api::Vec3<units::length::meter_t>& central_entity_position,
                                                const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities);

  private:
    astas_osi3::GroundTruth& GetMutableGroundTruth();
    void SetVersion();
    void SetTimestamp();
    void FillAdditionalData();
    void ValidateSensorView() const;

    static std::uint32_t GetTimeOfDayInSeconds(mantle_api::Time date_time_ms);

    const environment::map::AstasMap& astas_map_;
    astas_osi3::SensorView sensor_view_;
    StaticProtoGroundTruthBuilder static_proto_ground_truth_builder_;
    DynamicProtoGroundTruthBuilder dynamic_proto_ground_truth_builder_;
    EnvironmentProtoGroundTruthBuilder environment_proto_ground_truth_builder_;
    service::user_settings::MapChunking chunking_settings_;

    chunking::DefaultMapChunker map_chunker_{};
    chunking::WorldChunks world_chunks_{};
};

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWBUILDER_H
