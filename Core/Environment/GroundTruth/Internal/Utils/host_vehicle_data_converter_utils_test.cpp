/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Utils/host_vehicle_data_converter_utils.h"

#include "Core/Environment/Exception/exception.h"
#include "astas_osi_sensorview.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth::utils
{
namespace
{
TEST(ConvertHadControlStateTest, GivenHadStateConverter_WhenConversionHappens_ThenExpectCorrectResults)
{

    ASSERT_EQ(ConvertHadControlStateToOsiDrivingFunctionState(mantle_api::ExternalControlState::kOff),
              astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_UNAVAILABLE);
    ASSERT_EQ(ConvertHadControlStateToOsiDrivingFunctionState(mantle_api::ExternalControlState::kLongitudinalOnly),
              astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE);
    ASSERT_EQ(ConvertHadControlStateToOsiDrivingFunctionState(mantle_api::ExternalControlState::kLateralOnly),
              astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE);
    ASSERT_EQ(ConvertHadControlStateToOsiDrivingFunctionState(mantle_api::ExternalControlState::kFull),
              astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE);
    ASSERT_THROW(ConvertHadControlStateToOsiDrivingFunctionState(static_cast<mantle_api::ExternalControlState>(999)),
                 EnvironmentException);
}
}  // namespace
}  // namespace astas::environment::proto_groundtruth::utils
