/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_UTILS_HOSTVEHICLEDATACONVERTERUTILS_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_UTILS_HOSTVEHICLEDATACONVERTERUTILS_H

#include "astas_osi_sensorview.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>

namespace astas::environment::proto_groundtruth::utils
{

astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::State ConvertHadControlStateToOsiDrivingFunctionState(
    const mantle_api::ExternalControlState had_control_state);

}  // namespace astas::environment::proto_groundtruth::utils

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_UTILS_HOSTVEHICLEDATACONVERTERUTILS_H
