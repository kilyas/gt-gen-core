/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_VEHICLEENTITYPROTOCONVERTERUTILS_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_VEHICLEENTITYPROTOCONVERTERUTILS_H

#include "astas_osi_object.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>

namespace astas::environment::proto_groundtruth
{
using ProtoIndicatorState = astas_osi3::MovingObject::VehicleClassification::LightState;
ProtoIndicatorState::IndicatorState ConvertAstasIndicatorStateToProto(mantle_api::IndicatorState indicator_state);

using ProtoVehicleClassification = astas_osi3::MovingObject::VehicleClassification;
ProtoVehicleClassification::Type ConvertVehicleTypeToProtoVehicleType(mantle_api::VehicleClass vehicle_class);

void FillWheelData(std::uint32_t axel_index,
                   std::uint32_t wheel_index,
                   double friction_coefficient,
                   astas_osi3::MovingObject_VehicleAttributes_WheelData* wheel_data);

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_VEHICLEENTITYPROTOCONVERTERUTILS_H
