/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter.h"

#include "Core/Environment/Entities/vehicle_entity.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{

TEST(VehicleEntityProtoConverterTest, GivenVehicleEntity_WhenFillProtoGroundTruthVehicleEntity_ThenGTCorrect)
{
    using units::literals::operator""_m;

    auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
    vehicle_properties->front_axle.bb_center_to_axle_center = {1.23_m, 0_m, 0_m};
    vehicle_properties->rear_axle.bb_center_to_axle_center = {-1.23_m, 0_m, 0_m};
    vehicle_properties->classification = mantle_api::VehicleClass::kLuxury_car;
    vehicle_properties->model = "BMW7S2015";

    entities::VehicleEntity vehicle(42, "VehicleName");
    vehicle.SetProperties(std::move(vehicle_properties));
    vehicle.SetIndicatorState(mantle_api::IndicatorState::kWarning);

    astas_osi3::GroundTruth gt;
    FillProtoGroundTruthVehicleEntity(&vehicle, gt);

    ASSERT_EQ(1, gt.moving_object().size());
    const auto object = gt.moving_object().cbegin();
    EXPECT_EQ(astas_osi3::MovingObject::TYPE_VEHICLE, object->type());

    const auto attributes = object->vehicle_attributes();
    EXPECT_EQ(1.23, attributes.bbcenter_to_front().x());
    EXPECT_EQ(-1.23, attributes.bbcenter_to_rear().x());

    const auto classification = object->vehicle_classification();
    EXPECT_EQ(astas_osi3::MovingObject::VehicleClassification::LightState::INDICATOR_STATE_WARNING,
              classification.light_state().indicator_state());
    EXPECT_EQ(astas_osi3::MovingObject::VehicleClassification::TYPE_LUXURY_CAR, classification.type());

    EXPECT_EQ(false, gt.has_host_vehicle_id());

    EXPECT_EQ("BMW7S2015", gt.moving_object().begin()->model_reference());

    EXPECT_EQ("Vehicle", gt.moving_object(0).source_reference().begin()->identifier(0));
    EXPECT_EQ("VehicleName", gt.moving_object(0).source_reference().begin()->identifier(1));
}

TEST(VehicleEntityProtoConverterTest,
     GivenHostVehicleEntityAndNotSetHostVehicleGroundtruth_WhenFillProtoGroundTruthVehicleEntity_ThenHostSpecificGTSet)
{
    const auto host_vehicle_id = 1;
    auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
    vehicle_properties->is_host = true;
    vehicle_properties->model = "BMW7S2015";
    entities::WheelStates expected_wheel_states{0.2, 0.3, 0.4, 0.5};

    entities::VehicleEntity vehicle(host_vehicle_id, "Host");
    vehicle.SetProperties(std::move(vehicle_properties));
    vehicle.SetHADControlState(mantle_api::ExternalControlState::kFull);
    vehicle.SetWheelStates(expected_wheel_states);

    astas_osi3::GroundTruth gt;
    gt.mutable_host_vehicle_id()->set_value(host_vehicle_id);
    FillProtoGroundTruthVehicleEntity(&vehicle, gt);

    ASSERT_EQ(1, gt.moving_object().size());
    EXPECT_EQ(1, gt.host_vehicle_id().value());

    EXPECT_EQ("BMW7S2015;1", gt.moving_object().begin()->model_reference());

    EXPECT_EQ("Vehicle", gt.moving_object(0).source_reference().begin()->identifier(0));
    EXPECT_EQ("Host", gt.moving_object(0).source_reference().begin()->identifier(1));

    const auto actual_wheel_states = gt.moving_object(0).vehicle_attributes().wheel_data();
    ASSERT_EQ(4, actual_wheel_states.size());

    EXPECT_DOUBLE_EQ(expected_wheel_states.front_right_mue, actual_wheel_states[0].friction_coefficient());
    EXPECT_EQ(0, actual_wheel_states[0].axle());
    EXPECT_EQ(0, actual_wheel_states[0].index());

    EXPECT_DOUBLE_EQ(expected_wheel_states.front_left_mue, actual_wheel_states[1].friction_coefficient());
    EXPECT_EQ(0, actual_wheel_states[1].axle());
    EXPECT_EQ(1, actual_wheel_states[1].index());

    EXPECT_DOUBLE_EQ(expected_wheel_states.rear_right_mue, actual_wheel_states[2].friction_coefficient());
    EXPECT_EQ(1, actual_wheel_states[2].axle());
    EXPECT_EQ(0, actual_wheel_states[2].index());

    EXPECT_DOUBLE_EQ(expected_wheel_states.rear_left_mue, actual_wheel_states[3].friction_coefficient());
    EXPECT_EQ(1, actual_wheel_states[3].axle());
    EXPECT_EQ(1, actual_wheel_states[3].index());
}

TEST(
    VehicleEntityProtoConverterTest,
    GivenHostVehicleEntityAndHostVehicleIdIsSetInGroundtruth_WhenFillProtoGroundTruthVehicleEntity_ThenHostVehicleIdIsNotOverridden)
{
    const auto expected_host_vehicle_id = 2;

    auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
    vehicle_properties->is_host = true;

    entities::VehicleEntity vehicle(1, "Host");
    vehicle.SetProperties(std::move(vehicle_properties));

    astas_osi3::GroundTruth gt;
    gt.mutable_host_vehicle_id()->set_value(expected_host_vehicle_id);

    FillProtoGroundTruthVehicleEntity(&vehicle, gt);

    EXPECT_EQ(expected_host_vehicle_id, gt.host_vehicle_id().value());
}

TEST(
    VehicleEntityProtoConverterTest,
    GivenVehicleEntityAndIdenticalToTheHostVehicleId_WhenFillProtoGroundTruthVehicleEntity_ThenVehicleHADControlStateIsSet)
{
    const auto expected_host_vehicle_id = 2;

    auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
    vehicle_properties->model = "BMW7S2015";

    entities::VehicleEntity vehicle(expected_host_vehicle_id, "VehicleName");
    vehicle.SetProperties(std::move(vehicle_properties));
    vehicle.SetHADControlState(mantle_api::ExternalControlState::kFull);

    astas_osi3::GroundTruth gt;
    gt.mutable_host_vehicle_id()->set_value(expected_host_vehicle_id);
    FillProtoGroundTruthVehicleEntity(&vehicle, gt);

    ASSERT_EQ(1, gt.moving_object().size());
    EXPECT_EQ(expected_host_vehicle_id, gt.host_vehicle_id().value());

    EXPECT_EQ("BMW7S2015;1", gt.moving_object().begin()->model_reference());
}

TEST(VehicleEntityProtoConverterTest,
     GivenVehicleEntity_WhenFillProtoGroundTruthVehicleEntity_ThenVehicleWheelStateIsSet)
{
    auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
    entities::WheelStates expected_wheel_states{0.2, 0.3, 0.4, 0.5};

    entities::VehicleEntity vehicle(1, "VehicleName");
    vehicle.SetProperties(std::move(vehicle_properties));
    vehicle.SetWheelStates(expected_wheel_states);

    astas_osi3::GroundTruth gt;
    FillProtoGroundTruthVehicleEntity(&vehicle, gt);

    ASSERT_EQ(1, gt.moving_object().size());

    EXPECT_EQ("Vehicle", gt.moving_object(0).source_reference().begin()->identifier(0));

    const auto actual_wheel_states = gt.moving_object(0).vehicle_attributes().wheel_data();
    ASSERT_EQ(4, actual_wheel_states.size());

    EXPECT_DOUBLE_EQ(expected_wheel_states.front_right_mue, actual_wheel_states[0].friction_coefficient());
    EXPECT_EQ(0, actual_wheel_states[0].axle());
    EXPECT_EQ(0, actual_wheel_states[0].index());

    EXPECT_DOUBLE_EQ(expected_wheel_states.front_left_mue, actual_wheel_states[1].friction_coefficient());
    EXPECT_EQ(0, actual_wheel_states[1].axle());
    EXPECT_EQ(1, actual_wheel_states[1].index());

    EXPECT_DOUBLE_EQ(expected_wheel_states.rear_right_mue, actual_wheel_states[2].friction_coefficient());
    EXPECT_EQ(1, actual_wheel_states[2].axle());
    EXPECT_EQ(0, actual_wheel_states[2].index());

    EXPECT_DOUBLE_EQ(expected_wheel_states.rear_left_mue, actual_wheel_states[3].friction_coefficient());
    EXPECT_EQ(1, actual_wheel_states[3].axle());
    EXPECT_EQ(1, actual_wheel_states[3].index());
}

}  // namespace astas::environment::proto_groundtruth
