/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_converter_utils.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"

namespace astas::environment::proto_groundtruth
{

void FillBasicMovingObjectProperties(const mantle_api::IEntity* entity, astas_osi3::MovingObject* gt_moving_object)
{
    gt_moving_object->mutable_id()->set_value(entity->GetUniqueId());

    service::gt_conversion::FillProtoObject(entity->GetPosition(),
                                            gt_moving_object->mutable_base()->mutable_position());

    service::gt_conversion::FillProtoObject(entity->GetVelocity(),
                                            gt_moving_object->mutable_base()->mutable_velocity());

    service::gt_conversion::FillProtoObject(entity->GetAcceleration(),
                                            gt_moving_object->mutable_base()->mutable_acceleration());

    service::gt_conversion::FillProtoObject(entity->GetProperties()->bounding_box.dimension,
                                            gt_moving_object->mutable_base()->mutable_dimension());

    service::gt_conversion::FillProtoObject(entity->GetOrientation(),
                                            gt_moving_object->mutable_base()->mutable_orientation());

    service::gt_conversion::FillProtoObject(entity->GetOrientationRate(),
                                            gt_moving_object->mutable_base()->mutable_orientation_rate());

    service::gt_conversion::FillProtoObject(entity->GetOrientationAcceleration(),
                                            gt_moving_object->mutable_base()->mutable_orientation_acceleration());

    gt_moving_object->set_model_reference(entity->GetProperties()->model);

    for (const auto& lane_id : entity->GetAssignedLaneIds())
    {
        gt_moving_object->add_assigned_lane_id()->set_value(lane_id);
        gt_moving_object->mutable_moving_object_classification()->add_assigned_lane_id()->set_value(lane_id);
    }
}

void FillSourceReference(const mantle_api::IEntity* entity,
                         const std::string& entity_type,
                         astas_osi3::ExternalReference* external_ref)
{
    external_ref->add_identifier(entity_type.c_str());
    external_ref->add_identifier(entity->GetName().c_str());
}

std::string EncodeHADControlState(mantle_api::ExternalControlState had_control_state)
{
    switch (had_control_state)
    {
        case mantle_api::ExternalControlState::kOff:
        {
            return "0";
        }
        case mantle_api::ExternalControlState::kLongitudinalOnly:
        case mantle_api::ExternalControlState::kLateralOnly:
        case mantle_api::ExternalControlState::kFull:
        {
            return "1";
        }
        default:
            throw EnvironmentException("Unsupported HAD control state encountered: {}", had_control_state);
    }
}

}  // namespace astas::environment::proto_groundtruth
