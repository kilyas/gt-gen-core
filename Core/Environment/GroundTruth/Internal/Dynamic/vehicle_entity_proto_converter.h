/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_VEHICLEENTITYPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_VEHICLEENTITYPROTOCONVERTER_H

#include "Core/Environment/Entities/vehicle_entity.h"
#include "astas_osi_groundtruth.pb.h"

namespace astas::environment::proto_groundtruth
{

void FillProtoGroundTruthVehicleEntity(const entities::VehicleEntity* vehicle_entity_ptr,
                                       astas_osi3::GroundTruth& proto_groundtruth);

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_VEHICLEENTITYPROTOCONVERTER_H
