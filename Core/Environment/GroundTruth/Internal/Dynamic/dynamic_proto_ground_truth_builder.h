/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_DYNAMICPROTOGROUNDTRUTHBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_DYNAMICPROTOGROUNDTRUTHBUILDER_H

#include "Core/Environment/Chunking/world_chunk.h"
#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Common/i_identifiable.h>

#include <set>

namespace astas::environment::proto_groundtruth
{

class DynamicProtoGroundTruthBuilder
{
  public:
    void FillDynamicGroundTruth(const environment::chunking::WorldChunks& vector, astas_osi3::GroundTruth& proto_gt);
    void AddEntityIdToIgnoreList(mantle_api::UniqueId entity_id);
    const std::set<mantle_api::UniqueId>& GetIgnoreList() const { return entities_to_ignore_; }

  private:
    void FillProtoGroundTruthMovingObjects(const environment::chunking::WorldChunks& world_chunks,
                                           astas_osi3::GroundTruth& proto_gt);

    std::set<mantle_api::UniqueId> entities_to_ignore_{};
};
}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_DYNAMICPROTOGROUNDTRUTHBUILDER_H
