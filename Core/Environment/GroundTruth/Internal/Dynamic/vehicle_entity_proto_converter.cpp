/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter.h"

#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_converter_utils.h"
#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter_utils.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"

namespace astas::environment::proto_groundtruth
{

void FillProtoGroundTruthVehicleEntity(const entities::VehicleEntity* vehicle_entity_ptr,
                                       astas_osi3::GroundTruth& proto_groundtruth)
{
    auto proto_vehicle_ptr = proto_groundtruth.add_moving_object();
    proto_vehicle_ptr->set_type(astas_osi3::MovingObject::TYPE_VEHICLE);

    FillBasicMovingObjectProperties(vehicle_entity_ptr, proto_vehicle_ptr);

    auto source_ref_ptr = proto_vehicle_ptr->add_source_reference();
    FillSourceReference(vehicle_entity_ptr, "Vehicle", source_ref_ptr);

    proto_vehicle_ptr->mutable_vehicle_classification()->mutable_light_state()->set_indicator_state(
        ConvertAstasIndicatorStateToProto(vehicle_entity_ptr->GetIndicatorState()));
    service::gt_conversion::FillProtoObject(
        vehicle_entity_ptr->GetProperties()->rear_axle.bb_center_to_axle_center,
        proto_vehicle_ptr->mutable_vehicle_attributes()->mutable_bbcenter_to_rear());
    service::gt_conversion::FillProtoObject(
        vehicle_entity_ptr->GetProperties()->front_axle.bb_center_to_axle_center,
        proto_vehicle_ptr->mutable_vehicle_attributes()->mutable_bbcenter_to_front());
    proto_vehicle_ptr->mutable_vehicle_classification()->set_type(
        ConvertVehicleTypeToProtoVehicleType(vehicle_entity_ptr->GetProperties()->classification));

    if (proto_groundtruth.has_host_vehicle_id())
    {
        if (proto_groundtruth.host_vehicle_id().value() == vehicle_entity_ptr->GetUniqueId())
        {
            proto_vehicle_ptr->set_model_reference(vehicle_entity_ptr->GetProperties()->model + ";" +
                                                   EncodeHADControlState(vehicle_entity_ptr->GetHADControlState()));
        }
    }

    const auto& entity_wheel_states = vehicle_entity_ptr->GetWheelStates();
    auto* proto_vehicle_attributes = proto_vehicle_ptr->mutable_vehicle_attributes();
    FillWheelData(0, 0, entity_wheel_states.front_right_mue, proto_vehicle_attributes->add_wheel_data());
    FillWheelData(0, 1, entity_wheel_states.front_left_mue, proto_vehicle_attributes->add_wheel_data());
    FillWheelData(1, 0, entity_wheel_states.rear_right_mue, proto_vehicle_attributes->add_wheel_data());
    FillWheelData(1, 1, entity_wheel_states.rear_left_mue, proto_vehicle_attributes->add_wheel_data());
}

}  // namespace astas::environment::proto_groundtruth
