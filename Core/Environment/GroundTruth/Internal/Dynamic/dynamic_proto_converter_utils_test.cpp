/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_converter_utils.h"

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{

class DynamicProtoConverterUtilsTestFixure
    : public ::testing::TestWithParam<std::pair<mantle_api::ExternalControlState, std::string>>
{
};

INSTANTIATE_TEST_SUITE_P(DynamicProtoConverterUtils,
                         DynamicProtoConverterUtilsTestFixure,
                         ::testing::Values(std::make_pair(mantle_api::ExternalControlState::kOff, "0"),
                                           std::make_pair(mantle_api::ExternalControlState::kFull, "1"),
                                           std::make_pair(mantle_api::ExternalControlState::kLateralOnly, "1"),
                                           std::make_pair(mantle_api::ExternalControlState::kLongitudinalOnly, "1")));

TEST_P(DynamicProtoConverterUtilsTestFixure,
       GivenExternalControlStateOff_WhenEncodeHADControlState_ThenCorrectStateReturned)
{
    mantle_api::ExternalControlState state = GetParam().first;
    std::string expected_encoded_state = GetParam().second;
    std::string actual_encoded_state = EncodeHADControlState(state);
    EXPECT_EQ(expected_encoded_state, actual_encoded_state);
}

TEST(DynamicProtoConverterUtilsTest, GivenVehicleEntity_WhenFillSourceReference_ThenExternalReferenceIdentifierAdded)
{
    entities::VehicleEntity vehicle_entity(1, "car01");
    astas_osi3::ExternalReference external_reference;

    FillSourceReference(&vehicle_entity, "vehicle", &external_reference);
    EXPECT_EQ(external_reference.identifier(0), "vehicle");
    EXPECT_EQ(external_reference.identifier(1), "car01");
}

TEST(DynamicProtoConverterUtilsTest, GivenPedestrianEntity_WhenFillSourceReference_ThenExternalReferenceIdentifierAdded)
{
    entities::PedestrianEntity pedestrian_entity(1, "pedestrian01");
    astas_osi3::ExternalReference external_reference;

    FillSourceReference(&pedestrian_entity, "pedestrian", &external_reference);
    EXPECT_EQ(external_reference.identifier(0), "pedestrian");
    EXPECT_EQ(external_reference.identifier(1), "pedestrian01");
}

TEST(VehicleEntityProtoConverterTest, GivenVehicleEntity_WhenFillBasicMovingObjectProperties_ThenGTCorrect)
{
    using units::literals::operator""_m;
    using units::literals::operator""_mps;
    using units::literals::operator""_mps_sq;
    using units::literals::operator""_rad;
    using units::literals::operator""_rad_per_s;
    using units::literals::operator""_rad_per_s_sq;

    mantle_api::Vec3<units::length::meter_t> expected_position{12_m, 13_m, 14_m};
    mantle_api::Vec3<units::velocity::meters_per_second_t> expected_velocity{15_mps, 16_mps, 17_mps};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> expected_acceleration{
        18_mps_sq, 19_mps_sq, 20_mps_sq};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{21_rad, 22_rad, 23_rad};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> expected_orientation_rate{
        24_rad_per_s, 25_rad_per_s, 26_rad_per_s};
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> expected_orientation_acc{
        27_rad_per_s_sq, 28_rad_per_s_sq, 29_rad_per_s_sq};
    mantle_api::Dimension3 expected_dimension{5_m, 3_m, 1.5_m};
    std::vector<mantle_api::UniqueId> expected_lane_ids{1, 5};
    std::string expected_model{"some_model"};

    auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
    vehicle_properties->bounding_box.dimension = expected_dimension;
    vehicle_properties->model = expected_model;

    entities::VehicleEntity vehicle(42, "VehicleName");
    vehicle.SetProperties(std::move(vehicle_properties));
    vehicle.SetPosition(expected_position);
    vehicle.SetVelocity(expected_velocity);
    vehicle.SetAcceleration(expected_acceleration);
    vehicle.SetOrientation(expected_orientation);
    vehicle.SetOrientationRate(expected_orientation_rate);
    vehicle.SetOrientationAcceleration(expected_orientation_acc);
    vehicle.SetAssignedLaneIds(expected_lane_ids);

    astas_osi3::MovingObject gt_moving_object;
    FillBasicMovingObjectProperties(&vehicle, &gt_moving_object);

    EXPECT_EQ(42, gt_moving_object.id().value());

    const auto& base = gt_moving_object.base();
    EXPECT_TRIPLE(expected_position, service::gt_conversion::ToVec3Length(base.position()));
    EXPECT_TRIPLE(expected_velocity, service::gt_conversion::ToVec3Velocity(base.velocity()));
    EXPECT_TRIPLE(expected_acceleration, service::gt_conversion::ToVec3Acceleration(base.acceleration()));
    EXPECT_TRIPLE(expected_orientation, service::gt_conversion::ToOrientation3(base.orientation()));
    EXPECT_TRIPLE(expected_orientation_rate, service::gt_conversion::ToOrientation3Rate(base.orientation_rate()));
    EXPECT_TRIPLE(expected_orientation_acc,
                  service::gt_conversion::ToOrientation3Acceleration(base.orientation_acceleration()));
    EXPECT_TRIPLE(expected_dimension, service::gt_conversion::ToDimension3(base.dimension()));

    EXPECT_EQ(expected_model, gt_moving_object.model_reference());

    EXPECT_EQ(expected_lane_ids.size(), gt_moving_object.assigned_lane_id_size());
    EXPECT_EQ(expected_lane_ids.at(0), gt_moving_object.assigned_lane_id(0).value());
    EXPECT_EQ(expected_lane_ids.at(1), gt_moving_object.assigned_lane_id(1).value());
    EXPECT_EQ(expected_lane_ids.size(), gt_moving_object.moving_object_classification().assigned_lane_id_size());
    EXPECT_EQ(expected_lane_ids.at(0), gt_moving_object.moving_object_classification().assigned_lane_id(0).value());
    EXPECT_EQ(expected_lane_ids.at(1), gt_moving_object.moving_object_classification().assigned_lane_id(1).value());
}

}  // namespace astas::environment::proto_groundtruth
