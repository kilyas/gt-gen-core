/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_PEDESTRIANENTITYPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_PEDESTRIANENTITYPROTOCONVERTER_H

#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace astas::environment::proto_groundtruth
{
void FillProtoGroundTruthPedestrianEntity(const mantle_api::IPedestrian* pedestrian_entity_ptr,
                                          astas_osi3::GroundTruth& proto_groundtruth);
}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_PEDESTRIANENTITYPROTOCONVERTER_H
