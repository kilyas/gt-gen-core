/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/vehicle_entity_proto_converter_utils.h"

#include "Core/Service/Logging/logging.h"

namespace astas::environment::proto_groundtruth
{

ProtoIndicatorState::IndicatorState ConvertAstasIndicatorStateToProto(mantle_api::IndicatorState indicator_state)
{
    switch (indicator_state)
    {
        case mantle_api::IndicatorState::kOff:
        {
            return ProtoIndicatorState::INDICATOR_STATE_OFF;
        }
        case mantle_api::IndicatorState::kWarning:
        {
            return ProtoIndicatorState::INDICATOR_STATE_WARNING;
        }
        case mantle_api::IndicatorState::kRight:
        {
            return ProtoIndicatorState::INDICATOR_STATE_RIGHT;
        }
        case mantle_api::IndicatorState::kLeft:
        {
            return ProtoIndicatorState::INDICATOR_STATE_LEFT;
        }
        case mantle_api::IndicatorState::kUnknown:
        {
            return ProtoIndicatorState::INDICATOR_STATE_UNKNOWN;
        }
        case mantle_api::IndicatorState::kOther:
        {
            return ProtoIndicatorState::INDICATOR_STATE_OTHER;
        }
        default:
        {
            ASSERT(false && "Could not convert the passed in GTGen indicator state to proto indicator state")
        }
    }
}

ProtoVehicleClassification::Type ConvertVehicleTypeToProtoVehicleType(const mantle_api::VehicleClass vehicle_class)
{
    switch (vehicle_class)
    {
        case mantle_api::VehicleClass::kOther:
        {
            return ProtoVehicleClassification::TYPE_OTHER;
        }
        case mantle_api::VehicleClass::kSmall_car:
        {
            return ProtoVehicleClassification::TYPE_SMALL_CAR;
        }
        case mantle_api::VehicleClass::kCompact_car:
        {
            return ProtoVehicleClassification::TYPE_COMPACT_CAR;
        }
        case mantle_api::VehicleClass::kMedium_car:
        {
            return ProtoVehicleClassification::TYPE_MEDIUM_CAR;
        }
        case mantle_api::VehicleClass::kLuxury_car:
        {
            return ProtoVehicleClassification::TYPE_LUXURY_CAR;
        }
        case mantle_api::VehicleClass::kDelivery_van:
        {
            return ProtoVehicleClassification::TYPE_DELIVERY_VAN;
        }
        case mantle_api::VehicleClass::kHeavy_truck:
        {
            return ProtoVehicleClassification::TYPE_HEAVY_TRUCK;
        }
        case mantle_api::VehicleClass::kSemitrailer:
        {
            return ProtoVehicleClassification::TYPE_SEMITRAILER;
        }
        case mantle_api::VehicleClass::kTrailer:
        {
            return ProtoVehicleClassification::TYPE_TRAILER;
        }
        case mantle_api::VehicleClass::kMotorbike:
        {
            return ProtoVehicleClassification::TYPE_MOTORBIKE;
        }
        case mantle_api::VehicleClass::kBicycle:
        {
            return ProtoVehicleClassification::TYPE_BICYCLE;
        }
        case mantle_api::VehicleClass::kBus:
        {
            return ProtoVehicleClassification::TYPE_BUS;
        }
        case mantle_api::VehicleClass::kTram:
        {
            return ProtoVehicleClassification::TYPE_TRAM;
        }
        case mantle_api::VehicleClass::kTrain:
        {
            return ProtoVehicleClassification::TYPE_TRAIN;
        }
        case mantle_api::VehicleClass::kWheelchair:
        {
            return ProtoVehicleClassification::TYPE_WHEELCHAIR;
        }
        default:
        {
            ASSERT(false && "Could not convert the passed in GTGen vehicle type to proto vehicle type")
        }
    }
}

void FillWheelData(std::uint32_t axel_index,
                   std::uint32_t wheel_index,
                   double friction_coefficient,
                   astas_osi3::MovingObject_VehicleAttributes_WheelData* wheel_data)
{
    wheel_data->set_axle(axel_index);
    wheel_data->set_index(wheel_index);
    wheel_data->set_friction_coefficient(friction_coefficient);
}

}  // namespace astas::environment::proto_groundtruth
