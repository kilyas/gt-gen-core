/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_ground_truth_builder.h"

#include "Core/Environment/GroundTruth/Internal/Dynamic/moving_object_proto_converter.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/algorithm_utils.h"

namespace astas::environment::proto_groundtruth
{

void DynamicProtoGroundTruthBuilder::FillDynamicGroundTruth(const environment::chunking::WorldChunks& vector,
                                                            astas_osi3::GroundTruth& proto_gt)
{
    ASTAS_PROFILE_SCOPE

    FillProtoGroundTruthMovingObjects(vector, proto_gt);
}

void DynamicProtoGroundTruthBuilder::AddEntityIdToIgnoreList(mantle_api::UniqueId entity_id)
{
    entities_to_ignore_.insert(entity_id);
}

void DynamicProtoGroundTruthBuilder::FillProtoGroundTruthMovingObjects(
    const environment::chunking::WorldChunks& world_chunks,
    astas_osi3::GroundTruth& proto_gt)
{
    ASTAS_PROFILE_SCOPE

    for (const auto& ground_truth_chunk : world_chunks)
    {
        for (const auto* entity : ground_truth_chunk.entities)
        {
            if (entity->GetVisibility().traffic &&
                !service::utility::Contains(entities_to_ignore_, entity->GetUniqueId()))
            {
                FillProtoGroundTruthMovingObject(entity, proto_gt);
            }
        }
    }
}

}  // namespace astas::environment::proto_groundtruth
