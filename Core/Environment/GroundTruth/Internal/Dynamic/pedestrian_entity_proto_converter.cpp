/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/pedestrian_entity_proto_converter.h"

#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_converter_utils.h"

namespace astas::environment::proto_groundtruth
{

void FillProtoGroundTruthPedestrianEntity(const mantle_api::IPedestrian* pedestrian_entity_ptr,
                                          astas_osi3::GroundTruth& proto_groundtruth)
{
    auto proto_pedestrian_ptr = proto_groundtruth.add_moving_object();
    proto_pedestrian_ptr->set_type(astas_osi3::MovingObject::TYPE_PEDESTRIAN);

    FillBasicMovingObjectProperties(pedestrian_entity_ptr, proto_pedestrian_ptr);

    auto source_ref_ptr = proto_pedestrian_ptr->add_source_reference();
    FillSourceReference(pedestrian_entity_ptr, "Pedestrian", source_ref_ptr);
}

}  // namespace astas::environment::proto_groundtruth
