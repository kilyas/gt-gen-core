/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Environment/environment_converter.h"

#include <MantleAPI/EnvironmentalConditions/weather.h>

namespace astas::environment::proto_groundtruth
{

astas_osi3::EnvironmentalConditions_AmbientIllumination GetOSIAmbientIllumination(mantle_api::Illumination illumination)
{
    using proto_type = astas_osi3::EnvironmentalConditions_AmbientIllumination;

    switch (illumination)
    {
        case mantle_api::Illumination::kOther:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_OTHER;
        }
        case mantle_api::Illumination::kLevel1:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL1;
        }
        case mantle_api::Illumination::kLevel2:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL2;
        }
        case mantle_api::Illumination::kLevel3:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL3;
        }
        case mantle_api::Illumination::kLevel4:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL4;
        }
        case mantle_api::Illumination::kLevel5:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL5;
        }
        case mantle_api::Illumination::kLevel6:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL6;
        }
        case mantle_api::Illumination::kLevel7:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL7;
        }
        case mantle_api::Illumination::kLevel8:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL8;
        }
        case mantle_api::Illumination::kLevel9:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL9;
        }
        case mantle_api::Illumination::kUnknown:
        default:
        {
            return proto_type::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_UNKNOWN;
        }
    }
}

astas_osi3::EnvironmentalConditions_Precipitation GetOSIPrecipitation(mantle_api::Precipitation precipitation)
{
    using proto_type = astas_osi3::EnvironmentalConditions_Precipitation;

    switch (precipitation)
    {
        case mantle_api::Precipitation::kOther:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_OTHER;
        }
        case mantle_api::Precipitation::kNone:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_NONE;
        }
        case mantle_api::Precipitation::kVeryLight:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_LIGHT;
        }
        case mantle_api::Precipitation::kLight:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_LIGHT;
        }
        case mantle_api::Precipitation::kModerate:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_MODERATE;
        }
        case mantle_api::Precipitation::kHeavy:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_HEAVY;
        }
        case mantle_api::Precipitation::kVeryHeavy:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_HEAVY;
        }
        case mantle_api::Precipitation::kExtreme:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_EXTREME;
        }
        case mantle_api::Precipitation::kUnknown:
        default:
        {
            return proto_type::EnvironmentalConditions_Precipitation_PRECIPITATION_UNKNOWN;
        }
    }
}

astas_osi3::EnvironmentalConditions_Fog GetOSIFog(mantle_api::Fog fog)
{
    using proto_type = astas_osi3::EnvironmentalConditions_Fog;

    switch (fog)
    {
        case mantle_api::Fog::kOther:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_OTHER;
        }
        case mantle_api::Fog::kExcellentVisibility:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_EXCELLENT_VISIBILITY;
        }
        case mantle_api::Fog::kGoodVisibility:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_GOOD_VISIBILITY;
        }
        case mantle_api::Fog::kModerateVisibility:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_MODERATE_VISIBILITY;
        }
        case mantle_api::Fog::kPoorVisibility:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_POOR_VISIBILITY;
        }
        case mantle_api::Fog::kMist:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_MIST;
        }
        case mantle_api::Fog::kLight:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_LIGHT;
        }
        case mantle_api::Fog::kThick:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_THICK;
        }
        case mantle_api::Fog::kDense:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_DENSE;
        }
        case mantle_api::Fog::kUnknown:
        default:
        {
            return proto_type::EnvironmentalConditions_Fog_FOG_UNKNOWN;
        }
    }
}

}  // namespace astas::environment::proto_groundtruth
