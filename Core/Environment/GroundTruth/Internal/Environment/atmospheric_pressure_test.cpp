/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Environment/atmospheric_pressure.h"

#include <gtest/gtest.h>
#include <units.h>

namespace astas::environment::proto_groundtruth
{

TEST(AtmosphericPressureTest, GivenAltitude_WhenTicking_ThenAtmosphericPressureComputed)
{
    const double expected_atmospheric_pressure{95493.095};
    const double altitude{500.0};
    const units::temperature::kelvin_t temperature{288.15};

    const auto pressure = CalculateAtmosphericPressure(altitude, temperature);

    EXPECT_NEAR(pressure.value(), expected_atmospheric_pressure, 0.001);
}

TEST(AtmosphericPressureTest, GivenZeroKelvin_WhenTicking_ThenPressureSetToZero)
{
    const double zero_altitude{0.0};
    const units::temperature::kelvin_t zero_kelvin{0.0};
    const units::pressure::pascal_t expected_zero_pascal{0.0};

    const auto pressure = CalculateAtmosphericPressure(zero_altitude, zero_kelvin);

    EXPECT_EQ(expected_zero_pascal, pressure);
}

TEST(AtmosphericPressureTest, GivenNegativeTemperature_WhenTicking_ThenPressureSetToZero)
{
    const double zero_altitude{0.0};
    const units::temperature::kelvin_t minus_one_kelvin{-1.0};
    const units::pressure::pascal_t expected_zero_pascal{0.0};

    const auto pressure = CalculateAtmosphericPressure(zero_altitude, minus_one_kelvin);

    EXPECT_EQ(expected_zero_pascal, pressure);
}

}  // namespace astas::environment::proto_groundtruth
