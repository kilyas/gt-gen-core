/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ENVIRONMENTPROTOGROUNDTRUTHBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ENVIRONMENTPROTOGROUNDTRUTHBUILDER_H

#include "astas_osi_environment.pb.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/EnvironmentalConditions/weather.h>

namespace astas::environment::proto_groundtruth
{

class EnvironmentProtoGroundTruthBuilder
{
  public:
    explicit EnvironmentProtoGroundTruthBuilder(const mantle_api::Time& step_size);
    void FillEnvironment(astas_osi3::EnvironmentalConditions* proto_environment, double altitude);

    void SetWeather(const mantle_api::Weather& weather);
    void SetDateTime(const mantle_api::Time& date_time);
    mantle_api::Time GetDateTime() const;

  private:
    static std::uint32_t GetTimeOfDayInSeconds(mantle_api::Time date_time_ms);

    mantle_api::Time step_size_;
    mantle_api::Weather weather_{};
    mantle_api::Time date_time_{};
};

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ENVIRONMENTPROTOGROUNDTRUTHBUILDER_H
