/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ATMOSPHERICPRESSURE_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ATMOSPHERICPRESSURE_H

#include <units.h>

#include <cmath>
#include <cstdint>

namespace astas::environment::proto_groundtruth
{

inline units::pressure::pascal_t CalculateAtmosphericPressure(double height, units::temperature::kelvin_t temperature)
{
    const units::temperature::kelvin_t zero_kelvin{0};
    if (temperature <= zero_kelvin)
    {
        return units::pressure::pascal_t{0};
    }

    // Calculating atmospheric pressure depending on height,
    // https://en.wikipedia.org/wiki/Barometric_formula
    const std::uint32_t p_b{101325};  // static pressure (Pa)
    const float g{9.80665F};          // gravitational acceleration (m/s^2)
    const double m{0.0289644};        // molar mass of Earth's air (kg/mol)
    const double r{8.3144598};        // universal gas constant (J/mol K)

    return units::pressure::pascal_t(p_b * std::exp((-g * m * height) / (r * temperature.value())));
}

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ATMOSPHERICPRESSURE_H
