/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ENVIRONMENTCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ENVIRONMENTCONVERTER_H

#include "astas_osi_environment.pb.h"

#include <MantleAPI/EnvironmentalConditions/weather.h>

namespace astas::environment::proto_groundtruth
{
astas_osi3::EnvironmentalConditions_AmbientIllumination GetOSIAmbientIllumination(
    mantle_api::Illumination illumination);

astas_osi3::EnvironmentalConditions_Precipitation GetOSIPrecipitation(mantle_api::Precipitation precipitation);

astas_osi3::EnvironmentalConditions_Fog GetOSIFog(mantle_api::Fog fog);
}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_ENVIRONMENT_ENVIRONMENTCONVERTER_H
