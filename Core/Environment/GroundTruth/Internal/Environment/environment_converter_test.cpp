/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Environment/environment_converter.h"

#include <gtest/gtest.h>

#include <utility>

namespace astas::environment::proto_groundtruth
{

//
// EnvironmentConditions AmbientIllumination
//
class AmbientIlluminationOSIConverterTestFixture
    : public testing::TestWithParam<
          std::tuple<mantle_api::Illumination, astas_osi3::EnvironmentalConditions_AmbientIllumination>>
{
};

INSTANTIATE_TEST_SUITE_P(
    EnvironmentConverter,
    AmbientIlluminationOSIConverterTestFixture,
    testing::ValuesIn(
        std::vector<std::tuple<mantle_api::Illumination, astas_osi3::EnvironmentalConditions_AmbientIllumination>>{
            std::make_tuple(mantle_api::Illumination::kOther,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_OTHER),
            std::make_tuple(mantle_api::Illumination::kUnknown,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_UNKNOWN),
            std::make_tuple(mantle_api::Illumination::kLevel1,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL1),
            std::make_tuple(mantle_api::Illumination::kLevel2,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL2),
            std::make_tuple(mantle_api::Illumination::kLevel3,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL3),
            std::make_tuple(mantle_api::Illumination::kLevel4,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL4),
            std::make_tuple(mantle_api::Illumination::kLevel5,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL5),
            std::make_tuple(mantle_api::Illumination::kLevel6,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL6),
            std::make_tuple(mantle_api::Illumination::kLevel7,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL7),
            std::make_tuple(mantle_api::Illumination::kLevel8,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL8),
            std::make_tuple(mantle_api::Illumination::kLevel9,
                            astas_osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL9)}));

TEST_P(AmbientIlluminationOSIConverterTestFixture,
       GivenEnvironmentIllumination_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::Illumination astas_type = std::get<0>(GetParam());
    astas_osi3::EnvironmentalConditions_AmbientIllumination expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GetOSIAmbientIllumination(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// EnvironmentConditions Precipitation
//
class PrecipitationOSIConverterTestFixture
    : public testing::TestWithParam<
          std::tuple<mantle_api::Precipitation, astas_osi3::EnvironmentalConditions_Precipitation>>
{
};

INSTANTIATE_TEST_SUITE_P(
    EnvironmentConverter,
    PrecipitationOSIConverterTestFixture,
    testing::ValuesIn(
        std::vector<std::tuple<mantle_api::Precipitation, astas_osi3::EnvironmentalConditions_Precipitation>>{
            std::make_tuple(mantle_api::Precipitation::kOther,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_OTHER),
            std::make_tuple(mantle_api::Precipitation::kNone,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_NONE),
            std::make_tuple(mantle_api::Precipitation::kVeryLight,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_LIGHT),
            std::make_tuple(mantle_api::Precipitation::kLight,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_LIGHT),
            std::make_tuple(mantle_api::Precipitation::kModerate,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_MODERATE),
            std::make_tuple(mantle_api::Precipitation::kHeavy,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_HEAVY),
            std::make_tuple(mantle_api::Precipitation::kVeryHeavy,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_HEAVY),
            std::make_tuple(mantle_api::Precipitation::kExtreme,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_EXTREME),
            std::make_tuple(mantle_api::Precipitation::kUnknown,
                            astas_osi3::EnvironmentalConditions_Precipitation::
                                EnvironmentalConditions_Precipitation_PRECIPITATION_UNKNOWN)}));

TEST_P(PrecipitationOSIConverterTestFixture, GivenPrecipitation_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::Precipitation astas_type = std::get<0>(GetParam());
    astas_osi3::EnvironmentalConditions_Precipitation expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GetOSIPrecipitation(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// EnvironmentConditions Fog
//
class FogConverterTestFixture
    : public testing::TestWithParam<std::tuple<mantle_api::Fog, astas_osi3::EnvironmentalConditions_Fog>>
{
};

INSTANTIATE_TEST_SUITE_P(
    EnvironmentConverter,
    FogConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<mantle_api::Fog, astas_osi3::EnvironmentalConditions_Fog>>{
        std::make_tuple(mantle_api::Fog::kOther,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_OTHER),
        std::make_tuple(mantle_api::Fog::kUnknown,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_UNKNOWN),
        std::make_tuple(mantle_api::Fog::kExcellentVisibility,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_EXCELLENT_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kGoodVisibility,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_GOOD_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kModerateVisibility,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_MODERATE_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kPoorVisibility,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_POOR_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kMist,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_MIST),
        std::make_tuple(mantle_api::Fog::kLight,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_LIGHT),
        std::make_tuple(mantle_api::Fog::kThick,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_THICK),
        std::make_tuple(mantle_api::Fog::kDense,
                        astas_osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_DENSE)}));

TEST_P(FogConverterTestFixture, GivenFog_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::Fog astas_type = std::get<0>(GetParam());
    astas_osi3::EnvironmentalConditions_Fog expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GetOSIFog(astas_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

}  // namespace astas::environment::proto_groundtruth
