/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Environment/environment_proto_ground_truth_builder.h"

#include "Core/Environment/GroundTruth/Internal/Environment/atmospheric_pressure.h"
#include "Core/Environment/GroundTruth/Internal/Environment/environment_converter.h"

namespace astas::environment::proto_groundtruth
{

EnvironmentProtoGroundTruthBuilder::EnvironmentProtoGroundTruthBuilder(const mantle_api::Time& step_size)
    : step_size_{step_size}
{
}

void EnvironmentProtoGroundTruthBuilder::FillEnvironment(astas_osi3::EnvironmentalConditions* proto_environment,
                                                         double altitude)
{
    date_time_ += step_size_;
    weather_.atmospheric_pressure = CalculateAtmosphericPressure(altitude, weather_.temperature);

    proto_environment->set_precipitation(GetOSIPrecipitation(weather_.precipitation));
    proto_environment->set_fog(GetOSIFog(weather_.fog));
    proto_environment->set_ambient_illumination(GetOSIAmbientIllumination(weather_.illumination));
    proto_environment->mutable_time_of_day()->set_seconds_since_midnight(GetTimeOfDayInSeconds(date_time_));
    proto_environment->set_temperature(weather_.temperature());
    proto_environment->set_relative_humidity(weather_.humidity());
    proto_environment->set_atmospheric_pressure(weather_.atmospheric_pressure());
}

void EnvironmentProtoGroundTruthBuilder::SetWeather(const mantle_api::Weather& weather)
{
    weather_ = weather;
}

void EnvironmentProtoGroundTruthBuilder::SetDateTime(const mantle_api::Time& date_time)
{
    date_time_ = date_time;
}

mantle_api::Time EnvironmentProtoGroundTruthBuilder::GetDateTime() const
{
    return date_time_;
}

std::uint32_t EnvironmentProtoGroundTruthBuilder::GetTimeOfDayInSeconds(mantle_api::Time date_time_ms)
{
    return static_cast<std::uint32_t>(static_cast<std::int64_t>(date_time_ms() / 1000) % 86400);
}

}  // namespace astas::environment::proto_groundtruth
