/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTER_H

#include "Core/Environment/Map/AstasMap/road_objects.h"
#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace astas::environment::proto_groundtruth
{
/// @deprecated - remove when astas map dos not contain any map::RoadObjects anymore!
void FillProtoGroundTruthStationaryObject(const map::RoadObject& astas_road_object,
                                          astas_osi3::GroundTruth& groundtruth);

void FillProtoSourceReference(const mantle_api::IEntity* entity,
                              const std::string& entity_type,
                              astas_osi3::ExternalReference* external_ref);

void AddStationaryEntityToGroundTruth(const mantle_api::IEntity* entity, astas_osi3::GroundTruth& ground_truth);

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTER_H
