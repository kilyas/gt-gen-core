/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/traffic_light_proto_converter.h"

#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"

namespace astas::environment::proto_groundtruth
{

void FillProtoGroundTruthTrafficLight(const environment::map::TrafficLight* astas_traffic_light,
                                      astas_osi3::GroundTruth& groundtruth)
{
    for (const environment::map::TrafficLightBulb& astas_traffic_light_bulb : astas_traffic_light->light_bulbs)
    {
        auto proto_traffic_light = groundtruth.add_traffic_light();

        proto_traffic_light->mutable_id()->set_value(astas_traffic_light_bulb.id);

        service::gt_conversion::FillProtoObject(astas_traffic_light_bulb.pose.position,
                                                proto_traffic_light->mutable_base()->mutable_position());
        service::gt_conversion::FillProtoObject(astas_traffic_light_bulb.pose.orientation,
                                                proto_traffic_light->mutable_base()->mutable_orientation());
        service::gt_conversion::FillProtoObject(astas_traffic_light_bulb.dimensions,
                                                proto_traffic_light->mutable_base()->mutable_dimension());

        proto_traffic_light->mutable_classification()->set_color(
            static_cast<astas_osi3::TrafficLight::Classification::Color>(astas_traffic_light_bulb.color));
        proto_traffic_light->mutable_classification()->set_icon(
            static_cast<astas_osi3::TrafficLight::Classification::Icon>(astas_traffic_light_bulb.icon));
        proto_traffic_light->mutable_classification()->set_mode(
            static_cast<astas_osi3::TrafficLight::Classification::Mode>(astas_traffic_light_bulb.mode));
        proto_traffic_light->mutable_classification()->set_is_out_of_service(false);

        if (astas_traffic_light_bulb.mode == environment::map::OsiTrafficLightMode::kCounting)
        {
            proto_traffic_light->mutable_classification()->set_counter(astas_traffic_light_bulb.count);
        }

        auto proto_traffic_light_lane_ids = proto_traffic_light->mutable_classification()->mutable_assigned_lane_id();
        for (const auto& astas_lane_id : astas_traffic_light_bulb.assigned_lanes)
        {
            proto_traffic_light_lane_ids->Add()->set_value(astas_lane_id);
        }
    }
}

void AddTrafficLightEntityToGroundTruth(const mantle_api::IEntity* entity, astas_osi3::GroundTruth& ground_truth)
{
    const auto* traffic_light_entity = dynamic_cast<const entities::TrafficLightEntity*>(entity);

    if (traffic_light_entity == nullptr)
    {
        return;
    }

    auto properties = traffic_light_entity->GetProperties();
    for (const auto& bulb_properties : properties->bulbs)
    {
        auto* proto_traffic_light = ground_truth.add_traffic_light();
        auto* proto_base = proto_traffic_light->mutable_base();
        auto* proto_class = proto_traffic_light->mutable_classification();

        proto_traffic_light->mutable_id()->set_value(bulb_properties.id);
        auto bulb_position = traffic_light_entity->GetBulbPosition(bulb_properties.id);

        service::gt_conversion::FillProtoObject(bulb_position, proto_base->mutable_position());
        service::gt_conversion::FillProtoObject(entity->GetOrientation(), proto_base->mutable_orientation());
        service::gt_conversion::FillProtoObject(bulb_properties.bounding_box.dimension,
                                                proto_base->mutable_dimension());

        proto_class->set_color(static_cast<astas_osi3::TrafficLight::Classification::Color>(bulb_properties.color));
        proto_class->set_icon(static_cast<astas_osi3::TrafficLight::Classification::Icon>(bulb_properties.icon));
        proto_class->set_mode(static_cast<astas_osi3::TrafficLight::Classification::Mode>(bulb_properties.mode));
        proto_class->set_is_out_of_service(false);

        if (bulb_properties.mode == mantle_api::TrafficLightBulbMode::kCounting)
        {
            proto_class->set_counter(bulb_properties.count);
        }

        auto proto_traffic_light_lane_ids = proto_class->mutable_assigned_lane_id();
        for (const auto& astas_lane_id : entity->GetAssignedLaneIds())
        {
            proto_traffic_light_lane_ids->Add()->set_value(astas_lane_id);
        }
    }
}

}  // namespace astas::environment::proto_groundtruth
