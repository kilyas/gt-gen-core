/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/stationary_object_proto_converter.h"

#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{
using units::literals::operator""_m;
using units::literals::operator""_rad;

TEST(StationaryObjectProtoConverterTest, GivenRoadObject_WhenCovertingToStationaryObject_ThenConversionIsCorrect)
{
    map::RoadObject astas_road_object{};
    astas_road_object.id = 10;
    astas_road_object.name = "pylon";
    astas_road_object.type = osi::StationaryObjectEntityType::kConstructionSiteElement;
    astas_road_object.material = osi::StationaryObjectEntityMaterial::kPlastic;
    astas_road_object.dimensions = {0.5_m, 0.5_m, 0.7_m};
    astas_road_object.base_polygon = {{0_m, 0_m, 0_m}, {0.5_m, 0_m, 0_m}, {0.5_m, 0.5_m, 0_m}, {0_m, 0.5_m, 0_m}};
    astas_road_object.pose.position = {5_m, 0_m, 0_m};
    astas_road_object.pose.orientation = {0_rad, 3_rad, 0_rad};

    astas_osi3::GroundTruth ground_truth{};

    FillProtoGroundTruthStationaryObject(astas_road_object, ground_truth);

    ASSERT_EQ(1, ground_truth.stationary_object_size());
    auto& proto_road_object = ground_truth.stationary_object(0);

    EXPECT_EQ(proto_road_object.id().value(), astas_road_object.id);
    EXPECT_EQ(proto_road_object.model_reference(), astas_road_object.name);
    EXPECT_EQ(proto_road_object.classification().type(),
              static_cast<astas_osi3::StationaryObject::Classification::Type>(astas_road_object.type));
    EXPECT_EQ(proto_road_object.classification().material(),
              static_cast<astas_osi3::StationaryObject::Classification::Material>(astas_road_object.material));
    EXPECT_EQ(proto_road_object.base().dimension().length(), astas_road_object.dimensions.length());
    EXPECT_EQ(proto_road_object.base().dimension().width(), astas_road_object.dimensions.width());
    EXPECT_EQ(proto_road_object.base().dimension().height(), astas_road_object.dimensions.height());

    for (int i = 0; i < proto_road_object.base().base_polygon().size(); ++i)
    {
        const auto& astas_base_poly = astas_road_object.base_polygon.at(static_cast<std::size_t>(i));
        const auto& proto_base_poly = proto_road_object.base().base_polygon().Get(i);
        EXPECT_EQ(proto_base_poly.x(), astas_base_poly.x());
        EXPECT_EQ(proto_base_poly.y(), astas_base_poly.y());
    }

    EXPECT_EQ(proto_road_object.base().position().x(), astas_road_object.pose.position.x());  // only x is set
    EXPECT_EQ(proto_road_object.base().orientation().yaw(),
              astas_road_object.pose.orientation.yaw());  // only yaw is set
}

TEST(StationaryObjectProtoConverterTest,
     GivenStationaryObjectEntity_WhenAddStationaryEntityToGroundTruth_ThenGroundTruthCorrectlyFilled)
{
    mantle_ext::StationaryObjectProperties properties;
    properties.bounding_box.dimension = {0.5_m, 0.5_m, 0.7_m};
    properties.type = mantle_api::EntityType::kStatic;
    properties.object_type = osi::StationaryObjectEntityType::kConstructionSiteElement;
    properties.material = osi::StationaryObjectEntityMaterial::kPlastic;
    properties.base_polygon = {{0_m, 0_m, 0_m}, {0.5_m, 0_m, 0_m}, {0.5_m, 0.5_m, 0_m}, {0_m, 0.5_m, 0_m}};

    auto static_object_entity = std::make_unique<mantle_api::MockStaticObject>();
    static_object_entity->SetProperties(std::make_unique<mantle_ext::StationaryObjectProperties>(properties));
    static_object_entity->SetName("StaticObjectName");

    EXPECT_CALL(*static_object_entity, GetUniqueId()).WillRepeatedly(testing::Return(10));
    EXPECT_CALL(*static_object_entity, GetPosition())
        .WillRepeatedly(testing::Return(mantle_api::Vec3<units::length::meter_t>{5_m, 2_m, 3_m}));
    EXPECT_CALL(*static_object_entity, GetOrientation())
        .WillRepeatedly(testing::Return(mantle_api::Orientation3<units::angle::radian_t>{3_rad, 7_rad, 8_rad}));

    astas_osi3::GroundTruth ground_truth{};

    AddStationaryEntityToGroundTruth(static_object_entity.get(), ground_truth);

    ASSERT_EQ(1, ground_truth.stationary_object_size());
    const auto& proto_road_object = ground_truth.stationary_object(0);
    const auto& proto_base = proto_road_object.base();
    const auto& proto_dim = proto_base.dimension();

    EXPECT_EQ(proto_road_object.id().value(), static_object_entity->GetUniqueId());
    EXPECT_EQ(proto_road_object.model_reference(), properties.model);
    EXPECT_TRIPLE(service::gt_conversion::ToVec3Length(proto_base.position()), static_object_entity->GetPosition());
    EXPECT_TRIPLE(service::gt_conversion::ToOrientation3(proto_base.orientation()),
                  static_object_entity->GetOrientation());
    EXPECT_TRIPLE(service::gt_conversion::ToDimension3(proto_dim), properties.bounding_box.dimension);
    EXPECT_EQ("MiscObject", proto_road_object.source_reference().begin()->identifier(0));
    EXPECT_EQ(static_object_entity->GetName(), proto_road_object.source_reference().begin()->identifier(1));

    EXPECT_EQ(proto_road_object.classification().type(),
              static_cast<astas_osi3::StationaryObject::Classification::Type>(properties.object_type));
    EXPECT_EQ(proto_road_object.classification().material(),
              static_cast<astas_osi3::StationaryObject::Classification::Material>(properties.material));

    for (int i = 0; i < proto_road_object.base().base_polygon().size(); ++i)
    {
        const auto& astas_base_poly = properties.base_polygon.at(static_cast<std::size_t>(i));
        const auto& proto_base_poly = proto_road_object.base().base_polygon().Get(i);
        EXPECT_EQ(proto_base_poly.x(), astas_base_poly.x());
        EXPECT_EQ(proto_base_poly.y(), astas_base_poly.y());
    }
}

}  // namespace astas::environment::proto_groundtruth
