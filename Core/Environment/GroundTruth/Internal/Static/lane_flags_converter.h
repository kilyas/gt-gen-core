/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANEFLAGSCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANEFLAGSCONVERTER_H

#include "Core/Environment/Map/AstasMap/lane.h"
#include "astas_osi_lane.pb.h"

namespace astas::environment::proto_groundtruth
{
void AddLaneFlags(const map::Lane& lane, astas_osi3::Lane& gt_lane);

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_LANEFLAGSCONVERTER_H
