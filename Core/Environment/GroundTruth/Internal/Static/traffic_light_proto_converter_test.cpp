/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/traffic_light_proto_converter.h"

#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{
using units::literals::operator""_m;

// Note: This test covers only the new conversion code (mantle_api), the old code is tested somewhere in
// static_proto_ground_truth_builder_test

TEST(TrafficLightProtoConverterTest,
     GivenTrafficLight_WhenAddTrafficLightEntityToGroundTruth_ThenGroundTruthFilledCorrectly)
{
    std::vector<mantle_api::UniqueId> expected_bulb_ids{1, 2, 3};
    std::vector<mantle_api::Vec3<units::dimensionless::scalar_t>> expected_offset_coeffs{
        {0, 0, 1.0 / 3.0}, {0, 0, 0}, {0, 0, -1.0 / 3.0}};
    std::vector<mantle_api::Vec3<units::length::meter_t>> expected_positions{
        {5_m, 1_m, 2.5_m}, {5_m, 1_m, 2.0_m}, {5_m, 1_m, 1.5_m}};
    std::vector<mantle_api::Dimension3> expected_dimensions{
        {0.5_m, 0.5_m, 0.5_m}, {0.5_m, 0.5_m, 0.5_m}, {0.5_m, 0.5_m, 0.5_m}};
    std::vector<mantle_api::TrafficLightBulbColor> expected_colors{mantle_api::TrafficLightBulbColor::kRed,
                                                                   mantle_api::TrafficLightBulbColor::kYellow,
                                                                   mantle_api::TrafficLightBulbColor::kGreen};
    std::vector<osi::OsiTrafficLightIcon> expected_icons{
        osi::OsiTrafficLightIcon::kPedestrian, osi::OsiTrafficLightIcon::kNone, osi::OsiTrafficLightIcon::kArrowLeft};
    std::vector<mantle_api::TrafficLightBulbMode> expected_modes{mantle_api::TrafficLightBulbMode::kCounting,
                                                                 mantle_api::TrafficLightBulbMode::kConstant,
                                                                 mantle_api::TrafficLightBulbMode::kFlashing};
    std::vector<double> expected_count{42, 0, 0};

    mantle_ext::TrafficLightProperties traffic_light_properties;
    traffic_light_properties.bounding_box.dimension = {0.5_m, 0.5_m, 1.5_m};

    for (std::size_t i = 0; i < 3; i++)
    {
        mantle_ext::TrafficLightBulbProperties bulb_properties;
        bulb_properties.id = expected_bulb_ids.at(i);
        bulb_properties.bounding_box.dimension = expected_dimensions.at(i);
        bulb_properties.position_offset_coeffs = expected_offset_coeffs.at(i);
        bulb_properties.color = expected_colors.at(i);
        bulb_properties.icon = expected_icons.at(i);
        bulb_properties.mode = expected_modes.at(i);
        bulb_properties.count = expected_count.at(i);

        traffic_light_properties.bulbs.push_back(bulb_properties);
    }

    auto traffic_light_entity = std::make_unique<entities::TrafficLightEntity>(10, "traffic_light");
    traffic_light_entity->SetProperties(std::make_unique<mantle_ext::TrafficLightProperties>(traffic_light_properties));
    traffic_light_entity->SetAssignedLaneIds({42, 21});
    traffic_light_entity->SetPosition({5_m, 1_m, 2_m});

    astas_osi3::GroundTruth ground_truth{};

    AddTrafficLightEntityToGroundTruth(traffic_light_entity.get(), ground_truth);

    ASSERT_EQ(3, ground_truth.traffic_light_size());
    for (int i = 0; i < ground_truth.traffic_light_size(); i++)
    {
        std::size_t idx = static_cast<std::size_t>(i);
        const auto& traffic_light_bulb = ground_truth.traffic_light(i);
        const auto& proto_class = traffic_light_bulb.classification();
        const auto& proto_base = traffic_light_bulb.base();
        const auto& proto_dim = proto_base.dimension();

        EXPECT_EQ(traffic_light_bulb.id().value(), expected_bulb_ids.at(idx));
        EXPECT_TRIPLE(expected_positions.at(idx), service::gt_conversion::ToVec3Length(proto_base.position()));
        EXPECT_TRIPLE(traffic_light_entity->GetOrientation(),
                      service::gt_conversion::ToOrientation3(proto_base.orientation()));
        EXPECT_TRIPLE(expected_dimensions.at(idx), service::gt_conversion::ToDimension3(proto_dim));

        EXPECT_EQ(proto_class.color(),
                  static_cast<astas_osi3::TrafficLight::Classification::Color>(expected_colors.at(idx)));
        EXPECT_EQ(proto_class.icon(),
                  static_cast<astas_osi3::TrafficLight::Classification::Icon>(expected_icons.at(idx)));
        EXPECT_EQ(proto_class.mode(),
                  static_cast<astas_osi3::TrafficLight::Classification::Mode>(expected_modes.at(idx)));
        EXPECT_EQ(proto_class.counter(), expected_count.at(idx));

        EXPECT_EQ(proto_class.is_out_of_service(), false);  // always false

        ASSERT_EQ(2, proto_class.assigned_lane_id_size());
        EXPECT_EQ(42, proto_class.assigned_lane_id(0).value());
        EXPECT_EQ(21, proto_class.assigned_lane_id(1).value());
    }
}

}  // namespace astas::environment::proto_groundtruth
