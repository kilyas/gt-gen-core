/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/lane_flags_converter.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{
// -------- Lane Classification Type ----------------------------------------------------------

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithDrivableSet_WhenAddLaneFlags_ThenLaneTypeIsDriving)
{
    map::Lane lane{0};
    lane.flags.SetDrivable();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Type_TYPE_DRIVING, gt_lane.classification().type());
}

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithDrivableNotSet_WhenAddLaneFlags_ThenLaneTypeIsNonDriving)
{
    map::Lane lane{0};

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Type_TYPE_NONDRIVING, gt_lane.classification().type());
}

// -------- Lane Classification Sub-Type ----------------------------------------------------------

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithShoulderLaneSet_WhenAddLaneFlags_ThenLaneSubTypeIsShoulder)
{
    map::Lane lane{0};
    lane.flags.SetShoulderLane();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_SHOULDER, gt_lane.classification().subtype());
}

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithMergeLaneSet_WhenAddLaneFlags_ThenLaneSubTypeIsEntry)
{
    map::Lane lane{0};
    lane.flags.SetMergeLane();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_ENTRY, gt_lane.classification().subtype());
}

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithSplitLaneSet_WhenAddLaneFlags_ThenLaneSubTypeIsExit)
{
    map::Lane lane{0};
    lane.flags.SetSplitLane();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_EXIT, gt_lane.classification().subtype());
}

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithNormalLaneSet_WhenAddLaneFlags_ThenLaneSubTypeIsNormal)
{
    map::Lane lane{0};
    lane.flags.SetNormalLane();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL, gt_lane.classification().subtype());
}

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithExitLaneSet_WhenAddLaneFlags_ThenLaneSubTypeIsExit)
{
    map::Lane lane{0};
    lane.flags.SetExitLane();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_EXIT, gt_lane.classification().subtype());
}

// Continue-Flag is currently not supported in OSI
TEST(LaneFlagsConverterTest, GivenLaneFlagsWithContinueSet_WhenAddLaneFlags_ThenLaneSubTypeIsOther)
{
    map::Lane lane{0};
    lane.flags.SetContinue();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_OTHER, gt_lane.classification().subtype());
}

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithParkingSet_WhenAddLaneFlags_ThenLaneSubTypeIs)
{
    map::Lane lane{0};
    lane.flags.SetParking();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_PARKING, gt_lane.classification().subtype());
}

TEST(LaneFlagsConverterTest, GivenLaneFlagsWithBicycleSet_WhenAddLaneFlags_ThenLaneSubTypeIs)
{
    map::Lane lane{0};
    lane.flags.SetBicycle();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_BIKING, gt_lane.classification().subtype());
}

// Bus-Flag is currently not supported in OSI
TEST(LaneFlagsConverterTest, GivenLaneFlagsWithBusSet_WhenAddLaneFlags_ThenLaneSubTypeIsOther)
{
    map::Lane lane{0};
    lane.flags.SetBus();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_OTHER, gt_lane.classification().subtype());
}

// FullyAttributed-Flag is currently not supported in OSI - makes this sense at all to encode it in GT?!
TEST(LaneFlagsConverterTest, GivenLaneFlagsWithFullyAttributedSet_WhenAddLaneFlags_ThenLaneSubTypeIsOther)
{
    map::Lane lane{0};
    lane.flags.SetFullyAttributed();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_OTHER, gt_lane.classification().subtype());
}

TEST(LaneFlagsConverterTest, GivenLaneWithShoulderAndMergeFlagSet_WhenAddLaneFlags_ThenSoulderFlagWins)
{
    map::Lane lane{0};
    lane.flags.SetShoulderLane();
    lane.flags.SetMergeLane();

    astas_osi3::Lane gt_lane;
    AddLaneFlags(lane, gt_lane);

    EXPECT_EQ(astas_osi3::Lane_Classification_Subtype_SUBTYPE_SHOULDER, gt_lane.classification().subtype());
}

}  // namespace astas::environment::proto_groundtruth
