/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICSIGNPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICSIGNPROTOCONVERTER_H

#include "Core/Environment/Map/AstasMap/signs.h"
#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace astas::environment::proto_groundtruth
{
/// @deprecated - remove when legacy code is deleted - and the astas map does contain only mantle entities
void FillProtoGroundTruthTrafficSign(const environment::map::TrafficSign* astas_traffic_sign,
                                     astas_osi3::GroundTruth& groundtruth);

void AddTrafficSignEntityToGroundTruth(const mantle_api::IEntity* entity, astas_osi3::GroundTruth& ground_truth);

/// @deprecated - remove when legacy code is deleted - and the astas map does contain only mantle entities
void FillProtoGroundTruthRoadMarking(const environment::map::GroundSign* astas_ground_sign,
                                     astas_osi3::GroundTruth& groundtruth);

void AddRoadMarkingEntityToGroundTruth(const mantle_api::IEntity* entity, astas_osi3::GroundTruth& ground_truth);

astas_osi3::RoadMarking_Classification_Color ConvertAstasRoadMarkingColorToOsi(
    const osi::OsiRoadMarkingColor& astas_marking_color,
    const mantle_api::UniqueId& road_marking_id);

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICSIGNPROTOCONVERTER_H
