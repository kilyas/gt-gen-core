/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/traffic_sign_proto_converter.h"

#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{
using units::literals::operator""_m;
using units::literals::operator""_rad;

// Note: This test covers only the new conversion code (mantle_api), the old code is tested somewhere in
// static_proto_ground_truth_builder_test

TEST(TrafficSignProtoConverterTest,
     GivenTrafficSign_WhenAddTrafficSignEntityToGroundTruth_ThenGroundTruthFilledCorrectly)
{
    mantle_ext::TrafficSignProperties properties;
    properties.bounding_box.dimension = {0.2_m, 0.3_m, 0.4_m};
    properties.type = mantle_api::EntityType::kStatic;
    properties.sign_type = osi::OsiTrafficSignType::kSpeedLimitBegin;
    properties.value = 60;
    properties.vertical_offset = 2.2_m;
    properties.unit = osi::OsiTrafficSignValueUnit::kKilometerPerHour;
    properties.direction = osi::OsiTrafficSignDirectionScope::kNoDirection;
    properties.variability = osi::OsiTrafficSignVariability::kOther;
    properties.text = "text";

    auto traffic_sign_entity = std::make_unique<mantle_api::MockStaticObject>();
    traffic_sign_entity->SetProperties(std::make_unique<mantle_ext::TrafficSignProperties>(properties));

    EXPECT_CALL(*traffic_sign_entity, GetUniqueId()).WillRepeatedly(testing::Return(10));
    EXPECT_CALL(*traffic_sign_entity, GetPosition())
        .WillRepeatedly(testing::Return(mantle_api::Vec3<units::length::meter_t>{5_m, 1_m, 2_m}));
    EXPECT_CALL(*traffic_sign_entity, GetOrientation())
        .WillRepeatedly(testing::Return(mantle_api::Orientation3<units::angle::radian_t>{3_rad, 4_rad, 9_rad}));
    EXPECT_CALL(*traffic_sign_entity, GetAssignedLaneIds())
        .WillRepeatedly(testing::Return(std::vector<mantle_api::UniqueId>{42, 21}));

    astas_osi3::GroundTruth ground_truth{};

    AddTrafficSignEntityToGroundTruth(traffic_sign_entity.get(), ground_truth);

    ASSERT_EQ(1, ground_truth.traffic_sign_size());
    const auto& proto_traffic_sign = ground_truth.traffic_sign(0);
    const auto& proto_main_sign = proto_traffic_sign.main_sign();
    const auto& proto_class = proto_main_sign.classification();
    const auto& proto_main_sign_base = proto_main_sign.base();
    const auto& proto_dim = proto_main_sign_base.dimension();

    EXPECT_EQ(proto_traffic_sign.id().value(), traffic_sign_entity->GetUniqueId());
    EXPECT_TRIPLE(service::gt_conversion::ToVec3Length(proto_main_sign_base.position()),
                  traffic_sign_entity->GetPosition());
    EXPECT_TRIPLE(service::gt_conversion::ToOrientation3(proto_main_sign_base.orientation()),
                  traffic_sign_entity->GetOrientation());
    EXPECT_TRIPLE(service::gt_conversion::ToDimension3(proto_dim), properties.bounding_box.dimension);

    EXPECT_EQ(proto_class.type(),
              static_cast<astas_osi3::TrafficSign::MainSign::Classification::Type>(properties.sign_type));
    EXPECT_EQ(proto_class.variability(), static_cast<astas_osi3::TrafficSign::Variability>(properties.variability));
    EXPECT_EQ(proto_class.direction_scope(),
              static_cast<astas_osi3::TrafficSign::MainSign::Classification::DirectionScope>(properties.direction));
    EXPECT_EQ(proto_class.value().value_unit(), static_cast<astas_osi3::TrafficSignValue::Unit>(properties.unit));
    EXPECT_EQ(proto_class.value().value(), properties.value);

    ASSERT_EQ(2, proto_class.assigned_lane_id_size());
    EXPECT_EQ(42, proto_class.assigned_lane_id(0).value());
    EXPECT_EQ(21, proto_class.assigned_lane_id(1).value());
}

}  // namespace astas::environment::proto_groundtruth
