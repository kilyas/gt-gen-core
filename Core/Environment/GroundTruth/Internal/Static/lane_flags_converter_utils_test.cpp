/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/lane_flags_converter_utils.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{

// Lane flag type
TEST(LaneFlagTypeTestFixture, GivenLaneFlag_WhenGetLaneFlagType_ThenOsiLaneClassificationTypeReturned)
{
    map::LaneFlags lane_flag;
    lane_flag.SetDrivable();
    auto actual_osi_lane_classification_type = GetLaneFlagType(lane_flag);
    EXPECT_EQ(astas_osi3::Lane_Classification_Type_TYPE_DRIVING, actual_osi_lane_classification_type);

    lane_flag.SetNonDrivable();
    actual_osi_lane_classification_type = GetLaneFlagType(lane_flag);
    EXPECT_EQ(astas_osi3::Lane_Classification_Type_TYPE_NONDRIVING, actual_osi_lane_classification_type);
}

// Lane flag sub type
class LaneFlagSubTypeTestFixture
    : public ::testing::TestWithParam<std::pair<map::LaneFlags::Flags, astas_osi3::Lane_Classification_Subtype>>
{
  protected:
    map::LaneFlags CreateLaneFlagWithSubType(const map::LaneFlags::Flags& flag)
    {
        map::LaneFlags lane_flag;
        switch (flag)
        {
            case map::LaneFlags::Flags::kShoulder:
                lane_flag.SetShoulderLane();
                break;
            case map::LaneFlags::Flags::kMergeLane:
                lane_flag.SetMergeLane();
                break;
            case map::LaneFlags::Flags::kSplitLane:
                lane_flag.SetSplitLane();
                break;
            case map::LaneFlags::Flags::kExitLane:
                lane_flag.SetExitLane();
                break;
            case map::LaneFlags::Flags::kNormalLane:
                lane_flag.SetNormalLane();
                break;
            case map::LaneFlags::Flags::kParking:
                lane_flag.SetParking();
                break;
            case map::LaneFlags::Flags::kBicycle:
                lane_flag.SetBicycle();
                break;
            case map::LaneFlags::Flags::kBus:
                lane_flag.SetBus();
                break;
            case map::LaneFlags::Flags::kContinue:
                lane_flag.SetContinue();
                break;
            case map::LaneFlags::Flags::kFullyAttributed:
                lane_flag.SetFullyAttributed();
                break;
            default:
                break;
        }
        return lane_flag;
    }
};

INSTANTIATE_TEST_SUITE_P(
    LaneFlagSubTypeConverterUtilsTest,
    LaneFlagSubTypeTestFixture,
    ::testing::Values(
        std::make_pair(map::LaneFlags::Flags::kShoulder,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_SHOULDER),
        std::make_pair(map::LaneFlags::Flags::kMergeLane,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_ENTRY),
        std::make_pair(map::LaneFlags::Flags::kSplitLane,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_EXIT),
        std::make_pair(map::LaneFlags::Flags::kExitLane,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_EXIT),
        std::make_pair(map::LaneFlags::Flags::kNormalLane,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_NORMAL),
        std::make_pair(map::LaneFlags::Flags::kParking,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_PARKING),
        std::make_pair(map::LaneFlags::Flags::kBicycle,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_BIKING),
        std::make_pair(map::LaneFlags::Flags::kBus,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_OTHER),
        std::make_pair(map::LaneFlags::Flags::kContinue,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_OTHER),
        std::make_pair(map::LaneFlags::Flags::kFullyAttributed,
                       astas_osi3::Lane_Classification_Subtype::Lane_Classification_Subtype_SUBTYPE_OTHER)));

TEST_P(LaneFlagSubTypeTestFixture, GivenLaneFlag_WhenGetLaneFlagSubType_ThenOsiLaneClassificationSubTypeReturned)
{
    auto lane_flag_type = GetParam().first;
    auto expected_osi_lane_classification_subtype = GetParam().second;
    const auto& lane_flag = CreateLaneFlagWithSubType(lane_flag_type);
    auto actual_osi_lane_classification_subtype = GetLaneFlagSubType(lane_flag);
    EXPECT_EQ(expected_osi_lane_classification_subtype, actual_osi_lane_classification_subtype);
}
}  // namespace astas::environment::proto_groundtruth
