/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/stationary_object_proto_converter.h"

#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace astas::environment::proto_groundtruth
{

template <typename AstasVector2, typename ProtoVector2>
void FillProtoVector2(const AstasVector2& astas_vector2, ProtoVector2* osi_vector2)
{
    osi_vector2->set_x(astas_vector2.x());
    osi_vector2->set_y(astas_vector2.y());
}

void FillProtoSourceReference(const mantle_api::IEntity* entity,
                              const std::string& entity_type,
                              astas_osi3::ExternalReference* external_ref)
{
    external_ref->add_identifier(entity_type.c_str());
    external_ref->add_identifier(entity->GetName().c_str());
}

void FillProtoGroundTruthStationaryObject(const map::RoadObject& astas_road_object,
                                          astas_osi3::GroundTruth& groundtruth)
{
    auto proto_stationary_object = groundtruth.add_stationary_object();

    proto_stationary_object->mutable_id()->set_value(astas_road_object.id);

    switch (astas_road_object.type)
    {
        default:
        {
            proto_stationary_object->mutable_classification()->set_type(
                static_cast<astas_osi3::StationaryObject::Classification::Type>(astas_road_object.type));
        }
    }

    proto_stationary_object->mutable_classification()->set_material(
        static_cast<astas_osi3::StationaryObject::Classification::Material>(astas_road_object.material));

    service::gt_conversion::FillProtoObject(astas_road_object.pose.position,
                                            proto_stationary_object->mutable_base()->mutable_position());
    service::gt_conversion::FillProtoObject(astas_road_object.dimensions,
                                            proto_stationary_object->mutable_base()->mutable_dimension());

    auto proto_stationary_object_polygon = proto_stationary_object->mutable_base()->mutable_base_polygon();
    for (const auto& pos : astas_road_object.base_polygon)
    {
        FillProtoVector2(pos, proto_stationary_object_polygon->Add());
    }

    service::gt_conversion::FillProtoObject(astas_road_object.pose.orientation,
                                            proto_stationary_object->mutable_base()->mutable_orientation());

    proto_stationary_object->set_model_reference(astas_road_object.name);
}

void AddStationaryEntityToGroundTruth(const mantle_api::IEntity* entity, astas_osi3::GroundTruth& ground_truth)
{
    auto* properties = dynamic_cast<mantle_api::StaticObjectProperties*>(entity->GetProperties());

    if (properties == nullptr)
    {
        return;
    }

    auto* proto_stationary_object = ground_truth.add_stationary_object();
    auto* proto_base = proto_stationary_object->mutable_base();

    auto* proto_class = proto_stationary_object->mutable_classification();

    proto_stationary_object->mutable_id()->set_value(entity->GetUniqueId());
    service::gt_conversion::FillProtoObject(entity->GetPosition(), proto_base->mutable_position());
    service::gt_conversion::FillProtoObject(entity->GetOrientation(), proto_base->mutable_orientation());
    service::gt_conversion::FillProtoObject(entity->GetProperties()->bounding_box.dimension,
                                            proto_base->mutable_dimension());

    proto_stationary_object->set_model_reference(properties->model);

    auto source_ref_ptr = proto_stationary_object->add_source_reference();
    FillProtoSourceReference(entity, "MiscObject", source_ref_ptr);

    auto* stationary_object_properties = dynamic_cast<mantle_ext::StationaryObjectProperties*>(entity->GetProperties());
    if (stationary_object_properties != nullptr)
    {
        auto* proto_stationary_object_polygon = proto_base->mutable_base_polygon();
        proto_class->set_material(static_cast<astas_osi3::StationaryObject::Classification::Material>(
            stationary_object_properties->material));

        switch (stationary_object_properties->object_type)
        {
            default:
            {
                proto_class->set_type(static_cast<astas_osi3::StationaryObject::Classification::Type>(
                    stationary_object_properties->object_type));
            }
        }

        for (const auto& pos : stationary_object_properties->base_polygon)
        {
            FillProtoVector2(pos, proto_stationary_object_polygon->Add());
        }
    }
}

}  // namespace astas::environment::proto_groundtruth
