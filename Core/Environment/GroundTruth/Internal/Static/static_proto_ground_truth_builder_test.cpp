/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/static_proto_ground_truth_builder.h"

#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"
#include "Core/Tests/TestUtils/MapUtils/astas_map_builder.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"
#include "astas_osi_common.pb.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{
using units::literals::operator""_rad;
using units::literals::operator""_m;

class StaticProtoGroundTruthBuilderTest : public testing::Test
{
  public:
    StaticProtoGroundTruthBuilderTest() : astas_map_(astas_map_builder_.astas_map) {}

  protected:
    void AddLanesAndLaneGroups()
    {
        const mantle_api::UniqueId lane_group_id = 0;
        test_utils::SetupBasicLaneGroup(lane_group_id, astas_map_);

        astas_map_builder_.SetIds(lane_group_id, 69);
        astas_map_builder_.SetDrivable(true);
        astas_map_builder_.SetCenterLine({{1_m, 2_m, 3_m}, {4_m, 5_m, 6_m}});
        astas_map_builder_.SetPredecessors({3, 5});
        astas_map_builder_.SetSuccessors({2, 4});
        astas_map_builder_.SetLeftAdjacentLanes({6, 8});
        astas_map_builder_.SetRightAdjacentLanes({7, 9});
        astas_map_builder_.SetLeftLaneBoundaries({42, 69});
        astas_map_builder_.SetRightLaneBoundaries({96, 24});
        astas_map_builder_.CreateLane();

        // Actually create the boundaries
        map::LaneBoundary::Points left_boundary_points{{{7_m, 8_m, 9_m}, 0, 0}, {{10_m, 11_m, 12_m}, 0, 0}};
        map::LaneBoundary::Points right_boundary_points{{{13_m, 14_m, 15_m}, 0, 0}, {{16_m, 17_m, 18_m}, 0, 0}};

        auto& lane_group = astas_map_.GetLaneGroup(lane_group_id);

        astas_map_.AddLaneBoundary(
            lane_group_id,
            {42, map::LaneBoundary::Type::kGrassEdge, map::LaneBoundary::Color::kBlue, left_boundary_points});

        astas_map_.AddLaneBoundary(
            lane_group_id, {69, map::LaneBoundary::Type::kCurb, map::LaneBoundary::Color::kBlue, left_boundary_points});

        astas_map_.AddLaneBoundary(
            lane_group_id,
            {96, map::LaneBoundary::Type::kCurb, map::LaneBoundary::Color::kBlue, right_boundary_points});

        astas_map_.AddLaneBoundary(
            lane_group_id,
            {24, map::LaneBoundary::Type::kGrassEdge, map::LaneBoundary::Color::kBlue, right_boundary_points});

        // add left / right that should exist in test
        map::Lane dummy_lane{0};
        dummy_lane.id = 6;
        astas_map_.AddLane(lane_group.id, dummy_lane);
        dummy_lane.id = 9;
        astas_map_.AddLane(lane_group.id, dummy_lane);

        auto second_lane_group = map::LaneGroup{lane_group_id + 1, map::LaneGroup::Type::kUnknown};

        astas_map_.AddLaneGroup(second_lane_group);

        dummy_lane.id = 2;
        astas_map_.AddLane(second_lane_group.id, dummy_lane);
        dummy_lane.id = 3;
        astas_map_.AddLane(second_lane_group.id, dummy_lane);
    }

    void AddTrafficSigns()
    {
        // Create mounted sign (with supplementary signs)
        std::shared_ptr<map::MountedSign> mounted_sign{new map::MountedSign};

        mounted_sign->type = osi::OsiTrafficSignType::kOther;
        mounted_sign->variability = osi::OsiTrafficSignVariability::kVariable;
        mounted_sign->direction_scope = osi::OsiTrafficSignDirectionScope::kRight;
        mounted_sign->value_information =
            map::SignValueInformation{"", 30.0, osi::OsiTrafficSignValueUnit::kKilometerPerHour};
        mounted_sign->id = 10;
        mounted_sign->dimensions = mantle_api::Dimension3{0.1_m, 0.2_m, 0.3_m};
        mounted_sign->pose = mantle_api::Pose{mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m},
                                              mantle_api::Orientation3<units::angle::radian_t>{2_rad, 1_rad, 3_rad}};
        mounted_sign->stvo_id = "69";
        mounted_sign->assigned_lanes = {123, 321};

        std::vector<map::OsiSupplementaryTrafficSignActor> supplementary_sign_actors{};
        supplementary_sign_actors.push_back(map::OsiSupplementaryTrafficSignActor::kNoActor);
        supplementary_sign_actors.push_back(map::OsiSupplementaryTrafficSignActor::kCarsWithTrailers);
        supplementary_sign_actors.push_back(map::OsiSupplementaryTrafficSignActor::kTrucks);
        supplementary_sign_actors.push_back(map::OsiSupplementaryTrafficSignActor::kBuses);

        std::vector<map::SignValueInformation> supplementary_sign_values{
            map::SignValueInformation{"TestText", 2.0, osi::OsiTrafficSignValueUnit::kKilometer},
            map::SignValueInformation{"", 10.0, osi::OsiTrafficSignValueUnit::kMeter}};

        mounted_sign->supplementary_signs = std::vector<map::MountedSign::SupplementarySign>{
            map::MountedSign::SupplementarySign{mounted_sign.get(),
                                                osi::OsiTrafficSignVariability::kFixed,
                                                supplementary_sign_actors,
                                                map::OsiSupplementarySignType::kOther,
                                                supplementary_sign_values,
                                                mounted_sign->pose,
                                                mounted_sign->dimensions}};

        // Create ground sign
        std::shared_ptr<map::GroundSign> ground_sign{new map::GroundSign};

        ground_sign->marking_type = osi::OsiRoadMarkingsType::kPaintedTrafficSign;
        ground_sign->marking_color = osi::OsiRoadMarkingColor::kRed;
        ground_sign->type = osi::OsiTrafficSignType::kOther;
        ground_sign->variability = osi::OsiTrafficSignVariability::kFixed;
        ground_sign->direction_scope = osi::OsiTrafficSignDirectionScope::kLeft;
        ground_sign->value_information = map::SignValueInformation{"", 15.0, osi::OsiTrafficSignValueUnit::kMetricTon};
        ground_sign->id = 5;
        ground_sign->dimensions = mantle_api::Dimension3{0.1_m, 0.2_m, 0.3_m};
        ground_sign->pose = mantle_api::Pose{mantle_api::Vec3<units::length::meter_t>{0.6_m, 0.7_m, 0.8_m},
                                             mantle_api::Orientation3<units::angle::radian_t>{5_rad, 4_rad, 6_rad}};
        ground_sign->stvo_id = "42";

        astas_map_.traffic_signs.push_back(mounted_sign);
        astas_map_.traffic_signs.push_back(ground_sign);
    }

    void AddTrafficLights()
    {
        map::TrafficLight traffic_light{};
        traffic_light.id = 101;

        map::TrafficLightBulb off_red_bulb{};
        off_red_bulb.id = 102;
        off_red_bulb.pose = {{0.5_m, 0.6_m, 0.7_m}, {1.1_rad, 2.2_rad, 3.3_rad}};
        off_red_bulb.dimensions = {0.5_m, 0.5_m, 0.5_m};
        off_red_bulb.color = map::OsiTrafficLightColor::kRed;
        off_red_bulb.mode = map::OsiTrafficLightMode::kOff;
        off_red_bulb.icon = map::OsiTrafficLightIcon::kNone;
        off_red_bulb.assigned_lanes = {43, 23};
        traffic_light.light_bulbs.emplace_back(off_red_bulb);

        map::TrafficLightBulb flashing_green_bulb{};
        flashing_green_bulb.id = 103;
        flashing_green_bulb.pose = {{0.6_m, 0.7_m, 0.8_m}, {1.1_rad, 2.2_rad, 3.3_rad}};
        flashing_green_bulb.dimensions = {0.5_m, 0.5_m, 0.5_m};
        flashing_green_bulb.color = map::OsiTrafficLightColor::kGreen;
        flashing_green_bulb.mode = map::OsiTrafficLightMode::kFlashing;
        flashing_green_bulb.icon = map::OsiTrafficLightIcon::kNone;
        flashing_green_bulb.assigned_lanes = {43};
        traffic_light.light_bulbs.emplace_back(flashing_green_bulb);

        map::TrafficLightBulb counting_green_bulb{};
        counting_green_bulb.id = 104;
        counting_green_bulb.pose = {{0.7_m, 0.8_m, 0.9_m}, {1.1_rad, 2.2_rad, 3.3_rad}};
        counting_green_bulb.dimensions = {0.5_m, 0.5_m, 0.5_m};
        counting_green_bulb.color = map::OsiTrafficLightColor::kGreen;
        counting_green_bulb.mode = map::OsiTrafficLightMode::kCounting;
        counting_green_bulb.icon = map::OsiTrafficLightIcon::kCountdownSeconds;
        counting_green_bulb.count = 42.0;
        counting_green_bulb.assigned_lanes = {23};
        traffic_light.light_bulbs.emplace_back(counting_green_bulb);

        astas_map_.traffic_lights.emplace_back(traffic_light);
    }

    void AddRoadObjects()
    {
        map::RoadObject road_object;

        road_object.pose = mantle_api::Pose{mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m},
                                            mantle_api::Orientation3<units::angle::radian_t>{8_rad, 7_rad, 9_rad}};
        road_object.dimensions = mantle_api::Dimension3{0.2_m, 0.1_m, 0.3_m};
        road_object.id = 42;
        road_object.type = osi::StationaryObjectEntityType::kBuilding;
        road_object.material = osi::StationaryObjectEntityMaterial::kMud;
        road_object.name = "Building";
        road_object.base_polygon =
            std::vector<mantle_api::Vec3<units::length::meter_t>>{{1_m, 2_m, 0.3_m}, {4_m, 5_m, 0.6_m}};
        astas_map_.road_objects.push_back(road_object);

        astas_map_.road_objects.push_back(road_object);
    }

    // contains two lanes that are imagined to be next to each other, thus sharing a boundary
    void AddLaneGroupWithSharedBoundary()
    {
        const mantle_api::UniqueId lane_group_id = 42;
        test_utils::SetupBasicLaneGroup(lane_group_id, astas_map_);

        environment::map::Lane right_lane{3};
        right_lane.flags.SetDrivable();
        environment::map::Lane left_lane{4};
        left_lane.flags.SetDrivable();

        environment::map::LaneBoundary shared_boundary{
            69, environment::map::LaneBoundary::Type::kSolidLine, environment::map::LaneBoundary::Color::kWhite};

        right_lane.left_lane_boundaries.push_back(shared_boundary.id);
        left_lane.right_lane_boundaries.push_back(shared_boundary.id);

        astas_map_.AddLane(lane_group_id, right_lane);
        astas_map_.AddLane(lane_group_id, left_lane);
        astas_map_.AddLaneBoundary(lane_group_id, shared_boundary);
    }

    void AddLaneGroupWithAllLaneFlags()
    {
        const mantle_api::UniqueId lane_group_id = 42;
        test_utils::SetupBasicLaneGroup(lane_group_id, astas_map_);

        environment::map::Lane normal_lane{3};
        normal_lane.flags.SetDrivable();
        normal_lane.flags.SetNormalLane();
        environment::map::Lane shoulder_lane{4};
        shoulder_lane.flags.SetDrivable();
        shoulder_lane.flags.SetShoulderLane();
        environment::map::Lane merge_lane{5};
        merge_lane.flags.SetDrivable();
        merge_lane.flags.SetMergeLane();
        environment::map::Lane split_lane{6};
        split_lane.flags.SetDrivable();
        split_lane.flags.SetSplitLane();
        environment::map::Lane exit_lane{7};
        exit_lane.flags.SetDrivable();
        exit_lane.flags.SetExitLane();

        astas_map_.AddLane(lane_group_id, normal_lane);
        astas_map_.AddLane(lane_group_id, shoulder_lane);
        astas_map_.AddLane(lane_group_id, merge_lane);
        astas_map_.AddLane(lane_group_id, split_lane);
        astas_map_.AddLane(lane_group_id, exit_lane);
    }

    void CreateFullAstasMap()
    {
        AddLanesAndLaneGroups();
        AddTrafficSigns();
        AddTrafficLights();
        AddRoadObjects();
    }

    environment::chunking::WorldChunks CreateChunks()
    {
        environment::chunking::WorldChunks result;
        environment::chunking::WorldChunk chunk{};
        chunk.lane_groups.push_back(&astas_map_.GetLaneGroups().front());
        chunk.lane_groups.push_back(&astas_map_.GetLaneGroups().back());

        for (const auto& road_object : astas_map_.road_objects)
        {
            chunk.road_objects.push_back(&road_object);
        }

        for (const auto& traffic_sign : astas_map_.traffic_signs)
        {
            chunk.traffic_signs.push_back(traffic_sign.get());
        }

        for (const auto& traffic_light : astas_map_.traffic_lights)
        {
            chunk.traffic_lights.push_back(&traffic_light);
        }

        result.emplace_back(std::move(chunk));
        return result;
    }

    test_utils::AstasMapBuilder astas_map_builder_;
    map::AstasMap& astas_map_;
};

void AssertEqualCoordinateLists(const google::protobuf::RepeatedPtrField<astas_osi3::Vector3d>& expected_points,
                                const google::protobuf::RepeatedPtrField<astas_osi3::Vector3d>& gt_points)
{
    ASSERT_EQ(expected_points.size(), gt_points.size());

    for (int i = 0; i < expected_points.size(); ++i)
    {
        EXPECT_EQ(expected_points.Get(i).SerializeAsString(), gt_points.Get(i).SerializeAsString());
    }
}

void AssertEqualCoordinateLists(const google::protobuf::RepeatedPtrField<astas_osi3::Vector3d>& expected_points,
                                const astas_osi3::LaneBoundary& gt_lane_boundary)
{
    ASSERT_EQ(expected_points.size(), gt_lane_boundary.boundary_line().size());

    for (int i = 0; i < expected_points.size(); ++i)
    {
        EXPECT_EQ(expected_points.Get(i).SerializeAsString(),
                  gt_lane_boundary.boundary_line(i).position().SerializeAsString());
    }
}

TEST_F(StaticProtoGroundTruthBuilderTest,
       GivenAstasMap_WhenFillStaticGroundTruth_ThenTrafficSignsAndRoadMarkingsAreFilled)
{
    astas_osi3::Dimension3d expected_boundingbox =
        service::gt_conversion::CreateProtoObject<astas_osi3::Dimension3d>(mantle_api::Dimension3{0.1_m, 0.2_m, 0.3_m});
    astas_osi3::Vector3d expected_position = service::gt_conversion::CreateProtoObject<astas_osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m});
    astas_osi3::Orientation3d expected_orientation =
        service::gt_conversion::CreateProtoObject<astas_osi3::Orientation3d>(
            mantle_api::Orientation3<units::angle::radian_t>{2_rad, 1_rad, 3_rad});

    astas_osi3::GroundTruth proto_gt;

    CreateFullAstasMap();

    StaticProtoGroundTruthBuilder static_proto_gt_builder{astas_map_};
    static_proto_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto& gt_traffic_signs = proto_gt.traffic_sign();
    ASSERT_EQ(1, gt_traffic_signs.size());

    // Assert that astas mounted sign and its supplementary signs have been correctly copied into the groundtruth
    EXPECT_EQ(astas_osi3::TrafficSign::MainSign::Classification::TYPE_OTHER,
              gt_traffic_signs.Get(0).main_sign().classification().type());
    EXPECT_EQ(astas_osi3::TrafficSign::VARIABILITY_VARIABLE,
              gt_traffic_signs.Get(0).main_sign().classification().variability());
    EXPECT_EQ(astas_osi3::TrafficSign::MainSign::Classification::DIRECTION_SCOPE_RIGHT,
              gt_traffic_signs.Get(0).main_sign().classification().direction_scope());
    EXPECT_EQ(astas_osi3::TrafficSignValue::UNIT_KILOMETER_PER_HOUR,
              gt_traffic_signs.Get(0).main_sign().classification().value().value_unit());
    EXPECT_EQ(30.0, gt_traffic_signs.Get(0).main_sign().classification().value().value());
    EXPECT_EQ(10, gt_traffic_signs.Get(0).id().value());

    EXPECT_EQ(expected_boundingbox.SerializeAsString(),
              gt_traffic_signs.Get(0).main_sign().base().dimension().SerializeAsString());
    EXPECT_EQ(expected_position.SerializeAsString(),
              gt_traffic_signs.Get(0).main_sign().base().position().SerializeAsString());
    EXPECT_EQ(expected_orientation.SerializeAsString(),
              gt_traffic_signs.Get(0).main_sign().base().orientation().SerializeAsString());

    ASSERT_EQ(1, gt_traffic_signs.Get(0).supplementary_sign().size());
    ASSERT_EQ(2, gt_traffic_signs.Get(0).main_sign().classification().assigned_lane_id().size());
    EXPECT_EQ(123, gt_traffic_signs.Get(0).main_sign().classification().assigned_lane_id(0).value());
    EXPECT_EQ(321, gt_traffic_signs.Get(0).main_sign().classification().assigned_lane_id(1).value());

    // Assert that astas ground sign has been correctly copied into the groundtruth
    const auto& gt_road_markings = proto_gt.road_marking();
    ASSERT_EQ(1, gt_road_markings.size());
    EXPECT_EQ(astas_osi3::RoadMarking::Classification::TYPE_PAINTED_TRAFFIC_SIGN,
              gt_road_markings.Get(0).classification().type());
    EXPECT_EQ(astas_osi3::RoadMarking::Classification::COLOR_RED,
              gt_road_markings.Get(0).classification().monochrome_color());
    EXPECT_EQ(astas_osi3::TrafficSign::MainSign::Classification::TYPE_OTHER,
              gt_road_markings.Get(0).classification().traffic_main_sign_type());
    EXPECT_EQ(astas_osi3::TrafficSignValue::UNIT_METRIC_TON,
              gt_road_markings.Get(0).classification().value().value_unit());
    EXPECT_EQ(15.0, gt_road_markings.Get(0).classification().value().value());
    EXPECT_EQ(5, gt_road_markings.Get(0).id().value());

    expected_position = service::gt_conversion::CreateProtoObject<astas_osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{0.6_m, 0.7_m, 0.8_m});
    expected_orientation = service::gt_conversion::CreateProtoObject<astas_osi3::Orientation3d>(
        mantle_api::Orientation3<units::angle::radian_t>{5_rad, 4_rad, 6_rad});

    EXPECT_EQ(expected_boundingbox.SerializeAsString(), gt_road_markings.Get(0).base().dimension().SerializeAsString());
    EXPECT_EQ(expected_position.SerializeAsString(), gt_road_markings.Get(0).base().position().SerializeAsString());
    EXPECT_EQ(expected_orientation.SerializeAsString(),
              gt_road_markings.Get(0).base().orientation().SerializeAsString());

    ASSERT_EQ(0, gt_road_markings.Get(0).classification().assigned_lane_id().size());
}

TEST_F(StaticProtoGroundTruthBuilderTest, GivenAstasMap_WhenFillStaticGroundTruth_ThenSupplementarySignsAreFilled)
{
    astas_osi3::Dimension3d expected_boundingbox =
        service::gt_conversion::CreateProtoObject<astas_osi3::Dimension3d>(mantle_api::Dimension3{0.1_m, 0.2_m, 0.3_m});

    astas_osi3::Vector3d expected_coordinate = service::gt_conversion::CreateProtoObject<astas_osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 3_m});
    astas_osi3::Orientation3d expected_orientation =
        service::gt_conversion::CreateProtoObject<astas_osi3::Orientation3d>(
            mantle_api::Orientation3<units::angle::radian_t>{2_rad, 1_rad, 3_rad});
    astas_osi3::GroundTruth proto_gt;

    CreateFullAstasMap();

    StaticProtoGroundTruthBuilder static_gt_builder{astas_map_};
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto& gt_supp_signs = proto_gt.traffic_sign(0).supplementary_sign();
    ASSERT_EQ(1, gt_supp_signs.size());

    EXPECT_EQ(astas_osi3::TrafficSign::SupplementarySign::Classification::TYPE_OTHER,
              gt_supp_signs.Get(0).classification().type());
    EXPECT_EQ(astas_osi3::TrafficSign::VARIABILITY_FIXED, gt_supp_signs.Get(0).classification().variability());

    EXPECT_EQ(expected_boundingbox.SerializeAsString(), gt_supp_signs.Get(0).base().dimension().SerializeAsString());
    EXPECT_EQ(expected_coordinate.SerializeAsString(), gt_supp_signs.Get(0).base().position().SerializeAsString());
    EXPECT_EQ(expected_orientation.SerializeAsString(), gt_supp_signs.Get(0).base().orientation().SerializeAsString());

    // Supplementary signs' values
    ASSERT_EQ(2, gt_supp_signs.Get(0).classification().value().size());

    const auto& gt_supp_sign_values = gt_supp_signs.Get(0).classification().value();
    EXPECT_EQ(2.0, gt_supp_sign_values.Get(0).value());
    EXPECT_EQ(astas_osi3::TrafficSignValue::UNIT_KILOMETER, gt_supp_sign_values.Get(0).value_unit());
    EXPECT_EQ("TestText", gt_supp_sign_values.Get(0).text());

    ASSERT_EQ(4, gt_supp_signs.Get(0).classification().actor().size());
    EXPECT_EQ(astas_osi3::TrafficSign_SupplementarySign_Classification_Actor::
                  TrafficSign_SupplementarySign_Classification_Actor_ACTOR_NO_ACTOR,
              gt_supp_signs.Get(0).classification().actor(0));
    EXPECT_EQ(astas_osi3::TrafficSign_SupplementarySign_Classification_Actor::
                  TrafficSign_SupplementarySign_Classification_Actor_ACTOR_CARS_WITH_TRAILERS,
              gt_supp_signs.Get(0).classification().actor(1));
    EXPECT_EQ(astas_osi3::TrafficSign_SupplementarySign_Classification_Actor::
                  TrafficSign_SupplementarySign_Classification_Actor_ACTOR_TRUCKS,
              gt_supp_signs.Get(0).classification().actor(2));
    EXPECT_EQ(astas_osi3::TrafficSign_SupplementarySign_Classification_Actor::
                  TrafficSign_SupplementarySign_Classification_Actor_ACTOR_BUSES,
              gt_supp_signs.Get(0).classification().actor(3));
}

TEST_F(StaticProtoGroundTruthBuilderTest,
       GivenTrafficLightsInAstasMap_WhenBuildingStaticProtoGroundTruth_ThenTrafficLightsAreFilled)
{
    const auto expected_bounding_box =
        service::gt_conversion::CreateProtoObject<astas_osi3::Dimension3d>(mantle_api::Dimension3{0.5_m, 0.5_m, 0.5_m});
    auto expected_position = service::gt_conversion::CreateProtoObject<astas_osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{0.5_m, 0.6_m, 0.7_m});
    const auto expected_orientation = service::gt_conversion::CreateProtoObject<astas_osi3::Orientation3d>(
        mantle_api::Orientation3<units::angle::radian_t>{1.1_rad, 2.2_rad, 3.3_rad});

    astas_osi3::GroundTruth proto_gt;

    CreateFullAstasMap();

    StaticProtoGroundTruthBuilder static_proto_gt_builder{astas_map_};
    static_proto_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto& gt_traffic_lights = proto_gt.traffic_light();
    ASSERT_EQ(3, gt_traffic_lights.size());

    const auto& red_light = gt_traffic_lights.Get(0);
    EXPECT_EQ(102, red_light.id().value());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Color::TrafficLight_Classification_Color_COLOR_RED,
              red_light.classification().color());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Mode::TrafficLight_Classification_Mode_MODE_OFF,
              red_light.classification().mode());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Icon::TrafficLight_Classification_Icon_ICON_NONE,
              red_light.classification().icon());
    EXPECT_EQ(43, red_light.classification().assigned_lane_id(0).value());
    EXPECT_EQ(23, red_light.classification().assigned_lane_id(1).value());
    EXPECT_EQ(expected_bounding_box.SerializeAsString(), red_light.base().dimension().SerializeAsString());
    EXPECT_EQ(expected_position.SerializeAsString(), red_light.base().position().SerializeAsString());
    EXPECT_EQ(expected_orientation.SerializeAsString(), red_light.base().orientation().SerializeAsString());

    const auto& green_light = gt_traffic_lights.Get(1);
    EXPECT_EQ(103, green_light.id().value());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Color::TrafficLight_Classification_Color_COLOR_GREEN,
              green_light.classification().color());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Mode::TrafficLight_Classification_Mode_MODE_FLASHING,
              green_light.classification().mode());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Icon::TrafficLight_Classification_Icon_ICON_NONE,
              green_light.classification().icon());
    EXPECT_EQ(43, green_light.classification().assigned_lane_id(0).value());
    EXPECT_EQ(expected_bounding_box.SerializeAsString(), green_light.base().dimension().SerializeAsString());
    expected_position = service::gt_conversion::CreateProtoObject<astas_osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{0.6_m, 0.7_m, 0.8_m});
    EXPECT_EQ(expected_position.SerializeAsString(), green_light.base().position().SerializeAsString());
    EXPECT_EQ(expected_orientation.SerializeAsString(), green_light.base().orientation().SerializeAsString());

    const auto& counting_light = gt_traffic_lights.Get(2);
    EXPECT_EQ(104, counting_light.id().value());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Color::TrafficLight_Classification_Color_COLOR_GREEN,
              counting_light.classification().color());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Mode::TrafficLight_Classification_Mode_MODE_COUNTING,
              counting_light.classification().mode());
    EXPECT_EQ(astas_osi3::TrafficLight_Classification_Icon::TrafficLight_Classification_Icon_ICON_COUNTDOWN_SECONDS,
              counting_light.classification().icon());
    EXPECT_EQ(23, counting_light.classification().assigned_lane_id(0).value());
    EXPECT_DOUBLE_EQ(42.0, counting_light.classification().counter());
    EXPECT_EQ(expected_bounding_box.SerializeAsString(), counting_light.base().dimension().SerializeAsString());
    expected_position = service::gt_conversion::CreateProtoObject<astas_osi3::Vector3d>(
        mantle_api::Vec3<units::length::meter_t>{0.7_m, 0.8_m, 0.9_m});
    EXPECT_EQ(expected_position.SerializeAsString(), counting_light.base().position().SerializeAsString());
    EXPECT_EQ(expected_orientation.SerializeAsString(), counting_light.base().orientation().SerializeAsString());
}

TEST_F(StaticProtoGroundTruthBuilderTest, GivenAstasMap_WhenFillStaticGroundTruth_ThenStationaryObjectsAreFilled)
{
    CreateFullAstasMap();

    astas_osi3::GroundTruth proto_gt;

    StaticProtoGroundTruthBuilder static_gt_builder{astas_map_};
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto& gt_stat_objects = proto_gt.stationary_object();
    ASSERT_EQ(2, gt_stat_objects.size());

    const auto& building = gt_stat_objects.Get(0);
    EXPECT_EQ(42, building.id().value());
    EXPECT_EQ("Building", building.model_reference());
    EXPECT_EQ(astas_osi3::StationaryObject::Classification::Type::StationaryObject_Classification_Type_TYPE_BUILDING,
              building.classification().type());
    EXPECT_EQ(
        astas_osi3::StationaryObject::Classification::Material::StationaryObject_Classification_Material_MATERIAL_MUD,
        building.classification().material());
    EXPECT_EQ(0.1, building.base().dimension().width());
    EXPECT_EQ(0.2, building.base().dimension().length());
    EXPECT_EQ(0.3, building.base().dimension().height());
    EXPECT_EQ(1, building.base().position().x());
    EXPECT_EQ(2, building.base().position().y());
    EXPECT_EQ(3, building.base().position().z());
    EXPECT_EQ(7, building.base().orientation().pitch());
    EXPECT_EQ(8, building.base().orientation().yaw());
    EXPECT_EQ(9, building.base().orientation().roll());
    ASSERT_EQ(2, building.base().base_polygon().size());

    const auto& polygon_0 = building.base().base_polygon();
    EXPECT_EQ(1, polygon_0.Get(0).x());
    EXPECT_EQ(2, polygon_0.Get(0).y());
    // Here used bounding box's height as height of polygon according to OSI documentation.
    EXPECT_EQ(0.3, building.base().dimension().height());
    EXPECT_EQ(4, polygon_0.Get(1).x());
    EXPECT_EQ(5, polygon_0.Get(1).y());
}

TEST_F(StaticProtoGroundTruthBuilderTest,
       GivenAstasMap_WhenFillStaticGroundTruth_ThenLanesBasicPropertiesAreCopiedCorrectly)
{
    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_centerline_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({1_m, 2_m, 3_m}),
                                            expected_centerline_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({4_m, 5_m, 6_m}),
                                            expected_centerline_points.Add());

    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_left_boundary_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({7_m, 8_m, 9_m}),
                                            expected_left_boundary_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({10_m, 11_m, 12_m}),
                                            expected_left_boundary_points.Add());

    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_right_boundary_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({13_m, 14_m, 15_m}),
                                            expected_right_boundary_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({16_m, 17_m, 18_m}),
                                            expected_right_boundary_points.Add());

    CreateFullAstasMap();

    astas_osi3::GroundTruth proto_gt;

    StaticProtoGroundTruthBuilder static_gt_builder{astas_map_};
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    ASSERT_EQ(7, proto_gt.lane().size());
    const auto& gt_lane = proto_gt.lane(2);

    EXPECT_EQ(astas_osi3::Lane::Classification::TYPE_DRIVING, gt_lane.classification().type());
    EXPECT_EQ(69, gt_lane.id().value());

    // Check lane's centerline
    AssertEqualCoordinateLists(expected_centerline_points, gt_lane.classification().centerline());
}

TEST_F(StaticProtoGroundTruthBuilderTest, GivenAstasMap_WhenFillStaticGroundTruth_ThenLanesRelationsAreCopiedCorrectly)
{
    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_centerline_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({1_m, 2_m, 3_m}),
                                            expected_centerline_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({4_m, 5_m, 6_m}),
                                            expected_centerline_points.Add());

    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_left_boundary_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({7_m, 8_m, 9_m}),
                                            expected_left_boundary_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({10_m, 11_m, 12_m}),
                                            expected_left_boundary_points.Add());

    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_right_boundary_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({13_m, 14_m, 15_m}),
                                            expected_right_boundary_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({16_m, 17_m, 18_m}),
                                            expected_right_boundary_points.Add());

    CreateFullAstasMap();

    astas_osi3::GroundTruth proto_gt;

    StaticProtoGroundTruthBuilder static_gt_builder{astas_map_};
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto& gt_lane = proto_gt.lane(2);

    const auto invalid_id{service::utility::UniqueIdProvider::GetInvalidId()};

    const auto& antecessor_ids = test_utils::GetAllAntecessorIds(gt_lane);
    const auto& successor_ids = test_utils::GetAllSuccessorIds(gt_lane);
    // Check lane relations
    ASSERT_EQ(2, successor_ids.size());
    EXPECT_EQ(2, successor_ids[0]);

    EXPECT_EQ(invalid_id, successor_ids[1]);

    ASSERT_EQ(2, antecessor_ids.size());
    EXPECT_EQ(3, antecessor_ids[0]);
    EXPECT_EQ(invalid_id, antecessor_ids[1]);

    ASSERT_EQ(1, gt_lane.classification().left_adjacent_lane_id().size());
    EXPECT_EQ(6, gt_lane.classification().left_adjacent_lane_id(0).value());

    ASSERT_EQ(1, gt_lane.classification().right_adjacent_lane_id().size());
    EXPECT_EQ(9, gt_lane.classification().right_adjacent_lane_id(0).value());
}

TEST_F(StaticProtoGroundTruthBuilderTest, GivenAstasMap_WhenFillStaticGroundTruth_ThenLaneBoundariesAreCopiedCorrectly)
{
    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_centerline_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({1_m, 2_m, 3_m}),
                                            expected_centerline_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({4_m, 5_m, 6_m}),
                                            expected_centerline_points.Add());

    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_left_boundary_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({7_m, 8_m, 9_m}),
                                            expected_left_boundary_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({10_m, 11_m, 12_m}),
                                            expected_left_boundary_points.Add());

    google::protobuf::RepeatedPtrField<astas_osi3::Vector3d> expected_right_boundary_points;
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({13_m, 14_m, 15_m}),
                                            expected_right_boundary_points.Add());
    service::gt_conversion::FillProtoObject(mantle_api::Vec3<units::length::meter_t>({16_m, 17_m, 18_m}),
                                            expected_right_boundary_points.Add());

    CreateFullAstasMap();

    astas_osi3::GroundTruth proto_gt;

    astas_osi3::Lane lane;
    StaticProtoGroundTruthBuilder static_gt_builder(astas_map_);
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto& gt_lane = proto_gt.lane(2);

    EXPECT_EQ(2, gt_lane.classification().left_lane_boundary_id().size());
    const auto& gt_left_boundaries =
        test_utils::GetLaneBoundariesByIds(proto_gt, gt_lane.classification().left_lane_boundary_id());
    // Check left boundaries
    // const auto& gt_left_boundaries =
    // proto_gt.lane_boundary(static_cast<int>(gt_lane.classification().left_lane_boundary_id(0).value()));
    ASSERT_EQ(2, gt_left_boundaries.size());

    // Left boundary 0
    EXPECT_EQ(astas_osi3::LaneBoundary::Classification::TYPE_GRASS_EDGE,
              gt_left_boundaries[0]->classification().type());
    EXPECT_EQ(42, gt_left_boundaries[0]->id().value());
    AssertEqualCoordinateLists(expected_left_boundary_points, *gt_left_boundaries[0]);

    // Left boundary 1
    EXPECT_EQ(astas_osi3::LaneBoundary::Classification::TYPE_CURB, gt_left_boundaries[1]->classification().type());
    EXPECT_EQ(69, gt_left_boundaries[1]->id().value());
    AssertEqualCoordinateLists(expected_left_boundary_points, *gt_left_boundaries[0]);

    // Check right boundaries
    const auto& gt_right_boundaries =
        test_utils::GetLaneBoundariesByIds(proto_gt, gt_lane.classification().right_lane_boundary_id());
    ASSERT_EQ(2, gt_right_boundaries.size());

    // Right boundary 0
    EXPECT_EQ(astas_osi3::LaneBoundary::Classification::TYPE_CURB, gt_right_boundaries[0]->classification().type());
    EXPECT_EQ(96, gt_right_boundaries[0]->id().value());
    AssertEqualCoordinateLists(expected_right_boundary_points, *gt_right_boundaries[0]);

    // Right boundary 1
    EXPECT_EQ(astas_osi3::LaneBoundary::Classification::TYPE_GRASS_EDGE,
              gt_right_boundaries[1]->classification().type());
    EXPECT_EQ(24, gt_right_boundaries[1]->id().value());
    AssertEqualCoordinateLists(expected_right_boundary_points, *gt_right_boundaries[1]);
}

TEST_F(StaticProtoGroundTruthBuilderTest,
       GivenMapWithSharedBoundary_WhenFillStaticGroundTruth_ThenNoDuplicatedBoundaries)
{
    AddLaneGroupWithSharedBoundary();

    astas_osi3::GroundTruth proto_gt{};
    StaticProtoGroundTruthBuilder static_gt_builder(astas_map_);
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    EXPECT_EQ(proto_gt.lane_boundary_size(), 1);
}

TEST_F(StaticProtoGroundTruthBuilderTest, GivenMapWithFlags_WhenFillStaticGroundTruth_ThenLaneFlagsAreCopiedCorrectly)
{
    AddLaneGroupWithAllLaneFlags();

    astas_osi3::GroundTruth proto_gt{};
    StaticProtoGroundTruthBuilder static_gt_builder(astas_map_);
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    EXPECT_EQ(proto_gt.lane(2).classification().subtype(), astas_osi3::Lane_Classification_Subtype_SUBTYPE_NORMAL);
    EXPECT_EQ(proto_gt.lane(3).classification().subtype(), astas_osi3::Lane_Classification_Subtype_SUBTYPE_SHOULDER);
    EXPECT_EQ(proto_gt.lane(4).classification().subtype(), astas_osi3::Lane_Classification_Subtype_SUBTYPE_ENTRY);
    EXPECT_EQ(proto_gt.lane(5).classification().subtype(), astas_osi3::Lane_Classification_Subtype_SUBTYPE_EXIT);
    EXPECT_EQ(proto_gt.lane(6).classification().subtype(), astas_osi3::Lane_Classification_Subtype_SUBTYPE_EXIT);
}

TEST_F(
    StaticProtoGroundTruthBuilderTest,
    GivenAstasMapWithTrafficSignsAndGroundSignsAsStationaryObjects_WhenFillWithOneStaticGroundTruth_ThenTrafficSignsObjectsAndRoadMarkingsAreFilled)
{
    AddLanesAndLaneGroups();
    AddTrafficSigns();

    astas_osi3::GroundTruth proto_gt;

    StaticProtoGroundTruthBuilder static_gt_builder{astas_map_};
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto gt_stat_objects_size = proto_gt.stationary_object_size();
    ASSERT_EQ(0, gt_stat_objects_size);

    const auto gt_traffic_lights_objects_size = proto_gt.traffic_light_size();
    ASSERT_EQ(0, gt_traffic_lights_objects_size);

    const auto gt_traffic_signs_objects_size = proto_gt.traffic_sign_size();
    ASSERT_EQ(1, gt_traffic_signs_objects_size);

    const auto gt_road_markings_objects_size = proto_gt.road_marking_size();
    ASSERT_EQ(1, gt_road_markings_objects_size);
}

TEST_F(
    StaticProtoGroundTruthBuilderTest,
    GivenAstasMapWithOnlyTrafficLightsAsStationaryObjects_WhenFillWithOneStaticGroundTruth_ThenOnlyTrafficLightsObjectsAreFilled)
{
    AddLanesAndLaneGroups();
    AddTrafficLights();

    astas_osi3::GroundTruth proto_gt;

    StaticProtoGroundTruthBuilder static_gt_builder{astas_map_};
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto gt_stat_objects_size = proto_gt.stationary_object_size();
    ASSERT_EQ(0, gt_stat_objects_size);

    const auto gt_traffic_lights_objects_size = proto_gt.traffic_light_size();
    ASSERT_EQ(3, gt_traffic_lights_objects_size);

    const auto gt_traffic_signs_objects_size = proto_gt.traffic_sign_size();
    ASSERT_EQ(0, gt_traffic_signs_objects_size);
}

TEST_F(
    StaticProtoGroundTruthBuilderTest,
    GivenAstasMapWithOnlyRoadObjectsAsStationaryObjects_WhenFillWithOneStaticGroundTruth_ThenOnlyStationaryObjectsAreFilled)
{
    AddLanesAndLaneGroups();
    AddRoadObjects();

    astas_osi3::GroundTruth proto_gt;

    StaticProtoGroundTruthBuilder static_gt_builder{astas_map_};
    static_gt_builder.FillStaticGroundTruth(CreateChunks(), proto_gt);

    const auto gt_stat_objects_size = proto_gt.stationary_object_size();
    ASSERT_EQ(2, gt_stat_objects_size);

    const auto gt_traffic_lights_objects_size = proto_gt.traffic_light_size();
    ASSERT_EQ(0, gt_traffic_lights_objects_size);

    const auto gt_traffic_signs_objects_size = proto_gt.traffic_sign_size();
    ASSERT_EQ(0, gt_traffic_signs_objects_size);
}

}  // namespace astas::environment::proto_groundtruth
