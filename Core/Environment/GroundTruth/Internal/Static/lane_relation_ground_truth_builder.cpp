/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/lane_relation_ground_truth_builder.h"

namespace astas::environment::proto_groundtruth
{

LaneRelationGroundTruthBuilder::LaneRelationGroundTruthBuilder(
    const std::unordered_set<mantle_api::UniqueId>& existing_lane_ids)
    : existing_lane_ids_{existing_lane_ids}
{
}

void LaneRelationGroundTruthBuilder::AddLanePairings(const map::Lane& astas_lane, astas_osi3::Lane& gt_lane) const
{
    if (astas_lane.predecessors.empty())
    {
        for (const auto& successor_id : astas_lane.successors)
        {
            AddLanePairing(gt_lane, GetExistingId(successor_id), mantle_api::InvalidId);
        }
    }
    else if (astas_lane.successors.empty())
    {
        for (const auto& predecessor_id : astas_lane.predecessors)
        {
            AddLanePairing(gt_lane, mantle_api::InvalidId, GetExistingId(predecessor_id));
        }
    }
    else
    {
        AddAllLanePairingCombinations(gt_lane, astas_lane);
    }
}

void LaneRelationGroundTruthBuilder::AddAdjacentLaneIds(const map::Lane& astas_lane, astas_osi3::Lane& gt_lane) const
{
    for (const auto& left_adjacent_lane_id : astas_lane.left_adjacent_lanes)
    {
        if (IdExists(left_adjacent_lane_id))
        {
            gt_lane.mutable_classification()->add_left_adjacent_lane_id()->set_value(left_adjacent_lane_id);
        }
    }

    for (const auto& right_adjacent_lane_id : astas_lane.right_adjacent_lanes)
    {
        if (IdExists(right_adjacent_lane_id))
        {
            gt_lane.mutable_classification()->add_right_adjacent_lane_id()->set_value(right_adjacent_lane_id);
        }
    }
}

void LaneRelationGroundTruthBuilder::AddAllLanePairingCombinations(astas_osi3::Lane& gt_lane,
                                                                   const map::Lane& astas_lane) const
{
    for (const auto& predecessor_id : astas_lane.predecessors)
    {
        for (const auto& successor_id : astas_lane.successors)
        {
            AddLanePairing(gt_lane, GetExistingId(successor_id), GetExistingId(predecessor_id));
        }
    }
}

void LaneRelationGroundTruthBuilder::AddLanePairing(astas_osi3::Lane& gt_lane,
                                                    mantle_api::UniqueId successor_id,
                                                    mantle_api::UniqueId predecessor_id) const
{
    if (successor_id != mantle_api::InvalidId || predecessor_id != mantle_api::InvalidId)
    {
        auto* lane_pairing = gt_lane.mutable_classification()->add_lane_pairing();
        lane_pairing->mutable_successor_lane_id()->set_value(successor_id);
        lane_pairing->mutable_antecessor_lane_id()->set_value(predecessor_id);
    }
}

mantle_api::UniqueId LaneRelationGroundTruthBuilder::GetExistingId(mantle_api::UniqueId id) const
{
    return IdExists(id) ? id : mantle_api::InvalidId;
}

bool LaneRelationGroundTruthBuilder::IdExists(mantle_api::UniqueId id) const
{
    return existing_lane_ids_.find(id) != existing_lane_ids_.end();
}

}  // namespace astas::environment::proto_groundtruth
