/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICLIGHTPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICLIGHTPROTOCONVERTER_H

#include "Core/Environment/Map/AstasMap/Internal/traffic_light.h"
#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace astas::environment::proto_groundtruth
{
/// @deprecated - remove when legacy code is deleted, and refactor astas map to only use i_entity is done
void FillProtoGroundTruthTrafficLight(const environment::map::TrafficLight* astas_traffic_light,
                                      astas_osi3::GroundTruth& groundtruth);

void AddTrafficLightEntityToGroundTruth(const mantle_api::IEntity* entity, astas_osi3::GroundTruth& ground_truth);

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_TRAFFICLIGHTPROTOCONVERTER_H
