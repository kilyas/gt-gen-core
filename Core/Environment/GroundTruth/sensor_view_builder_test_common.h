/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWBUILDERTESTCOMMON_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWBUILDERTESTCOMMON_H

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{

class BaseSensorViewBuilderObjectGenerationTest : public testing::Test
{
  protected:
    void SetUp() override
    {
        CreateHostEntityOnly();
        AddObjectsToMap();
        AddEntities();
        sensor_view_builder_.Init();
    }

    virtual void CreateHostEntityOnly()
    {
        entities_.push_back(std::make_unique<environment::entities::VehicleEntity>(GetNewId(), "host"));
        entities_.back()->SetProperties(std::move(CreateHostVehicleProperties()));
    }

    virtual std::unique_ptr<mantle_api::VehicleProperties> CreateHostVehicleProperties() const
    {
        auto host_vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
        host_vehicle_properties->is_host = true;
        return host_vehicle_properties;
    }

    virtual void AddObjectsToMap() {}
    virtual void AddEntities() {}

    mantle_api::UniqueId GetNewId() { return new_id_++; }

    std::unique_ptr<map::AstasMap> astas_map_{
        test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach()};
    SensorViewBuilder sensor_view_builder_{*astas_map_, mantle_api::Time{10}, service::user_settings::MapChunking{}};
    std::vector<std::unique_ptr<mantle_api::IEntity>> entities_{};
    mantle_api::UniqueId new_id_{0};
};

}  // namespace astas::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_SENSORVIEWBUILDERTESTCOMMON_H
