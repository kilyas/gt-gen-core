/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/sensor_view_sanity_checker.h"

#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Service/Utility/algorithm_utils.h"

namespace astas::environment::proto_groundtruth
{

using chunking::WorldChunk;
using chunking::WorldChunks;

SensorViewSanityChecker::SensorViewSanityChecker(const astas_osi3::SensorView& sensor_view,
                                                 const WorldChunks& world_chunks,
                                                 const std::list<mantle_api::UniqueId>& static_entities_to_ignore,
                                                 const std::set<mantle_api::UniqueId>& dynamic_entities_to_ignore)
    : sensor_view_(sensor_view),
      world_chunks_(world_chunks),
      static_entities_to_ignore_(static_entities_to_ignore),
      dynamic_entities_to_ignore_(dynamic_entities_to_ignore)
{
}

void SensorViewSanityChecker::PerformSanityCheck()
{
    CollectIdsFromChunk();
    CheckNumberObjects();
}

void SensorViewSanityChecker::CollectIdsFromChunk()
{
    for (const auto& map_chunk : world_chunks_)
    {
        CollectIdsFromRoadObjects(map_chunk);
        CollectIdsFromTrafficSigns(map_chunk);
        CollectIdsFromTrafficLights(map_chunk);
        CollectIdsFromEntities(map_chunk);
    }
}

void SensorViewSanityChecker::CheckNumberObjects()
{
    const auto& ground_truth = sensor_view_.global_ground_truth();

    using ExecptionInfo = std::tuple<std::string, std::size_t, std::size_t>;
    std::vector<ExecptionInfo> infos{
        {"moving objects", moving_object_ids_.size(), ground_truth.moving_object_size()},
        {"stationary objects", stationary_object_ids_.size(), ground_truth.stationary_object_size()},
        {"traffic signs",
         traffic_sign_ids_.size(),
         ground_truth.traffic_sign_size() +
             ground_truth.road_marking_size()},  // RoadMarkings have to be included here, as they are created from
                                                 // GroundSigns which are derived from TrafficSigns
        {"traffic lights", traffic_light_ids_.size(), ground_truth.traffic_light_size()}};

    for (const auto& info : infos)
    {
        if (std::get<1>(info) != std::get<2>(info))
        {
            throw EnvironmentException(
                "SensorViewBuilder: Number of {} in the scenario ({}) and generated OSI Ground Truth ({}) do not "
                "match.",
                std::get<0>(info),
                std::get<1>(info),
                std::get<2>(info));
        }
    }
}

void SensorViewSanityChecker::CollectIdsFromRoadObjects(const WorldChunk& map_chunk)
{
    for (const auto& road_object : map_chunk.road_objects)
    {
        const auto& id = road_object->id;
        if (IsIdIgnored(id))
        {
            continue;
        }
        stationary_object_ids_.insert(id);
    }
}

void SensorViewSanityChecker::CollectIdsFromTrafficSigns(const WorldChunk& map_chunk)
{
    for (const auto& traffic_sign : map_chunk.traffic_signs)
    {
        const auto& id = traffic_sign->id;
        if (IsIdIgnored(id))
        {
            continue;
        }
        traffic_sign_ids_.insert(id);
    }
}

void SensorViewSanityChecker::CollectIdsFromTrafficLights(const WorldChunk& map_chunk)
{
    for (const auto& traffic_light : map_chunk.traffic_lights)
    {
        const auto& id = traffic_light->id;
        if (IsIdIgnored(id))
        {
            continue;
        }
        for (const auto& light_bulb : traffic_light->light_bulbs)
        {
            traffic_light_ids_.insert(light_bulb.id);
        }
    }
}

void SensorViewSanityChecker::CollectIdsFromEntities(const WorldChunk& map_chunk)
{
    for (const auto& entity : map_chunk.entities)
    {
        const auto& id = entity->GetUniqueId();
        if (IsIdIgnored(id))
        {
            continue;
        }
        if (IsStaticEntity(*entity))
        {
            ProcessStaticEntity(*entity);
        }
        else if (IsDynamicEntity(*entity))
        {
            ProcessDynamicEntity(*entity);
        }
    }
}

void SensorViewSanityChecker::ProcessStaticEntity(const mantle_api::IEntity& entity)
{
    if (IsEntityTrafficSign(entity))
    {
        traffic_sign_ids_.insert(entity.GetUniqueId());
    }
    else if (IsEntityTrafficLight(entity))
    {
        if (auto traffic_light_entity = dynamic_cast<const entities::TrafficLightEntity*>(&entity))
        {
            auto properties = traffic_light_entity->GetProperties();
            for (const auto& bulb_properties : properties->bulbs)
            {
                traffic_light_ids_.insert(bulb_properties.id);
            }
        }
    }
    else
    {
        if (entity.GetVisibility().traffic)
        {
            stationary_object_ids_.insert(entity.GetUniqueId());
        }
    }
}

void SensorViewSanityChecker::ProcessDynamicEntity(const mantle_api::IEntity& entity)
{
    if (entity.GetVisibility().traffic)
    {
        moving_object_ids_.insert(entity.GetUniqueId());
    }
}

bool SensorViewSanityChecker::IsStaticEntity(const mantle_api::IEntity& entity) const
{
    return (entity.GetProperties()->type == mantle_api::EntityType::kStatic);
}

bool SensorViewSanityChecker::IsDynamicEntity(const mantle_api::IEntity& entity) const
{
    return !IsStaticEntity(entity);
}

bool SensorViewSanityChecker::IsEntityTrafficSign(const mantle_api::IEntity& entity) const
{
    /// @todo Create a separate check for RoadMarkings once the ASTAS map does only contain Mantle entities
    return (dynamic_cast<mantle_ext::TrafficSignProperties*>(entity.GetProperties()) != nullptr ||
            dynamic_cast<mantle_ext::RoadMarkingProperties*>(entity.GetProperties()) != nullptr);
}

bool SensorViewSanityChecker::IsEntityTrafficLight(const mantle_api::IEntity& entity) const
{
    return dynamic_cast<mantle_ext::TrafficLightProperties*>(entity.GetProperties()) != nullptr;
}

bool SensorViewSanityChecker::IsIdIgnored(const mantle_api::UniqueId& id) const
{
    return service::utility::Contains(static_entities_to_ignore_, id) ||
           service::utility::Contains(dynamic_entities_to_ignore_, id);
}

}  // namespace astas::environment::proto_groundtruth
