/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/sensor_view_builder.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/GroundTruth/Internal/Utils/host_vehicle_data_converter_utils.h"
#include "Core/Environment/GroundTruth/sensor_view_sanity_checker.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/clock.h"

#include <google/protobuf/util/message_differencer.h>

namespace astas::environment::proto_groundtruth
{

SensorViewBuilder::SensorViewBuilder(const environment::map::AstasMap& astas_map,
                                     const mantle_api::Time& step_size,
                                     const service::user_settings::MapChunking& chunking_settings)
    : astas_map_(astas_map),
      static_proto_ground_truth_builder_(astas_map),
      environment_proto_ground_truth_builder_(step_size),
      chunking_settings_{chunking_settings}
{
}

void SensorViewBuilder::SetWeather(const mantle_api::Weather& weather)
{
    environment_proto_ground_truth_builder_.SetWeather(weather);
}

void SensorViewBuilder::SetDateTime(const mantle_api::Time& date_time)
{
    environment_proto_ground_truth_builder_.SetDateTime(date_time);
}

mantle_api::Time SensorViewBuilder::GetDateTime() const
{
    return environment_proto_ground_truth_builder_.GetDateTime();
}

chunking::WorldChunks SensorViewBuilder::GetChunksAroundEntity(
    const mantle_api::Vec3<units::length::meter_t>& central_entity_position,
    const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities)
{
    return map_chunker_.GetWorldChunks(entities, central_entity_position);
}

void SensorViewBuilder::Init()
{
    map_chunker_.Initialize(astas_map_, chunking_settings_.chunk_grid_size, chunking_settings_.cells_per_direction);
}

void SensorViewBuilder::Step(const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities,
                             mantle_api::IEntity* central_entity)
{
    if (central_entity == nullptr)
    {
        throw EnvironmentException("SensorViewBuilder: central_entity is not defined.");
    }

    world_chunks_ = GetChunksAroundEntity(central_entity->GetPosition(), entities);
    const auto& chunks_around_entity = world_chunks_;

    // important for async GUI server!
    std::scoped_lock lock(gt_mutex);

    ASTAS_PROFILE_SCOPE

    /// Protobuf suggests to reuse message objects whenever possible
    /// https://developers.google.com/protocol-buffers/docs/cpptutorial#optimization-tips
    GetMutableGroundTruth().Clear();

    auto host_vehicle_entity{dynamic_cast<entities::VehicleEntity*>(central_entity)};
    if (host_vehicle_entity != nullptr)
    {
        GetMutableGroundTruth().mutable_host_vehicle_id()->set_value(central_entity->GetUniqueId());

        auto* automated_driving_function =
            sensor_view_.mutable_host_vehicle_data()->add_vehicle_automated_driving_function();
        if (automated_driving_function != nullptr)
        {
            automated_driving_function->set_name(
                astas_osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_HIGHWAY_AUTOPILOT);
            automated_driving_function->set_state(
                utils::ConvertHadControlStateToOsiDrivingFunctionState(host_vehicle_entity->GetHADControlState()));
        }
        else
        {
            LogAndThrow<EnvironmentException>(
                EnvironmentException("Failed to add Automated Driving Function to SensorView"));
        }
    }

    SetVersion();
    SetTimestamp();

    static_proto_ground_truth_builder_.FillStaticGroundTruth(chunks_around_entity, GetMutableGroundTruth());
    dynamic_proto_ground_truth_builder_.FillDynamicGroundTruth(chunks_around_entity, GetMutableGroundTruth());

    double altitude = astas_map_.reference_point_altitude + central_entity->GetPosition().z();
    environment_proto_ground_truth_builder_.FillEnvironment(GetMutableGroundTruth().mutable_environmental_conditions(),
                                                            altitude);

    FillAdditionalData();
    ValidateSensorView();
}

void SensorViewBuilder::AddEntityIdToIgnoreList(mantle_api::UniqueId entity_id)
{
    static_proto_ground_truth_builder_.AddEntityIdToIgnoreList(entity_id);
    dynamic_proto_ground_truth_builder_.AddEntityIdToIgnoreList(entity_id);
}

const astas_osi3::SensorView& SensorViewBuilder::GetSensorView() const
{
    return sensor_view_;
}

astas_osi3::GroundTruth& SensorViewBuilder::GetMutableGroundTruth()
{
    return *sensor_view_.mutable_global_ground_truth();
}

void SensorViewBuilder::SetVersion()
{
    GetMutableGroundTruth().mutable_version()->set_version_major(3);
    GetMutableGroundTruth().mutable_version()->set_version_minor(5);
    GetMutableGroundTruth().mutable_version()->set_version_patch(0);

    sensor_view_.mutable_version()->CopyFrom(sensor_view_.global_ground_truth().version());
}

void SensorViewBuilder::SetTimestamp()
{
    auto sim_time = service::utility::Clock::Instance().Now();
    GetMutableGroundTruth().mutable_timestamp()->set_seconds(service::utility::Clock::Instance().Seconds(sim_time));
    GetMutableGroundTruth().mutable_timestamp()->set_nanos(service::utility::Clock::Instance().Nanos(sim_time));

    sensor_view_.mutable_timestamp()->CopyFrom(sensor_view_.global_ground_truth().timestamp());
}

void SensorViewBuilder::FillAdditionalData()
{
    ASTAS_PROFILE_SCOPE

    sensor_view_.mutable_host_vehicle_id()->set_value(GetMutableGroundTruth().host_vehicle_id().value());

    GetMutableGroundTruth().set_proj_string(astas_map_.projection_string);
    GetMutableGroundTruth().set_map_reference(astas_map_.path);
}

chunking::StaticChunkList SensorViewBuilder::GetStaticChunks() const
{
    return map_chunker_.GetStaticChunks();
}

void SensorViewBuilder::ValidateSensorView() const
{
    if (!sensor_view_.has_timestamp())
    {
        throw EnvironmentException("SensorViewBuilder: time_stamp is not set in the generated OSI Sensor View.");
    }

    const auto& ground_truth = sensor_view_.global_ground_truth();

    if (!ground_truth.has_timestamp())
    {
        throw EnvironmentException("SensorViewBuilder:time_stamp is not set in the generated OSI Ground Truth.");
    }

    if (!google::protobuf::util::MessageDifferencer::Equals(sensor_view_.timestamp(), ground_truth.timestamp()))
    {
        throw EnvironmentException("SensorViewBuilder:time_stamp is not set in the generated OSI Ground Truth.");
    }

    if (ground_truth.moving_object_size() == 0)
    {
        throw EnvironmentException("SensorViewBuilder: Generated OSI Ground Truth does NOT contain any moving object.");
    }

    if (!ground_truth.has_host_vehicle_id())
    {
        throw EnvironmentException("SensorViewBuilder: host_vehicle_id is not set in the generated OSI Ground Truth.");
    }

    const auto gt_host_vehicle_id = ground_truth.host_vehicle_id().value();
    const auto sv_host_vehicle_id = sensor_view_.host_vehicle_id().value();
    if (gt_host_vehicle_id != sv_host_vehicle_id)
    {
        throw EnvironmentException("host_vehicle_id in Sensor View and Global Ground Truth do not match.");
    }

    if (std::none_of(ground_truth.moving_object().begin(),
                     ground_truth.moving_object().end(),
                     [&gt_host_vehicle_id](const auto& moving_object) {
                         return gt_host_vehicle_id == moving_object.id().value();
                     }))
    {
        throw EnvironmentException("SensorViewBuilder: Generated OSI Ground Truth does NOT contain a host_vehicle.");
    }

    if (ground_truth.lane_size() == 0)
    {
        throw EnvironmentException("SensorViewBuilder: Lane is not set in the generated OSI Ground Truth.");
    }
    if (ground_truth.lane_boundary_size() < 2)
    {
        throw EnvironmentException(
            "SensorViewBuilder: minimum of Lane boundaries is 2 in the generated OSI Ground Truth.");
    }

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    sanity_checker.PerformSanityCheck();
}

}  // namespace astas::environment::proto_groundtruth
