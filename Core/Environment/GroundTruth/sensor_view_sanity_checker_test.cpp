/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/sensor_view_sanity_checker.h"

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/Entities/static_object_entity.h"
#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <gtest/gtest.h>

namespace astas::environment::proto_groundtruth
{
class SensorViewSanityCheckerTest : public testing::Test
{
  protected:
    void SetUp() override { CreateHost(); }

    void CreateHost()
    {
        entities_.push_back(std::make_unique<environment::entities::VehicleEntity>(GetNewId(), "host"));
        auto host_vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
        host_vehicle_properties->is_host = true;
        entities_.back()->SetProperties(std::move(host_vehicle_properties));
    }

    void CreateMovingObjectsAsEntity()
    {
        entities_.push_back(std::make_unique<environment::entities::VehicleEntity>(GetNewId(), "vehicle"));
        entities_.back()->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

        entities_.push_back(std::make_unique<environment::entities::PedestrianEntity>(GetNewId(), "pedestrian"));
        entities_.back()->SetProperties(std::make_unique<mantle_api::PedestrianProperties>());
    }

    void CreateStationaryObjectAsEntity()
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "stationary_object"));
        auto stationary_object_properties = std::make_unique<mantle_api::StaticObjectProperties>();
        stationary_object_properties->type = mantle_api::EntityType::kStatic;
        entities_.back()->SetProperties(std::move(stationary_object_properties));
    }

    void CreateStationaryObjectAsMapObject()
    {
        astas_map_->road_objects.push_back({.name{"road_object"}, .id{GetNewId()}});
    }

    void CreateTrafficSignAsEntity()
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "traffic_sign"));
        auto traffic_sign_properties = std::make_unique<mantle_ext::TrafficSignProperties>();
        traffic_sign_properties->type = mantle_api::EntityType::kStatic;
        entities_.back()->SetProperties(std::move(traffic_sign_properties));
    }

    void CreateTrafficSignAsMapObject()
    {
        astas_map_->traffic_signs.push_back(std::make_shared<map::TrafficSign>());
        astas_map_->traffic_signs.back()->id = GetNewId();
    }

    void CreateTrafficLightAsEntity()
    {
        entities_.push_back(std::make_unique<environment::entities::TrafficLightEntity>(GetNewId(), "traffic_light"));
        auto traffic_light_properties = std::make_unique<mantle_ext::TrafficLightProperties>();
        traffic_light_properties->type = mantle_api::EntityType::kStatic;
        constexpr std::size_t bulbs_per_light = 3;
        std::vector<mantle_ext::TrafficLightBulbProperties> bulbs(bulbs_per_light);
        bulbs[0].id = GetNewId();
        bulbs[1].id = GetNewId();
        bulbs[2].id = GetNewId();
        traffic_light_properties->bulbs = bulbs;
        entities_.back()->SetProperties(std::move(traffic_light_properties));
        entities_.back()->SetPosition({});
    }

    void CreateTrafficLightAsMapObject()
    {
        std::vector<map::TrafficLightBulb> light_bulbs{{.id{GetNewId()}, .color{map::OsiTrafficLightColor::kRed}},
                                                       {.id{GetNewId()}, .color{map::OsiTrafficLightColor::kYellow}},
                                                       {.id{GetNewId()}, .color{map::OsiTrafficLightColor::kGreen}}};
        astas_map_->traffic_lights.push_back({.id{GetNewId()}, .light_bulbs{light_bulbs}});
    }

    void CreateChunksAndGroundTruth()
    {
        map_chunker_.Initialize(
            *astas_map_, chunking_settings_.chunk_grid_size, chunking_settings_.cells_per_direction);
        world_chunks_ = map_chunker_.GetWorldChunks(entities_, entities_.front().get()->GetPosition());
        static_proto_ground_truth_builder_.FillStaticGroundTruth(world_chunks_,
                                                                 *sensor_view_.mutable_global_ground_truth());
        dynamic_proto_ground_truth_builder_.FillDynamicGroundTruth(world_chunks_,
                                                                   *sensor_view_.mutable_global_ground_truth());
    }

    void AddMovingObjectToGroundTruth()
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& moving_objects = *ground_truth.mutable_moving_object();
        auto& proto_moving_object = *ground_truth.add_moving_object();
        proto_moving_object.mutable_id()->set_value(GetNewId());
    }

    void RemoveMovingObjectFromGroundTruth(int start, int num)
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& moving_objects = *ground_truth.mutable_moving_object();
        moving_objects.DeleteSubrange(start, num);
    }

    void AddStationaryObjectToGroundTruth()
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& stationary_objects = *ground_truth.mutable_stationary_object();
        auto& proto_stationary_object = *ground_truth.add_stationary_object();
        proto_stationary_object.mutable_id()->set_value(GetNewId());
    }

    void RemoveStationaryObjectFromGroundTruth(int start, int num)
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& stationary_objects = *ground_truth.mutable_stationary_object();
        stationary_objects.DeleteSubrange(start, num);
    }

    void AddTrafficSignToGroundTruth()
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& traffic_signs = *ground_truth.mutable_traffic_sign();
        auto& proto_traffic_sign = *ground_truth.add_traffic_sign();
        proto_traffic_sign.mutable_id()->set_value(GetNewId());
    }

    void RemoveTrafficSignFromGroundTruth(int start, int num)
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& traffic_signs = *ground_truth.mutable_traffic_sign();
        traffic_signs.DeleteSubrange(start, num);
    }

    void AddTrafficLightToGroundTruth()
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& traffic_lights = *ground_truth.mutable_traffic_light();
        auto& proto_traffic_light = *ground_truth.add_traffic_light();
        proto_traffic_light.mutable_id()->set_value(GetNewId());
    }

    void RemoveTrafficLightFromGroundTruth(int start, int num)
    {
        auto& ground_truth = *sensor_view_.mutable_global_ground_truth();
        auto& traffic_light = *ground_truth.mutable_traffic_light();
        traffic_light.DeleteSubrange(start, num);
    }

    mantle_api::UniqueId GetNewId() { return id_++; }

    std::unique_ptr<map::AstasMap> astas_map_{test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    service::user_settings::MapChunking chunking_settings_{};
    chunking::DefaultMapChunker map_chunker_{};
    chunking::WorldChunks world_chunks_;
    StaticProtoGroundTruthBuilder static_proto_ground_truth_builder_{*astas_map_};
    DynamicProtoGroundTruthBuilder dynamic_proto_ground_truth_builder_;
    astas_osi3::SensorView sensor_view_;
    std::vector<std::unique_ptr<mantle_api::IEntity>> entities_{};

    mantle_api::UniqueId id_{0};
};

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenCorrectNumberOfObjects_ThenNoExceptionThrown)
{
    CreateMovingObjectsAsEntity();
    CreateStationaryObjectAsEntity();
    CreateStationaryObjectAsMapObject();
    CreateTrafficSignAsEntity();
    CreateTrafficSignAsMapObject();
    CreateTrafficLightAsEntity();
    CreateTrafficLightAsMapObject();

    CreateChunksAndGroundTruth();
    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_NO_THROW(sanity_checker.PerformSanityCheck());
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthMissingMovingObjectEntity_ThenExceptionThrown)
{
    CreateMovingObjectsAsEntity();

    CreateChunksAndGroundTruth();
    RemoveMovingObjectFromGroundTruth(1, 1);

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthHasExtraMovingObject_ThenExceptionThrown)
{
    CreateMovingObjectsAsEntity();

    CreateChunksAndGroundTruth();
    AddMovingObjectToGroundTruth();

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest,
       GivenMapWithEntities_WhenGroundTruthMissingStationaryObjectEntity_ThenExceptionThrown)
{
    CreateStationaryObjectAsEntity();
    CreateStationaryObjectAsMapObject();

    CreateChunksAndGroundTruth();
    RemoveStationaryObjectFromGroundTruth(0, 1);

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest,
       GivenMapWithEntities_WhenGroundTruthMissingStationaryObjectMapObject_ThenExceptionThrown)
{
    CreateStationaryObjectAsEntity();
    CreateStationaryObjectAsMapObject();

    CreateChunksAndGroundTruth();
    RemoveStationaryObjectFromGroundTruth(1, 1);

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthHasExtraStationaryObject_ThenExceptionThrown)
{
    CreateStationaryObjectAsEntity();
    CreateStationaryObjectAsMapObject();

    CreateChunksAndGroundTruth();
    AddStationaryObjectToGroundTruth();

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthMissingTrafficSignEntity_ThenExceptionThrown)
{
    CreateTrafficSignAsEntity();
    CreateTrafficSignAsMapObject();

    CreateChunksAndGroundTruth();
    RemoveTrafficSignFromGroundTruth(0, 1);

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthMissingTrafficSignMapObject_ThenExceptionThrown)
{
    CreateTrafficSignAsEntity();
    CreateTrafficSignAsMapObject();

    CreateChunksAndGroundTruth();
    RemoveTrafficSignFromGroundTruth(1, 1);

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthHasExtraTrafficSign_ThenExceptionThrown)
{
    CreateTrafficSignAsEntity();
    CreateTrafficSignAsMapObject();

    CreateChunksAndGroundTruth();
    AddTrafficSignToGroundTruth();

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthMissingTrafficLightEntity_ThenExceptionThrown)
{
    CreateTrafficLightAsEntity();
    CreateTrafficLightAsMapObject();

    CreateChunksAndGroundTruth();
    RemoveTrafficLightFromGroundTruth(0, 3);

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest,
       GivenMapWithEntities_WhenGroundTruthMissingTrafficLightMapObject_ThenExceptionThrown)
{
    CreateTrafficLightAsEntity();
    CreateTrafficLightAsMapObject();

    CreateChunksAndGroundTruth();
    RemoveTrafficLightFromGroundTruth(3, 3);

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

TEST_F(SensorViewSanityCheckerTest, GivenMapWithEntities_WhenGroundTruthHasExtraTraffic_ThenExceptionThrown)
{
    CreateTrafficLightAsEntity();
    CreateTrafficLightAsMapObject();

    CreateChunksAndGroundTruth();
    AddTrafficLightToGroundTruth();

    auto sanity_checker = SensorViewSanityChecker(sensor_view_,
                                                  world_chunks_,
                                                  static_proto_ground_truth_builder_.GetIgnoreList(),
                                                  dynamic_proto_ground_truth_builder_.GetIgnoreList());
    EXPECT_THROW(sanity_checker.PerformSanityCheck(), EnvironmentException);
}

}  // namespace astas::environment::proto_groundtruth
