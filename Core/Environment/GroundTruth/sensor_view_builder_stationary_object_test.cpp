/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/static_object_entity.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/GroundTruth/sensor_view_builder_test_common.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"

#include <gtest/gtest.h>
#include <units.h>

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_deg;

namespace astas::environment::proto_groundtruth
{

class StationaryObjectBaseTestParameterized : public BaseSensorViewBuilderObjectGenerationTest
{
  protected:
    void SetUp() override
    {
        BaseSensorViewBuilderObjectGenerationTest::SetUp();
        sensor_view_builder_.Step(entities_, entities_.front().get());
        gt_ = sensor_view_builder_.GetSensorView().global_ground_truth();
    }

    astas_osi3::GroundTruth gt_;
};

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class StationaryObjectTypeTestParameterized
    : public StationaryObjectBaseTestParameterized,
      public testing::WithParamInterface<
          std::tuple<osi::StationaryObjectEntityType, astas_osi3::StationaryObject_Classification_Type>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::RoadObject stationary_object;
        stationary_object.type = std::get<0>(GetParam());
        stationary_object.id = GetNewId();
        astas_map_->road_objects.push_back(stationary_object);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_object"));
        auto stationary_object_properties = std::make_unique<mantle_ext::StationaryObjectProperties>();
        stationary_object_properties->type = mantle_api::EntityType::kStatic;
        stationary_object_properties->object_type = std::get<0>(GetParam());
        entities_.back()->SetProperties(std::move(stationary_object_properties));
    }
};

TEST_P(StationaryObjectTypeTestParameterized,
       GivenStationaryObjectWithSpecificType_WhenStep_ThenExpectedStationaryObjectHasRightType)
{
    ASSERT_TRUE(gt_.stationary_object_size());

    const auto& actual_map_object_type = gt_.stationary_object(0).classification().type();
    EXPECT_EQ(actual_map_object_type, std::get<1>(GetParam()));

    const auto& actual_entity_object_type = gt_.stationary_object(1).classification().type();
    EXPECT_EQ(actual_entity_object_type, std::get<1>(GetParam()));
}

// clang-format off

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CREATE_STATIONARY_OBJECT_TYPE_TUPLE(custom_type, osi_type) \
    std::make_tuple(osi::StationaryObjectEntityType::custom_type,  \
                    astas_osi3::StationaryObject_Classification_Type::osi_type)

INSTANTIATE_TEST_SUITE_P(
    StationaryObjectTypeTest,
    StationaryObjectTypeTestParameterized,
    testing::Values(
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kUnknown, StationaryObject_Classification_Type_TYPE_UNKNOWN),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kOther, StationaryObject_Classification_Type_TYPE_OTHER),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kBridge, StationaryObject_Classification_Type_TYPE_BRIDGE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kBuilding, StationaryObject_Classification_Type_TYPE_BUILDING),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kPole, StationaryObject_Classification_Type_TYPE_POLE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kPylon, StationaryObject_Classification_Type_TYPE_PYLON),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kDelineator, StationaryObject_Classification_Type_TYPE_DELINEATOR),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kTree, StationaryObject_Classification_Type_TYPE_TREE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kBarrier, StationaryObject_Classification_Type_TYPE_BARRIER),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kVegetation, StationaryObject_Classification_Type_TYPE_VEGETATION),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kCurbstone, StationaryObject_Classification_Type_TYPE_CURBSTONE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kWall, StationaryObject_Classification_Type_TYPE_WALL),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kVerticalStructure, StationaryObject_Classification_Type_TYPE_VERTICAL_STRUCTURE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kRectangularStructure, StationaryObject_Classification_Type_TYPE_RECTANGULAR_STRUCTURE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kOverheadStructure, StationaryObject_Classification_Type_TYPE_OVERHEAD_STRUCTURE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kReflectiveStructure, StationaryObject_Classification_Type_TYPE_REFLECTIVE_STRUCTURE),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kConstructionSiteElement, StationaryObject_Classification_Type_TYPE_CONSTRUCTION_SITE_ELEMENT),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kSpeedBump, StationaryObject_Classification_Type_TYPE_SPEED_BUMP),
        CREATE_STATIONARY_OBJECT_TYPE_TUPLE(kEmittingStructure, StationaryObject_Classification_Type_TYPE_EMITTING_STRUCTURE)
    ));

// clang-format on

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class StationaryObjectMaterialTestParameterized
    : public StationaryObjectBaseTestParameterized,
      public testing::WithParamInterface<
          std::tuple<osi::StationaryObjectEntityMaterial, astas_osi3::StationaryObject_Classification_Material>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::RoadObject stationary_object;
        stationary_object.material = std::get<0>(GetParam());
        stationary_object.id = GetNewId();
        astas_map_->road_objects.push_back(stationary_object);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_object"));
        auto stationary_object_properties = std::make_unique<mantle_ext::StationaryObjectProperties>();
        stationary_object_properties->type = mantle_api::EntityType::kStatic;
        stationary_object_properties->material = std::get<0>(GetParam());
        entities_.back()->SetProperties(std::move(stationary_object_properties));
    }
};

TEST_P(StationaryObjectMaterialTestParameterized,
       GivenStationaryObjectWithSpecificMaterial_WhenStep_ThenExpectedStationaryObjectHasRightMaterial)
{
    ASSERT_TRUE(gt_.stationary_object_size());

    const auto& actual_map_object_material = gt_.stationary_object(0).classification().material();
    EXPECT_EQ(actual_map_object_material, std::get<1>(GetParam()));

    const auto& actual_entity_object_material = gt_.stationary_object(1).classification().material();
    EXPECT_EQ(actual_entity_object_material, std::get<1>(GetParam()));
}

// clang-format off

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(custom_type, osi_type) \
    std::make_tuple(osi::StationaryObjectEntityMaterial::custom_type,  \
                    astas_osi3::StationaryObject_Classification_Material::osi_type)

INSTANTIATE_TEST_SUITE_P(
    StationaryObjectMaterialTest,
    StationaryObjectMaterialTestParameterized,
    testing::Values(
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kUnknown, StationaryObject_Classification_Material_MATERIAL_UNKNOWN),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kOther, StationaryObject_Classification_Material_MATERIAL_OTHER),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kWood, StationaryObject_Classification_Material_MATERIAL_WOOD),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kPlastic, StationaryObject_Classification_Material_MATERIAL_PLASTIC),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kConcrete, StationaryObject_Classification_Material_MATERIAL_CONCRETE),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kMetal, StationaryObject_Classification_Material_MATERIAL_METAL),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kStone, StationaryObject_Classification_Material_MATERIAL_STONE),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kGlas, StationaryObject_Classification_Material_MATERIAL_GLAS),
        CREATE_STATIONARY_OBJECT_MATERIAL_TUPLE(kMud, StationaryObject_Classification_Material_MATERIAL_MUD)
    ));

// clang-format on

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class StationaryObjectPoseTestParameterized : public StationaryObjectBaseTestParameterized,
                                              public testing::WithParamInterface<mantle_api::Pose>
{
  protected:
    void AddObjectsToMap() override
    {
        map::RoadObject stationary_object;
        stationary_object.pose = GetParam();
        stationary_object.id = GetNewId();
        astas_map_->road_objects.push_back(stationary_object);
    }

    void AddEntities() override
    {
        auto stationary_object_entity =
            std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_object");
        stationary_object_entity->SetPosition(GetParam().position);
        stationary_object_entity->SetOrientation(GetParam().orientation);

        auto stationary_object_properties = std::make_unique<mantle_ext::StationaryObjectProperties>();
        stationary_object_properties->type = mantle_api::EntityType::kStatic;
        stationary_object_entity->SetProperties(std::move(stationary_object_properties));

        entities_.push_back(std::move(stationary_object_entity));
    }
};

TEST_P(StationaryObjectPoseTestParameterized,
       GivenStationaryObjectWithSpecificPose_WhenStep_ThenExpectedStationaryObjectHasRightPositionOrientation)
{
    ASSERT_TRUE(gt_.stationary_object_size());

    const auto& actual_map_object_position = gt_.stationary_object(0).base().position();
    const auto& actual_map_object_orientation = gt_.stationary_object(0).base().orientation();

    const mantle_api::Pose ref_pose(GetParam());

    EXPECT_EQ(actual_map_object_position.x(), ref_pose.position.x.value());
    EXPECT_EQ(actual_map_object_position.y(), ref_pose.position.y.value());
    EXPECT_EQ(actual_map_object_position.z(), ref_pose.position.z.value());

    EXPECT_EQ(actual_map_object_orientation.pitch(), ref_pose.orientation.pitch.value());
    EXPECT_EQ(actual_map_object_orientation.roll(), ref_pose.orientation.roll.value());
    EXPECT_EQ(actual_map_object_orientation.yaw(), ref_pose.orientation.yaw.value());

    const auto& actual_entity_object_position = gt_.stationary_object(1).base().position();
    const auto& actual_entity_object_orientation = gt_.stationary_object(1).base().orientation();

    EXPECT_EQ(actual_entity_object_position.x(), ref_pose.position.x.value());
    EXPECT_EQ(actual_entity_object_position.y(), ref_pose.position.y.value());
    EXPECT_EQ(actual_entity_object_position.z(), ref_pose.position.z.value());

    EXPECT_EQ(actual_entity_object_orientation.pitch(), ref_pose.orientation.pitch.value());
    EXPECT_EQ(actual_entity_object_orientation.roll(), ref_pose.orientation.roll.value());
    EXPECT_EQ(actual_entity_object_orientation.yaw(), ref_pose.orientation.yaw.value());
}

INSTANTIATE_TEST_SUITE_P(StationaryObjectPoseTest,
                         StationaryObjectPoseTestParameterized,
                         testing::Values(mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {1.0_rad, 2.0_rad, 3.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {90.0_deg, 30.0_deg, 180_deg}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {90.0_deg, 30.0_deg, 180_deg}}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class StationaryObjectDimensionsTestParameterized : public StationaryObjectBaseTestParameterized,
                                                    public testing::WithParamInterface<mantle_api::Dimension3>
{
  protected:
    void AddObjectsToMap() override
    {
        map::RoadObject stationary_object;
        stationary_object.dimensions = GetParam();
        stationary_object.id = GetNewId();
        astas_map_->road_objects.push_back(stationary_object);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_object"));
        auto stationary_object_properties = std::make_unique<mantle_ext::StationaryObjectProperties>();
        stationary_object_properties->type = mantle_api::EntityType::kStatic;
        stationary_object_properties->bounding_box.dimension = GetParam();
        entities_.back()->SetProperties(std::move(stationary_object_properties));
    }
};

TEST_P(StationaryObjectDimensionsTestParameterized,
       GivenStationaryObjectWithSpecificDimensions_WhenStep_ThenExpectedStationaryObjectHasRightDimensions)
{
    ASSERT_TRUE(gt_.stationary_object_size());

    const auto& actual_map_object_dimension = gt_.stationary_object(0).base().dimension();
    const auto& actual_entity_object_dimension = gt_.stationary_object(1).base().dimension();

    const mantle_api::Dimension3 ref_dimension(GetParam());

    EXPECT_EQ(actual_map_object_dimension.length(), ref_dimension.length.value());
    EXPECT_EQ(actual_map_object_dimension.width(), ref_dimension.width.value());
    EXPECT_EQ(actual_map_object_dimension.height(), ref_dimension.height.value());

    EXPECT_EQ(actual_entity_object_dimension.length(), ref_dimension.length.value());
    EXPECT_EQ(actual_entity_object_dimension.width(), ref_dimension.width.value());
    EXPECT_EQ(actual_entity_object_dimension.height(), ref_dimension.height.value());
}

INSTANTIATE_TEST_SUITE_P(StationaryObjectDimensionsTest,
                         StationaryObjectDimensionsTestParameterized,
                         testing::Values(mantle_api::Dimension3{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Dimension3{0.1_m, 0.02_m, 0.3_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class StationaryObjectBasePolygonTestParameterized
    : public StationaryObjectBaseTestParameterized,
      public testing::WithParamInterface<std::vector<mantle_api::Vec3<units::length::meter_t>>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::RoadObject stationary_object;
        stationary_object.base_polygon = GetParam();
        stationary_object.id = GetNewId();
        astas_map_->road_objects.push_back(stationary_object);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_object"));
        auto stationary_object_properties = std::make_unique<mantle_ext::StationaryObjectProperties>();
        stationary_object_properties->type = mantle_api::EntityType::kStatic;
        stationary_object_properties->base_polygon = GetParam();
        entities_.back()->SetProperties(std::move(stationary_object_properties));
    }
};

TEST_P(StationaryObjectBasePolygonTestParameterized,
       GivenStationaryObjectWithSpecificBasePolygon_WhenStep_ThenExpectedStationaryObjectHasRightBasePolygon)
{
    ASSERT_TRUE(gt_.stationary_object_size());

    const auto& actual_map_object_base_polygon = gt_.stationary_object(0).base().base_polygon();
    const auto& actual_entity_object_base_polygon = gt_.stationary_object(1).base().base_polygon();

    const std::vector<mantle_api::Vec3<units::length::meter_t>> ref_base_polygon(GetParam());

    for (int i(0); i < ref_base_polygon.size(); ++i)
    {
        const auto& ref_point = ref_base_polygon.at(i);
        const auto& actual_map_point = actual_map_object_base_polygon[i];
        const auto& actual_entity_point = actual_entity_object_base_polygon[i];

        EXPECT_EQ(actual_map_point.x(), ref_point.x.value());
        EXPECT_EQ(actual_map_point.y(), ref_point.y.value());

        EXPECT_EQ(actual_entity_point.x(), ref_point.x.value());
        EXPECT_EQ(actual_entity_point.y(), ref_point.y.value());
    }
}

INSTANTIATE_TEST_SUITE_P(StationaryObjectBasePolygonTest,
                         StationaryObjectBasePolygonTestParameterized,
                         testing::Values(std::vector<mantle_api::Vec3<units::length::meter_t>>{
                             mantle_api::Vec3{0.0_m, 0.0_m, 0.0_m},
                             mantle_api::Vec3{0.1_m, 0.02_m, 0.0_m},
                             mantle_api::Vec3{-0.1_m, -0.2_m, 0.0_m}}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class StationaryObjectModelReferenceTestParameterized : public StationaryObjectBaseTestParameterized,
                                                        public testing::WithParamInterface<std::string>
{
  protected:
    void AddObjectsToMap() override
    {
        map::RoadObject stationary_object;
        stationary_object.name = GetParam();
        stationary_object.id = GetNewId();
        astas_map_->road_objects.push_back(stationary_object);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_object"));
        auto stationary_object_properties = std::make_unique<mantle_ext::StationaryObjectProperties>();
        stationary_object_properties->type = mantle_api::EntityType::kStatic;
        stationary_object_properties->model = GetParam();
        entities_.back()->SetProperties(std::move(stationary_object_properties));
    }
};

TEST_P(StationaryObjectModelReferenceTestParameterized,
       GivenStationaryObjectWithSpecificModelReference_WhenStep_ThenExpectedStationaryObjectHasRightModelReference)
{
    ASSERT_TRUE(gt_.stationary_object_size());

    const auto& actual_map_object_model_reference = gt_.stationary_object(0).model_reference();
    EXPECT_EQ(actual_map_object_model_reference, GetParam());

    const auto& actual_entity_model_reference = gt_.stationary_object(1).model_reference();
    EXPECT_EQ(actual_entity_model_reference, GetParam());
}

INSTANTIATE_TEST_SUITE_P(StationaryObjectModelReferenceTest,
                         StationaryObjectModelReferenceTestParameterized,
                         testing::Values("foo", ""));

}  // namespace astas::environment::proto_groundtruth
