/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/Internal/geometry_helper.h"

#include <gtest/gtest.h>

namespace astas::environment::api
{

using units::literals::operator""_rad;

TEST(GeometryHelperTest, GivenSameOrientations_WhenAreOrientedSimilarly_ThenExpectTrue)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation{1.0_rad, 1.0_rad, 1.0_rad};

    GeometryHelper geometry_helper;
    EXPECT_TRUE(geometry_helper.AreOrientedSimilarly(orientation, orientation));
}

TEST(GeometryHelperTest, GivenOrientationsYaw0DegAnd180Deg_WhenAreOrientedSimilarly_ThenExpectFalse)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation1{0.0_rad, 0.0_rad, 0.0_rad};
    mantle_api::Orientation3<units::angle::radian_t> orientation2{3.14_rad, 0.0_rad, 0.0_rad};

    GeometryHelper geometry_helper;
    EXPECT_FALSE(geometry_helper.AreOrientedSimilarly(orientation1, orientation2));
}

TEST(GeometryHelperTest, GivenOrientationsYaw0DegAnd45Deg_WhenAreOrientedSimilarly_ThenExpectTrue)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation1{0.0_rad, 0.0_rad, 0.0_rad};
    mantle_api::Orientation3<units::angle::radian_t> orientation2{0.8_rad, 0.0_rad, 0.0_rad};

    GeometryHelper geometry_helper;
    EXPECT_TRUE(geometry_helper.AreOrientedSimilarly(orientation1, orientation2));
}

TEST(GeometryHelperTest, GivenOrientationsYaw0DegAndMinus45Deg_WhenAreOrientedSimilarly_ThenExpectTrue)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation1{0.0_rad, 0.0_rad, 0.0_rad};
    mantle_api::Orientation3<units::angle::radian_t> orientation2{-0.8_rad, 0.0_rad, 0.0_rad};

    GeometryHelper geometry_helper;
    EXPECT_TRUE(geometry_helper.AreOrientedSimilarly(orientation1, orientation2));
}

TEST(GeometryHelperTest, GivenOrientationsYaw0DegAnd90Deg_WhenAreOrientedSimilarly_ThenExpectFalse)
{
    mantle_api::Orientation3<units::angle::radian_t> orientation1{0.0_rad, 0.0_rad, 0.0_rad};
    mantle_api::Orientation3<units::angle::radian_t> orientation2{1.5708_rad, 0.0_rad, 0.0_rad};

    GeometryHelper geometry_helper;
    EXPECT_FALSE(geometry_helper.AreOrientedSimilarly(orientation1, orientation2));
}

}  // namespace astas::environment::api
