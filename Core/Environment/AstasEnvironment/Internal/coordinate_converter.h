/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_COORDINATECONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_COORDINATECONVERTER_H

#include "Core/Environment/Map/Common/coordinate_converter.h"

#include <MantleAPI/Map/i_coord_converter.h>

namespace astas::environment::api
{

using UnderlyingMapCoordinate = std::variant<mantle_api::OpenDriveLanePosition, mantle_api::LatLonPosition>;
using Base = environment::map::IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>;

class CoordinateConverter : public mantle_api::ICoordConverter
{
  public:
    explicit CoordinateConverter(Base* underlying_converter) : underlying_converter_{underlying_converter} {}

    mantle_api::Vec3<units::length::meter_t> Convert(mantle_api::Position position) const override;

  private:
    environment::map::IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>*
        underlying_converter_;
};

}  // namespace astas::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_COORDINATECONVERTER_H
