/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/Internal/geometry_helper.h"

#include "Core/Service/GlmWrapper/glm_wrapper.h"

namespace astas::environment::api
{

GeometryHelper::GeometryHelper() = default;

mantle_api::Vec3<units::length::meter_t> GeometryHelper::TranslateGlobalPositionLocally(
    const mantle_api::Vec3<units::length::meter_t>& global_position,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation,
    const mantle_api::Vec3<units::length::meter_t>& local_translation) const
{
    return service::glmwrapper::ToGlobalSpace(global_position, local_orientation, local_translation);
}

std::vector<mantle_api::Vec3<units::length::meter_t>> GeometryHelper::TransformPolylinePointsFromWorldToLocal(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline_points,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation) const
{
    return service::glmwrapper::TransformPolylinePointsFromWorldToLocal(
        polyline_points, local_origin, local_orientation);
}

mantle_api::Vec3<units::length::meter_t> GeometryHelper::TransformPositionFromWorldToLocal(
    const mantle_api::Vec3<units::length::meter_t>& world_position,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation) const
{
    return service::glmwrapper::TransformPositionFromWorldToLocal(world_position, local_origin, local_orientation);
}

mantle_api::Vec3<units::length::meter_t> GeometryHelper::GetDirectionVector(
    const mantle_api::Orientation3<units::angle::radian_t>& orientation) const
{
    const auto direction_vector_glm =
        service::glmwrapper::CreateRotationMatrix(orientation) * service::glmwrapper::ForwardVector();
    mantle_api::Vec3<units::length::meter_t> direction_vector_mantle{};
    direction_vector_mantle.x = units::make_unit<units::length::meter_t>(direction_vector_glm[0]);
    direction_vector_mantle.y = units::make_unit<units::length::meter_t>(direction_vector_glm[1]);
    direction_vector_mantle.z = units::make_unit<units::length::meter_t>(direction_vector_glm[2]);
    return direction_vector_mantle;
}

bool GeometryHelper::AreOrientedSimilarly(const mantle_api::Orientation3<units::angle::radian_t>& orientation1,
                                          const mantle_api::Orientation3<units::angle::radian_t>& orientation2) const
{
    const auto direction1 = GetDirectionVector(orientation1);
    const auto direction2 = GetDirectionVector(orientation2);

    const auto dot_product = service::glmwrapper::Dot(direction1, direction2);
    return dot_product > 0;
}
}  // namespace astas::environment::api
