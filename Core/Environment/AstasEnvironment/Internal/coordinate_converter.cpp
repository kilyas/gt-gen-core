/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/Internal/coordinate_converter.h"

#include <MantleAPI/Common/position.h>

namespace astas::environment::api
{
using units::literals::operator""_m;

mantle_api::Vec3<units::length::meter_t> CoordinateConverter::Convert(mantle_api::Position position) const
{
    UnderlyingMapCoordinate converted_pos;
    if (auto odr_pos = std::get_if<mantle_api::OpenDriveLanePosition>(&position))
    {
        converted_pos = *odr_pos;
    }
    else if (auto nds_pos = std::get_if<mantle_api::LatLonPosition>(&position))
    {
        converted_pos = *nds_pos;
    }
    else if (auto xyz_pos = std::get_if<mantle_api::Vec3<units::length::meter_t>>(&position))
    {
        return *xyz_pos;
    }
    auto ret = underlying_converter_->Convert(converted_pos);

    if (ret.has_value())
    {
        auto value = ret.value();
        return {value.x, value.y, value.z};
    }
    return {-1_m, -1_m, -1_m};
}
}  // namespace astas::environment::api
