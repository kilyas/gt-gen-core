/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/Internal/active_controller_repository.h"

#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Exception/exception.h"

namespace astas::environment::api
{

void ActiveControllerRepository::AddEntityToController(mantle_api::IEntity* entity, CompositeControllerPtr controller)
{
    controller->SetEntity(*entity);
    auto& entity_controllers = entity_id_to_controllers_[entity->GetUniqueId()];
    entity_controllers.push_back(std::move(controller));
}

std::vector<mantle_api::UniqueId> ActiveControllerRepository::GetCurrentEntityIdsWithoutControllers() const
{
    std::vector<mantle_api::UniqueId> ids_without_controllers{};
    for (const auto& id_to_controllers : entity_id_to_controllers_)
    {
        if (id_to_controllers.second.empty())
        {
            ids_without_controllers.push_back(id_to_controllers.first);
        }
    }
    return ids_without_controllers;
}

std::vector<ActiveControllerRepository::CompositeControllerPtr::pointer>
ActiveControllerRepository::GetControllersByEntityId(std::uint64_t entity_id) const
{
    std::vector<ActiveControllerRepository::CompositeControllerPtr::pointer> controllers{};

    if (entity_id_to_controllers_.find(entity_id) != entity_id_to_controllers_.end())
    {
        for (const auto& controller : entity_id_to_controllers_.at(entity_id))
        {
            controllers.push_back(controller.get());
        }
    }

    return controllers;
}

void ActiveControllerRepository::Step(mantle_api::Time current_simulation_time)
{
    RemoveIdsWithoutControllers();

    for (auto& id_to_controllers : entity_id_to_controllers_)
    {
        for (auto& controller : id_to_controllers.second)
        {
            if (controller->IsActive() && !controller->HasFinished())
            {
                controller->Step(current_simulation_time);
            }
        }
    }

    RemoveFinishedControllers();
}

void ActiveControllerRepository::RemoveIdsWithoutControllers()
{
    for (auto it = entity_id_to_controllers_.cbegin(); it != entity_id_to_controllers_.cend();)
    {
        if (it->second.empty())
        {
            entity_id_to_controllers_.erase(it++);
        }
        else
        {
            ++it;
        }
    }
}

void ActiveControllerRepository::RemoveControllersFromEntity(std::uint64_t entity_id)
{
    if (entity_id_to_controllers_.find(entity_id) != entity_id_to_controllers_.end())
    {
        entity_id_to_controllers_.erase(entity_id);
    }
}

void ActiveControllerRepository::RemoveFinishedControllers()
{
    for (auto& [_, controllers] : entity_id_to_controllers_)
    {
        for (size_t i{controllers.size() - 1}; i <= 0; --i)
        {
            if (controllers[i]->HasFinished())
            {
                auto erase_index_it{controllers.begin()};
                std::advance(erase_index_it, i);
                controllers.erase(erase_index_it);
            }
        }
    }
}

}  // namespace astas::environment::api
