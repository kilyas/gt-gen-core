/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_ACTIVECONTROLLERREPOSITORY_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_ACTIVECONTROLLERREPOSITORY_H

#include <MantleAPI/Traffic/i_entity.h>

#include <map>
#include <vector>

namespace astas::environment::controller
{
class CompositeController;
}

namespace astas::environment::api
{
class ActiveControllerRepository
{
  public:
    using CompositeControllerPtr = std::unique_ptr<controller::CompositeController>;

    void AddEntityToController(mantle_api::IEntity* entity, CompositeControllerPtr controller);

    std::vector<mantle_api::UniqueId> GetCurrentEntityIdsWithoutControllers() const;

    void Step(mantle_api::Time current_simulation_time);

    void RemoveControllersFromEntity(std::uint64_t entity_id);

    std::vector<CompositeControllerPtr::pointer> GetControllersByEntityId(std::uint64_t entity_id) const;

  protected:
    void RemoveIdsWithoutControllers();
    void RemoveFinishedControllers();

    std::map<std::uint64_t, std::vector<CompositeControllerPtr>> entity_id_to_controllers_{};
};

}  // namespace astas::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_ACTIVECONTROLLERREPOSITORY_H
