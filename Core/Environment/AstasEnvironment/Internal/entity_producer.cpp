/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/Internal/entity_producer.h"

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/Entities/static_object_entity.h"
#include "Core/Environment/Entities/traffic_light_entity.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/StaticObjects/static_object_properties_provider.h"
#include "Core/Environment/StaticObjects/traffic_light_properties_provider.h"
#include "Core/Environment/StaticObjects/traffic_sign_properties_provider.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <algorithm>

namespace astas::environment::api
{

void EntityProducer::SetUserDefinedProperties(
    mantle_api::StaticObjectProperties* static_object_properties,
    const mantle_api::StaticObjectProperties& user_defined_object_properties) const
{
    if (static_object_properties != nullptr)
    {
        auto it = user_defined_object_properties.properties.find("mount_height");
        if (it != user_defined_object_properties.properties.end())
        {
            static_object_properties->vertical_offset = units::length::meter_t(std::stod(it->second));
        }
        for (const auto& entry : user_defined_object_properties.properties)
        {
            static_object_properties->properties[entry.first] = entry.second;
        }

        // restore geometric center
        static_object_properties->bounding_box.geometric_center =
            user_defined_object_properties.bounding_box.geometric_center;
    }
}
std::unique_ptr<mantle_api::IEntity> EntityProducer::Produce(mantle_api::UniqueId id,
                                                             const std::string& name,
                                                             const mantle_api::VehicleProperties& properties) const
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(id, name);

    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>(properties));

    return entity;
}

std::unique_ptr<mantle_api::IEntity> EntityProducer::Produce(mantle_api::UniqueId id,
                                                             const std::string& name,
                                                             const mantle_api::PedestrianProperties& properties) const
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::PedestrianEntity>(id, name);

    entity->SetProperties(std::make_unique<mantle_api::PedestrianProperties>(properties));

    return entity;
}

std::unique_ptr<mantle_api::IEntity> EntityProducer::Produce(
    mantle_api::UniqueId id,
    const std::string& name,
    const mantle_api::StaticObjectProperties& static_object_properties) const
{
    std::unique_ptr<mantle_api::IEntity> entity;

    std::string identifier;
    auto it = static_object_properties.properties.find("type");
    if (it != static_object_properties.properties.end())
    {
        identifier = it->second;
    }

    it = static_object_properties.properties.find("object_type");
    if (it != static_object_properties.properties.end() &&
        (it->second == "traffic_light" || it->second == "traffic_sign"))
    {
        std::string sub_type = static_object_properties.properties.at("sub_type");
        if (!sub_type.empty())
        {
            identifier += "-";
            identifier += sub_type;
        }
    }

    if (!identifier.empty())
    {
        if (auto properties = static_objects::StaticObjectPropertiesProvider::Get(identifier); properties != nullptr)
        {
            entity = std::make_unique<entities::StaticObject>(id, name);
            auto object_properties = std::make_unique<mantle_api::StaticObjectProperties>(static_object_properties);
            if (object_properties->bounding_box.dimension != properties->bounding_box.dimension)
            {
                Warn("User defined dimensions are being discarded for entity with ID {} and name {}", id, name);
            }
            entity->SetProperties(std::move(properties));
        }
        else if (properties = static_objects::TrafficLightPropertiesProvider::Get(identifier); properties != nullptr)
        {
            entity = std::make_unique<entities::TrafficLightEntity>(id, name);
            entity->SetProperties(std::move(properties));
        }
        else if (properties = static_objects::TrafficSignPropertiesProvider::Get(identifier); properties != nullptr)
        {
            entity = std::make_unique<entities::StaticObject>(id, name);
            entity->SetProperties(std::move(properties));
        }
        else
        {
            throw EnvironmentException{"Not supported static object identifier '{}'. Please check your scenario file",
                                       identifier};
        }
    }
    else
    {
        entity = std::make_unique<entities::StaticObject>(id, name);
        entity->SetProperties(std::make_unique<mantle_api::StaticObjectProperties>(static_object_properties));
    }

    auto properties = dynamic_cast<mantle_api::StaticObjectProperties*>(entity->GetProperties());
    SetUserDefinedProperties(properties, static_object_properties);

    if (auto* traffic_light_entity = dynamic_cast<entities::TrafficLightEntity*>(entity.get()))
    {
        traffic_light_entity->SetInitialStateFromProperties();
    }

    return entity;
}

}  // namespace astas::environment::api
