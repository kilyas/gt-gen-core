/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_IMAPENGINE_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_IMAPENGINE_H

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Service/UserSettings/user_settings.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <MantleAPI/Map/map_details.h>

#include <string>

namespace astas::environment::api
{

class IMapEngine
{
  public:
    virtual ~IMapEngine() = default;

    virtual void Load(const std::string& absolute_map_file_path,
                      const service::user_settings::UserSettings& settings,
                      const mantle_api::MapDetails& map_details,
                      map::AstasMap& astas_map,
                      service::utility::UniqueIdProvider* unique_id_provider) = 0;
};
}  // namespace astas::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_IMAPENGINE_H
