/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_GEOMETRYHELPER_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_GEOMETRYHELPER_H

#include <MantleAPI/Common/i_geometry_helper.h>

#include <vector>

namespace astas::environment::api
{

class GeometryHelper : public mantle_api::IGeometryHelper
{
  public:
    explicit GeometryHelper();

    mantle_api::Vec3<units::length::meter_t> TranslateGlobalPositionLocally(
        const mantle_api::Vec3<units::length::meter_t>& global_position,
        const mantle_api::Orientation3<units::angle::radian_t>& local_orientation,
        const mantle_api::Vec3<units::length::meter_t>& local_translation) const override;

    std::vector<mantle_api::Vec3<units::length::meter_t>> TransformPolylinePointsFromWorldToLocal(
        const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline_points,
        const mantle_api::Vec3<units::length::meter_t>& local_origin,
        const mantle_api::Orientation3<units::angle::radian_t>& local_orientation) const override;

    mantle_api::Vec3<units::length::meter_t> TransformPositionFromWorldToLocal(
        const mantle_api::Vec3<units::length::meter_t>& world_position,
        const mantle_api::Vec3<units::length::meter_t>& local_origin,
        const mantle_api::Orientation3<units::angle::radian_t>& local_orientation) const override;

    mantle_api::Vec3<units::length::meter_t> GetDirectionVector(
        const mantle_api::Orientation3<units::angle::radian_t>& orientation) const;

    bool AreOrientedSimilarly(const mantle_api::Orientation3<units::angle::radian_t>& orientation1,
                              const mantle_api::Orientation3<units::angle::radian_t>& orientation2) const override;
};

}  // namespace astas::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_GEOMETRYHELPER_H
