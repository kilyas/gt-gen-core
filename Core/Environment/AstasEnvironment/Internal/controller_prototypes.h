/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_CONTROLLERPROTOTYPES_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_CONTROLLERPROTOTYPES_H

#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Controller/external_controller_config_converter.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <MantleAPI/Traffic/i_controller_repository.h>

#include <memory>
#include <vector>

namespace astas::environment::api
{

class ControllerPrototypes : public mantle_api::IControllerRepository
{
  public:
    explicit ControllerPrototypes(service::utility::UniqueIdProvider* unique_id_provider);

    mantle_api::IController& Create(std::unique_ptr<mantle_api::IControllerConfig> config) override;
    mantle_api::IController& Create(mantle_api::UniqueId id,
                                    std::unique_ptr<mantle_api::IControllerConfig> config) override;

    std::optional<std::reference_wrapper<mantle_api::IController>> Get(mantle_api::UniqueId id) override
    {
        // not used yet so not implemented
        std::ignore = id;
        return {};
    }

    bool Contains(mantle_api::UniqueId id) const override
    {
        // not used yet so not implemented
        std::ignore = id;
        return false;
    }

    void Delete(mantle_api::UniqueId id) override
    {
        // not used yet so not implemented
        std::ignore = id;
    }

    std::unique_ptr<controller::CompositeController> Move(mantle_api::UniqueId id);

    void SetIsEntityAllowedToLeaveLane(bool is_entity_allowed_to_leave_lane);
    void SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior);
    void SetConfigConverter(controller::ExternalControllerConfigConverter* converter);
    void SetLaneLocationProvider(map::LaneLocationProvider* lane_location_provider);

  private:
    [[nodiscard]] controller::CompositeController* Find(mantle_api::UniqueId id) const;

    std::vector<std::unique_ptr<controller::CompositeController>> template_controllers_{};
    bool is_entity_allowed_to_leave_lane_{false};
    service::utility::UniqueIdProvider* unique_id_provider_{nullptr};
    controller::ExternalControllerConfigConverter* config_converter_{nullptr};
    map::LaneLocationProvider* lane_location_provider_{nullptr};
    mantle_api::DefaultRoutingBehavior default_routing_behavior_{mantle_api::DefaultRoutingBehavior::kStop};
};

}  // namespace astas::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_CONTROLLERPROTOTYPES_H
