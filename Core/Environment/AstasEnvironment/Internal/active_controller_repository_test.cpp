/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/Internal/active_controller_repository.h"

#include "Core/Environment/AstasEnvironment/Internal/controller_prototypes.h"
#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Environment/Controller/Internal/ControlUnits/no_op_control_unit.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::api
{

class ActiveControllerRepositorySut : public ActiveControllerRepository
{
  public:
    const std::map<std::uint64_t, std::vector<CompositeControllerPtr>>& GetEntityIdToControllers() const
    {
        return entity_id_to_controllers_;
    }
};

EntityRepository GetEntityRepoWithHostAndTraffic()
{
    mantle_api::VehicleProperties host_properties;
    host_properties.is_host = true;
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(0, "host", host_properties);
    repo.Create(1, "vehicle", mantle_api::VehicleProperties{});
    return repo;
}

ControllerPrototypes GetControllerPrototypesWithOneNoOpController(
    std::uint64_t controller_id,
    service::utility::UniqueIdProvider* unique_id_provider)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    ControllerPrototypes prototypes(unique_id_provider);
    prototypes.Create(controller_id, std::move(config));
    return prototypes;
}

ControllerPrototypes GetControllerPrototypesWithMultipleNoOpControllers(
    const std::vector<std::uint64_t>& controller_ids,
    service::utility::UniqueIdProvider* unique_id_provider)
{
    ControllerPrototypes prototypes(unique_id_provider);
    for (const auto& controller_id : controller_ids)
    {
        prototypes.Create(controller_id, std::make_unique<mantle_api::NoOpControllerConfig>());
    }
    return prototypes;
}

TEST(EntityControllerTest, GivenEntityController_WhenConstructed_ThenNoEntriesInAssingedControllerMap)
{
    ActiveControllerRepositorySut active_controller_repo{};

    EXPECT_TRUE(active_controller_repo.GetEntityIdToControllers().empty());
}

TEST(EntityControllerTest, GivenNoAddedControllers_WhenAddingEntityToController_ThenEntityAssignedToController)
{
    std::uint64_t expected_controller_id{555};
    auto entity_repo = GetEntityRepoWithHostAndTraffic();
    service::utility::UniqueIdProvider unique_id_provider;
    auto controller_prototypes =
        GetControllerPrototypesWithOneNoOpController(expected_controller_id, &unique_id_provider);

    ActiveControllerRepositorySut active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.GetHost(),
                                                 controller_prototypes.Move(expected_controller_id));

    const auto& entity_id_to_controllers_map = active_controller_repo.GetEntityIdToControllers();
    ASSERT_EQ(1, entity_id_to_controllers_map.size());

    const auto& assigned_controllers = entity_id_to_controllers_map.at(0);
    ASSERT_FALSE(assigned_controllers.empty());
    EXPECT_EQ(expected_controller_id, assigned_controllers.at(0)->GetUniqueId());
}

TEST(EntityControllerTest,
     GivenNoAddedControllers_WhenAddingTwoControllersToSameEntity_ThenControllersAreAddedSuccessfully)
{
    std::vector<std::uint64_t> controller_ids{555, 556};

    auto entity_repo = GetEntityRepoWithHostAndTraffic();
    service::utility::UniqueIdProvider unique_id_provider;
    auto controller_prototypes =
        GetControllerPrototypesWithMultipleNoOpControllers(controller_ids, &unique_id_provider);

    ActiveControllerRepositorySut active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.GetHost(), controller_prototypes.Move(controller_ids[0]));
    active_controller_repo.AddEntityToController(&entity_repo.GetHost(), controller_prototypes.Move(controller_ids[1]));
    EXPECT_EQ(active_controller_repo.GetControllersByEntityId(entity_repo.GetHost().GetUniqueId()).size(), 2);
}

class MockController : public controller::CompositeController
{
  public:
    MockController() : controller::CompositeController(42, nullptr) {}

    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
};

TEST(EntityControllerTest, GivenEntityAndController_WhenAddEntityToController_ThenEntityIsSetToController)
{
    auto entity_repo = GetEntityRepoWithHostAndTraffic();

    auto mock_controller = std::make_unique<MockController>();
    MockController* mock_controller_ptr = mock_controller.get();

    EXPECT_CALL(*mock_controller_ptr, SetEntity(testing::_)).Times(1);

    ActiveControllerRepository active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.GetHost(), std::move(mock_controller));
}

TEST(EntityControllerTest, GivenEntityControllerWithOneAssignedController_WhenStep_ThenControllerStepIsCalledInEachStep)
{
    auto entity_repo = GetEntityRepoWithHostAndTraffic();

    auto mock_controller = std::make_unique<MockController>();
    mock_controller->ChangeState(mantle_api::IController::LateralState::kActivate,
                                 mantle_api::IController::LongitudinalState::kActivate);

    EXPECT_CALL(*mock_controller, Step(testing::_)).Times(2);

    ActiveControllerRepository active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.GetHost(), std::move(mock_controller));

    active_controller_repo.Step(mantle_api::Time{0});
    active_controller_repo.Step(mantle_api::Time{40});
}

TEST(EntityControllerTest, GivenTwoEntitiesWithAnAssignedControllerEach_WhenStep_ThenEachControllerStepIsCalled)
{
    auto entity_repo = GetEntityRepoWithHostAndTraffic();

    auto mock_controller1 = std::make_unique<MockController>();
    auto mock_controller2 = std::make_unique<MockController>();

    mock_controller1->ChangeState(mantle_api::IController::LateralState::kActivate,
                                  mantle_api::IController::LongitudinalState::kActivate);
    mock_controller2->ChangeState(mantle_api::IController::LateralState::kActivate,
                                  mantle_api::IController::LongitudinalState::kActivate);

    MockController* mock_controller1_ptr = mock_controller1.get();
    MockController* mock_controller2_ptr = mock_controller2.get();

    ActiveControllerRepository active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.Get(0)->get(), std::move(mock_controller1));
    active_controller_repo.AddEntityToController(&entity_repo.Get(1)->get(), std::move(mock_controller2));

    EXPECT_CALL(*mock_controller1_ptr, Step(testing::_)).Times(1);
    EXPECT_CALL(*mock_controller2_ptr, Step(testing::_)).Times(1);

    active_controller_repo.Step(mantle_api::Time{0});
}

TEST(EntityControllerTest, GivenEntityWithFinishedController_WhenStep_ThenFinishedControllerIsRemoved)
{
    auto entity_repo = GetEntityRepoWithHostAndTraffic();

    auto finished_controller = std::make_unique<MockController>();
    ON_CALL(*finished_controller, HasFinished()).WillByDefault(::testing::Return(true));

    ActiveControllerRepositorySut active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.GetHost(), std::move(finished_controller));

    const auto& ids_to_controllers = active_controller_repo.GetEntityIdToControllers();
    ASSERT_EQ(1, ids_to_controllers.size());
    ASSERT_FALSE(ids_to_controllers.at(0).empty());

    active_controller_repo.Step(mantle_api::Time{0});

    EXPECT_EQ(1, ids_to_controllers.size());
    EXPECT_TRUE(ids_to_controllers.at(0).empty());
}

TEST(EntityControllerTest, GivenJustFinishedControllerForTraffic_WhenStep_ThenListOfEntityIdsWithoutControllersEmpty)
{
    mantle_api::UniqueId expected_traffic_id{1};
    auto entity_repo = GetEntityRepoWithHostAndTraffic();

    auto not_finished_controller = std::make_unique<MockController>();

    ActiveControllerRepository active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.Get(expected_traffic_id)->get(),
                                                 std::move(not_finished_controller));

    active_controller_repo.Step(mantle_api::Time{0});

    EXPECT_TRUE(active_controller_repo.GetCurrentEntityIdsWithoutControllers().empty());
}

TEST(EntityControllerTest,
     GivenFinishedControllerForTraffic_WhenStep_ThenTrafficIdWillBeInTheListOfEntitiesWithoutControllers)
{
    mantle_api::UniqueId expected_traffic_id{1};
    auto entity_repo = GetEntityRepoWithHostAndTraffic();

    auto finished_controller = std::make_unique<MockController>();
    ON_CALL(*finished_controller, HasFinished()).WillByDefault(::testing::Return(true));

    ActiveControllerRepository active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.Get(expected_traffic_id)->get(),
                                                 std::move(finished_controller));

    active_controller_repo.Step(mantle_api::Time{0});

    auto ids_without_controllers = active_controller_repo.GetCurrentEntityIdsWithoutControllers();
    ASSERT_EQ(1, ids_without_controllers.size());
    EXPECT_EQ(expected_traffic_id, ids_without_controllers.front());
}

TEST(EntityControllerTest, GivenJustFinishedControllerForTraffic_WhenStepTwice_ThenTrafficEntryIsNotInMapAnymore)
{
    mantle_api::UniqueId expected_traffic_id{1};
    auto entity_repo = GetEntityRepoWithHostAndTraffic();

    auto finished_controller = std::make_unique<MockController>();
    ON_CALL(*finished_controller, HasFinished()).WillByDefault(::testing::Return(true));

    ActiveControllerRepositorySut active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.Get(expected_traffic_id)->get(),
                                                 std::move(finished_controller));

    active_controller_repo.Step(mantle_api::Time{0});

    const auto& ids_to_controllers = active_controller_repo.GetEntityIdToControllers();
    ASSERT_EQ(1, ids_to_controllers.size());

    active_controller_repo.Step(mantle_api::Time{40});
    EXPECT_TRUE(ids_to_controllers.empty());
}

TEST(EntityControllerTest,
     GivenEntityWithController_WhenRemoveControllersFromEntity_ThenListOfEntityIdsWithoutControllersEmpty)
{
    auto entity_repo = GetEntityRepoWithHostAndTraffic();
    auto mock_controller = std::make_unique<MockController>();
    ActiveControllerRepositorySut active_controller_repo{};
    active_controller_repo.AddEntityToController(&entity_repo.GetHost(), std::move(mock_controller));

    const auto& ids_to_controllers = active_controller_repo.GetEntityIdToControllers();
    ASSERT_EQ(1, ids_to_controllers.size());

    active_controller_repo.RemoveControllersFromEntity(entity_repo.GetHost().GetUniqueId());

    ASSERT_TRUE(ids_to_controllers.empty());
}

}  // namespace astas::environment::api
