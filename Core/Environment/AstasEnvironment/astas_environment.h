/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_ASTASENVIRONMENT_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_ASTASENVIRONMENT_H

#include "Core/Environment/AstasEnvironment/Internal/active_controller_repository.h"
#include "Core/Environment/AstasEnvironment/Internal/controller_prototypes.h"
#include "Core/Environment/AstasEnvironment/Internal/coordinate_converter.h"
#include "Core/Environment/AstasEnvironment/Internal/geometry_helper.h"
#include "Core/Environment/AstasEnvironment/Internal/i_map_engine.h"
#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Environment/AstasEnvironment/i_astas_environment.h"
#include "Core/Environment/Controller/external_controller_config_converter.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/Host/host_vehicle_interface.h"
#include "Core/Environment/Host/host_vehicle_model.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Environment/TrafficCommand/traffic_command_builder.h"
#include "Core/Environment/TrafficSwarm/Xosc/traffic_swarm_xosc.h"
#include "Core/Service/UserSettings/user_settings.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/EnvironmentalConditions/road_condition.h>
#include <MantleAPI/EnvironmentalConditions/weather.h>

namespace astas::environment::api
{

class AstasEnvironment : public IAstasEnvironment
{
  public:
    explicit AstasEnvironment(const service::user_settings::UserSettings& user_settings,
                              const mantle_api::Time& step_size);

    void Init() override;
    void Step() override;

    void CreateMap(const std::string& absolute_map_file_path, const mantle_api::MapDetails& map_details) override;

    void AddEntityToController(mantle_api::IEntity& entity, std::uint64_t controller_id) override;
    void UpdateControlStrategies(mantle_api::UniqueId entity_id,
                                 std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) override;

    bool HasControlStrategyGoalBeenReached(std::uint64_t entity_id,
                                           mantle_api::ControlStrategyType type) const override;

    const mantle_api::ILaneLocationQueryService& GetQueryService() const override;
    const mantle_api::ICoordConverter* GetConverter() const override;
    const mantle_api::IGeometryHelper* GetGeometryHelper() const override;

    mantle_api::IEntityRepository& GetEntityRepository() override;
    const mantle_api::IEntityRepository& GetEntityRepository() const override;
    mantle_api::IControllerRepository& GetControllerRepository() override;
    const mantle_api::IControllerRepository& GetControllerRepository() const override;

    void SetWeather(mantle_api::Weather weather) override;
    void SetRoadCondition(std::vector<mantle_api::FrictionPatch> friction_patches) override;
    void SetTrafficSignalState(const std::string& traffic_signal_name,
                               const std::string& traffic_signal_state) override;
    void ExecuteCustomCommand(const std::vector<std::string>& actors,
                              const std::string& type,
                              const std::string& command) override;
    void SetUserDefinedValue(const std::string& name, const std::string& value) override;
    std::optional<std::string> GetUserDefinedValue(const std::string& name) override;

    void SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior) override;
    void AssignRoute(mantle_api::UniqueId entity_id, mantle_api::RouteDefinition route_definition) override;

    host::HostVehicleModel& GetHostVehicleModel() override;
    const host::HostVehicleInterface& GetHostVehicleInterface() const override;

    // TODO: NO need to provide access to both, GT and GT builder
    // The "old" idea was, to expose the sv builder such that the GT can be accessed via a SV builder's public mutex...
    // We should think if we can do this differently: We do not need the SV and GT building logic but only the SV and GT
    // data itself
    const proto_groundtruth::SensorViewBuilder& GetSensorViewBuilder() const override;
    const astas_osi3::SensorView& GetSensorView() const override;
    const std::vector<astas_osi3::TrafficCommand>& GetTrafficCommands() const override;

    chunking::StaticChunkList GetStaticChunks() const override;  // TODO: move to proto-gt-builder

    const map::AstasMap& GetAstasMap() const override;

    void SetDateTime(mantle_api::Time date_time) override;
    mantle_api::Time GetDateTime() override;
    mantle_api::Time GetSimulationTime() override;
    void RemoveEntityFromController(std::uint64_t entity_id, std::uint64_t controller_id) override;

    mutable std::mutex user_defined_value_mutex{};

    ActiveControllerRepository* GetActiveControllerRepository() { return &active_controller_repository_; }

    mantle_api::ITrafficSwarmService& GetTrafficSwarmService() override;

    void InitTrafficSwarmService(const mantle_api::TrafficSwarmParameters& parameters) override;

    void SetMapEngine(std::unique_ptr<IMapEngine> map_engine);

  protected:
    std::unique_ptr<environment::map::AstasMap> astas_map_{nullptr};

  private:
    controller::ExternalControllerConfigConverterData GetExternalControllerConfigConverterData() const;

    void StepEntities();

    service::user_settings::UserSettings user_settings_;
    service::utility::UniqueIdProvider unique_id_provider_{};
    std::unique_ptr<controller::ExternalControllerConfigConverter> config_converter_{nullptr};
    EntityRepository entity_repository_{&unique_id_provider_};
    ControllerPrototypes controller_prototypes_{&unique_id_provider_};
    ActiveControllerRepository active_controller_repository_{};

    std::unique_ptr<mantle_api::ICoordConverter> coordinate_converter_{nullptr};
    GeometryHelper geometry_helper_;

    std::vector<mantle_api::FrictionPatch> friction_patches_;

    std::unique_ptr<proto_groundtruth::SensorViewBuilder> sensor_view_builder_{nullptr};
    std::unique_ptr<traffic_command::TrafficCommandBuilder> traffic_command_builder_{nullptr};
    std::unique_ptr<map::LaneLocationProvider> lane_location_provider_{nullptr};

    std::unique_ptr<host::HostVehicleInterface> host_vehicle_interface_{nullptr};
    std::unique_ptr<host::HostVehicleModel> host_vehicle_model_{nullptr};

    std::map<std::string, std::string> user_defined_values_{};

    mantle_api::Time step_size_{mantle_api::Time{0}};

    std::unique_ptr<traffic_swarm::TrafficSwarmXosc> traffic_swarm_{nullptr};
    std::unique_ptr<IMapEngine> map_engine_{};
};

}  // namespace astas::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_ASTASENVIRONMENT_H
