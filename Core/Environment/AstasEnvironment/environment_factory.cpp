/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/environment_factory.h"

#include "Core/Environment/AstasEnvironment/astas_environment.h"
namespace astas::environment::api
{

std::unique_ptr<environment::api::AstasEnvironment> EnvironmentFactory::Create(
    const service::user_settings::UserSettings& user_settings,
    const mantle_api::Time step_size)
{
    return std::make_unique<environment::api::AstasEnvironment>(user_settings, step_size);
}
}  // namespace astas::environment::api
