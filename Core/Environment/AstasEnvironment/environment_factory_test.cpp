/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/environment_factory.h"

#include <gtest/gtest.h>

namespace astas::environment::api
{
using units::literals::operator""_ms;

TEST(EnvironmentFactoryTest, GivenUserSettings_WhenCreatingEnvironment_ThenAstasEnvironmentCreated)
{
    auto environment = EnvironmentFactory::Create(service::user_settings::UserSettings{}, 40_ms);
    EXPECT_NE(environment, nullptr);
}

}  // namespace astas::environment::api
