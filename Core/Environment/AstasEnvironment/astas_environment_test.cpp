/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/astas_environment.h"

#include "Core/Environment/AstasEnvironment/Internal/i_map_engine.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MockMapEngine/mock_map_engine.h"
#include "Core/Tests/TestUtils/convert_first_custom_traffic_command_of_host_vehicle_to_route.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <gtest/gtest.h>

namespace astas::environment::api
{
using units::literals::operator""_ms;
using units::literals::operator""_m;

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateController_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_NO_THROW(env.GetControllerRepository().Create(0, std::move(config)));
}

TEST(EnvironmentApiControllerTest, GivenEnvironmentControllerAndEntity_WhenAddingEntityToController_ThenAddingSucceeds)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    auto& entity_repository = env.GetEntityRepository();

    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    ASSERT_NO_THROW(env.GetControllerRepository().Create(0, std::move(config)));
    ASSERT_NO_THROW(entity_repository.Create(0, "vehicle", mantle_api::VehicleProperties{}));

    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(0)->get(), 0));
}

TEST(EnvironmentApiControllerTest, GivenEnvironmentWithoutController_WhenAddingEntityToController_ThenExceptionThrown)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    auto& entity_repository = env.GetEntityRepository();

    EXPECT_THROW(env.AddEntityToController(entity_repository.Get(0)->get(), 0), EnvironmentException);
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithCompositeControllerWithEntity_WhenUpdatingControlStrategies_ThenUpdateSucceeds)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    ASSERT_NO_THROW(env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity_id, control_strategies));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithExternalCompositeControllerWithEntity_WhenUpdatingControlStrategies_ThenControlUnitsUnchanged)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    env.Init();

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    ASSERT_NO_THROW(env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_shared<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity_id, control_strategies));

    auto controllers = env.GetActiveControllerRepository()->GetControllersByEntityId(entity_id);
    ASSERT_EQ(controllers.size(), 1);
    auto& control_units = controllers.front()->GetControlUnits();
    ASSERT_EQ(2, control_units.size());
    EXPECT_EQ(control_units[0]->GetName(), "HostVehicleInterfaceControlUnit");
    EXPECT_EQ(control_units[1]->GetName(), "VehicleModelControlUnit");
}

TEST(
    EnvironmentApiControllerTest,
    GivenEnvironmentWithExternalCompositeControllerWithEntity_WhenUpdatingControlStrategiesAndStep_ThenTrafficCommandAvailable)
{
    auto map_engine = std::make_unique<test_utils::MockMapEngine>();
    auto map = test_utils::MapCatalogue::MapStraightRoad2km();
    test_utils::MockCoordinateConverter* coordinate_converter =
        dynamic_cast<test_utils::MockCoordinateConverter*>(map->coordinate_converter.get());
    EXPECT_CALL(*coordinate_converter, Convert(testing::_)).Times(1);
    map_engine->SetMap(std::move(map));

    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    env.SetMapEngine(std::move(map_engine));
    env.Init();

    const fs::path maps_path{"external"};
    env.CreateMap(maps_path / "Dummy.xodr", {});

    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    auto& entity_repository = env.GetEntityRepository();
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = entity_repository.Create("Host", vehicle_properties);
    EXPECT_NO_THROW(env.AddEntityToController(entity_repository.Get("Host")->get(), controller.GetUniqueId()));

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>());
    EXPECT_NO_THROW(env.UpdateControlStrategies(entity.GetUniqueId(), control_strategies));
    env.Step();
    EXPECT_EQ(1, env.GetTrafficCommands().size());
}

TEST(EnvironmentApiControllerTest, GivenEnvironmentWithoutActiveController_WhenUpdatingControlStrategies_ThenThrow)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    EXPECT_THROW(env.UpdateControlStrategies(0, control_strategies), EnvironmentException);
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithInternalControllerWithoutControlUnit_WhenAskingGoalReached_ThenThrow)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_THROW(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepVelocity),
                 EnvironmentException);
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithInternalCompositeControllerWithEntity_WhenAssignRoute_ThenControllerContainsPathControlUnit)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 0;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    mantle_api::UniqueId entity_id = 10;
    auto& entity_repository = env.GetEntityRepository();
    env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity_repository.Get(entity_id)->get(), controller_id);
    auto composite_controllers = env.GetActiveControllerRepository()->GetControllersByEntityId(entity_id);
    ASSERT_EQ(composite_controllers.size(), 1);
    EXPECT_EQ(composite_controllers.front()->GetControlUnits().size(), 0);

    mantle_api::RouteDefinition route_definition;
    route_definition.waypoints.push_back({{0_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    route_definition.waypoints.push_back({{10_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    EXPECT_NO_THROW(env.AssignRoute(entity_id, route_definition));

    auto& control_units = composite_controllers.front()->GetControlUnits();
    ASSERT_EQ(1, control_units.size());
    EXPECT_EQ(control_units[0]->GetName(), "PathControlUnit");
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithHostVehicleInterfaceControlUnit_WhenAssignRoute_ThenHostVehicleTrafficCommandsContainRoute)
{
    auto map_engine = std::make_unique<test_utils::MockMapEngine>();
    map_engine->SetMap(test_utils::MapCatalogue::MapStraightRoad2km());
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    env.SetMapEngine(std::move(map_engine));
    env.Init();

    const fs::path maps_path{"external"};
    env.CreateMap(maps_path / "Dummy.xodr", {});

    mantle_api::UniqueId entity_id = 0;
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.is_host = true;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", vehicle_properties);
    auto& controller = env.GetControllerRepository().Create(std::make_unique<mantle_api::ExternalControllerConfig>());
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);
    env.AddEntityToController(entity, controller.GetUniqueId());
    mantle_api::RouteDefinition route_definition;
    route_definition.waypoints.push_back({{0_m, 0_m, 0_m}, mantle_api::RouteStrategy::kShortest});
    route_definition.waypoints.push_back({{0_m, 100_m, 0_m}, mantle_api::RouteStrategy::kShortest});

    EXPECT_NO_THROW(env.AssignRoute(entity_id, route_definition));
    env.Step();
    const auto actual_path =
        astas::test_utils::ConvertFirstCustomTrafficCommandOfHostVehicleToRoute(env.GetTrafficCommands());
    EXPECT_FALSE(actual_path.empty());
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForKeepVelocityControlStrategy_WhenAskingGoalReached_ThenReturnFalse)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_FALSE(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepVelocity));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForKeepLaneOffsetControlStrategy_WhenAskingGoalReached_ThenReturnFalse)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepLaneOffsetControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_FALSE(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kKeepLaneOffset));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForFollowEmptyHeadingSplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowHeadingSplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowHeadingSpline));
}

TEST(
    EnvironmentApiControllerTest,
    GivenEnvironmentWithControllerForFollowEmptyLateralOffsetSplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowLateralOffsetSplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowLateralOffsetSpline));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForFollowEmptyVelocitySplineControlStrategy_WhenAskingGoalReached_ThenReturnTrue)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::FollowVelocitySplineControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_TRUE(
        env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kFollowVelocitySpline));
}

TEST(EnvironmentApiControllerTest,
     GivenEnvironmentWithControllerForAcquireLaneOffsetControlStrategy_WhenAskingGoalReached_ThenThrow)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    std::uint64_t controller_id = 1;
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::AcquireLaneOffsetControlStrategy>());
    auto& controller = env.GetControllerRepository().Create(controller_id, std::move(config));
    controller.ChangeState(mantle_api::IController::LateralState::kActivate,
                           mantle_api::IController::LongitudinalState::kActivate);

    std::uint64_t entity_id = 0;
    auto& entity = env.GetEntityRepository().Create(entity_id, "vehicle", mantle_api::VehicleProperties{});
    env.AddEntityToController(entity, controller_id);

    EXPECT_THROW(env.HasControlStrategyGoalBeenReached(entity_id, mantle_api::ControlStrategyType::kAcquireLaneOffset),
                 EnvironmentException);
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenSettingTimeInEnvironment_ThenEnvironmentReturnsCorrectTime)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    service::utility::Clock::Instance().SetNow(mantle_api::Time{0});

    EXPECT_EQ(0_ms, env.GetSimulationTime());

    service::utility::Clock::Instance().SetNow(mantle_api::Time{100});
    EXPECT_EQ(100_ms, env.GetSimulationTime());

    service::utility::Clock::Instance().SetNow(mantle_api::Time{150});
    EXPECT_EQ(150_ms, env.GetSimulationTime());
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateTpmControlUnit_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    const fs::path maps_path{"external"};

    config->name = "traffic_participant_model_example";

    auto map_engine = std::make_unique<test_utils::MockMapEngine>();
    map_engine->SetMap(test_utils::MapCatalogue::MapStraightRoad2km());
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    env.SetMapEngine(std::move(map_engine));
    env.Init();
    env.CreateMap(maps_path / "Dummy.xodr", {});
    EXPECT_NO_THROW(env.GetControllerRepository().Create(42, std::move(config)));
}

TEST(EnvironmentApiControllerTest, GivenEnvironment_WhenCreateExternalHostConfig_ThenCreationSucceeds)
{
    auto config = std::make_unique<mantle_api::ExternalControllerConfig>();
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};

    env.Init();

    EXPECT_NO_THROW(env.GetControllerRepository().Create(42, std::move(config)));
}

TEST(EnvironmentApiCustomCommandTest, GivenEnvironment_WhenExecuteCustomCommand_ThenNoThrow)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_NO_THROW(env.ExecuteCustomCommand({"actor1", "actor2"}, "MyType", "MyCommand"));
}

TEST(EnvironmentApiUserDefinedValueTest, GivenEnvironment_WhenUserDefinedValueNotSet_ThenGetReturnsNoValue)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    EXPECT_FALSE(env.GetUserDefinedValue("MyName").has_value());
}

TEST(EnvironmentApiUserDefinedValueTest, GivenEnvironment_WhenUserDefinedValueSet_ThenGetReturnsValue)
{
    AstasEnvironment env{service::user_settings::UserSettings{}, 40_ms};
    env.SetUserDefinedValue("MyName", "MyValue");
    ASSERT_TRUE(env.GetUserDefinedValue("MyName").has_value());
    EXPECT_EQ("MyValue", env.GetUserDefinedValue("MyName").value());
}

}  // namespace astas::environment::api
