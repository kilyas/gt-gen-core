/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/astas_environment.h"

#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"
#include "Core/Environment/Controller/Internal/internal_controller_factory.h"
#include "Core/Environment/Controller/external_controller_config_converter.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/clock.h"

namespace astas::environment::api
{

constexpr units::length::meter_t longitudinal_distance_between_traffic_swarm_vehicles(50.0);

AstasEnvironment::AstasEnvironment(const service::user_settings::UserSettings& user_settings,
                                   const mantle_api::Time& step_size)
    : user_settings_{user_settings}, step_size_{step_size}
{
}

void AstasEnvironment::Init()
{
    astas_map_ = std::make_unique<map::AstasMap>();

    const auto& chunking_settings = user_settings_.map_chunking;
    sensor_view_builder_ =
        std::make_unique<proto_groundtruth::SensorViewBuilder>(*astas_map_, step_size_, chunking_settings);

    traffic_command_builder_ = std::make_unique<traffic_command::TrafficCommandBuilder>();
    host_vehicle_interface_ = std::make_unique<host::HostVehicleInterface>();
    host_vehicle_model_ = std::make_unique<host::HostVehicleModel>();
    lane_location_provider_ = std::make_unique<map::LaneLocationProvider>(*astas_map_);

    controller_prototypes_.SetIsEntityAllowedToLeaveLane(user_settings_.ground_truth.allow_invalid_lane_locations);

    config_converter_ =
        std::make_unique<controller::ExternalControllerConfigConverter>(GetExternalControllerConfigConverterData());

    controller_prototypes_.SetConfigConverter(config_converter_.get());
    controller_prototypes_.SetLaneLocationProvider(lane_location_provider_.get());
}

void AstasEnvironment::CreateMap(const std::string& absolute_map_file_path, const mantle_api::MapDetails& map_details)
{
    if (!map_engine_)
    {
        throw EnvironmentException("No MapEngine is set in the Environment.");
    }

    map_engine_->Load(absolute_map_file_path, user_settings_, map_details, *astas_map_, &unique_id_provider_);
    coordinate_converter_ = std::make_unique<CoordinateConverter>(astas_map_->coordinate_converter.get());

    sensor_view_builder_->Init();
}

controller::ExternalControllerConfigConverterData AstasEnvironment::GetExternalControllerConfigConverterData() const
{
    controller::ProtoGroundTruthBuilderConfig proto_ground_truth_builder_config{step_size_, &entity_repository_};
    controller::ExternalControllerConfigConverterData data{proto_ground_truth_builder_config,
                                                           astas_map_.get(),
                                                           host_vehicle_interface_.get(),
                                                           host_vehicle_model_.get(),
                                                           &user_settings_,
                                                           traffic_command_builder_.get()};

    return data;
}

void AstasEnvironment::AddEntityToController(mantle_api::IEntity& entity, std::uint64_t controller_id)
{
    Info("Adding entity #{} to controller #{}", entity.GetUniqueId(), controller_id);
    auto controller = controller_prototypes_.Move(controller_id);
    active_controller_repository_.AddEntityToController(&entity, std::move(controller));
}

void AstasEnvironment::RemoveEntityFromController(std::uint64_t entity_id, std::uint64_t controller_id)
{
    Info("Remove controllers of entity #{}", entity_id);
    std::ignore = controller_id;
    active_controller_repository_.RemoveControllersFromEntity(entity_id);
}

void AstasEnvironment::UpdateControlStrategies(
    mantle_api::UniqueId entity_id,
    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies)
{
    auto composite_controllers = active_controller_repository_.GetControllersByEntityId(entity_id);
    if (composite_controllers.empty())
    {
        throw EnvironmentException(
            "Cannot get active controller for entity with id {}. Please check if "
            "controller was created and assigned an entity.",
            entity_id);
    }

    for (auto& composite_controller : composite_controllers)
    {
        if (!composite_controller->IsActive())
        {
            continue;
        }

        if (composite_controller->GetType() == controller::CompositeController::Type::kExternal)
        {
            Debug("Adding control strategies for entity {} to TrafficCommandBuilder", entity_id);
            traffic_command_builder_->AddControlStrategiesForEntity(entity_id, control_strategies);
        }
        else
        {
            for (const auto& control_strategy : control_strategies)
            {
                Debug("Trying to add control unit for control strategy {} to controller #{}",
                      control_strategy->type,
                      composite_controller->GetUniqueId());
                if (auto control_unit = controller::InternalControllerFactory::CreateControlUnit(
                        control_strategy.get(), lane_location_provider_.get()))
                {
                    composite_controller->AddControlUnit(std::move(control_unit));
                }
            }
        }
    }
}

bool AstasEnvironment::HasControlStrategyGoalBeenReached(std::uint64_t entity_id,
                                                         mantle_api::ControlStrategyType type) const
{
    auto composite_controllers = active_controller_repository_.GetControllersByEntityId(entity_id);

    if (composite_controllers.empty())
    {
        throw EnvironmentException(
            "Cannot get active controller for entity with id {}. Please check if "
            "controller was created and assigned an entity.",
            entity_id);
    }

    for (const auto& composite_controller : composite_controllers)
    {
        if (!composite_controller->IsActive())
        {
            continue;
        }

        if (composite_controller->GetType() == controller::CompositeController::Type::kExternal)
        {
            // We cannot ask the VehicleModel/TrafficParticipantControlUnit, so assuming false
            return false;
        }

        return composite_controller->HasControlStrategyGoalBeenReached(entity_id, type);
    }

    // Return false when no active controller available
    return false;
}

void AstasEnvironment::Step()
{
    StepEntities();

    sensor_view_builder_->Step(entity_repository_.GetEntities(), &entity_repository_.GetHost());
    traffic_command_builder_->Step();
}

void AstasEnvironment::StepEntities()
{
    active_controller_repository_.Step(service::utility::Clock::Instance().Now());

    for (const auto& id : active_controller_repository_.GetCurrentEntityIdsWithoutControllers())
    {
        if (entity_repository_.GetHost().GetUniqueId() != id)
        {
            entity_repository_.Delete(id);
        }
    }
}

const mantle_api::ILaneLocationQueryService& AstasEnvironment::GetQueryService() const
{
    return *lane_location_provider_;
}

const mantle_api::ICoordConverter* AstasEnvironment::GetConverter() const
{
    return coordinate_converter_.get();
}

const mantle_api::IGeometryHelper* AstasEnvironment::GetGeometryHelper() const
{
    return &geometry_helper_;
}

mantle_api::IEntityRepository& AstasEnvironment::GetEntityRepository()
{
    return entity_repository_;
}

const mantle_api::IEntityRepository& AstasEnvironment::GetEntityRepository() const
{
    return entity_repository_;
}

mantle_api::IControllerRepository& AstasEnvironment::GetControllerRepository()
{
    return controller_prototypes_;
}

const mantle_api::IControllerRepository& AstasEnvironment::GetControllerRepository() const
{
    return controller_prototypes_;
};

void AstasEnvironment::SetWeather(mantle_api::Weather weather)
{
    sensor_view_builder_->SetWeather(weather);
}

void AstasEnvironment::SetDateTime(mantle_api::Time date_time)
{
    sensor_view_builder_->SetDateTime(date_time);
}

void AstasEnvironment::SetRoadCondition(std::vector<mantle_api::FrictionPatch> friction_patches)
{
    friction_patches_ = friction_patches;
}

void AstasEnvironment::SetTrafficSignalState(const std::string& traffic_signal_name,
                                             const std::string& traffic_signal_state)
{
    auto entity = entity_repository_.Get(traffic_signal_name).value();
    auto& traffic_light_entity = dynamic_cast<entities::TrafficLightEntity&>(entity.get());
    traffic_light_entity.ChangeTrafficSignalState(traffic_signal_state);
}

void AstasEnvironment::ExecuteCustomCommand(const std::vector<std::string>& actors,
                                            const std::string& type,
                                            const std::string& command)
{
    // No custom commands defined yet
    std::ignore = actors;
    std::ignore = type;
    std::ignore = command;

    Warn("Custom command of type '{}' will be ignored, as it is not yet supported", type);
}

void AstasEnvironment::SetUserDefinedValue(const std::string& name, const std::string& value)
{
    // Value is set in thread from async server
    std::scoped_lock lock(user_defined_value_mutex);
    user_defined_values_[name] = value;
}

std::optional<std::string> AstasEnvironment::GetUserDefinedValue(const std::string& name)
{
    std::scoped_lock lock(user_defined_value_mutex);
    if (user_defined_values_.find(name) != user_defined_values_.end())
    {
        return user_defined_values_[name];
    }
    return {};
}

host::HostVehicleModel& AstasEnvironment::GetHostVehicleModel()
{
    return *host_vehicle_model_;
}

const host::HostVehicleInterface& AstasEnvironment::GetHostVehicleInterface() const
{
    return *host_vehicle_interface_;
}

chunking::StaticChunkList AstasEnvironment::GetStaticChunks() const
{
    return sensor_view_builder_->GetStaticChunks();
}

const proto_groundtruth::SensorViewBuilder& AstasEnvironment::GetSensorViewBuilder() const
{
    return *sensor_view_builder_;
}

const astas_osi3::SensorView& AstasEnvironment::GetSensorView() const
{
    return sensor_view_builder_->GetSensorView();
}

const std::vector<astas_osi3::TrafficCommand>& AstasEnvironment::GetTrafficCommands() const
{
    return traffic_command_builder_->GetTrafficCommands();
}

mantle_api::Time AstasEnvironment::GetDateTime()
{
    return sensor_view_builder_->GetDateTime();
}

mantle_api::Time AstasEnvironment::GetSimulationTime()
{
    return service::utility::Clock::Instance().Now();
}

const map::AstasMap& AstasEnvironment::GetAstasMap() const
{
    return *astas_map_;
}

void AstasEnvironment::SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior default_routing_behavior)
{
    controller_prototypes_.SetDefaultRoutingBehavior(default_routing_behavior);
}

void AstasEnvironment::AssignRoute(mantle_api::UniqueId entity_id, mantle_api::RouteDefinition route_definition)
{
    auto composite_controllers = active_controller_repository_.GetControllersByEntityId(entity_id);

    if (composite_controllers.empty())
    {
        throw EnvironmentException(
            "Cannot get active controller for entity with id {}. Please check if "
            "controller was created and assigned an entity.",
            entity_id);
    }

    for (auto& composite_controller : composite_controllers)
    {
        if (!composite_controller->IsActive())
        {
            continue;
        }

        if (composite_controller->GetType() == controller::CompositeController::Type::kExternal)
        {
            for (auto& control_unit : composite_controller->GetControlUnits())
            {
                if (dynamic_cast<controller::HostVehicleInterfaceControlUnit*>(control_unit.get()) != nullptr)
                {
                    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints;
                    waypoints.reserve(route_definition.waypoints.size());
                    for (auto& route_waypoint : route_definition.waypoints)
                    {
                        waypoints.push_back(route_waypoint.waypoint);
                    }
                    composite_controller->AddControlUnit(std::make_unique<controller::HostVehicleInterfaceControlUnit>(
                        waypoints, lane_location_provider_.get(), traffic_command_builder_.get()));
                    break;
                }
            }
        }
        else
        {
            composite_controller->AddControlUnit(controller::InternalControllerFactory::CreateControlUnit(
                route_definition, lane_location_provider_.get()));
        }
    }
}

mantle_api::ITrafficSwarmService& AstasEnvironment::GetTrafficSwarmService()
{
    if (traffic_swarm_)
    {
        return *traffic_swarm_;
    }
    throw std::runtime_error("AstasEnvironment::GetTrafficSwarmService : traffic swarm has not been initialized");
}

void AstasEnvironment::InitTrafficSwarmService(const mantle_api::TrafficSwarmParameters& parameters)
{
    if (lane_location_provider_ == nullptr)
    {
        throw EnvironmentException("InitTrafficSwarmService : lane_location_provider_ is unassigned");
    }

    traffic_swarm_ = std::make_unique<traffic_swarm::TrafficSwarmXosc>(
        *lane_location_provider_, entity_repository_, parameters, longitudinal_distance_between_traffic_swarm_vehicles);
}

void AstasEnvironment::SetMapEngine(std::unique_ptr<IMapEngine> map_engine)
{
    map_engine_ = std::move(map_engine);
}

}  // namespace astas::environment::api
