/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/entity_repository.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <gtest/gtest.h>

namespace astas::environment::api
{

TEST(EntityRepositoryTest, GivenNoEntitiesInRepo_WhenGet_ThenTrow)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    EXPECT_THROW(repo.Get(0), EnvironmentException);
}

TEST(EntityRepositoryTest, GivenNoEntitiesInRepo_WhenCreateEntitiesForeachType_ThenEntiesAreReturned)
{
    std::string expected_vehicle_name{"test-vehicle"};
    std::string expected_pedestrian_name{"test-pedestrian"};
    std::string expected_static_object_name{"test-static-object"};
    std::string expected_traffic_light_name{"test-traffic_light"};
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};

    auto& entity = repo.Create(expected_vehicle_name, mantle_api::VehicleProperties{});
    auto& pedestrian = repo.Create(expected_pedestrian_name, mantle_api::PedestrianProperties{});
    auto& static_object = repo.Create(expected_static_object_name, mantle_api::StaticObjectProperties());
    auto& traffic_light = repo.Create(expected_traffic_light_name, mantle_ext::TrafficLightProperties());

    EXPECT_EQ(expected_vehicle_name, entity.GetName());
    EXPECT_EQ(expected_pedestrian_name, pedestrian.GetName());
    EXPECT_EQ(expected_static_object_name, static_object.GetName());
    EXPECT_EQ(expected_traffic_light_name, traffic_light.GetName());
}

TEST(EntityRepositoryTest, GivenEntityNamesWithIntegers_WhenCreateEntitiesOfAllTypes_ThenEntityIdsAreSetFromNames)
{
    std::string expected_vehicle_name{"1"};
    std::string expected_pedestrian_name{"2"};
    std::string expected_static_object_name{"3"};
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};

    auto& entity = repo.Create(expected_vehicle_name, mantle_api::VehicleProperties{});
    auto& pedestrian = repo.Create(expected_pedestrian_name, mantle_api::PedestrianProperties{});
    auto& static_object = repo.Create(expected_static_object_name, mantle_api::StaticObjectProperties());

    EXPECT_EQ(std::stoull(expected_vehicle_name), entity.GetUniqueId());
    EXPECT_EQ(std::stoull(expected_pedestrian_name), pedestrian.GetUniqueId());
    EXPECT_EQ(std::stoull(expected_static_object_name), static_object.GetUniqueId());
}

TEST(EntityRepositoryTest, GivenEntityNameNoInteger_WhenCreateEntity_ThenIdNotInReservedRange)
{
    std::string vehicle_name{"NoInteger"};
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};

    auto& entity = repo.Create(vehicle_name, mantle_api::VehicleProperties{});
    EXPECT_NE(0, entity.GetUniqueId());
    EXPECT_GT(entity.GetUniqueId(), 1000);
}

TEST(EntityRepositoryTest, GivenEntityNameWithIntegerValueAbove1000_WhenCreateEntity_ThenThrow)
{
    std::string vehicle_name{"1001"};
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};

    EXPECT_ANY_THROW(repo.Create(vehicle_name, mantle_api::VehicleProperties{}));
}

TEST(EntityRepositoryTest, GivenCreatedEntity_WhenGetById_ThenEntityReturned)
{
    mantle_api::UniqueId expected_id{42};
    std::string expected_name{"test-name"};
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(expected_id, expected_name, mantle_api::VehicleProperties{});

    auto entity = repo.Get(expected_id);

    EXPECT_EQ(expected_id, entity->get().GetUniqueId());
    EXPECT_EQ(expected_name, entity->get().GetName());
}

TEST(EntityRepositoryTest, GivenCreatedEntityAsHost_WhenGetByIdZero_ThenHostIsReturned)
{
    mantle_api::UniqueId expected_id{0};
    std::string expected_name{"test-host-name"};
    mantle_api::VehicleProperties host_properties{};
    host_properties.is_host = true;

    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(expected_name, host_properties);

    auto entity = repo.Get(expected_id);

    EXPECT_EQ(expected_id, entity->get().GetUniqueId());
    EXPECT_EQ(expected_name, entity->get().GetName());
}

TEST(EntityRepositoryTest, GivenHostEntityAlreadyCreated_WhenEntityCreatedAgain_ThenThrow)
{
    std::string host_name{"test-host-name"};
    mantle_api::VehicleProperties host_properties{};
    host_properties.is_host = true;

    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    EXPECT_NO_THROW(repo.Create(host_name, host_properties));

    EXPECT_THROW(repo.Create(host_name, host_properties), EnvironmentException);
}

TEST(EntityRepositoryTest, GivenTwoEntities_WhenGetById_ThenCorrectEntityReturned)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(1, "first", mantle_api::VehicleProperties{});
    repo.Create(2, "second", mantle_api::VehicleProperties{});

    EXPECT_EQ(1, repo.Get(1)->get().GetUniqueId());
    EXPECT_EQ(2, repo.Get(2)->get().GetUniqueId());
}

TEST(EntityRepositoryTest, GivenTwoEntities_WhenGetByName_ThenCorrectEntityReturned)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(1, "first", mantle_api::VehicleProperties{});
    repo.Create(2, "second", mantle_api::VehicleProperties{});

    EXPECT_EQ("first", repo.Get("first")->get().GetName());
    EXPECT_EQ("second", repo.Get("second")->get().GetName());
}

TEST(EntityRepositoryTest, GivenEntityExists_WhenCreatingAgainSameId_ThenExceptionThrow)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};

    EXPECT_NO_THROW(repo.Create(42, "car", mantle_api::VehicleProperties{}));
    EXPECT_THROW(repo.Create(42, "bus", mantle_api::VehicleProperties{}), EnvironmentException);
}

TEST(EntityRepositoryTest, GivenEntityExists_WhenCreatingAgainSameName_ThenExceptionThrow)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};

    EXPECT_NO_THROW(repo.Create(1, "car", mantle_api::VehicleProperties{}));
    EXPECT_THROW(repo.Create(2, "car", mantle_api::VehicleProperties{}), EnvironmentException);
}

TEST(EntityRepositoryTest, GivenEmptyRepo_WhenGettingHost_ThenExceptionThrow)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};

    EXPECT_THROW(repo.GetHost(), EnvironmentException);
}

TEST(EntityRepositoryTest, GivenRepoWithHost_WhenGettingHost_ThenHostIsReturned)
{
    mantle_api::VehicleProperties host_properties;
    host_properties.is_host = true;

    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    EXPECT_NO_THROW(repo.Create(0, "host", host_properties));
    EXPECT_NO_THROW(repo.Create(42, "another-car", mantle_api::VehicleProperties{}));

    const auto& host = repo.GetHost();
    EXPECT_EQ(0, host.GetUniqueId());
    EXPECT_EQ("host", host.GetName());
}

TEST(EntityRepositoryTest, GivenRepoWithEntities_WhenGettingEntities_ThenAllEntitiesAreReturned)
{
    mantle_api::VehicleProperties host_properties;
    host_properties.is_host = true;

    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    EXPECT_NO_THROW(repo.Create(0, "host", host_properties));
    EXPECT_NO_THROW(repo.Create(1, "car", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(repo.Create(2, "bus", mantle_api::VehicleProperties{}));
    EXPECT_NO_THROW(repo.Create(3, "pedestrian", mantle_api::PedestrianProperties{}));
    EXPECT_NO_THROW(repo.Create(4, "pylon", mantle_api::StaticObjectProperties()));

    const auto& entities = repo.GetEntities();
    EXPECT_EQ(5, entities.size());
    EXPECT_EQ("host", entities.at(0)->GetName());
    EXPECT_EQ("car", entities.at(1)->GetName());
    EXPECT_EQ("bus", entities.at(2)->GetName());
    EXPECT_EQ("pedestrian", entities.at(3)->GetName());
    EXPECT_EQ("pylon", entities.at(4)->GetName());
}

TEST(EntityRepositoryTest, GivenRepo_WhenDeletingEntity_ThenEntityIsRemoved)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(0, "host", mantle_api::VehicleProperties{});
    repo.Create(1, "car", mantle_api::VehicleProperties{});

    repo.Delete(mantle_api::UniqueId{1});

    EXPECT_EQ(1, repo.GetEntities().size());
}

TEST(EntityRepositoryTest, GivenRepo_WhenDeletingEntityWithWrongId_ThenExceptionThrow)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(0, "host", mantle_api::VehicleProperties{});

    EXPECT_THROW(repo.Delete(mantle_api::UniqueId{1}), EnvironmentException);
}

TEST(EntityRepositoryTest, GivenRepo_WhenContainsEntityWithId_ThenTrue)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(1, "car", mantle_api::VehicleProperties{});

    EXPECT_TRUE(repo.Contains(mantle_api::UniqueId{1}));
}

TEST(EntityRepositoryTest, GivenRepo_WhenNotContainsEntityWithId_ThenFalse)
{
    service::utility::UniqueIdProvider id_provider{};
    EntityRepository repo{&id_provider};
    repo.Create(1, "car", mantle_api::VehicleProperties{});

    EXPECT_FALSE(repo.Contains(mantle_api::UniqueId{2}));
}

}  // namespace astas::environment::api
