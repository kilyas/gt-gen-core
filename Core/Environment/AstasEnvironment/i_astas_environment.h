/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_IASTASENVIRONMENT_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_IASTASENVIRONMENT_H

#include "Core/Environment/Chunking/chunking.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/Host/host_vehicle_interface.h"
#include "Core/Environment/Host/host_vehicle_model.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"

#include <MantleAPI/Execution/i_environment.h>
#include <astas_osi_trafficcommand.pb.h>

namespace astas::environment::api
{

class IAstasEnvironment : public mantle_api::IEnvironment
{
  public:
    virtual void Init() = 0;
    virtual void Step() = 0;
    virtual environment::host::HostVehicleModel& GetHostVehicleModel() = 0;
    virtual const environment::map::AstasMap& GetAstasMap() const = 0;
    virtual const environment::proto_groundtruth::SensorViewBuilder& GetSensorViewBuilder() const = 0;
    virtual environment::chunking::StaticChunkList GetStaticChunks() const = 0;
    virtual const astas_osi3::SensorView& GetSensorView() const = 0;
    virtual const environment::host::HostVehicleInterface& GetHostVehicleInterface() const = 0;
    virtual const std::vector<astas_osi3::TrafficCommand>& GetTrafficCommands() const = 0;
};

}  // namespace astas::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_IASTASENVIRONMENT_H
