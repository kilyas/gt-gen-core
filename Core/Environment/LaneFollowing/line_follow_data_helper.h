/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_LINEFOLLOWDATAHELPER_H
#define GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_LINEFOLLOWDATAHELPER_H

#include "Core/Environment/LaneFollowing/point_distance_list.h"
#include "Core/Environment/PathFinding/path.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>

#include <vector>

namespace astas::environment::lanefollowing
{

using CenterLine = std::vector<mantle_api::Vec3<units::length::meter_t>>;
using LaneIdToCenterLine = std::pair<mantle_api::UniqueId, CenterLine>;
using LaneData = std::vector<LaneIdToCenterLine>;

void RemovePointsBeforeIndex(std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line, std::size_t index);
void RemovePointsAfterIndex(std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line, std::size_t index);

LaneFollowData CreateLaneFollowData(LaneData& lane_data,
                                    const mantle_api::Vec3<units::length::meter_t>& projected_start);

LaneFollowData CreateLaneFollowData(LaneData& lane_data,
                                    const mantle_api::Vec3<units::length::meter_t>& projected_start,
                                    const mantle_api::Vec3<units::length::meter_t>& projected_end);

void AddPointAndDistance(LaneFollowData& lane_follow_data,
                         const mantle_api::Vec3<units::length::meter_t>& point,
                         mantle_api::UniqueId lane_id);

void AddDistanceToFirstPointIfPathIsClosed(LaneFollowData& lane_follow_data, const path_finding::Path& path);

}  // namespace astas::environment::lanefollowing

#endif  // GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_LINEFOLLOWDATAHELPER_H
