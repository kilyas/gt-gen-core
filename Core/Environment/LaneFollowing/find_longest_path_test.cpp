/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/LaneFollowing/find_longest_path.h"

#include "Core/Environment/Map/AstasMap/lane.h"

#include <gtest/gtest.h>

namespace astas::environment::lanefollowing
{

map::AstasMap GetMapWithLaneGroupZero()
{
    map::AstasMap astas_map;
    map::LaneGroup lane_group(0, map::LaneGroup::Type::kOther);
    astas_map.AddLaneGroup(lane_group);
    return astas_map;
}

map::Lane GetLaneWithIdAndSuccessors(mantle_api::UniqueId id, const std::vector<mantle_api::UniqueId>& successor_ids)
{
    map::Lane lane{id};
    lane.successors = successor_ids;
    return lane;
}

map::Lane GetLaneWithIdAndPredecessors(mantle_api::UniqueId id,
                                       const std::vector<mantle_api::UniqueId>& predecessor_ids)
{
    map::Lane lane{id};
    lane.predecessors = predecessor_ids;
    return lane;
}

// Lane1 -> Lane2 -> Lane3
// Lane4 -> Lane5 ---^
map::AstasMap GetMergeLaneWithSuccessorsMap()
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane1 = GetLaneWithIdAndSuccessors(1, {2});
    auto lane2 = GetLaneWithIdAndSuccessors(2, {3});
    auto lane3 = GetLaneWithIdAndSuccessors(3, {});

    auto lane4 = GetLaneWithIdAndSuccessors(4, {5});
    auto lane5 = GetLaneWithIdAndSuccessors(5, {3});  // merge lane
    astas_map.AddLane(0, lane1);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);
    astas_map.AddLane(0, lane4);
    astas_map.AddLane(0, lane5);

    return astas_map;
}

// Lane3 <-Lane1 <- Lane2
//   ^---- Lane4 <- Lane5
map::AstasMap GetSplitLaneWithPredecessorsMap()
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane2 = GetLaneWithIdAndPredecessors(2, {1});
    auto lane5 = GetLaneWithIdAndPredecessors(5, {4});

    auto lane1 = GetLaneWithIdAndPredecessors(1, {3});
    auto lane4 = GetLaneWithIdAndPredecessors(4, {3});

    auto lane3 = GetLaneWithIdAndPredecessors(3, {});

    astas_map.AddLane(0, lane1);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);
    astas_map.AddLane(0, lane4);
    astas_map.AddLane(0, lane5);

    return astas_map;
}

TEST(FindLongestPathTest, GivenOneLane_WhenFindLongestPath_ThenOneLaneContained)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane1 = GetLaneWithIdAndSuccessors(1, {});
    astas_map.AddLane(0, lane1);

    auto lanes = FindLongestPath(astas_map, astas_map.FindLane(lane1.id));

    EXPECT_EQ(1, lanes.size());
}

TEST(FindLongestBackwardsPathTest, GivenOneLane_WhenFindLongestBackwardsPath_ThenOneLaneContained)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane1 = GetLaneWithIdAndPredecessors(1, {});
    astas_map.AddLane(0, lane1);

    auto lanes = FindLongestBackwardsPath(astas_map, astas_map.FindLane(lane1.id));

    EXPECT_EQ(1, lanes.size());
}

TEST(FindLongestBackwardsPathTest,
     GivenThreeStraightLanes_WhenFindLongestBackwardsPathFromLastLane_ThenThreeLanesContained)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane3 = GetLaneWithIdAndPredecessors(3, {});
    auto lane2 = GetLaneWithIdAndPredecessors(2, {3});
    auto lane1 = GetLaneWithIdAndPredecessors(1, {2});

    astas_map.AddLane(0, lane1);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);

    auto lanes = FindLongestBackwardsPath(astas_map, astas_map.FindLane(lane1.id));

    EXPECT_EQ(3, lanes.size());
}

TEST(FindLongestPathTest, GivenThreeStraightLanes_WhenFindLongestPath_ThenThreeLanesContained)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane1 = GetLaneWithIdAndSuccessors(1, {2});
    auto lane2 = GetLaneWithIdAndSuccessors(2, {3});
    auto lane3 = GetLaneWithIdAndSuccessors(3, {});
    astas_map.AddLane(0, lane1);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);

    auto lanes = FindLongestPath(astas_map, astas_map.FindLane(lane1.id));

    EXPECT_EQ(3, lanes.size());
}

TEST(FindLongestPathTest, GivenMergeLaneWithSuccessorsMap_WhenFindLongestPath_ThenThreeLanesContained)
{
    auto astas_map = GetMergeLaneWithSuccessorsMap();

    auto lanes = FindLongestPath(astas_map, astas_map.FindLane(1));

    EXPECT_EQ(3, lanes.size());
    EXPECT_EQ(1, lanes.at(0)->id);
    EXPECT_EQ(2, lanes.at(1)->id);
    EXPECT_EQ(3, lanes.at(2)->id);
}

TEST(FindLongestBackwardsPathTest,
     GivenSplitLaneWithPredecessorsMap_WhenFindLongestBackwardsPathFromLastLane_ThenThreeLanesContained)
{
    auto astas_map = GetSplitLaneWithPredecessorsMap();

    auto lanes = FindLongestBackwardsPath(astas_map, astas_map.FindLane(5));

    ASSERT_EQ(3, lanes.size());
    EXPECT_EQ(5, lanes.at(0)->id);
    EXPECT_EQ(4, lanes.at(1)->id);
    EXPECT_EQ(3, lanes.at(2)->id);
}

// Lane1 -> Lane2
//      \-> Lane3
TEST(FindLongestPathTest, GivenSplitLanesWithoutSuccessor_WhenFindLongestPath_ThenTwoLanesContained)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto split_lane = GetLaneWithIdAndSuccessors(1, {2, 3});
    auto lane2 = GetLaneWithIdAndSuccessors(2, {});
    auto lane3 = GetLaneWithIdAndSuccessors(3, {});

    astas_map.AddLane(0, split_lane);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);

    auto lanes = FindLongestPath(astas_map, astas_map.FindLane(split_lane.id));

    ASSERT_EQ(2, lanes.size());
    EXPECT_EQ(1, lanes.at(0)->id);
    EXPECT_EQ(3, lanes.at(1)->id);
}

// Lane2 <-Lane1
// Lane3 <--/
TEST(FindLongestBackwardsPathTest, GivenMergeLanesWithPredecessors_WhenFindLongestBackwardsPath_ThenTwoLanesContained)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane1 = GetLaneWithIdAndPredecessors(1, {2, 3});
    auto lane2 = GetLaneWithIdAndPredecessors(2, {});
    auto lane3 = GetLaneWithIdAndPredecessors(3, {});

    astas_map.AddLane(0, lane1);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);

    auto lanes = FindLongestBackwardsPath(astas_map, astas_map.FindLane(lane1.id));

    ASSERT_EQ(2, lanes.size());
    EXPECT_EQ(1, lanes.at(0)->id);
    EXPECT_EQ(3, lanes.at(1)->id);
}

// Lane1 -> Lane2 -> Lane3
//      \-> Lane4
TEST(FindLongestPathTest, GivenHighwaylikeExitWithoutSuccessor_WhenFindLongestPath_ThenContinuingLanesAreTaken)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto split_lane = GetLaneWithIdAndSuccessors(1, {2, 4});
    auto lane2 = GetLaneWithIdAndSuccessors(2, {3});
    auto lane3 = GetLaneWithIdAndSuccessors(3, {});
    auto lane4 = GetLaneWithIdAndSuccessors(4, {});

    astas_map.AddLane(0, split_lane);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);
    astas_map.AddLane(0, lane4);

    auto lanes = FindLongestPath(astas_map, astas_map.FindLane(split_lane.id));

    ASSERT_EQ(3, lanes.size());
    EXPECT_EQ(1, lanes.at(0)->id);
    EXPECT_EQ(2, lanes.at(1)->id);
    EXPECT_EQ(3, lanes.at(2)->id);
}

// Lane1 <- Lane2 <- Lane3
//   \---<- Lane4
TEST(FindLongestBackwardsPathTest,
     GivenSplitWithAnExitAndWithPredecessors_WhenFindLongestBackwardsPath_ThenContinuingLanesAreTaken)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane3 = GetLaneWithIdAndPredecessors(3, {2, 4});
    auto lane2 = GetLaneWithIdAndPredecessors(2, {1});
    auto lane4 = GetLaneWithIdAndPredecessors(4, {1});

    auto lane1 = GetLaneWithIdAndPredecessors(1, {});

    astas_map.AddLane(0, lane1);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);
    astas_map.AddLane(0, lane4);

    auto lanes = FindLongestBackwardsPath(astas_map, astas_map.FindLane(lane3.id));

    ASSERT_EQ(3, lanes.size());
    EXPECT_EQ(3, lanes.at(0)->id);
    EXPECT_EQ(2, lanes.at(1)->id);
    EXPECT_EQ(1, lanes.at(2)->id);
}

// Lane1-> Lane2 ->Lane1
TEST(FindLongestPathTest, GivenCyclicLanesWithSuccessors_WhenFindLongestPath_ThenFindLanesTerminates)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto right_half_cycle = GetLaneWithIdAndSuccessors(1, {2});
    auto left_half_cycle = GetLaneWithIdAndSuccessors(2, {1});

    astas_map.AddLane(0, right_half_cycle);
    astas_map.AddLane(0, left_half_cycle);

    auto lanes = FindLongestPath(astas_map, astas_map.FindLane(right_half_cycle.id));

    ASSERT_EQ(2, lanes.size());
    EXPECT_EQ(1, lanes.at(0)->id);
    EXPECT_EQ(2, lanes.at(1)->id);
}

// Lane1 <-Lane2 <-Lane1
TEST(FindLongestBackwardsPathTest,
     GivenCyclicLanesWithPredecessors_WhenFindLongestBackwardsPath_ThenFindLanesTerminates)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto right_half_cycle = GetLaneWithIdAndPredecessors(1, {2});
    auto left_half_cycle = GetLaneWithIdAndPredecessors(2, {1});

    astas_map.AddLane(0, right_half_cycle);
    astas_map.AddLane(0, left_half_cycle);

    auto lanes = FindLongestBackwardsPath(astas_map, astas_map.FindLane(right_half_cycle.id));

    ASSERT_EQ(2, lanes.size());
    EXPECT_EQ(1, lanes.at(0)->id);
    EXPECT_EQ(2, lanes.at(1)->id);
}

// Lane 1 and 5 overlap. Both lanes are potential start lanes
// Lane1-> Lane2 ->Lane3 -> Lane4
// Lane5 -> Lane6 -> Lane7
TEST(FindLongestPathTest, GivenTwoPossibleStartLanes_WhenFindLongestPath_ThenTheLongestPathIsSelected)
{
    auto astas_map = GetMapWithLaneGroupZero();
    auto lane1 = GetLaneWithIdAndSuccessors(1, {2});
    auto lane2 = GetLaneWithIdAndSuccessors(2, {3});
    auto lane3 = GetLaneWithIdAndSuccessors(3, {4});
    auto lane4 = GetLaneWithIdAndSuccessors(4, {});
    auto lane5 = GetLaneWithIdAndSuccessors(5, {6});
    auto lane6 = GetLaneWithIdAndSuccessors(6, {7});
    auto lane7 = GetLaneWithIdAndSuccessors(7, {});

    astas_map.AddLane(0, lane1);
    astas_map.AddLane(0, lane2);
    astas_map.AddLane(0, lane3);
    astas_map.AddLane(0, lane4);
    astas_map.AddLane(0, lane5);
    astas_map.AddLane(0, lane6);
    astas_map.AddLane(0, lane7);

    auto lanes = FindLongestPath(astas_map, {astas_map.FindLane(lane1.id), astas_map.FindLane(lane5.id)});

    ASSERT_EQ(4, lanes.size());
    EXPECT_EQ(1, lanes.at(0)->id);
    EXPECT_EQ(2, lanes.at(1)->id);
    EXPECT_EQ(3, lanes.at(2)->id);
    EXPECT_EQ(4, lanes.at(3)->id);
}

}  // namespace astas::environment::lanefollowing
