/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/LaneFollowing/find_longest_path.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <gtest/gtest.h>

namespace astas::environment::lanefollowing
{

TEST(FindLongestPathComponentTest, GivenOneStraightLane_WhenFindLongestPath_ThenOneLaneInLaneList)
{
    mantle_api::UniqueId start_lane_id{6};
    auto astas_map = test_utils::MapCatalogue::MapStraightRoad2km();

    auto lanes = FindLongestPath(*astas_map, astas_map->FindLane(start_lane_id));
    ASSERT_EQ(1, lanes.size());
    EXPECT_EQ(start_lane_id, lanes.at(0)->id);
}

TEST(FindLongestPathComponentTest, GivenMapWithNonCyclicLanes_WhenFindLongestPath_ThenRoadIsFollowedToTheEnd)
{
    mantle_api::UniqueId start_lane_id{3};

    auto astas_map = test_utils::MapCatalogue::MapWithFiveConnectedEastingLanesWithHundredPointsEach();

    auto lanes = FindLongestPath(*astas_map, astas_map->FindLane(start_lane_id));

    ASSERT_EQ(5, lanes.size());
    EXPECT_EQ(start_lane_id, lanes.at(0)->id);
    EXPECT_EQ(7, lanes.at(1)->id);
    EXPECT_EQ(11, lanes.at(2)->id);
    EXPECT_EQ(15, lanes.at(3)->id);
    EXPECT_EQ(19, lanes.at(4)->id);
}

// See CyclicLanes defined in test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups
// Cyclic Lanes id: 6->16->26->36->6
TEST(FindLongestPathComponentTest,
     GivenMapWithCyclicLanes_WhenFindLongestPath_ThenTerminateWhenProcessingPreviouslySeenLane)
{
    mantle_api::UniqueId start_lane_id{6};

    auto astas_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();

    auto lanes = FindLongestPath(*astas_map, astas_map->FindLane(start_lane_id));

    ASSERT_EQ(4, lanes.size());
    EXPECT_EQ(start_lane_id, lanes.at(0)->id);
    EXPECT_EQ(16, lanes.at(1)->id);
    EXPECT_EQ(26, lanes.at(2)->id);
    EXPECT_EQ(36, lanes.at(3)->id);
}

}  // namespace astas::environment::lanefollowing
