/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_POINTDISTANCELIST_H
#define GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_POINTDISTANCELIST_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>

#include <vector>

namespace astas::environment::lanefollowing
{
struct PointDistanceListEntry
{
    mantle_api::Vec3<units::length::meter_t> point{};
    units::length::meter_t distance_to_next_point{0.0};
    mantle_api::UniqueId lane_id{0};
    mantle_api::Vec3<units::length::meter_t> heading{};
};

using PointDistanceList = std::vector<PointDistanceListEntry>;
using LaneFollowData = std::vector<PointDistanceListEntry>;

}  // namespace astas::environment::lanefollowing

#endif  // GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_POINTDISTANCELIST_H
