/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/LaneFollowing/line_follow_data_from_lanes.h"

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::environment::lanefollowing
{
using units::literals::operator""_m;

map::LaneLocation GetLaneLocation(map::Lane* lane, std::size_t index)
{
    map::LaneLocation location;
    location.centerline_point_index = index;
    location.lanes.push_back(lane);
    location.projected_centerline_point = lane->center_line.at(index);
    return location;
}

map::LaneLocation GetLaneLocation(map::Lane* lane,
                                  std::size_t index,
                                  mantle_api::Vec3<units::length::meter_t> projected_point)
{
    auto location = GetLaneLocation(lane, index);
    location.projected_centerline_point = projected_point;
    return location;
}

TEST(LineFollowDataFromLaneTest,
     GivenOneLaneAndStartPositionAtFirstCenterPointWithForwardDirectoin_WhenGetList_ThenListContainsFullCenterLine)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    lane1.center_line = {clp1, clp2, clp3};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);

    auto list = CreateLineFollowDataFromLanes({&lane1}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(3, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(2).point)
    EXPECT_EQ(0.0_m, list.at(2).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenOneLaneAndStartPositionAtThirdCenterPointWithBackwardsDirection_WhenGetList_ThenListContainsFullCenterLineReversed)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    lane1.center_line = {clp1, clp2, clp3};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 2);

    auto list = CreateLineFollowDataFromLanes({&lane1}, start_location, mantle_api::Direction::kBackwards);

    ASSERT_EQ(3, list.size());
    EXPECT_TRIPLE(clp3, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp1, list.at(2).point)
    EXPECT_EQ(0.0_m, list.at(2).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenOneLaneAndStartPositionAfterFirstCenterPointWithForwardDirection_WhenGetList_ThenListStartsWithProjectedStartPosition)
{
    map::Lane lane1{1};
    lane1.center_line = {{0_m, 0_m, 0_m}, {1_m, 0_m, 0_m}, {2_m, 0_m, 0_m}};
    mantle_api::Vec3<units::length::meter_t> projected_point{0.5_m, 0.0_m, 0.0_m};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0, projected_point);

    auto list = CreateLineFollowDataFromLanes({&lane1}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(3, list.size());
    EXPECT_TRIPLE(projected_point, list.at(0).point)
    EXPECT_EQ(0.5_m, list.at(0).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenOneLaneAndStartPositionAfterFirstCenterPointWithBackwardsDirection_WhenGetList_ThenListStartsWithProjectedStartPosition)
{
    map::Lane lane1{1};
    lane1.center_line = {{0_m, 0_m, 0_m}, {1_m, 0_m, 0_m}, {2_m, 0_m, 0_m}};
    mantle_api::Vec3<units::length::meter_t> projected_point{1.5_m, 0.0_m, 0.0_m};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 1, projected_point);

    auto list = CreateLineFollowDataFromLanes({&lane1}, start_location, mantle_api::Direction::kBackwards);

    ASSERT_EQ(3, list.size());

    EXPECT_TRIPLE(projected_point, list.at(0).point);
    EXPECT_EQ(0.5_m, list.at(0).distance_to_next_point);
}

TEST(LineFollowDataFromLaneTest,
     GivenOneLaneAndStartPositionAtSecondCenterPointWithForwardDirection_WhenGetList_ThenListContainsPartialCenterLine)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    lane1.center_line = {{0_m, 0_m, 0_m}, clp2, clp3};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 1);

    auto list = CreateLineFollowDataFromLanes({&lane1}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(2, list.size());

    EXPECT_TRIPLE(clp2, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(1).point)
    EXPECT_EQ(0.0_m, list.at(1).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenOneLaneAndStartPositionAtSecondCenterPointWithBackwardsDirection_WhenGetList_ThenListContainsReversedPartialCenterLine)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp1{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{2_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{3_m, 0_m, 0_m};
    lane1.center_line = {clp1, clp2, clp3};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 1);

    auto list = CreateLineFollowDataFromLanes({&lane1}, start_location, mantle_api::Direction::kBackwards);

    ASSERT_EQ(2, list.size());

    EXPECT_TRIPLE(clp2, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp1, list.at(1).point)
    EXPECT_EQ(0.0_m, list.at(1).distance_to_next_point);
}

TEST(LineFollowDataFromLaneTest,
     GivenTwoLanesAndStartAtIdxZeroWithForwardDirection_WhenGetList_ThenListContainsAllCenterLinePoints)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);

    auto list = CreateLineFollowDataFromLanes({&lane1, &lane2}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(3, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(2).point)
    EXPECT_EQ(0.0_m, list.at(2).distance_to_next_point);
}

TEST(LineFollowDataFromLaneTest,
     GivenTwoLanesAndStartAtIdxOneWithBackwardsDirection_WhenGetList_ThenListContainsAllCenterLinePointsReversed)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp0{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp1{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{2_m, 0_m, 0_m};
    lane1.center_line = {clp0, clp1};
    map::Lane lane2{2};
    lane2.center_line = {clp1, clp2};
    map::LaneLocation start_location = GetLaneLocation(&lane2, 1);

    auto list = CreateLineFollowDataFromLanes({&lane2, &lane1}, start_location, mantle_api::Direction::kBackwards);

    ASSERT_EQ(3, list.size());
    EXPECT_TRIPLE(clp2, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp1, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp0, list.at(2).point)
    EXPECT_EQ(0.0_m, list.at(2).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenTwoLanesAndStartAtLastCenterPointOfLaneOneWithForwardDirection_WhenGetList_ThenListStartsAtLastPointOfFirstLaneUntilEndOfRoad)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{3_m, 0_m, 0_m};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 1);

    auto list =
        CreateLineFollowDataFromLanes({&lane1, &lane2, &lane3}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(3, list.size());
    EXPECT_TRIPLE(clp2, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp4, list.at(2).point)
    EXPECT_EQ(0.0_m, list.at(2).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenTwoLanesAndStartAtLastCenterPointOfLaneOneWithBackwardsDirection_WhenGetList_ThenListStartsAtLastPointOfFirstLaneUntilBeginOfTheRoad)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{3_m, 0_m, 0_m};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::LaneLocation start_location = GetLaneLocation(&lane3, 0);

    auto list =
        CreateLineFollowDataFromLanes({&lane3, &lane2, &lane1}, start_location, mantle_api::Direction::kBackwards);

    ASSERT_EQ(3, list.size());

    EXPECT_TRIPLE(clp3, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp1, list.at(2).point)
    EXPECT_EQ(0.0_m, list.at(2).distance_to_next_point);
}

TEST(LineFollowDataFromLaneTest,
     GivenCyclicLanesStartingAtIndexZeroOfFirstLane_WhenGetList_ThenLastPointHasDistanceToFirstPoint)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{1_m, 1_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{0_m, 1_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::Lane lane4{4};
    lane4.center_line = {clp4, clp1};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);

    auto list = CreateLineFollowDataFromLanes(
        {&lane1, &lane2, &lane3, &lane4}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(4, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(2).point)
    EXPECT_EQ(1.0_m, list.at(2).distance_to_next_point);

    EXPECT_EQ(clp4, list.at(3).point);
    EXPECT_EQ(1.0_m, list.at(3).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenCyclicLanesStartingAtIndexZeroOfFirstLaneWithForwardDirection_WhenGetList_ThenLastPointHasDistanceToFirstPoint)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{1_m, 1_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{0_m, 1_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::Lane lane4{4};
    lane4.center_line = {clp4, clp1};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);

    auto list = CreateLineFollowDataFromLanes(
        {&lane1, &lane2, &lane3, &lane4}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(4, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(2).point)
    EXPECT_EQ(1.0_m, list.at(2).distance_to_next_point);

    EXPECT_EQ(clp4, list.at(3).point);
    EXPECT_EQ(1.0_m, list.at(3).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenCyclicLanesStartingAtIndexZeroOfLastLaneWithBackwardsDirection_WhenGetList_ThenLastPointHasDistanceToFirstPoint)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{1_m, 1_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{0_m, 1_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::Lane lane4{4};
    lane4.center_line = {clp4, clp1};
    map::LaneLocation start_location = GetLaneLocation(&lane4, 0);

    auto list = CreateLineFollowDataFromLanes(
        {&lane4, &lane3, &lane2, &lane1}, start_location, mantle_api::Direction::kBackwards);

    ASSERT_EQ(4, list.size());
    EXPECT_TRIPLE(clp4, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(2).point)
    EXPECT_EQ(1.0_m, list.at(2).distance_to_next_point);

    EXPECT_EQ(clp1, list.at(3).point);
    EXPECT_EQ(1.0_m, list.at(3).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenCyclicLanesStartingAtLastIndexOfFirstLaneWithForwardDirection_WhenGetList_ThenLastPointHasDistanceToFirstPoint)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{1_m, 1_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{0_m, 1_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::Lane lane4{4};
    lane4.center_line = {clp4, clp1};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 1);

    auto list = CreateLineFollowDataFromLanes(
        {&lane1, &lane2, &lane3, &lane4}, start_location, mantle_api::Direction::kForward);

    ASSERT_EQ(4, list.size());
    EXPECT_TRIPLE(clp2, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp4, list.at(2).point)
    EXPECT_EQ(1.0_m, list.at(2).distance_to_next_point);

    EXPECT_TRIPLE(clp1, list.at(3).point)
    EXPECT_EQ(1.0_m, list.at(3).distance_to_next_point);
}

TEST(
    LineFollowDataFromLaneTest,
    GivenCyclicLanesStartingAtLastIndexOfLastLaneWithBackwardsDirection_WhenGetList_ThenLastPointHasDistanceToFirstPoint)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{1_m, 1_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{0_m, 1_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::Lane lane4{4};
    lane4.center_line = {clp4, clp1};
    map::LaneLocation start_location = GetLaneLocation(&lane4, 0);

    auto list = CreateLineFollowDataFromLanes(
        {&lane4, &lane3, &lane2, &lane1}, start_location, mantle_api::Direction::kBackwards);

    ASSERT_EQ(4, list.size());
    EXPECT_TRIPLE(clp4, list.at(0).point)
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);

    EXPECT_TRIPLE(clp3, list.at(1).point)
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);

    EXPECT_TRIPLE(clp2, list.at(2).point)
    EXPECT_EQ(1.0_m, list.at(2).distance_to_next_point);

    EXPECT_TRIPLE(clp1, list.at(3).point)
    EXPECT_EQ(1.0_m, list.at(3).distance_to_next_point);
}

}  // namespace astas::environment::lanefollowing
