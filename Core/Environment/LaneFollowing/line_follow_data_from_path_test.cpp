/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/LaneFollowing/line_follow_data_from_path.h"

#include "Core/Environment/PathFinding/path_finder.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::environment::lanefollowing
{
using units::literals::operator""_m;

map::LaneLocation GetLaneLocation(map::Lane* lane, std::size_t index)
{
    map::LaneLocation location;
    location.centerline_point_index = index;
    location.lanes.push_back(lane);
    location.projected_centerline_point = lane->center_line.at(index);
    return location;
}

map::LaneLocation GetLaneLocation(map::Lane* lane,
                                  std::size_t index,
                                  mantle_api::Vec3<units::length::meter_t> projected_point)
{
    auto location = GetLaneLocation(lane, index);
    location.projected_centerline_point = projected_point;
    return location;
}

TEST(LineFollowDataFromPathTest, GivenPathOverOneLane_WhenConstructing_ThenListContainsAllCenterLinePoints)
{
    map::Lane lane1{1};
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    lane1.center_line = {clp1, clp2};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);
    map::LaneLocation end_location = GetLaneLocation(&lane1, 1);

    path_finding::Path path;
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane1, path_finding::EdgeType::kFollow));

    auto list = CreateLineFollowDataFromPath(path, start_location, end_location);

    ASSERT_EQ(2, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point);
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);
    EXPECT_EQ(1, list.at(0).lane_id);

    EXPECT_TRIPLE(clp2, list.at(1).point);
    EXPECT_EQ(0.0_m, list.at(1).distance_to_next_point);
    EXPECT_EQ(1, list.at(1).lane_id);
}

TEST(LineFollowDataFromPathTest, GivenPathOverTwoLanes_WhenConstructing_ThenListContainsAllCenterLinePoints)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);
    map::LaneLocation end_location = GetLaneLocation(&lane2, 1);

    path_finding::Path path;
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane1, path_finding::EdgeType::kFollow));
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane2, path_finding::EdgeType::kFollow));

    auto list = CreateLineFollowDataFromPath(path, start_location, end_location);

    ASSERT_EQ(3, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point);
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);
    EXPECT_EQ(1, list.at(0).lane_id);

    EXPECT_TRIPLE(clp2, list.at(1).point);
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);
    EXPECT_EQ(2, list.at(1).lane_id);

    EXPECT_EQ(clp3, list.at(2).point);
    EXPECT_EQ(0.0_m, list.at(2).distance_to_next_point);
    EXPECT_EQ(2, list.at(2).lane_id);
}

TEST(LineFollowDataFromPathTest,
     GivenPathFromEndOfLaneOneToBeginOfLaneThree_WhenConstructing_ThenListStartsAndEndsWithThosePoints)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{2_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{3_m, 0_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp3};
    map::Lane lane3{3};
    lane3.center_line = {clp3, clp4};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 1);
    map::LaneLocation end_location = GetLaneLocation(&lane3, 0);

    path_finding::Path path;
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane1, path_finding::EdgeType::kFollow));
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane2, path_finding::EdgeType::kFollow));
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane3, path_finding::EdgeType::kFollow));

    auto list = CreateLineFollowDataFromPath(path, start_location, end_location);

    ASSERT_EQ(2, list.size());
    EXPECT_TRIPLE(clp2, list.at(0).point);
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);
    EXPECT_EQ(2, list.at(0).lane_id);

    EXPECT_TRIPLE(clp3, list.at(1).point);
    EXPECT_EQ(0.0_m, list.at(1).distance_to_next_point);
    EXPECT_EQ(3, list.at(1).lane_id);
}

TEST(LineFollowDataFromPathTest, GivenClosedLoopPath_WhenConstructing_ThenLastListEntryHasDistanceToFirstPoint)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp2, clp1};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);
    map::LaneLocation end_location = GetLaneLocation(&lane2, 1);

    path_finding::Path path;
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane1, path_finding::EdgeType::kFollow));
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane2, path_finding::EdgeType::kFollow));
    path.EnableClosedPath();

    auto list = CreateLineFollowDataFromPath(path, start_location, end_location);

    ASSERT_EQ(2, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point);
    EXPECT_EQ(1.0_m, list.at(0).distance_to_next_point);
    EXPECT_EQ(1, list.at(0).lane_id);

    EXPECT_TRIPLE(clp2, list.at(1).point);
    EXPECT_EQ(1.0_m, list.at(1).distance_to_next_point);
    EXPECT_EQ(2, list.at(1).lane_id);
}

TEST(LineFollowDataFromPathTest, GivenStartAndEndOnSameLineSegment_WhenConstructing_ThenBothOriginalPointsRemoved)
{
    map::Lane lane1{1};
    lane1.center_line = {{0_m, 0_m, 0_m}, {1_m, 0_m, 0_m}};
    mantle_api::Vec3<units::length::meter_t> projected_point1{0.2_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> projected_point2{0.8_m, 0_m, 0_m};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0, projected_point1);
    map::LaneLocation end_location = GetLaneLocation(&lane1, 0, projected_point2);

    path_finding::Path path;
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane1, path_finding::EdgeType::kFollow));

    auto list = CreateLineFollowDataFromPath(path, start_location, end_location);

    ASSERT_EQ(2, list.size());
    EXPECT_TRIPLE(projected_point1, list.at(0).point);
    EXPECT_EQ(0.6_m, list.at(0).distance_to_next_point);
    EXPECT_EQ(1, list.at(0).lane_id);

    EXPECT_TRIPLE(projected_point2, list.at(1).point);
    EXPECT_EQ(0.0_m, list.at(1).distance_to_next_point);
    EXPECT_EQ(1, list.at(1).lane_id);
}

TEST(LineFollowDataFromPathTest,
     GivenStartAndEndProjectedOnSameCenterLinePoint_WhenConstructing_ThenOriginalPointsRemoved)
{
    map::Lane lane1{1};
    // center line point at -1 is needed as otherwise centerline in RemovePointsBeforeIndex is empty
    lane1.center_line = {{-1_m, 0_m, 0_m}, {0_m, 0_m, 0_m}, {1_m, 0_m, 0_m}};
    mantle_api::Vec3<units::length::meter_t> projected_point1{0.2_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> projected_point2{0.8_m, 0_m, 0_m};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0, projected_point1);
    map::LaneLocation end_location = GetLaneLocation(&lane1, 0, projected_point2);

    path_finding::Path path;
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane1, path_finding::EdgeType::kFollow));

    auto list = CreateLineFollowDataFromPath(path, start_location, end_location);

    ASSERT_EQ(2, list.size());
    EXPECT_TRIPLE(projected_point1, list.at(0).point);
    EXPECT_EQ(0.6_m, list.at(0).distance_to_next_point);
    EXPECT_EQ(1, list.at(0).lane_id);

    EXPECT_TRIPLE(projected_point2, list.at(1).point);
    EXPECT_EQ(0.0_m, list.at(1).distance_to_next_point);
    EXPECT_EQ(1, list.at(1).lane_id);
}

TEST(LineFollowDataFromPathTest,
     GivenStartAndEndOnAdjacentLanes_WhenConstructing_ThenPathContainsOneLaneWithEndPointOfAdjacentLane)
{
    mantle_api::Vec3<units::length::meter_t> clp1{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp2{1_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp3{0_m, 4_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> clp4{1_m, 4_m, 0_m};
    map::Lane lane1{1};
    lane1.center_line = {clp1, clp2};
    map::Lane lane2{2};
    lane2.center_line = {clp3, clp4};
    map::LaneLocation start_location = GetLaneLocation(&lane1, 0);
    map::LaneLocation end_location = GetLaneLocation(&lane2, 1);

    path_finding::Path path;
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane1, path_finding::EdgeType::kFollow));
    path.emplace_back(std::make_shared<path_finding::PathEntry>(&lane2, path_finding::EdgeType::kChange));

    auto list = CreateLineFollowDataFromPath(path, start_location, end_location);

    ASSERT_EQ(2, list.size());
    EXPECT_TRIPLE(clp1, list.at(0).point);
    EXPECT_NEAR(4.1231, list.at(0).distance_to_next_point(), 0.0001);
    EXPECT_EQ(1, list.at(0).lane_id);

    EXPECT_TRIPLE(clp4, list.at(1).point);
    EXPECT_EQ(0.0_m, list.at(1).distance_to_next_point);
    EXPECT_EQ(1, list.at(1).lane_id);
}

}  // namespace astas::environment::lanefollowing
