/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_POINTLISTTRAVERSER_H
#define GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_POINTLISTTRAVERSER_H

#include "Core/Environment/LaneFollowing/point_distance_list.h"

#include <MantleAPI/Common/vector.h>

namespace astas::environment::lanefollowing
{
class PointListTraverser
{
  public:
    explicit PointListTraverser(const lanefollowing::PointDistanceList& list);

    void Move(units::length::meter_t distance);

    mantle_api::Vec3<units::length::meter_t> GetPosition() const;
    mantle_api::Vec3<units::length::meter_t> GetOrientation() const;
    units::length::meter_t GetAdditionalDistance() const;
    bool IsLastPointReached() const;
    mantle_api::UniqueId GetLaneId() const;
    mantle_api::Vec3<units::length::meter_t> GetNextPoint() const;
    units::length::meter_t GetDistanceToNextPoint() const;
    std::size_t GetIndex() const;

  protected:
    units::length::meter_t additional_distance_{0.0};
    std::size_t index_{0};

    lanefollowing::PointDistanceList list_{};
};

}  // namespace astas::environment::lanefollowing

#endif  // GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_POINTLISTTRAVERSER_H
