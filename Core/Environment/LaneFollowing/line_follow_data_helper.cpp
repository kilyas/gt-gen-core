/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/LaneFollowing/line_follow_data_helper.h"

#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include <algorithm>

namespace astas::environment::lanefollowing
{
using units::literals::operator""_m;

void RemovePointsBeforeIndex(std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line, std::size_t index)
{
    if (!center_line.empty())
    {
        if (index + 1 <= center_line.size())
        {
            index += 1;
        }
        auto erase_index_it{center_line.begin()};
        std::advance(erase_index_it, index);
        center_line.erase(center_line.begin(), erase_index_it);
    }
}

void RemovePointsAfterIndex(std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line, std::size_t index)
{
    auto erase_index_it{center_line.begin()};
    std::advance(erase_index_it, index);
    center_line.erase(erase_index_it, center_line.end());
}

LaneFollowData CreateLaneFollowData(LaneData& lane_data,
                                    const mantle_api::Vec3<units::length::meter_t>& projected_start,
                                    const mantle_api::Vec3<units::length::meter_t>& projected_end)
{
    LaneFollowData lane_follow_data;

    std::reverse(lane_data.begin(), lane_data.end());

    AddPointAndDistance(lane_follow_data, projected_end, lane_data.front().first);
    for (auto& entry : lane_data)
    {
        std::reverse(entry.second.begin(), entry.second.end());
        for (const auto& pt : entry.second)
        {
            AddPointAndDistance(lane_follow_data, pt, entry.first);
        }
    }
    AddPointAndDistance(lane_follow_data, projected_start, lane_data.back().first);

    std::reverse(lane_follow_data.begin(), lane_follow_data.end());

    return lane_follow_data;
}

LaneFollowData CreateLaneFollowData(LaneData& lane_data,
                                    const mantle_api::Vec3<units::length::meter_t>& projected_start)
{
    LaneFollowData lane_follow_data;
    // lane data is reversed it to add projected_start at the end and reversed again to make projected_start the first
    // item in laneFollowData.
    std::reverse(lane_data.begin(), lane_data.end());
    for (auto& entry : lane_data)
    {
        // entry vector is reversed to make the duplicates center line continuous
        // center line points are added into lane_follow_data when the distance is greater than zero between the current
        // point and the last point added in lane_follow_data
        std::reverse(entry.second.begin(), entry.second.end());
        for (const auto& pt : entry.second)
        {
            AddPointAndDistance(lane_follow_data, pt, entry.first);
        }
    }
    AddPointAndDistance(lane_follow_data, projected_start, lane_data.back().first);

    std::reverse(lane_follow_data.begin(), lane_follow_data.end());

    return lane_follow_data;
}

void AddPointAndDistance(LaneFollowData& lane_follow_data,
                         const mantle_api::Vec3<units::length::meter_t>& point,
                         mantle_api::UniqueId lane_id)
{
    if (lane_follow_data.empty())
    {
        lane_follow_data.push_back({point, 0_m, lane_id, {}});
    }
    else
    {
        auto previous_point = lane_follow_data.back();
        const auto min_distance_between_points{0.1_m};
        const units::length::meter_t distance{service::glmwrapper::Distance(previous_point.point, point)};
        if (distance > min_distance_between_points)
        {
            lane_follow_data.push_back({point, distance, lane_id, {}});
        }
    }
}

void AddDistanceToFirstPointIfPathIsClosed(LaneFollowData& lane_follow_data, const path_finding::Path& path)
{
    if (path.IsClosedPath())
    {
        lane_follow_data.pop_back();

        auto& first_entry = lane_follow_data.front();
        auto& last_entry = lane_follow_data.back();
        const units::length::meter_t distance{service::glmwrapper::Distance(first_entry.point, last_entry.point)};
        first_entry.distance_to_next_point = distance;
    }
}

}  // namespace astas::environment::lanefollowing
