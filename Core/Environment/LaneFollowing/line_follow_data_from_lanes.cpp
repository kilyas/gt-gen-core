/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/LaneFollowing/line_follow_data_from_lanes.h"

#include "Core/Environment/LaneFollowing/line_follow_data_helper.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/Logging/logging.h"

namespace astas::environment::lanefollowing
{

LaneData CollectCenterLinesFromLanes(const std::vector<const map::Lane*>& lanes, mantle_api::Direction direction)
{
    LaneData lane_data;
    lane_data.reserve(lanes.size());
    for (const auto* lane : lanes)
    {
        auto center_line = lane->center_line;
        if (direction == mantle_api::Direction::kBackwards)
        {
            std::reverse(center_line.begin(), center_line.end());
        }
        lane_data.push_back({lane->id, center_line});
    }
    return lane_data;
}

bool IsClosedLoop(const std::vector<const map::Lane*>& lanes, mantle_api::Direction direction)
{
    if (direction == mantle_api::Direction::kBackwards)
    {
        if (lanes.size() > 1 && lanes.front()->center_line.back() == lanes.back()->center_line.front())
        {
            return true;
        }
    }
    else
    {
        if (lanes.size() > 1 && lanes.front()->center_line.front() == lanes.back()->center_line.back())
        {
            return true;
        }
    }
    return false;
}

void AppendFirstCenterLineIfClosedLoop(LaneData& lane_data,
                                       const std::vector<const map::Lane*>& lanes,
                                       const map::LaneLocation& start_lane_location,
                                       mantle_api::Direction direction)
{
    if (IsClosedLoop(lanes, direction))
    {
        auto center_line = lanes.front()->center_line;
        lane_data.push_back({lanes.front()->id, center_line});

        RemovePointsAfterIndex(lane_data.back().second, start_lane_location.centerline_point_index);
    }
}

void AddDistanceToFirstPointIfClosedLoop(LaneFollowData& lane_follow_data,
                                         const std::vector<const map::Lane*>& lanes,
                                         mantle_api::Direction direction)
{
    if (IsClosedLoop(lanes, direction))
    {
        if (lane_follow_data.front().point == lane_follow_data.back().point)
        {
            lane_follow_data.pop_back();
        }
        auto& first_entry = lane_follow_data.front();
        auto& last_entry = lane_follow_data.back();
        const units::length::meter_t distance{service::glmwrapper::Distance(first_entry.point, last_entry.point)};
        last_entry.distance_to_next_point = distance;
    }
}

std::size_t GetStartLocationIndex(const std::vector<const map::Lane*>& lanes,
                                  const map::LaneLocation& start_lane_location,
                                  mantle_api::Direction direction)
{
    auto index = start_lane_location.centerline_point_index;
    if (direction == mantle_api::Direction::kBackwards)
    {
        index = (lanes[0]->center_line.size() - 1) - start_lane_location.centerline_point_index - 1;
    }
    return index;
}

LaneFollowData CreateLineFollowDataFromLanes(const std::vector<const map::Lane*>& lanes,
                                             const map::LaneLocation& start_lane_location,
                                             mantle_api::Direction direction)
{
    /// TODO: remove logging when route controller works stable
    Info("Start lane: {}, proj-pt: {}, {}, {}, idx: {}",
         start_lane_location.lanes.front()->id,
         start_lane_location.projected_centerline_point.x,
         start_lane_location.projected_centerline_point.y,
         start_lane_location.projected_centerline_point.z,
         start_lane_location.centerline_point_index);

    auto start_location_index = GetStartLocationIndex(lanes, start_lane_location, direction);
    auto lane_data = CollectCenterLinesFromLanes(lanes, direction);

    AppendFirstCenterLineIfClosedLoop(lane_data, lanes, start_lane_location, direction);
    RemovePointsBeforeIndex(lane_data.front().second, start_location_index);
    auto lane_follow_data = CreateLaneFollowData(lane_data, start_lane_location.projected_centerline_point);
    AddDistanceToFirstPointIfClosedLoop(lane_follow_data, lanes, direction);
    return lane_follow_data;
}

}  // namespace astas::environment::lanefollowing
