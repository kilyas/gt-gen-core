/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/LaneFollowing/point_list_traverser.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::environment::lanefollowing
{
using units::literals::operator""_m;

PointDistanceList GetPointDistanceList()
{
    PointDistanceList list;
    list.push_back({mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}, 1.0_m, 1, {}});
    list.push_back({mantle_api::Vec3<units::length::meter_t>{1_m, 0_m, 0_m}, 0.0_m, 2, {}});
    list.push_back({mantle_api::Vec3<units::length::meter_t>{1_m, 0_m, 0_m}, 1.0_m, 3, {}});
    list.push_back({mantle_api::Vec3<units::length::meter_t>{2_m, 0_m, 0_m}, 0.0_m, 4, {}});
    return list;
}

PointDistanceList GetClosedLoopPointDistanceList()
{
    lanefollowing::PointDistanceList list;
    list.push_back({mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}, 1.0_m, 1, {}});
    list.push_back({mantle_api::Vec3<units::length::meter_t>{1_m, 0_m, 0_m}, 0.0_m, 1, {}});
    list.push_back({mantle_api::Vec3<units::length::meter_t>{1_m, 0_m, 0_m}, 1.0_m, 2, {}});
    list.push_back({mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}, 1.0_m, 2, {}});
    return list;
}

class PointListTraverserSUT : public PointListTraverser
{
  public:
    explicit PointListTraverserSUT(const PointDistanceList& list) : PointListTraverser(list) {}

    void SetAdditionalDistance(units::length::meter_t s) { additional_distance_ = s; }
};

//////////////////////////////////////////////////
// Point-Distance-List:
// (0,0), 1
// (1,0), 0
// (1,0), 1
// (2,0), 0
//
// Test cases:
// dist | add_dist | exp_pos | add_dist' | exp_lane_id
// -----|----------|---------|-----------
// 0    | 0        | (0,0)   | 0         | 1
// 0.5  | 0        | (0,0)   | 0.5       | 1
// 1    | 0        | (1,0)   | 0         | 3
// 3    | 0        | (2,0)   | 0         | 4
// 0.5  | 1        | (1,0)   | 0.5       | 3
// 2    | 1        | (2,0)   | 0         | 4

TEST(PointListTraverserTest, GivenNoDistanceToTravel_WhenMove_ThenPositionStaysAtFirstPoint)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{0.0};

    PointListTraverser traverser(list);
    traverser.Move(s);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), traverser.GetPosition());
    EXPECT_EQ(0.0_m, traverser.GetAdditionalDistance());
    EXPECT_EQ(1, traverser.GetLaneId());
}

TEST(PointListTraverserTest, GivenShortDistanceToTravel_WhenMove_ThenPositionAtFirstPointAndAdditionDistanceSet)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{0.5};

    PointListTraverser traverser(list);
    traverser.Move(s);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), traverser.GetPosition());
    EXPECT_EQ(0.5_m, traverser.GetAdditionalDistance());
    EXPECT_EQ(1, traverser.GetLaneId());
}

TEST(PointListTraverserTest, GivenDistanceMatchesDistanceToNextPoint_WhenMove_ThenPositionIsOnNextPoint)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{1.0};

    PointListTraverser traverser(list);
    traverser.Move(s);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(1_m, 0_m, 0_m), traverser.GetPosition());
    EXPECT_EQ(0.0_m, traverser.GetAdditionalDistance());
    EXPECT_EQ(3, traverser.GetLaneId());
}

TEST(PointListTraverserTest, GivenDistanceExceedingLastPoint_WhenMove_ThenPositionIsOnLastPoint)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{3.0};

    PointListTraverser traverser(list);
    traverser.Move(s);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(2_m, 0_m, 0_m), traverser.GetPosition());
    EXPECT_EQ(0.0_m, traverser.GetAdditionalDistance());
    EXPECT_EQ(4, traverser.GetLaneId());
}

TEST(PointListTraverserTest,
     GivenShortDistanceAndAdditionalDistance_WhenMove_ThenPositionIsOnNextPointAndAdditionalDistanceSet)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{0.5};

    PointListTraverserSUT traverser(list);
    traverser.SetAdditionalDistance(1.0_m);
    traverser.Move(s);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(1_m, 0_m, 0_m), traverser.GetPosition());
    EXPECT_EQ(0.5_m, traverser.GetAdditionalDistance());
    EXPECT_EQ(3, traverser.GetLaneId());
}

TEST(PointListTraverserTest, GivenDistanceAndAdditionalDistanceExceedLastPoint_WhenMove_ThenPositionIsOnLastPoint)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{2.0};

    PointListTraverserSUT traverser(list);
    traverser.SetAdditionalDistance(1.0_m);
    traverser.Move(s);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(2_m, 0_m, 0_m), traverser.GetPosition());
    EXPECT_EQ(0.0_m, traverser.GetAdditionalDistance());
    EXPECT_EQ(4, traverser.GetLaneId());
}

TEST(PointListTraverserTest, GivenClosedPathAndExceedLastPoint_WhenMove_ThenIndexSetToBegin)
{
    auto list = GetClosedLoopPointDistanceList();
    units::length::meter_t s{2.5};

    PointListTraverser traverser(list);
    traverser.Move(s);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), traverser.GetPosition());
    EXPECT_EQ(0.5_m, traverser.GetAdditionalDistance());
    EXPECT_EQ(2, traverser.GetLaneId());
}

TEST(PointListTraverserTest, GivenDistanceMatchesDistanceToNextPoint_WhenMove_ThenIndexIsMoving)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{1.0};

    PointListTraverser traverser(list);
    EXPECT_EQ(0, traverser.GetIndex());
    traverser.Move(s);
    EXPECT_EQ(2, traverser.GetIndex());
    traverser.Move(s);
    EXPECT_EQ(3, traverser.GetIndex());
}

TEST(PointListTraverserTest, GivenDistanceMatchesDistanceToLastPoint_WhenMove_ThenIndexIsOnLastPoint)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{3.0};

    PointListTraverser traverser(list);
    traverser.Move(s);
    EXPECT_EQ(3, traverser.GetIndex());
}

TEST(PointListTraverserTest, GivenNotAllPointTraversed_WhenIsLastPointReached_ThenReturnFalse)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{1.99};

    PointListTraverser traverser(list);
    traverser.Move(s);

    EXPECT_FALSE(traverser.IsLastPointReached());
    EXPECT_EQ(3, traverser.GetLaneId());
}

TEST(PointListTraverserTest, GivenAllPointTraversed_WhenIsLastPointReached_ThenReturnTrue)
{
    auto list = GetPointDistanceList();
    units::length::meter_t s{2.0};

    PointListTraverser traverser(list);
    traverser.Move(s);

    EXPECT_TRUE(traverser.IsLastPointReached());
    EXPECT_EQ(4, traverser.GetLaneId());
}

}  // namespace astas::environment::lanefollowing
