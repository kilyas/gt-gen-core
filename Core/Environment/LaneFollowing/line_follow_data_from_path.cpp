/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/LaneFollowing/line_follow_data_from_path.h"

#include "Core/Environment/LaneFollowing/line_follow_data_helper.h"
#include "Core/Service/Logging/logging.h"

namespace astas::environment::lanefollowing
{

LaneData CollectCenterLinesFromPath(const path_finding::Path& path)
{
    LaneData lane_data;
    lane_data.reserve(path.size());
    for (const auto& entry : path)
    {
        auto center_line = entry->lane->center_line;
        if (entry->edge_type == path_finding::EdgeType::kChange)
        {
            // In order to allow a smooth lane change:
            // 1. Do not add both adjacent lanes otherwise both lanes would be driven
            //    -> Resulting in driving back to the beginning of the adjacent lane
            // 2. By fitting the center line to a spline, we cannot have "hard" jumps from end of one lane
            //    to beginning of succeeding + adjacent lane. Therefore, set end point of first adjacent lane to
            //    represent the end point of the second adjacent lane.
            lane_data.back().second.back() = center_line.back();
        }
        else
        {
            lane_data.push_back({entry->lane->id, center_line});
        }
    }

    return lane_data;
}

LaneFollowData CreateLineFollowDataFromPath(const path_finding::Path& path,
                                            const map::LaneLocation& start_lane_location,
                                            const map::LaneLocation& end_lane_location)
{
    /// TODO: remove logging when route controller works stable
    Info("Start lane: {}, proj-pt: {}, {}, {}, idx: {}",
         start_lane_location.lanes.front()->id,
         start_lane_location.projected_centerline_point.x,
         start_lane_location.projected_centerline_point.y,
         start_lane_location.projected_centerline_point.z,
         start_lane_location.centerline_point_index);

    Info("End lane: {}, proj-pt: {}, {}, {}, idx: {}",
         end_lane_location.lanes.front()->id,
         end_lane_location.projected_centerline_point.x,
         end_lane_location.projected_centerline_point.y,
         end_lane_location.projected_centerline_point.z,
         end_lane_location.centerline_point_index);

    auto lane_data = CollectCenterLinesFromPath(path);
    /// TODO: What happens if the start and end points are not in the lane data?
    RemovePointsAfterIndex(lane_data.back().second, end_lane_location.centerline_point_index);
    RemovePointsBeforeIndex(lane_data.front().second, start_lane_location.centerline_point_index);

    auto lane_follow_data = CreateLaneFollowData(
        lane_data, start_lane_location.projected_centerline_point, end_lane_location.projected_centerline_point);
    AddDistanceToFirstPointIfPathIsClosed(lane_follow_data, path);

    return lane_follow_data;
}

}  // namespace astas::environment::lanefollowing
