/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_LINEFOLLOWDATAFROMPATH_H
#define GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_LINEFOLLOWDATAFROMPATH_H

#include "Core/Environment/LaneFollowing/point_distance_list.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location.h"
#include "Core/Environment/PathFinding/path.h"

namespace astas::environment::lanefollowing
{

LaneFollowData CreateLineFollowDataFromPath(const path_finding::Path& path,
                                            const map::LaneLocation& start_lane_location,
                                            const map::LaneLocation& end_lane_location);

}  // namespace astas::environment::lanefollowing

#endif  // GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_LINEFOLLOWDATAFROMPATH_H
