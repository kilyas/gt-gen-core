/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/LaneFollowing/line_follow_data_from_path.h"
#include "Core/Environment/PathFinding/path_finder.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::environment::lanefollowing
{
using units::literals::operator""_m;

TEST(LineFollowDataFromPathComponentTest,
     GivenMapWithOneLane_WhenCreatingFromStartToEndOfLane_ThenListContainsAllPoints)
{
    auto astas_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(3, 1);
    map::LaneLocationProvider lane_location_provider{*astas_map};

    mantle_api::Vec3<units::length::meter_t> start{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> end{2_m, 0_m, 0_m};
    map::LaneLocation start_location = lane_location_provider.GetLaneLocation(start);
    map::LaneLocation end_location = lane_location_provider.GetLaneLocation(end);

    path_finding::PathFinder finder(*astas_map);
    auto opt_route = finder.ComputeRoute({start, end});
    ASSERT_TRUE(opt_route.has_value());

    auto point_distance_list = CreateLineFollowDataFromPath(opt_route.value(), start_location, end_location);

    ASSERT_EQ(3, point_distance_list.size());
    EXPECT_TRIPLE(start, point_distance_list.front().point)
    EXPECT_TRIPLE(end, point_distance_list.back().point)
}

TEST(LineFollowDataFromPathComponentTest, GivenMapWithTwoLanes_WhenCreatingFromStartToEnd_ThenListContainsAllPoints)
{
    auto astas_map = test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach();
    map::LaneLocationProvider lane_location_provider{*astas_map};

    mantle_api::Vec3<units::length::meter_t> start{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> end{198_m, 0_m, 0_m};
    map::LaneLocation start_location = lane_location_provider.GetLaneLocation(start);
    map::LaneLocation end_location = lane_location_provider.GetLaneLocation(end);

    path_finding::PathFinder finder(*astas_map);
    auto opt_route = finder.ComputeRoute({start, end});
    ASSERT_TRUE(opt_route.has_value());

    auto point_distance_list = CreateLineFollowDataFromPath(opt_route.value(), start_location, end_location);
    ASSERT_EQ(199, point_distance_list.size());
    EXPECT_TRIPLE(start, point_distance_list.front().point)
    EXPECT_TRIPLE(end, point_distance_list.back().point)
}

TEST(LineFollowDataFromPathComponentTest,
     GivenThreeConnectedLanes_WhenConstructingWithStartPointAlongFirstLane_ThenListStartsWithinFirstLane)
{
    auto astas_map = test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach();
    map::LaneLocationProvider lane_location_provider{*astas_map};

    mantle_api::Vec3<units::length::meter_t> start_not_on_first_point{50_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> end{297_m, 0_m, 0_m};
    map::LaneLocation start_location = lane_location_provider.GetLaneLocation(start_not_on_first_point);
    map::LaneLocation end_location = lane_location_provider.GetLaneLocation(end);

    path_finding::PathFinder finder(*astas_map);
    auto opt_route = finder.ComputeRoute({start_not_on_first_point, end});
    ASSERT_TRUE(opt_route.has_value());

    auto point_distance_list = CreateLineFollowDataFromPath(opt_route.value(), start_location, end_location);
    ASSERT_EQ(248, point_distance_list.size());
    EXPECT_TRIPLE(start_not_on_first_point, point_distance_list.front().point)
    EXPECT_TRIPLE(end, point_distance_list.back().point)
}

TEST(LineFollowDataFromPathComponentTest,
     GivenThreeConnectedLanes_WhenConstructingWithEndPointAlongLastLane_ThenListEndsWithinLastLane)
{
    auto astas_map = test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach();
    map::LaneLocationProvider lane_location_provider{*astas_map};

    mantle_api::Vec3<units::length::meter_t> start{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> end_not_last_point{247_m, 0_m, 0_m};
    map::LaneLocation start_location = lane_location_provider.GetLaneLocation(start);
    map::LaneLocation end_location = lane_location_provider.GetLaneLocation(end_not_last_point);

    path_finding::PathFinder finder(*astas_map);
    auto opt_route = finder.ComputeRoute({start, end_not_last_point});
    ASSERT_TRUE(opt_route.has_value());

    auto point_distance_list = CreateLineFollowDataFromPath(opt_route.value(), start_location, end_location);
    ASSERT_EQ(248, point_distance_list.size());
    EXPECT_TRIPLE(start, point_distance_list.front().point)
    EXPECT_TRIPLE(end_not_last_point, point_distance_list.back().point)
}

}  // namespace astas::environment::lanefollowing
