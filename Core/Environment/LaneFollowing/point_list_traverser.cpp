/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/LaneFollowing/point_list_traverser.h"

namespace astas::environment::lanefollowing
{
using units::literals::operator""_m;

PointListTraverser::PointListTraverser(const lanefollowing::PointDistanceList& list) : list_{list} {}

void PointListTraverser::Move(units::length::meter_t distance)
{
    distance += additional_distance_;

    while (distance > GetDistanceToNextPoint() ||
           mantle_api::AlmostEqual(distance, GetDistanceToNextPoint(), 0.001_m, true))
    {
        distance -= GetDistanceToNextPoint();
        if (IsLastPointReached())
        {
            distance = 0_m;
            break;
        }
        index_++;
    }

    additional_distance_ = distance;
}

units::length::meter_t PointListTraverser::GetDistanceToNextPoint() const
{
    return list_.at(index_).distance_to_next_point;
}

mantle_api::Vec3<units::length::meter_t> PointListTraverser::GetPosition() const
{
    return list_.at(index_).point;
}

mantle_api::Vec3<units::length::meter_t> PointListTraverser::GetNextPoint() const
{
    return list_.at(index_ + 1).point;
}

mantle_api::Vec3<units::length::meter_t> PointListTraverser::GetOrientation() const
{
    return list_.at(index_).heading;
}

units::length::meter_t PointListTraverser::GetAdditionalDistance() const
{
    return additional_distance_;
}
bool PointListTraverser::IsLastPointReached() const
{
    return index_ == list_.size() - 1;
}

mantle_api::UniqueId PointListTraverser::GetLaneId() const
{
    return list_.at(index_).lane_id;
}

std::size_t PointListTraverser::GetIndex() const
{
    return index_;
}

}  // namespace astas::environment::lanefollowing
