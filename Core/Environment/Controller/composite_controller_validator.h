/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLERVALIDATOR_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLERVALIDATOR_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_control_unit.h"

#include <MantleAPI/Common/i_identifiable.h>

namespace astas::environment::controller
{

class CompositeControllerValidator
{
  public:
    static void DeleteConflictingControlUnits(IControlUnit* control_unit,
                                              std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                              mantle_api::UniqueId unique_id);
    static void InsertControlUnitInOrder(std::unique_ptr<IControlUnit> control_unit,
                                         std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                         mantle_api::UniqueId unique_id);
    static bool NeedsPathControlUnit(std::vector<std::unique_ptr<IControlUnit>>& control_units);

  private:
    static void DeleteConflictingLongitudinalControlUnits(IControlUnit* control_unit,
                                                          std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                                          mantle_api::UniqueId unique_id);
    static void DeleteConflictingLateralControlUnits(IControlUnit* control_unit,
                                                     std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                                     mantle_api::UniqueId unique_id);
    static void DeleteConflictingRoutingControlUnits(IControlUnit* control_unit,
                                                     std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                                     mantle_api::UniqueId unique_id);

    static bool IsLongitudinalControlUnit(IControlUnit* control_unit);
    static bool IsLateralControlUnit(IControlUnit* control_unit);
    static bool IsRoutingControlUnit(IControlUnit* control_unit);
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_COMPOSITECONTROLLERVALIDATOR_H
