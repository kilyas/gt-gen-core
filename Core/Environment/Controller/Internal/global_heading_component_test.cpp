/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/lane_assignment_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/maneuver_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"
#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps_pow4;
using units::literals::operator""_mps_cu;
using units::literals::operator""_mps_sq;
using units::literals::operator""_mps;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;
using units::literals::operator""_rad_per_s_cu;

TEST(GlobalHeadingMovementIntegrationTest,
     GivenVelocityAndManeuverControlUnits_WhenSimulatingCutInStraight_ThenHeadingAndVelocityMatchScenario)
{
    units::velocity::meters_per_second_t expected_velocity{27.77777777777778};

    // Define velocity spline sections: maneuver.velocity[0] -> maneuver.velocity[2]
    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines{};
    velocity_splines.push_back(
        {mantle_api::Time{0}, mantle_api::Time{1'000}, {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, expected_velocity}});
    velocity_splines.push_back(
        {mantle_api::Time{1'000}, mantle_api::Time{16'000}, {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, expected_velocity}});
    velocity_splines.push_back({mantle_api::Time{16'000},
                                mantle_api::Time{30'000},
                                {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, expected_velocity}});

    // Define heading spline sections: maneuver.heading[0] -> maneuver.heading[3]
    std::vector<mantle_api::SplineSection<units::angle::radian>> heading_splines{};
    heading_splines.push_back(
        {mantle_api::Time{0}, mantle_api::Time{1'000}, {0.0_rad_per_s_cu, 0.0_rad_per_s_sq, 0.0_rad_per_s, 0.0_rad}});
    heading_splines.push_back(
        {mantle_api::Time{1'000},
         mantle_api::Time{8'500},
         {7.964678213244062e-05_rad_per_s_cu, -0.0008960262989899569_rad_per_s_sq, 0.0_rad_per_s, 0.0_rad}});
    heading_splines.push_back({mantle_api::Time{8'500},
                               mantle_api::Time{12'250},
                               {-7.964678213244062e-05_rad_per_s_cu,
                                0.0008960262989899569_rad_per_s_sq,
                                -0.0_rad_per_s,
                                -0.01680049310606169_rad}});
    heading_splines.push_back({mantle_api::Time{12'250},
                               mantle_api::Time{30'000},
                               {0.0_rad_per_s_cu, 0.0_rad_per_s_sq, 0.0_rad_per_s, 0.0_rad}});

    std::map<int, mantle_api::Vec3<units::length::meter_t>> expected_positions;
    expected_positions.emplace(0, mantle_api::Vec3<units::length::meter_t>{10.0_m, -15.25_m, 0.0_m});
    expected_positions.emplace(1, mantle_api::Vec3<units::length::meter_t>{37.7777777777778_m, -15.25_m, 0.0_m});
    /// y-pos from previous GlobalHeadingMovement impl: -17.2323276403702
    expected_positions.emplace(9, mantle_api::Vec3<units::length::meter_t>{259.987136251795_m, -17.232558_m, 0.0_m});
    /// y-pos from previous GlobalHeadingMovement impl: -18.3606198922759
    expected_positions.emplace(12, mantle_api::Vec3<units::length::meter_t>{343.312629029793_m, -18.3607482_m, 0.0_m});
    /// y-pos from previous GlobalHeadingMovement impl: -18.4218687335916
    expected_positions.emplace(30, mantle_api::Vec3<units::length::meter_t>{843.31235920871_m, -18.42175206_m, 0.0_m});

    std::map<int, mantle_api::Vec3<units::velocity::meters_per_second_t>> expected_velocities;
    expected_velocities.emplace(
        0, mantle_api::Vec3<units::velocity::meters_per_second_t>{27.7777777777778_mps, 0.0_mps, 0.0_mps});
    expected_velocities.emplace(
        8,
        mantle_api::Vec3<units::velocity::meters_per_second_t>{27.7739568921705_mps, -0.460713385369975_mps, 0.0_mps});
    expected_velocities.emplace(
        11,
        mantle_api::Vec3<units::velocity::meters_per_second_t>{27.7765922360044_mps, -0.25663598022121_mps, 0.0_mps});

    std::map<int, mantle_api::Orientation3<units::angle::radian_t>> expected_orientations;
    expected_orientations.emplace(0, mantle_api::Orientation3<units::angle::radian_t>{0.0_rad, 0.0_rad, 0.0_rad});
    expected_orientations.emplace(
        8, mantle_api::Orientation3<units::angle::radian_t>{-0.0165864423790808_rad, 0.0_rad, 0.0_rad});
    expected_orientations.emplace(
        11, mantle_api::Orientation3<units::angle::radian_t>{-0.00923902672736311_rad, 0.0_rad, 0.0_rad});

    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::EmptyMap()};
    map::LaneLocationProvider lane_location_provider{*map};
    CompositeController composite_controller{2, &lane_location_provider};

    composite_controller.SetLaneAssignmentControlUnit(
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider, true));
    composite_controller.AddControlUnit(std::make_unique<VelocityControlUnit>(velocity_splines, 0.0_mps));
    composite_controller.AddControlUnit(
        std::make_unique<ManeuverControlUnit>(heading_splines, &lane_location_provider));

    std::unique_ptr<mantle_api::IEntity> vehicle = std::make_unique<entities::VehicleEntity>(0, "host");
    vehicle->SetPosition(expected_positions.at(0));
    composite_controller.SetEntity(*vehicle);

    // Check at first cycle
    composite_controller.Step(mantle_api::Time(0));
    EXPECT_TRIPLE(expected_orientations.at(0), vehicle->GetOrientation());
    EXPECT_TRIPLE(expected_velocities.at(0), vehicle->GetVelocity());
    EXPECT_TRIPLE(
        (mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq}),
        vehicle->GetAcceleration());
    EXPECT_TRIPLE(expected_positions.at(0), vehicle->GetPosition());

    // Check at beginning of heading change
    composite_controller.Step(mantle_api::Time(1000));
    EXPECT_TRIPLE(expected_orientations.at(0), vehicle->GetOrientation());
    EXPECT_TRIPLE(expected_velocities.at(0), vehicle->GetVelocity());
    EXPECT_TRIPLE(
        (mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq}),
        vehicle->GetAcceleration());
    EXPECT_TRIPLE(expected_positions.at(1), vehicle->GetPosition());

    // Check during heading change
    composite_controller.Step(mantle_api::Time(9000));
    EXPECT_TRIPLE(expected_orientations.at(8), vehicle->GetOrientation());
    EXPECT_TRIPLE(expected_velocities.at(8), vehicle->GetVelocity());
    EXPECT_TRIPLE(
        (mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq}),
        vehicle->GetAcceleration());
    EXPECT_TRIPLE(expected_positions.at(9), vehicle->GetPosition());

    // Check last cycle before maneuver is complete
    composite_controller.Step(mantle_api::Time(12000));
    EXPECT_TRIPLE(expected_orientations.at(11), vehicle->GetOrientation());
    EXPECT_TRIPLE(expected_velocities.at(11), vehicle->GetVelocity());
    EXPECT_TRIPLE(
        (mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq}),
        vehicle->GetAcceleration());
    EXPECT_TRIPLE(expected_positions.at(12), vehicle->GetPosition());

    // Check at the end
    composite_controller.Step(mantle_api::Time(30000));
    EXPECT_TRIPLE(expected_orientations.at(0), vehicle->GetOrientation());
    EXPECT_TRIPLE(expected_velocities.at(0), vehicle->GetVelocity());
    EXPECT_TRIPLE(
        (mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq}),
        vehicle->GetAcceleration());
    EXPECT_TRIPLE(expected_positions.at(30), vehicle->GetPosition());
}

TEST(GlobalHeadingMovementIntegrationTest, GivenEntityOrientedDiagonal_WhenStep_ThenVelocityComponentsXAndYPositive)
{
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::EmptyMap()};
    map::LaneLocationProvider lane_location_provider{*map};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");

    units::velocity::meters_per_second_t expected_velocity{27.7777777777777778};
    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines{};
    velocity_splines.push_back(
        {mantle_api::Time{0}, mantle_api::Time{1'000}, {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, expected_velocity}});

    std::vector<mantle_api::SplineSection<units::angle::radian>> heading_splines{};
    heading_splines.push_back({mantle_api::Time{0},
                               mantle_api::Time{1'000},
                               {0.0_rad_per_s_cu, 0.0_rad_per_s_sq, 0.0_rad_per_s, units::angle::radian_t(M_PI_2)}});

    CompositeController composite_controller{2, &lane_location_provider};
    composite_controller.SetLaneAssignmentControlUnit(
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider, true));
    composite_controller.AddControlUnit(std::make_unique<VelocityControlUnit>(velocity_splines, 0.0_mps));
    composite_controller.AddControlUnit(
        std::make_unique<ManeuverControlUnit>(heading_splines, &lane_location_provider));

    composite_controller.SetEntity(*entity);

    composite_controller.Step(mantle_api::Time{100});

    EXPECT_GT(entity->GetVelocity().x(), 0.0);
    EXPECT_GT(entity->GetVelocity().y(), 0.0);
    EXPECT_DOUBLE_EQ(entity->GetVelocity().z(), 0.0);
    EXPECT_DOUBLE_EQ(expected_velocity(), entity->GetVelocity().Length()());
}

TEST(GlobalHeadingMovementIntegrationTest, GivenEntityOrientedDiagonal_WhenStep_ThenAccelerationComponentsXAndYPositive)
{
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::EmptyMap()};
    map::LaneLocationProvider lane_location_provider{*map};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");

    units::acceleration::meters_per_second_squared_t expected_acceleration{2.5};
    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines{};
    velocity_splines.push_back(
        {mantle_api::Time{0}, mantle_api::Time{1'000}, {0.0_mps_pow4, 0.0_mps_cu, expected_acceleration, 0.0_mps}});

    std::vector<mantle_api::SplineSection<units::angle::radian>> heading_splines{};
    heading_splines.push_back({mantle_api::Time{0},
                               mantle_api::Time{1'000},
                               {0.0_rad_per_s_cu, 0.0_rad_per_s_sq, 0.0_rad_per_s, units::angle::radian_t(M_PI_2)}});

    CompositeController composite_controller{2, &lane_location_provider};
    composite_controller.SetLaneAssignmentControlUnit(
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider, true));
    composite_controller.AddControlUnit(std::make_unique<VelocityControlUnit>(velocity_splines, 0.0_mps));
    composite_controller.AddControlUnit(
        std::make_unique<ManeuverControlUnit>(heading_splines, &lane_location_provider));

    composite_controller.SetEntity(*entity);

    composite_controller.Step(mantle_api::Time{100});

    EXPECT_GT(entity->GetAcceleration().x(), 0.0);
    EXPECT_GT(entity->GetAcceleration().y(), 0.0);
    EXPECT_DOUBLE_EQ(entity->GetAcceleration().z(), 0.0);
    EXPECT_DOUBLE_EQ(expected_acceleration(), entity->GetAcceleration().Length()());
}

}  // namespace astas::environment::controller
