/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/recovery_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Service/Utility/position_utils.h"

namespace astas::environment::controller
{

RecoveryControlUnit::RecoveryControlUnit(host::HostVehicleInterface* host_interface,
                                         mantle_api::ILaneLocationQueryService* map_query_service)
    : host_vehicle_interface_(host_interface),
      lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)}
{
}

std::unique_ptr<IControlUnit> RecoveryControlUnit::Clone() const
{
    return std::make_unique<RecoveryControlUnit>(*this);
}

void RecoveryControlUnit::StepControlUnit()
{
    auto lane_location = lane_location_provider_->GetLaneLocation(entity_->GetPosition());

    bool was_recovered{false};
    if (lane_location.IsValid())
    {
        if (IsRecoverySuitable(lane_location.lanes))
        {
            last_recoverable_lane_location_ = lane_location;
        }
    }
    else
    {
        RecoverToLastValidLocationIfAllowed();
        was_recovered = true;
    }
    recovery_in_tick_.push_back(was_recovered);

    host_vehicle_interface_->SetHostPoseRecoveredInLastStep(was_recovered);
}

bool RecoveryControlUnit::IsRecoverySuitable(const std::vector<const environment::map::Lane*>& lanes)
{
    bool is_suitable{true};

    for (const auto* lane : lanes)
    {
        if (lane->flags.IsShoulderLane() || !lane->flags.IsDrivable())
        {
            is_suitable = false;
        }
    }

    return is_suitable;
}

void RecoveryControlUnit::RecoverToLastValidLocationIfAllowed()
{
    if (RecoveryDeadlockDetected())
    {
        throw(EnvironmentException(
            "Recovery deadlock occurred! Host vehicle was recovered more than {} times on the same position.",
            max_recovery_attempts));
    }
    RecoverHostVehiclePose();

    std::vector<mantle_api::UniqueId> lane_ids{};

    std::transform(last_recoverable_lane_location_.lanes.begin(),
                   last_recoverable_lane_location_.lanes.end(),
                   std::back_inserter(lane_ids),
                   [](const map::Lane* lane) { return lane->id; });
}

void RecoveryControlUnit::RecoverHostVehiclePose()
{
    if (!last_recoverable_lane_location_.IsValid())
    {
        throw(
            EnvironmentException{"Tried to recover HostVehicle without a valid lane location to recover to! "
                                 "Possibly the host vehicle was never placed on a drivable lane."});
    }

    mantle_api::Vec3<units::length::meter_t> recovery_position = service::utility::GetVerticalShiftedPosition(
        last_recoverable_lane_location_.projected_centerline_point, entity_->GetProperties()->bounding_box);

    entity_->SetPosition(recovery_position);
    entity_->SetOrientation(last_recoverable_lane_location_.GetOrientation());
}

bool RecoveryControlUnit::RecoveryDeadlockDetected()
{
    auto recoveries_in_last_ticks = std::count_if(
        recovery_in_tick_.begin(), recovery_in_tick_.end(), [](bool was_recovered) { return was_recovered; });

    return recoveries_in_last_ticks >= static_cast<decltype(recoveries_in_last_ticks)>(max_recovery_attempts);
}

bool RecoveryControlUnit::HasFinished() const
{
    return false;
}

}  // namespace astas::environment::controller
