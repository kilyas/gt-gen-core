/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Traffic/i_controller_config.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;

class FollowTrajectoryControlUnitTest : public testing::Test
{
  protected:
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    ControllerDataExchangeContainer data_exchange_container_{{0.0_mps},
                                                             0.0_mps_sq,
                                                             0.0_rad_per_s,
                                                             0.0_rad_per_s_sq,
                                                             {0.0_m}};
};

TEST_F(FollowTrajectoryControlUnitTest, GivenFollowTrajectoryUnit_WhenCopy_ThenExceptionNotThrown)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory);

    EXPECT_NO_THROW(FollowTrajectoryControlUnit trajectory_controller_copy(trajectory_controller));
}

TEST_F(FollowTrajectoryControlUnitTest, GivenEmptyTrajectory_WhenCreatingFollowTrajectoryController_ThenExceptionThrown)
{

    mantle_api::Trajectory trajectory{};

    EXPECT_THROW(FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory),
                 EnvironmentException);
}

TEST_F(FollowTrajectoryControlUnitTest, GivenFollowTrajectoryUnit_WhenClone_ThenExceptionNotThrown)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;

    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    auto controller_ptr = std::make_unique<FollowTrajectoryControlUnit>(&lane_location_provider_, trajectory);

    auto copy = controller_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(controller_ptr.get(), copy.get());
}

TEST_F(FollowTrajectoryControlUnitTest,
       GivenFollowTrajectoryUnitWithEnoughEntitySpeed_WhenReachLastPoint_ThenHasFinishedIsTrue)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory);
    trajectory_controller.SetEntity(*entity);
    data_exchange_container_.velocity_scalars.push_back(10_mps);
    trajectory_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    trajectory_controller.Step(mantle_api::Time{0});
    trajectory_controller.Step(mantle_api::Time{1000});
    trajectory_controller.Step(mantle_api::Time{2000});

    EXPECT_TRUE(trajectory_controller.HasFinished());
}

TEST_F(FollowTrajectoryControlUnitTest,
       GivenFollowTrajectoryUnitWithNotEnoughEntitySpeed_WhenReachLastPoint_ThenHasFinishedIsFalse)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory);
    trajectory_controller.SetEntity(*entity);
    data_exchange_container_.velocity_scalars.push_back(5_mps);
    data_exchange_container_.velocity_scalars.push_back(5_mps);
    data_exchange_container_.velocity_scalars.push_back(5_mps);
    trajectory_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    trajectory_controller.Step(mantle_api::Time{0});
    trajectory_controller.Step(mantle_api::Time{1000});
    trajectory_controller.Step(mantle_api::Time{2000});

    EXPECT_FALSE(trajectory_controller.HasFinished());
}

TEST_F(FollowTrajectoryControlUnitTest, GivenTrajectory_WhenStepMultiplesTimes_ThenEntityHasPositionFromTrajectory)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;

    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{10.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);

    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory);
    trajectory_controller.SetEntity(*entity);
    data_exchange_container_.velocity_scalars.push_back(10_mps);
    trajectory_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(5_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(10_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(15_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(20_m, 0_m, 0_m), entity->GetPosition());
}

TEST_F(FollowTrajectoryControlUnitTest,
       GivenTrajectoryPointsWithSplitOnLeftSide_WhenMoveProgresses_ThenTargetOnOtherLaneIsReached)
{
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes()};
    map::LaneLocationProvider lane_location_provider{*map};

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;

    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{80.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{159.83983299_m, 4_m, 0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{239.83983299_m, 4_m, 0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{297_m, 4_m, 0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);

    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory);
    trajectory_controller.SetEntity(*entity);
    data_exchange_container_.velocity_scalars.push_back(80_mps);
    trajectory_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition());

    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(80_m, 0_m, 0_m), entity->GetPosition());

    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(159.89986219_m, 4_m, 0_m), entity->GetPosition());

    trajectory_controller.Step(mantle_api::Time{3000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(239.89986219_m, 4_m, 0_m), entity->GetPosition());

    trajectory_controller.Step(mantle_api::Time{4000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(297_m, 4_m, 0_m), entity->GetPosition());
}

TEST_F(
    FollowTrajectoryControlUnitTest,
    GivenTrajectoryWithOrientationPoints_WhenStepMultiplesTimes_ThenEntityHasOrientationInterpolatedFromTrajectoryOrientationPoints)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {
        {10.0_m, 0.0_m, 0.0_m},
        {units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}}};
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::vector<mantle_api::Orientation3<units::angle::radian_t>> expected_orientation;
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4 / 4}, units::angle::radian_t{M_PI_4 / 4}, units::angle::radian_t{M_PI_4 / 4}});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4 / 2}, units::angle::radian_t{M_PI_4 / 2}, units::angle::radian_t{M_PI_4 / 2}});
    expected_orientation.push_back(
        mantle_api::Orientation3<units::angle::radian_t>{units::angle::radian_t{M_PI_4 * 3 / 4},
                                                         units::angle::radian_t{M_PI_4 * 3 / 4},
                                                         units::angle::radian_t{M_PI_4 * 3 / 4}});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}});

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory);
    trajectory_controller.SetEntity(*entity);
    data_exchange_container_.velocity_scalars.push_back(10_mps);
    trajectory_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_orientation[0], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{250});
    EXPECT_TRIPLE(expected_orientation[1], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(expected_orientation[2], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{750});
    EXPECT_TRIPLE(expected_orientation[3], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_orientation[4], entity->GetOrientation());
}

TEST_F(
    FollowTrajectoryControlUnitTest,
    GivenTrajectoryWitOrientationPoints_WhenStepMultiplesTimes_ThenEntityHasOrientationFromTrajectoryOrientationPoints)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {
        {10.0_m, 0.0_m, 0.0_m},
        {units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}}};
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {
        {20.0_m, 0.0_m, 0.0_m},
        {units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}}};
    poly_line.emplace_back(poly_line_point);

    trajectory.type = poly_line;

    std::vector<mantle_api::Orientation3<units::angle::radian_t>> expected_orientation;
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}});

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryControlUnit trajectory_controller(&lane_location_provider_, trajectory);
    trajectory_controller.SetEntity(*entity);
    data_exchange_container_.velocity_scalars.push_back(10_mps);
    trajectory_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_orientation[0], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_orientation[1], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(expected_orientation[2], entity->GetOrientation());
}

}  // namespace astas::environment::controller
