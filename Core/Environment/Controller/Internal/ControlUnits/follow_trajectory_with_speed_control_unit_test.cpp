/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_with_speed_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Traffic/i_controller_config.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps_sq;
using units::literals::operator""_mps;
using units::literals::operator""_rad;

class FollowTrajectoryWithSpeedControlUnitTest : public testing::Test
{
  protected:
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference_{
        mantle_api::ReferenceContext::kAbsolute,
        1.0,
        units::time::second_t(0)};
};

TEST_F(FollowTrajectoryWithSpeedControlUnitTest, GivenFollowTrajectoryUnit_WhenCopy_ThenExceptionNotThrown)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);

    EXPECT_NO_THROW(FollowTrajectoryWithSpeedControlUnit trajectory_controller_copy(trajectory_controller));
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest,
       GivenEmptyTrajectory_WhenCreatingFollowTrajectoryWithSpeedController_ThenExceptionThrown)
{

    mantle_api::Trajectory trajectory{};

    EXPECT_THROW(FollowTrajectoryWithSpeedControlUnit trajectory_controller(
                     &lane_location_provider_, trajectory, time_reference_),
                 EnvironmentException);
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest, GivenFollowTrajectoryUnit_WhenClone_ThenExceptionNotThrown)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;

    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    auto controller_ptr =
        std::make_unique<FollowTrajectoryWithSpeedControlUnit>(&lane_location_provider_, trajectory, time_reference_);

    auto copy = controller_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(controller_ptr.get(), copy.get());
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest, GivenFollowTrajectoryUnit_WhenReachLastPoint_ThenHasFinishedIsTrue)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(0.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    trajectory_controller.Step(mantle_api::Time{1000});
    trajectory_controller.Step(mantle_api::Time{2000});

    EXPECT_TRUE(trajectory_controller.HasFinished());
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest,
       GivenTrajectoryStartingAtZeroAndRelativeTimeReference_WhenStepMultiplesTimes_ThenEntityHasPositionFromTrajectory)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    time_reference_.domainAbsoluteRelative = mantle_api::ReferenceContext::kRelative;
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(0.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{30.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{60.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(2.0);
    poly_line.emplace_back(poly_line_point);

    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);
    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(20_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(25_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(30_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(45_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(60_m, 0_m, 0_m), entity->GetPosition());
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest,
       GivenTrajectoryStartingNotAtZeroAndRelativeTimeFrame_WhenStepMultiplesTimes_ThenEntityHasPositionFromTrajectory)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    time_reference_.domainAbsoluteRelative = mantle_api::ReferenceContext::kRelative;
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{30.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(2.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{60.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(3.0);
    poly_line.emplace_back(poly_line_point);

    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(20_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(25_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(30_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{2500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(45_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{3000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(60_m, 0_m, 0_m), entity->GetPosition());
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest,
       GivenTrajectoryNotStartingAtZero_WhenStepMultiplesTimes_ThenEntityHasPositionFromTrajectory)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{30.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(2.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{60.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(3.0);
    poly_line.emplace_back(poly_line_point);

    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(20_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(25_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(30_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{2500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(45_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{3000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(60_m, 0_m, 0_m), entity->GetPosition());
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest,
       GivenTrajectoryStartingAtZero_WhenStepMultiplesTimes_ThenEntityHasPositionFromTrajectory)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(0.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{40.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(2.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(10_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(20_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(40_m, 0_m, 0_m), entity->GetPosition());
    trajectory_controller.Step(mantle_api::Time{3000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(40_m, 0_m, 0_m), entity->GetPosition());
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest,
       GivenTrajectoryWithNotSteadyVelocity_WhenStepMultiplesTimes_ThenEntityHasVelocity)
{

    mantle_api::Trajectory trajectory{};
    mantle_api::PolyLine poly_line{};

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(0.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{20.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{30.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(2.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::vector<mantle_api::Vec3<units::velocity::meters_per_second_t>> expected_velocities;
    expected_velocities.push_back(mantle_api::Vec3<units::velocity::meters_per_second_t>{0_mps, 0.0_mps, 0.0_mps});
    expected_velocities.push_back(mantle_api::Vec3<units::velocity::meters_per_second_t>{20_mps, 0.0_mps, 0.0_mps});
    expected_velocities.push_back(mantle_api::Vec3<units::velocity::meters_per_second_t>{20_mps, 0.0_mps, 0.0_mps});
    expected_velocities.push_back(mantle_api::Vec3<units::velocity::meters_per_second_t>{10_mps, 0.0_mps, 0.0_mps});
    expected_velocities.push_back(mantle_api::Vec3<units::velocity::meters_per_second_t>{10_mps, 0.0_mps, 0.0_mps});

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_velocities[0], entity->GetVelocity());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(expected_velocities[1], entity->GetVelocity());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_velocities[2], entity->GetVelocity());
    trajectory_controller.Step(mantle_api::Time{1500});
    EXPECT_TRIPLE(expected_velocities[3], entity->GetVelocity());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(expected_velocities[4], entity->GetVelocity());
}

TEST_F(FollowTrajectoryWithSpeedControlUnitTest,
       GivenTrajectoryWithNotSteadyVelocity_WhenStepMultiplesTimes_ThenEntityHasAcceleration)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(0.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{10.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{25.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(2.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{30.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(3.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::vector<mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>> expected_acceleration;
    expected_acceleration.push_back(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq});
    expected_acceleration.push_back(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{10.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq});
    expected_acceleration.push_back(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq});
    expected_acceleration.push_back(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{5.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq});
    expected_acceleration.push_back(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq});
    expected_acceleration.push_back(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{-10.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq});
    expected_acceleration.push_back(
        mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>{0.0_mps_sq, 0.0_mps_sq, 0.0_mps_sq});

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_acceleration[0], entity->GetAcceleration());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(expected_acceleration[1], entity->GetAcceleration());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_acceleration[2], entity->GetAcceleration());
    trajectory_controller.Step(mantle_api::Time{1500});
    EXPECT_TRIPLE(expected_acceleration[3], entity->GetAcceleration());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(expected_acceleration[4], entity->GetAcceleration());
    trajectory_controller.Step(mantle_api::Time{2500});
    EXPECT_TRIPLE(expected_acceleration[5], entity->GetAcceleration());
    trajectory_controller.Step(mantle_api::Time{3000});
    EXPECT_TRIPLE(expected_acceleration[6], entity->GetAcceleration());
}

TEST_F(
    FollowTrajectoryWithSpeedControlUnitTest,
    GivenTrajectoryWithOrientationPoints_WhenStepMultiplesTimes_ThenEntityHasOrientationInterpolatedFromTrajectoryOrientationPoints)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(0.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {
        {10.0_m, 0.0_m, 0.0_m},
        {units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    std::vector<mantle_api::Orientation3<units::angle::radian_t>> expected_orientation;
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4 / 4}, units::angle::radian_t{M_PI_4 / 4}, units::angle::radian_t{M_PI_4 / 4}});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4 / 2}, units::angle::radian_t{M_PI_4 / 2}, units::angle::radian_t{M_PI_4 / 2}});
    expected_orientation.push_back(
        mantle_api::Orientation3<units::angle::radian_t>{units::angle::radian_t{M_PI_4 * 3 / 4},
                                                         units::angle::radian_t{M_PI_4 * 3 / 4},
                                                         units::angle::radian_t{M_PI_4 * 3 / 4}});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}});

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_orientation[0], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{250});
    EXPECT_TRIPLE(expected_orientation[1], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{500});
    EXPECT_TRIPLE(expected_orientation[2], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{750});
    EXPECT_TRIPLE(expected_orientation[3], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_orientation[4], entity->GetOrientation());
}

TEST_F(
    FollowTrajectoryWithSpeedControlUnitTest,
    GivenTrajectoryWitOrientationPoints_WhenStepMultiplesTimes_ThenEntityHasOrientationFromTrajectoryOrientationPoints)
{

    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;

    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(0.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {
        {10.0_m, 0.0_m, 0.0_m},
        {units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {
        {10.0_m, 0.0_m, 0.0_m},
        {units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}}};
    poly_line_point.time = units::time::second_t(2.0);
    poly_line.emplace_back(poly_line_point);

    trajectory.type = poly_line;

    std::vector<mantle_api::Orientation3<units::angle::radian_t>> expected_orientation;
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{0_rad, 0_rad, 0_rad});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}, units::angle::radian_t{M_PI_4}});
    expected_orientation.push_back(mantle_api::Orientation3<units::angle::radian_t>{
        units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}, units::angle::radian_t{M_PI_2}});

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    FollowTrajectoryWithSpeedControlUnit trajectory_controller(&lane_location_provider_, trajectory, time_reference_);
    trajectory_controller.SetEntity(*entity);

    trajectory_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_orientation[0], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_orientation[1], entity->GetOrientation());
    trajectory_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(expected_orientation[2], entity->GetOrientation());
}

}  // namespace astas::environment::controller
