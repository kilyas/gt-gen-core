/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYWITHSPEEDCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYWITHSPEEDCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/Utilities/controller_helpers.h"
#include "Core/Environment/LaneFollowing/point_list_traverser.h"

namespace astas::environment::controller
{

class FollowTrajectoryWithSpeedControlUnit : public IAbstractControlUnit
{
  public:
    /// @brief Create a follow trajectory with a pre-defined trajectory.
    /// @param map_query_service Query service containing a pointer to the AstasMap.
    /// @param trajectory Defines the trajectory to travel indicating at which time should be pass at each point
    /// @param time_reference Defines the time reference to be applied in the trajectory
    /// @throws EnvironmentException Throws when trajectory is empty.
    explicit FollowTrajectoryWithSpeedControlUnit(
        mantle_api::ILaneLocationQueryService* map_query_service,
        const mantle_api::Trajectory& trajectory,
        const mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference);

    FollowTrajectoryWithSpeedControlUnit(
        const FollowTrajectoryWithSpeedControlUnit& follow_trajectory_with_speed_control_unit);
    FollowTrajectoryWithSpeedControlUnit(FollowTrajectoryWithSpeedControlUnit&&) = default;
    FollowTrajectoryWithSpeedControlUnit& operator=(const FollowTrajectoryWithSpeedControlUnit&) = delete;
    FollowTrajectoryWithSpeedControlUnit& operator=(FollowTrajectoryWithSpeedControlUnit&&) = delete;
    ~FollowTrajectoryWithSpeedControlUnit() override = default;

    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    bool EntityNeedsToBeUpdated();
    void CalculateCurrentOrientation();
    void CalculateNextVelocityAndAcceleration();
    std::optional<std::size_t> GetSimulationTimerIndexInPolyline();
    mantle_api::Time GetTimeInTrajectoryPointList(const std::size_t index);

    void UpdateEntity();
    void UpdateTraverser();
    void SetStartSimulationTime();
    void SetEntityVelocityAndAcceleration();

    const map::LaneLocationProvider* lane_location_provider_;
    mantle_api::Trajectory trajectory_{};

    mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference_{};
    std::unique_ptr<lanefollowing::PointListTraverser> traverser_;
    std::unique_ptr<ControllerHelpers> controller_helpers_;

    units::velocity::meters_per_second_t current_velocity_{};
    units::acceleration::meters_per_second_squared_t current_acceleration_{};
    mantle_api::Orientation3<units::angle::radian_t> current_orientation_{};
    std::optional<mantle_api::Time> start_simulation_time_{};
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYWITHSPEEDCONTROLUNIT_H
