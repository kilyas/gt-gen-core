/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANEASSIGNMENTCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANEASSIGNMENTCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::controller
{

class LaneAssignmentControlUnit : public IAbstractControlUnit
{
  public:
    explicit LaneAssignmentControlUnit(mantle_api::ILaneLocationQueryService* map_query_service,
                                       bool is_entity_allowed_to_leave_lane);

    std::unique_ptr<IControlUnit> Clone() const override;
    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    const map::LaneLocationProvider* lane_location_provider_;
    bool is_entity_allowed_to_leave_lane_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANEASSIGNMENTCONTROLUNIT_H
