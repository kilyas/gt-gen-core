/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANEOFFSETCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANEOFFSETCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::controller
{

class LaneOffsetControlUnit : public IAbstractControlUnit
{
  public:
    /// Create a LaneOffsetControlUnit with lane offset splines. The control unit will finish once the
    /// last spline has been followed.
    /// \param lateral_lane_offsets Defines the lane offsets to follow over time.
    explicit LaneOffsetControlUnit(
        const std::vector<mantle_api::SplineSection<units::length::meter>>& lateral_lane_offsets);
    /// Create a LaneOffsetControlUnit without lane offset splines. On the first step the control unit will calculate
    /// the current lane offset of the entity and keep it infinitely.
    /// \param map_query_service Query service containing a pointer to the AstasMap.
    explicit LaneOffsetControlUnit(mantle_api::ILaneLocationQueryService* map_query_service);

    std::unique_ptr<IControlUnit> Clone() const override;
    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    void SetLocalOrientationScalars(const mantle_api::Time& time);
    bool IsInvalidLateralOffset(const mantle_api::Time& time) const;

    std::vector<mantle_api::SplineSection<units::length::meter>> lateral_lane_offsets_;
    bool first_step_{true};
    const map::LaneLocationProvider* lane_location_provider_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANEOFFSETCONTROLUNIT_H
