/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_VELOCITYCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_VELOCITYCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"

#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/time_utils.h>

#include <vector>

namespace astas::environment::controller
{

class VelocityControlUnit : public IAbstractControlUnit
{
  public:
    explicit VelocityControlUnit(
        std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines,
        units::velocity::meters_per_second_t default_velocity = units::velocity::meters_per_second_t{0.0});

    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    units::velocity::meters_per_second_t GetCurrentVelocity(const mantle_api::Time& time) const;
    units::acceleration::meters_per_second_squared_t GetCurrentAcceleration(const mantle_api::Time& time) const;
    mantle_api::Time GetSplineTime() const;

    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines_;
    units::velocity::meters_per_second_t default_velocity_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_VELOCITYCONTROLUNIT_H
