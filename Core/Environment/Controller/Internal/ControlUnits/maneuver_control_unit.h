/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_MANEUVERCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_MANEUVERCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::controller
{

class ManeuverControlUnit : public IAbstractControlUnit
{
  public:
    ManeuverControlUnit(std::vector<mantle_api::SplineSection<units::angle::radian>> splines,
                        mantle_api::ILaneLocationQueryService* map_query_service);

    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    void UpdateOrientation(mantle_api::Time time);
    void UpdatePosition(std::size_t step_index);
    void UpdateEntity();
    mantle_api::Orientation3<units::angle::radian_t> GetOrientationVector();
    std::vector<mantle_api::SplineSection<units::angle::radian>> splines_;
    const map::LaneLocationProvider* lane_location_provider_;
    units::angle::radian_t heading_{0.0};
    mantle_api::Vec3<units::length::meter_t> incremental_position_change_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_MANEUVERCONTROLUNIT_H
