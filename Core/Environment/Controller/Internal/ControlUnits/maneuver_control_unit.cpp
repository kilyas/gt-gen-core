/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/maneuver_control_unit.h"

#include "Core/Environment/Controller/Internal/Utilities/spline_utilities.h"
#include "Core/Environment/Controller/Internal/Utilities/vector_utilities.h"
#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;

ManeuverControlUnit::ManeuverControlUnit(std::vector<mantle_api::SplineSection<units::angle::radian>> splines,
                                         mantle_api::ILaneLocationQueryService* map_query_service)
    : splines_{std::move(splines)}, lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)}
{
    originating_control_strategy_ = mantle_api::ControlStrategyType::kFollowHeadingSpline;
}

std::unique_ptr<IControlUnit> ManeuverControlUnit::Clone() const
{
    return std::make_unique<ManeuverControlUnit>(*this);
}

void ManeuverControlUnit::StepControlUnit()
{
    ASTAS_PROFILE_SCOPE

    incremental_position_change_ = {0.0_m, 0.0_m, 0.0_m};

    auto sample_time = (last_simulation_time_ - spawn_time_) + sub_sample_increment;
    if (current_simulation_time_ == last_simulation_time_)
    {
        sample_time = last_simulation_time_ - spawn_time_;
    }

    // Depending on the step_size the calculation of the position might be inaccurate. Imagine the heading spline
    // describes driving a curve, however due to a big step_size the sampling happens at the beginning and the end of
    // the curve. In that case the position is computed based on the line between these two points. However, the actual
    // curve was driven. Therefore the sampling must be done in very small steps, such that the curve is represented in
    // many very small "lines", and the final position is as the curve was followed.
    std::size_t index_counter = 0;
    while (sample_time <= current_simulation_time_ - spawn_time_)
    {
        UpdateOrientation(sample_time);

        if (current_simulation_time_ != last_simulation_time_)
        {
            UpdatePosition(index_counter);
        }

        sample_time += sub_sample_increment;
        index_counter++;
    }

    UpdateEntity();
}

void ManeuverControlUnit::UpdateOrientation(mantle_api::Time time)
{
    if (IsSplineEmptyOrTimeOutOfSplineRange(splines_, time))
    {
        data_exchange_container_->orientation_rate_scalar = 0_rad_per_s;
        heading_ = 0_rad;
    }
    else
    {
        auto current_spline_index = GetSplineIndexForTime(time, splines_);
        data_exchange_container_->orientation_rate_scalar = units::angular_velocity::radians_per_second_t(
            EvaluateSplineDerivative(time, splines_[current_spline_index]));
        heading_ = units::angle::radian_t{EvaluateSpline(time, splines_[current_spline_index])};
    }
}

void ManeuverControlUnit::UpdatePosition(std::size_t step_index)
{
    const auto new_vehicle_orientation = GetOrientationVector();
    const auto new_velocity = GetVelocityVector(
        heading_, new_vehicle_orientation.pitch, data_exchange_container_->velocity_scalars.at(step_index));

    const mantle_api::Vec3<units::length::meter_t> moved_distance{new_velocity.x * sub_sample_increment,
                                                                  new_velocity.y * sub_sample_increment,
                                                                  new_velocity.z * sub_sample_increment};

    incremental_position_change_ = incremental_position_change_ + moved_distance;
}

void ManeuverControlUnit::UpdateEntity()
{
    const auto new_vehicle_orientation = GetOrientationVector();
    const auto new_velocity_vector =
        GetVelocityVector(heading_, new_vehicle_orientation.pitch, data_exchange_container_->velocity_scalars.back());

    entity_->SetPosition(entity_->GetPosition() + incremental_position_change_);
    entity_->SetVelocity(new_velocity_vector);
    entity_->SetAcceleration(
        GetAccelerationVector(heading_, new_vehicle_orientation.pitch, data_exchange_container_->acceleration_scalar));

    SetEntityOrientationProperties(new_vehicle_orientation);
}

mantle_api::Orientation3<units::angle::radian_t> ManeuverControlUnit::GetOrientationVector()
{
    const auto lane_location = lane_location_provider_->GetLaneLocation(entity_->GetPosition());
    if (lane_location.IsValid())
    {
        auto orientation = lane_location.GetOrientation();
        orientation.yaw = heading_;
        return orientation;
    }
    else
    {
        // keep on moving with pitch and roll from the last valid lane location
        auto orientation = entity_->GetOrientation();
        orientation.yaw = heading_;
        return orientation;
    }
}

bool ManeuverControlUnit::HasFinished() const
{
    return splines_.empty() || (last_simulation_time_ - spawn_time_) > splines_.back().end_time;
}

}  // namespace astas::environment::controller
