/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/keep_velocity_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_mps;

/// clone tests
TEST(KeepVelocityControlUnitTest, GivenKeepVelocityControlUnit_WhenClone_ThenCopyCreated)
{
    auto control_unit_ptr = std::make_unique<KeepVelocityControlUnit>();

    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

/// step tests
TEST(KeepVelocityControlUnitTest,
     GivenEntityWithDefaultVelocity_WhenInitialStep_ThenDefaultVelocityIsSetToVelocityScalar)
{
    KeepVelocityControlUnit keep_velocity_control_unit{};
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetVelocity({5_mps, 0_mps, 0_mps});

    ControllerDataExchangeContainer data_exchange_container{};
    keep_velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    keep_velocity_control_unit.SetEntity(*entity);

    keep_velocity_control_unit.Step(mantle_api::Time{0});

    EXPECT_EQ(5.0_mps, data_exchange_container.velocity_scalars.back());
}

TEST(KeepVelocityControlUnitTest, GivenEntityWithNoDefaultVelocity_WhenInitialStep_ThenVelocityScalarIsZero)
{
    KeepVelocityControlUnit keep_velocity_control_unit{};
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");

    ControllerDataExchangeContainer data_exchange_container{};
    keep_velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    keep_velocity_control_unit.SetEntity(*entity);

    keep_velocity_control_unit.Step(mantle_api::Time{0});

    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());
}

TEST(KeepVelocityControlUnitTest, GivenEntityWithChangedVelocity_WhenMultipleStep_ThenEntityVelocityIsUsedInEachStep)
{
    KeepVelocityControlUnit keep_velocity_control_unit{};
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetVelocity({5.0_mps, 0.0_mps, 0.0_mps});

    ControllerDataExchangeContainer data_exchange_container{};
    keep_velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    keep_velocity_control_unit.SetEntity(*entity);

    keep_velocity_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(5.0_mps, data_exchange_container.velocity_scalars.back());

    entity->SetVelocity({20.0_mps, 0.0_mps, 0.0_mps});
    keep_velocity_control_unit.SetEntity(*entity);
    keep_velocity_control_unit.Step(mantle_api::Time{2});
    EXPECT_EQ(20.0_mps, data_exchange_container.velocity_scalars.back());
}

TEST(KeepVelocityControlUnitTest,
     GivenEntityWithVelocity_WhenControllerIsStepped100Times_Then100VelocityScalarsAreCalculated)
{
    KeepVelocityControlUnit keep_velocity_control_unit{};
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetVelocity({5.0_mps, 0.0_mps, 0.0_mps});

    ControllerDataExchangeContainer data_exchange_container{};
    keep_velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    keep_velocity_control_unit.SetEntity(*entity);

    keep_velocity_control_unit.Step(mantle_api::Time{0});
    EXPECT_TRUE(data_exchange_container.velocity_scalars.size() == 1);

    data_exchange_container.Reset();
    keep_velocity_control_unit.Step(mantle_api::Time{100});
    EXPECT_TRUE(data_exchange_container.velocity_scalars.size() == 100);
}

/// has finished tests
TEST(KeepVelocityControlUnitTest, GivenKeepVelocityControlUnit_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    KeepVelocityControlUnit control_unit{};

    EXPECT_FALSE(control_unit.HasFinished());
}

}  // namespace astas::environment::controller
