/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/LaneFollowing/line_follow_data_from_lanes.h"
#include "Core/Environment/PathFinding/path_finder.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/MantleApiExtension/formatting.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/math_utils.h"
#include "Core/Service/Utility/position_utils.h"

#include <MantleAPI/Common/orientation.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;

PathControlUnit::PathControlUnit(mantle_api::ILaneLocationQueryService* map_query_service)
    : lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)},
      controller_helpers_{std::make_unique<ControllerHelpers>(lane_location_provider_)}

{
}

PathControlUnit::PathControlUnit(mantle_api::ILaneLocationQueryService* map_query_service,
                                 const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints)
    : lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)},
      controller_helpers_{std::make_unique<ControllerHelpers>(lane_location_provider_)}
{
    traverser_ = std::make_unique<lanefollowing::PointListTraverser>(
        controller_helpers_->ComputePointDistanceListFromPath(waypoints));
}

PathControlUnit::PathControlUnit(const PathControlUnit& path_control_unit)
    : IAbstractControlUnit(path_control_unit),
      lane_location_provider_{path_control_unit.lane_location_provider_},
      controller_helpers_{std::make_unique<ControllerHelpers>(lane_location_provider_)}
{
    if (path_control_unit.traverser_ != nullptr)
    {
        traverser_ = std::make_unique<lanefollowing::PointListTraverser>(*path_control_unit.traverser_);
    }
}

std::unique_ptr<IControlUnit> PathControlUnit::Clone() const
{
    return std::make_unique<PathControlUnit>(*this);
}

void PathControlUnit::InitControlUnit()
{
    /// If no waypoint list was provided in construction, assume lane keeping from current position as desired
    /// behaviour
    if (traverser_ == nullptr)
    {
        traverser_ = std::make_unique<lanefollowing::PointListTraverser>(
            controller_helpers_->ComputePointDistanceListFromPath({entity_->GetPosition()}));
    }
}

void PathControlUnit::StepControlUnit()
{
    ASTAS_PROFILE_SCOPE
    units::length::meter_t distance_to_move{0.0};

    current_lane_offset_ = data_exchange_container_->lane_offset_scalars.empty()
                               ? 0.0_m
                               : data_exchange_container_->lane_offset_scalars.back();

    if (HasEntityBeenModifiedOutsideOfControlUnit())
    {
        traverser_ = std::make_unique<lanefollowing::PointListTraverser>(
            controller_helpers_->ComputePointDistanceListFromPath({entity_->GetPosition()}));
    }
    const auto longitudinal_distance = data_exchange_container_->velocity_scalars.empty()
                                           ? 0.0_m
                                           : data_exchange_container_->velocity_scalars.back() * delta_time_;
    const auto lateral_distance = current_lane_offset_ - last_entity_lane_offset_;
    distance_to_move = units::length::meter_t(
        service::utility::CalculateOtherLegInARectangledTriangle(longitudinal_distance(), lateral_distance()));
    traverser_->Move(distance_to_move);

    UpdateEntity(distance_to_move);

    last_entity_lane_offset_ = data_exchange_container_->lane_offset_scalars.empty()
                                   ? 0.0_m
                                   : data_exchange_container_->lane_offset_scalars.back();
}

void PathControlUnit::UpdateEntity(units::length::meter_t distance_to_move)
{
    auto new_position = traverser_->GetPosition();

    if (traverser_->GetAdditionalDistance() > 0.0_m && !traverser_->IsLastPointReached())
    {
        const auto forward = service::glmwrapper::Normalize(traverser_->GetNextPoint() - new_position);
        const auto position_on_segment = forward * traverser_->GetAdditionalDistance()();
        new_position = new_position + position_on_segment;
    }

    if (lane_location_provider_ == nullptr)
    {
        throw EnvironmentException("PathControlUnit cannot update entity. Lane location provider is nullptr.");
    }

    // TODO: do this  and the vertical shift after the position was shifted laterally??
    auto lane_location = lane_location_provider_->GetLaneLocationById(traverser_->GetLaneId(), new_position);

    /// TODO: think about moving this to a separate control unit, which runs like the lane assignment
    new_position = service::utility::GetVerticalShiftedPosition(new_position, entity_->GetProperties()->bounding_box);

    if (!data_exchange_container_->lane_offset_scalars.empty())
    {
        new_position =
            service::utility::GetLateralShiftedPosition(new_position,
                                                        lane_location.direction,
                                                        lane_location.lane_normal,
                                                        data_exchange_container_->lane_offset_scalars.back());
    }

    entity_->SetPosition(new_position);

    SetEntityOrientationProperties(lane_location, distance_to_move);
    SetEntityVelocityAndAcceleration();
}

void PathControlUnit::SetEntityOrientationProperties(const map::LaneLocation& lane_location,
                                                     units::length::meter_t distance_to_move)
{
    /// Do not update the orientation if entity is not moving
    if (mantle_api::AlmostEqual(distance_to_move.value(), 0.0, MANTLE_API_DEFAULT_EPS, true))
    {
        using units::literals::operator""_rad_per_s;
        using units::literals::operator""_rad_per_s_sq;

        entity_->SetOrientationRate(mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>(
            0_rad_per_s, 0_rad_per_s, 0_rad_per_s));
        entity_->SetOrientationAcceleration(
            mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>(
                0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq));

        return;
    }

    units::angle::radian_t lane_offset_yaw{0.0};
    if (distance_to_move.value() > 0.0)
    {
        lane_offset_yaw = units::angle::radian_t{
            atan2((current_lane_offset_ - last_entity_lane_offset_).value(), distance_to_move.value())};
    }

    const auto lane_orientation = service::glmwrapper::ToQuaternion(
        service::glmwrapper::FromBasisVectors(traverser_->GetOrientation(), lane_location.lane_normal));

    using units::literals::operator""_rad;

    const auto offset_orientation{service::glmwrapper::ToQuaternion({lane_offset_yaw, 0_rad, 0_rad})};

    /// This needs to be done to take both the lane orientation (world space) and local entity orientation into account
    IAbstractControlUnit::SetEntityOrientationProperties(
        service::glmwrapper::FromQuaternion(lane_orientation * offset_orientation));
}

bool PathControlUnit::HasFinished() const
{
    return traverser_ != nullptr && traverser_->IsLastPointReached();
}

bool PathControlUnit::HasReachedEndOfRoadNetwork() const
{
    // If the traverser contains only one point, then the path was planned from the current position of the entity and
    // it was already at the end of the road network
    return traverser_ != nullptr && traverser_->IsLastPointReached() && traverser_->GetIndex() == 0;
}

}  // namespace astas::environment::controller
