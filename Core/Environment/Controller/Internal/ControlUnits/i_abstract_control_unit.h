/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_IABSTRACTCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_IABSTRACTCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_control_unit.h"
#include "Core/Environment/Controller/Internal/Utilities/vector_utilities.h"
#include "Core/Service/Utility/derivative_utils.h"

#include <MantleAPI/Traffic/i_entity.h>
#include <cxxabi.h>  // GCC and Clang specific

namespace astas::environment::controller
{

class IAbstractControlUnit : public IControlUnit
{
  public:
    std::string GetName() const override
    {
        int status{0};
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg) - OK
        char* type_name_buffer = abi::__cxa_demangle(typeid(*this).name(), nullptr, nullptr, &status);
        std::string type_name_with_namespace(type_name_buffer);
        free(type_name_buffer);  // NOLINT(cppcoreguidelines-no-malloc) - accepted
        auto pos = type_name_with_namespace.find_last_of(':');
        auto type_name = type_name_with_namespace.substr(++pos);
        return type_name;
    }

    virtual void StepControlUnit() = 0;

    virtual void InitControlUnit(){};

    void Step(mantle_api::Time current_simulation_time) override
    {
        current_simulation_time_ = current_simulation_time;

        if (initial_step_)
        {
            spawn_time_ = current_simulation_time;
            StorePreviousValues();
            InitControlUnit();
            initial_step_ = false;
        }

        delta_time_ = current_simulation_time - last_simulation_time_;

        StepControlUnit();
        StorePreviousValues();
    }

    void SetEntity(mantle_api::IEntity& entity) override { entity_ = &entity; }

    void SetControllerDataExchangeContainer(ControllerDataExchangeContainer& data_exchange_container) override
    {
        data_exchange_container_ = &data_exchange_container;
    }

    const mantle_api::ControlStrategyType& GetOriginatingControlStrategy() override
    {
        return originating_control_strategy_;
    }

    bool HasEntityBeenModifiedOutsideOfControlUnit() const
    {
        return entity_ != nullptr && last_entity_position_ != entity_->GetPosition();
    }

  protected:
    mantle_api::IEntity* entity_{nullptr};
    ControllerDataExchangeContainer* data_exchange_container_{nullptr};

    mantle_api::Time spawn_time_{0};
    mantle_api::Time last_simulation_time_{0};
    mantle_api::Time current_simulation_time_{0};
    units::time::second_t delta_time_{0};
    mantle_api::Vec3<units::length::meter_t> last_entity_position_{};
    mantle_api::Vec3<units::velocity::meters_per_second_t> last_entity_velocity_{};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> last_entity_acceleration_{};
    const mantle_api::Time sub_sample_increment{1};
    mantle_api::ControlStrategyType originating_control_strategy_{mantle_api::ControlStrategyType::kUndefined};
    bool initial_step_{true};

    void SetEntityVelocityAndAcceleration()
    {
        assert(entity_);
        assert(data_exchange_container_);

        using units::literals::operator""_mps_sq;

        const auto orientation{entity_->GetOrientation()};
        const auto velocity_vector{GetVelocityVector(orientation.yaw,
                                                     orientation.pitch,
                                                     data_exchange_container_->velocity_scalars.empty()
                                                         ? units::velocity::meters_per_second_t(0.0)
                                                         : data_exchange_container_->velocity_scalars.back())};
        const auto acceleration_vector{
            GetAccelerationVector(orientation.yaw, orientation.pitch, data_exchange_container_->acceleration_scalar)};

        entity_->SetVelocity(velocity_vector);
        entity_->SetAcceleration(acceleration_vector);

        last_entity_velocity_ = velocity_vector;
        last_entity_acceleration_ = acceleration_vector;
    }

    void SetEntityOrientationProperties(const mantle_api::Orientation3<units::angle::radian_t>& to_orientation)
    {
        using units::literals::operator""_s;

        assert(entity_);

        if (delta_time_ > 0.0_s)
        {
            const auto orientation_rate{
                service::utility::GetOrientationDerivativeVector<units::angle::radian_t,
                                                                 units::angular_velocity::radians_per_second_t>(
                    to_orientation, entity_->GetOrientation(), delta_time_)};
            const auto orientation_acceleration{service::utility::GetOrientationDerivativeVector<
                units::angular_velocity::radians_per_second_t,
                units::angular_acceleration::radians_per_second_squared_t>(
                orientation_rate, entity_->GetOrientationRate(), delta_time_)};

            entity_->SetOrientationRate(orientation_rate);
            entity_->SetOrientationAcceleration(orientation_acceleration);
        }

        entity_->SetOrientation(to_orientation);
    }

  private:
    void StorePreviousValues()
    {
        last_simulation_time_ = current_simulation_time_;

        if (entity_ != nullptr)
        {
            last_entity_position_ = entity_->GetPosition();
            last_entity_velocity_ = entity_->GetVelocity();
            last_entity_acceleration_ = entity_->GetAcceleration();
        }
    }
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_IABSTRACTCONTROLUNIT_H
