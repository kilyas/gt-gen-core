/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/FileSystem/filesystem.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/Logging/logging.h"
#include "astas_osi_groundtruth.pb.h"
#include "astas_osi_sensorview.pb.h"
#include "astas_osi_trafficupdate.pb.h"

#include <MantleAPI/Traffic/entity_helper.h>
#include <boost/dll/import.hpp>
#include <fmt/format.h>

#include <chrono>

namespace astas::environment::controller
{

std::unique_ptr<proto_groundtruth::SensorViewBuilder> InitSensorViewBuilder(
    const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config)
{
    auto sensor_view_builder = std::make_unique<proto_groundtruth::SensorViewBuilder>(
        *traffic_participant_control_unit_config.astas_map,
        traffic_participant_control_unit_config.proto_ground_truth_builder_config.step_size,
        traffic_participant_control_unit_config.map_chunking);
    sensor_view_builder->Init();

    return sensor_view_builder;
}

TrafficParticipantControlUnit::TrafficParticipantControlUnit(
    const std::map<std::string, std::string>& parameters,
    const TrafficParticipantControlUnitConfig& traffic_participant_control_unit_config)
    : traffic_participant_control_unit_config_{traffic_participant_control_unit_config}
{
    // Needed for dog
    if (parameters.find("velocity") != parameters.end())
    {
        desired_velocity_ = units::velocity::meters_per_second_t{std::stod(parameters.at("velocity"))};
    }

    sensor_view_builder_ = InitSensorViewBuilder(traffic_participant_control_unit_config_);

    // Dynamically load TPM shared library
    fs::path so_name = fmt::format("{}.so", traffic_participant_control_unit_config_.name);
    fs::path so_file_path =
        ResolvePluginsPath(so_name, traffic_participant_control_unit_config_.plugins_paths.elements);
    tpm_creator_ =
        boost::dll::import_alias<tpm_create_t>(so_file_path.c_str(), "CreateTpm", boost::dll::load_mode::rtld_now);
    tpm_ = tpm_creator_(parameters);
}

TrafficParticipantControlUnit::TrafficParticipantControlUnit(
    const TrafficParticipantControlUnit& traffic_participant_control_unit)
    : IAbstractControlUnit(traffic_participant_control_unit)
{
    this->traffic_participant_control_unit_config_ =
        traffic_participant_control_unit.traffic_participant_control_unit_config_;
    this->sensor_view_builder_ = InitSensorViewBuilder(traffic_participant_control_unit_config_);
    this->tpm_ = traffic_participant_control_unit.tpm_;
    this->desired_velocity_ = traffic_participant_control_unit.desired_velocity_;
    this->tpm_creator_ = traffic_participant_control_unit.tpm_creator_;
}

TrafficParticipantControlUnit::~TrafficParticipantControlUnit()
{
    tpm_.reset();
}

std::unique_ptr<IControlUnit> TrafficParticipantControlUnit::Clone() const
{
    return std::make_unique<TrafficParticipantControlUnit>(*this);
}

fs::path TrafficParticipantControlUnit::ResolvePluginsPath(fs::path& input_path,
                                                           const std::vector<std::string>& search_directories)
{
    std::vector<fs::path> search_dirs{};
    std::transform(search_directories.begin(),
                   search_directories.end(),
                   std::back_inserter(search_dirs),
                   [](const std::string& directory) { return fs::path(directory); });

    search_dirs.push_back(fs::current_path());

    service::file_system::ReplaceTildeWithAbsoluteHomeDirectoryPath(input_path);

    std::string not_found_files{};
    for (const auto& search_directory : search_dirs)
    {
        if (fs::exists(search_directory))
        {
            auto found_path = service::file_system::SearchRecursivelyInDirectory(input_path, search_directory);
            if (found_path.empty() || !fs::exists(found_path))
            {
                not_found_files.append(search_directory.string() + " (including all sub-folders)\n");
            }
            else
            {
                return fs::canonical(found_path);
            }
        }
    }

    throw EnvironmentException(
        "Input file path '{}' could not be resolved. The following locations have been considered:\n{}",
        input_path.string(),
        not_found_files);
}

void TrafficParticipantControlUnit::SetEntity(mantle_api::IEntity& entity)
{
    IAbstractControlUnit::SetEntity(entity);

    mantle_api::SetSpeed(entity_, desired_velocity_);
    Info("Entity {}, Initial TPM velocity: {}", entity_->GetUniqueId(), desired_velocity_.value());
}

void TrafficParticipantControlUnit::StepControlUnit()
{
    // Skip first cycle, because GT does not contain yet the controlled moving_object
    if (skip_first_cycle_)
    {
        skip_first_cycle_ = false;
        return;
    }

    const auto* entity_repository =
        traffic_participant_control_unit_config_.proto_ground_truth_builder_config.entity_repository;
    sensor_view_builder_->Step(entity_repository->GetEntities(), entity_);
    std::uint64_t raw_milliseconds{static_cast<std::uint64_t>(delta_time_() * 1e3)};
    auto traffic_update =
        tpm_->Update(std::chrono::milliseconds(raw_milliseconds), sensor_view_builder_->GetSensorView());

    UpdateEntity(traffic_update);

    const std::string map_coordinates{environment::map::GetMapCoordinateString(
        entity_->GetPosition(), traffic_participant_control_unit_config_.astas_map->coordinate_converter.get())};
    plausibility_check_.CheckPlausibility(*entity_, delta_time_, map_coordinates);
}

void TrafficParticipantControlUnit::UpdateEntity(const astas_osi3::TrafficUpdate& traffic_update)
{
    auto moving_object = traffic_update.update(0);
    auto base = moving_object.base();

    const auto& position = base.position();
    entity_->SetPosition(service::gt_conversion::ToVec3Length(position));

    const auto& orientation = base.orientation();
    entity_->SetOrientation(service::gt_conversion::ToOrientation3(orientation));

    const auto& orientation_rate = base.orientation_rate();
    entity_->SetOrientationRate(service::gt_conversion::ToOrientation3Rate(orientation_rate));

    const auto& orientation_acceleration = base.orientation_acceleration();
    entity_->SetOrientationAcceleration(service::gt_conversion::ToOrientation3Acceleration(orientation_acceleration));

    const auto& acceleration = base.acceleration();
    entity_->SetAcceleration(service::gt_conversion::ToVec3Acceleration(acceleration));

    const auto& velocity = base.velocity();
    entity_->SetVelocity(service::gt_conversion::ToVec3Velocity(velocity));
}

bool TrafficParticipantControlUnit::HasFinished() const
{
    return tpm_->HasFinished();
}

}  // namespace astas::environment::controller
