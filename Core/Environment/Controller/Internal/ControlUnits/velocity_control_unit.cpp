/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"

#include "Core/Environment/Controller/Internal/Utilities/spline_utilities.h"
#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::controller
{

VelocityControlUnit::VelocityControlUnit(
    std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> velocity_splines,
    units::velocity::meters_per_second_t default_velocity)
    : velocity_splines_{std::move(velocity_splines)}, default_velocity_{default_velocity}
{
    originating_control_strategy_ = mantle_api::ControlStrategyType::kFollowVelocitySpline;
}

std::unique_ptr<IControlUnit> VelocityControlUnit::Clone() const
{
    return std::make_unique<VelocityControlUnit>(*this);
}

mantle_api::Time VelocityControlUnit::GetSplineTime() const
{
    return current_simulation_time_ - spawn_time_;
}

void VelocityControlUnit::StepControlUnit()
{
    ASTAS_PROFILE_SCOPE

    auto sample_time = last_simulation_time_ + sub_sample_increment;
    if (current_simulation_time_ == last_simulation_time_)
    {
        sample_time = last_simulation_time_;
    }

    while (sample_time <= current_simulation_time_)
    {
        auto spline_time = GetSplineTime();

        data_exchange_container_->velocity_scalars.push_back(GetCurrentVelocity(spline_time));
        sample_time += sub_sample_increment;
    }

    data_exchange_container_->acceleration_scalar = GetCurrentAcceleration(GetSplineTime());
}

bool VelocityControlUnit::HasFinished() const
{
    return velocity_splines_.empty() || GetSplineTime() > velocity_splines_.back().end_time;
}

units::velocity::meters_per_second_t VelocityControlUnit::GetCurrentVelocity(const mantle_api::Time& time) const
{
    if (IsSplineEmptyOrTimeOutOfSplineRange(velocity_splines_, time))
    {
        return default_velocity_;
    }

    auto current_velocity_index = GetSplineIndexForTime(time, velocity_splines_);
    return units::velocity::meters_per_second_t{EvaluateSpline(time, velocity_splines_[current_velocity_index])};
}

units::acceleration::meters_per_second_squared_t VelocityControlUnit::GetCurrentAcceleration(
    const mantle_api::Time& time) const
{
    if (IsSplineEmptyOrTimeOutOfSplineRange(velocity_splines_, time))
    {
        return units::acceleration::meters_per_second_squared_t{0.0};
    }

    auto current_velocity_index = GetSplineIndexForTime(time, velocity_splines_);
    return units::acceleration::meters_per_second_squared_t{
        EvaluateSplineDerivative(time, velocity_splines_[current_velocity_index])};
}

}  // namespace astas::environment::controller
