/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/no_op_control_unit.h"

namespace astas::environment::controller
{
std::unique_ptr<IControlUnit> NoOpControlUnit::Clone() const
{
    return std::make_unique<NoOpControlUnit>(*this);
}

void NoOpControlUnit::StepControlUnit() {}

bool NoOpControlUnit::HasFinished() const
{
    return false;
}

}  // namespace astas::environment::controller
