/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_ICONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_ICONTROLUNIT_H

#include "Core/Environment/Controller/controller_data_exchange_container.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <memory>

namespace astas::environment::controller
{

class IControlUnit
{
  public:
    IControlUnit() = default;
    virtual ~IControlUnit() = default;
    IControlUnit(IControlUnit const&) = default;
    IControlUnit& operator=(const IControlUnit&) = default;
    IControlUnit(IControlUnit&&) = delete;
    IControlUnit& operator=(IControlUnit&&) = delete;

    virtual std::string GetName() const = 0;

    virtual std::unique_ptr<IControlUnit> Clone() const = 0;

    virtual void Step(mantle_api::Time current_simulation_time) = 0;

    virtual bool HasFinished() const = 0;

    virtual void SetEntity(mantle_api::IEntity& entity) = 0;

    virtual void SetControllerDataExchangeContainer(ControllerDataExchangeContainer& data_exchange_container) = 0;

    virtual const mantle_api::ControlStrategyType& GetOriginatingControlStrategy() = 0;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_ICONTROLUNIT_H
