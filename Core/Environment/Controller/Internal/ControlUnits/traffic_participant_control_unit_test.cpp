/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"

#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/FileSystem/file_system_utils.h"
#include "Core/Service/Utility/clock.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/ProtoUtils/proto_utilities.h"
#include "astas_osi_groundtruth.pb.h"

#include <gtest/gtest.h>

namespace astas::environment::control_unit
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_rad;

class MockTpmControlUnit : public controller::TrafficParticipantControlUnit
{
  public:
    using controller::TrafficParticipantControlUnit::TrafficParticipantControlUnit;

    void UpdateEntity(const astas_osi3::TrafficUpdate& traffic_update)
    {
        controller::TrafficParticipantControlUnit::UpdateEntity(traffic_update);
    }

    mantle_api::IEntity* GetEntity() { return entity_; }
};

class TrafficParticipantControlUnitTest : public testing::Test
{
  public:
    controller::TrafficParticipantControlUnitConfig CreateTrafficParticipantControlConfig()
    {
        controller::TrafficParticipantControlUnitConfig tpm_control_unit_config{};
        astas_map = std::move(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());
        tpm_control_unit_config.astas_map = astas_map.get();
        tpm_control_unit_config.name = "traffic_participant_model_example";

        return tpm_control_unit_config;
    }

    std::unique_ptr<mantle_api::IEntity> CreateTpmEntity(mantle_api::UniqueId id)
    {
        std::unique_ptr<mantle_api::IEntity> entity =
            std::make_unique<entities::VehicleEntity>(id, "traffic_participant_model_example");
        entity->SetOrientation({0_rad, 0_rad, 0_rad});

        auto vehicle_properties = std::make_unique<mantle_api::VehicleProperties>();
        vehicle_properties->bounding_box.dimension = {2_m, 2_m, 1.5_m};
        entity->SetProperties(std::move(vehicle_properties));
        entity->SetAssignedLaneIds({1});

        return entity;
    }

    astas_osi3::TrafficUpdate CreateTrafficUpdate(mantle_api::IEntity* entity)
    {
        astas_osi3::TrafficUpdate traffic_update;
        auto* moving_object = traffic_update.add_update();

        moving_object->mutable_id()->set_value(entity->GetUniqueId());
        moving_object->set_type(astas_osi3::MovingObject::TYPE_VEHICLE);
        moving_object->mutable_base()->mutable_position()->set_x(entity->GetPosition().x());
        moving_object->mutable_base()->mutable_position()->set_y(entity->GetPosition().y());
        moving_object->mutable_base()->mutable_position()->set_z(entity->GetPosition().z());

        moving_object->mutable_base()->mutable_velocity()->set_x(entity->GetVelocity().x());
        moving_object->mutable_base()->mutable_velocity()->set_y(entity->GetVelocity().y());
        moving_object->mutable_base()->mutable_velocity()->set_z(entity->GetVelocity().z());

        moving_object->mutable_base()->mutable_orientation()->set_yaw(entity->GetOrientation().yaw());
        moving_object->mutable_base()->mutable_orientation()->set_pitch(entity->GetOrientation().pitch());
        moving_object->mutable_base()->mutable_orientation()->set_roll(entity->GetOrientation().roll());

        moving_object->mutable_base()->mutable_orientation_rate()->set_yaw(entity->GetOrientationRate().yaw());
        moving_object->mutable_base()->mutable_orientation_rate()->set_pitch(entity->GetOrientationRate().pitch());
        moving_object->mutable_base()->mutable_orientation_rate()->set_roll(entity->GetOrientationRate().roll());

        moving_object->mutable_base()->mutable_orientation_acceleration()->set_yaw(
            entity->GetOrientationAcceleration().yaw());
        moving_object->mutable_base()->mutable_orientation_acceleration()->set_pitch(
            entity->GetOrientationAcceleration().pitch());
        moving_object->mutable_base()->mutable_orientation_acceleration()->set_roll(
            entity->GetOrientationAcceleration().roll());

        mantle_api::Dimension3& dimension = entity->GetProperties()->bounding_box.dimension;
        moving_object->mutable_base()->mutable_dimension()->set_length(dimension.length());
        moving_object->mutable_base()->mutable_dimension()->set_width(dimension.width());
        moving_object->mutable_base()->mutable_dimension()->set_height(dimension.height());

        return traffic_update;
    }

    std::map<std::string, std::string> tpm_params{{"velocity", "22"}};
    std::unique_ptr<map::AstasMap> astas_map;
};

TEST_F(TrafficParticipantControlUnitTest, GivenEntityNotInGroundTruth_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    controller::TrafficParticipantControlUnit control_unit{tpm_params, CreateTrafficParticipantControlConfig()};

    EXPECT_FALSE(control_unit.HasFinished());
}

TEST_F(TrafficParticipantControlUnitTest, GivenTpmControlUnit_WhenClone_ThenCopyCreated)
{
    auto control_unit_ptr = std::make_unique<controller::TrafficParticipantControlUnit>(
        tpm_params, CreateTrafficParticipantControlConfig());
    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenVehicleInGroundTruthAndEntityIsSet_WhenStepControlUnit_ThenNoThrownException)
{
    auto entity = CreateTpmEntity(1);
    controller::TrafficParticipantControlUnit control_unit{tpm_params, CreateTrafficParticipantControlConfig()};

    control_unit.SetEntity(*entity);

    EXPECT_NO_THROW(control_unit.Step(mantle_api::Time{30}));
}

TEST_F(TrafficParticipantControlUnitTest, GivenVehicleInGroundTruth_WhenStepControlUnit_ThenHasNotFinished)
{
    auto entity = CreateTpmEntity(1);
    controller::TrafficParticipantControlUnit control_unit{tpm_params, CreateTrafficParticipantControlConfig()};
    control_unit.SetEntity(*entity);

    control_unit.Step(mantle_api::Time{30});

    EXPECT_FALSE(control_unit.HasFinished());
}

TEST_F(TrafficParticipantControlUnitTest, GivenMovingObject_WhenUpdateEntity_ThenEntityIsEqualToMovingObject)
{
    auto ego_entity = CreateTpmEntity(1);

    MockTpmControlUnit control_unit{tpm_params, CreateTrafficParticipantControlConfig()};

    control_unit.SetEntity(*ego_entity);

    auto traffic_update = CreateTrafficUpdate(ego_entity.get());
    auto moving_object = traffic_update.update(0);

    moving_object.mutable_base()->mutable_position()->set_y(ego_entity->GetPosition().y() + 10);

    control_unit.UpdateEntity(traffic_update);

    auto updated_entity = control_unit.GetEntity();

    EXPECT_EQ(updated_entity->GetPosition().y(), ego_entity->GetPosition().y());  // EXTEND ME
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenEgoVehicleAtSimulationTimeZero_WhenStepControlUnit_ThenEntityIsNotChanged)
{
    auto ego_entity = CreateTpmEntity(1);
    ego_entity->SetVelocity({22_mps, 0_mps, 0_mps});
    MockTpmControlUnit control_unit{tpm_params, CreateTrafficParticipantControlConfig()};
    control_unit.SetEntity(*ego_entity);

    control_unit.StepControlUnit();

    auto entity = control_unit.GetEntity();
    EXPECT_EQ(entity->GetPosition().x(), 0);
}

TEST_F(TrafficParticipantControlUnitTest,
       GivenEgoVehicleWithDesiredTargetVelocity_WhenStepControlUnit_ThenOnlyPositionIsUpdated)
{
    service::utility::UniqueIdProvider unique_id_provider{};
    api::EntityRepository entity_repository{&unique_id_provider};

    mantle_api::VehicleProperties vehicle_properties{};
    vehicle_properties.bounding_box.dimension = {2_m, 2_m, 1.5_m};
    vehicle_properties.is_host = true;
    auto& ego_entity = entity_repository.Create(mantle_api::UniqueId{1}, "TPM", vehicle_properties);
    ego_entity.SetAssignedLaneIds({3});
    ego_entity.SetVelocity({22_mps, 0_mps, 0_mps});

    controller::TrafficParticipantControlUnitConfig tpm_control_unit_config{};
    auto astas_map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    tpm_control_unit_config.astas_map = astas_map.get();
    tpm_control_unit_config.proto_ground_truth_builder_config.entity_repository = &entity_repository;
    tpm_control_unit_config.name = "traffic_participant_model_example";
    MockTpmControlUnit control_unit{tpm_params, tpm_control_unit_config};
    control_unit.SetEntity(ego_entity);
    control_unit.Step(mantle_api::Time{0});  // Initial step will be always skipped
    control_unit.Step(mantle_api::Time{30});

    auto entity = control_unit.GetEntity();
    EXPECT_NE(entity->GetPosition().x(), 0);
}

}  // namespace astas::environment::control_unit
