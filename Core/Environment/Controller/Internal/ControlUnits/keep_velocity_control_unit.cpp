/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/keep_velocity_control_unit.h"

#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::controller
{

KeepVelocityControlUnit::KeepVelocityControlUnit()
{
    originating_control_strategy_ = mantle_api::ControlStrategyType::kKeepVelocity;
}

std::unique_ptr<IControlUnit> KeepVelocityControlUnit::Clone() const
{
    return std::make_unique<KeepVelocityControlUnit>(*this);
}

void KeepVelocityControlUnit::StepControlUnit()
{
    ASTAS_PROFILE_SCOPE
    auto sample_time = last_simulation_time_ + sub_sample_increment;
    if (current_simulation_time_ == last_simulation_time_)
    {
        sample_time = last_simulation_time_;
    }

    while (sample_time <= current_simulation_time_)
    {
        data_exchange_container_->velocity_scalars.push_back(entity_->GetVelocity().Length());
        sample_time += sub_sample_increment;
    }
}

bool KeepVelocityControlUnit::HasFinished() const
{
    return false;
}

}  // namespace astas::environment::controller
