/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_light_control_unit.h"

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/Exception/exception.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{

std::vector<mantle_api::TrafficLightPhase> CreateTrafficLightPhases()
{
    std::vector<mantle_api::TrafficLightPhase> traffic_light_phases{};

    mantle_api::TrafficLightPhase light_phase_1{};
    light_phase_1.start_time = mantle_api::Time(0);
    light_phase_1.end_time = mantle_api::Time(5'000);
    light_phase_1.bulb_states.push_back(
        {mantle_api::TrafficLightBulbColor::kRed, mantle_api::TrafficLightBulbMode::kFlashing});
    light_phase_1.bulb_states.push_back(
        {mantle_api::TrafficLightBulbColor::kGreen, mantle_api::TrafficLightBulbMode::kOff});

    mantle_api::TrafficLightPhase light_phase_2{};
    light_phase_2.start_time = mantle_api::Time(5'000);
    light_phase_2.end_time = mantle_api::Time(10'000);
    light_phase_2.bulb_states.push_back(
        {mantle_api::TrafficLightBulbColor::kRed, mantle_api::TrafficLightBulbMode::kOff});
    light_phase_2.bulb_states.push_back(
        {mantle_api::TrafficLightBulbColor::kGreen, mantle_api::TrafficLightBulbMode::kConstant});

    traffic_light_phases.push_back(light_phase_1);
    traffic_light_phases.push_back(light_phase_2);

    return traffic_light_phases;
}

void AddPropertiesToTrafficLightEntity(entities::TrafficLightEntity& traffic_light_entity)
{
    mantle_ext::TrafficLightProperties traffic_light_properties;

    mantle_ext::TrafficLightBulbProperties red_bulb;
    red_bulb.color = mantle_api::TrafficLightBulbColor::kRed;
    red_bulb.icon = osi::OsiTrafficLightIcon::kNone;
    red_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

    mantle_ext::TrafficLightBulbProperties green_bulb;
    green_bulb.color = mantle_api::TrafficLightBulbColor::kGreen;
    green_bulb.icon = osi::OsiTrafficLightIcon::kNone;
    green_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

    traffic_light_properties.bulbs.push_back(red_bulb);
    traffic_light_properties.bulbs.push_back(green_bulb);

    traffic_light_entity.SetProperties(
        std::make_unique<mantle_ext::TrafficLightProperties>(std::move(traffic_light_properties)));
}

TEST(TrafficLightControlUnitTest, GivenTrafficLightControlUnit_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    TrafficLightControlUnit controller{{}, false};

    EXPECT_FALSE(controller.HasFinished());
}

TEST(TrafficLightControlUnitTest, GivenTrafficLightControlUnit_WhenClone_ThenCopyCreated)
{
    auto controller_ptr =
        std::make_unique<TrafficLightControlUnit>(std::vector<mantle_api::TrafficLightPhase>{}, false);

    auto copy = controller_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(controller_ptr.get(), copy.get());
}

TEST(TrafficLightControlUnitTest, GivenTrafficLightControlUnit_WhenSetEntityWithTrafficLightEntity_ThenDoesNotThrow)
{
    auto traffic_light_phases = CreateTrafficLightPhases();
    auto controller_ptr = std::make_unique<TrafficLightControlUnit>(traffic_light_phases, false);

    auto traffic_light_entity = entities::TrafficLightEntity{1, "TrafficLightEntity"};
    AddPropertiesToTrafficLightEntity(traffic_light_entity);

    EXPECT_NO_THROW(controller_ptr->SetEntity(traffic_light_entity));
}

TEST(TrafficLightControlUnitTest, GivenTrafficLightControlUnit_WhenSetEntityWithNonTrafficLightEntity_ThenThrows)
{
    auto controller_ptr =
        std::make_unique<TrafficLightControlUnit>(std::vector<mantle_api::TrafficLightPhase>{}, false);

    auto non_traffic_light_entity = entities::PedestrianEntity{1, "PedestrianEntity"};

    EXPECT_THROW(controller_ptr->SetEntity(non_traffic_light_entity), EnvironmentException);
}

TEST(TrafficLightControlUnitTest,
     GivenTrafficLightControlUnit_WhenSetEntityWithTrafficLightEntity_ThenEntityPropertiesAreSetToFirstState)
{
    auto traffic_light_phases = CreateTrafficLightPhases();
    auto controller_ptr = std::make_unique<TrafficLightControlUnit>(traffic_light_phases, false);

    auto traffic_light_entity = entities::TrafficLightEntity{1, "TrafficLightEntity"};
    AddPropertiesToTrafficLightEntity(traffic_light_entity);

    controller_ptr->SetEntity(traffic_light_entity);

    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(0).color,
              traffic_light_entity.GetProperties()->bulbs.at(0).color);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(1).color,
              traffic_light_entity.GetProperties()->bulbs.at(1).color);
}

TEST(TrafficLightControlUnitTest, GivenTrafficLightControlUnit_WhenStepped_ThenEntityPropertiesAreSetToSecondState)
{
    auto traffic_light_phases = CreateTrafficLightPhases();
    auto controller_ptr = std::make_unique<TrafficLightControlUnit>(traffic_light_phases, false);

    auto traffic_light_entity = entities::TrafficLightEntity{1, "TrafficLightEntity"};
    AddPropertiesToTrafficLightEntity(traffic_light_entity);

    controller_ptr->SetEntity(traffic_light_entity);

    controller_ptr->Step(mantle_api::Time(0));

    auto& expected_bulb_states = traffic_light_phases.at(0).bulb_states;
    auto& entity_bulb_states = traffic_light_entity.GetProperties()->bulbs;
    EXPECT_EQ(expected_bulb_states.at(0).color, entity_bulb_states.at(0).color);
    EXPECT_EQ(expected_bulb_states.at(1).color, entity_bulb_states.at(1).color);

    controller_ptr->Step(mantle_api::Time(5'000));

    expected_bulb_states = traffic_light_phases.at(1).bulb_states;
    entity_bulb_states = traffic_light_entity.GetProperties()->bulbs;
    EXPECT_EQ(expected_bulb_states.at(0).color, entity_bulb_states.at(0).color);
    EXPECT_EQ(expected_bulb_states.at(1).color, entity_bulb_states.at(1).color);
}

TEST(TrafficLightControlUnitTest,
     GivenTrafficLightControlUnitWithoutRepeat_WhenSteppedBeyondLastState_ThenEntityPropertiesRemainInLastState)
{
    auto traffic_light_phases = CreateTrafficLightPhases();
    auto controller_ptr = std::make_unique<TrafficLightControlUnit>(traffic_light_phases, false);

    auto traffic_light_entity = entities::TrafficLightEntity{1, "TrafficLightEntity"};
    AddPropertiesToTrafficLightEntity(traffic_light_entity);

    controller_ptr->SetEntity(traffic_light_entity);

    controller_ptr->Step(mantle_api::Time(0));
    controller_ptr->Step(mantle_api::Time(5'000));

    controller_ptr->Step(mantle_api::Time(11'000));

    auto& expected_bulb_states = traffic_light_phases.at(1).bulb_states;
    auto& entity_bulb_states = traffic_light_entity.GetProperties()->bulbs;
    EXPECT_EQ(expected_bulb_states.at(0).color, entity_bulb_states.at(0).color);
    EXPECT_EQ(expected_bulb_states.at(1).color, entity_bulb_states.at(1).color);
}

TEST(TrafficLightControlUnitTest,
     GivenTrafficLightControlUnitWithRepeat_WhenSteppedBeyondLastState_ThenEntityPropertiesMatchDefinePhasesAgain)
{
    auto traffic_light_phases = CreateTrafficLightPhases();
    auto controller_ptr = std::make_unique<TrafficLightControlUnit>(traffic_light_phases, true);

    auto traffic_light_entity = entities::TrafficLightEntity{1, "TrafficLightEntity"};
    AddPropertiesToTrafficLightEntity(traffic_light_entity);

    controller_ptr->SetEntity(traffic_light_entity);

    controller_ptr->Step(mantle_api::Time(0));
    controller_ptr->Step(mantle_api::Time(5'000));

    controller_ptr->Step(mantle_api::Time(11'000));

    auto& expected_bulb_states = traffic_light_phases.at(0).bulb_states;
    auto& entity_bulb_states = traffic_light_entity.GetProperties()->bulbs;
    EXPECT_EQ(expected_bulb_states.at(0).color, entity_bulb_states.at(0).color);
    EXPECT_EQ(expected_bulb_states.at(1).color, entity_bulb_states.at(1).color);

    controller_ptr->Step(mantle_api::Time(17'000));

    expected_bulb_states = traffic_light_phases.at(1).bulb_states;
    entity_bulb_states = traffic_light_entity.GetProperties()->bulbs;
    EXPECT_EQ(expected_bulb_states.at(0).color, entity_bulb_states.at(0).color);
    EXPECT_EQ(expected_bulb_states.at(1).color, entity_bulb_states.at(1).color);

    controller_ptr->Step(mantle_api::Time(20'000));

    expected_bulb_states = traffic_light_phases.at(0).bulb_states;
    entity_bulb_states = traffic_light_entity.GetProperties()->bulbs;
    EXPECT_EQ(expected_bulb_states.at(0).color, entity_bulb_states.at(0).color);
    EXPECT_EQ(expected_bulb_states.at(1).color, entity_bulb_states.at(1).color);
}

}  // namespace astas::environment::controller
