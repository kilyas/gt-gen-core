/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_PATHCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_PATHCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/Utilities/controller_helpers.h"
#include "Core/Environment/LaneFollowing/point_list_traverser.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::controller
{

class PathControlUnit : public IAbstractControlUnit
{
  public:
    /// Create a Path Controller without a pre-defined route. The longest route will be calculated, the first time the
    /// Step() function is called, based on the position of the assigned entity at that time.
    /// \param map_query_service Query service containing a pointer to the AstasMap.
    explicit PathControlUnit(mantle_api::ILaneLocationQueryService* map_query_service);

    /// Create a Path Controller with a pre-defined route.
    /// \param map_query_service Query service containing a pointer to the AstasMap.
    /// \param waypoints Defines the route to travel, as well as access to a Query Service.
    /// \throws EnvironmentException Throws when a path for the route cannot be computed.
    explicit PathControlUnit(mantle_api::ILaneLocationQueryService* map_query_service,
                             const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints);

    PathControlUnit(const PathControlUnit& path_control_unit);
    PathControlUnit(PathControlUnit&&) = default;
    PathControlUnit& operator=(const PathControlUnit&) = delete;
    PathControlUnit& operator=(PathControlUnit&&) = delete;
    ~PathControlUnit() override = default;

    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;

    virtual bool HasReachedEndOfRoadNetwork() const;

  private:
    lanefollowing::PointDistanceList ComputePointDistanceListFromPath(
        const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints);

    void InitControlUnit() override;
    void UpdateEntity(units::length::meter_t distance_to_move);
    void SetEntityOrientationProperties(const map::LaneLocation& lane_location,
                                        units::length::meter_t distance_to_move);

    const map::LaneLocationProvider* lane_location_provider_;
    std::unique_ptr<lanefollowing::PointListTraverser> traverser_;
    std::unique_ptr<ControllerHelpers> controller_helpers_;
    mantle_api::Vec3<units::length::meter_t> last_entity_position_{};
    units::length::meter_t current_lane_offset_{};
    units::length::meter_t last_entity_lane_offset_{};
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_PATHCONTROLUNIT_H
