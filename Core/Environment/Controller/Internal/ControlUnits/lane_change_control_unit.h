/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANECHANGECONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANECHANGECONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/TransitionDynamics/i_transition_dynamics.h"
#include "Core/Environment/Controller/Internal/Utilities/controller_helpers.h"

namespace astas::environment::controller
{

class LaneChangeControlUnit : public IAbstractControlUnit
{

  public:
    LaneChangeControlUnit(const mantle_api::PerformLaneChangeControlStrategy& control_strategy,
                          mantle_api::ILaneLocationQueryService* map_query_service,
                          std::unique_ptr<ITransitionDynamics> transition_dynamics);
    LaneChangeControlUnit(const LaneChangeControlUnit& lane_control_unit);
    LaneChangeControlUnit(LaneChangeControlUnit&&) = default;
    LaneChangeControlUnit& operator=(const LaneChangeControlUnit&) = delete;
    LaneChangeControlUnit& operator=(LaneChangeControlUnit&&) = delete;
    ~LaneChangeControlUnit() override = default;

    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    void InitControlUnit() override;

    mantle_api::PerformLaneChangeControlStrategy control_strategy_{};
    mantle_api::ILaneLocationQueryService* map_query_service_{nullptr};
    std::unique_ptr<ITransitionDynamics> transition_dynamics_{};

    std::unique_ptr<IControlUnit> route_controller_{};
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_LANECHANGECONTROLUNIT_H
