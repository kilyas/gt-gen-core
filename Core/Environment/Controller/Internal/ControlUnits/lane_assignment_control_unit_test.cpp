/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/lane_assignment_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;

class LaneAssignmentControlUnitTest : public testing::Test
{
  protected:
    LaneAssignmentControlUnitTest() : entity_(std::make_unique<entities::VehicleEntity>(0, "host"))
    {
        entity_->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
        entity_->SetPosition({0_m, 0_m, 0_m});
    }

    std::unique_ptr<mantle_api::IEntity> entity_;
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    bool is_allowed_to_leave_lane_ = true;  // Consistent with the default value in UserSettings
};

TEST_F(LaneAssignmentControlUnitTest, GivenLaneAssignmentControlUnit_WhenClone_ThenCopyCreated)
{
    auto control_unit_ptr =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);

    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

TEST_F(LaneAssignmentControlUnitTest, GivenLaneAssignmentControlUnit_WhenHasFinishedCalled_ThenAlwaysFalse)
{
    auto control_unit_ptr =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);

    EXPECT_FALSE(control_unit_ptr->HasFinished());
}

TEST_F(LaneAssignmentControlUnitTest, GivenAssignedEntityOverRoadAndCannotLeaveLane_WhenStep_ThenNoThrow)
{
    is_allowed_to_leave_lane_ = false;

    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    EXPECT_NO_THROW(lane_assignment_control_unit->Step(mantle_api::Time{0}));
}

TEST_F(LaneAssignmentControlUnitTest, GivenAssignedEntityNotOverRoadAndCannotLeaveLane_WhenStep_ThenThrow)
{
    is_allowed_to_leave_lane_ = false;
    entity_->SetPosition({-500_m, -500_m, -500_m});

    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    EXPECT_THROW(lane_assignment_control_unit->Step(mantle_api::Time{0}), EnvironmentException);
}

TEST_F(LaneAssignmentControlUnitTest, GivenAssignedEntityOverRoadAndCanLeaveLane_WhenStep_ThenNoThrow)
{
    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    EXPECT_NO_THROW(lane_assignment_control_unit->Step(mantle_api::Time{0}));
}

TEST_F(LaneAssignmentControlUnitTest, GivenAssignedEntityNotOverRoadAndCanLeaveLane_WhenStep_ThenNoThrow)
{
    entity_->SetPosition({-500_m, -500_m, -500_m});

    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    EXPECT_NO_THROW(lane_assignment_control_unit->Step(mantle_api::Time{0}));
}

TEST_F(LaneAssignmentControlUnitTest, GivenAssignedEntityOverSingleLane_WhenStep_ThenEntityHasCorrectLaneAssignment)
{
    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    lane_assignment_control_unit->Step(mantle_api::Time{0});

    const auto& entity_assigned_lanes = entity_->GetAssignedLaneIds();
    ASSERT_EQ(entity_assigned_lanes.size(), 1);
    EXPECT_EQ(map_.get()->GetLanes().front().id, entity_assigned_lanes.at(0));
}

TEST_F(LaneAssignmentControlUnitTest,
       GivenAssignedEntityOverMultipleLanesAndEntityCentered_WhenStep_ThenEntityHasCenteredLaneAsFirstAssigned)
{
    std::unique_ptr<environment::map::AstasMap> overlapping_lanes_map =
        test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();
    map::LaneLocationProvider lane_location_provider{*overlapping_lanes_map};
    entity_->SetPosition({100_m, 0.0_m, 0_m});

    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    lane_assignment_control_unit->Step(mantle_api::Time{0});

    const auto& entity_assigned_lanes = entity_->GetAssignedLaneIds();
    ASSERT_EQ(entity_assigned_lanes.size(), 2);
    EXPECT_EQ(overlapping_lanes_map.get()->GetLanes().at(1).id, entity_assigned_lanes.at(0));
    EXPECT_EQ(overlapping_lanes_map.get()->GetLanes().at(2).id, entity_assigned_lanes.at(1));
}

TEST_F(LaneAssignmentControlUnitTest,
       GivenAssignedEntityOverMultipleLanesAndEntityShiftedFromCenter_WhenStep_ThenEntityHasShiftedLaneAsFirstAssigned)
{
    std::unique_ptr<environment::map::AstasMap> overlapping_lanes_map =
        test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();
    map::LaneLocationProvider lane_location_provider{*overlapping_lanes_map};
    entity_->SetPosition({100_m, 1.25_m, 0_m});

    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    lane_assignment_control_unit->Step(mantle_api::Time{0});

    const auto& entity_assigned_lanes = entity_->GetAssignedLaneIds();
    ASSERT_EQ(entity_assigned_lanes.size(), 2);
    EXPECT_EQ(overlapping_lanes_map.get()->GetLanes().at(1).id, entity_assigned_lanes.at(0));
    EXPECT_EQ(overlapping_lanes_map.get()->GetLanes().at(2).id, entity_assigned_lanes.at(1));
}

TEST_F(LaneAssignmentControlUnitTest,
       GivenAssignedEntityDrivesOverDifferentLanes_WhenStep_ThenEntityHasUpdatedLaneAssignments)
{
    auto lane_assignment_control_unit =
        std::make_unique<LaneAssignmentControlUnit>(&lane_location_provider_, is_allowed_to_leave_lane_);
    lane_assignment_control_unit->SetEntity(*entity_);

    lane_assignment_control_unit->Step(mantle_api::Time{0});
    const auto& first_assigned_lanes = entity_->GetAssignedLaneIds();
    ASSERT_EQ(first_assigned_lanes.size(), 1);
    EXPECT_EQ(map_.get()->GetLanes().at(0).id, first_assigned_lanes.at(0));

    entity_->SetPosition({100_m, 0_m, 0_m});
    lane_assignment_control_unit->Step(mantle_api::Time{0});
    const auto& second_assigned_lanes = entity_->GetAssignedLaneIds();
    ASSERT_EQ(second_assigned_lanes.size(), 1);
    EXPECT_EQ(map_.get()->GetLanes().at(1).id, second_assigned_lanes.at(0));
}

}  // namespace astas::environment::controller
