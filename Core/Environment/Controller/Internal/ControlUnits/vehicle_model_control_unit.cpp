/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/vehicle_model_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Service/Utility/derivative_utils.h"

#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::controller
{
using units::literals::operator""_s;

VehicleModelControlUnit::VehicleModelControlUnit(host::HostVehicleModel* vehicle_model, const map::AstasMap* map)
    : vehicle_model_in_{vehicle_model->vehicle_model_in},
      vehicle_model_out_{vehicle_model->vehicle_model_out},
      map_{map}
{
}

void VehicleModelControlUnit::SetEntity(mantle_api::IEntity& entity)
{
    IAbstractControlUnit::SetEntity(entity);
    wheel_contact_.Init(&entity);
}

void VehicleModelControlUnit::StepControlUnit()
{
    UpdateEntityFromVehicleModelOut();
    UpdateVehicleModelIn();  /// TODO: Remove when data is read from OSI output
    UpdateWheelStates();

    const std::string map_coordinates{
        environment::map::GetMapCoordinateString(entity_->GetPosition(), map_->coordinate_converter.get())};
    plausibility_check_.CheckPlausibility(*entity_, delta_time_, map_coordinates);
}

std::unique_ptr<IControlUnit> VehicleModelControlUnit::Clone() const
{
    return std::make_unique<VehicleModelControlUnit>(*this);
}

bool VehicleModelControlUnit::HasFinished() const
{
    return false;
}

void VehicleModelControlUnit::UpdateEntityFromVehicleModelOut()
{
    if (vehicle_model_out_.time_stamp != previous_vehicle_model_out_timestamp_)
    {

        entity_->SetPosition(vehicle_model_out_.position);
        entity_->SetVelocity(vehicle_model_out_.velocity);
        entity_->SetAcceleration(vehicle_model_out_.acceleration);

        if (delta_time_ > 0.0_s)
        {
            const auto new_orientation_acceleration{service::utility::GetOrientationDerivativeVector<
                units::angular_velocity::radians_per_second_t,
                units::angular_acceleration::radians_per_second_squared_t>(
                vehicle_model_out_.orientation_rate, entity_->GetOrientationRate(), delta_time_)};
            entity_->SetOrientationAcceleration(new_orientation_acceleration);
        }

        entity_->SetOrientation(vehicle_model_out_.orientation);
        entity_->SetOrientationRate(vehicle_model_out_.orientation_rate);

        auto* vehicle_entity = dynamic_cast<entities::VehicleEntity*>(entity_);
        vehicle_entity->SetIndicatorState(vehicle_model_out_.indicator_state);
        vehicle_entity->SetHADControlState(vehicle_model_out_.had_control_state);

        previous_vehicle_model_out_timestamp_ = vehicle_model_out_.time_stamp;
    }
}

void VehicleModelControlUnit::UpdateVehicleModelIn()
{
    auto wheel_contact_points = wheel_contact_.GetWheelsWorldSpaceContactPoints();

    auto& wheel_states = vehicle_model_in_.wheel_states;
    wheel_states.front_left_mue = map_->GetFrictionAt(wheel_contact_points.front_left);
    wheel_states.front_right_mue = map_->GetFrictionAt(wheel_contact_points.front_right);
    wheel_states.rear_left_mue = map_->GetFrictionAt(wheel_contact_points.rear_left);
    wheel_states.rear_right_mue = map_->GetFrictionAt(wheel_contact_points.rear_right);

    vehicle_model_in_.time_stamp = current_simulation_time_;
}

void VehicleModelControlUnit::UpdateWheelStates()
{
    auto wheel_contact_points = wheel_contact_.GetWheelsWorldSpaceContactPoints();

    entities::WheelStates wheel_states{};
    wheel_states.front_left_mue = map_->GetFrictionAt(wheel_contact_points.front_left);
    wheel_states.front_right_mue = map_->GetFrictionAt(wheel_contact_points.front_right);
    wheel_states.rear_left_mue = map_->GetFrictionAt(wheel_contact_points.rear_left);
    wheel_states.rear_right_mue = map_->GetFrictionAt(wheel_contact_points.rear_right);

    auto* vehicle_entity = dynamic_cast<entities::VehicleEntity*>(entity_);
    vehicle_entity->SetWheelStates(wheel_states);
}

}  // namespace astas::environment::controller
