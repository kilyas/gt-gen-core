/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Traffic/i_controller_config.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;

class PathControlUnitTest : public testing::Test
{
  protected:
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    ControllerDataExchangeContainer data_exchange_container_{{0.0_mps},
                                                             0.0_mps_sq,
                                                             0.0_rad_per_s,
                                                             0.0_rad_per_s_sq,
                                                             {0.0_m}};
};

/// Test with a Route
TEST_F(PathControlUnitTest, GivenEmptyWaypointList_WhenCreatingRouteController_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {};

    EXPECT_THROW(PathControlUnit route_controller(&lane_location_provider_, waypoints), EnvironmentException);
}

TEST_F(PathControlUnitTest, GivenFirstWaypointNotOnALane_WhenCreatingRouteController_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{42_m, 42_m, 0_m},  // not on lane
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{20_m, 0_m, 0_m}};

    EXPECT_THROW(PathControlUnit route_controller(&lane_location_provider_, waypoints), EnvironmentException);
}

TEST_F(PathControlUnitTest, GivenWaypointNotOnALane_WhenCreatingRouteController_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{42_m, 42_m, 0_m},  // not on lane
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m}};

    EXPECT_THROW(PathControlUnit route_controller(&lane_location_provider_, waypoints), EnvironmentException);
}

TEST_F(PathControlUnitTest, GivenLastWaypointNotOnALane_WhenCreatingRouteController_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{42_m, 42_m, 0_m}};  // not on lane

    EXPECT_THROW(PathControlUnit route_controller(&lane_location_provider_, waypoints), EnvironmentException);
}

TEST_F(PathControlUnitTest, GivenOneWaypoint_WhenCreatingRouteController_ThenNoExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}};

    EXPECT_NO_THROW(PathControlUnit route_controller(&lane_location_provider_, waypoints));
}

TEST_F(PathControlUnitTest, GivenValidWaypoints_WhenCreatingRouteController_ThenNoExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    EXPECT_NO_THROW(PathControlUnit route_controller(&lane_location_provider_, waypoints));
}

TEST_F(PathControlUnitTest,
       GivenFirstWaypointExactlyOnFirstCenterLinePoint_WhenMoveInitially_ThenEntityPlacedOnFirstWaypoint)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit route_controller(&lane_location_provider_, waypoints);
    route_controller.SetEntity(*entity);

    route_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    route_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), entity->GetPosition())
}

TEST_F(PathControlUnitTest,
       GivenFirstWaypointExactlyOnFirstCenterLinePoint_WhenMoveInitially_ThenEntityHasLaneOrientation)
{
    using units::literals::operator""_rad;
    using units::literals::operator""_rad_per_s;
    using units::literals::operator""_rad_per_s_sq;

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{142_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit route_controller(&lane_location_provider_, waypoints);
    route_controller.SetEntity(*entity);

    route_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    route_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 0_rad), entity->GetOrientation())
    EXPECT_TRIPLE(
        mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>(0_rad_per_s, 0_rad_per_s, 0_rad_per_s),
        entity->GetOrientationRate())
    EXPECT_TRIPLE(mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>(
                      0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq),
                  entity->GetOrientationAcceleration())
}

TEST_F(PathControlUnitTest, GivenLastWaypointAlongLane_WhenMove_ThenDoNoExceedLastWaypoint)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{140_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{150_m, 0_m, 0_m}};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit route_controller(&lane_location_provider_, waypoints);
    route_controller.SetEntity(*entity);

    data_exchange_container_.velocity_scalars.push_back(80_mps);
    route_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    route_controller.Step(mantle_api::Time{0});  // initial step

    route_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(150_m, 0_m, 0_m), entity->GetPosition())
}

TEST_F(PathControlUnitTest,
       GivenMapWithThreeConnectedLanes_WhenMoveMultipleTimes_ThenFollowAllCenterLinesFromBeginToEnd)
{
    std::map<int, mantle_api::Vec3<units::length::meter_t>> expected_positions;
    expected_positions.emplace(0, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m});
    expected_positions.emplace(1000, mantle_api::Vec3<units::length::meter_t>{80_m, 0_m, 0_m});
    expected_positions.emplace(2000, mantle_api::Vec3<units::length::meter_t>{160_m, 0_m, 0_m});
    expected_positions.emplace(3000, mantle_api::Vec3<units::length::meter_t>{240_m, 0_m, 0_m});
    expected_positions.emplace(4000, mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m});

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit route_controller(&lane_location_provider_, waypoints);
    route_controller.SetEntity(*entity);

    data_exchange_container_.velocity_scalars.push_back(80_mps);
    route_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    route_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_positions.at(0), entity->GetPosition())

    route_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_positions.at(1000), entity->GetPosition())

    route_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(expected_positions.at(2000), entity->GetPosition())

    route_controller.Step(mantle_api::Time{3000});
    EXPECT_TRIPLE(expected_positions.at(3000), entity->GetPosition())

    route_controller.Step(mantle_api::Time{4000});
    EXPECT_TRIPLE(expected_positions.at(4000), entity->GetPosition())
}

TEST_F(PathControlUnitTest, GivenRouteWithSplitOnLeftSide_WhenMoveProgresses_ThenTargetOnOtherLaneIsReached)
{
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes()};
    map::LaneLocationProvider lane_location_provider{*map};

    std::map<int, mantle_api::Vec3<units::length::meter_t>> expected_positions;
    expected_positions.emplace(0, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m});
    expected_positions.emplace(1000, mantle_api::Vec3<units::length::meter_t>{80_m, 0_m, 0_m});
    expected_positions.emplace(2000, mantle_api::Vec3<units::length::meter_t>{159.83983299_m, 4_m, 0_m});
    expected_positions.emplace(3000, mantle_api::Vec3<units::length::meter_t>{239.83983299_m, 4_m, 0_m});
    expected_positions.emplace(4000, mantle_api::Vec3<units::length::meter_t>{297_m, 4_m, 0_m});

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 4_m, 0_m}};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit route_controller(&lane_location_provider, waypoints);
    route_controller.SetEntity(*entity);

    data_exchange_container_.velocity_scalars.push_back(80_mps);
    route_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    route_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_positions.at(0), entity->GetPosition())

    route_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_positions.at(1000), entity->GetPosition())

    route_controller.Step(mantle_api::Time{2000});
    EXPECT_TRIPLE(expected_positions.at(2000), entity->GetPosition())

    route_controller.Step(mantle_api::Time{3000});
    EXPECT_TRIPLE(expected_positions.at(3000), entity->GetPosition())

    route_controller.Step(mantle_api::Time{4000});
    EXPECT_TRIPLE(expected_positions.at(4000), entity->GetPosition())
}

TEST_F(PathControlUnitTest, GivenRouteControllerWithPath_WhenClone_ThenCopyCreated)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    auto controller_ptr = std::make_unique<PathControlUnit>(&lane_location_provider_, waypoints);

    auto copy = controller_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(controller_ptr.get(), copy.get());
}

/// Test without a Route
TEST_F(PathControlUnitTest, GivenRouteControllerWithoutPath_WhenClone_ThenCopyCreated)
{
    auto controller_ptr = std::make_unique<PathControlUnit>(&lane_location_provider_);

    auto copy = controller_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(controller_ptr.get(), copy.get());
}

TEST_F(PathControlUnitTest, GivenEntityOnLane_WhenMoveProgressesToEndOfRoadNetwork_ThenControllerIsFinished)
{
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    map::LaneLocationProvider lane_location_provider{*map};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit lane_follow_controller(&lane_location_provider);
    lane_follow_controller.SetEntity(*entity);

    data_exchange_container_.velocity_scalars.push_back(100_mps);
    lane_follow_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    lane_follow_controller.Step(mantle_api::Time{0});

    lane_follow_controller.Step(mantle_api::Time{4000});
    const mantle_api::Vec3<units::length::meter_t>& expected_end_position =
        mantle_api::Vec3<units::length::meter_t>{99_m, 0_m, 0_m};
    EXPECT_TRIPLE(expected_end_position, entity->GetPosition());

    EXPECT_TRUE(lane_follow_controller.HasFinished());
}

TEST_F(PathControlUnitTest, GivenEntityAtEndOfRoadNetwork_WhenStep_ThenEndOfRoadNetworkReached)
{
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    map::LaneLocationProvider lane_location_provider{*map};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    entity->SetPosition(mantle_api::Vec3<units::length::meter_t>{99_m, 0_m, 0_m});

    PathControlUnit path_control_unit(&lane_location_provider);
    path_control_unit.SetEntity(*entity);

    data_exchange_container_.velocity_scalars.push_back(100_mps);
    path_control_unit.SetControllerDataExchangeContainer(data_exchange_container_);

    EXPECT_FALSE(path_control_unit.HasReachedEndOfRoadNetwork());

    path_control_unit.Step(mantle_api::Time{0});

    EXPECT_TRUE(path_control_unit.HasReachedEndOfRoadNetwork());
}

TEST_F(PathControlUnitTest, GivenMapWithMerge_WhenMoveProgresses_ThenEndOfLastLaneReached)
{
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::MapMergeTwoLanesToTheLeftIntoOneLane()};
    map::LaneLocationProvider lane_location_provider{*map};

    std::map<int, mantle_api::Vec3<units::length::meter_t>> expected_positions;
    expected_positions.emplace(0, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m});
    expected_positions.emplace(4000, mantle_api::Vec3<units::length::meter_t>{297_m, 4_m, 0_m});

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit lane_follow_controller(&lane_location_provider);
    lane_follow_controller.SetEntity(*entity);

    data_exchange_container_.velocity_scalars.push_back(80_mps);
    lane_follow_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    lane_follow_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_positions.at(0), entity->GetPosition())

    lane_follow_controller.Step(mantle_api::Time{4000});
    EXPECT_TRIPLE(expected_positions.at(4000), entity->GetPosition())
}

/// Test is based on the following issue: #2515693
TEST_F(PathControlUnitTest,
       GivenEntityWithLaneOffsetsAndZeroVelocity_WhenControllerIsStepped_ThenEntityHasLateralOffsets)
{
    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit lane_follow_controller(&lane_location_provider_);
    lane_follow_controller.SetEntity(*entity);

    data_exchange_container_.lane_offset_scalars.push_back(1.0_m);
    lane_follow_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    const mantle_api::Vec3<units::length::meter_t>& expected_position =
        mantle_api::Vec3<units::length::meter_t>{0_m, 1_m, 0_m};

    lane_follow_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_position, entity->GetPosition());
}

TEST_F(
    PathControlUnitTest,
    GivenMapWithThreeConnectedLanes_WhenMoveMultipleTimesAndEntityPositionIsChangedByExternalAction_ThenPositionIsNotOverwritten)
{
    std::map<int, mantle_api::Vec3<units::length::meter_t>> expected_positions;
    auto distance_to_move = 80_m;
    auto position_changed = mantle_api::Vec3<units::length::meter_t>{200_m, 0_m, 0_m};
    auto new_position = position_changed + mantle_api::Vec3<units::length::meter_t>{distance_to_move, 0_m, 0_m};
    expected_positions.emplace(0, mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m});
    expected_positions.emplace(1000, mantle_api::Vec3<units::length::meter_t>{distance_to_move, 0_m, 0_m});
    expected_positions.emplace(2000, new_position);

    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

    PathControlUnit route_controller(&lane_location_provider_, waypoints);
    route_controller.SetEntity(*entity);

    data_exchange_container_.velocity_scalars.push_back(80_mps);
    route_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    route_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(expected_positions.at(0), entity->GetPosition())

    route_controller.Step(mantle_api::Time{1000});
    EXPECT_TRIPLE(expected_positions.at(1000), entity->GetPosition())

    entity->SetPosition(position_changed);

    route_controller.Step(mantle_api::Time{2000});

    EXPECT_TRIPLE(expected_positions.at(2000), entity->GetPosition());
}

TEST_F(PathControlUnitTest,
       GivenEntityWithNoVelocity_WhenEntityNotOrientedInLaneDrivingDirection_ThenEntityOrientationDoesNotChange)
{
    using units::literals::operator""_rad;
    using units::literals::operator""_rad_per_s;
    using units::literals::operator""_rad_per_s_sq;

    std::unique_ptr<mantle_api::IEntity> entity = std::make_unique<entities::VehicleEntity>(0, "host");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    entity->SetOrientation(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 1_rad));

    PathControlUnit route_controller(&lane_location_provider_);
    route_controller.SetEntity(*entity);
    route_controller.SetControllerDataExchangeContainer(data_exchange_container_);

    route_controller.Step(mantle_api::Time{0});
    EXPECT_TRIPLE(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 1_rad), entity->GetOrientation())
    EXPECT_TRIPLE(
        mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>(0_rad_per_s, 0_rad_per_s, 0_rad_per_s),
        entity->GetOrientationRate())
    EXPECT_TRIPLE(mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>(
                      0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq),
                  entity->GetOrientationAcceleration())
}
}  // namespace astas::environment::controller
