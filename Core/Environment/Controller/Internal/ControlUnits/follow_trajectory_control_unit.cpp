/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_control_unit.h"

#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::controller
{

using units::literals::operator""_m;
using units::literals::operator""_mps;

FollowTrajectoryControlUnit::FollowTrajectoryControlUnit(mantle_api::ILaneLocationQueryService* map_query_service,
                                                         const mantle_api::Trajectory& trajectory)
    : lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)},
      trajectory_{trajectory},
      controller_helpers_{std::make_unique<ControllerHelpers>(lane_location_provider_)}

{
    traverser_ = std::make_unique<lanefollowing::PointListTraverser>(
        controller_helpers_->ConvertTrajectoryIntoPointDistanceList(trajectory));
    originating_control_strategy_ = mantle_api::ControlStrategyType::kFollowTrajectory;
}

FollowTrajectoryControlUnit::FollowTrajectoryControlUnit(
    const FollowTrajectoryControlUnit& follow_trajectory_control_unit)
    : IAbstractControlUnit(follow_trajectory_control_unit),
      lane_location_provider_{follow_trajectory_control_unit.lane_location_provider_},
      trajectory_{follow_trajectory_control_unit.trajectory_},
      controller_helpers_{std::make_unique<ControllerHelpers>(lane_location_provider_)}
{
    originating_control_strategy_ = follow_trajectory_control_unit.originating_control_strategy_;

    if (follow_trajectory_control_unit.traverser_ != nullptr)
    {
        traverser_ = std::make_unique<lanefollowing::PointListTraverser>(*follow_trajectory_control_unit.traverser_);
    }
}

std::unique_ptr<IControlUnit> FollowTrajectoryControlUnit::Clone() const
{
    return std::make_unique<FollowTrajectoryControlUnit>(*this);
}

void FollowTrajectoryControlUnit::CalculateCurrentOrientation()
{
    const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
    auto ratio = 1.0;
    if (traverser_->GetDistanceToNextPoint() > 0.0_m)
    {
        ratio = traverser_->GetAdditionalDistance() / traverser_->GetDistanceToNextPoint();
    }

    const auto index = traverser_->GetIndex();
    const auto current_point_orientation = poly_line[index].pose.orientation;
    if (ratio == 1.0 || index + 1 >= poly_line.size())
    {
        current_orientation_ = current_point_orientation;
    }
    else
    {
        const auto next_point_orientation = poly_line[index + 1].pose.orientation;
        const auto new_roll =
            current_point_orientation.roll + (next_point_orientation.roll - current_point_orientation.roll) * ratio;
        const auto new_yaw =
            current_point_orientation.yaw + (next_point_orientation.yaw - current_point_orientation.yaw) * ratio;
        const auto new_pitch =
            current_point_orientation.pitch + (next_point_orientation.pitch - current_point_orientation.pitch) * ratio;
        current_orientation_ = {new_yaw, new_pitch, new_roll};
    }
}

void FollowTrajectoryControlUnit::UpdateTraverser()
{
    const auto longitudinal_distance{data_exchange_container_->velocity_scalars.back() * delta_time_};
    traverser_->Move(longitudinal_distance);
}

void FollowTrajectoryControlUnit::StepControlUnit()
{
    ASTAS_PROFILE_SCOPE
    UpdateTraverser();
    CalculateCurrentOrientation();
    UpdateEntity();
}

void FollowTrajectoryControlUnit::UpdateEntity()
{
    auto new_position = traverser_->GetPosition();
    if (traverser_->GetAdditionalDistance() > 0.0_m && !traverser_->IsLastPointReached())
    {
        const auto forward = service::glmwrapper::Normalize(traverser_->GetNextPoint() - new_position);
        const auto position_on_segment = forward * traverser_->GetAdditionalDistance()();
        new_position = new_position + position_on_segment;
    }

    entity_->SetPosition(new_position);
    SetEntityOrientationProperties(current_orientation_);
    SetEntityVelocityAndAcceleration();
}

bool FollowTrajectoryControlUnit::HasFinished() const
{
    return traverser_ != nullptr && traverser_->IsLastPointReached();
}

}  // namespace astas::environment::controller
