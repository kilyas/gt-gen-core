/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/no_op_control_unit.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{

TEST(NoOpControlUnitTest, GivenNoOpControlUnit_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    NoOpControlUnit control_unit{};

    EXPECT_FALSE(control_unit.HasFinished());
}

TEST(NoOpControlUnitTest, GivenNoOpControlUnit_WhenClone_ThenCopyCreated)
{
    auto control_unit_ptr = std::make_unique<NoOpControlUnit>();

    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

}  // namespace astas::environment::controller
