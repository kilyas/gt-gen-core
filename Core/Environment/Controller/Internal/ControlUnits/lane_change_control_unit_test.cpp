/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/lane_change_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using testing::Return;
using units::literals::operator""_s;
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;

class MockTransitionDynamics : public ITransitionDynamics
{
  public:
    using ITransitionDynamics::ITransitionDynamics;
    MOCK_METHOD(void, SetStartVal, (double), (override));
    MOCK_METHOD(void, SetTargetVal, (double), (override));
    MOCK_METHOD(void, SetMaxRate, (double), (override));
    MOCK_METHOD(void, SetParamVal, (double), (override));
    MOCK_METHOD(bool, Step, (double), (override));
    MOCK_METHOD(double, Evaluate, (), (const, override));
    MOCK_METHOD(double, EvaluateFirstDerivative, (), (const, override));
    MOCK_METHOD(bool, IsTargetReached, (), (const, override));
    MOCK_METHOD(double, CalculatePeakRate, (), (const, override));
    MOCK_METHOD(double, CalculateTargetParameterValByRate, (const double rate), (const, override));
    MOCK_METHOD(mantle_api::Dimension, GetDimension, (), (const, override));
};

class LaneChangeControlUnitTest : public testing::Test
{
  protected:
    void SetMap(std::unique_ptr<environment::map::AstasMap> map) { map_ = std::move(map); }
    void Init()
    {
        if (!map_)
        {
            map_ = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
        }

        lane_location_provider_ = std::make_unique<map::LaneLocationProvider>(*map_);

        entity_ = std::make_unique<entities::VehicleEntity>(0, "host");
        entity_->SetProperties(std::make_unique<mantle_api::VehicleProperties>());

        auto mock_transition_dynamics_unique_ptr = std::make_unique<MockTransitionDynamics>();
        mock_transition_dynamics_ = mock_transition_dynamics_unique_ptr.get();
        control_unit_ = std::make_unique<LaneChangeControlUnit>(
            control_strategy_, lane_location_provider_.get(), std::move(mock_transition_dynamics_unique_ptr));

        control_unit_->SetEntity(*entity_);
        control_unit_->SetControllerDataExchangeContainer(data_exchange_container_);
    }

    mantle_api::PerformLaneChangeControlStrategy control_strategy_{};
    ControllerDataExchangeContainer data_exchange_container_{{0.0_mps},
                                                             0.0_mps_sq,
                                                             0.0_rad_per_s,
                                                             0.0_rad_per_s_sq,
                                                             {0.0_m}};
    std::unique_ptr<environment::map::AstasMap> map_{};
    std::unique_ptr<map::LaneLocationProvider> lane_location_provider_{};

    MockTransitionDynamics* mock_transition_dynamics_{nullptr};
    std::unique_ptr<LaneChangeControlUnit> control_unit_{};
    std::unique_ptr<entities::VehicleEntity> entity_{};
};

/// Creation tests
TEST_F(LaneChangeControlUnitTest, GivenValidInputs_WhenCreatingLaneChangeControlUnitUnit_ThenNoExceptionThrown)
{
    EXPECT_NO_THROW(
        LaneChangeControlUnit lane_offset_control_unit(control_strategy_, lane_location_provider_.get(), nullptr));
}

/// Clone tests
TEST_F(LaneChangeControlUnitTest, GivenLaneChangeControlUnitUnit_WhenClone_ThenCopyCreated)
{
    control_strategy_.target_lane_id = 5;  // move from lane 2 to 5 (one shift to the left), lateral offset is 4m

    Init();
    const auto copy = control_unit_->Clone();
    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_.get(), copy.get());
}

/// Step tests
TEST_F(LaneChangeControlUnitTest,
       GivenEntityProjectedPositionNotOnValidLane_WhenStepControlUnit_ThenThrowEnvironmentException)
{
    control_strategy_.target_lane_id = 30;  // target lane does not exist

    Init();

    EXPECT_THROW(control_unit_->Step(mantle_api::Time{0}), EnvironmentException);
}

TEST_F(LaneChangeControlUnitTest,
       GivenValidInputs_WhenStepControlUnit_ThenSetCorrectTransitionDynamicsStartTargetValueAndNoThrown)
{
    control_strategy_.target_lane_id = 5;  // move from lane 2 to 5 (one shift to the left), lateral offset is 4m

    Init();

    EXPECT_CALL(*mock_transition_dynamics_, SetStartVal(-4.0));
    EXPECT_CALL(*mock_transition_dynamics_, SetTargetVal(0.0));
    EXPECT_NO_THROW(control_unit_->Step(mantle_api::Time{0}));
}

/// HasFinished tests
TEST_F(LaneChangeControlUnitTest, GivenNoLaneChange_WhenHasFinished_ThenReturnTrue)
{
    control_strategy_.target_lane_id = 5;

    Init();

    control_unit_->Step(mantle_api::Time{0});

    EXPECT_CALL(*mock_transition_dynamics_, IsTargetReached()).WillOnce(Return(true));

    EXPECT_TRUE(control_unit_->HasFinished());
}

TEST_F(LaneChangeControlUnitTest,
       GivenEntityOnLaneWithLaneChange_WhenMoveProgressesToEndOfRoadNetwork_ThenControllerIsFinished)
{
    control_strategy_.target_lane_id = 5;  // move from lane 2 to 5 (one shift to the left), lateral offset is 4m
    data_exchange_container_.velocity_scalars.push_back(100_mps);
    Init();

    control_unit_->Step(mantle_api::Time{0});
    control_unit_->Step(mantle_api::Time{4000});

    const auto expected_end_position = mantle_api::Vec3<units::length::meter_t>{297_m, 4_m, 0_m};
    EXPECT_TRIPLE(expected_end_position, entity_->GetPosition());
    EXPECT_TRUE(control_unit_->HasFinished());
}

TEST_F(LaneChangeControlUnitTest, GivenMapWithMerge_WhenMoveProgresses_ThenEndOfLastLaneReached)
{
    SetMap(test_utils::MapCatalogue::MapMergeTwoLanesToTheLeftIntoOneLane());

    control_strategy_.target_lane_id = 5;  // move from lane 2 to 5 (one shift to the left), lateral offset is 4m
    data_exchange_container_.velocity_scalars.push_back(100_mps);

    Init();

    control_unit_->Step(mantle_api::Time{0});
    control_unit_->Step(mantle_api::Time{4000});

    const auto expected_end_position = mantle_api::Vec3<units::length::meter_t>{297_m, 4_m, 0_m};
    EXPECT_TRIPLE(expected_end_position, entity_->GetPosition());
    EXPECT_TRUE(control_unit_->HasFinished());
}

TEST_F(LaneChangeControlUnitTest,
       GivenEntityWithLaneOffsetsAndZeroVelocity_WhenControllerIsStepped_ThenEntityMovedVertically)
{
    control_strategy_.target_lane_id = 5;  // move from lane 2 to 5 (one shift to the left), lateral offset is 4m
    Init();
    EXPECT_CALL(*mock_transition_dynamics_, Evaluate()).WillOnce(Return(-2.0));  // offset within the initial step
    control_unit_->Step(mantle_api::Time{0});

    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{0_m, 2_m, 0_m};

    EXPECT_TRIPLE(expected_position, entity_->GetPosition());
}

TEST_F(LaneChangeControlUnitTest,
       GivenEntityWithLaneOffsetsAndVelocity_WhenControllerIsStepped_ThenEntityMovedInBothHorizontalAndVertical)
{
    data_exchange_container_.velocity_scalars.push_back(10_mps);
    control_strategy_.target_lane_id = 5;  // move from lane 2 to 5 (one shift to the left), lateral offset is 4m
    Init();
    EXPECT_CALL(*mock_transition_dynamics_, Evaluate())
        .WillOnce(Return(0.0))    // initial step
        .WillOnce(Return(-2.0));  // move to left 2m

    control_unit_->Step(mantle_api::Time{0});
    control_unit_->Step(mantle_api::Time{100});

    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{1_m, 2_m, 0_m};

    EXPECT_TRIPLE(expected_position, entity_->GetPosition());
}

TEST_F(LaneChangeControlUnitTest,
       GivenDynamicDimensionDistance_WhenControllerIsStepped_ThenTransitionDynamicsIsSteppedWithDistanceValue)
{
    const auto speed{10.0_mps};
    const auto delta_time{5.0_s};
    const auto expected_delta_distance{speed * delta_time};

    data_exchange_container_.velocity_scalars.push_back(speed);
    control_strategy_.target_lane_id = 5;
    Init();

    EXPECT_CALL(*mock_transition_dynamics_, GetDimension()).WillRepeatedly(Return(mantle_api::Dimension::kDistance));

    ::testing::InSequence s;
    EXPECT_CALL(*mock_transition_dynamics_, Step(0)).Times(1);
    EXPECT_CALL(*mock_transition_dynamics_, Step(expected_delta_distance())).Times(1);

    control_unit_->Step(units::time::millisecond_t{0});
    control_unit_->Step(delta_time);
}

}  // namespace astas::environment::controller
