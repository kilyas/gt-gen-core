/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/Utilities/controller_helpers.h"
#include "Core/Environment/LaneFollowing/point_list_traverser.h"

namespace astas::environment::controller
{

class FollowTrajectoryControlUnit : public IAbstractControlUnit
{
  public:
    /// @brief Create a follow trajectory with a pre-defined trajectory.
    /// @param map_query_service Query service containing a pointer to the AstasMap.
    /// @param trajectory Defines the trajectory to travel indicating the position for each point
    /// @throws EnvironmentException Throws when trajectory is empty.
    explicit FollowTrajectoryControlUnit(mantle_api::ILaneLocationQueryService* map_query_service,
                                         const mantle_api::Trajectory& trajectory);

    FollowTrajectoryControlUnit(const FollowTrajectoryControlUnit& follow_trajectory_control_unit);
    FollowTrajectoryControlUnit(FollowTrajectoryControlUnit&&) = default;
    FollowTrajectoryControlUnit& operator=(const FollowTrajectoryControlUnit&) = delete;
    FollowTrajectoryControlUnit& operator=(FollowTrajectoryControlUnit&&) = delete;
    ~FollowTrajectoryControlUnit() override = default;

    std::unique_ptr<IControlUnit> Clone() const override;

    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    void CalculateCurrentOrientation();

    void UpdateEntity();
    void UpdateTraverser();

    const map::LaneLocationProvider* lane_location_provider_;
    mantle_api::Trajectory trajectory_{};

    std::unique_ptr<lanefollowing::PointListTraverser> traverser_;
    std::unique_ptr<ControllerHelpers> controller_helpers_;

    mantle_api::Orientation3<units::angle::radian_t> current_orientation_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_FOLLOWTRAJECTORYCONTROLUNIT_H
