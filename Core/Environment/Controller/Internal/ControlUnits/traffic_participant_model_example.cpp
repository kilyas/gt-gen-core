/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_model_example.h"

#include "Core/Service/Logging/logging.h"

namespace astas::environment::controller
{

using osi_traffic_participant::ITrafficParticipantModel;

TpmExample::~TpmExample() = default;

std::shared_ptr<ITrafficParticipantModel> TpmExample::Create(
    [[maybe_unused]] const std::map<std::string, std::string>& parameters)
{
    return std::make_shared<TpmExample>();
}

astas_osi3::TrafficUpdate TpmExample::Update([[maybe_unused]] const std::chrono::milliseconds& delta_time,
                                             const astas_osi3::SensorView& sensor_view)
{
    auto& ground_truth = sensor_view.global_ground_truth();

    ASSERT(ground_truth.moving_object_size() > 0 &&
           "TpmExample: received groundtruth does not contain any moving objects.");

    auto& original_object = ground_truth.moving_object(0);

    astas_osi3::TrafficUpdate traffic_update;
    auto* moving_object = traffic_update.add_update();

    moving_object->CopyFrom(original_object);

    auto* base = moving_object->mutable_base();
    base->mutable_position()->set_x(123.45);

    num_spins_++;

    return traffic_update;
}

bool TpmExample::HasFinished() const
{
    return (num_spins_ > 1);
}

}  // namespace astas::environment::controller
