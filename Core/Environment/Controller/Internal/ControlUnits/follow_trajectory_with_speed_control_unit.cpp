/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_with_speed_control_unit.h"

#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/derivative_utils.h"

#include <MantleAPI/Common/floating_point_helper.h>

namespace astas::environment::controller
{
using units::literals::operator""_s;
using units::literals::operator""_m;
using units::literals::operator""_rad;

FollowTrajectoryWithSpeedControlUnit::FollowTrajectoryWithSpeedControlUnit(
    mantle_api::ILaneLocationQueryService* map_query_service,
    const mantle_api::Trajectory& trajectory,
    const mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference time_reference)
    : lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)},
      trajectory_{trajectory},
      time_reference_{time_reference},
      controller_helpers_{std::make_unique<ControllerHelpers>(lane_location_provider_)}

{
    traverser_ = std::make_unique<lanefollowing::PointListTraverser>(
        controller_helpers_->ConvertTrajectoryIntoPointDistanceList(trajectory));
    originating_control_strategy_ = mantle_api::ControlStrategyType::kFollowTrajectory;
}

FollowTrajectoryWithSpeedControlUnit::FollowTrajectoryWithSpeedControlUnit(
    const FollowTrajectoryWithSpeedControlUnit& follow_trajectory_with_speed_control_unit)
    : IAbstractControlUnit(follow_trajectory_with_speed_control_unit),
      lane_location_provider_{follow_trajectory_with_speed_control_unit.lane_location_provider_},
      trajectory_{follow_trajectory_with_speed_control_unit.trajectory_},
      controller_helpers_{std::make_unique<ControllerHelpers>(lane_location_provider_)}
{
    originating_control_strategy_ = follow_trajectory_with_speed_control_unit.originating_control_strategy_;

    if (follow_trajectory_with_speed_control_unit.traverser_ != nullptr)
    {
        traverser_ =
            std::make_unique<lanefollowing::PointListTraverser>(*follow_trajectory_with_speed_control_unit.traverser_);
    }
}

std::unique_ptr<IControlUnit> FollowTrajectoryWithSpeedControlUnit::Clone() const
{
    return std::make_unique<FollowTrajectoryWithSpeedControlUnit>(*this);
}

bool FollowTrajectoryWithSpeedControlUnit::EntityNeedsToBeUpdated()
{
    const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
    const auto first_point_time = GetTimeInTrajectoryPointList(0);
    const auto last_point_time = GetTimeInTrajectoryPointList(poly_line.size() - 1);
    return !(current_simulation_time_ < first_point_time || current_simulation_time_ > last_point_time);
}

void FollowTrajectoryWithSpeedControlUnit::CalculateCurrentOrientation()
{
    const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
    double ratio = 1.0;
    if (traverser_->GetDistanceToNextPoint() > 0.0_m)
    {
        ratio = traverser_->GetAdditionalDistance() / traverser_->GetDistanceToNextPoint();
    }

    auto index = GetSimulationTimerIndexInPolyline();
    const auto current_point_orientation = poly_line[index.value()].pose.orientation;

    if (mantle_api::AlmostEqual(ratio, 1.0, MANTLE_API_DEFAULT_EPS, true))
    {
        current_orientation_ = current_point_orientation;
    }
    else
    {
        if (index.value() + 1 < poly_line.size())
        {
            const auto next_point_orientation = poly_line[index.value() + 1].pose.orientation;
            const auto new_roll =
                current_point_orientation.roll + (next_point_orientation.roll - current_point_orientation.roll) * ratio;
            const auto new_yaw =
                current_point_orientation.yaw + (next_point_orientation.yaw - current_point_orientation.yaw) * ratio;
            const auto new_pitch = current_point_orientation.pitch +
                                   (next_point_orientation.pitch - current_point_orientation.pitch) * ratio;
            current_orientation_ = {new_yaw, new_pitch, new_roll};
        }
    }
}

std::optional<std::size_t> FollowTrajectoryWithSpeedControlUnit::GetSimulationTimerIndexInPolyline()
{
    const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
    std::size_t max_index = poly_line.size();

    for (auto index = 0; index < poly_line.size(); index++)
    {
        const auto time_in_trajectory_point = GetTimeInTrajectoryPointList(index);
        if (current_simulation_time_ >= time_in_trajectory_point)
        {
            max_index = index;
        }
    }
    if (max_index < poly_line.size())
    {
        return max_index;
    }
    return std::nullopt;
}

mantle_api::Time FollowTrajectoryWithSpeedControlUnit::GetTimeInTrajectoryPointList(const std::size_t index)
{
    const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
    if (time_reference_.domainAbsoluteRelative == mantle_api::ReferenceContext::kAbsolute)
    {
        return (poly_line[index].time.value() * time_reference_.scale) + time_reference_.offset;
    }
    else
    {
        return start_simulation_time_.value() + (poly_line[index].time.value() * time_reference_.scale) +
               time_reference_.offset;
    }
}

void FollowTrajectoryWithSpeedControlUnit::CalculateNextVelocityAndAcceleration()
{
    const auto index = GetSimulationTimerIndexInPolyline();
    if (index.has_value())
    {
        const auto next_index = index.value() + 1;
        const auto& poly_line = std::get<mantle_api::PolyLine>(trajectory_.type);
        if (next_index < poly_line.size())
        {
            const auto next_time_in_trajectory_point = GetTimeInTrajectoryPointList(next_index);
            const units::time::second_t delta_time{next_time_in_trajectory_point - current_simulation_time_};

            if (delta_time > 0.0_s)
            {
                const auto new_velocity{
                    service::utility::GetPositionDerivativeVector<units::length::meter_t,
                                                                  units::velocity::meters_per_second_t>(
                        poly_line[next_index].pose.position, entity_->GetPosition(), delta_time)
                        .Length()};
                current_acceleration_ =
                    service::utility::GetDerivative<units::velocity::meters_per_second_t,
                                                    units::acceleration::meters_per_second_squared_t>(
                        new_velocity, current_velocity_, delta_time);
                current_velocity_ = new_velocity;
            }
        }
    }
}

void FollowTrajectoryWithSpeedControlUnit::UpdateTraverser()
{
    const auto longitudinal_distance{current_velocity_ * delta_time_};
    traverser_->Move(longitudinal_distance);
}

void FollowTrajectoryWithSpeedControlUnit::SetStartSimulationTime()
{
    if (!start_simulation_time_.has_value())
    {
        start_simulation_time_ = current_simulation_time_;
    }
}

void FollowTrajectoryWithSpeedControlUnit::StepControlUnit()
{
    ASTAS_PROFILE_SCOPE
    SetStartSimulationTime();
    if (EntityNeedsToBeUpdated())
    {
        UpdateTraverser();
        CalculateCurrentOrientation();
        UpdateEntity();
        CalculateNextVelocityAndAcceleration();
    }
}

void FollowTrajectoryWithSpeedControlUnit::UpdateEntity()
{
    auto new_position = traverser_->GetPosition();
    if (traverser_->GetAdditionalDistance() > 0.0_m && !traverser_->IsLastPointReached())
    {
        const auto forward = service::glmwrapper::Normalize(traverser_->GetNextPoint() - new_position);
        const auto position_on_segment = forward * traverser_->GetAdditionalDistance()();
        new_position = new_position + position_on_segment;
    }

    entity_->SetPosition(new_position);

    SetEntityOrientationProperties(current_orientation_);
    SetEntityVelocityAndAcceleration();
}

void FollowTrajectoryWithSpeedControlUnit::SetEntityVelocityAndAcceleration()
{
    const auto entity_orientation = entity_->GetOrientation();
    const auto velocity_vector = GetVelocityVector(entity_orientation.yaw, entity_orientation.pitch, current_velocity_);
    const auto acceleration_vector =
        GetAccelerationVector(entity_orientation.yaw, entity_orientation.pitch, current_acceleration_);

    entity_->SetVelocity(velocity_vector);
    entity_->SetAcceleration(acceleration_vector);
}

bool FollowTrajectoryWithSpeedControlUnit::HasFinished() const
{
    return traverser_ != nullptr && traverser_->IsLastPointReached();
}

}  // namespace astas::environment::controller
