/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_HOSTVEHICLEINTERFACECONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_HOSTVEHICLEINTERFACECONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/TrafficCommand/traffic_command_builder.h"

#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::controller
{

class HostVehicleInterfaceControlUnit : public IAbstractControlUnit
{
  public:
    HostVehicleInterfaceControlUnit(const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints,
                                    mantle_api::ILaneLocationQueryService* map_query_service,
                                    traffic_command::TrafficCommandBuilder* traffic_command_builder);

    void StepControlUnit() override;

    std::unique_ptr<IControlUnit> Clone() const override;

    bool HasFinished() const override;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_HOSTVEHICLEINTERFACECONTROLUNIT_H
