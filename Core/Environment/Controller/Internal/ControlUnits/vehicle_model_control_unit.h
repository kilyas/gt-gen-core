/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_VEHICLEMODELCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_VEHICLEMODELCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/plausibility_check.h"
#include "Core/Environment/Controller/Internal/Utilities/wheel_contact.h"
#include "Core/Environment/Host/host_vehicle_model.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <memory>

namespace astas::environment::controller
{

class VehicleModelControlUnit : public IAbstractControlUnit
{
  public:
    VehicleModelControlUnit(host::HostVehicleModel* vehicle_model, const map::AstasMap* map);

    void SetEntity(mantle_api::IEntity& entity) override;
    void StepControlUnit() override;
    std::unique_ptr<IControlUnit> Clone() const override;
    bool HasFinished() const override;

  private:
    void UpdateEntityFromVehicleModelOut();
    void UpdateVehicleModelIn();
    void UpdateWheelStates();

    double GetFrictionAt(const mantle_api::Vec3<units::length::meter_t>& position) const;

    WheelContact wheel_contact_{};
    mantle_api::Time previous_vehicle_model_out_timestamp_{-1};

    host::VehicleModelIn& vehicle_model_in_;
    const host::VehicleModelOut& vehicle_model_out_;
    const map::AstasMap* map_;

    plausibility_check::PlausibilityCheck plausibility_check_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_VEHICLEMODELCONTROLUNIT_H
