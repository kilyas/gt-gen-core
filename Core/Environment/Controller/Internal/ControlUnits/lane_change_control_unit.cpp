/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/lane_change_control_unit.h"

#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Utility/position_utils.h"

namespace astas::environment::controller
{

LaneChangeControlUnit::LaneChangeControlUnit(const mantle_api::PerformLaneChangeControlStrategy& control_strategy,
                                             mantle_api::ILaneLocationQueryService* map_query_service,
                                             std::unique_ptr<ITransitionDynamics> transition_dynamics)
    : control_strategy_{control_strategy},
      map_query_service_{map_query_service},
      transition_dynamics_{std::move(transition_dynamics)}
{
    originating_control_strategy_ = mantle_api::ControlStrategyType::kPerformLaneChange;
}

LaneChangeControlUnit::LaneChangeControlUnit(const LaneChangeControlUnit& lane_control_unit)
    : IAbstractControlUnit(lane_control_unit)
{
    originating_control_strategy_ = lane_control_unit.originating_control_strategy_;
}

std::unique_ptr<IControlUnit> LaneChangeControlUnit::Clone() const
{
    return std::make_unique<LaneChangeControlUnit>(*this);
}

void LaneChangeControlUnit::InitControlUnit()
{
    const auto lane_location_provider = dynamic_cast<map::LaneLocationProvider*>(map_query_service_);
    if (lane_location_provider == nullptr)
    {
        throw EnvironmentException("LaneChangeControlUnit lane location provider is not set.");
    }

    const auto entity_position = entity_->GetPosition();
    const auto entity_pose_projected_to_target_lane =
        lane_location_provider->GetProjectedPoseAtLane(entity_position, control_strategy_.target_lane_id);
    if (!entity_pose_projected_to_target_lane)
    {
        throw EnvironmentException(
            "LaneChangeControlUnit cannot find the target position, please adjust the scenario. (Casues can be such as "
            "target lane does not exist. More details please find in the warning messages above).");
    }
    const auto& entity_position_projected_to_target_lane = entity_pose_projected_to_target_lane.value().position;
    const auto total_lateral_distance = service::utility::GetDistance2D(
        entity_position, entity_position_projected_to_target_lane);  // lateral move only in the x-y plane

    const auto reference_lane_location = lane_location_provider->GetLaneLocation(entity_position);
    const auto move_right = service::utility::IsRight2D(entity_position_projected_to_target_lane - entity_position,
                                                        reference_lane_location.direction);

    transition_dynamics_->SetStartVal((move_right ? 1 : -1) * total_lateral_distance);
    transition_dynamics_->SetTargetVal(control_strategy_.target_lane_offset.value());

    route_controller_ = std::make_unique<controller::PathControlUnit>(
        map_query_service_,
        std::vector<mantle_api::Vec3<units::length::meter_t>>{entity_position_projected_to_target_lane});
    route_controller_->SetEntity(*entity_);
    route_controller_->SetControllerDataExchangeContainer(*data_exchange_container_);
}

void LaneChangeControlUnit::StepControlUnit()
{
    if (HasEntityBeenModifiedOutsideOfControlUnit())
    {
        Error("LaneChangeAction cannot work together with external controller(s), please remove it/them.");
    }
    if (!route_controller_)
    {
        throw EnvironmentException("LaneChangeControlUnit: router controller is not initialized.");
    }

    auto transition_dynamics_step_size = mantle_api::TimeToSeconds(current_simulation_time_ - last_simulation_time_);
    if (transition_dynamics_->GetDimension() == mantle_api::Dimension::kDistance)
    {
        transition_dynamics_step_size *= data_exchange_container_->velocity_scalars.back()();
    }
    transition_dynamics_->Step(transition_dynamics_step_size);

    data_exchange_container_->lane_offset_scalars.push_back(units::length::meter_t(transition_dynamics_->Evaluate()));

    route_controller_->Step(current_simulation_time_);
}

bool LaneChangeControlUnit::HasFinished() const
{
    const bool is_transition_finished = transition_dynamics_ != nullptr && transition_dynamics_->IsTargetReached();
    const bool is_route_controller_finished = route_controller_ != nullptr && route_controller_->HasFinished();
    return is_transition_finished || is_route_controller_finished;
}

}  // namespace astas::environment::controller
