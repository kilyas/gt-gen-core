/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/lane_assignment_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/Common/converter_utility.h"
#include "Core/Service/MantleApiExtension/formatting.h"
#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::controller
{

LaneAssignmentControlUnit::LaneAssignmentControlUnit(mantle_api::ILaneLocationQueryService* map_query_service,
                                                     bool is_entity_allowed_to_leave_lane)
    : lane_location_provider_{dynamic_cast<map::LaneLocationProvider*>(map_query_service)},
      is_entity_allowed_to_leave_lane_(is_entity_allowed_to_leave_lane)
{
}

std::unique_ptr<IControlUnit> LaneAssignmentControlUnit::Clone() const
{
    return std::make_unique<LaneAssignmentControlUnit>(*this);
}

void LaneAssignmentControlUnit::StepControlUnit()
{
    ASTAS_PROFILE_SCOPE

    const mantle_api::Vec3<units::length::meter_t>& entity_position = entity_->GetPosition();
    if (!lane_location_provider_->IsPositionOnLane(entity_position) && !is_entity_allowed_to_leave_lane_)
    {
        throw EnvironmentException(
            "A moving object with Id {} was positioned off the road.\nWorld-Coordinate: "
            "{}\nMap-Coordinate: {}",
            entity_->GetUniqueId(),
            entity_->GetPosition(),
            environment::map::GetMapCoordinateString(
                entity_->GetPosition(), lane_location_provider_->GetAstasMap().coordinate_converter.get()));
    }

    // TODO: For now, do not set lane-ids when they are empty. This is due to the fact that:
    // When placing the entity (entity-placer.cpp) the position, lane-id, and orientation is set. After that, the
    // entity is shifted upwards ("placed on the road"). However, when spawning an entity at the very beginning of a
    // road (i.e. s=0.0) it might be, that the new position is not withing the road-geometry anymore. This means, when
    // this controller is stepped, it will not find an assigned lane, and the simulation will quit with an exception.
    //
    // There are several solutions for this:
    // a) The vertical shift is handled in an own controller (running after all other controllers have been stepped).
    // This is a bigger change and includes a lot of refactoring.
    // b) We do not allow a vehicle to be spawned somewhere halfway off the road: i.e. when placing at s=0.0 the rear
    // tires are not on the road. If the full vehicle (all 4 tires) needs to be placed withing the road geometry, then
    // this problem of not getting the assigned land id does not happen anymore, and we could also remove the
    // if-statement below. This might prevent some use-cases (i.e. a parking car on the side of the road, where 2 tires
    // are not within the road geometry). Withing this fix, the AllowInvalidLaneLocation setting could be re-worked,
    // since this concept is by now outdated, and not really used in the new architecture.
    const auto sorted_lane_ids =
        lane_location_provider_->GetSortedLaneIdsAtPosition(entity_position, entity_->GetOrientation());
    if (!sorted_lane_ids.empty())
    {
        entity_->SetAssignedLaneIds(sorted_lane_ids);
    }
}

bool LaneAssignmentControlUnit::HasFinished() const
{
    return false;
}

}  // namespace astas::environment::controller
