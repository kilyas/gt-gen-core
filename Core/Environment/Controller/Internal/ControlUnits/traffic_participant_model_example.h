/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTMODELEXAMPLE_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTMODELEXAMPLE_H

#include <OsiTrafficParticipant/i_traffic_participant_model.h>
#include <astas_osi_sensorview.pb.h>
#include <astas_osi_trafficupdate.pb.h>
#include <boost/dll/alias.hpp>

#include <map>
#include <memory>
#include <string>

namespace astas::environment::controller
{

using osi_traffic_participant::ITrafficParticipantModel;

class TpmExample : public ITrafficParticipantModel
{
  public:
    TpmExample() = default;

    TpmExample(TpmExample const&) = delete;
    TpmExample& operator=(const TpmExample&) = delete;
    TpmExample(TpmExample&&) = delete;
    TpmExample& operator=(TpmExample&&) = delete;

    /// @brief Destructor.
    ~TpmExample() override;

    static std::shared_ptr<ITrafficParticipantModel> Create(const std::map<std::string, std::string>& parameters);

    /// @brief Identifies the OSI entity from astas_osi3::GroundTruth entities in the provided \p sensor_view and
    ///        provides a new state.
    /// @param delta_time  Delta time between the step in milliseconds.
    /// @param sensor_view astas_osi3::SensorView containing astas_osi3::GroundTruth containing all the entities
    ///                    (including the controlled entity).
    astas_osi3::TrafficUpdate Update(const std::chrono::milliseconds& delta_time,
                                     const astas_osi3::SensorView& sensor_view) override;

    /// @brief Indicates that the controlled entity will be updated or not.
    /// @return true if the entity will not be updated in future cycles, i.e. when the road has ended. False otherwise.
    bool HasFinished() const override;

  private:
    int num_spins_{0};
};

// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-avoid-non-const-global-variables,misc-definitions-in-headers)
BOOST_DLL_ALIAS(TpmExample::Create, CreateTpm)

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICPARTICIPANTMODELEXAMPLE_H
