/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICLIGHTCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICLIGHTCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Controller/Internal/Utilities/traffic_light_state_machine.h"
#include "Core/Environment/Entities/traffic_light_entity.h"

namespace astas::environment::controller
{

class TrafficLightControlUnit : public IAbstractControlUnit
{
  public:
    TrafficLightControlUnit(std::vector<mantle_api::TrafficLightPhase> light_phases, bool repeat);

    TrafficLightControlUnit(TrafficLightControlUnit const&) = default;
    TrafficLightControlUnit& operator=(const TrafficLightControlUnit&) = delete;
    TrafficLightControlUnit(TrafficLightControlUnit&&) = delete;
    TrafficLightControlUnit& operator=(TrafficLightControlUnit&&) = delete;
    ~TrafficLightControlUnit() override = default;

    std::unique_ptr<IControlUnit> Clone() const override;

    void SetEntity(mantle_api::IEntity& entity) override;

    void StepControlUnit() override;

    bool HasFinished() const override;

  private:
    entities::TrafficLightEntity* traffic_light_entity_{nullptr};
    TrafficLightStateMachine state_machine_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_TRAFFICLIGHTCONTROLUNIT_H
