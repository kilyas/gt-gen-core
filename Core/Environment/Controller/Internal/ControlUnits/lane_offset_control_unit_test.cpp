/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/lane_offset_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_mps_cu;

class LaneOffsetControlUnitTest : public testing::Test
{
  protected:
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    const std::vector<mantle_api::SplineSection<units::length::meter>> empty_lateral_offsets{};
};

/// Creation tests
TEST_F(LaneOffsetControlUnitTest, GivenValidInputs_WhenCreatingLaneOffsetControlUnit_ThenNoExceptionThrown)
{
    EXPECT_NO_THROW(LaneOffsetControlUnit lane_offset_control_unit(empty_lateral_offsets));
}

/// Clone tests
TEST_F(LaneOffsetControlUnitTest, GivenLaneOffsetControlUnit_WhenClone_ThenCopyCreated)
{
    auto control_unit_ptr = std::make_unique<LaneOffsetControlUnit>(empty_lateral_offsets);

    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

/// Step tests

TEST_F(LaneOffsetControlUnitTest, GivenEmptyLaneOffsets_WhenControllerIsStepped_ThenExchangeDataHasNoLaneOffset)
{
    LaneOffsetControlUnit lane_offset_control_unit(empty_lateral_offsets);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    lane_offset_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(0_m, data_exchange_container.lane_offset_scalars.back());
}

TEST_F(LaneOffsetControlUnitTest, GivenNoLaneOffsetsAndNoEntity_WhenControllerIsStepped_ThenThrow)
{
    LaneOffsetControlUnit lane_offset_control_unit(&lane_location_provider_);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    EXPECT_THROW(lane_offset_control_unit.Step(mantle_api::Time{0}), environment::EnvironmentException);
}

TEST_F(LaneOffsetControlUnitTest, GivenNoLaneOffsetsAndEntityNotOnRoad_WhenControllerIsStepped_ThenThrow)
{
    LaneOffsetControlUnit lane_offset_control_unit(&lane_location_provider_);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    environment::entities::VehicleEntity entity{0, ""};
    entity.SetPosition({0_m, 20_m, 0_m});
    lane_offset_control_unit.SetEntity(entity);

    EXPECT_THROW(lane_offset_control_unit.Step(mantle_api::Time{0}), environment::EnvironmentException);
}

TEST_F(LaneOffsetControlUnitTest, GivenNoLaneOffsetsAndEntityOnRoadWithoutProperties_WhenControllerIsStepped_ThenThrow)
{
    LaneOffsetControlUnit lane_offset_control_unit(&lane_location_provider_);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    environment::entities::VehicleEntity entity{0, ""};
    entity.SetPosition({5_m, 1_m, 0_m});
    lane_offset_control_unit.SetEntity(entity);

    EXPECT_THROW(lane_offset_control_unit.Step(mantle_api::Time{0}), environment::EnvironmentException);
}

TEST_F(LaneOffsetControlUnitTest,
       GivenNoLaneOffsetsAndEntityOnRoad_WhenControllerIsStepped_ThenDataExchangeContainerContainsCurrentLaneOffset)
{
    LaneOffsetControlUnit lane_offset_control_unit(&lane_location_provider_);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    environment::entities::VehicleEntity entity{0, ""};
    entity.SetPosition({5_m, 1_m, 0_m});
    mantle_api::VehicleProperties vehicle_properties;
    entity.SetProperties(std::make_unique<mantle_api::VehicleProperties>(vehicle_properties));
    lane_offset_control_unit.SetEntity(entity);

    EXPECT_NO_THROW(lane_offset_control_unit.Step(mantle_api::Time{0}));
    EXPECT_EQ(1, data_exchange_container.lane_offset_scalars.size());
    EXPECT_EQ(1.0_m, data_exchange_container.lane_offset_scalars.back());

    EXPECT_NO_THROW(lane_offset_control_unit.Step(mantle_api::Time{1000}));
    EXPECT_EQ(1.0_m, data_exchange_container.lane_offset_scalars.back());
}

TEST_F(LaneOffsetControlUnitTest, GivenLaneOffsets_WhenControllerIsStepped_ThenEntityLaneOffsetIsUpdated)
{
    mantle_api::SplineSection<units::length::meter> t_offset_profile_1{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_mps_cu, 0_mps_sq, 1_mps, 0_m}};
    mantle_api::SplineSection<units::length::meter> t_offset_profile_2{
        mantle_api::Time{2'000}, mantle_api::Time{3'000}, {0_mps_cu, 0_mps_sq, 0_mps, 1_m}};
    mantle_api::SplineSection<units::length::meter> t_offset_profile_3{
        mantle_api::Time{3'000}, mantle_api::Time{4'000}, {0_mps_cu, 0_mps_sq, -2_mps, 1_m}};
    const std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offsets{
        t_offset_profile_1, t_offset_profile_2, t_offset_profile_3};

    std::map<int, units::length::meter_t> expected_offsets;
    expected_offsets.emplace(0, 0.0_m);
    expected_offsets.emplace(1, 0.5_m);
    expected_offsets.emplace(2, 1.0_m);
    expected_offsets.emplace(3, 1.0_m);
    expected_offsets.emplace(4, -1.0_m);

    LaneOffsetControlUnit lane_offset_control_unit(lateral_offsets);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    lane_offset_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(expected_offsets.at(0), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{1500});
    EXPECT_EQ(expected_offsets.at(1), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{2000});
    EXPECT_EQ(expected_offsets.at(2), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{3000});
    EXPECT_EQ(expected_offsets.at(3), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{4000});
    EXPECT_EQ(expected_offsets.at(4), data_exchange_container.lane_offset_scalars.back());
}

TEST_F(LaneOffsetControlUnitTest,
       GivenLaneOffsets_WhenControllerIsStepped100Times_Then100LaneOffsetScalarsAreCalculated)
{
    mantle_api::SplineSection<units::length::meter> t_offset_profile_1{
        mantle_api::Time{0}, mantle_api::Time{2'000}, {0_mps_cu, 0_mps_sq, 1_mps, 0_m}};

    LaneOffsetControlUnit lane_offset_control_unit({t_offset_profile_1});

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    lane_offset_control_unit.Step(mantle_api::Time{0});
    ASSERT_TRUE(data_exchange_container.lane_offset_scalars.size() == 1);

    data_exchange_container.Reset();
    lane_offset_control_unit.Step(mantle_api::Time{100});
    ASSERT_TRUE(data_exchange_container.lane_offset_scalars.size() == 100);
}

TEST_F(LaneOffsetControlUnitTest,
       GivenFirstStepAfterStartOfSimulation_WhenControllerIsStepped_ThenEntityLaneOffsetIsCorrectlyUpdated)
{
    mantle_api::SplineSection<units::length::meter> t_offset_profile_1{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_mps_cu, 0_mps_sq, 1_mps, 0_m}};
    mantle_api::SplineSection<units::length::meter> t_offset_profile_2{
        mantle_api::Time{2'000}, mantle_api::Time{3'000}, {0_mps_cu, 0_mps_sq, 0_mps, 1_m}};
    mantle_api::SplineSection<units::length::meter> t_offset_profile_3{
        mantle_api::Time{3'000}, mantle_api::Time{4'000}, {0_mps_cu, 0_mps_sq, -2_mps, 1_m}};
    const std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offsets{
        t_offset_profile_1, t_offset_profile_2, t_offset_profile_3};

    std::map<int, units::length::meter_t> expected_offsets;
    expected_offsets.emplace(0, 0.0_m);
    expected_offsets.emplace(1, 0.5_m);
    expected_offsets.emplace(2, 1.0_m);
    expected_offsets.emplace(3, 1.0_m);
    expected_offsets.emplace(4, -1.0_m);

    LaneOffsetControlUnit lane_offset_control_unit(lateral_offsets);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    lane_offset_control_unit.Step(mantle_api::Time{1000});
    EXPECT_EQ(expected_offsets.at(0), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{2500});
    EXPECT_EQ(expected_offsets.at(1), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{3000});
    EXPECT_EQ(expected_offsets.at(2), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{4000});
    EXPECT_EQ(expected_offsets.at(3), data_exchange_container.lane_offset_scalars.back());

    lane_offset_control_unit.Step(mantle_api::Time{5000});
    EXPECT_EQ(expected_offsets.at(4), data_exchange_container.lane_offset_scalars.back());
}

/// HasFinished tests

TEST_F(LaneOffsetControlUnitTest, GivenEmptyLaneOffsets_WhenHasFinished_ThenReturnTrue)
{
    LaneOffsetControlUnit lane_offset_control_unit(empty_lateral_offsets);

    EXPECT_TRUE(lane_offset_control_unit.HasFinished());
}

TEST_F(LaneOffsetControlUnitTest, GivenNoLaneOffsets_WhenHasFinished_ThenAlwaysReturnFalse)
{
    LaneOffsetControlUnit lane_offset_control_unit(&lane_location_provider_);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);
    environment::entities::VehicleEntity entity{0, ""};
    entity.SetPosition({5_m, 1_m, 0_m});
    mantle_api::VehicleProperties vehicle_properties;
    entity.SetProperties(std::make_unique<mantle_api::VehicleProperties>(vehicle_properties));
    lane_offset_control_unit.SetEntity(entity);

    EXPECT_FALSE(lane_offset_control_unit.HasFinished());

    lane_offset_control_unit.Step(mantle_api::Time{0});
    EXPECT_FALSE(lane_offset_control_unit.HasFinished());

    lane_offset_control_unit.Step(mantle_api::Time{500});
    EXPECT_FALSE(lane_offset_control_unit.HasFinished());
}

TEST_F(LaneOffsetControlUnitTest, GivenCurrentSimulationTimeBeforeLaneOffsets_WhenHasFinished_ThenReturnFalse)
{
    mantle_api::SplineSection<units::length::meter> lane_offset_profile{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_mps_cu, 0_mps_sq, 1_mps, 0_m}};
    const std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offsets{lane_offset_profile};

    LaneOffsetControlUnit lane_offset_control_unit(lateral_offsets);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    lane_offset_control_unit.Step(mantle_api::Time{0});
    lane_offset_control_unit.Step(mantle_api::Time{500});

    EXPECT_FALSE(lane_offset_control_unit.HasFinished());
}

TEST_F(LaneOffsetControlUnitTest, GivenCurrentSimulationTimeWithinLaneOffsets_WhenHasFinished_ThenReturnFalse)
{
    mantle_api::SplineSection<units::length::meter> lane_offset_profile{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_mps_cu, 0_mps_sq, 1_mps, 0_m}};
    const std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offsets{lane_offset_profile};

    LaneOffsetControlUnit lane_offset_control_unit(lateral_offsets);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    lane_offset_control_unit.Step(mantle_api::Time{0});
    lane_offset_control_unit.Step(mantle_api::Time{1500});

    EXPECT_FALSE(lane_offset_control_unit.HasFinished());
}

TEST_F(LaneOffsetControlUnitTest, GivenAllLaneOffsetsEndBeforeCurrentSimulationTime_WhenHasFinished_ThenReturnTrue)
{
    mantle_api::SplineSection<units::length::meter> lane_offset_profile{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_mps_cu, 0_mps_sq, 1_mps, 0_m}};
    const std::vector<mantle_api::SplineSection<units::length::meter>> lateral_offsets{lane_offset_profile};

    LaneOffsetControlUnit lane_offset_control_unit(lateral_offsets);

    ControllerDataExchangeContainer data_exchange_container{};
    lane_offset_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    lane_offset_control_unit.Step(mantle_api::Time{0});
    lane_offset_control_unit.Step(mantle_api::Time{3000});

    EXPECT_TRUE(lane_offset_control_unit.HasFinished());
}

}  // namespace astas::environment::controller
