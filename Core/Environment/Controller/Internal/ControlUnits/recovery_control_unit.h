/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_RECOVERYCONTROLUNIT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_RECOVERYCONTROLUNIT_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_abstract_control_unit.h"
#include "Core/Environment/Host/host_vehicle_interface.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include <boost/circular_buffer.hpp>

namespace astas::environment::controller
{

class RecoveryControlUnit : public IAbstractControlUnit
{
  public:
    RecoveryControlUnit(host::HostVehicleInterface* host_interface,
                        mantle_api::ILaneLocationQueryService* map_query_service);

    std::unique_ptr<IControlUnit> Clone() const override;
    void StepControlUnit() override;
    bool HasFinished() const override;

  private:
    bool IsRecoverySuitable(const std::vector<const environment::map::Lane*>& lanes);
    void RecoverToLastValidLocationIfAllowed();
    void RecoverHostVehiclePose();
    bool RecoveryDeadlockDetected();

    host::HostVehicleInterface* host_vehicle_interface_;
    const map::LaneLocationProvider* lane_location_provider_;

    static constexpr std::size_t max_recovery_attempts{10};
    static constexpr std::size_t max_ticks_for_recovery{20};
    boost::circular_buffer<bool> recovery_in_tick_{max_ticks_for_recovery};
    environment::map::LaneLocation last_recoverable_lane_location_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_RECOVERYCONTROLUNIT_H
