/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/PathFinding/path_finder.h"
#include "Core/Service/Utility/string_utils.h"

namespace astas::environment::controller
{

HostVehicleInterfaceControlUnit::HostVehicleInterfaceControlUnit(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints,
    mantle_api::ILaneLocationQueryService* map_query_service,
    traffic_command::TrafficCommandBuilder* traffic_command_builder)
{
    const map::LaneLocationProvider* lane_location_provider =
        dynamic_cast<map::LaneLocationProvider*>(map_query_service);

    std::vector<std::uint64_t> lanes{};

    environment::path_finding::PathFinder path_finder(lane_location_provider->GetAstasMap());

    if (waypoints.size() >= 2)
    {
        const auto optional_path = path_finder.ComputeRoute(waypoints);

        if (optional_path)
        {
            for (const auto& entry : *optional_path)
            {
                lanes.push_back(entry->lane->id);
            }
        }
        else
        {
            Warn(
                "HostVehicleInterfaceControlUnit was created with an empty path. No path was found for the given "
                "waypoints. This indicates that either the waypoints are not correctly defined in the scenario file, "
                "or there is a topology connection error in the road network.");
        }

        if (lanes.size() > 0)
        {
            const auto lanes_string = service::utility::VectorToString(lanes);
            traffic_command_builder->AddCustomTrafficActionForEntity(0, "route", lanes_string);
        }
    }
}

void HostVehicleInterfaceControlUnit::StepControlUnit() {}

std::unique_ptr<IControlUnit> HostVehicleInterfaceControlUnit::Clone() const
{
    return std::make_unique<HostVehicleInterfaceControlUnit>(*this);
}

bool HostVehicleInterfaceControlUnit::HasFinished() const
{
    return false;
}

}  // namespace astas::environment::controller
