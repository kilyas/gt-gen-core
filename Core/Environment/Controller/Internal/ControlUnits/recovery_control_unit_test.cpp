/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/recovery_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;

class RecoveryControlUnitTest : public testing::Test
{
  protected:
    RecoveryControlUnitTest()
        : entity_(std::make_unique<entities::VehicleEntity>(0, "host")),
          host_vehicle_interface_(std::make_unique<host::HostVehicleInterface>())
    {
        entity_->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
        entity_->SetPosition(position_on_road);
        entity_->SetAssignedLaneIds({3});
    }

    std::unique_ptr<mantle_api::IEntity> entity_;
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    std::unique_ptr<host::HostVehicleInterface> host_vehicle_interface_{nullptr};

    const mantle_api::Vec3<units::length::meter_t> position_on_road{0.0_m, 0.0_m, 0.0_m};
    const mantle_api::Vec3<units::length::meter_t> position_off_road{-50.0_m, -50.0_m, 0.0_m};
};

TEST_F(RecoveryControlUnitTest, GivenRecoveryControlUnit_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);

    EXPECT_FALSE(recovery_control_unit->HasFinished());
}

TEST_F(RecoveryControlUnitTest, GivenRecoveryControlUnit_WhenClone_ThenCopyCreated)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);

    auto copy = recovery_control_unit->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(recovery_control_unit.get(), copy.get());
}

TEST_F(RecoveryControlUnitTest, GivenEntityOverRoad_WhenRecoveryControlUnitStepped_ThenEntityIsNotRecovererd)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);
    recovery_control_unit->SetEntity(*entity_);

    recovery_control_unit->Step(mantle_api::Time{0});

    EXPECT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
}

TEST_F(RecoveryControlUnitTest, GivenEntityNeverOverRoad_WhenRecoveryControlUnitStepped_ThenThrows)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);
    entity_->SetPosition(position_off_road);
    recovery_control_unit->SetEntity(*entity_);

    EXPECT_THROW(recovery_control_unit->Step(mantle_api::Time{0}), EnvironmentException);
}

TEST_F(RecoveryControlUnitTest,
       GivenEntityLeavesRoad_WhenRecoveryControlUnitStepped_ThenEntityIsRecovererdToPreviousLocation)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);
    recovery_control_unit->SetEntity(*entity_);

    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
    const auto last_position_on_road = entity_->GetPosition();

    entity_->SetPosition(position_off_road);
    recovery_control_unit->Step(mantle_api::Time{0});

    ASSERT_TRUE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
    EXPECT_TRIPLE(last_position_on_road, entity_->GetPosition());
}

TEST_F(RecoveryControlUnitTest, GivenEntityRepeatedlyLeavesRoad_WhenRecoveryControlUnitSteped_ThenDeadlockIsDetected)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);
    recovery_control_unit->SetEntity(*entity_);

    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());

    const int max_recovery_attempts = 10;
    mantle_api::Vec3<units::length::meter_t> position_off_road{-50.0_m, -30.0_m, 0.0_m};
    for (int i = 0; i < max_recovery_attempts; i++)
    {
        entity_->SetPosition(position_off_road);
        recovery_control_unit->Step(mantle_api::Time{0});
        ASSERT_TRUE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
    }

    entity_->SetPosition(position_off_road);
    EXPECT_THROW(recovery_control_unit->Step(mantle_api::Time{0}), EnvironmentException);
}

TEST_F(RecoveryControlUnitTest,
       GivenHostLeavesTheRoad_WhenStepAfterRecovery_ThenHostPoseRecoveredInLastStepReturnsFalseAgain)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);
    recovery_control_unit->SetEntity(*entity_);

    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());

    entity_->SetPosition(position_off_road);
    recovery_control_unit->Step(mantle_api::Time{0});

    ASSERT_TRUE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());

    entity_->SetPosition(position_on_road);
    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
}

TEST_F(RecoveryControlUnitTest, GivenHostOnNormalLane_WhenDrivesOverShoulderLaneAndLeavesRoad_ThenRecoveredOnNormalLane)
{
    std::unique_ptr<environment::map::AstasMap> map_with_shoulder_lane =
        test_utils::MapCatalogue::MapWithDrivingAndShoulderLaneWithSharedBoundary();
    map::LaneLocationProvider lane_location_provider{*map_with_shoulder_lane};
    entity_->SetAssignedLaneIds({2});

    mantle_api::Vec3<units::length::meter_t> last_position_on_normal_lane{0_m, 1_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> position_on_shoulder_lane{0.0_m, 4.0_m, 0.0_m};

    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider);
    recovery_control_unit->SetEntity(*entity_);

    entity_->SetPosition(last_position_on_normal_lane);
    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());

    entity_->SetPosition(position_on_shoulder_lane);
    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());

    entity_->SetPosition(position_off_road);
    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_TRUE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
    ASSERT_EQ(entity_->GetAssignedLaneIds().at(0), 2);
    EXPECT_TRIPLE(position_on_road, entity_->GetPosition());
}

TEST_F(RecoveryControlUnitTest,
       HostIsRecoveredUntilDeadlockAndRepositionedOnRoad_HostLeavesRoadAgain_RecoveryDeadlockStillDetected)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);
    recovery_control_unit->SetEntity(*entity_);

    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());

    const int max_recovery_attempts = 10;

    entity_->SetPosition(position_on_road);
    EXPECT_NO_THROW(recovery_control_unit->Step(mantle_api::Time{0}));

    for (int i = 0; i < max_recovery_attempts; i++)
    {
        entity_->SetPosition(position_off_road);
        EXPECT_NO_THROW(recovery_control_unit->Step(mantle_api::Time{0}));
        ASSERT_TRUE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
    }

    entity_->SetPosition(position_on_road);
    EXPECT_NO_THROW(recovery_control_unit->Step(mantle_api::Time{0}));

    entity_->SetPosition(position_off_road);
    EXPECT_THROW(recovery_control_unit->Step(mantle_api::Time{0}), EnvironmentException);
}

TEST_F(RecoveryControlUnitTest,
       GivenHostLeavesTheRoadMultipleSteps_WhenRecoveryHappensAfterBufferHasCleared_ThenNoRecoveryDeadlock)
{
    auto recovery_control_unit =
        std::make_unique<RecoveryControlUnit>(host_vehicle_interface_.get(), &lane_location_provider_);
    recovery_control_unit->SetEntity(*entity_);

    recovery_control_unit->Step(mantle_api::Time{0});
    ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());

    const int max_recovery_attempts = 10;
    const int max_ticks_for_recovery = 20;

    entity_->SetPosition(position_on_road);
    EXPECT_NO_THROW(recovery_control_unit->Step(mantle_api::Time{0}));

    for (int i = 0; i < max_recovery_attempts; i++)
    {
        entity_->SetPosition(position_off_road);
        EXPECT_NO_THROW(recovery_control_unit->Step(mantle_api::Time{0}));
        ASSERT_TRUE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
    }

    for (int i = 0; i < max_ticks_for_recovery; i++)
    {
        entity_->SetPosition(position_on_road);
        EXPECT_NO_THROW(recovery_control_unit->Step(mantle_api::Time{0}));
        ASSERT_FALSE(host_vehicle_interface_.get()->HostPoseRecoveredInLastStep());
    }

    entity_->SetPosition(position_on_road);
    EXPECT_NO_THROW(recovery_control_unit->Step(mantle_api::Time{0}));
}

}  // namespace astas::environment::controller
