/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/maneuver_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;
using units::literals::operator""_rad_per_s_cu;

class ManeuverControlUnitTest : public testing::Test
{
  protected:
    ManeuverControlUnitTest() = default;
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    std::vector<mantle_api::SplineSection<units::angle::radian>> splines_{};
    std::unique_ptr<mantle_api::IEntity> entity_ = std::make_unique<entities::VehicleEntity>(0, "host");
    ControllerDataExchangeContainer data_exchange_container_{{0.0_mps},
                                                             0.0_mps_sq,
                                                             0.0_rad_per_s,
                                                             0.0_rad_per_s_sq,
                                                             {0.0_m}};
};

/// Creation tests
TEST_F(ManeuverControlUnitTest, GivenValidInputs_WhenCreatingManeuverControlUnit_ThenNoExceptionThrown)
{
    EXPECT_NO_THROW(ManeuverControlUnit maneuver_control_unit(splines_, &lane_location_provider_));
}

/// Clone tests
TEST_F(ManeuverControlUnitTest, GivenManeuverControlUnit_WhenClone_ThenCopyCreated)
{
    auto control_unit_ptr = std::make_unique<ManeuverControlUnit>(splines_, &lane_location_provider_);

    auto copy = control_unit_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_unit_ptr.get(), copy.get());
}

/// Step tests
TEST_F(ManeuverControlUnitTest, GivenHeadingSplines_WhenStep_ThenEntityHasNewHeading)
{
    mantle_api::SplineSection<units::angle::radian> heading_spline{
        mantle_api::Time{0}, mantle_api::Time{2'000}, {0_rad_per_s_cu, 0_rad_per_s_sq, 1_rad_per_s, 0_rad}};
    const std::vector<mantle_api::SplineSection<units::angle::radian>> lateral_offsets{heading_spline};

    ManeuverControlUnit maneuver_control_unit(lateral_offsets, &lane_location_provider_);

    maneuver_control_unit.SetControllerDataExchangeContainer(data_exchange_container_);
    maneuver_control_unit.SetEntity(*entity_);

    maneuver_control_unit.Step(mantle_api::Time{0});

    data_exchange_container_.velocity_scalars = std::vector<units::velocity::meters_per_second_t>(500, 1.0_mps);
    maneuver_control_unit.Step(mantle_api::Time{500});

    EXPECT_TRIPLE(mantle_api::Orientation3<units::angle::radian_t>(0.5_rad, 0_rad, 0_rad), entity_->GetOrientation())
    EXPECT_TRIPLE(
        mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>(1_rad_per_s, 0_rad_per_s, 0_rad_per_s),
        entity_->GetOrientationRate())
    EXPECT_TRIPLE(mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>(
                      2_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq),
                  entity_->GetOrientationAcceleration())
}

TEST_F(ManeuverControlUnitTest, GivenEntitySpawnsNotAtTimeZero_WhenStep_ThenEntityHasNewHeading)
{
    mantle_api::SplineSection<units::angle::radian> heading_spline{
        mantle_api::Time{0}, mantle_api::Time{2'000}, {0_rad_per_s_cu, 0_rad_per_s_sq, 1_rad_per_s, 5_rad}};
    const std::vector<mantle_api::SplineSection<units::angle::radian>> lateral_offsets{heading_spline};

    ManeuverControlUnit maneuver_control_unit(lateral_offsets, &lane_location_provider_);

    maneuver_control_unit.SetControllerDataExchangeContainer(data_exchange_container_);
    maneuver_control_unit.SetEntity(*entity_);

    data_exchange_container_.velocity_scalars = std::vector<units::velocity::meters_per_second_t>(500, 1.0_mps);
    maneuver_control_unit.Step(mantle_api::Time{500});

    EXPECT_TRIPLE(mantle_api::Orientation3<units::angle::radian_t>(5_rad, 0_rad, 0_rad), entity_->GetOrientation())
    EXPECT_TRIPLE(
        mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>(0_rad_per_s, 0_rad_per_s, 0_rad_per_s),
        entity_->GetOrientationRate())
    EXPECT_TRIPLE(mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>(
                      0_rad_per_s_sq, 0_rad_per_s_sq, 0_rad_per_s_sq),
                  entity_->GetOrientationAcceleration())
}

/// HasFinished tests
TEST_F(ManeuverControlUnitTest, GivenNoHeadingSplines_WhenHasFinished_ThenReturnTrue)
{
    ManeuverControlUnit maneuver_control_unit(splines_, &lane_location_provider_);

    EXPECT_TRUE(maneuver_control_unit.HasFinished());
}

TEST_F(ManeuverControlUnitTest, GivenCurrentSimulationTimeBeforeHeadingSplines_WhenHasFinished_ThenReturnFalse)
{
    mantle_api::SplineSection<units::angle::radian> heading_spline{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_rad_per_s_cu, 0_rad_per_s_sq, 1_rad_per_s, 0_rad}};
    const std::vector<mantle_api::SplineSection<units::angle::radian>> lateral_offsets{heading_spline};

    ManeuverControlUnit maneuver_control_unit(lateral_offsets, &lane_location_provider_);

    maneuver_control_unit.SetControllerDataExchangeContainer(data_exchange_container_);
    maneuver_control_unit.SetEntity(*entity_);

    maneuver_control_unit.Step(mantle_api::Time{0});

    data_exchange_container_.velocity_scalars = std::vector<units::velocity::meters_per_second_t>(500, 0.0_mps);
    maneuver_control_unit.Step(mantle_api::Time{500});

    EXPECT_FALSE(maneuver_control_unit.HasFinished());
}

TEST_F(ManeuverControlUnitTest, GivenCurrentSimulationTimeWithinHeadingSplines_WhenHasFinished_ThenReturnFalse)
{
    mantle_api::SplineSection<units::angle::radian> heading_spline{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_rad_per_s_cu, 0_rad_per_s_sq, 1_rad_per_s, 0_rad}};
    const std::vector<mantle_api::SplineSection<units::angle::radian>> lateral_offsets{heading_spline};

    ManeuverControlUnit maneuver_control_unit(lateral_offsets, &lane_location_provider_);

    maneuver_control_unit.SetControllerDataExchangeContainer(data_exchange_container_);
    maneuver_control_unit.SetEntity(*entity_);

    maneuver_control_unit.Step(mantle_api::Time{0});

    data_exchange_container_.velocity_scalars = std::vector<units::velocity::meters_per_second_t>(1500, 0.0_mps);
    maneuver_control_unit.Step(mantle_api::Time{1500});

    EXPECT_FALSE(maneuver_control_unit.HasFinished());
}

TEST_F(ManeuverControlUnitTest, GivenAllHeadingSplinesEndBeforeCurrentSimulationTime_WhenHasFinished_ThenReturnTrue)
{
    mantle_api::SplineSection<units::angle::radian> heading_spline{
        mantle_api::Time{1'000}, mantle_api::Time{2'000}, {0_rad_per_s_cu, 0_rad_per_s_sq, 1_rad_per_s, 0_rad}};
    const std::vector<mantle_api::SplineSection<units::angle::radian>> lateral_offsets{heading_spline};

    ManeuverControlUnit maneuver_control_unit(lateral_offsets, &lane_location_provider_);

    maneuver_control_unit.SetControllerDataExchangeContainer(data_exchange_container_);
    maneuver_control_unit.SetEntity(*entity_);

    maneuver_control_unit.Step(mantle_api::Time{0});

    data_exchange_container_.velocity_scalars = std::vector<units::velocity::meters_per_second_t>(3000, 0.0_mps);
    maneuver_control_unit.Step(mantle_api::Time{3000});

    EXPECT_TRUE(maneuver_control_unit.HasFinished());
}

}  // namespace astas::environment::controller
