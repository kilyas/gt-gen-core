/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/external_controller_factory.h"

#include "Core/Environment/Controller/Internal/ControlUnits/control_unit_test_utils.h"
#include "Core/Environment/Controller/Internal/composite_controller_sut.h"
#include "Core/Environment/Controller/controller_factory.h"
#include "Core/Environment/Host/host_vehicle_model.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/control_strategy.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{

TEST(
    ExternalControllerFactoryTest,
    GivenExternalConfigWithRecoveryModeNotEnabled_WhenCreate_ThenOnlyHostVehicleInterfaceControlUnitAndVehicleModelControlUnitAreCreated)
{
    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};
    host::HostVehicleModel vehicle_model{};

    auto astas_external_config = std::make_unique<AstasExternalControllerConfig>();
    astas_external_config->recovery_mode_enabled = false;
    astas_external_config->map_query_service = &lane_location_provider;
    astas_external_config->host_vehicle_model = &vehicle_model;

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(astas_external_config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(2, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::HostVehicleInterfaceControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::VehicleModelControlUnit>(controller->GetControlUnits()));
}

TEST(
    ExternalControllerFactoryTest,
    GivenExternalConfigWithRecoveryModeEnabled_WhenCreate_ThenHostVehicleInterfaceControlUnitAndVehicleModelControlUnitAndRecoveryControlUnitAreCreated)
{
    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};
    host::HostVehicleModel vehicle_model{};

    auto astas_external_config = std::make_unique<AstasExternalControllerConfig>();
    astas_external_config->recovery_mode_enabled = true;
    astas_external_config->map_query_service = &lane_location_provider;
    astas_external_config->host_vehicle_model = &vehicle_model;

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(astas_external_config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(3, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::HostVehicleInterfaceControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::VehicleModelControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::RecoveryControlUnit>(controller->GetControlUnits()));
}

TEST(ExternalControllerFactoryTest, GivenTpmConfig_WhenCreate_ThenTpmControlUnitCreated)
{
    auto tpm_config = std::make_unique<TrafficParticipantControlUnitConfig>();
    tpm_config->name = "traffic_participant_model_example";

    auto astas_map{test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    tpm_config->astas_map = astas_map.get();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(tpm_config), false);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(1, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::TrafficParticipantControlUnit>(controller->GetControlUnits()));
}

TEST(ExternalControllerFactoryTest, GivenUnsupportedConfig_WhenCreate_ThenEnvironmentExceptionIsThrown)
{
    auto config = std::make_unique<mantle_api::IControllerConfig>();

    EXPECT_THROW(ControllerFactory::Create<CompositeControllerSUT>(0, std::move(config), true), EnvironmentException);
}

}  // namespace astas::environment::controller
