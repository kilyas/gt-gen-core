/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/internal_controller_factory.h"

#include "Core/Environment/Controller/Internal/ControlUnits/control_unit_test_utils.h"
#include "Core/Environment/Controller/Internal/composite_controller_sut.h"
#include "Core/Environment/Controller/controller_factory.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <MantleAPI/Traffic/control_strategy.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;

TEST(InternalControllerFactoryTest,
     GivenFollowVelocitySplineStrategyAndRouteDefinition_WhenCreate_ThenVelocityAndPathControlUnitsAreCreated)
{
    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};

    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->map_query_service = &lane_location_provider;

    auto velocity_spline_control_strategy = std::make_unique<mantle_api::FollowVelocitySplineControlStrategy>();
    config->control_strategies.push_back(std::move(velocity_spline_control_strategy));

    config->route_definition.waypoints.push_back({mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}, {}});
    config->route_definition.waypoints.push_back({mantle_api::Vec3<units::length::meter_t>{42_m, 0_m, 0_m}, {}});

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(2, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::VelocityControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::PathControlUnit>(controller->GetControlUnits()));
}

TEST(
    InternalControllerFactoryTest,
    GivenFollowVelocitySplineAndLaneOffsetSplineStrategyAndRouteDefinition_WhenCreate_ThenVelocityAndPathAndLaneOffsetControlUnitsAreCreated)
{
    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};

    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->map_query_service = &lane_location_provider;

    auto velocity_spline_control_strategy = std::make_unique<mantle_api::FollowVelocitySplineControlStrategy>();
    config->control_strategies.push_back(std::move(velocity_spline_control_strategy));

    config->route_definition.waypoints.push_back({mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}, {}});
    config->route_definition.waypoints.push_back({mantle_api::Vec3<units::length::meter_t>{42_m, 0_m, 0_m}, {}});

    using units::literals::operator""_mps_cu;
    using units::literals::operator""_mps_sq;
    using units::literals::operator""_mps;

    auto lane_offset_control_strategy = std::make_unique<mantle_api::FollowLateralOffsetSplineControlStrategy>();
    lane_offset_control_strategy->lateral_offset_splines.push_back(mantle_api::SplineSection<units::length::meter>{
        mantle_api::Time{0'000}, mantle_api::Time{2'000}, {0_mps_cu, 0_mps_sq, 0_mps, 1_m}});
    config->control_strategies.push_back(std::move(lane_offset_control_strategy));

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(3, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::VelocityControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::PathControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::LaneOffsetControlUnit>(controller->GetControlUnits()));
}

TEST(InternalControllerFactoryTest,
     GivenFollowVelocitySplineAndFollowHeadingSplineStrategy_WhenCreate_ThenVelocityAndManeuverControlUnitIsCreated)
{
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();

    auto velocity_spline_control_strategy = std::make_unique<mantle_api::FollowVelocitySplineControlStrategy>();
    config->control_strategies.push_back(std::move(velocity_spline_control_strategy));

    auto heading_spline_control_strategy = std::make_unique<mantle_api::FollowHeadingSplineControlStrategy>();
    config->control_strategies.push_back(std::move(heading_spline_control_strategy));

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(2, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::VelocityControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::ManeuverControlUnit>(controller->GetControlUnits()));
}

// TODO: Create test for default routing behaviour=stop => no path control unit created
TEST(InternalControllerFactoryTest,
     GivenInternalConfigWithKeepVelocityControlStrategy_WhenCreate_ThenKeepVelocityAndPathControlUnitAreCreated)
{
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(2, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::KeepVelocityControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::PathControlUnit>(controller->GetControlUnits()));
}

TEST(
    InternalControllerFactoryTest,
    GivenInternalConfigWithKeepLaneKeepVelocityControlStrategy_WhenCreate_ThenPathAndLaneOffsetAndKeepVelocityControlUnitAreCreated)
{
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepLaneOffsetControlStrategy>());
    config->control_strategies.emplace_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(3, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::PathControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::LaneOffsetControlUnit>(controller->GetControlUnits()));
    EXPECT_TRUE(ContainsController<controller::KeepVelocityControlUnit>(controller->GetControlUnits()));
}

TEST(InternalControllerFactoryTest, GivenConfigWithNotSupportedControlStrategy_WhenCreate_ThenNoControlUnitCreated)
{
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    config->control_strategies.emplace_back(std::make_unique<mantle_api::AcquireLaneOffsetControlStrategy>());

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(0, std::move(config), true);
    ASSERT_EQ(0, controller->GetControlUnitCount());
}

TEST(InternalControllerFactoryTest, GivenKeepLaneOffsetStrategy_WhenCreate_ThenLaneOffsetControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::KeepLaneOffsetControlStrategy>();
    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::LaneOffsetControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest, GivenFollowHeadingSplineStrategy_WhenCreate_ThenManeuverControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::FollowHeadingSplineControlStrategy>();
    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::ManeuverControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest, GivenFollowVelocitySplineStrategy_WhenCreate_ThenVelocityControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::FollowVelocitySplineControlStrategy>();
    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::VelocityControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest,
     GivenFollowLateralOffsetSplineStrategy_WhenCreate_ThenLaneOffsetControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::FollowLateralOffsetSplineControlStrategy>();
    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::LaneOffsetControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest, GivenKeepVelocityStrategy_WhenCreate_ThenKeepVelocityControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::KeepVelocityControlStrategy>();
    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::KeepVelocityControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest, GivenTrafficLightControlStrategy_WhenCreate_ThenTrafficLightControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::TrafficLightStateControlStrategy>();

    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::TrafficLightControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest, GivenNonSupportedStrategy_WhenCreate_ThenNoControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::AcquireLaneOffsetControlStrategy>();
    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_EQ(nullptr, control_unit);
}

TEST(InternalControllerFactoryTest,
     GivenPerformLaneChangeControlStrategyWithLinearShape_WhenCreate_ThenLaneChangeControlUnitAreCreated)
{
    auto control_strategy = std::make_unique<mantle_api::PerformLaneChangeControlStrategy>();
    control_strategy->transition_dynamics.shape = mantle_api::Shape::kLinear;
    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::LaneChangeControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest,
     GivenFollowTrajectoryControlStrategyWithTimeReference_WhenCreate_ThenFollowTrajectoryWithSpeedControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::FollowTrajectoryControlStrategy>();

    control_strategy->trajectory.type = mantle_api::PolyLine{mantle_api::PolyLinePoint()};
    control_strategy->timeReference = mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference();

    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::FollowTrajectoryWithSpeedControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest,
     GivenFollowTrajectoryControlStrategyWithoutTimeReference_WhenCreate_ThenFollowTrajectoryControlUnitIsCreated)
{
    auto control_strategy = std::make_unique<mantle_api::FollowTrajectoryControlStrategy>();
    control_strategy->trajectory.type = mantle_api::PolyLine{mantle_api::PolyLinePoint()};

    auto control_unit = InternalControllerFactory::CreateControlUnit(control_strategy.get(), nullptr);

    EXPECT_TRUE(dynamic_cast<controller::FollowTrajectoryControlUnit*>(control_unit.get()));
}

TEST(InternalControllerFactoryTest, GivenRouteDefinition_WhenCallCreateControlUnit_ThenPathControlUnitIsCreated)
{
    mantle_api::RouteDefinition route_definition;
    route_definition.waypoints.push_back({{0_m, 0_m, 0_m}, {}});
    route_definition.waypoints.push_back({{10_m, 0_m, 0_m}, {}});
    auto control_unit = InternalControllerFactory::CreateControlUnit(route_definition, nullptr);

    EXPECT_TRUE(dynamic_cast<controller::PathControlUnit*>(control_unit.get()));
}

}  // namespace astas::environment::controller
