/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_TRANSITIONDYNAMICSFACTORY_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_TRANSITIONDYNAMICSFACTORY_H

#include "Core/Environment/Controller/Internal/TransitionDynamics/i_transition_dynamics.h"

namespace astas::environment::controller
{

class TransitionDynamicsFactory
{
  public:
    static std::unique_ptr<ITransitionDynamics> Create(
        const mantle_api::TransitionDynamics& transition_dynamics_config);

    static std::unique_ptr<ITransitionDynamics>
    Create(double start_val, double target_val, const mantle_api::TransitionDynamics& transition_dynamics_config);
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_TRANSITIONDYNAMICSFACTORY_H
