/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/transition_dynamics_factory.h"

#include "Core/Environment/Controller/Internal/TransitionDynamics/cubic_transition_dynamics.h"
#include "Core/Environment/Controller/Internal/TransitionDynamics/linear_transition_dynamics.h"
#include "Core/Environment/Exception/exception.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{

class TransitionDynamicsFactoryTest : public testing::Test
{
  protected:
    double start_value_{1.2};
    double target_value_{3.4};
    mantle_api::TransitionDynamics transition_dynamics_{};
};

TEST_F(TransitionDynamicsFactoryTest, GivenTransitionDynamicsWithCubicShape_WhenCreate_ThenCubicTransitionDynamics)
{
    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    const auto transition_dynamics_strategy =
        TransitionDynamicsFactory::Create(start_value_, target_value_, transition_dynamics_);
    EXPECT_TRUE(dynamic_cast<CubicTransitionDynamics*>(transition_dynamics_strategy.get()));
}

TEST_F(TransitionDynamicsFactoryTest, GivenTransitionDynamicsWithLinearShape_WhenCreate_ThenLinearTransitionDynamics)
{
    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    const auto transition_dynamics_strategy =
        TransitionDynamicsFactory::Create(start_value_, target_value_, transition_dynamics_);

    EXPECT_TRUE(dynamic_cast<LinearTransitionDynamics*>(transition_dynamics_strategy.get()));
}

TEST_F(TransitionDynamicsFactoryTest, GivenTransitionDynamicsWithSinusoidalShape_WhenCreate_ThenRuntimeErrorDetected)
{
    transition_dynamics_.shape = mantle_api::Shape::kSinusoidal;
    EXPECT_THROW(TransitionDynamicsFactory::Create(start_value_, target_value_, transition_dynamics_),
                 EnvironmentException);
}

TEST_F(TransitionDynamicsFactoryTest, GivenTransitionDynamicsWithStepShape_WhenCreate_ThenRuntimeErrorDetected)
{
    transition_dynamics_.shape = mantle_api::Shape::kStep;
    EXPECT_THROW(TransitionDynamicsFactory::Create(start_value_, target_value_, transition_dynamics_),
                 EnvironmentException);
}

TEST_F(TransitionDynamicsFactoryTest, GivenTransitionDynamicsWithUndefinedShape_WhenCreate_ThenRuntimeErrorDetected)
{
    transition_dynamics_.shape = mantle_api::Shape::kUndefined;
    EXPECT_THROW(TransitionDynamicsFactory::Create(start_value_, target_value_, transition_dynamics_),
                 EnvironmentException);
}

}  // namespace astas::environment::controller
