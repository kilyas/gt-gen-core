/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_ITRANSITIONDYNAMICS_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_ITRANSITIONDYNAMICS_H

#include <MantleAPI/Traffic/control_strategy.h>

namespace astas::environment::controller
{

class ITransitionDynamics
{
  public:
    ITransitionDynamics() = default;
    ITransitionDynamics(double start_val,
                        double target_val,
                        const mantle_api::TransitionDynamics& transition_dynamics_config);
    ITransitionDynamics(const ITransitionDynamics&) = default;
    ITransitionDynamics& operator=(ITransitionDynamics const&) = default;
    ITransitionDynamics(ITransitionDynamics&&) = default;
    ITransitionDynamics& operator=(ITransitionDynamics&&) = default;
    virtual ~ITransitionDynamics() = default;

    virtual void SetStartVal(double start_val) = 0;
    virtual void SetTargetVal(double target_val) = 0;
    virtual void SetParamVal(double param_val) = 0;

    virtual double Evaluate() const = 0;
    virtual double EvaluateFirstDerivative() const = 0;
    virtual void SetMaxRate(double max_rate) = 0;
    virtual bool Step(double delta_param_val) = 0;
    virtual bool IsTargetReached() const = 0;
    virtual mantle_api::Dimension GetDimension() const = 0;

    virtual double CalculateTargetParameterValByRate(double rate) const = 0;
    virtual double CalculatePeakRate() const = 0;
};

}  // namespace astas::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_ITRANSITIONDYNAMICS_H
