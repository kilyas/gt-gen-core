/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/linear_transition_dynamics.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{

class LinearTransitionDynamicsTest : public testing::Test
{
  protected:
    double expected_start_val_{1.2};
    double expected_target_val_{3.4};
    double transition_dynamics_value_{1.1};  // applied in the TransitionDynamics value filed

    mantle_api::TransitionDynamics transition_dynamics_{};
};

TEST_F(LinearTransitionDynamicsTest, GivenLinearTransitionDynamics_WhenWrongShape_ThenRuntimeError)
{
    transition_dynamics_.shape = mantle_api::Shape::kUndefined;
    EXPECT_THROW(LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_),
                 std::runtime_error);
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionNotRate_WhenCalculatePeakRate_ThenReturnCorrectValue)
{
    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.value = transition_dynamics_value_;

    const auto expected_rate = (expected_target_val_ - expected_start_val_) / transition_dynamics_value_;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_rate, transition.CalculatePeakRate());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionRate_WhenCalculateTargetParameterValByRate_ThenReturnCorrectValue)
{
    const auto& rate = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    const auto expected_parameter_target_value = (expected_target_val_ - expected_start_val_) / rate;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_parameter_target_value,
                     transition.CalculateTargetParameterValByRate(transition_dynamics_value_));
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateBeforeStep_ThenReturnStartValue)
{
    // This test case is identical when the dimension is distance

    const auto& time_to_target = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = time_to_target;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_start_val_, transition.Evaluate());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateReachedTarget_ThenReturnTargetValue)
{
    // This test case is identical when the dimension is distance

    const auto& time_to_target = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = time_to_target;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(time_to_target);

    EXPECT_DOUBLE_EQ(expected_target_val_, transition.Evaluate());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateInTheMiddleOfTransition_ThenReturnCorrectValue)
{
    // This test case is identical when the dimension is distance
    const auto& time_to_target = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = time_to_target;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(time_to_target / 3.0);

    const auto expected_value = expected_start_val_ + (expected_target_val_ - expected_start_val_) / 3.0;
    EXPECT_DOUBLE_EQ(expected_value, transition.Evaluate());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionRate_WhenCallEvaluateBeforeStep_ThenReturnStartValue)
{
    const auto& rate = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_start_val_, transition.Evaluate());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionRate_WhenCallEvaluateReachedTarget_ThenReturnTargetValue)
{
    const auto& rate = transition_dynamics_value_;
    const double parameter_target_value = (expected_target_val_ - expected_start_val_) / rate;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(parameter_target_value);

    EXPECT_DOUBLE_EQ(expected_target_val_, transition.Evaluate());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionRate_WhenCallEvaluateInTheMiddleOfTransition_ThenReturnCorrectValue)
{
    const auto& rate = transition_dynamics_value_;
    const double parameter_target_value = (expected_target_val_ - expected_start_val_) / rate;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();
    transition.Step(parameter_target_value / 3.0);

    const auto expected_value = expected_start_val_ + (expected_target_val_ - expected_start_val_) / 3.0;
    EXPECT_DOUBLE_EQ(expected_value, transition.Evaluate());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionRate_WhenEvaluateFirstDerivative_ThenReturnCorrectValue)
{
    const auto& rate = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    const auto expected_value = rate;
    EXPECT_DOUBLE_EQ(expected_value, transition.EvaluateFirstDerivative());
}

TEST_F(LinearTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionTime_WhenEvaluateFirstDerivative_ThenReturnCorrectValue)
{
    // This test case is identical when the dimension is distance
    const auto& time_to_target = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kLinear;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = time_to_target;

    LinearTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    const auto expected_value = (expected_target_val_ - expected_start_val_) / time_to_target;

    EXPECT_DOUBLE_EQ(expected_value, transition.EvaluateFirstDerivative());
}

}  // namespace astas::environment::controller
