/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/i_abstract_transition_dynamics.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{
using testing::Return;

class MockTransitionDynamics : public IAbstractTransitionDynamics
{
  public:
    using IAbstractTransitionDynamics::IAbstractTransitionDynamics;

    MOCK_METHOD(double, Evaluate, (), (const, override));
    MOCK_METHOD(double, EvaluateFirstDerivative, (), (const, override));
    MOCK_METHOD(double, CalculatePeakRate, (), (const, override));
    MOCK_METHOD(double, CalculateTargetParameterValByRate, (const double rate), (const, override));
};

class TransitionDynamicsStrategyTest : public testing::Test
{
  protected:
    double expected_start_val_{1.2};
    double expected_target_val_{3.4};
    double transition_dynamics_value_{1.1};  // applied in the TransitionDynamics value filed

    mantle_api::TransitionDynamics transition_dynamics_config_{};
};

TEST_F(TransitionDynamicsStrategyTest, GivenTransitionDynamics_WhenInitialize_ThenStartAndTargetValuesAreCorrect)
{
    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_start_val_, transition.GetStartVal());
    EXPECT_DOUBLE_EQ(expected_target_val_, transition.GetTargetVal());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamics_WhenInitializeWithNotTimeDimension_ThenTargetParameterValueIsCorrect)
{
    transition_dynamics_config_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_config_.value = transition_dynamics_value_;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();

    EXPECT_DOUBLE_EQ(transition_dynamics_value_, transition.GetParamTargetVal());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamics_WhenInitializeWithNotRateDimension_ThenTargetParameterValueIsCorrect)
{
    transition_dynamics_config_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_config_.value = transition_dynamics_value_;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    const double expected_param_target_val = 123.0;
    EXPECT_CALL(transition, CalculateTargetParameterValByRate(transition_dynamics_value_))
        .Times(1)
        .WillOnce(Return(expected_param_target_val));
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_param_target_val, transition.GetParamTargetVal());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamicsWithDimensionRate_WhenPeakRateIsHigherThanMaxRate_ThenScaleFactorIsCalculatedCorrectly)
{
    const auto& rate = transition_dynamics_value_;
    const double max_rate = 1.0;
    const double expected_scale_factor = max_rate / rate;

    transition_dynamics_config_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_config_.value = rate;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();
    transition.SetMaxRate(max_rate);

    EXPECT_DOUBLE_EQ(expected_scale_factor, transition.GetScaleFactor());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamicsWithDimensionRate_WhenPeakRateIsLowerThanMaxRate_ThenScaleFactorIsCalculatedCorrectly)
{
    const auto& rate = transition_dynamics_value_;
    const double max_rate = 1.2;
    const double expected_scale_factor = 1.0;

    transition_dynamics_config_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_config_.value = rate;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();
    transition.SetMaxRate(max_rate);

    EXPECT_DOUBLE_EQ(expected_scale_factor, transition.GetScaleFactor());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamicsWithDimensionNotRate_WhenPeakRateIsHigherThanMaxRate_ThenScaleFactorIsCalculatedCorrectly)
{
    const double max_rate = 10.0;
    const double peak_rate = 20.0;
    const double expected_scale_factor = max_rate / peak_rate;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    EXPECT_CALL(transition, CalculatePeakRate()).Times(1).WillOnce(Return(peak_rate));
    transition.Init();
    transition.SetMaxRate(max_rate);

    EXPECT_DOUBLE_EQ(expected_scale_factor, transition.GetScaleFactor());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamicsWithDimensionNotRate_WhenPeakRateIsLowerThanMaxRate_ThenScaleFactorIsCalculatedCorrectly)
{
    const double max_rate = 10.0;
    const double peak_rate = 5.0;
    const double expected_scale_factor = 1.0;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    EXPECT_CALL(transition, CalculatePeakRate()).Times(1).WillOnce(Return(peak_rate));
    transition.Init();
    transition.SetMaxRate(max_rate);

    EXPECT_DOUBLE_EQ(expected_scale_factor, transition.GetScaleFactor());
}

TEST_F(TransitionDynamicsStrategyTest, GivenTransitionDynamics_WhenStepMultipleTimes_ThenParamValueIncreasesCorrectly)
{
    transition_dynamics_config_.value = transition_dynamics_value_;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();
    transition.Step(0.1);
    transition.Step(0.2);

    EXPECT_DOUBLE_EQ(0.3, transition.GetParamVal());
}

TEST_F(TransitionDynamicsStrategyTest, GivenTransitionDynamics_WhenStepReachedTarget_ThenParamValueDoesNotIncrease)
{
    transition_dynamics_config_.value = transition_dynamics_value_;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();
    EXPECT_TRUE(transition.Step(1.2));  // After this step, target reached
    EXPECT_FALSE(transition.Step(0.2));

    EXPECT_DOUBLE_EQ(1.2, transition.GetParamVal());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamics_WhenStepNotExceedFinshedAndCallIsTargetReached_ThenHasNotReachedTarget)
{
    transition_dynamics_config_.value = transition_dynamics_value_;

    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();

    EXPECT_TRUE(transition.Step(1.0));
    EXPECT_FALSE(transition.IsTargetReached());
}

TEST_F(TransitionDynamicsStrategyTest,
       GivenTransitionDynamics_WhenStepExceedFinshedAndCallIsTargetReached_ThenHasReachedTarget)
{
    transition_dynamics_config_.value = transition_dynamics_value_;
    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);
    transition.Init();
    transition.Step(1.2);

    EXPECT_TRUE(transition.IsTargetReached());
    EXPECT_FALSE(transition.Step(0.1));
}

TEST_F(TransitionDynamicsStrategyTest, GivenTransitionDynamics_WhenInitialize_ThenParamTargetValIsSet)
{
    transition_dynamics_config_.value = transition_dynamics_value_;
    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);

    EXPECT_DOUBLE_EQ(transition_dynamics_value_, transition.GetParamTargetVal());
}

TEST_F(TransitionDynamicsStrategyTest, GivenTransitionDynamics_WhenInitialize_ThenInitialStateTargetIsNotReached)
{
    transition_dynamics_config_.value = transition_dynamics_value_;
    MockTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_config_);

    EXPECT_FALSE(transition.IsTargetReached());
}

}  // namespace astas::environment::controller
