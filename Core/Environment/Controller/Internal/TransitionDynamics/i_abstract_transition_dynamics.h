/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_IABSTRACTTRANSITIONDYNAMICS_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_IABSTRACTTRANSITIONDYNAMICS_H

#include "Core/Environment/Controller/Internal/TransitionDynamics/i_transition_dynamics.h"

namespace astas::environment::controller
{

class IAbstractTransitionDynamics : public ITransitionDynamics
{
  public:
    IAbstractTransitionDynamics() = default;
    IAbstractTransitionDynamics(double start_val,
                                double target_val,
                                const mantle_api::TransitionDynamics& transition_dynamics_config);
    IAbstractTransitionDynamics(const IAbstractTransitionDynamics&) = default;
    IAbstractTransitionDynamics& operator=(IAbstractTransitionDynamics const&) = default;
    IAbstractTransitionDynamics(IAbstractTransitionDynamics&&) = default;
    IAbstractTransitionDynamics& operator=(IAbstractTransitionDynamics&&) = default;
    ~IAbstractTransitionDynamics() override = default;

    void SetMaxRate(double max_rate) override;
    void Init();
    bool Step(double delta_param_val) override;
    bool IsTargetReached() const override;

    void SetStartVal(double start_val) final { start_val_ = start_val; }
    void SetTargetVal(double target_val) final { target_val_ = target_val; }
    void SetParamVal(double param_val) final { param_val_ = param_val; }
    mantle_api::Dimension GetDimension() const final { return dimension_; };

    double GetStartVal() const { return start_val_; }
    double GetTargetVal() const { return target_val_; }
    double GetParamTargetVal() const { return param_target_val_; }
    double GetParamVal() const { return param_val_; }
    double GetScaleFactor() const { return scale_factor_; }
    double GetRate() const { return rate_; }

  private:
    double start_val_{0.0};
    double target_val_{0.0};
    double param_target_val_{0.0};
    double param_val_{0.0};
    double rate_{0.0};
    double scale_factor_{1.0};

    mantle_api::Dimension dimension_{mantle_api::Dimension::kUndefined};

    bool initialized_{false};
};

}  // namespace astas::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_TRANSITIONDYNAMICS_IABSTRACTTRANSITIONDYNAMICS_H
