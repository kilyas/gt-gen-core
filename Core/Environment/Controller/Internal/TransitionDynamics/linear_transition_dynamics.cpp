/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/linear_transition_dynamics.h"

#include "Core/Service/Utility/math_utils.h"

namespace astas::environment::controller
{

LinearTransitionDynamics::LinearTransitionDynamics(double start_val,
                                                   double target_val,
                                                   const mantle_api::TransitionDynamics& transition_dynamics_config)
    : IAbstractTransitionDynamics(start_val, target_val, transition_dynamics_config)
{
    if (transition_dynamics_config.shape != mantle_api::Shape::kLinear)
    {
        throw std::runtime_error("Wrong TransitionDynamicsStrategy (i.e. LinearTransitionDynamics) used.");
    }
}

double LinearTransitionDynamics::Evaluate() const
{
    return GetStartVal() +
           GetParamVal() * (GetTargetVal() - GetStartVal()) / service::utility::AvoidZero(GetParamTargetVal());
}

double LinearTransitionDynamics::EvaluateFirstDerivative() const
{
    return (GetTargetVal() - GetStartVal()) / service::utility::AvoidZero(GetParamTargetVal());
}

double LinearTransitionDynamics::CalculateTargetParameterValByRate(double rate) const
{
    return (GetTargetVal() - GetStartVal()) / service::utility::AvoidZero(rate);
}

double LinearTransitionDynamics::CalculatePeakRate() const
{
    return (GetTargetVal() - GetStartVal()) / service::utility::AvoidZero(GetParamTargetVal());
}

}  // namespace astas::environment::controller
