/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/cubic_transition_dynamics.h"

#include "Core/Service/Utility/math_utils.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{

class CubicTransitionDynamicsTest : public testing::Test
{
  protected:
    double expected_start_val_{1.2};
    double expected_target_val_{3.4};
    double transition_dynamics_value_{1.1};  // applied in the TransitionDynamics value filed

    mantle_api::TransitionDynamics transition_dynamics_{};
};

TEST_F(CubicTransitionDynamicsTest, GivenCubicTransitionDynamics_WhenWrongShape_ThenRuntimeError)
{
    transition_dynamics_.shape = mantle_api::Shape::kUndefined;
    EXPECT_THROW(CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_),
                 std::runtime_error);
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionTime_WhenCalculatePeakRate_ThenReturnCorrectValue)
{
    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.value = transition_dynamics_value_;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;

    const auto expected_rate = 1.5 * (expected_target_val_ - expected_start_val_) / transition_dynamics_value_;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_rate, transition.CalculatePeakRate());
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionRate_WhenCalculateTargetParameterValByRate_ThenReturnCorrectValue)
{
    const auto& rate = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    const auto expected_parameter_target_value = 1.5 * (expected_target_val_ - expected_start_val_) / rate;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_parameter_target_value,
                     transition.CalculateTargetParameterValByRate(transition_dynamics_value_));
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionTime_WhenCallEvaluateBeforeStep_ThenReturnStartValue)
{
    const auto& time_to_target = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = time_to_target;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_start_val_, transition.Evaluate());
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateReachedTarget_ThenReturnTargetValue)
{
    const auto& time_to_target = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = time_to_target;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);

    transition.Init();
    transition.Step(time_to_target);

    EXPECT_DOUBLE_EQ(expected_target_val_, transition.Evaluate());
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionNotRate_WhenCallEvaluateInTheMiddleOfTransition_ThenReturnCorrectValue)
{
    const auto& time_to_target = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = time_to_target;
    const double expected_param_target_val = time_to_target;
    const double expected_param_val = time_to_target / 2.0;

    const double expected_value = expected_start_val_ + (expected_target_val_ - expected_start_val_) *
                                                            pow(expected_param_val / expected_param_target_val, 2) *
                                                            (3 - 2 * expected_param_val / expected_param_target_val);

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);

    transition.Init();
    transition.Step(expected_param_val);

    EXPECT_NEAR(expected_value, transition.Evaluate(), 0.01);
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionRate_WhenCallEvaluateBeforeStep_ThenReturnStartValue)
{
    const auto& rate = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);
    transition.Init();

    EXPECT_DOUBLE_EQ(expected_start_val_, transition.Evaluate());
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionRate_WhenCallEvaluateReachedTarget_ThenReturnTargetValue)
{
    const auto& rate = transition_dynamics_value_;
    const double parameter_target_value = 1.5 * (expected_target_val_ - expected_start_val_) / rate;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);

    transition.Init();
    transition.Step(parameter_target_value);

    EXPECT_DOUBLE_EQ(expected_target_val_, transition.Evaluate());
}

TEST_F(CubicTransitionDynamicsTest,
       GivenCubicTransitionDynamicsWithDimensionRate_WhenCallEvaluateInTheMiddleOfTransition_ThenReturnCorrectValue)
{
    const auto& rate = transition_dynamics_value_;
    const double parameter_target_value = 1.5 * (expected_target_val_ - expected_start_val_) / rate;
    const double expected_param_val = parameter_target_value / 2.0;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);

    transition.Init();
    transition.Step(expected_param_val);

    double expected_value = expected_start_val_ + (expected_target_val_ - expected_start_val_) *
                                                      pow(expected_param_val / parameter_target_value, 2) *
                                                      (3 - 2 * expected_param_val / parameter_target_value);

    EXPECT_NEAR(expected_value, transition.Evaluate(), 0.01);
}

TEST_F(CubicTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionRate_WhenEvaluateFirstDerivative_ThenReturnCorrectValue)
{
    const auto& rate = transition_dynamics_value_;

    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kRate;
    transition_dynamics_.value = rate;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);

    transition.Init();
    transition.SetParamVal(transition.GetParamTargetVal() / 2 - 0.1);
    // Slope (rate) peaks midway
    auto maximum_minus_delta = transition.EvaluateFirstDerivative();
    transition.SetParamVal(transition.GetParamTargetVal() / 2);
    auto maximum = transition.EvaluateFirstDerivative();
    transition.SetParamVal(transition.GetParamTargetVal() / 2 + 0.1);
    auto maximum_plus_delta = transition.EvaluateFirstDerivative();

    EXPECT_GT(maximum, maximum_minus_delta);
    EXPECT_GT(maximum, maximum_plus_delta);
}

TEST_F(CubicTransitionDynamicsTest,
       GivenLinearTransitionDynamicsWithDimensionTime_WhenEvaluateFirstDerivative_ThenReturnCorrectValue)
{
    transition_dynamics_.shape = mantle_api::Shape::kCubic;
    transition_dynamics_.dimension = mantle_api::Dimension::kTime;
    transition_dynamics_.value = transition_dynamics_value_;

    CubicTransitionDynamics transition(expected_start_val_, expected_target_val_, transition_dynamics_);

    transition.Init();
    transition.SetParamVal(transition.GetParamTargetVal() / 2 - 0.1);
    // Slope (rate) peaks midway
    auto maximum_minus_delta = transition.EvaluateFirstDerivative();
    transition.SetParamVal(transition.GetParamTargetVal() / 2);
    auto maximum = transition.EvaluateFirstDerivative();
    transition.SetParamVal(transition.GetParamTargetVal() / 2 + 0.1);
    auto maximum_plus_delta = transition.EvaluateFirstDerivative();

    EXPECT_GT(maximum, maximum_minus_delta);
    EXPECT_GT(maximum, maximum_plus_delta);
}

}  // namespace astas::environment::controller
