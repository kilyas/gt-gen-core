/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/TransitionDynamics/cubic_transition_dynamics.h"

#include "Core/Service/Utility/math_utils.h"

namespace astas::environment::controller
{

// For the calculations we assume: S = start_val, T = target_val,  V = param_target_val, x = param_val;
// K = T-S

CubicTransitionDynamics::CubicTransitionDynamics(double start_val,
                                                 double target_val,
                                                 const mantle_api::TransitionDynamics& transition_dynamics_config)
    : IAbstractTransitionDynamics(start_val, target_val, transition_dynamics_config)
{
    if (transition_dynamics_config.shape != mantle_api::Shape::kCubic)
    {
        throw std::runtime_error("Wrong TransitionDynamicsStrategy (i.e. CubicTransitionDynamics) used.");
    }
}

double CubicTransitionDynamics::EvaluateFirstDerivative() const
{
    const double param_target_val = service::utility::AvoidZero(GetParamTargetVal());
    const double target_distance = GetTargetVal() - GetStartVal();
    const double param_val = GetParamVal();

    // f(x) = K*(x/V)^2*(3-2*x/V) + S
    // f(x) = 3*K*(x/V)^2 - 2*K*(x/V)^3 + S
    // f'(x) = 6*K*x/V^2 - 6*K*x^2/V^3
    // f'(x) = 6*K*x*V/V^3 - 6*K*x^2/V^3
    // f'(x) = 6*K*x*(V - x)/V^3

    return 6 * target_distance * (param_target_val - param_val) * param_val / pow(param_target_val, 3);
}

double CubicTransitionDynamics::Evaluate() const
{
    const double param_target_val = service::utility::AvoidZero(GetParamTargetVal());
    const double target_distance = GetTargetVal() - GetStartVal();
    const double param_val = GetParamVal();

    // f(x) = A*x^3 + B*x^2 + C*x + D  ----[E0]
    // given condition start value (S) and end value (T) :
    // f(x = 0) = S => D = S  ---- [E1]
    // f(x = V) = T ----[E2]
    //
    // given constrains that the gradient must be zero at start and end
    // f'(x) = 3*A*x^3 + B*x^2 + C*x + D
    // f'(x =0) = 0  => C = 0    ---- [E3]
    // f'(x =V) = 0 => 3*A*V^2 + 2*B*V = 0 ----[E4]
    //
    // Resolving E0 using E1-E4, cubic equation is then depended on values T,S,V :
    // f(x) = K(x/V)^2*(3-2 x/V) + S
    return target_distance * pow(param_val / param_target_val, 2) * (3 - 2 * param_val / param_target_val) +
           GetStartVal();
}

double CubicTransitionDynamics::CalculateTargetParameterValByRate(double rate) const
{
    const double target_difference = GetTargetVal() - GetStartVal();
    rate = service::utility::AvoidZero(rate);

    // f(x) = K(x/V)x^2*(3-X/V) + S
    // f'(V/2) = 3K/2V where f'(V/2) = rate
    // rate = 3*K/2*V
    // V = 3*K/2*rate
    return 1.5 * (target_difference) / rate;
}

double CubicTransitionDynamics::CalculatePeakRate() const
{
    const double param_target_val = service::utility::AvoidZero(GetParamTargetVal());
    const double target_difference = GetTargetVal() - GetStartVal();

    // f(x) = K*(x/V)*x^2*(3-X/V) + S
    // f'(V/2) = 3*K/2*V
    return 1.5 * (target_difference) / service::utility::AvoidZero(param_target_val);
}

}  // namespace astas::environment::controller
