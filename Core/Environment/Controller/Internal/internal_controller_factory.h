/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_INTERNALCONTROLLERFACTORY_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_INTERNALCONTROLLERFACTORY_H

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_with_speed_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/keep_velocity_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_change_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_offset_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/maneuver_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_light_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"
#include "Core/Environment/Controller/Internal/TransitionDynamics/transition_dynamics_factory.h"
#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Traffic/control_strategy.h>

namespace astas::environment::controller
{
class InternalControllerFactory
{
  public:
    static void Create(CompositeController* composite_controller, mantle_api::InternalControllerConfig* internal_config)
    {
        for (const auto& control_strategy : internal_config->control_strategies)
        {
            auto control_unit = CreateControlUnit(control_strategy.get(), internal_config->map_query_service);
            if (control_unit != nullptr)
            {
                composite_controller->AddControlUnit(std::move(control_unit));
            }
        }

        if (!internal_config->route_definition.waypoints.empty())
        {
            auto control_unit =
                CreateControlUnit(internal_config->route_definition, internal_config->map_query_service);
            composite_controller->AddControlUnit(std::move(control_unit));
        }
    }

    static std::unique_ptr<controller::IControlUnit> CreateControlUnit(
        mantle_api::ControlStrategy* control_strategy,
        mantle_api::ILaneLocationQueryService* map_query_service)
    {
        if (dynamic_cast<mantle_api::KeepLaneOffsetControlStrategy*>(control_strategy) != nullptr)
        {
            return std::make_unique<controller::LaneOffsetControlUnit>(map_query_service);
        }
        else if (auto* follow_heading_spline_control_strategy =
                     dynamic_cast<mantle_api::FollowHeadingSplineControlStrategy*>(control_strategy))
        {
            return std::make_unique<controller::ManeuverControlUnit>(
                follow_heading_spline_control_strategy->heading_splines, map_query_service);
        }
        else if (auto* follow_velocity_spline_control_strategy =
                     dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategy))
        {
            return std::make_unique<controller::VelocityControlUnit>(
                follow_velocity_spline_control_strategy->velocity_splines,
                follow_velocity_spline_control_strategy->default_value);
        }
        else if (auto* follow_trajectory_control_strategy =
                     dynamic_cast<mantle_api::FollowTrajectoryControlStrategy*>(control_strategy))
        {
            if (follow_trajectory_control_strategy->timeReference.has_value())
            {
                return std::make_unique<controller::FollowTrajectoryWithSpeedControlUnit>(
                    map_query_service,
                    follow_trajectory_control_strategy->trajectory,
                    follow_trajectory_control_strategy->timeReference.value());
            }
            else
            {
                return std::make_unique<controller::FollowTrajectoryControlUnit>(
                    map_query_service, follow_trajectory_control_strategy->trajectory);
            }
        }
        else if (auto follow_lateral_offset_control_strategy =
                     dynamic_cast<mantle_api::FollowLateralOffsetSplineControlStrategy*>(control_strategy))
        {
            return std::make_unique<controller::LaneOffsetControlUnit>(
                follow_lateral_offset_control_strategy->lateral_offset_splines);
        }
        else if (dynamic_cast<mantle_api::KeepVelocityControlStrategy*>(control_strategy) != nullptr)
        {
            return std::make_unique<controller::KeepVelocityControlUnit>();
        }
        else if (auto* traffic_light_control_strategy =
                     dynamic_cast<mantle_api::TrafficLightStateControlStrategy*>(control_strategy))
        {
            return std::make_unique<controller::TrafficLightControlUnit>(
                std::move(traffic_light_control_strategy->traffic_light_phases),
                traffic_light_control_strategy->repeat_states);
        }
        else if (auto lane_change_strategy =
                     dynamic_cast<mantle_api::PerformLaneChangeControlStrategy*>(control_strategy))
        {
            return std::make_unique<controller::LaneChangeControlUnit>(
                *lane_change_strategy,
                map_query_service,
                TransitionDynamicsFactory::Create(lane_change_strategy->transition_dynamics));
        }
        Warn(
            "Tried to create a control unit from an unsupported control strategy. Please contact GTGen "
            "Support for further assistance.");
        return nullptr;
    }

    static std::unique_ptr<controller::IControlUnit> CreateControlUnit(
        const mantle_api::RouteDefinition& route_definition,
        mantle_api::ILaneLocationQueryService* map_query_service)
    {

        std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints;
        waypoints.reserve(route_definition.waypoints.size());
        for (auto route_waypoint : route_definition.waypoints)
        {
            waypoints.push_back(route_waypoint.waypoint);
        }
        return std::make_unique<controller::PathControlUnit>(map_query_service, waypoints);
    }
};
}  // namespace astas::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_INTERNALCONTROLLERFACTORY_H
