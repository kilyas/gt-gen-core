/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/controller_helpers.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

using units::literals::operator""_m;
using units::literals::operator""_rad;

namespace astas::environment::controller
{
class ControllerHelperTest : public testing::Test
{
  protected:
    std::unique_ptr<environment::map::AstasMap> map_{
        test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider_{*map_};
    ControllerHelpers controller_helpers_{&lane_location_provider_};
};

TEST_F(ControllerHelperTest, GivenNoWaypoints_WhenComputePointDistanceListFromPath_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {};

    EXPECT_THROW(controller_helpers_.ComputePointDistanceListFromPath(waypoints), EnvironmentException);
}

TEST_F(ControllerHelperTest, GivenFirstWaypointNotOnALane_WhenComputePointDistanceListFromPath_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{42_m, 42_m, 0_m},  // not on lane
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{20_m, 0_m, 0_m}};

    EXPECT_THROW(controller_helpers_.ComputePointDistanceListFromPath(waypoints), EnvironmentException);
}

TEST_F(ControllerHelperTest, GivenWaypointNotOnALane_WhenComputePointDistanceListFromPath_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{42_m, 42_m, 0_m},  // not on lane
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m}};

    EXPECT_THROW(controller_helpers_.ComputePointDistanceListFromPath(waypoints), EnvironmentException);
}

TEST_F(ControllerHelperTest, GivenLastWaypointNotOnALane_WhenComputePointDistanceListFromPath_ThenExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{42_m, 42_m, 0_m}};  // not on lane

    EXPECT_THROW(controller_helpers_.ComputePointDistanceListFromPath(waypoints), EnvironmentException);
}

TEST_F(ControllerHelperTest, GivenOneWaypoint_WhenComputePointDistanceListFromPath_ThenNoExceptionThrown)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}};

    EXPECT_NO_THROW(controller_helpers_.ComputePointDistanceListFromPath(waypoints));
}

TEST_F(ControllerHelperTest, GivenValidWaypoints_WhenComputePointDistanceListFromPath_ThenStartAndEndPointIsCorrect)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{10_m, 0_m, 0_m}};

    const auto point_list = controller_helpers_.ComputePointDistanceListFromPath(waypoints);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), point_list.front().point);
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(10_m, 0_m, 0_m), point_list.back().point);
}

TEST_F(ControllerHelperTest,
       GivenFirstWaypointExactlyOnFirstCenterLinePoint_ComputePointDistanceListFrom_ThenFirstPointDistanceIsCorrect)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    lanefollowing::PointDistanceList list;
    list.push_back({mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m}, 0.05_m, 3, {1.0_m, 0.0_m, 0.0_m}});

    const auto point_list = controller_helpers_.ComputePointDistanceListFromPath(waypoints);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), point_list.front().point);
    EXPECT_EQ(0.05_m, point_list.front().distance_to_next_point);
    EXPECT_EQ(3, point_list.front().lane_id);
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(1_m, 0_m, 0_m), point_list.front().heading);
}

TEST_F(ControllerHelperTest,
       GivenFirstWaypointExactlyOnFirstCenterLinePoint_ComputePointDistanceListFrom_ThenLastPointDistanceIsCorrect)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints = {
        mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m},
        mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}};

    lanefollowing::PointDistanceList list{};
    list.push_back({mantle_api::Vec3<units::length::meter_t>{297_m, 0_m, 0_m}, 0.0_m, 11, {1.0_m, 0.0_m, 0.0_m}});

    const auto point_list = controller_helpers_.ComputePointDistanceListFromPath(waypoints);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(297_m, 0_m, 0_m), point_list.back().point);
    EXPECT_EQ(0.0_m, point_list.back().distance_to_next_point);
    EXPECT_EQ(11, point_list.back().lane_id);
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(1_m, 0_m, 0_m), point_list.back().heading);
}

TEST_F(ControllerHelperTest, GivenNoTrajectoryPoint_WhenConvertTrajectoryIntoPointDistanceList_ThenExceptionThrown)
{
    mantle_api::Trajectory trajectory;

    EXPECT_THROW(controller_helpers_.ConvertTrajectoryIntoPointDistanceList(trajectory), EnvironmentException);
}

TEST_F(ControllerHelperTest,
       GivenValidTrajectoryPoints_WhenConvertTrajectoryIntoPointDistanceList_ThenExceptionNotThrown)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    EXPECT_NO_THROW(controller_helpers_.ConvertTrajectoryIntoPointDistanceList(trajectory));
}

TEST_F(ControllerHelperTest,
       GivenValidTrajectory_WhenConvertTrajectoryIntoPointDistanceList_ThenStartAndEndPointAreCorrect)
{
    mantle_api::Trajectory trajectory;
    mantle_api::PolyLine poly_line;
    mantle_api::PolyLinePoint poly_line_point;
    poly_line_point.pose = {{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(1.0);
    poly_line.emplace_back(poly_line_point);
    poly_line_point.pose = {{10.0_m, 0.0_m, 0.0_m}, {units::angle::radian_t(M_PI_2), 0.0_rad, 0.0_rad}};
    poly_line_point.time = units::time::second_t(3.0);
    poly_line.emplace_back(poly_line_point);
    trajectory.type = poly_line;

    const auto point_list = controller_helpers_.ConvertTrajectoryIntoPointDistanceList(trajectory);

    ASSERT_EQ(2, point_list.size());

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), point_list.front().point);
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(1_m, 0_m, 0_m), point_list.front().heading);
    EXPECT_EQ(10.0_m, point_list.front().distance_to_next_point);

    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(10_m, 0_m, 0_m), point_list.back().point);
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 1_m, 0_m), point_list.back().heading);

    EXPECT_EQ(0.0_m, point_list.back().distance_to_next_point);
}

}  // namespace astas::environment::controller
