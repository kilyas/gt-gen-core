/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/path_spline_fitting.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;

// TODO: add more tests

TEST(PathSplineFittingTest, GivenPointDistanceListWithOnePoint_WhenFittingSpline_ThenOnlyOnePointReturned)
{
    lanefollowing::PointDistanceList list;
    list.push_back(lanefollowing::PointDistanceListEntry{{0_m, 0_m, 0_m}, 10.0_m, 0, {0_m, 0_m, 0_m}});
    auto spline = SamplePathAsSpline(list);

    EXPECT_EQ(1, spline.size());
}

TEST(PathSplineFittingTest, GivenPointDistanceList_WhenFittingSpline_ThenSecondLastPointHasDistanceToLastPoint)
{
    lanefollowing::PointDistanceList list;
    list.push_back(lanefollowing::PointDistanceListEntry{{0_m, 0_m, 0_m}, 10.0_m, 0, {0_m, 0_m, 0_m}});
    list.push_back(lanefollowing::PointDistanceListEntry{{10_m, 0_m, 0_m}, 0.0_m, 0, {0_m, 0_m, 0_m}});
    auto spline = SamplePathAsSpline(list);

    EXPECT_GT(spline.at(spline.size() - 2).distance_to_next_point, 0.0_m);
}

}  // namespace astas::environment::controller
