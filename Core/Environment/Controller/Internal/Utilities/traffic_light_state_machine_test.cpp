/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Controller/Internal/Utilities/traffic_light_state_machine.h"

#include "Core/Environment/Controller/Internal/Utilities/traffic_light_phase_state.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{

class TrafficLightStateMachineFixture : public testing::Test
{
  public:
    TrafficLightStateMachineFixture()
    {
        SetupTrafficLightEntity();
        SetupLightPhases();
    }

    entities::TrafficLightEntity traffic_light_entity{1, "TestTrafficLight"};
    std::vector<mantle_api::TrafficLightPhase> traffic_light_phases{};

  private:
    void SetupTrafficLightEntity()
    {
        mantle_ext::TrafficLightProperties traffic_light_properties;

        mantle_ext::TrafficLightBulbProperties red_bulb;
        red_bulb.color = mantle_api::TrafficLightBulbColor::kRed;
        red_bulb.icon = osi::OsiTrafficLightIcon::kNone;
        red_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

        mantle_ext::TrafficLightBulbProperties yellow_bulb;
        yellow_bulb.color = mantle_api::TrafficLightBulbColor::kYellow;
        yellow_bulb.icon = osi::OsiTrafficLightIcon::kNone;
        yellow_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

        mantle_ext::TrafficLightBulbProperties green_bulb;
        green_bulb.color = mantle_api::TrafficLightBulbColor::kGreen;
        green_bulb.icon = osi::OsiTrafficLightIcon::kNone;
        green_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

        traffic_light_properties.bulbs.push_back(red_bulb);
        traffic_light_properties.bulbs.push_back(yellow_bulb);
        traffic_light_properties.bulbs.push_back(green_bulb);

        traffic_light_entity.SetProperties(
            std::make_unique<mantle_ext::TrafficLightProperties>(std::move(traffic_light_properties)));
    }

    void SetupLightPhases()
    {
        mantle_api::TrafficLightPhase phase_1{};
        phase_1.start_time = mantle_api::Time(0);
        phase_1.end_time = mantle_api::Time(10'000);
        phase_1.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kRed, mantle_api::TrafficLightBulbMode::kConstant});
        phase_1.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kYellow, mantle_api::TrafficLightBulbMode::kOff});
        phase_1.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kGreen, mantle_api::TrafficLightBulbMode::kOff});

        mantle_api::TrafficLightPhase phase_2{};
        phase_2.start_time = mantle_api::Time(10'000);
        phase_2.end_time = mantle_api::Time(20'000);
        phase_2.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kRed, mantle_api::TrafficLightBulbMode::kOff});
        phase_2.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kYellow, mantle_api::TrafficLightBulbMode::kOff});
        phase_2.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kGreen, mantle_api::TrafficLightBulbMode::kConstant});

        mantle_api::TrafficLightPhase phase_3{};
        phase_3.start_time = mantle_api::Time(20'000);
        phase_3.end_time = mantle_api::Time(25'000);
        phase_3.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kRed, mantle_api::TrafficLightBulbMode::kOff});
        phase_3.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kYellow, mantle_api::TrafficLightBulbMode::kConstant});
        phase_3.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kGreen, mantle_api::TrafficLightBulbMode::kOff});

        traffic_light_phases.push_back(std::move(phase_1));
        traffic_light_phases.push_back(std::move(phase_2));
        traffic_light_phases.push_back(std::move(phase_3));
    }
};

TEST_F(TrafficLightStateMachineFixture, GivenTrafficLightStateMachine_WhenStartAtTimeZero_ThenFirstStateIsStarted)
{
    TrafficLightStateMachine state_machine{traffic_light_phases, false};

    state_machine.Start(&traffic_light_entity);

    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);
}

TEST_F(TrafficLightStateMachineFixture,
       GivenSimulationTimeWithinFirstState_WhenStateMachineIsStepped_ThenEntityDoesNotUpdated)
{
    TrafficLightStateMachine state_machine{traffic_light_phases, false};

    state_machine.Start(&traffic_light_entity);

    state_machine.Step(mantle_api::Time(5'000));

    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);
}

TEST_F(TrafficLightStateMachineFixture,
       GivenSimulationTimeWithinSecondState_WhenStateMachineIsStepped_ThenEntityIsUpdated)
{
    TrafficLightStateMachine state_machine{traffic_light_phases, false};

    state_machine.Start(&traffic_light_entity);

    state_machine.Step(mantle_api::Time(12'000));

    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);
}

TEST_F(TrafficLightStateMachineFixture,
       GivenSimulationTimeOutsideLastState_WhenStateMachineIsStepped_ThenEntityIsNotUpdated)
{
    TrafficLightStateMachine state_machine{traffic_light_phases, false};

    state_machine.Start(&traffic_light_entity);

    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);

    state_machine.Step(mantle_api::Time(12'000));

    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);

    state_machine.Step(mantle_api::Time(22'000));

    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);

    state_machine.Step(mantle_api::Time(30'000));

    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);
}

TEST_F(TrafficLightStateMachineFixture,
       GivenSimulationTimeOutsideLastStateAndRepeatSequence_WhenStateMachineIsStepped_ThenEntityIsUpdated)
{
    TrafficLightStateMachine state_machine{traffic_light_phases, true};

    state_machine.Start(&traffic_light_entity);

    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);

    state_machine.Step(mantle_api::Time(12'000));

    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(1).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);

    state_machine.Step(mantle_api::Time(22'000));

    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(2).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);

    state_machine.Step(mantle_api::Time(30'000));

    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(0).mode,
              traffic_light_entity.GetProperties()->bulbs.at(0).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(1).mode,
              traffic_light_entity.GetProperties()->bulbs.at(1).mode);
    EXPECT_EQ(traffic_light_phases.at(0).bulb_states.at(2).mode,
              traffic_light_entity.GetProperties()->bulbs.at(2).mode);
}

}  // namespace astas::environment::controller
