/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/traffic_light_phase_state.h"

namespace astas::environment::controller
{
TrafficLightPhaseState::TrafficLightPhaseState(const mantle_api::TrafficLightPhase& traffic_light_phase,
                                               environment::entities::TrafficLightEntity* traffic_light_entity)
    : traffic_light_phase_(traffic_light_phase), traffic_light_entity_(traffic_light_entity)
{
}

void TrafficLightPhaseState::Start()
{
    auto& entity_bulbs = traffic_light_entity_->GetProperties()->bulbs;

    for (const auto phase_bulb : traffic_light_phase_.bulb_states)
    {
        auto matching_entity_bulb =
            std::find_if(entity_bulbs.begin(),
                         entity_bulbs.end(),
                         [&phase_bulb](const mantle_ext::TrafficLightBulbProperties& bulb_properties) {
                             return bulb_properties.color == phase_bulb.color;
                         });
        if (matching_entity_bulb != entity_bulbs.end())
        {
            matching_entity_bulb->mode = phase_bulb.mode;
        }
    }
}

bool TrafficLightPhaseState::HasPhaseFinished(mantle_api::Time current_simulation_time)
{
    return !(current_simulation_time >= traffic_light_phase_.start_time &&
             current_simulation_time < traffic_light_phase_.end_time);
}

}  // namespace astas::environment::controller
