/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_CONTROLLERHELPERS_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_CONTROLLERHELPERS_H

#include "Core/Environment/LaneFollowing/point_distance_list.h"
#include "Core/Environment/LaneFollowing/point_list_traverser.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Environment/PathFinding/path.h"

#include <MantleAPI/Common/trajectory.h>

namespace astas::environment::controller
{

class ControllerHelpers
{
  public:
    explicit ControllerHelpers(const map::LaneLocationProvider* lane_location_provider);

    lanefollowing::PointDistanceList ComputePointDistanceListFromPosition(
        mantle_api::Vec3<units::length::meter_t> position);

    path_finding::Path ComputeRoute(const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints);

    lanefollowing::PointDistanceList ComputePointDistanceListFromPath(
        const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints);

    lanefollowing::PointDistanceList ConvertTrajectoryIntoPointDistanceList(const mantle_api::Trajectory& trajectory);

  private:
    const map::LaneLocationProvider* lane_location_provider_;
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_CONTROLLERHELPERS_H
