/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_WHEELCONTACT_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_WHEELCONTACT_H

#include "Core/Environment/Entities/vehicle_entity.h"

namespace astas::environment::controller
{
struct WheelContactPoints
{
    mantle_api::Vec3<units::length::meter_t> front_left;
    mantle_api::Vec3<units::length::meter_t> front_right;
    mantle_api::Vec3<units::length::meter_t> rear_left;
    mantle_api::Vec3<units::length::meter_t> rear_right;
};

class WheelContact
{
  public:
    void Init(mantle_api::IEntity* entity);

    WheelContactPoints GetWheelsWorldSpaceContactPoints() const;

  protected:
    void ComputeLocalSpaceWheelsContactPoints();

    entities::VehicleEntity* vehicle_entity_{nullptr};
    WheelContactPoints wheels_contact_points_{};
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_WHEELCONTACT_H
