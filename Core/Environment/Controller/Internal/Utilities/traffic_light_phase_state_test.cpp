/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/traffic_light_phase_state.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{

class TrafficLightPhaseStateFixture : public testing::Test
{
  public:
    TrafficLightPhaseStateFixture()
    {
        SetupTrafficLightEntity();
        SetupAllOnPhase();
    }

    mantle_api::TrafficLightPhase all_on_phase{};
    entities::TrafficLightEntity traffic_light_entity{1, "TestTrafficLight"};

  private:
    void SetupTrafficLightEntity()
    {
        mantle_ext::TrafficLightProperties traffic_light_properties;

        mantle_ext::TrafficLightBulbProperties red_bulb;
        red_bulb.color = mantle_api::TrafficLightBulbColor::kRed;
        red_bulb.icon = osi::OsiTrafficLightIcon::kNone;
        red_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

        mantle_ext::TrafficLightBulbProperties yellow_bulb;
        yellow_bulb.color = mantle_api::TrafficLightBulbColor::kYellow;
        yellow_bulb.icon = osi::OsiTrafficLightIcon::kNone;
        yellow_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

        mantle_ext::TrafficLightBulbProperties green_bulb;
        green_bulb.color = mantle_api::TrafficLightBulbColor::kGreen;
        green_bulb.icon = osi::OsiTrafficLightIcon::kNone;
        green_bulb.mode = mantle_api::TrafficLightBulbMode::kOff;

        traffic_light_properties.bulbs.push_back(red_bulb);
        traffic_light_properties.bulbs.push_back(yellow_bulb);
        traffic_light_properties.bulbs.push_back(green_bulb);

        traffic_light_entity.SetProperties(
            std::make_unique<mantle_ext::TrafficLightProperties>(std::move(traffic_light_properties)));
    }

    void SetupAllOnPhase()
    {
        all_on_phase.start_time = mantle_api::Time(0);
        all_on_phase.end_time = mantle_api::Time(10);
        all_on_phase.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kRed, mantle_api::TrafficLightBulbMode::kConstant});
        all_on_phase.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kYellow, mantle_api::TrafficLightBulbMode::kConstant});
        all_on_phase.bulb_states.push_back(
            {mantle_api::TrafficLightBulbColor::kGreen, mantle_api::TrafficLightBulbMode::kConstant});
    }
};

TEST_F(TrafficLightPhaseStateFixture, GivenTrafficLightPhaseState_WhenCreated_ThenDoesNotThrow)
{
    EXPECT_NO_THROW(TrafficLightPhaseState(all_on_phase, nullptr));
}

TEST_F(TrafficLightPhaseStateFixture, GivenTrafficLightPhaseState_WhenStarted_ThenEntityStateIsUpdated)
{
    TrafficLightPhaseState phase_state{all_on_phase, &traffic_light_entity};

    phase_state.Start();

    auto* traffic_light_properties = traffic_light_entity.GetProperties();

    EXPECT_EQ(mantle_api::TrafficLightBulbMode::kConstant, traffic_light_properties->bulbs.at(0).mode);
    EXPECT_EQ(mantle_api::TrafficLightBulbMode::kConstant, traffic_light_properties->bulbs.at(1).mode);
    EXPECT_EQ(mantle_api::TrafficLightBulbMode::kConstant, traffic_light_properties->bulbs.at(2).mode);
}

TEST_F(TrafficLightPhaseStateFixture, GivenTrafficLightPhaseWithinSimulationTime_WhenStepped_ThenPhaseHasNotFinished)
{
    TrafficLightPhaseState phase_state{all_on_phase, &traffic_light_entity};
    phase_state.Start();

    EXPECT_FALSE(phase_state.HasPhaseFinished(mantle_api::Time(5)));
}

TEST_F(TrafficLightPhaseStateFixture, GivenTrafficLightPhaseOutsideSimulationTime_WhenStepped_ThenPhaseHasFinished)
{
    TrafficLightPhaseState phase_state{all_on_phase, &traffic_light_entity};
    phase_state.Start();

    EXPECT_TRUE(phase_state.HasPhaseFinished(mantle_api::Time(10)));
}

}  // namespace astas::environment::controller
