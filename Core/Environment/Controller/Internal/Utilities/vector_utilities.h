/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_VECTORUTILITIES_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_VECTORUTILITIES_H

#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include <MantleAPI/Common/vector.h>

#include <cmath>

namespace astas::environment::controller
{

inline mantle_api::Vec3<units::velocity::meters_per_second_t> GetVelocityVector(
    units::angle::radian_t heading,
    units::angle::radian_t elevation,
    units::velocity::meters_per_second_t velocity)
{
    auto cos_elevation{std::cos(elevation())};
    return {static_cast<units::velocity::meters_per_second_t>(velocity * std::cos(heading()) * cos_elevation),
            static_cast<units::velocity::meters_per_second_t>(velocity * std::sin(heading()) * cos_elevation),
            static_cast<units::velocity::meters_per_second_t>(velocity * -std::sin(elevation()))};
}

inline mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> GetAccelerationVector(
    units::angle::radian_t heading,
    units::angle::radian_t elevation,
    units::acceleration::meters_per_second_squared_t acceleration)
{
    auto cos_elevation{std::cos(elevation())};
    return {static_cast<units::acceleration::meters_per_second_squared_t>(acceleration * std::cos(heading()) *
                                                                          cos_elevation),
            static_cast<units::acceleration::meters_per_second_squared_t>(acceleration * std::sin(heading()) *
                                                                          cos_elevation),
            static_cast<units::acceleration::meters_per_second_squared_t>(acceleration * -std::sin(elevation()))};
}

inline double GetVelocity(const mantle_api::Vec3<units::velocity::meters_per_second_t>& velocity)
{
    return velocity.Length()();
}

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_VECTORUTILITIES_H
