/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_PLAUSIBILITYCHECK_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_PLAUSIBILITYCHECK_H

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/AttributePlausibilityCheck/plausibility_check_orientation_derivatives.h"
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/AttributePlausibilityCheck/plausibility_check_position_derivatives.h"

#include <MantleAPI/Traffic/i_entity.h>
#include <units.h>

namespace astas::environment::plausibility_check
{
class PlausibilityCheck final
{
  public:
    void CheckPlausibility(const mantle_api::IEntity& entity,
                           const units::time::second_t& elapsed_time,
                           const std::string& map_coordinates);

  private:
    PlausibilityCheckOrientationDerivatives plausibility_check_orientation_derivatives_;
    PlausibilityCheckPositionDerivatives plausibility_check_position_derivatives_;
};
}  // namespace astas::environment::plausibility_check

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_PLAUSIBILITYCHECK_H
