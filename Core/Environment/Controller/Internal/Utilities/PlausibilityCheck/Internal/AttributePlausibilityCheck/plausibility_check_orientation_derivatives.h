/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_ATTRIBUTEPLAUSIBILITYCHECK_PLAUSIBILITYCHECKORIENTATIONDERIVATIVES_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_ATTRIBUTEPLAUSIBILITYCHECK_PLAUSIBILITYCHECKORIENTATIONDERIVATIVES_H

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_base.h"
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_log_messages.h"

#include <MantleAPI/Common/orientation.h>
#include <units.h>

#include <optional>

namespace astas::environment::plausibility_check
{

struct PlausibilityThresholdsOrientationDerivatives
{
    static constexpr units::angle::radian_t pitch_maximum{0.26};
    static constexpr units::angle::radian_t pitch_minimum{-0.26};

    static constexpr units::angle::radian_t roll_maximum{0.26};
    static constexpr units::angle::radian_t roll_minimum{-0.26};

    static constexpr units::angular_velocity::radians_per_second_t orientation_rate_maximum{0.5};
    static constexpr units::angular_velocity::radians_per_second_t orientation_rate_minimum{-0.5};

    static constexpr units::angular_acceleration::radians_per_second_squared_t orientation_acceleration_maximum{50};
    static constexpr units::angular_acceleration::radians_per_second_squared_t orientation_acceleration_minimum{-50};
};

class PlausibilityCheckOrientationDerivatives final : public PlausibilityCheckBase
{
  private:
    PlausibilityCheckOrientationDerivativesLogMessages log_messages_;
    PlausibilityThresholdsOrientationDerivatives thresholds_;

    void ProcessAttributes() override;
    void CompareInternalAndExternalAttributes() const override;
    void CheckExternalAttributesPlausibility() const override;
    void UpdatePreviousAttributes() override;
    void CheckExternalOrientationPlausibility() const;
    void CheckExternalOrientationRatePlausibility() const;
    void CheckExternalOrientationAccelerationPlausibility() const;

    mantle_api::Orientation3<units::angle::radian_t> current_external_orientation_{};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> current_external_orientation_rate_{};
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>
        current_external_orientation_acceleration_{};
    std::optional<mantle_api::Orientation3<units::angle::radian_t>> previous_external_orientation_{std::nullopt};
    std::optional<mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>>
        previous_internal_orientation_rate_{std::nullopt};
    std::optional<mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>>
        current_internal_orientation_rate_{std::nullopt};
    std::optional<mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>>
        current_internal_orientation_acceleration_{std::nullopt};
};

}  // namespace astas::environment::plausibility_check

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_ATTRIBUTEPLAUSIBILITYCHECK_PLAUSIBILITYCHECKORIENTATIONDERIVATIVES_H
