/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/AttributePlausibilityCheck/plausibility_check_orientation_derivatives.h"

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_test_fixture.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <fmt/format.h>
#include <units.h>

namespace astas::environment::plausibility_check
{

using units::literals::operator""_s;
using units::literals::operator""_rad;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;
using Orientation = mantle_api::Orientation3<units::angle::radian_t>;
using OrientationRate = mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>;
using OrientationAcceleration = mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>;

class PlausibilityCheckOrientationDerivativesFixture : public PlausibilityCheckTestFixture
{
  protected:
    PlausibilityCheckOrientationDerivatives plausibility_check_orientation_derivatives_;
    PlausibilityCheckOrientationDerivativesLogMessages log_messages_;
    PlausibilityThresholdsOrientationDerivatives thresholds_;
};

TEST_F(PlausibilityCheckOrientationDerivativesFixture, GivenMoreThanMaximumOrientation_WhenStep_ThenOutputError)
{
    const Orientation orientation{0.0_rad, 0.3_rad, 0.3_rad};

    const auto pitch_above_message{fmt::format(log_messages_.pitch_above_threshold,
                                               vehicle_id,
                                               nds_position,
                                               orientation.pitch.value(),
                                               thresholds_.pitch_maximum.value())};

    const auto roll_above_message{fmt::format(log_messages_.pitch_above_threshold,
                                              vehicle_id,
                                              nds_position,
                                              orientation.roll.value(),
                                              thresholds_.roll_maximum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientation(orientation);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(pitch_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(roll_above_message));
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture, GivenLessThanMinimumOrientation_WhenStep_ThenOutputError)
{
    const Orientation orientation{0.0_rad, -0.3_rad, -0.3_rad};

    const auto pitch_below_message{fmt::format(log_messages_.pitch_below_threshold,
                                               vehicle_id,
                                               nds_position,
                                               orientation.pitch.value(),
                                               thresholds_.pitch_minimum.value())};

    const auto roll_below_message{fmt::format(log_messages_.pitch_below_threshold,
                                              vehicle_id,
                                              nds_position,
                                              orientation.roll.value(),
                                              thresholds_.roll_minimum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientation(orientation);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(pitch_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(roll_below_message));
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture, GivenPlausibleOrientation_WhenStep_ThenNoOutputError)
{
    const Orientation orientation{0.0_rad, 0.1_rad, 0.1_rad};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientation(orientation);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture, GivenMoreThanMaximumOrientationRate_WhenStep_ThenOutputError)
{
    const OrientationRate orientation_rate{1.0_rad_per_s, 1.0_rad_per_s, 1.0_rad_per_s};

    const auto yaw_rate_above_message{fmt::format(log_messages_.yaw_rate_above_threshold,
                                                  vehicle_id,
                                                  nds_position,
                                                  orientation_rate.yaw.value(),
                                                  thresholds_.orientation_rate_maximum.value())};

    const auto pitch_rate_above_message{fmt::format(log_messages_.pitch_rate_above_threshold,
                                                    vehicle_id,
                                                    nds_position,
                                                    orientation_rate.pitch.value(),
                                                    thresholds_.orientation_rate_maximum.value())};

    const auto roll_rate_above_message{fmt::format(log_messages_.pitch_rate_above_threshold,
                                                   vehicle_id,
                                                   nds_position,
                                                   orientation_rate.roll.value(),
                                                   thresholds_.orientation_rate_maximum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientationRate(orientation_rate);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(yaw_rate_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(pitch_rate_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(roll_rate_above_message));
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture, GivenLessThanMinimumOrientationRate_WhenStep_ThenOutputError)
{
    const OrientationRate orientation_rate{-1.0_rad_per_s, -1.0_rad_per_s, -1.0_rad_per_s};

    const auto yaw_rate_below_message{fmt::format(log_messages_.yaw_rate_below_threshold,
                                                  vehicle_id,
                                                  nds_position,
                                                  orientation_rate.yaw.value(),
                                                  thresholds_.orientation_rate_minimum.value())};

    const auto pitch_rate_below_message{fmt::format(log_messages_.pitch_rate_below_threshold,
                                                    vehicle_id,
                                                    nds_position,
                                                    orientation_rate.pitch.value(),
                                                    thresholds_.orientation_rate_minimum.value())};

    const auto roll_rate_below_message{fmt::format(log_messages_.roll_rate_below_threshold,
                                                   vehicle_id,
                                                   nds_position,
                                                   orientation_rate.roll.value(),
                                                   thresholds_.orientation_rate_minimum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientationRate(orientation_rate);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(yaw_rate_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(pitch_rate_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(roll_rate_below_message));
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture, GivenPlausibleOrientationRate_WhenStep_ThenNoOutputError)
{
    const OrientationRate orientation_rate{0.1_rad_per_s, 0.1_rad_per_s, 0.1_rad_per_s};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientationRate(orientation_rate);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture,
       GivenMoreThanMaximumOrientationAcceleration_WhenStep_ThenOutputError)
{
    const OrientationAcceleration orientation_acceleration{60.0_rad_per_s_sq, 60.0_rad_per_s_sq, 60.0_rad_per_s_sq};

    const auto yaw_acceleration_above_message{fmt::format(log_messages_.yaw_acceleration_above_threshold,
                                                          vehicle_id,
                                                          nds_position,
                                                          orientation_acceleration.yaw.value(),
                                                          thresholds_.orientation_acceleration_maximum.value())};

    const auto pitch_acceleration_above_message{fmt::format(log_messages_.pitch_acceleration_above_threshold,
                                                            vehicle_id,
                                                            nds_position,
                                                            orientation_acceleration.pitch.value(),
                                                            thresholds_.orientation_acceleration_maximum.value())};

    const auto roll_acceleration_above_message{fmt::format(log_messages_.pitch_acceleration_above_threshold,
                                                           vehicle_id,
                                                           nds_position,
                                                           orientation_acceleration.roll.value(),
                                                           thresholds_.orientation_acceleration_maximum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientationAcceleration(orientation_acceleration);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(yaw_acceleration_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(pitch_acceleration_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(roll_acceleration_above_message));
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture,
       GivenLessThanMinimumOrientationAcceleration_WhenStep_ThenOutputError)
{
    const OrientationAcceleration orientation_acceleration{-60.0_rad_per_s_sq, -60.0_rad_per_s_sq, -60.0_rad_per_s_sq};

    const auto yaw_acceleration_below_message{fmt::format(log_messages_.yaw_acceleration_below_threshold,
                                                          vehicle_id,
                                                          nds_position,
                                                          orientation_acceleration.yaw.value(),
                                                          thresholds_.orientation_acceleration_minimum.value())};

    const auto pitch_acceleration_below_message{fmt::format(log_messages_.pitch_acceleration_below_threshold,
                                                            vehicle_id,
                                                            nds_position,
                                                            orientation_acceleration.pitch.value(),
                                                            thresholds_.orientation_acceleration_minimum.value())};

    const auto roll_acceleration_below_message{fmt::format(log_messages_.pitch_acceleration_below_threshold,
                                                           vehicle_id,
                                                           nds_position,
                                                           orientation_acceleration.roll.value(),
                                                           thresholds_.orientation_acceleration_minimum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientationAcceleration(orientation_acceleration);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(yaw_acceleration_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(pitch_acceleration_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(roll_acceleration_below_message));
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture, GivenPlausibleOrientationAcceleration_WhenStep_ThenNoOutputError)
{
    const OrientationAcceleration orientation_acceleration{10.0_rad_per_s_sq, 10.0_rad_per_s_sq, 10.0_rad_per_s_sq};

    testing::internal::CaptureStdout();
    vehicle_->SetOrientationAcceleration(orientation_acceleration);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, nds_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture,
       GivenExpectedOrientationRatesAndAcceleration_WhenStep_ThenNoOutputWarning)
{
    const Orientation orientation_1{0.0_rad, 0.0_rad, 0.0_rad};
    const Orientation orientation_2{0.1_rad, 0.1_rad, 0.1_rad};
    const Orientation orientation_3{0.25_rad, 0.25_rad, 0.25_rad};
    const OrientationRate orientation_rate_1{0.1_rad_per_s, 0.1_rad_per_s, 0.1_rad_per_s};
    const OrientationRate orientation_rate_2{0.15_rad_per_s, 0.15_rad_per_s, 0.15_rad_per_s};
    const OrientationAcceleration orientation_acceleration{0.05_rad_per_s_sq, 0.05_rad_per_s_sq, 0.05_rad_per_s_sq};

    testing::internal::CaptureStdout();

    vehicle_->SetOrientation(orientation_1);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    vehicle_->SetOrientation(orientation_2);
    vehicle_->SetOrientationRate(orientation_rate_1);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    vehicle_->SetOrientation(orientation_3);
    vehicle_->SetOrientationRate(orientation_rate_2);
    vehicle_->SetOrientationAcceleration(orientation_acceleration);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture,
       GivenUnexpectedOrientationRatesAndAcceleration_WhenStep_ThenOutputWarning)
{
    const Orientation orientation_1{0.0_rad, 0.0_rad, 0.0_rad};
    const Orientation orientation_2{0.1_rad, 0.1_rad, 0.1_rad};
    const Orientation orientation_3{0.25_rad, 0.25_rad, 0.25_rad};
    const OrientationRate orientation_rate_1{0.2_rad_per_s, 0.2_rad_per_s, 0.2_rad_per_s};
    const OrientationRate orientation_rate_2{0.3_rad_per_s, 0.3_rad_per_s, 0.3_rad_per_s};
    const OrientationAcceleration orientation_acceleration{0.5_rad_per_s_sq, 0.5_rad_per_s_sq, 0.5_rad_per_s_sq};

    const OrientationRate expected_orientation_rate_1{0.1_rad_per_s, 0.1_rad_per_s, 0.1_rad_per_s};
    const OrientationRate expected_orientation_rate_2{0.15_rad_per_s, 0.15_rad_per_s, 0.15_rad_per_s};
    const OrientationAcceleration expected_orientation_acceleration{
        0.04999999999999999_rad_per_s_sq, 0.04999999999999999_rad_per_s_sq, 0.04999999999999999_rad_per_s_sq};

    const auto unexpected_yaw_rate_1_message{fmt::format(log_messages_.yaw_rate_unexpected,
                                                         vehicle_id,
                                                         odr_position,
                                                         orientation_rate_1.yaw.value(),
                                                         expected_orientation_rate_1.yaw.value())};

    const auto unexpected_pitch_rate_1_message{fmt::format(log_messages_.pitch_rate_unexpected,
                                                           vehicle_id,
                                                           odr_position,
                                                           orientation_rate_1.pitch.value(),
                                                           expected_orientation_rate_1.pitch.value())};

    const auto unexpected_roll_rate_1_message{fmt::format(log_messages_.roll_rate_unexpected,
                                                          vehicle_id,
                                                          odr_position,
                                                          orientation_rate_1.roll.value(),
                                                          expected_orientation_rate_1.roll.value())};

    const auto unexpected_yaw_rate_2_message{fmt::format(log_messages_.yaw_rate_unexpected,
                                                         vehicle_id,
                                                         odr_position,
                                                         orientation_rate_2.yaw.value(),
                                                         expected_orientation_rate_2.yaw.value())};

    const auto unexpected_pitch_rate_2_message{fmt::format(log_messages_.pitch_rate_unexpected,
                                                           vehicle_id,
                                                           odr_position,
                                                           orientation_rate_2.pitch.value(),
                                                           expected_orientation_rate_2.pitch.value())};

    const auto unexpected_roll_rate_2_message{fmt::format(log_messages_.roll_rate_unexpected,
                                                          vehicle_id,
                                                          odr_position,
                                                          orientation_rate_2.roll.value(),
                                                          expected_orientation_rate_2.roll.value())};

    const auto unexpected_yaw_acceleration_message{fmt::format(log_messages_.yaw_acceleration_unexpected,
                                                               vehicle_id,
                                                               odr_position,
                                                               orientation_acceleration.yaw.value(),
                                                               expected_orientation_acceleration.yaw.value())};

    const auto unexpected_pitch_acceleration_message{fmt::format(log_messages_.pitch_acceleration_unexpected,
                                                                 vehicle_id,
                                                                 odr_position,
                                                                 orientation_acceleration.pitch.value(),
                                                                 expected_orientation_acceleration.pitch.value())};

    const auto unexpected_roll_acceleration_message{fmt::format(log_messages_.roll_acceleration_unexpected,
                                                                vehicle_id,
                                                                odr_position,
                                                                orientation_acceleration.roll.value(),
                                                                expected_orientation_acceleration.roll.value())};

    testing::internal::CaptureStdout();

    vehicle_->SetOrientation(orientation_1);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    vehicle_->SetOrientation(orientation_2);
    vehicle_->SetOrientationRate(orientation_rate_1);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    vehicle_->SetOrientation(orientation_3);
    vehicle_->SetOrientationRate(orientation_rate_2);
    vehicle_->SetOrientationAcceleration(orientation_acceleration);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_yaw_rate_1_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_pitch_rate_1_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_roll_rate_1_message));

    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_yaw_rate_2_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_pitch_rate_2_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_roll_rate_2_message));

    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_yaw_acceleration_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_pitch_acceleration_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_roll_acceleration_message));
}

TEST_F(PlausibilityCheckOrientationDerivativesFixture,
       GivenUnexpectedOrientationRatesAndAcceleration_WhenStepAndDifferenceBelowEpsilon_ThenNoOutputWarning)
{
    const Orientation orientation_1{0.0_rad, 0.0_rad, 0.0_rad};
    const Orientation orientation_2{0.1_rad, 0.1_rad, 0.1_rad};
    const Orientation orientation_3{0.25_rad, 0.25_rad, 0.25_rad};
    const OrientationRate orientation_rate_1{0.095_rad_per_s, 0.095_rad_per_s, 0.095_rad_per_s};
    const OrientationRate orientation_rate_2{0.145_rad_per_s, 0.145_rad_per_s, 0.145_rad_per_s};
    const OrientationAcceleration orientation_acceleration{0.045_rad_per_s_sq, 0.045_rad_per_s_sq, 0.045_rad_per_s_sq};

    testing::internal::CaptureStdout();

    vehicle_->SetOrientation(orientation_1);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    vehicle_->SetOrientation(orientation_2);
    vehicle_->SetOrientationRate(orientation_rate_1);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    vehicle_->SetOrientation(orientation_3);
    vehicle_->SetOrientationRate(orientation_rate_2);
    vehicle_->SetOrientationAcceleration(orientation_acceleration);
    plausibility_check_orientation_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

}  // namespace astas::environment::plausibility_check
