/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/AttributePlausibilityCheck/plausibility_check_position_derivatives.h"

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_log_messages.h"
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_test_fixture.h"

#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>
#include <fmt/format.h>
#include <units.h>

namespace astas::environment::plausibility_check
{

using units::literals::operator""_s;
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using Position = mantle_api::Vec3<units::length::meter_t>;
using Velocity = mantle_api::Vec3<units::velocity::meters_per_second_t>;
using Acceleration = mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>;

class PlausibilityCheckPositionDerivativesFixture : public PlausibilityCheckTestFixture
{
  protected:
    PlausibilityCheckPositionDerivatives plausibility_check_position_derivatives_;
    PlausibilityCheckPositionDerivativesLogMessages log_messages_;
    PlausibilityThresholdsPositionDerivatives thresholds_;
};

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenMoreThanMaximumVelocity_WhenStep_ThenOutputWarning)
{
    const Velocity velocity{90.0_mps, 5.0_mps, 1.0_mps};

    const auto longitudinal_velocity_above_message{fmt::format(log_messages_.longitudinal_velocity_above_threshold,
                                                               vehicle_id,
                                                               odr_position,
                                                               velocity.x.value(),
                                                               thresholds_.longitudinal_velocity_maximum.value())};

    const auto lateral_velocity_above_message{fmt::format(log_messages_.lateral_velocity_above_threshold,
                                                          vehicle_id,
                                                          odr_position,
                                                          velocity.y.value(),
                                                          thresholds_.lateral_velocity_maximum.value())};

    const auto vertical_velocity_above_message{fmt::format(log_messages_.vertical_velocity_above_threshold,
                                                           vehicle_id,
                                                           odr_position,
                                                           velocity.z.value(),
                                                           thresholds_.vertical_velocity_maximum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetVelocity(velocity);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(longitudinal_velocity_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(lateral_velocity_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(vertical_velocity_above_message));
}

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenLessThanMinimumVelocity_WhenStep_ThenOutputWarning)
{
    const Velocity velocity{-14.0_mps, -5.0_mps, -1.0_mps};

    const auto longitudinal_velocity_below_message{fmt::format(log_messages_.longitudinal_velocity_below_threshold,
                                                               vehicle_id,
                                                               odr_position,
                                                               velocity.x.value(),
                                                               thresholds_.longitudinal_velocity_minimum.value())};

    const auto lateral_velocity_below_message{fmt::format(log_messages_.lateral_velocity_below_threshold,
                                                          vehicle_id,
                                                          odr_position,
                                                          velocity.y.value(),
                                                          thresholds_.lateral_velocity_minimum.value())};

    const auto vertical_velocity_below_message{fmt::format(log_messages_.vertical_velocity_below_threshold,
                                                           vehicle_id,
                                                           odr_position,
                                                           velocity.z.value(),
                                                           thresholds_.vertical_velocity_minimum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetVelocity(velocity);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(longitudinal_velocity_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(lateral_velocity_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(vertical_velocity_below_message));
}

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenPlausibleVelocity_WhenStep_ThenNoOutputWarning)
{
    const Velocity velocity{1.0_mps, 1.0_mps, 0.5_mps};

    testing::internal::CaptureStdout();
    vehicle_->SetVelocity(velocity);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenMoreThanMaximumAcceleration_WhenStep_ThenOutputWarning)
{
    const Acceleration acceleration{13_mps_sq, 13_mps_sq, 0_mps_sq};

    const auto longitudinal_acceleration_above_message{
        fmt::format(log_messages_.longitudinal_acceleration_above_threshold,
                    vehicle_id,
                    odr_position,
                    acceleration.x.value(),
                    thresholds_.longitudinal_acceleration_maximum.value())};

    const auto lateral_acceleration_above_message{fmt::format(log_messages_.lateral_acceleration_above_threshold,
                                                              vehicle_id,
                                                              odr_position,
                                                              acceleration.y.value(),
                                                              thresholds_.lateral_acceleration_maximum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetAcceleration(acceleration);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(longitudinal_acceleration_above_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(lateral_acceleration_above_message));
}

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenLessThanMinimumAcceleration_WhenStep_ThenOutputWarning)
{
    const Acceleration acceleration{-13_mps_sq, -13_mps_sq, 0_mps_sq};

    const auto longitudinal_acceleration_below_message{
        fmt::format(log_messages_.longitudinal_acceleration_below_threshold,
                    vehicle_id,
                    odr_position,
                    acceleration.x.value(),
                    thresholds_.longitudinal_acceleration_minimum.value())};

    const auto lateral_acceleration_below_message{fmt::format(log_messages_.lateral_acceleration_below_threshold,
                                                              vehicle_id,
                                                              odr_position,
                                                              acceleration.y.value(),
                                                              thresholds_.lateral_acceleration_minimum.value())};

    testing::internal::CaptureStdout();
    vehicle_->SetAcceleration(acceleration);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);
    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(longitudinal_acceleration_below_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(lateral_acceleration_below_message));
}

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenPlausibleAcceleration_WhenStep_ThenNoOutputWarning)
{
    const Acceleration acceleration{1_mps_sq, 1_mps_sq, 0_mps_sq};

    testing::internal::CaptureStdout();
    vehicle_->SetAcceleration(acceleration);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenExpectedVelocitiesAndAcceleration_WhenStep_ThenNoOutputWarning)
{
    const Position position_1{0.0_m, 0.0_m, 0.0_m};
    const Position position_2{0.5_m, 0.5_m, 0.5_m};
    const Position position_3{0.75_m, 0.75_m, 0.75_m};
    const Velocity velocity_1{0.5_mps, 0.5_mps, 0.5_mps};
    const Velocity velocity_2{0.25_mps, 0.25_mps, 0.25_mps};
    const Acceleration acceleration{-0.25_mps_sq, -0.25_mps_sq, -0.25_mps_sq};

    testing::internal::CaptureStdout();

    vehicle_->SetPosition(position_1);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    vehicle_->SetPosition(position_2);
    vehicle_->SetVelocity(velocity_1);
    plausibility_check_position_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    vehicle_->SetPosition(position_3);
    vehicle_->SetVelocity(velocity_2);
    vehicle_->SetAcceleration(acceleration);
    plausibility_check_position_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

TEST_F(PlausibilityCheckPositionDerivativesFixture, GivenUnexpectedVelocitiesAndAcceleration_WhenStep_ThenOutputWarning)
{
    const Position position_1{0.0_m, 0.0_m, 0.0_m};
    const Position position_2{0.5_m, 0.5_m, 0.5_m};
    const Position position_3{0.75_m, 0.75_m, 0.75_m};
    const Velocity velocity_1{2.0_mps, 2.0_mps, 2.0_mps};
    const Velocity velocity_2{4.0_mps, 4.0_mps, 4.0_mps};
    const Acceleration acceleration{2.0_mps_sq, 2.0_mps_sq, 2.0_mps_sq};

    const Velocity expected_velocity_1{0.5_mps, 0.5_mps, 0.5_mps};
    const Velocity expected_velocity_2{0.25_mps, 0.25_mps, 0.25_mps};
    const Acceleration expected_acceleration{-0.25_mps_sq, -0.25_mps_sq, -0.25_mps_sq};

    const auto unexpected_longitudinal_velocity1_message{fmt::format(log_messages_.longitudinal_velocity_unexpected,
                                                                     vehicle_id,
                                                                     odr_position,
                                                                     velocity_1.x.value(),
                                                                     expected_velocity_1.x.value())};

    const auto unexpected_lateral_velocity1_message{fmt::format(log_messages_.lateral_velocity_unexpected,
                                                                vehicle_id,
                                                                odr_position,
                                                                velocity_1.y.value(),
                                                                expected_velocity_1.y.value())};

    const auto unexpected_vertical_velocity1_message{fmt::format(log_messages_.vertical_velocity_unexpected,
                                                                 vehicle_id,
                                                                 odr_position,
                                                                 velocity_1.z.value(),
                                                                 expected_velocity_1.z.value())};

    const auto unexpected_longitudinal_velocity2_message{fmt::format(log_messages_.longitudinal_velocity_unexpected,
                                                                     vehicle_id,
                                                                     odr_position,
                                                                     velocity_2.x.value(),
                                                                     expected_velocity_2.x.value())};

    const auto unexpected_lateral_velocity2_message{fmt::format(log_messages_.lateral_velocity_unexpected,
                                                                vehicle_id,
                                                                odr_position,
                                                                velocity_2.y.value(),
                                                                expected_velocity_2.y.value())};

    const auto unexpected_vertical_velocity2_message{fmt::format(log_messages_.vertical_velocity_unexpected,
                                                                 vehicle_id,
                                                                 odr_position,
                                                                 velocity_2.z.value(),
                                                                 expected_velocity_2.z.value())};

    const auto unexpected_longitudinal_acceleration_message{
        fmt::format(log_messages_.longitudinal_acceleration_unexpected,
                    vehicle_id,
                    odr_position,
                    acceleration.x.value(),
                    expected_acceleration.x.value())};

    const auto unexpected_lateral_acceleration_message{fmt::format(log_messages_.lateral_acceleration_unexpected,
                                                                   vehicle_id,
                                                                   odr_position,
                                                                   acceleration.y.value(),
                                                                   expected_acceleration.y.value())};

    testing::internal::CaptureStdout();

    vehicle_->SetPosition(position_1);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    vehicle_->SetPosition(position_2);
    vehicle_->SetVelocity(velocity_1);
    plausibility_check_position_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    vehicle_->SetPosition(position_3);
    vehicle_->SetVelocity(velocity_2);
    vehicle_->SetAcceleration(acceleration);
    plausibility_check_position_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    const auto captured_stdout{testing::internal::GetCapturedStdout()};

    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_longitudinal_velocity1_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_lateral_velocity1_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_vertical_velocity1_message));

    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_longitudinal_velocity2_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_lateral_velocity2_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_vertical_velocity2_message));

    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_longitudinal_acceleration_message));
    EXPECT_THAT(captured_stdout, testing::HasSubstr(unexpected_lateral_acceleration_message));
}

TEST_F(PlausibilityCheckPositionDerivativesFixture,
       GivenUnexpectedVelocitiesAndAcceleration_WhenStepAndDifferenceBelowEpsilon_ThenNoOutputWarning)
{
    const Position position_1{0.0_m, 0.0_m, 0.0_m};
    const Position position_2{0.5_m, 0.5_m, 0.5_m};
    const Position position_3{0.75_m, 0.75_m, 0.75_m};
    const Velocity velocity_1{0.495_mps, 0.495_mps, 0.495_mps};
    const Velocity velocity_2{0.245_mps, 0.245_mps, 0.245_mps};
    const Acceleration acceleration{-0.245_mps_sq, -0.245_mps_sq, -0.245_mps_sq};

    testing::internal::CaptureStdout();

    vehicle_->SetPosition(position_1);
    plausibility_check_position_derivatives_.Step(*vehicle_, 0.0_s, odr_position);

    vehicle_->SetPosition(position_2);
    vehicle_->SetVelocity(velocity_1);
    plausibility_check_position_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    vehicle_->SetPosition(position_3);
    vehicle_->SetVelocity(velocity_2);
    vehicle_->SetAcceleration(acceleration);
    plausibility_check_position_derivatives_.Step(*vehicle_, elapsed_time_per_step, odr_position);

    EXPECT_THAT(testing::internal::GetCapturedStdout(), testing::IsEmpty());
}

}  // namespace astas::environment::plausibility_check
