/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/AttributePlausibilityCheck/plausibility_check_position_derivatives.h"

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_base.h"
#include "Core/Service/Utility/derivative_utils.h"

namespace astas::environment::plausibility_check
{

using Velocity = units::velocity::meters_per_second_t;
using Acceleration = units::acceleration::meters_per_second_squared_t;

void PlausibilityCheckPositionDerivatives::ProcessAttributes()
{
    current_external_position_ = entity_->GetPosition();
    current_external_velocity_ = entity_->GetVelocity();
    current_external_acceleration_ = entity_->GetAcceleration();

    if (previous_external_position_.has_value())
    {
        current_internal_velocity_ = service::utility::GetPositionDerivativeVector<units::length::meter_t, Velocity>(
            current_external_position_, previous_external_position_.value(), elapsed_time_);
    }

    if (previous_internal_velocity_.has_value())
    {
        current_internal_acceleration_ = service::utility::GetPositionDerivativeVector<Velocity, Acceleration>(
            current_internal_velocity_.value(), previous_internal_velocity_.value(), elapsed_time_);
    }
}

void PlausibilityCheckPositionDerivatives::CompareInternalAndExternalAttributes() const
{
    if (current_internal_velocity_.has_value())
    {
        CompareAndLog<Velocity>(current_external_velocity_.x,
                                current_internal_velocity_->x,
                                &details::NotAlmostEqual<Velocity>,
                                log_messages_.longitudinal_velocity_unexpected);
        CompareAndLog<Velocity>(current_external_velocity_.y,
                                current_internal_velocity_->y,
                                &details::NotAlmostEqual<Velocity>,
                                log_messages_.lateral_velocity_unexpected);
        CompareAndLog<Velocity>(current_external_velocity_.z,
                                current_internal_velocity_->z,
                                &details::NotAlmostEqual<Velocity>,
                                log_messages_.vertical_velocity_unexpected);
    }

    if (current_internal_acceleration_.has_value())
    {
        CompareAndLog<Acceleration>(current_external_acceleration_.x,
                                    current_internal_acceleration_->x,
                                    &details::NotAlmostEqual<Acceleration>,
                                    log_messages_.longitudinal_acceleration_unexpected);
        CompareAndLog<Acceleration>(current_external_acceleration_.y,
                                    current_internal_acceleration_->y,
                                    &details::NotAlmostEqual<Acceleration>,
                                    log_messages_.lateral_acceleration_unexpected);
    }
}

void PlausibilityCheckPositionDerivatives::CheckExternalVelocityPlausibility() const
{
    CompareAndLog<Velocity>(current_external_velocity_.x,
                            thresholds_.longitudinal_velocity_maximum,
                            std::greater<Velocity>(),
                            log_messages_.longitudinal_velocity_above_threshold);
    CompareAndLog<Velocity>(current_external_velocity_.x,
                            thresholds_.longitudinal_velocity_minimum,
                            std::less<Velocity>(),
                            log_messages_.longitudinal_velocity_below_threshold);

    CompareAndLog<Velocity>(current_external_velocity_.y,
                            thresholds_.lateral_velocity_maximum,
                            std::greater<Velocity>(),
                            log_messages_.lateral_velocity_above_threshold);
    CompareAndLog<Velocity>(current_external_velocity_.y,
                            thresholds_.lateral_velocity_minimum,
                            std::less<Velocity>(),
                            log_messages_.lateral_velocity_below_threshold);

    // we check the vertical velocity to make sure we don't have cars flying for a few steps
    CompareAndLog<Velocity>(current_external_velocity_.z,
                            thresholds_.vertical_velocity_maximum,
                            std::greater<Velocity>(),
                            log_messages_.vertical_velocity_above_threshold);
    CompareAndLog<Velocity>(current_external_velocity_.z,
                            thresholds_.vertical_velocity_minimum,
                            std::less<Velocity>(),
                            log_messages_.vertical_velocity_below_threshold);
}

void PlausibilityCheckPositionDerivatives::CheckExternalAccelerationPlausibility() const
{
    CompareAndLog<Acceleration>(current_external_acceleration_.x,
                                thresholds_.longitudinal_acceleration_maximum,
                                std::greater<Acceleration>(),
                                log_messages_.longitudinal_acceleration_above_threshold);
    CompareAndLog<Acceleration>(current_external_acceleration_.x,
                                thresholds_.longitudinal_acceleration_minimum,
                                std::less<Acceleration>(),
                                log_messages_.longitudinal_acceleration_below_threshold);

    CompareAndLog<Acceleration>(current_external_acceleration_.y,
                                thresholds_.lateral_acceleration_maximum,
                                std::greater<Acceleration>(),
                                log_messages_.lateral_acceleration_above_threshold);
    CompareAndLog<Acceleration>(current_external_acceleration_.y,
                                thresholds_.lateral_acceleration_minimum,
                                std::less<Acceleration>(),
                                log_messages_.lateral_acceleration_below_threshold);
}

void PlausibilityCheckPositionDerivatives::CheckExternalAttributesPlausibility() const
{
    CheckExternalVelocityPlausibility();
    CheckExternalAccelerationPlausibility();
}

void PlausibilityCheckPositionDerivatives::UpdatePreviousAttributes()
{
    previous_external_position_ = current_external_position_;

    if (current_internal_velocity_.has_value())
    {
        previous_internal_velocity_ = current_internal_velocity_;
    }
}

}  // namespace astas::environment::plausibility_check
