/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKBASE_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKBASE_H

#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <units.h>

namespace details
{

constexpr double comparison_epsilon{1e-2};

template <typename T>
bool NotAlmostEqual(T lhs, T rhs)
{
    static_assert(units::traits::is_unit_t<T>(), "Template parameter must be a unit type");
    return !mantle_api::AlmostEqual(lhs, rhs, T(comparison_epsilon), true);
}

}  // namespace details
namespace astas::environment::plausibility_check
{
class PlausibilityCheckBase
{
  protected:
    const mantle_api::IEntity* entity_{nullptr};
    mantle_api::UniqueId entity_id_{};
    units::time::second_t elapsed_time_{};
    std::string map_coordinates_{};

    virtual void ProcessAttributes() = 0;
    virtual void CompareInternalAndExternalAttributes() const = 0;
    virtual void CheckExternalAttributesPlausibility() const = 0;
    virtual void UpdatePreviousAttributes() = 0;

    template <typename T>
    void CompareAndLog(T lhs,
                       T rhs,
                       const std::function<bool(T, T)>& compare_function,
                       const std::string& log_message) const
    {
        if (compare_function(lhs, rhs))
        {
            Warn(log_message.c_str(), entity_id_, map_coordinates_, lhs, rhs);
        }
    }

  public:
    void Step(const mantle_api::IEntity& entity,
              const units::time::second_t& elapsed_time,
              const std::string& map_coordinates);
};

}  // namespace astas::environment::plausibility_check

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKBASE_H
