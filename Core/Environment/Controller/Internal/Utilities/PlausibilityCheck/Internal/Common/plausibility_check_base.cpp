/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_base.h"

namespace astas::environment::plausibility_check
{

void PlausibilityCheckBase::Step(const mantle_api::IEntity& entity,
                                 const units::time::second_t& elapsed_time,
                                 const std::string& map_coordinates)
{
    entity_ = &entity;
    entity_id_ = entity_->GetUniqueId();
    elapsed_time_ = elapsed_time;
    map_coordinates_ = map_coordinates;

    ProcessAttributes();
    CompareInternalAndExternalAttributes();
    CheckExternalAttributesPlausibility();
    UpdatePreviousAttributes();
}

}  // namespace astas::environment::plausibility_check
