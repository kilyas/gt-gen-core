/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKTESTFIXTURE_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKTESTFIXTURE_H

#include "Core/Environment/Entities/vehicle_entity.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <units.h>

namespace astas::environment::plausibility_check
{

class PlausibilityCheckTestFixture : public testing::Test
{
  protected:
    PlausibilityCheckTestFixture() { vehicle_ = std::make_unique<entities::VehicleEntity>(vehicle_id, "host"); }

    std::unique_ptr<mantle_api::IVehicle> vehicle_{nullptr};
    const mantle_api::UniqueId vehicle_id{123};
    const std::string nds_position{"48.34, 11.71"};
    const std::string odr_position{"road 0 lane -3 s 10.000 t 0.000"};
    const units::time::second_t elapsed_time_per_step{1.0};
};

}  // namespace astas::environment::plausibility_check

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKTESTFIXTURE_H
