/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKLOGMESSAGES_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKLOGMESSAGES_H

#include <fmt/format.h>

namespace astas::environment::plausibility_check
{

class PlausibilityCheckLogMessages
{
  private:
    // Double braces are necessary to allow insertion for future 'format' calls.
    // For now, we simply want to insert into 'is_above_message' and 'is_below_message'
    const std::string vehicle{"Vehicle #{{}} at position [{{}}] : {}"};
    const std::string is_unexpected{"={{}} is unexpected. Expected value={{}}"};
    const std::string is_above{"={{}} is above maximum "};
    const std::string is_below{"={{}} is below minimum "};
    const std::string threshold{"threshold of plausibility={{}}"};

  public:
    const std::string is_unexpected_message{vehicle + is_unexpected};
    const std::string is_above_message{vehicle + is_above + threshold};
    const std::string is_below_message{vehicle + is_below + threshold};
};

class PlausibilityCheckPositionDerivativesLogMessages : public PlausibilityCheckLogMessages
{
  private:
    const std::string longitudinal_velocity{"longitudinal velocity"};
    const std::string lateral_velocity{"lateral velocity"};
    const std::string vertical_velocity{"vertical velocity"};
    const std::string longitudinal_acceleration{"longitudinal acceleration"};
    const std::string lateral_acceleration{"lateral acceleration"};

  public:
    const std::string longitudinal_velocity_unexpected;
    const std::string longitudinal_velocity_above_threshold;
    const std::string longitudinal_velocity_below_threshold;
    const std::string lateral_velocity_unexpected;
    const std::string lateral_velocity_above_threshold;
    const std::string lateral_velocity_below_threshold;
    const std::string vertical_velocity_unexpected;
    const std::string vertical_velocity_above_threshold;
    const std::string vertical_velocity_below_threshold;
    const std::string longitudinal_acceleration_unexpected;
    const std::string longitudinal_acceleration_above_threshold;
    const std::string longitudinal_acceleration_below_threshold;
    const std::string lateral_acceleration_unexpected;
    const std::string lateral_acceleration_above_threshold;
    const std::string lateral_acceleration_below_threshold;

    PlausibilityCheckPositionDerivativesLogMessages()
        : longitudinal_velocity_unexpected(fmt::format(is_unexpected_message, longitudinal_velocity)),
          longitudinal_velocity_above_threshold(fmt::format(is_above_message, longitudinal_velocity)),
          longitudinal_velocity_below_threshold(fmt::format(is_below_message, longitudinal_velocity)),
          lateral_velocity_unexpected(fmt::format(is_unexpected_message, lateral_velocity)),
          lateral_velocity_above_threshold(fmt::format(is_above_message, lateral_velocity)),
          lateral_velocity_below_threshold(fmt::format(is_below_message, lateral_velocity)),
          vertical_velocity_unexpected(fmt::format(is_unexpected_message, vertical_velocity)),
          vertical_velocity_above_threshold(fmt::format(is_above_message, vertical_velocity)),
          vertical_velocity_below_threshold(fmt::format(is_below_message, vertical_velocity)),
          longitudinal_acceleration_unexpected(fmt::format(is_unexpected_message, longitudinal_acceleration)),
          longitudinal_acceleration_above_threshold(fmt::format(is_above_message, longitudinal_acceleration)),
          longitudinal_acceleration_below_threshold(fmt::format(is_below_message, longitudinal_acceleration)),
          lateral_acceleration_unexpected(fmt::format(is_unexpected_message, lateral_acceleration)),
          lateral_acceleration_above_threshold(fmt::format(is_above_message, lateral_acceleration)),
          lateral_acceleration_below_threshold(fmt::format(is_below_message, lateral_acceleration))
    {
    }
};

class PlausibilityCheckOrientationDerivativesLogMessages : public PlausibilityCheckLogMessages
{
  private:
    const std::string pitch{"pitch"};
    const std::string roll{"roll"};
    const std::string yaw_rate{"yaw rate"};
    const std::string pitch_rate{"pitch rate"};
    const std::string roll_rate{"roll rate"};
    const std::string yaw_acceleration{"yaw acceleration"};
    const std::string pitch_acceleration{"pitch acceleration"};
    const std::string roll_acceleration{"roll acceleration"};

  public:
    const std::string pitch_above_threshold;
    const std::string pitch_below_threshold;
    const std::string roll_above_threshold;
    const std::string roll_below_threshold;

    const std::string yaw_rate_unexpected;
    const std::string yaw_rate_above_threshold;
    const std::string yaw_rate_below_threshold;
    const std::string pitch_rate_unexpected;
    const std::string pitch_rate_above_threshold;
    const std::string pitch_rate_below_threshold;
    const std::string roll_rate_unexpected;
    const std::string roll_rate_above_threshold;
    const std::string roll_rate_below_threshold;

    const std::string yaw_acceleration_unexpected;
    const std::string yaw_acceleration_above_threshold;
    const std::string yaw_acceleration_below_threshold;
    const std::string pitch_acceleration_unexpected;
    const std::string pitch_acceleration_above_threshold;
    const std::string pitch_acceleration_below_threshold;
    const std::string roll_acceleration_unexpected;
    const std::string roll_acceleration_above_threshold;
    const std::string roll_acceleration_below_threshold;

    PlausibilityCheckOrientationDerivativesLogMessages()
        : pitch_above_threshold(fmt::format(is_above_message, pitch)),
          pitch_below_threshold(fmt::format(is_below_message, pitch)),
          roll_above_threshold(fmt::format(is_above_message, roll)),
          roll_below_threshold(fmt::format(is_below_message, roll)),
          yaw_rate_unexpected(fmt::format(is_unexpected_message, yaw_rate)),
          yaw_rate_above_threshold(fmt::format(is_above_message, yaw_rate)),
          yaw_rate_below_threshold(fmt::format(is_below_message, yaw_rate)),
          pitch_rate_unexpected(fmt::format(is_unexpected_message, pitch_rate)),
          pitch_rate_above_threshold(fmt::format(is_above_message, pitch_rate)),
          pitch_rate_below_threshold(fmt::format(is_below_message, pitch_rate)),
          roll_rate_unexpected(fmt::format(is_unexpected_message, roll_rate)),
          roll_rate_above_threshold(fmt::format(is_above_message, roll_rate)),
          roll_rate_below_threshold(fmt::format(is_below_message, roll_rate)),
          yaw_acceleration_unexpected(fmt::format(is_unexpected_message, yaw_acceleration)),
          yaw_acceleration_above_threshold(fmt::format(is_above_message, yaw_acceleration)),
          yaw_acceleration_below_threshold(fmt::format(is_below_message, yaw_acceleration)),
          pitch_acceleration_unexpected(fmt::format(is_unexpected_message, pitch_acceleration)),
          pitch_acceleration_above_threshold(fmt::format(is_above_message, pitch_acceleration)),
          pitch_acceleration_below_threshold(fmt::format(is_below_message, pitch_acceleration)),
          roll_acceleration_unexpected(fmt::format(is_unexpected_message, roll_acceleration)),
          roll_acceleration_above_threshold(fmt::format(is_above_message, roll_acceleration)),
          roll_acceleration_below_threshold(fmt::format(is_below_message, roll_acceleration))
    {
    }
};

}  // namespace astas::environment::plausibility_check

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_COMMON_PLAUSIBILITYCHECKLOGMESSAGES_H
