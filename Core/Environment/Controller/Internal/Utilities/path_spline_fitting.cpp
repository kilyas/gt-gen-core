/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/path_spline_fitting.h"

#include "Core/Environment/LaneFollowing/point_distance_list.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include <MantleAPI/Common/spline.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;

mantle_api::SplineSegment<units::length::meter_t> ComputeSpline(const mantle_api::Vec3<units::length::meter_t>& p0,
                                                                const mantle_api::Vec3<units::length::meter_t>& p1,
                                                                const mantle_api::Vec3<units::length::meter_t>& p2,
                                                                const mantle_api::Vec3<units::length::meter_t>& p3,
                                                                double alpha,
                                                                double tension)
{
    double t0 = 0.0;
    double t1 = t0 + glm::pow(service::glmwrapper::Distance(p0, p1), alpha);
    double t2 = t1 + glm::pow(service::glmwrapper::Distance(p1, p2), alpha);
    double t3 = t2 + glm::pow(service::glmwrapper::Distance(p2, p3), alpha);

    mantle_api::Vec3<units::length::meter_t> m1 =
        (1.0 - tension) * (t2 - t1) * ((p1 - p0) / (t1 - t0) - (p2 - p0) / (t2 - t0) + (p2 - p1) / (t2 - t1));
    mantle_api::Vec3<units::length::meter_t> m2 =
        (1.0 - tension) * (t2 - t1) * ((p2 - p1) / (t2 - t1) - (p3 - p1) / (t3 - t1) + (p3 - p2) / (t3 - t2));

    mantle_api::SplineSegment<units::length::meter_t> segment;
    segment.a = 2.0 * (p1 - p2) + m1 + m2;
    segment.b = -3.0F * (p1 - p2) - m1 - m1 - m2;
    segment.c = m1;
    segment.d = p1;

    return segment;
}

void AddArtificialFirstPoint(lanefollowing::LaneFollowData& list)
{
    auto second_to_first = list.at(0).point - list.at(1).point;
    auto new_first = list.at(0).point + second_to_first;
    list.insert(std::begin(list), {new_first, 0.0_m, 0, {}});
}

void AddArtificialLastPoint(lanefollowing::LaneFollowData& list)
{
    auto previous_to_last = list.back().point - list.rbegin()[1].point;
    auto new_last = list.back().point + previous_to_last;
    list.insert(std::end(list), {new_last, 0.0_m, 0, {}});
}

lanefollowing::PointDistanceListEntry SampleSpline(mantle_api::SplineSegment<units::length::meter_t> segment,
                                                   double t,
                                                   mantle_api::UniqueId lane_id)
{
    auto point = segment.a * t * t * t + segment.b * t * t + segment.c * t + segment.d;
    auto heading = 3 * segment.a * t * t + 2 * segment.b * t + segment.c;
    heading = service::glmwrapper::Normalize(heading);

    return {point, 0.0_m, lane_id, heading};
}

void UpdateDistanceToLastPoint(lanefollowing::LaneFollowData& sampled_path_spline)
{
    if (sampled_path_spline.size() > 1)
    {
        auto& second_last_entry = sampled_path_spline.at(sampled_path_spline.size() - 2);
        const auto& last_entry = sampled_path_spline.at(sampled_path_spline.size() - 1);
        const units::length::meter_t distance{service::glmwrapper::Distance(second_last_entry.point, last_entry.point)};
        second_last_entry.distance_to_next_point = distance;
    }
}

lanefollowing::LaneFollowData SamplePathAsSpline(lanefollowing::PointDistanceList path_control_points)
{
    lanefollowing::LaneFollowData sampled_path_spline{};
    if (path_control_points.size() == 1)
    {
        sampled_path_spline.push_back(path_control_points[0]);
    }
    else if (path_control_points.size() > 1)
    {
        // Without these points, the spline is missing the first and last section.
        // For computing the spline 4 control points are needed, to be able to compute the "middle" section.
        AddArtificialFirstPoint(path_control_points);
        AddArtificialLastPoint(path_control_points);

        std::size_t count = 0;
        mantle_api::SplineSegment<units::length::meter_t> segment{};
        while (count <= path_control_points.size() - 4)
        {
            segment = ComputeSpline(path_control_points.at(count).point,
                                    path_control_points.at(count + 1).point,
                                    path_control_points.at(count + 2).point,
                                    path_control_points.at(count + 3).point);
            double t = 0.0;
            double t_increment = 0.05;
            while (t < 1.0)
            {
                mantle_api::UniqueId lane_id{path_control_points.at(count + 1).lane_id};
                sampled_path_spline.push_back(SampleSpline(segment, t, lane_id));
                UpdateDistanceToLastPoint(sampled_path_spline);

                t += t_increment;
            }
            count++;
        }

        mantle_api::UniqueId lane_id{path_control_points.at(count + 1).lane_id};
        sampled_path_spline.push_back(SampleSpline(segment, 1.0, lane_id));
        UpdateDistanceToLastPoint(sampled_path_spline);
    }
    return sampled_path_spline;
}

}  // namespace astas::environment::controller
