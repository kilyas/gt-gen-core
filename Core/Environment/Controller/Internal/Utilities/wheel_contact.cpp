/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/wheel_contact.h"

#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>

namespace astas::environment::controller
{

void WheelContact::Init(mantle_api::IEntity* entity)
{
    vehicle_entity_ = dynamic_cast<entities::VehicleEntity*>(entity);
    ComputeLocalSpaceWheelsContactPoints();
}

void WheelContact::ComputeLocalSpaceWheelsContactPoints()
{
    mantle_api::VehicleProperties* vehicle_properties = vehicle_entity_->GetProperties();

    const auto half_height = vehicle_properties->bounding_box.dimension.height * 0.5;
    const auto half_width = vehicle_properties->bounding_box.dimension.width * 0.5;

    // TODO: shift by distance (Vec3<units::length::meter_t>) from center of bounding box to origin of entity coordinate
    // system
    wheels_contact_points_.front_left = {
        vehicle_properties->front_axle.bb_center_to_axle_center.x, half_width, -half_height};
    wheels_contact_points_.front_right = {
        vehicle_properties->front_axle.bb_center_to_axle_center.x, -half_width, -half_height};
    wheels_contact_points_.rear_left = {
        vehicle_properties->rear_axle.bb_center_to_axle_center.x, half_width, -half_height};
    wheels_contact_points_.rear_right = {
        vehicle_properties->rear_axle.bb_center_to_axle_center.x, -half_width, -half_height};
}

WheelContactPoints WheelContact::GetWheelsWorldSpaceContactPoints() const
{
    auto get_world_space_point = [this](const mantle_api::Vec3<units::length::meter_t>& wheel_point) {
        return service::glmwrapper::ToGlobalSpace(
            vehicle_entity_->GetPosition(), vehicle_entity_->GetOrientation(), wheel_point);
    };

    WheelContactPoints world_space_contact_points{};
    world_space_contact_points.front_left = get_world_space_point(wheels_contact_points_.front_left);
    world_space_contact_points.front_right = get_world_space_point(wheels_contact_points_.front_right);
    world_space_contact_points.rear_left = get_world_space_point(wheels_contact_points_.rear_left);
    world_space_contact_points.rear_right = get_world_space_point(wheels_contact_points_.rear_right);

    return world_space_contact_points;
}

}  // namespace astas::environment::controller
