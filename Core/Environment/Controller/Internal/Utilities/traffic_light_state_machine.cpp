/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/traffic_light_state_machine.h"

namespace astas::environment::controller
{

TrafficLightStateMachine::TrafficLightStateMachine(std::vector<mantle_api::TrafficLightPhase> light_phases, bool repeat)
    : light_phases_(std::move(light_phases)), repeat_(repeat)
{
}

TrafficLightStateMachine::TrafficLightStateMachine(const TrafficLightStateMachine& state_machine)
{
    this->light_phases_ = state_machine.light_phases_;
    this->repeat_ = state_machine.repeat_;
}

void TrafficLightStateMachine::Start(entities::TrafficLightEntity* traffic_light_entity)
{
    traffic_light_entity_ = traffic_light_entity;

    ActivateLightPhaseState();
}

void TrafficLightStateMachine::Step(mantle_api::Time simulation_time)
{
    const auto local_state_machine_time = simulation_time - (light_phases_.back().end_time * repeat_count_);
    if (active_phase_state_->HasPhaseFinished(local_state_machine_time))
    {
        Transition();
    }
}

void TrafficLightStateMachine::Transition()
{
    if (active_phase_index_ == light_phases_.size() - 1)
    {
        if (repeat_)
        {
            active_phase_index_ = 0;
            ++repeat_count_;
        }
    }
    else
    {
        ++active_phase_index_;
    }

    ActivateLightPhaseState();
}

void TrafficLightStateMachine::ActivateLightPhaseState()
{
    active_phase_state_ =
        std::make_unique<TrafficLightPhaseState>(light_phases_.at(active_phase_index_), traffic_light_entity_);
    active_phase_state_->Start();
}

}  // namespace astas::environment::controller
