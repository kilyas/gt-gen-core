/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_SPLINEUTILITIES_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_SPLINEUTILITIES_H

#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/time_utils.h>

namespace astas::environment::controller
{

template <typename T, class = typename std::enable_if_t<units::traits::is_unit<T>::value>>
inline std::size_t GetSplineIndexForTime(const mantle_api::Time& time,
                                         const std::vector<mantle_api::SplineSection<T>>& splines)
{
    for (std::size_t i = 0; i < splines.size(); i++)
    {
        if (time >= splines[i].start_time && time < splines[i].end_time)
        {
            return i;
        }
    }

    // the splines are always valid and contain no "time-gaps" between one section and another.
    // Thus, if we haven't found a spline section above, it can only be the very last time-step and
    // therefore we return the last spline section
    return splines.size() - 1;
}

/// f(x) = ax³ + bx² + cx + d
/// f(x) = x * (a * x * x + b * x + c) + d;
/// f(x) = x * (x * (a * x + b) + c) + d;
template <typename T>
inline double GetYOfCubicFunction(
    double x,
    const std::tuple<units::unit_t<units::compound_unit<T, units::inverse<units::cubed<units::time::second>>>>,
                     units::unit_t<units::compound_unit<T, units::inverse<units::squared<units::time::second>>>>,
                     units::unit_t<units::compound_unit<T, units::inverse<units::time::second>>>,
                     units::unit_t<T>>& coefficients)
{
    double a = std::get<0>(coefficients).value();
    double b = std::get<1>(coefficients).value();
    double c = std::get<2>(coefficients).value();
    double d = std::get<3>(coefficients).value();
    return x * (x * (a * x + b) + c) + d;
}

/// f(x) = ax³ + bx² + cx + d
/// f'(x) = 3ax² + 2bx + c
/// f'(x) = (3ax + 2b)x + c
template <typename T>
inline double GetYOfFirstDerivativeOfCubicFunction(
    double x,
    const std::tuple<units::unit_t<units::compound_unit<T, units::inverse<units::cubed<units::time::second>>>>,
                     units::unit_t<units::compound_unit<T, units::inverse<units::squared<units::time::second>>>>,
                     units::unit_t<units::compound_unit<T, units::inverse<units::time::second>>>,
                     units::unit_t<T>>& coefficients)
{
    double a = std::get<0>(coefficients).value();
    double b = std::get<1>(coefficients).value();
    double c = std::get<2>(coefficients).value();
    return (3 * a * x + 2 * b) * x + c;
}

/// f(x) = ax³ + bx² + cx + d
/// f'(x) = 3ax² + 2bx + c
/// f''(x) = 6ax + b
template <typename T>
inline double GetYOfSecondDerivativeOfCubicFunction(
    double x,
    const std::tuple<units::unit_t<units::compound_unit<T, units::inverse<units::cubed<units::time::second>>>>,
                     units::unit_t<units::compound_unit<T, units::inverse<units::squared<units::time::second>>>>,
                     units::unit_t<units::compound_unit<T, units::inverse<units::time::second>>>,
                     units::unit_t<T>>& coefficients)

{
    double a = std::get<0>(coefficients).value();
    double b = std::get<1>(coefficients).value();
    return (6 * a * x) + b;
}

template <typename T, class = typename std::enable_if_t<units::traits::is_unit<T>::value>>
inline double EvaluateSpline(mantle_api::Time time, const mantle_api::SplineSection<T>& spline)
{
    ASSERT(time >= spline.start_time && "Simulation time must be higher or equal than the spline start time")
    ASSERT(time <= spline.end_time && "Simulation time must be less or equal than the spline end time")

    double x = mantle_api::TimeToSeconds(time - spline.start_time);
    return GetYOfCubicFunction(x, spline.polynomial);
}

template <typename T, class = typename std::enable_if_t<units::traits::is_unit<T>::value>>
inline double EvaluateSplineDerivative(mantle_api::Time time, const mantle_api::SplineSection<T>& spline)
{
    ASSERT(time >= spline.start_time && "Simulation time must be higher or equal than the spline start time")
    ASSERT(time <= spline.end_time && "Simulation time must be less or equal than the spline end time")

    double x = mantle_api::TimeToSeconds(time - spline.start_time);
    return GetYOfFirstDerivativeOfCubicFunction(x, spline.polynomial);
}

template <typename T, class = typename std::enable_if_t<units::traits::is_unit<T>::value>>
inline double EvaluateSplineSecondDerivative(mantle_api::Time time, const mantle_api::SplineSection<T>& spline)
{
    ASSERT(time >= spline.start_time && "Simulation time must be higher or equal than the spline start time")
    ASSERT(time <= spline.end_time && "Simulation time must be less or equal than the spline end time")

    double x = mantle_api::TimeToSeconds(time - spline.start_time);
    return GetYOfSecondDerivativeOfCubicFunction(x, spline.polynomial);
}

template <typename T, class = typename std::enable_if_t<units::traits::is_unit<T>::value>>
inline bool IsSplineEmptyOrTimeOutOfSplineRange(const std::vector<mantle_api::SplineSection<T>>& splines,
                                                mantle_api::Time time)
{
    return splines.empty() || time < splines.front().start_time || time > splines.back().end_time;
}

}  // namespace astas::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_SPLINEUTILITIES_H
