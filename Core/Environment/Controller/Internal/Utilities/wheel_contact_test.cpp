/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/wheel_contact.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <glm/glm.hpp>
#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_deg;

class WheelContactSUT : public WheelContact
{
  public:
    WheelContactPoints GetWheelContactPoints() { return wheels_contact_points_; };
};

TEST(WheelContactTest, GivenVehicle_WhenSettingProperties_ThenLocalContactPointsAreComputed)
{
    mantle_api::Vec3<units::length::meter_t> expected_front_left_wheel_contact_point{1.5_m, 1.0_m, -1.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_front_right_wheel_contact_point{1.5_m, -1.0_m, -1.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_rear_left_wheel_contact_point{-1.5_m, 1.0_m, -1.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_rear_right_wheel_contact_point{-1.5_m, -1.0_m, -1.0_m};

    auto properties = std::make_unique<mantle_api::VehicleProperties>();
    properties->front_axle.bb_center_to_axle_center = {1.5_m, 0_m, 0_m};
    properties->rear_axle.bb_center_to_axle_center = {-1.5_m, 0_m, 0_m};
    properties->bounding_box.dimension.width = 2_m;
    properties->bounding_box.dimension.length = 4_m;
    properties->bounding_box.dimension.height = 2_m;

    entities::VehicleEntity vehicle(0, "name");
    vehicle.SetProperties(std::move(properties));

    WheelContactSUT wheel_contact;
    wheel_contact.Init(&vehicle);
    auto wheels_contact_points = wheel_contact.GetWheelContactPoints();

    EXPECT_TRIPLE(wheels_contact_points.front_left, expected_front_left_wheel_contact_point)
    EXPECT_TRIPLE(wheels_contact_points.front_right, expected_front_right_wheel_contact_point)
    EXPECT_TRIPLE(wheels_contact_points.rear_left, expected_rear_left_wheel_contact_point)
    EXPECT_TRIPLE(wheels_contact_points.rear_right, expected_rear_right_wheel_contact_point)
}

TEST(WheelContactTest, GivenVehicleWithPosition_WhenSettingProperties_ThenGlobalContactPointsAreComputed)
{
    mantle_api::Vec3<units::length::meter_t> expected_front_left_wheel_contact_point{2.5_m, 2.0_m, 2.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_front_right_wheel_contact_point{2.5_m, 2.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_rear_left_wheel_contact_point{-0.5_m, 2.0_m, 2.0_m};
    mantle_api::Vec3<units::length::meter_t> expected_rear_right_wheel_contact_point{-0.5_m, 2.0_m, 0.0_m};

    auto properties = std::make_unique<mantle_api::VehicleProperties>();
    properties->front_axle.bb_center_to_axle_center = {1.5_m, 0_m, 0_m};
    properties->rear_axle.bb_center_to_axle_center = {-1.5_m, 0_m, 0_m};
    properties->bounding_box.dimension.width = 2_m;
    properties->bounding_box.dimension.length = 4_m;
    properties->bounding_box.dimension.height = 2_m;

    entities::VehicleEntity vehicle(0, "name");
    vehicle.SetProperties(std::move(properties));
    vehicle.SetPosition({1_m, 1_m, 1_m});
    vehicle.SetOrientation({0_rad, 0_rad, 90.0_deg});

    WheelContact wheel_contact;
    wheel_contact.Init(&vehicle);
    auto world_space_wheels_contact_points = wheel_contact.GetWheelsWorldSpaceContactPoints();

    EXPECT_TRIPLE(world_space_wheels_contact_points.front_left, expected_front_left_wheel_contact_point)
    EXPECT_TRIPLE(world_space_wheels_contact_points.front_right, expected_front_right_wheel_contact_point)
    EXPECT_TRIPLE(world_space_wheels_contact_points.rear_left, expected_rear_left_wheel_contact_point)
    EXPECT_TRIPLE(world_space_wheels_contact_points.rear_right, expected_rear_right_wheel_contact_point)
}

}  // namespace astas::environment::controller
