/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PATHSPLINEFITTING_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PATHSPLINEFITTING_H

#include "Core/Environment/LaneFollowing/point_distance_list.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include <MantleAPI/Common/spline.h>

namespace astas::environment::controller
{
mantle_api::SplineSegment<units::length::meter_t> ComputeSpline(const mantle_api::Vec3<units::length::meter_t>& p0,
                                                                const mantle_api::Vec3<units::length::meter_t>& p1,
                                                                const mantle_api::Vec3<units::length::meter_t>& p2,
                                                                const mantle_api::Vec3<units::length::meter_t>& p3,
                                                                double alpha = 0.5,
                                                                double tension = 0.0);

void AddArtificialFirstPoint(lanefollowing::LaneFollowData& list);

void AddArtificialLastPoint(lanefollowing::LaneFollowData& list);

lanefollowing::PointDistanceListEntry SampleSpline(mantle_api::SplineSegment<units::length::meter_t> segment,
                                                   double t,
                                                   mantle_api::UniqueId lane_id);

void UpdateDistanceToLastPoint(lanefollowing::LaneFollowData& sampled_path_spline);

lanefollowing::LaneFollowData SamplePathAsSpline(lanefollowing::PointDistanceList path_control_points);
}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PATHSPLINEFITTING_H
