/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/controller_data_exchange_container.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_mps_sq;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;

TEST(ControllerDataExchangeContainerTest, GivenFilledDataExchangeContainer_WhenClear_ThenContainerIsEmpty)
{
    ControllerDataExchangeContainer data_exchange_container{};
    data_exchange_container.velocity_scalars.push_back(10_mps);
    data_exchange_container.acceleration_scalar = 11_mps_sq;
    data_exchange_container.orientation_rate_scalar = 13_rad_per_s;
    data_exchange_container.orientation_acceleration_scalar = 14_rad_per_s_sq;
    data_exchange_container.lane_offset_scalars.push_back(12_m);

    data_exchange_container.Reset();

    EXPECT_TRUE(data_exchange_container.velocity_scalars.empty());
    EXPECT_EQ(data_exchange_container.acceleration_scalar, 0_mps_sq);
    EXPECT_EQ(data_exchange_container.orientation_rate_scalar, 0_rad_per_s);
    EXPECT_EQ(data_exchange_container.orientation_acceleration_scalar, 0.0_rad_per_s_sq);
    EXPECT_TRUE(data_exchange_container.lane_offset_scalars.empty());
}

}  // namespace astas::environment::controller
