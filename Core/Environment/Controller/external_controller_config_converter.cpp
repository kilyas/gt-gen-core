/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/external_controller_config_converter.h"

namespace astas::environment::controller
{

TrafficParticipantControlUnitConfig* ExternalControllerConfigConverter::ConvertToTpmConfig(
    mantle_api::ExternalControllerConfig* external_config) const
{
    auto tpm_config = std::make_unique<TrafficParticipantControlUnitConfig>();
    tpm_config->parameters = external_config->parameters;
    tpm_config->map_chunking = data_.user_settings->map_chunking;
    tpm_config->plugins_paths = data_.user_settings->user_directories.plugins;
    tpm_config->astas_map = data_.astas_map;
    tpm_config->proto_ground_truth_builder_config = data_.proto_ground_truth_builder_config;
    tpm_config->name = external_config->name;

    return tpm_config.release();
}

AstasExternalControllerConfig* ExternalControllerConfigConverter::ConvertToExternalHostConfig(
    mantle_api::ExternalControllerConfig* external_config) const
{
    // Create new config
    auto external_host_config = std::make_unique<AstasExternalControllerConfig>();

    // Copy control strategies from original config
    for (const auto& control_strategy : external_config->control_strategies)
    {
        external_host_config->control_strategies.push_back(
            std::make_unique<mantle_api::ControlStrategy>(*control_strategy));
    }

    // Copy route definition from original config
    external_host_config->route_definition = external_config->route_definition;

    // TODO: Waypoints are actually already contained in the route_definition. Delete waypoints member
    // from AstasExternalControllerConfig and use route_definition instead
    for (auto route_waypoint : external_config->route_definition.waypoints)
    {
        external_host_config->waypoints.push_back(route_waypoint.waypoint);
    }

    external_host_config->host_interface = data_.host_vehicle_interface;
    external_host_config->host_vehicle_model = data_.host_vehicle_model;
    external_host_config->astas_map = data_.astas_map;
    external_host_config->recovery_mode_enabled = data_.user_settings->host_vehicle.recovery_mode;
    external_host_config->traffic_command_builder = data_.traffic_command_builder;
    return external_host_config.release();
}

mantle_api::IControllerConfig* ExternalControllerConfigConverter::GetConfig(
    mantle_api::ExternalControllerConfig* external_config) const
{
    if (external_config->name == "Ego" || external_config->name == "Host" || external_config->name == "ExternalHost" ||
        external_config->name == "" || external_config->name.find("ExternalOverride") != std::string::npos)
    {
        return ConvertToExternalHostConfig(external_config);
    }
    else
    {
        return ConvertToTpmConfig(external_config);
    }
}

}  // namespace astas::environment::controller
