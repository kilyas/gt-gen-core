/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/control_unit_test_utils.h"
#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/i_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/keep_velocity_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_change_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_offset_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/recovery_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/vehicle_model_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"
#include "Core/Environment/Controller/Internal/composite_controller_sut.h"
#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <MantleAPI/Traffic/i_controller_config.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::controller
{

class MockController : public IControlUnit
{
  public:
    MOCK_METHOD(std::string, GetName, (), (const, override));
    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));

    MOCK_METHOD(std::unique_ptr<IControlUnit>, Clone, (), (const, override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
    MOCK_METHOD(void,
                SetControllerDataExchangeContainer,
                (ControllerDataExchangeContainer & data_exchange_container),
                (override));
    MOCK_METHOD(const mantle_api::ControlStrategyType&, GetOriginatingControlStrategy, (), (override));
};

class MockHostVehicleInterfaceControlUnit : public HostVehicleInterfaceControlUnit
{
  public:
    MockHostVehicleInterfaceControlUnit(const std::vector<mantle_api::Vec3<units::length::meter_t>>& waypoints,
                                        mantle_api::ILaneLocationQueryService* map_query_service)
        : HostVehicleInterfaceControlUnit(waypoints, map_query_service, nullptr)
    {
    }
    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));
    MOCK_METHOD(std::unique_ptr<IControlUnit>, Clone, (), (const, override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
    MOCK_METHOD(void,
                SetControllerDataExchangeContainer,
                (ControllerDataExchangeContainer & data_exchange_container),
                (override));
};

class MockVehicleModelControlUnit : public VehicleModelControlUnit
{
  public:
    explicit MockVehicleModelControlUnit(host::HostVehicleModel* vehicle_model)
        : VehicleModelControlUnit(vehicle_model, nullptr)
    {
    }
    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));
    MOCK_METHOD(std::unique_ptr<IControlUnit>, Clone, (), (const, override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
    MOCK_METHOD(void,
                SetControllerDataExchangeContainer,
                (ControllerDataExchangeContainer & data_exchange_container),
                (override));
};

class MockRecoveryControlUnit : public RecoveryControlUnit
{
  public:
    MockRecoveryControlUnit() : RecoveryControlUnit(nullptr, nullptr) {}
    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));
    MOCK_METHOD(std::unique_ptr<IControlUnit>, Clone, (), (const, override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
    MOCK_METHOD(void,
                SetControllerDataExchangeContainer,
                (ControllerDataExchangeContainer & data_exchange_container),
                (override));
};

class MockPathControlUnit : public PathControlUnit
{
  public:
    MockPathControlUnit() : PathControlUnit(nullptr) {}

    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));
    MOCK_METHOD(std::unique_ptr<IControlUnit>, Clone, (), (const, override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
    MOCK_METHOD(bool, HasReachedEndOfRoadNetwork, (), (const, override));
    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
    MOCK_METHOD(void,
                SetControllerDataExchangeContainer,
                (ControllerDataExchangeContainer & data_exchange_container),
                (override));
};

class MockLaneChangeControlUnit : public LaneChangeControlUnit
{
  public:
    MockLaneChangeControlUnit()
        : LaneChangeControlUnit(mantle_api::PerformLaneChangeControlStrategy{}, nullptr, nullptr){};
    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));
    MOCK_METHOD(std::unique_ptr<IControlUnit>, Clone, (), (const, override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
    MOCK_METHOD(void,
                SetControllerDataExchangeContainer,
                (ControllerDataExchangeContainer & data_exchange_container),
                (override));
};

class MockLaneAssignmentControlUnit : public LaneAssignmentControlUnit
{
  public:
    MockLaneAssignmentControlUnit() : LaneAssignmentControlUnit(nullptr, true){};

    MOCK_METHOD(void, Step, (mantle_api::Time current_simulation_time), (override));
    MOCK_METHOD(std::unique_ptr<IControlUnit>, Clone, (), (const, override));
    MOCK_METHOD(bool, HasFinished, (), (const, override));
    MOCK_METHOD(void, SetEntity, (mantle_api::IEntity & entity), (override));
};

class CompositeControllerTest : public testing::Test
{
  public:
    void WithEmptyCompositeController()
    {
        mantle_api::UniqueId id = 42;
        composite_controller_ = std::make_unique<CompositeControllerSUT>(id, nullptr);
    }

    void WithActivatedCompositeController()
    {
        composite_controller_->ChangeState(mantle_api::IController::LateralState::kActivate,
                                           mantle_api::IController::LongitudinalState::kActivate);
    }

    MockController* WithMockController()
    {
        auto mock_controller = std::make_unique<MockController>();
        MockController* mock_controller_ptr = mock_controller.get();
        composite_controller_->AddControlUnit(std::move(mock_controller));

        return mock_controller_ptr;
    }

    MockPathControlUnit* WithMockPathControlUnit()
    {
        auto path_control_unit = std::make_unique<MockPathControlUnit>();
        MockPathControlUnit* path_control_unit_ptr = path_control_unit.get();
        composite_controller_->AddControlUnit(std::move(path_control_unit));

        return path_control_unit_ptr;
    }

    MockLaneChangeControlUnit* WithMockLaneChangeControlUnit()
    {
        auto lane_change_control_unit = std::make_unique<MockLaneChangeControlUnit>();
        MockLaneChangeControlUnit* lane_change_control_unit_ptr = lane_change_control_unit.get();
        composite_controller_->AddControlUnit(std::move(lane_change_control_unit));

        return lane_change_control_unit_ptr;
    }

    MockHostVehicleInterfaceControlUnit* WithMockHostVehicleInterfaceControlUnit()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints;
        auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
        map::LaneLocationProvider lane_location_provider{*map};
        auto mock_host_vehicle_interface_control_unit =
            std::make_unique<MockHostVehicleInterfaceControlUnit>(waypoints, &lane_location_provider);
        MockHostVehicleInterfaceControlUnit* mock_host_vehicle_interface_control_unit_ptr =
            mock_host_vehicle_interface_control_unit.get();
        composite_controller_->AddControlUnit(std::move(mock_host_vehicle_interface_control_unit));

        return mock_host_vehicle_interface_control_unit_ptr;
    }

    MockVehicleModelControlUnit* WithMockVehicleModelControlUnit()
    {
        host::HostVehicleModel vehicle_model;
        auto mock_vehicle_model_control_unit = std::make_unique<MockVehicleModelControlUnit>(&vehicle_model);
        MockVehicleModelControlUnit* mock_vehicle_model_control_unit_ptr = mock_vehicle_model_control_unit.get();
        composite_controller_->AddControlUnit(std::move(mock_vehicle_model_control_unit));

        return mock_vehicle_model_control_unit_ptr;
    }

    MockRecoveryControlUnit* WithMockRecoveryControlUnit()
    {
        auto mock_recovery_control_unit = std::make_unique<MockRecoveryControlUnit>();
        MockRecoveryControlUnit* mock_recovery_control_unit_ptr = mock_recovery_control_unit.get();
        composite_controller_->AddControlUnit(std::move(mock_recovery_control_unit));

        return mock_recovery_control_unit_ptr;
    }

    KeepVelocityControlUnit* WithKeepVelocityControlUnit()
    {
        auto keep_velocity_control_unit = std::make_unique<KeepVelocityControlUnit>();
        KeepVelocityControlUnit* keep_velocity_control_unit_ptr = keep_velocity_control_unit.get();
        composite_controller_->AddControlUnit(std::move(keep_velocity_control_unit));

        return keep_velocity_control_unit_ptr;
    }

    VelocityControlUnit* WithVelocityControlUnit()
    {
        std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> splines;
        auto velocity_control_unit = std::make_unique<VelocityControlUnit>(splines);
        VelocityControlUnit* velocity_control_unit_ptr = velocity_control_unit.get();
        composite_controller_->AddControlUnit(std::move(velocity_control_unit));

        return velocity_control_unit_ptr;
    }

    LaneOffsetControlUnit* WithLaneOffsetControlUnit()
    {
        auto lane_offset_control_unit = std::make_unique<LaneOffsetControlUnit>(nullptr);
        LaneOffsetControlUnit* lane_offset_control_unit_ptr = lane_offset_control_unit.get();
        composite_controller_->AddControlUnit(std::move(lane_offset_control_unit));

        return lane_offset_control_unit_ptr;
    }

    MockLaneAssignmentControlUnit* WithMockLaneAssignmentControlUnit()
    {
        auto mock_lane_assignment_control_unit = std::make_unique<MockLaneAssignmentControlUnit>();
        auto* mock_lane_assignment_control_unit_ptr = mock_lane_assignment_control_unit.get();
        composite_controller_->SetLaneAssignmentControlUnit(std::move(mock_lane_assignment_control_unit));

        return mock_lane_assignment_control_unit_ptr;
    }

  protected:
    std::unique_ptr<CompositeControllerSUT> composite_controller_;
};

TEST_F(CompositeControllerTest, GivenNoControllers_WhenConstructed_ThenIdCanBeAccessed)
{
    WithEmptyCompositeController();

    EXPECT_EQ(42, composite_controller_->GetUniqueId());
}

TEST_F(CompositeControllerTest, GivenNoControllers_WhenHasFinshedIsCalled_ThenReturnsAlwaysTrue)
{
    WithEmptyCompositeController();

    EXPECT_TRUE(composite_controller_->HasFinished());
}

TEST_F(CompositeControllerTest, GivenNoControllers_WhenClone_ThenCopyCreated)
{
    WithEmptyCompositeController();
    composite_controller_->SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute);
    MockController* mock_controller_ptr = WithMockController();

    ON_CALL(*mock_controller_ptr, Clone()).WillByDefault([]() -> std::unique_ptr<MockController> {
        return std::make_unique<MockController>();
    });
    EXPECT_CALL(*mock_controller_ptr, Clone()).Times(1);

    auto copy = composite_controller_->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(composite_controller_.get(), copy.get());
    EXPECT_EQ(42, copy->GetUniqueId());
    EXPECT_EQ(mantle_api::DefaultRoutingBehavior::kRandomRoute, copy->GetDefaultRoutingBehavior());
}

TEST_F(CompositeControllerTest,
       GivenCompositeController_WhenAssigningController_ThenControllerDataExchangeContainerIsAssignedToController)
{
    WithEmptyCompositeController();
    auto mock_controller = std::make_unique<MockController>();
    MockController* mock_controller_ptr = mock_controller.get();

    EXPECT_CALL(*mock_controller_ptr, SetControllerDataExchangeContainer(::testing::_)).Times(1);

    composite_controller_->AddControlUnit(std::move(mock_controller));
}

TEST_F(CompositeControllerTest, GivenSuccessController_WhenStepping_ThenHasFinishedAndLaneAssignmentNotCalled)
{
    WithEmptyCompositeController();
    MockLaneAssignmentControlUnit* lane_assignment_controller_ptr = WithMockLaneAssignmentControlUnit();
    MockPathControlUnit* mock_controller1_ptr = WithMockPathControlUnit();
    MockController* mock_controller2_ptr = WithMockController();

    ON_CALL(*mock_controller1_ptr, HasFinished()).WillByDefault(::testing::Return(true));
    ON_CALL(*mock_controller2_ptr, HasFinished()).WillByDefault(::testing::Return(true));
    EXPECT_CALL(*lane_assignment_controller_ptr, Step(::testing::_)).Times(0);

    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    composite_controller_->SetEntity(vehicle);
    composite_controller_->Step(mantle_api::Time{0});

    EXPECT_TRUE(composite_controller_->HasFinished());
}

TEST_F(CompositeControllerTest, GivenSuccessAndFailureController_WhenStepping_ThenHasNotFinishedAndLaneAssignmentCalled)
{
    WithEmptyCompositeController();
    MockLaneAssignmentControlUnit* lane_assignment_controller_ptr = WithMockLaneAssignmentControlUnit();
    MockPathControlUnit* mock_controller1_ptr = WithMockPathControlUnit();
    MockController* mock_controller2_ptr = WithMockController();

    ON_CALL(*mock_controller1_ptr, HasFinished()).WillByDefault(::testing::Return(false));
    ON_CALL(*mock_controller2_ptr, HasFinished()).WillByDefault(::testing::Return(true));
    EXPECT_CALL(*lane_assignment_controller_ptr, Step(::testing::_)).Times(1);

    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    composite_controller_->SetEntity(vehicle);
    composite_controller_->Step(mantle_api::Time{0});

    EXPECT_FALSE(composite_controller_->HasFinished());
}

TEST_F(CompositeControllerTest,
       GivenDefaultRoutingBehaviourRandom_WhenPathControlUnitFinished_ThenPathControlUnitReplaced)
{
    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};
    composite_controller_ = std::make_unique<CompositeControllerSUT>(42, &lane_location_provider);
    composite_controller_->SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute);
    MockPathControlUnit* mock_controller1_ptr = WithMockPathControlUnit();

    EXPECT_CALL(*mock_controller1_ptr, HasFinished())
        .WillOnce(::testing::Return(false))
        .WillRepeatedly(::testing::Return(true));

    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    vehicle.SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    composite_controller_->SetEntity(vehicle);
    composite_controller_->Step(mantle_api::Time{0});

    auto& control_units = composite_controller_->GetControlUnits();
    ASSERT_EQ(1, composite_controller_->GetControlUnitCount());
    EXPECT_NE(nullptr, dynamic_cast<MockPathControlUnit*>(control_units[0].get()));

    composite_controller_->Step(mantle_api::Time{10});

    ASSERT_EQ(1, composite_controller_->GetControlUnitCount());
    EXPECT_EQ(nullptr, dynamic_cast<MockPathControlUnit*>(control_units[0].get()));
    EXPECT_NE(nullptr, dynamic_cast<PathControlUnit*>(control_units[0].get()));
}

TEST_F(
    CompositeControllerTest,
    GivenDefaultRoutingBehaviourRandom_WhenPathControlUnitFinishedAndReachedEndOfRoadNetwork_ThenPathControlUnitNotReplaced)
{
    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};
    composite_controller_ = std::make_unique<CompositeControllerSUT>(42, &lane_location_provider);
    composite_controller_->SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute);
    MockPathControlUnit* mock_controller1_ptr = WithMockPathControlUnit();

    ON_CALL(*mock_controller1_ptr, HasFinished()).WillByDefault(::testing::Return(true));
    ON_CALL(*mock_controller1_ptr, HasReachedEndOfRoadNetwork()).WillByDefault(::testing::Return(true));

    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    vehicle.SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    composite_controller_->SetEntity(vehicle);
    composite_controller_->Step(mantle_api::Time{0});

    auto& control_units = composite_controller_->GetControlUnits();
    ASSERT_EQ(1, composite_controller_->GetControlUnitCount());
    EXPECT_NE(nullptr, dynamic_cast<MockPathControlUnit*>(control_units[0].get()));
}

TEST_F(CompositeControllerTest, GivenMultipleController_WhenStepping_ThenRightSteppingOrder)
{
    WithEmptyCompositeController();
    WithMockLaneAssignmentControlUnit();
    MockPathControlUnit* mock_path_control_unit_ptr = WithMockPathControlUnit();
    MockController* mock_controller_ptr = WithMockController();
    testing::Sequence testing_sequence;

    EXPECT_CALL(*mock_controller_ptr, Step(testing::_)).InSequence(testing_sequence);
    EXPECT_CALL(*mock_path_control_unit_ptr, Step(testing::_)).InSequence(testing_sequence);

    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    composite_controller_->SetEntity(vehicle);
    composite_controller_->Step(mantle_api::Time{0});
}

TEST_F(CompositeControllerTest, GivenExternalController_WhenStepping_ThenRightSteppingOrder)
{
    WithEmptyCompositeController();
    WithMockLaneAssignmentControlUnit();
    MockRecoveryControlUnit* mock_recovery_control_unit_ptr = WithMockRecoveryControlUnit();
    MockVehicleModelControlUnit* mock_vehicle_model_control_unit_ptr = WithMockVehicleModelControlUnit();
    MockHostVehicleInterfaceControlUnit* mock_host_vehicle_interface_control_unit_ptr =
        WithMockHostVehicleInterfaceControlUnit();
    testing::Sequence testing_sequence;

    EXPECT_CALL(*mock_host_vehicle_interface_control_unit_ptr, Step(testing::_)).InSequence(testing_sequence);
    EXPECT_CALL(*mock_vehicle_model_control_unit_ptr, Step(testing::_)).InSequence(testing_sequence);
    EXPECT_CALL(*mock_recovery_control_unit_ptr, Step(testing::_)).InSequence(testing_sequence);

    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    composite_controller_->SetEntity(vehicle);
    composite_controller_->Step(mantle_api::Time{0});
}

TEST_F(CompositeControllerTest,
       GivenCompositeController_WhenAddingNonPositionUpdateControlUnit_ThenPathControlUnitIsAddedAutomatically)
{
    WithEmptyCompositeController();
    const auto& control_units = composite_controller_->GetControlUnits();

    EXPECT_EQ(0, control_units.size());
    WithMockController();
    ASSERT_EQ(2, control_units.size());
    EXPECT_TRUE(ContainsController<PathControlUnit>(composite_controller_->GetControlUnits()));
}

TEST_F(CompositeControllerTest, GivenCompositeController_WhenAddingMultipleVelocityControlUnits_ThenOnlyOneExists)
{
    WithEmptyCompositeController();
    const auto& control_units = composite_controller_->GetControlUnits();

    EXPECT_EQ(0, control_units.size());
    WithMockPathControlUnit();
    EXPECT_EQ(1, control_units.size());
    WithVelocityControlUnit();
    EXPECT_EQ(2, control_units.size());
    WithKeepVelocityControlUnit();
    EXPECT_EQ(2, control_units.size());
    WithVelocityControlUnit();
    EXPECT_EQ(2, control_units.size());
}

TEST_F(CompositeControllerTest, GivenCompositeController_WhenAddingMultipleLaneOffsetControlUnits_ThenOnlyOneExists)
{
    WithEmptyCompositeController();
    const auto& control_units = composite_controller_->GetControlUnits();

    EXPECT_EQ(0, control_units.size());
    WithMockPathControlUnit();
    EXPECT_EQ(1, control_units.size());
    WithLaneOffsetControlUnit();
    EXPECT_EQ(2, control_units.size());
    WithLaneOffsetControlUnit();
    EXPECT_EQ(2, control_units.size());
    WithLaneOffsetControlUnit();
    EXPECT_EQ(2, control_units.size());
}

TEST_F(CompositeControllerTest,
       GivenCompositeController_WhenAddingConflictingPositionUpdateControlUnits_ThenOnlyOneExists)
{
    WithEmptyCompositeController();
    const auto& control_units = composite_controller_->GetControlUnits();

    EXPECT_EQ(0, control_units.size());
    WithMockPathControlUnit();
    EXPECT_EQ(1, control_units.size());
    WithMockVehicleModelControlUnit();
    EXPECT_EQ(1, control_units.size());
}

TEST_F(CompositeControllerTest, GivenMultipleController_WhenSetEntity_ThenEntityIsSetToAllControllers)
{
    WithEmptyCompositeController();
    MockController* mock_controller1_ptr = WithMockController();
    MockController* mock_controller2_ptr = WithMockController();
    auto* mock_lane_assignment_control_unit_ptr = WithMockLaneAssignmentControlUnit();

    EXPECT_CALL(*mock_controller1_ptr, SetEntity(::testing::_)).Times(1);
    EXPECT_CALL(*mock_controller2_ptr, SetEntity(::testing::_)).Times(1);
    EXPECT_CALL(*mock_lane_assignment_control_unit_ptr, SetEntity(::testing::_)).Times(1);

    entities::VehicleEntity vehicle{42, "Hans-Peter"};

    composite_controller_->SetEntity(vehicle);
}

TEST_F(CompositeControllerTest, GivenCompositeControllerWithEntity_WhenAddControlUnit_ThenEntityIsSetInControlUnit)
{
    WithEmptyCompositeController();
    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    composite_controller_->SetEntity(vehicle);

    auto mock_controller = std::make_unique<MockController>();
    EXPECT_CALL(*mock_controller, SetEntity(::testing::_)).Times(1);

    composite_controller_->AddControlUnit(std::move(mock_controller));
}

TEST_F(CompositeControllerTest, GivenLaneChangeController_WhenStepping_ThenRightSteppingOrder)
{
    WithEmptyCompositeController();
    WithMockLaneAssignmentControlUnit();
    MockLaneChangeControlUnit* mock_lane_change_control_unit_ptr = WithMockLaneChangeControlUnit();
    MockController* mock_controller_ptr = WithMockController();
    testing::Sequence testing_sequence;

    EXPECT_CALL(*mock_controller_ptr, Step(testing::_)).InSequence(testing_sequence);
    EXPECT_CALL(*mock_lane_change_control_unit_ptr, Step(testing::_)).InSequence(testing_sequence);

    entities::VehicleEntity vehicle{42, "Hans-Peter"};
    composite_controller_->SetEntity(vehicle);
    composite_controller_->Step(mantle_api::Time{0});
}

TEST_F(CompositeControllerTest, GivenInactiveCompositeController_WhenLateralStateIsSetToActive_ThenControllerIsActive)
{
    WithEmptyCompositeController();
    composite_controller_->ChangeState(mantle_api::IController::LateralState::kActivate,
                                       mantle_api::IController::LongitudinalState::kNoChange);

    EXPECT_TRUE(composite_controller_->IsActive());
}

TEST_F(CompositeControllerTest,
       GivenInactiveCompositeController_WhenLongitudinalStateIsSetToActive_ThenControllerIsActive)
{
    WithEmptyCompositeController();
    composite_controller_->ChangeState(mantle_api::IController::LateralState::kNoChange,
                                       mantle_api::IController::LongitudinalState::kActivate);

    EXPECT_TRUE(composite_controller_->IsActive());
}

TEST_F(CompositeControllerTest,
       GivenInactiveCompositeController_WhenCallChangeStateWithNoChange_ThenControllerIsInactive)
{
    WithEmptyCompositeController();
    composite_controller_->ChangeState(mantle_api::IController::LateralState::kNoChange,
                                       mantle_api::IController::LongitudinalState::kNoChange);

    EXPECT_FALSE(composite_controller_->IsActive());
}

TEST_F(CompositeControllerTest, GivenActiveCompositeController_WhenCallChangeStateWithNoChange_ThenControllerIsActive)
{
    WithEmptyCompositeController();
    WithActivatedCompositeController();
    composite_controller_->ChangeState(mantle_api::IController::LateralState::kNoChange,
                                       mantle_api::IController::LongitudinalState::kNoChange);
    EXPECT_TRUE(composite_controller_->IsActive());
}

TEST_F(CompositeControllerTest,
       GivenActiveCompositeController_WhenCallChangeStateWithDeactivate_TheControllerIsInactive)
{
    WithEmptyCompositeController();
    WithActivatedCompositeController();
    composite_controller_->ChangeState(mantle_api::IController::LateralState::kDeactivate,
                                       mantle_api::IController::LongitudinalState::kDeactivate);
    EXPECT_FALSE(composite_controller_->IsActive());
}

}  // namespace astas::environment::controller
