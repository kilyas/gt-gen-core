/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/controller_factory.h"

#include "Core/Environment/Controller/Internal/ControlUnits/control_unit_test_utils.h"
#include "Core/Environment/Controller/Internal/composite_controller_sut.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "astas_osi_groundtruth.pb.h"

#include <gtest/gtest.h>

namespace astas::environment::controller
{

TEST(ControllerFactoryTest, GivenNoOpConfig_WhenCreate_ThenNoOpControllerIsCreated)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(1, controller->GetControlUnitCount());
    EXPECT_TRUE(ContainsController<controller::NoOpControlUnit>(controller->GetControlUnits()));
}

TEST(ControllerFactoryTest, GivenUnsupportedConfig_WhenCreate_ThenEnvironmentExceptionIsThrown)
{
    auto config = std::make_unique<mantle_api::IControllerConfig>();

    EXPECT_THROW(ControllerFactory::Create<CompositeControllerSUT>(0, std::move(config), true), EnvironmentException);
}

TEST(ControllerFactoryTest, GivenSupportedConfig_WhenCreate_ThenLaneAssignmentControlUnitIsAdded)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    ASSERT_EQ(42, controller->GetUniqueId());
    ASSERT_EQ(1, controller->GetControlUnitCount());
    ASSERT_TRUE(ContainsController<controller::NoOpControlUnit>(controller->GetControlUnits()));

    EXPECT_TRUE(controller->HasLaneAssignmentControlUnit());
}

TEST(ControllerFactoryTest, GivenSupportedConfig_WhenCreateWithoutRoutingBehavior_ThenDefaultRoutingBehaviorIsStop)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(mantle_api::DefaultRoutingBehavior::kStop, controller->GetDefaultRoutingBehavior());
}

TEST(ControllerFactoryTest, GivenSupportedConfig_WhenCreateWithRoutingBehavior_ThenRoutingBehaviorIsSet)
{
    auto config = std::make_unique<mantle_api::NoOpControllerConfig>();

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(
        42, std::move(config), true, mantle_api::DefaultRoutingBehavior::kRandomRoute);

    EXPECT_EQ(mantle_api::DefaultRoutingBehavior::kRandomRoute, controller->GetDefaultRoutingBehavior());
}

TEST(ControllerFactoryTest, GivenInternalConfigAndNotAllowedToLeaveLane_WhenCreate_ThenLaneAssignmentControllerThrows)
{
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::EmptyMap()};
    map::LaneLocationProvider lane_location_provider{*map};
    config->map_query_service = &lane_location_provider;

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), false);

    auto* lane_assignment_controller = controller->GetLaneAssignmentControlUnit();
    entities::VehicleEntity entity{1, "traffic"};
    lane_assignment_controller->SetEntity(entity);
    EXPECT_THROW(lane_assignment_controller->Step(mantle_api::Time{0}), EnvironmentException);
}

TEST(ControllerFactoryTest, GivenInternalConfigAndAllowedToLeaveLane_WhenCreate_ThenLaneAssignmentControllerNotThrows)
{
    auto config = std::make_unique<mantle_api::InternalControllerConfig>();
    std::unique_ptr<environment::map::AstasMap> map{test_utils::MapCatalogue::EmptyMap()};
    map::LaneLocationProvider lane_location_provider{*map};
    config->map_query_service = &lane_location_provider;

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    auto* lane_assignment_controller = controller->GetLaneAssignmentControlUnit();
    entities::VehicleEntity entity{1, "traffic"};
    lane_assignment_controller->SetEntity(entity);
    EXPECT_NO_THROW(lane_assignment_controller->Step(mantle_api::Time{0}));
}

TEST(ControllerFactoryTest, GivenExternalConfig_WhenCreate_ThenExternalCompositeControllerIsCreated)
{
    astas_osi3::GroundTruth ground_truth;
    std::map<std::string, std::string> tpm_params{{"velocity", "22"}};

    auto config = std::make_unique<TrafficParticipantControlUnitConfig>();
    config->name = "traffic_participant_model_example";
    config->parameters = tpm_params;
    auto astas_map{test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    config->astas_map = astas_map.get();
    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    EXPECT_EQ(controller->GetType(), CompositeController::Type::kExternal);
    EXPECT_TRUE(controller->HasLaneAssignmentControlUnit());
}

TEST(ControllerFactoryTest, GivenExternalConfigAndAllowedToLeaveLane_WhenCreate_ThenLaneAssignmentControllerThrows)
{
    using units::literals::operator""_m;

    auto map{test_utils::MapCatalogue::MapWithThreeConnectedEastingLanesWithHundredPointsEach()};
    map::LaneLocationProvider lane_location_provider{*map};
    host::HostVehicleModel vehicle_model{};

    auto config = std::make_unique<AstasExternalControllerConfig>();
    config->recovery_mode_enabled = false;
    config->map_query_service = &lane_location_provider;
    config->host_vehicle_model = &vehicle_model;

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    auto* lane_assignment_controller = controller->GetLaneAssignmentControlUnit();
    entities::VehicleEntity entity{1, "traffic"};
    entity.SetPosition(mantle_api::Vec3<units::length::meter_t>{10.0_m, -15.25_m, 0.0_m});
    lane_assignment_controller->SetEntity(entity);

    EXPECT_THROW(lane_assignment_controller->Step(mantle_api::Time{0}), EnvironmentException);
}

TEST(ControllerFactoryTest, GivenTpmConfigAndAllowedToLeaveLane_WhenCreate_ThenLaneAssignmentControllerNotThrows)
{
    using units::literals::operator""_m;

    astas_osi3::GroundTruth ground_truth;
    std::map<std::string, std::string> tpm_params{{"velocity", "22"}};

    auto config = std::make_unique<TrafficParticipantControlUnitConfig>();
    config->name = "traffic_participant_model_example";
    config->parameters = tpm_params;
    auto astas_map{test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints()};
    config->astas_map = astas_map.get();
    map::LaneLocationProvider lane_location_provider{*astas_map};
    config->map_query_service = &lane_location_provider;

    auto controller = ControllerFactory::Create<CompositeControllerSUT>(42, std::move(config), true);

    auto* lane_assignment_controller = controller->GetLaneAssignmentControlUnit();
    entities::VehicleEntity entity{1, "traffic"};
    entity.SetPosition(mantle_api::Vec3<units::length::meter_t>{10.0_m, -15.25_m, 0.0_m});
    lane_assignment_controller->SetEntity(entity);

    EXPECT_NO_THROW(lane_assignment_controller->Step(mantle_api::Time{0}));
}

}  // namespace astas::environment::controller
