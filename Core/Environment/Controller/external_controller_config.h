/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_EXTERNALCONTROLLERCONFIG_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_EXTERNALCONTROLLERCONFIG_H

#include "Core/Environment/Host/host_vehicle_interface.h"
#include "Core/Environment/Host/host_vehicle_model.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/TrafficCommand/traffic_command_builder.h"
#include "Core/Service/UserSettings/user_settings.h"
#include "astas_osi_groundtruth.pb.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <MantleAPI/Traffic/i_entity_repository.h>

namespace astas::environment::controller
{
struct AstasExternalControllerConfig : mantle_api::ExternalControllerConfig
{
    environment::host::HostVehicleInterface* host_interface{nullptr};
    std::vector<mantle_api::Vec3<units::length::meter_t>> waypoints{};
    host::HostVehicleModel* host_vehicle_model{nullptr};
    const map::AstasMap* astas_map{nullptr};
    bool recovery_mode_enabled{false};
    traffic_command::TrafficCommandBuilder* traffic_command_builder{nullptr};
};

struct ProtoGroundTruthBuilderConfig
{
    mantle_api::Time step_size{};
    const mantle_api::IEntityRepository* entity_repository{nullptr};
};

struct TrafficParticipantControlUnitConfig : mantle_api::ExternalControllerConfig
{
    ProtoGroundTruthBuilderConfig proto_ground_truth_builder_config{};
    service::user_settings::MapChunking map_chunking{};
    service::user_settings::UserSettingsPathList plugins_paths{};
    const map::AstasMap* astas_map{nullptr};
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_EXTERNALCONTROLLERCONFIG_H
