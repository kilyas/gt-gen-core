/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/composite_controller_validator.h"

#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/follow_trajectory_with_speed_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/host_vehicle_interface_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/keep_velocity_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_change_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/lane_offset_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/maneuver_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/no_op_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/path_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/recovery_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_light_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/vehicle_model_control_unit.h"
#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"

namespace astas::environment::controller
{

void CompositeControllerValidator::DeleteConflictingControlUnits(
    IControlUnit* control_unit,
    std::vector<std::unique_ptr<IControlUnit>>& control_units,
    mantle_api::UniqueId unique_id)
{
    DeleteConflictingLongitudinalControlUnits(control_unit, control_units, unique_id);
    DeleteConflictingLateralControlUnits(control_unit, control_units, unique_id);
    DeleteConflictingRoutingControlUnits(control_unit, control_units, unique_id);
}

void CompositeControllerValidator::InsertControlUnitInOrder(std::unique_ptr<IControlUnit> control_unit,
                                                            std::vector<std::unique_ptr<IControlUnit>>& control_units,
                                                            mantle_api::UniqueId unique_id)
{
    Info("Adding {} to composite controller #{}", control_unit->GetName(), unique_id);

    /// Necessary order of control units for stepping:
    /// 1. Update scalars
    /// 2. Update position
    /// 3. Recover position
    /// (4. lane assignment (done separately))
    if (dynamic_cast<RecoveryControlUnit*>(control_unit.get()) != nullptr)
    {
        control_units.push_back(std::move(control_unit));
    }
    else if (IsRoutingControlUnit(control_unit.get()))
    {
        auto it = control_units.begin();
        for (; it != control_units.end(); it++)
        {
            if (dynamic_cast<RecoveryControlUnit*>((*it).get()) != nullptr)
            {
                break;
            }
        }
        control_units.insert(it, std::move(control_unit));
    }
    else
    {
        control_units.insert(control_units.begin(), std::move(control_unit));
    }
}

bool CompositeControllerValidator::NeedsPathControlUnit(std::vector<std::unique_ptr<IControlUnit>>& control_units)
{
    auto needs_path_control_unit = [](const auto& control_unit) {
        return !IsRoutingControlUnit(control_unit.get()) &&
               dynamic_cast<controller::NoOpControlUnit*>(control_unit.get()) == nullptr &&
               dynamic_cast<controller::TrafficLightControlUnit*>(control_unit.get()) == nullptr;
    };
    return std::all_of(control_units.cbegin(), control_units.cend(), needs_path_control_unit);
}

void CompositeControllerValidator::DeleteConflictingLongitudinalControlUnits(
    IControlUnit* control_unit,
    std::vector<std::unique_ptr<IControlUnit>>& control_units,
    mantle_api::UniqueId unique_id)
{
    if (IsLongitudinalControlUnit(control_unit))
    {
        auto it = control_units.begin();
        while (it != control_units.end())
        {
            if (IsLongitudinalControlUnit((*it).get()))
            {
                Info("Deleting {} in composite controller #{}", (*it)->GetName(), unique_id);
                it = control_units.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }
}

void CompositeControllerValidator::DeleteConflictingLateralControlUnits(
    IControlUnit* control_unit,
    std::vector<std::unique_ptr<IControlUnit>>& control_units,
    mantle_api::UniqueId unique_id)
{
    if (IsLateralControlUnit(control_unit))
    {
        auto it = control_units.begin();
        while (it != control_units.end())
        {
            if (IsLateralControlUnit((*it).get()))
            {
                Info("Deleting {} in composite controller #{}", (*it)->GetName(), unique_id);
                it = control_units.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }
}

void CompositeControllerValidator::DeleteConflictingRoutingControlUnits(
    IControlUnit* control_unit,
    std::vector<std::unique_ptr<IControlUnit>>& control_units,
    mantle_api::UniqueId unique_id)
{
    if (IsRoutingControlUnit(control_unit))
    {
        auto it = control_units.begin();
        while (it != control_units.end())
        {
            if (IsRoutingControlUnit((*it).get()))
            {
                Info("Deleting {} in composite controller #{}", (*it)->GetName(), unique_id);
                it = control_units.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }
}

bool CompositeControllerValidator::IsLongitudinalControlUnit(IControlUnit* control_unit)
{
    return dynamic_cast<VelocityControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<KeepVelocityControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<HostVehicleInterfaceControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<TrafficParticipantControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<FollowTrajectoryWithSpeedControlUnit*>(control_unit) != nullptr;
}

bool CompositeControllerValidator::IsLateralControlUnit(IControlUnit* control_unit)
{
    return dynamic_cast<LaneOffsetControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<LaneChangeControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<ManeuverControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<HostVehicleInterfaceControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<TrafficParticipantControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<FollowTrajectoryWithSpeedControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<FollowTrajectoryControlUnit*>(control_unit) != nullptr;
}

bool CompositeControllerValidator::IsRoutingControlUnit(IControlUnit* control_unit)
{
    return dynamic_cast<PathControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<ManeuverControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<VehicleModelControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<TrafficParticipantControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<LaneChangeControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<FollowTrajectoryWithSpeedControlUnit*>(control_unit) != nullptr ||
           dynamic_cast<FollowTrajectoryControlUnit*>(control_unit) != nullptr;
}

}  // namespace astas::environment::controller
