/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_EXTERNALCONTROLLERCONFIGCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_EXTERNALCONTROLLERCONFIGCONVERTER_H

#include "Core/Environment/Controller/external_controller_config.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/TrafficCommand/traffic_command_builder.h"
#include "Core/Service/UserSettings/user_settings.h"

#include <MantleAPI/Traffic/i_controller_config.h>

namespace astas::environment::controller
{
struct ExternalControllerConfigConverterData
{
    ProtoGroundTruthBuilderConfig proto_ground_truth_builder_config{};
    const environment::map::AstasMap* astas_map{nullptr};
    host::HostVehicleInterface* host_vehicle_interface{nullptr};
    host::HostVehicleModel* host_vehicle_model{nullptr};
    const service::user_settings::UserSettings* user_settings{nullptr};
    traffic_command::TrafficCommandBuilder* traffic_command_builder{nullptr};
};

class ExternalControllerConfigConverter
{
  public:
    explicit ExternalControllerConfigConverter(const ExternalControllerConfigConverterData& data) : data_(data){};

    mantle_api::IControllerConfig* GetConfig(mantle_api::ExternalControllerConfig* external_config) const;

  private:
    TrafficParticipantControlUnitConfig* ConvertToTpmConfig(
        mantle_api::ExternalControllerConfig* external_config) const;
    AstasExternalControllerConfig* ConvertToExternalHostConfig(
        mantle_api::ExternalControllerConfig* external_config) const;

    ExternalControllerConfigConverterData data_;
};
}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_EXTERNALCONTROLLERCONFIGCONVERTER_H
