/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_CONTROLLERDATAEXCHANGECONTAINER_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_CONTROLLERDATAEXCHANGECONTAINER_H

#include <MantleAPI/Common/orientation.h>
#include <units.h>

#include <vector>

namespace astas::environment::controller
{

struct ControllerDataExchangeContainer
{
    std::vector<units::velocity::meters_per_second_t> velocity_scalars{};
    units::acceleration::meters_per_second_squared_t acceleration_scalar{};

    units::angular_velocity::radians_per_second_t orientation_rate_scalar{};
    units::angular_acceleration::radians_per_second_squared_t orientation_acceleration_scalar{};

    std::vector<units::length::meter_t> lane_offset_scalars{};

    void Reset()
    {
        velocity_scalars.clear();
        acceleration_scalar = units::acceleration::meters_per_second_squared_t(0.0);

        orientation_rate_scalar = units::angular_velocity::radians_per_second_t(0.0);
        orientation_acceleration_scalar = units::angular_acceleration::radians_per_second_squared_t(0.0);

        lane_offset_scalars.clear();
    }
};

}  // namespace astas::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_CONTROLLERDATAEXCHANGECONTAINER_H
