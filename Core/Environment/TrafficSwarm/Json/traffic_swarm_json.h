/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_JSON_TRAFFICSWARMJSON_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_JSON_TRAFFICSWARMJSON_H

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/TrafficSwarm/Internal/random_velocity_generator.h"
#include "Core/Environment/TrafficSwarm/Internal/spawn_position_finder.h"
#include "Core/Environment/TrafficSwarm/Internal/tpm_stuck_detection.h"
#include "Core/Environment/TrafficSwarm/Internal/traffic_swarm_parameters.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>

#include <memory>
#include <string>
#include <vector>

namespace astas::environment::traffic_swarm
{

struct SpawnInfo
{
    mantle_api::UniqueId entity_id;
    std::string entity_name;
    mantle_api::Vec3<units::length::meter_t> position;
    double velocity;
    bool dog;
};

/// @brief Contains the logic for traffic swarm vehicles in a given radius around the host vehicle
class TrafficSwarm
{
  public:
    const static mantle_api::UniqueId kTrafficSwarmStartId{10'000};

    /// @brief Constructs the traffic swarm functionality
    /// @param[in] map Map needed to lookup spawn positions
    explicit TrafficSwarm(const map::AstasMap& map);

    /// @brief Configures the behaviour of the traffic swarm
    void SetParameters(const TrafficSwarmParameters& parameters);

    /// @brief Sets entity, representing the central position of the traffic swarm
    void SetCentralEntity(const mantle_api::IEntity* central_entity);

    /// @brief Checks in every simulation step to spawn or despawn traffic vehicles inside given radius
    void Step(const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities);

    /// @brief Returns the Spawn information containing all necessary data to create new entities
    std::vector<SpawnInfo> GetSpawnInfos() const;

    /// @brief Returns a list of entity IDs to be removed from the Environment
    std::vector<mantle_api::UniqueId> GetDespawnIds();

  protected:
    void InitializePositionFinder();

    std::vector<mantle_api::IEntity*> FindActiveTrafficSwarmEntities(
        const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities);

    void HandleVehiclesToSpawn();

    void SpawnTrafficVehicles(std::size_t spawn_count);

    void SpawnTrafficVehicle(
        double host_velocity,
        const SpawnPositionFinder::PositionAndSearchDirection& traffic_vehicle_position_and_direction);

    std::vector<mantle_api::Vec3<units::length::meter_t>> GetTrafficSwarmVehiclePositions() const;

    void HandleVehiclesToDespawn();

    bool IsVehicleOutsideSpawningRadius(const mantle_api::Vec3<units::length::meter_t>& vehicle_position,
                                        const mantle_api::Vec3<units::length::meter_t>& host_position) const;

    void RemoveFromSpawnList(const std::vector<mantle_api::UniqueId>& ids_to_remove);

    const map::AstasMap& map_;

    mantle_api::UniqueId next_entity_id_{kTrafficSwarmStartId};
    TrafficSwarmParameters parameters_{};
    const mantle_api::IEntity* central_entity_{nullptr};
    std::vector<mantle_api::UniqueId> active_traffic_swarm_entity_ids_{};
    std::vector<mantle_api::IEntity*> active_traffic_swarm_entities_{};

    std::unique_ptr<SpawnPositionFinder> spawn_position_finder_{nullptr};
    std::unique_ptr<RandomVelocityGenerator> random_velocity_generator_{nullptr};

    std::vector<SpawnInfo> spawn_infos_{};
    std::vector<mantle_api::UniqueId> ids_to_remove_{};

    DogStuckDetection stuck_detection_{};
};

}  // namespace astas::environment::traffic_swarm

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_JSON_TRAFFICSWARMJSON_H
