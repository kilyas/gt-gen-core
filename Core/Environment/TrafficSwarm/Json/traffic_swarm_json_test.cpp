/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Json/traffic_swarm_json.h"

#include "Core/Service/Utility/clock.h"
#include "Core/Tests/TestUtils/MapCatalogue/lane_catalogue.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_map_builder.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::traffic_swarm
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_ms;
using units::literals::operator""_s;

class SutTrafficSwarm : public TrafficSwarm
{
  public:
    explicit SutTrafficSwarm(const map::AstasMap& map) : TrafficSwarm(map) {}

    void AddTrafficSwarmEntityId(mantle_api::UniqueId id) { active_traffic_swarm_entity_ids_.push_back(id); }
};

std::unique_ptr<environment::map::AstasMap> CreateMapWithSevenParallelStraightLanes()
{
    const std::size_t number_of_parallel_lanes{7};
    const units::length::meter_t lane_width{4.0};
    test_utils::RawMapBuilder builder;
    builder.AddNewLaneGroup();

    for (std::size_t i{0}; i < number_of_parallel_lanes; i++)
    {
        const mantle_api::Vec3<units::length::meter_t> origin{0.0_m, lane_width * static_cast<double>(i), 0.0_m};
        builder.AddFromCatalogue(
            test_utils::LaneCatalogue::LongStraightEastingLaneWithThreePoints({origin.x, origin.y, origin.z}));
    }

    return builder.Build();
}

class TrafficSwarmTest : public testing::Test
{
  public:
    TrafficSwarmTest()
    {
        AddMockEntity(0, default_host_position, default_host_velocity);

        map_ = CreateMapWithSevenParallelStraightLanes();
        traffic_swarm_ = std::make_unique<SutTrafficSwarm>(*map_);

        auto* host = entities_.back().get();
        traffic_swarm_->SetCentralEntity(host);
    }

    void AddMockEntity(mantle_api::UniqueId id, const mantle_api::Vec3<units::length::meter_t>& pos, double velocity)
    {
        auto mock_vehicle = std::make_unique<mantle_api::MockVehicle>();
        EXPECT_CALL(*mock_vehicle, GetUniqueId()).WillRepeatedly(testing::Return(id));
        EXPECT_CALL(*mock_vehicle, GetPosition()).WillRepeatedly(testing::Return(pos));
        EXPECT_CALL(*mock_vehicle, GetVelocity())
            .WillRepeatedly(testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{
                units::velocity::meters_per_second_t(velocity), 0.0_mps, 0.0_mps}));
        entities_.push_back(std::move(mock_vehicle));
    }

  protected:
    std::vector<std::unique_ptr<mantle_api::IEntity>> entities_{};
    std::unique_ptr<SutTrafficSwarm> traffic_swarm_{nullptr};
    std::unique_ptr<environment::map::AstasMap> map_{nullptr};

    const double default_host_velocity{70.0};
    const mantle_api::Vec3<units::length::meter_t> default_host_position{0_m, 0_m, 0_m};
    const double default_min_velocity{16.667};
    const double default_max_velocity{69.444};
    const double default_spawning_distance{200.0};

    TrafficSwarmParameters parameters_zero_vehicles_{0,
                                                     default_spawning_distance,
                                                     {default_min_velocity, default_max_velocity}};

    TrafficSwarmParameters parameters_one_vehicle_{1,
                                                   default_spawning_distance,
                                                   {default_min_velocity, default_max_velocity}};

    TrafficSwarmParameters parameters_two_vehicles_{2,
                                                    default_spawning_distance,
                                                    {default_min_velocity, default_max_velocity}};
};

TEST_F(TrafficSwarmTest,
       GivenNumberofVehiclesInParametersIsZeroAndNoVehiclesInSwarm_WhenStep_ThenNoVehicleShallBeSpawned)
{
    traffic_swarm_->SetParameters(parameters_zero_vehicles_);

    traffic_swarm_->Step(entities_);

    EXPECT_TRUE(traffic_swarm_->GetSpawnInfos().empty());
}

TEST_F(TrafficSwarmTest,
       GivenNumberofVehiclesInParametersIsOneAndOneVehicleAlreadyInSwarm_WhenStep_ThenNoAdditionalVehicleShallBeSpawned)
{
    mantle_api::UniqueId already_spawned_id{10'000};
    AddMockEntity(already_spawned_id, {0.0_m, 0.0_m, 0.0_m}, 80.0);
    traffic_swarm_->AddTrafficSwarmEntityId(already_spawned_id);
    traffic_swarm_->SetParameters(parameters_one_vehicle_);

    traffic_swarm_->Step(entities_);

    EXPECT_TRUE(traffic_swarm_->GetSpawnInfos().empty());
}

TEST_F(TrafficSwarmTest,
       GivenNumberofVehiclesInParametersIsTwoAndNoVehicleInSwarm_WhenStep_ThenTwoVehiclesShallBeSpawned)
{
    traffic_swarm_->SetParameters(parameters_two_vehicles_);

    traffic_swarm_->Step(entities_);

    ASSERT_EQ(2, traffic_swarm_->GetSpawnInfos().size());

    EXPECT_EQ(mantle_api::UniqueId(10'000), traffic_swarm_->GetSpawnInfos().at(0).entity_id);
    EXPECT_EQ("traffic-swarm-vehicle-10000", traffic_swarm_->GetSpawnInfos().at(0).entity_name);
    EXPECT_GT(default_host_velocity, traffic_swarm_->GetSpawnInfos().at(0).velocity);
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(200_m, 4_m, 0_m),
                  traffic_swarm_->GetSpawnInfos().at(0).position);

    EXPECT_EQ(mantle_api::UniqueId(10'001), traffic_swarm_->GetSpawnInfos().at(1).entity_id);
    EXPECT_EQ("traffic-swarm-vehicle-10001", traffic_swarm_->GetSpawnInfos().at(1).entity_name);
    EXPECT_GT(default_host_velocity, traffic_swarm_->GetSpawnInfos().at(1).velocity);
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(200_m, 24_m, 0_m),
                  traffic_swarm_->GetSpawnInfos().at(1).position);
}

TEST_F(TrafficSwarmTest,
       GivenNumberofVehiclesInParametersIsTwoAndOneVehicleInSwarm_WhenStep_ThenOneAdditionalVehicleShallBeSpawned)
{
    mantle_api::UniqueId already_spawned_id{10'000};
    AddMockEntity(already_spawned_id, {0.0_m, 0.0_m, 0.0_m}, 80.0);
    traffic_swarm_->AddTrafficSwarmEntityId(already_spawned_id);
    traffic_swarm_->SetParameters(parameters_two_vehicles_);

    traffic_swarm_->Step(entities_);

    EXPECT_EQ(1, traffic_swarm_->GetSpawnInfos().size());
}

TEST_F(TrafficSwarmTest, GivenSpawnedVehicleDespawnedDueToEndOfRoad_WhenStep_ThenAnotherVehicleIsSpawned)
{
    mantle_api::UniqueId already_spawned_but_not_existing_entity_id{10'000};
    traffic_swarm_->AddTrafficSwarmEntityId(already_spawned_but_not_existing_entity_id);
    traffic_swarm_->SetParameters(parameters_one_vehicle_);

    traffic_swarm_->Step(entities_);

    ASSERT_EQ(1, traffic_swarm_->GetSpawnInfos().size());
}

TEST_F(TrafficSwarmTest, GivenNoSpawnedVehicles_WhenStep_ThenNoVehiclesShallBeDespawned)
{
    traffic_swarm_->SetParameters(parameters_two_vehicles_);

    traffic_swarm_->Step(entities_);

    EXPECT_TRUE(traffic_swarm_->GetDespawnIds().empty());
}

TEST_F(TrafficSwarmTest, GivenSpawnedVehicleIsInsideSpawningRadius_WhenStep_ThenVehicleShallNotBeDespawned)
{
    mantle_api::UniqueId already_spawned_id{10'000};
    AddMockEntity(already_spawned_id,
                  {units::length::meter_t(default_spawning_distance) - 1_m, 0.0_m, 0.0_m},
                  default_max_velocity);
    traffic_swarm_->AddTrafficSwarmEntityId(already_spawned_id);
    traffic_swarm_->SetParameters(parameters_one_vehicle_);

    traffic_swarm_->Step(entities_);

    EXPECT_TRUE(traffic_swarm_->GetDespawnIds().empty());
}

TEST_F(TrafficSwarmTest, GivenSpawnedDogVehicleIsInsideSpawningRadiusButItDoesNotMove_WhenStep_ThenDogShallBeDespawned)
{
    service::utility::Clock::Instance().SetNow(0_ms);

    mantle_api::UniqueId dog_to_be_stuck_and_despawned_id{10'000};
    AddMockEntity(dog_to_be_stuck_and_despawned_id,
                  {units::length::meter_t(default_spawning_distance) - 1_m, 0.0_m, 0.0_m},
                  default_max_velocity);

    mantle_api::VehicleProperties dog_properties;
    dog_properties.properties["external"] = "dog";
    entities_.back()->SetProperties(std::make_unique<mantle_api::VehicleProperties>(dog_properties));

    traffic_swarm_->AddTrafficSwarmEntityId(dog_to_be_stuck_and_despawned_id);
    traffic_swarm_->SetParameters(parameters_one_vehicle_);

    traffic_swarm_->Step(entities_);
    ASSERT_TRUE(traffic_swarm_->GetDespawnIds().empty());

    service::utility::Clock::Instance().SetNow(20_s);
    traffic_swarm_->Step(entities_);
    ASSERT_FALSE(traffic_swarm_->GetDespawnIds().empty());
    EXPECT_EQ(dog_to_be_stuck_and_despawned_id, traffic_swarm_->GetDespawnIds().front());
}

TEST_F(TrafficSwarmTest, GivenSpawnedVehicleIsExactlyOnSpawningRadius_WhenStep_ThenVehicleShallNotBeDespawned)
{
    mantle_api::UniqueId already_spawned_id{10'000};
    AddMockEntity(
        already_spawned_id, {units::length::meter_t(default_spawning_distance), 0.0_m, 0.0_m}, default_max_velocity);
    traffic_swarm_->SetParameters(parameters_one_vehicle_);
    traffic_swarm_->AddTrafficSwarmEntityId(already_spawned_id);

    traffic_swarm_->Step(entities_);

    EXPECT_TRUE(traffic_swarm_->GetDespawnIds().empty());
}

TEST_F(TrafficSwarmTest, GivenSpawnedVehicleDespawnedDueToEndOfRoad_WhenStep_ThenNoVehicleShallBeDespawned)
{
    mantle_api::UniqueId already_spawned_but_not_existing_entity_id{10'000};
    traffic_swarm_->AddTrafficSwarmEntityId(already_spawned_but_not_existing_entity_id);
    traffic_swarm_->SetParameters(parameters_one_vehicle_);

    traffic_swarm_->Step(entities_);

    EXPECT_TRUE(traffic_swarm_->GetDespawnIds().empty());
}

TEST_F(TrafficSwarmTest, GivenTwoSpawnedVehicleOutsideOfTrafficSwarmRadius_WhenStep_ThenBothVehiclesShallBeDespawned)
{
    mantle_api::UniqueId spawned_id_1{10'000};
    AddMockEntity(spawned_id_1,
                  {units::length::meter_t(default_spawning_distance + 2), 0.0_m, 0.0_m},
                  default_max_velocity);  // tolerance of 1 - why?
    traffic_swarm_->AddTrafficSwarmEntityId(spawned_id_1);

    mantle_api::UniqueId spawned_id_2{10'001};
    AddMockEntity(
        spawned_id_2, {units::length::meter_t(default_spawning_distance + 2), 0.0_m, 0.0_m}, default_max_velocity);
    traffic_swarm_->AddTrafficSwarmEntityId(spawned_id_2);

    traffic_swarm_->SetParameters(parameters_one_vehicle_);

    traffic_swarm_->Step(entities_);

    ASSERT_EQ(2, traffic_swarm_->GetDespawnIds().size());
    EXPECT_EQ(spawned_id_1, traffic_swarm_->GetDespawnIds().at(0));
    EXPECT_EQ(spawned_id_2, traffic_swarm_->GetDespawnIds().at(1));
}

TEST_F(TrafficSwarmTest, GivenTrafficSwarmWithCentralEntitySet_WhenSettingCentralEntity_ThenDie)
{
    ASSERT_DEATH(traffic_swarm_->SetCentralEntity(entities_.back().get()), ".*");
}

TEST_F(TrafficSwarmTest,
       GivenNumberofVehiclesInParametersIsOneDogVehicleInSwarm_WhenStep_ThenOneDogVehicleShallBeSpawned)
{
    auto parameters_dog = parameters_one_vehicle_;
    parameters_dog.use_trm = true;

    traffic_swarm_->SetParameters(parameters_dog);

    traffic_swarm_->Step(entities_);

    ASSERT_EQ(1, traffic_swarm_->GetSpawnInfos().size());
    EXPECT_TRUE(traffic_swarm_->GetSpawnInfos().front().dog);
}

/**
 * Since the traffic swarm feature has a minor limitation, the limitation needs to be protected with a test.
 * The default calculation logic of the spawn position is the following:
 *      1. Get the host position.
 *      2. Get the position <spawning_distance>m in front / behind the host in the host lane.
 *      3. Get all possible spawning lanes from the position from step 2 (adjacent lanes).
 *      4. Get a random lane from step 3.
 *      5. Project the point from step 2 to a centerline of the lane from step 4. This is a final spawning position.
 *
 * With this algorithm there is a limitation if the host is on a lane with a large number of adjacent lanes.
 * In that case if we spawn the vehicle in the 6th lane from the hosts lane, the vehicle will be despawned in the
 * next tick because it is too far from the host and the despawn condition is satisfied.
 *
 * This limitation comes into place only if we have 7 lanes in the same direction as the hosts lane (under the
 * assumption that the lane width is 3.5m).
 */
TEST_F(
    TrafficSwarmTest,
    GivenTrafficSwarmVehicleIsOutsideSpawningRadiusToleranceDueToLateralDistance_WhenStep_ThenVehicleShallBeDespawned)
{
    constexpr double lane_width{3.5};
    constexpr units::length::meter_t y_sixth_lane{lane_width * 6};
    const mantle_api::Vec3<units::length::meter_t> position_at_spawning_distance_in_sixth_lane{
        units::length::meter_t(default_spawning_distance), y_sixth_lane, 0_m};

    mantle_api::UniqueId id_of_entity_in_sixth_lane{10'000};
    AddMockEntity(id_of_entity_in_sixth_lane, position_at_spawning_distance_in_sixth_lane, default_max_velocity);

    traffic_swarm_->AddTrafficSwarmEntityId(id_of_entity_in_sixth_lane);
    traffic_swarm_->SetParameters(parameters_one_vehicle_);

    traffic_swarm_->Step(entities_);

    ASSERT_EQ(1, traffic_swarm_->GetDespawnIds().size());
    EXPECT_EQ(id_of_entity_in_sixth_lane, traffic_swarm_->GetDespawnIds().at(0));
}

}  // namespace astas::environment::traffic_swarm
