/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Json/traffic_swarm_json.h"

#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/Logging/logging.h"

namespace astas::environment::traffic_swarm
{

TrafficSwarm::TrafficSwarm(const map::AstasMap& map) : map_{map} {}

void TrafficSwarm::SetParameters(const TrafficSwarmParameters& parameters)
{
    parameters_ = parameters;

    InitializePositionFinder();

    random_velocity_generator_ = std::make_unique<RandomVelocityGenerator>(parameters_.vehicle_velocity_bounds.first,
                                                                           parameters.vehicle_velocity_bounds.second);
}

void TrafficSwarm::SetCentralEntity(const mantle_api::IEntity* central_entity)
{

    ASSERT(central_entity_ == nullptr && "Central entity can only be set once");
    central_entity_ = central_entity;
}

void TrafficSwarm::InitializePositionFinder()
{
    constexpr double same_lane_minimum_spawning_distance{50};

    spawn_position_finder_ =
        std::make_unique<SpawnPositionFinder>(same_lane_minimum_spawning_distance, map_, parameters_);
}

std::vector<mantle_api::IEntity*> TrafficSwarm::FindActiveTrafficSwarmEntities(
    const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities)
{
    ids_to_remove_.clear();

    std::vector<mantle_api::IEntity*> tracked_entities;

    for (const auto& id : active_traffic_swarm_entity_ids_)
    {
        auto found_it =
            std::find_if(entities.begin(), entities.end(), [id](const std::unique_ptr<mantle_api::IEntity>& entity) {
                return id == entity->GetUniqueId();
            });

        if (found_it != entities.end())
        {
            tracked_entities.push_back((*found_it).get());
        }
        else
        {
            ids_to_remove_.push_back(id);
        }
    }

    RemoveFromSpawnList(ids_to_remove_);

    return tracked_entities;
}

void TrafficSwarm::Step(const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities)
{
    active_traffic_swarm_entities_ = FindActiveTrafficSwarmEntities(entities);

    HandleVehiclesToDespawn();
    HandleVehiclesToSpawn();
}

void TrafficSwarm::HandleVehiclesToDespawn()
{
    ids_to_remove_.clear();

    auto central_swarm_position = central_entity_->GetPosition();

    for (const auto& tracked_entity : active_traffic_swarm_entities_)
    {
        if (IsVehicleOutsideSpawningRadius(tracked_entity->GetPosition(), central_swarm_position) ||
            stuck_detection_.IsStuck(tracked_entity, map_.coordinate_converter.get()))
        {
            ids_to_remove_.push_back(tracked_entity->GetUniqueId());
        }
    }

    RemoveFromSpawnList(ids_to_remove_);
}

bool TrafficSwarm::IsVehicleOutsideSpawningRadius(const mantle_api::Vec3<units::length::meter_t>& vehicle_position,
                                                  const mantle_api::Vec3<units::length::meter_t>& host_position) const
{
    constexpr double despawn_radius_tolerance{1.0};

    double distance = service::glmwrapper::Distance(vehicle_position, host_position);
    return distance > parameters_.spawning_distance + despawn_radius_tolerance;
}

void TrafficSwarm::RemoveFromSpawnList(const std::vector<mantle_api::UniqueId>& ids_to_remove)
{
    active_traffic_swarm_entity_ids_.erase(
        std::remove_if(
            active_traffic_swarm_entity_ids_.begin(),
            active_traffic_swarm_entity_ids_.end(),
            [&](auto id) { return std::find(ids_to_remove.begin(), ids_to_remove.end(), id) != ids_to_remove.end(); }),
        active_traffic_swarm_entity_ids_.end());
}

void TrafficSwarm::HandleVehiclesToSpawn()
{
    spawn_infos_.clear();

    auto spawn_count = static_cast<std::size_t>(parameters_.vehicle_count) - active_traffic_swarm_entity_ids_.size();
    if (spawn_count > 0)
    {
        SpawnTrafficVehicles(spawn_count);
    }
}

void TrafficSwarm::SpawnTrafficVehicles(std::size_t spawn_count)
{
    double host_velocity = central_entity_->GetVelocity().Length()();
    auto host_position = central_entity_->GetPosition();

    auto occupied_positions = GetTrafficSwarmVehiclePositions();
    auto found_positions = spawn_position_finder_->TryFindSpawningPositionsAndDirections(
        host_position, host_velocity, spawn_count, std::move(occupied_positions));

    for (const auto& position : found_positions)
    {
        SpawnTrafficVehicle(host_velocity, position);
    }
}

std::vector<mantle_api::Vec3<units::length::meter_t>> TrafficSwarm::GetTrafficSwarmVehiclePositions() const
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> vehicle_positions;
    vehicle_positions.reserve(active_traffic_swarm_entities_.size());

    std::transform(active_traffic_swarm_entities_.begin(),
                   active_traffic_swarm_entities_.end(),
                   std::back_inserter(vehicle_positions),
                   [](const auto* entity) { return entity->GetPosition(); });

    return vehicle_positions;
}

void TrafficSwarm::SpawnTrafficVehicle(
    double host_velocity,
    const SpawnPositionFinder::PositionAndSearchDirection& traffic_vehicle_position_and_direction)
{
    const auto& [auto_spawn_vehicle_position, direction] = traffic_vehicle_position_and_direction;

    auto [lower_velocity_bound, upper_velocity_bound] = parameters_.vehicle_velocity_bounds;
    if (direction == SpawnPositionFinder::SearchDirection::kInFrontOfTheHost)
    {
        upper_velocity_bound = host_velocity;
    }
    else
    {
        lower_velocity_bound = host_velocity;
    }

    double traffic_velocity =
        random_velocity_generator_->GenerateVelocityInsideBounds(lower_velocity_bound, upper_velocity_bound);

    std::string name = fmt::format("traffic-swarm-vehicle-{}", next_entity_id_);
    spawn_infos_.push_back({next_entity_id_, name, auto_spawn_vehicle_position, traffic_velocity, parameters_.use_trm});

    active_traffic_swarm_entity_ids_.push_back(next_entity_id_);
    next_entity_id_++;
}

std::vector<SpawnInfo> TrafficSwarm::GetSpawnInfos() const
{
    return spawn_infos_;
}
std::vector<mantle_api::UniqueId> TrafficSwarm::GetDespawnIds()
{
    return ids_to_remove_;
}

}  // namespace astas::environment::traffic_swarm
