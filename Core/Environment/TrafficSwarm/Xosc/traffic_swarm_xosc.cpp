/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Xosc/traffic_swarm_xosc.h"

#include "Core/Environment/TrafficSwarm/Internal/traffic_swarm_parameters.h"
#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Common/pose.h>

namespace astas::environment::traffic_swarm
{

using units::literals::operator""_m;

TrafficSwarmXosc::TrafficSwarmXosc(const map::LaneLocationProvider& lane_location_provider,
                                   const api::EntityRepository& entity_repository,
                                   const mantle_api::TrafficSwarmParameters& parameters,
                                   units::length::meter_t longitudinal_distance_between_spawned_vehicles)
    : lane_location_provider_{lane_location_provider}, entity_repository_{entity_repository}, parameters{parameters}
{
    // TODO: remove when supported
    if (parameters.exclusion_radius != 0.0_m)
    {
        Error("TrafficSwarm : the \"innerRadius\" parameter is not yet supported.");
    }

    // TODO: remove when supported
    if (parameters.semi_major_spawning_radius != 0.0_m)
    {
        Error("TrafficSwarm : the \"semiMajorAxis\" parameter is not yet supported.");
    }

    // TODO: remove when supported
    if (parameters.spawning_area_longitudinal_offset != 0.0_m)
    {
        Error("TrafficSwarm : the \"offset\" parameter is not yet supported.");
    }

    TrafficSwarmParameters traffic_parameters;
    traffic_parameters.vehicle_count = parameters.maximum_number_of_vehicles;
    traffic_parameters.spawning_distance = parameters.semi_minor_spawning_radius.value();
    traffic_parameters.vehicle_velocity_bounds = {parameters.speed_range.minimum.value(),
                                                  parameters.speed_range.maximum.value()};
    traffic_parameters.use_trm = true;

    spawn_position_finder_ = std::make_unique<SpawnPositionFinder>(
        longitudinal_distance_between_spawned_vehicles(), lane_location_provider_.GetAstasMap(), traffic_parameters);
}

std::vector<mantle_api::ITrafficSwarmService::SpawningPosition> TrafficSwarmXosc::GetAvailableSpawningPoses() const
{
    using RelativePosition = mantle_api::ITrafficSwarmService::RelativePosition;

    const auto& central_entity{entity_repository_.Get(parameters.central_entity_name)};

    if (!central_entity.has_value())
    {
        return {};
    }

    std::vector<mantle_api::Vec3<units::length::meter_t>> occupied_positions{};

    const auto& entities{entity_repository_.GetEntities()};

    for (const auto& entity : entities)
    {
        if (entity->GetUniqueId() != central_entity->get().GetUniqueId())
        {
            occupied_positions.push_back(entity->GetPosition());
        }
    }

    const auto spawning_positions_and_directions{spawn_position_finder_->TryFindSpawningPositionsAndDirections(
        central_entity->get().GetPosition(),
        central_entity->get().GetVelocity().Length().value(),
        (parameters.maximum_number_of_vehicles - swarm_entities_count_),
        std::move(occupied_positions))};

    std::vector<mantle_api::ITrafficSwarmService::SpawningPosition> spawning_positions;

    for (const auto& [position, direction] : spawning_positions_and_directions)
    {
        mantle_api::Pose pose;
        pose.position = position;
        pose.orientation = lane_location_provider_.GetLaneOrientation(position);

        RelativePosition relative_position{direction == SpawnPositionFinder::SearchDirection::kInFrontOfTheHost
                                               ? RelativePosition::kInFront
                                               : RelativePosition::kBehind};

        spawning_positions.push_back({pose, relative_position});
    }

    return spawning_positions;
}

mantle_api::VehicleProperties TrafficSwarmXosc::GetVehicleProperties(mantle_api::VehicleClass vehicle_class) const
{
    if (vehicle_class != mantle_api::VehicleClass::kSmall_car && vehicle_class != mantle_api::VehicleClass::kMedium_car)
    {
        Error("TrafficSwarm : only the small car and medium car vehicle classes are currently supported");
    };

    mantle_api::VehicleProperties properties;
    properties.is_host = false;
    properties.is_controlled_externally = true;
    properties.type = mantle_api::EntityType::kVehicle;
    properties.classification = mantle_api::VehicleClass::kSmall_car;
    properties.model = "BMWMINI2014";  // use mini BMWMINI2014 as a default vehicle property
    properties.front_axle.bb_center_to_axle_center = {1.1615_m, 0_m, -0.413_m};
    properties.rear_axle.bb_center_to_axle_center = {-1.3335_m, 0_m, -0.413_m};
    properties.front_axle.wheel_diameter = units::length::meter_t(0.588);
    properties.rear_axle.wheel_diameter = units::length::meter_t(0.588);
    properties.bounding_box.dimension.length = units::length::meter_t(3.821);
    properties.bounding_box.dimension.width = units::length::meter_t(1.727);
    properties.bounding_box.dimension.height = units::length::meter_t(1.414);

    return properties;
}

void TrafficSwarmXosc::UpdateControllerConfig(std::unique_ptr<mantle_api::ExternalControllerConfig>& config,
                                              units::velocity::meters_per_second_t speed)
{
    config->parameters["velocity"] = std::to_string(speed.value());
}

void TrafficSwarmXosc::SetSwarmEntitiesCount(size_t count)
{
    swarm_entities_count_ = count;
}

}  // namespace astas::environment::traffic_swarm
