/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_XOSC_TRAFFICSWARMXOSC_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_XOSC_TRAFFICSWARMXOSC_H

#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Environment/TrafficSwarm/Internal/spawn_position_finder.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <MantleAPI/Traffic/i_traffic_swarm_service.h>

#include <vector>

namespace astas::environment::traffic_swarm
{

class TrafficSwarmXosc : public mantle_api::ITrafficSwarmService
{
  public:
    explicit TrafficSwarmXosc(const map::LaneLocationProvider& lane_location_provider,
                              const api::EntityRepository& entity_repository,
                              const mantle_api::TrafficSwarmParameters& parameters,
                              units::length::meter_t longitudinal_distance_between_spawned_vehicles);

    std::vector<mantle_api::ITrafficSwarmService::SpawningPosition> GetAvailableSpawningPoses() const override;
    mantle_api::VehicleProperties GetVehicleProperties(mantle_api::VehicleClass vehicle_class) const override;
    void UpdateControllerConfig(std::unique_ptr<mantle_api::ExternalControllerConfig>& config,
                                units::velocity::meters_per_second_t speed) override;
    void SetSwarmEntitiesCount(size_t count) override;

  private:
    const api::EntityRepository& entity_repository_;
    const map::LaneLocationProvider& lane_location_provider_;
    const mantle_api::TrafficSwarmParameters parameters;
    std::unique_ptr<SpawnPositionFinder> spawn_position_finder_{nullptr};
    size_t swarm_entities_count_{0};
};

}  // namespace astas::environment::traffic_swarm

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_XOSC_TRAFFICSWARMXOSC_H
