/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_TRAFFICSWARMPARAMETERS_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_TRAFFICSWARMPARAMETERS_H

#include <cstdint>
#include <string>
#include <utility>

namespace astas::environment::traffic_swarm
{

struct TrafficSwarmParameters
{
    std::size_t vehicle_count{};
    double spawning_distance{};
    std::pair<double, double> vehicle_velocity_bounds{};
    bool use_trm{false};
};

}  // namespace astas::environment::traffic_swarm

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_TRAFFICSWARMPARAMETERS_H
