/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/spawn_position_finder.h"

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"
#include "Core/Environment/TrafficSwarm/Internal/ray_sphere_intersection.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/MantleApiExtension/formatting.h"

namespace astas::environment::traffic_swarm
{

bool IsLaneValidForSpawning(const environment::map::Lane& lane)
{
    return lane.flags.IsDrivable() && !lane.flags.IsShoulderLane() && !lane.flags.IsMergeLane() &&
           !lane.flags.IsSplitLane();
}

bool ArePositionsWithinDistance(const mantle_api::Vec3<units::length::meter_t>& first_position,
                                const mantle_api::Vec3<units::length::meter_t>& second_position,
                                double distance)
{
    return service::glmwrapper::Distance(first_position, second_position) < distance;
}

std::optional<mantle_api::Vec3<units::length::meter_t>> InterpolateCenterLinePoint(
    const double spawning_range,
    const mantle_api::Vec3<units::length::meter_t>& host_position,
    const mantle_api::Vec3<units::length::meter_t>& point_within_spawning_range,
    const mantle_api::Vec3<units::length::meter_t>& point_outside_spawning_range)
{
    Ray ray{};
    ray.direction = service::glmwrapper::Normalize(point_within_spawning_range - point_outside_spawning_range);
    ray.origin = point_outside_spawning_range;

    Sphere sphere{};
    sphere.center = host_position;
    sphere.radius = spawning_range;

    const auto intersection = RaySphereIntersection(ray, sphere);

    if (intersection.has_value())
    {
        return intersection->intersection_point;
    }
    return std::nullopt;
}

SpawnPositionFinder::SpawnPositionFinder(double distance_between_spawned_vehicles,
                                         const environment::map::AstasMap& map,
                                         TrafficSwarmParameters auto_spawn_parameters)
    : lane_location_provider(map),
      auto_spawn_parameters(std::move(auto_spawn_parameters)),
      distance_between_spawned_vehicles(distance_between_spawned_vehicles)
{
}

std::vector<SpawnPositionFinder::PositionAndSearchDirection> SpawnPositionFinder::TryFindSpawningPositionsAndDirections(
    const mantle_api::Vec3<units::length::meter_t>& host_position,
    double host_velocity,
    std::size_t max_spawning_positions,
    std::vector<mantle_api::Vec3<units::length::meter_t>>&& occupied_positions)
{
    occupied_positions_ = std::move(occupied_positions);

    std::optional<LanesAtPosition> lanes_at_spawn_distance_in_front =
        FindLanesAtPositionInFrontOfHost(host_position, host_velocity);

    std::optional<LanesAtPosition> lanes_at_spawn_distance_behind =
        FindLanesAtPositionBehindHost(host_position, host_velocity);

    if (!lanes_at_spawn_distance_in_front.has_value() && !lanes_at_spawn_distance_behind.has_value())
    {
        TRACE("There are no available lanes {}m from host, which is at position {}",
              auto_spawn_parameters.spawning_distance,
              host_position)
        return {};
    }

    return PickValidSpawnPositions(
        max_spawning_positions, lanes_at_spawn_distance_behind, lanes_at_spawn_distance_in_front);
}

std::optional<SpawnPositionFinder::LanesAtPosition> SpawnPositionFinder::FindLanesAtPositionInFrontOfHost(
    const mantle_api::Vec3<units::length::meter_t>& host_position,
    double host_velocity)
{
    if (host_velocity > auto_spawn_parameters.vehicle_velocity_bounds.first)
    {
        return GetPositionAndLanesAtSpawningDistance(host_position, SearchDirection::kInFrontOfTheHost);
    }

    return std::nullopt;
}

std::optional<SpawnPositionFinder::LanesAtPosition> SpawnPositionFinder::FindLanesAtPositionBehindHost(
    const mantle_api::Vec3<units::length::meter_t>& host_position,
    double host_velocity)
{
    if (host_velocity < auto_spawn_parameters.vehicle_velocity_bounds.second)
    {
        return GetPositionAndLanesAtSpawningDistance(host_position, SearchDirection::kBehindTheHost);
    }

    return std::nullopt;
}

std::vector<SpawnPositionFinder::PositionAndSearchDirection> SpawnPositionFinder::PickValidSpawnPositions(
    const std::size_t max_spawning_positions,
    std::optional<LanesAtPosition>& possible_lanes_behind,
    std::optional<LanesAtPosition>& possible_lanes_front)
{
    const std::size_t number_of_spawning_positions =
        GetNumberOfSpawningPositions(possible_lanes_behind, possible_lanes_front, max_spawning_positions);

    std::vector<PositionAndSearchDirection> valid_spawning_positions;
    valid_spawning_positions.reserve(number_of_spawning_positions);

    for (std::size_t i = 0; i < number_of_spawning_positions; ++i)
    {
        const auto& spawn_position = PickPositionInRandomLane(possible_lanes_front, possible_lanes_behind);
        if (lane_location_provider.FindLanes(spawn_position.first).size() == 1)
        {
            valid_spawning_positions.emplace_back(spawn_position);
        }
    }

    return valid_spawning_positions;
}

std::optional<SpawnPositionFinder::LanesAtPosition> SpawnPositionFinder::GetPositionAndLanesAtSpawningDistance(
    const mantle_api::Vec3<units::length::meter_t>& host_position,
    const SearchDirection direction) const
{
    const auto position = (direction == SearchDirection::kInFrontOfTheHost) ? FindPositionInFrontOfHost(host_position)
                                                                            : FindPositionBehindHost(host_position);
    if (position.has_value())
    {
        const bool include_own_lane{direction == SearchDirection::kInFrontOfTheHost || auto_spawn_parameters.use_trm};

        auto lanes_at_position = GetParallelDrivingLanesAtPosition(*position, include_own_lane);

        lanes_at_position = GetLanesValidForSpawning(lanes_at_position, *position);

        if (!lanes_at_position.empty())
        {
            return LanesAtPosition{std::move(lanes_at_position), *position};
        }
    }
    return std::nullopt;
}

std::size_t SpawnPositionFinder::GetNumberOfSpawningPositions(
    const std::optional<LanesAtPosition>& possible_lanes_behind,
    const std::optional<LanesAtPosition>& possible_lanes_front,
    const std::size_t max_spawning_positions)
{
    std::size_t number_of_spawning_positions{0};

    if (possible_lanes_behind.has_value())
    {
        number_of_spawning_positions += possible_lanes_behind->lanes.size();
    }
    if (possible_lanes_front.has_value())
    {
        number_of_spawning_positions += possible_lanes_front->lanes.size();
    }

    return std::min(number_of_spawning_positions, max_spawning_positions);
}

SpawnPositionFinder::PositionAndSearchDirection SpawnPositionFinder::PickPositionInRandomLane(
    std::optional<LanesAtPosition>& lanes_at_spawn_distance_in_front,
    std::optional<LanesAtPosition>& lanes_at_spawn_distance_behind)
{
    const std::size_t num_lanes_in_front = GetNumberOfLanes(lanes_at_spawn_distance_in_front);
    const std::size_t num_lanes_behind = GetNumberOfLanes(lanes_at_spawn_distance_behind);

    const std::size_t lane_index = GetRandomIndex(num_lanes_in_front + num_lanes_behind);

    const environment::map::Lane* target_lane{nullptr};

    mantle_api::Vec3<units::length::meter_t> position_at_spawn_distance;
    SearchDirection direction = GetLaneDirection(lane_index, num_lanes_in_front);
    if (direction == SearchDirection::kInFrontOfTheHost)
    {
        position_at_spawn_distance = lanes_at_spawn_distance_in_front->position;
        target_lane = lanes_at_spawn_distance_in_front->lanes[lane_index];
        auto erase_index_it{lanes_at_spawn_distance_in_front->lanes.begin()};
        std::advance(erase_index_it, lane_index);
        lanes_at_spawn_distance_in_front->lanes.erase(erase_index_it);
    }
    else
    {
        position_at_spawn_distance = lanes_at_spawn_distance_behind->position;
        target_lane = lanes_at_spawn_distance_behind->lanes[lane_index - num_lanes_in_front];
        auto erase_index_it{lanes_at_spawn_distance_behind->lanes.begin()};
        std::advance(erase_index_it, lane_index - num_lanes_in_front);
        lanes_at_spawn_distance_behind->lanes.erase(erase_index_it);
    }

    const auto projected_centerline_point = std::get<0>(
        environment::map::ProjectQueryPointOnPolyline(position_at_spawn_distance, target_lane->center_line));
    return {projected_centerline_point, direction};
}

std::optional<mantle_api::Vec3<units::length::meter_t>> SpawnPositionFinder::FindPositionInFrontOfHost(
    const mantle_api::Vec3<units::length::meter_t>& host_position) const
{
    const auto host_lane_location = lane_location_provider.GetLaneLocation(host_position);
    if (!host_lane_location.IsValid())
    {
        return std::nullopt;
    }
    const environment::map::Lane* current_lane = host_lane_location.lanes.front();
    mantle_api::Vec3<units::length::meter_t> current_traffic_centerline_point;
    std::size_t current_centerline_index = host_lane_location.centerline_point_index;

    do
    {
        if (current_centerline_index == current_lane->center_line.size() - 1)
        {
            auto connected_lane = GetSuccessorLane(current_lane);
            if (connected_lane == nullptr)
            {
                TRACE("End of lane with ID {} reached {}m in front of host.",
                      current_lane->id,
                      service::glmwrapper::Distance(current_traffic_centerline_point, host_position))
                return std::nullopt;
            }
            current_lane = connected_lane;
            current_centerline_index = 0;
        }
        current_centerline_index++;
        current_traffic_centerline_point = current_lane->center_line[current_centerline_index];
    } while (ArePositionsWithinDistance(
        current_traffic_centerline_point, host_position, auto_spawn_parameters.spawning_distance));

    if (current_centerline_index <= 0)
    {
        return std::nullopt;
    }

    const mantle_api::Vec3<units::length::meter_t>& point_outside_of_spawning_range = current_traffic_centerline_point;
    const mantle_api::Vec3<units::length::meter_t>& point_within_spawning_range =
        current_lane->center_line[current_centerline_index - 1];

    return InterpolateCenterLinePoint(auto_spawn_parameters.spawning_distance,
                                      host_position,
                                      point_within_spawning_range,
                                      point_outside_of_spawning_range);
}

std::optional<mantle_api::Vec3<units::length::meter_t>> SpawnPositionFinder::FindPositionBehindHost(
    const mantle_api::Vec3<units::length::meter_t>& host_position) const
{
    const auto host_lane_location = lane_location_provider.GetLaneLocation(host_position);
    if (!host_lane_location.IsValid())
    {
        return std::nullopt;
    }

    const environment::map::Lane* current_lane = host_lane_location.lanes.front();
    std::size_t current_centerline_index = host_lane_location.centerline_point_index;
    mantle_api::Vec3 current_traffic_centerline_point = current_lane->center_line[current_centerline_index];

    while (ArePositionsWithinDistance(
        current_traffic_centerline_point, host_position, auto_spawn_parameters.spawning_distance))
    {
        if (current_centerline_index == 0)
        {
            auto connected_lane = GetPredecessorLane(current_lane);
            if (connected_lane == nullptr)
            {
                TRACE("End of lane with ID {} reached {}m behind host.",
                      current_lane->id,
                      service::glmwrapper::Distance(current_traffic_centerline_point, host_position))
                return std::nullopt;
            }
            current_lane = connected_lane;
            current_centerline_index = current_lane->center_line.size() - 1;
        }
        else
        {
            current_centerline_index--;
        }
        current_traffic_centerline_point = current_lane->center_line[current_centerline_index];
    }

    if (current_centerline_index >= current_lane->center_line.size() - 1)
    {
        return std::nullopt;
    }

    const mantle_api::Vec3<units::length::meter_t>& point_outside_of_spawning_range = current_traffic_centerline_point;
    const mantle_api::Vec3<units::length::meter_t>& point_within_spawning_range =
        current_lane->center_line[current_centerline_index + 1];

    return InterpolateCenterLinePoint(auto_spawn_parameters.spawning_distance,
                                      host_position,
                                      point_within_spawning_range,
                                      point_outside_of_spawning_range);
}

std::vector<const environment::map::Lane*> SpawnPositionFinder::GetParallelDrivingLanesAtPosition(
    const mantle_api::Vec3<units::length::meter_t>& position,
    const bool include_own_lane) const
{
    auto lanes = GetLanesAtPosition(position);
    if (lanes.empty())
    {
        return {};
    }

    const auto* first_lane_at_position = lanes.front();
    const auto* parent_lane_group = FindLaneGroup(first_lane_at_position->parent_lane_group_id);

    std::vector<const environment::map::Lane*> parallel_driving_lanes;
    for (mantle_api::UniqueId lane_id : parent_lane_group->lane_ids)
    {
        if (lane_id == first_lane_at_position->id && !include_own_lane)
        {
            continue;
        }

        const auto* lane_ptr = FindLane(lane_id);
        if (IsLaneValidForSpawning(*lane_ptr))
        {
            parallel_driving_lanes.emplace_back(lane_ptr);
        }
    }

    return parallel_driving_lanes;
}

std::vector<const environment::map::Lane*> SpawnPositionFinder::GetLanesAtPosition(
    const mantle_api::Vec3<units::length::meter_t>& position) const
{
    auto lane_location = lane_location_provider.GetLaneLocation(position);
    return lane_location.lanes;
}

const environment::map::LaneGroup* SpawnPositionFinder::FindLaneGroup(mantle_api::UniqueId lane_group_id) const
{
    const auto* lane_group = lane_location_provider.GetAstasMap().FindLaneGroup(lane_group_id);

    ASSERT(lane_group != nullptr &&
           "Could not find parent lane group of a lane in AstasMap. Please contact ASTAS support")

    return lane_group;
}

std::vector<const environment::map::Lane*> SpawnPositionFinder::GetLanesValidForSpawning(
    const std::vector<const environment::map::Lane*>& possible_spawn_lanes,
    const mantle_api::Vec3<units::length::meter_t>& position_at_spawning_distance) const
{
    std::vector<const environment::map::Lane*> lanes_with_no_conflicts;

    for (const auto* lane : possible_spawn_lanes)
    {
        const auto projected_centerline_point = std::get<0>(
            environment::map::ProjectQueryPointOnPolyline(position_at_spawning_distance, lane->center_line));

        if (IsLaneAtPositionInConflictWithOccupiedLanes(*lane, projected_centerline_point))
        {
            lanes_with_no_conflicts.emplace_back(lane);
            occupied_positions_.emplace_back(projected_centerline_point);
        }
    }

    return lanes_with_no_conflicts;
}

inline std::size_t SpawnPositionFinder::GetNumberOfLanes(const std::optional<LanesAtPosition>& lanes_at_position)
{
    if (lanes_at_position.has_value())
    {
        return lanes_at_position->lanes.size();
    }
    return 0;
}

std::size_t SpawnPositionFinder::GetRandomIndex(const std::size_t number_of_elements)
{
    std::uniform_int_distribution<std::size_t> distribution{0, number_of_elements - 1};

    return distribution(random_number_generator_);
}

SpawnPositionFinder::SearchDirection SpawnPositionFinder::GetLaneDirection(const std::size_t lane_index,
                                                                           const std::size_t num_lanes_in_front) const
{
    return (lane_index < num_lanes_in_front) ? SearchDirection::kInFrontOfTheHost : SearchDirection::kBehindTheHost;
}

inline const environment::map::Lane* SpawnPositionFinder::GetSuccessorLane(
    const environment::map::Lane* current_lane) const
{
    return current_lane->successors.empty() ? nullptr
                                            : lane_location_provider.FindLane(current_lane->successors.front());
}

inline const environment::map::Lane* SpawnPositionFinder::GetPredecessorLane(
    const environment::map::Lane* current_lane) const
{
    return current_lane->predecessors.empty() ? nullptr
                                              : lane_location_provider.FindLane(current_lane->predecessors.front());
}

const environment::map::Lane* SpawnPositionFinder::FindLane(mantle_api::UniqueId lane_id) const
{
    const auto* lane = lane_location_provider.FindLane(lane_id);

    ASSERT(lane != nullptr &&
           "Could not find lane referenced by a lane group in AstasMap. Please contact ASTAS support")

    return lane;
}

bool SpawnPositionFinder::IsLaneAtPositionInConflictWithOccupiedLanes(
    const environment::map::Lane& lane,
    const mantle_api::Vec3<units::length::meter_t>& position_projected_on_lane) const
{
    return (IsPositionOnLane(position_projected_on_lane, lane.id) &&
            !IsPositionTooCloseToExistingVehicles(position_projected_on_lane, lane));
}

bool SpawnPositionFinder::IsPositionTooCloseToExistingVehicles(const mantle_api::Vec3<units::length::meter_t>& position,
                                                               const environment::map::Lane& lane) const
{
    return std::any_of(
        occupied_positions_.cbegin(),
        occupied_positions_.cend(),
        [this, &lane, &position](const auto& occupied_position) {
            return (IsPositionInLaneOrItsPredecessorsAndSuccessors(occupied_position, lane) &&
                    ArePositionsWithinDistance(position, occupied_position, distance_between_spawned_vehicles));
        });
}

bool SpawnPositionFinder::IsPositionInLaneOrItsPredecessorsAndSuccessors(
    const mantle_api::Vec3<units::length::meter_t>& position,
    const environment::map::Lane& target_lane) const
{
    const auto lanes = GetLanesAtPosition(position);

    const auto is_target_lane_id = [target_lane](const mantle_api::UniqueId lane_id) {
        return lane_id == target_lane.id;
    };

    return std::any_of(lanes.cbegin(), lanes.cend(), [&is_target_lane_id](const auto* lane) {
        return (is_target_lane_id(lane->id) ||
                std::any_of(lane->successors.cbegin(), lane->successors.cend(), is_target_lane_id) ||
                std::any_of(lane->predecessors.cbegin(), lane->predecessors.cend(), is_target_lane_id));
    });
}

bool SpawnPositionFinder::IsPositionOnLane(const mantle_api::Vec3<units::length::meter_t>& position,
                                           const mantle_api::UniqueId target_lane_id) const
{
    const auto lanes = GetLanesAtPosition(position);

    return std::any_of(
        lanes.cbegin(), lanes.cend(), [&target_lane_id](const auto* lane) { return target_lane_id == lane->id; });
}

}  // namespace astas::environment::traffic_swarm
