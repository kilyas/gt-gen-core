/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_TPMSTUCKDETECTION_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_TPMSTUCKDETECTION_H

#include "Core/Environment/Map/Common/converter_utility.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <map>

namespace astas::environment::traffic_swarm
{

class DogStuckDetection
{
  public:
    bool IsStuck(
        const mantle_api::IEntity* entity,
        map::IConverter<map::UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>* coord_converter);

    const mantle_api::Time timeout{20'000};

  private:
    bool IsDog(const mantle_api::IEntity* entity) const;
    bool HasNotMoved(const mantle_api::Vec3<units::length::meter_t>& old_pos,
                     const mantle_api::Vec3<units::length::meter_t>& new_pos) const;
    bool IsTimeout(const mantle_api::Time& current_time, const mantle_api::Time& last_move_time) const;

    std::map<mantle_api::UniqueId, std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Time>> stuck_map_{};
};

}  // namespace astas::environment::traffic_swarm

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_TPMSTUCKDETECTION_H
