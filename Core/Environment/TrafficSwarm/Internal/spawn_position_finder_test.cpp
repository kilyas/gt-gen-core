/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/spawn_position_finder.h"

#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MapUtils/set_lane_flags.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::traffic_swarm
{
using units::literals::operator""_m;

class SpawnPositionFinderTest : public testing::Test
{
  public:
    SpawnPositionFinderTest() = default;
    SpawnPositionFinderTest(SpawnPositionFinderTest const&) = delete;
    SpawnPositionFinderTest& operator=(const SpawnPositionFinderTest&) = delete;
    SpawnPositionFinderTest(SpawnPositionFinderTest&&) = delete;
    SpawnPositionFinderTest& operator=(SpawnPositionFinderTest&&) = delete;

    ~SpawnPositionFinderTest() override { test_utils::MapCatalogueIdProvider::Instance().Reset(); }

  protected:
    void SetupMapAndLaneLocationProvider(std::unique_ptr<environment::map::AstasMap> map)
    {
        map_ = std::move(map);
        lane_location_provider_ = std::make_unique<environment::map::LaneLocationProvider>(*map_);
    }

    void ExpectTrafficPositionsAreAtSpawningDistance(
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        const double host_velocity,
        const double spawning_distance,
        std::vector<mantle_api::Vec3<units::length::meter_t>>&& occupied_positions,
        const std::size_t max_spawning_positions,
        const bool use_trm)
    {
        const auto positions_and_directions = FindTrafficPositionAndSearchDirection(host_position,
                                                                                    host_velocity,
                                                                                    spawning_distance,
                                                                                    std::move(occupied_positions),
                                                                                    max_spawning_positions,
                                                                                    use_trm);
        ASSERT_NE(0, positions_and_directions.size());

        for (const auto& [traffic_position, direction] : positions_and_directions)
        {
            EXPECT_NEAR(spawning_distance, std::abs(host_position.x() - traffic_position.x()), 1)
                << (direction == SpawnPositionFinder::SearchDirection::kInFrontOfTheHost ? "InFront" : "Behind");
        }
    }

    std::vector<SpawnPositionFinder::PositionAndSearchDirection> FindTrafficPositionAndSearchDirection(
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        const double host_velocity,
        const double spawning_distance,
        std::vector<mantle_api::Vec3<units::length::meter_t>>&& occupied_positions,
        const std::size_t max_spawning_positions,
        const bool use_trm)
    {
        auto spawn_position_finder = CreateSpawnPositionFinder(spawning_distance, use_trm);
        return spawn_position_finder.TryFindSpawningPositionsAndDirections(
            host_position, host_velocity, max_spawning_positions, std::move(occupied_positions));
    }

    SpawnPositionFinder CreateSpawnPositionFinder(const double spawning_distance, const bool use_trm)
    {
        const TrafficSwarmParameters auto_spawn_parameters{
            default_number_of_vehicles, spawning_distance, {traffic_min_velocity, traffic_max_velocity}, use_trm};

        return {distance_between_auto_spawn_vehicles, *map_, auto_spawn_parameters};
    }

    void ExpectTrafficPositionIsFoundInDirection(
        SpawnPositionFinder::SearchDirection direction,
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        const double host_velocity,
        const double spawning_distance,
        std::vector<mantle_api::Vec3<units::length::meter_t>>&& occupied_spawning_positions,
        const bool use_trm)
    {
        const auto positions_and_directions =
            FindTrafficPositionAndSearchDirection(host_position,
                                                  host_velocity,
                                                  spawning_distance,
                                                  std::move(occupied_spawning_positions),
                                                  default_max_spawning_positions,
                                                  use_trm);
        ASSERT_NE(0, positions_and_directions.size());

        const auto traffic_direction = positions_and_directions[0].second;
        EXPECT_EQ(direction, traffic_direction);
    }

    void ExpectTrafficPositionNotFound(
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        const double host_velocity,
        const double spawning_distance,
        std::vector<mantle_api::Vec3<units::length::meter_t>>&& occupied_spawning_positions,
        const bool use_trm)
    {
        const auto positions_and_directions =
            FindTrafficPositionAndSearchDirection(host_position,
                                                  host_velocity,
                                                  spawning_distance,
                                                  std::move(occupied_spawning_positions),
                                                  default_max_spawning_positions,
                                                  use_trm);
        EXPECT_EQ(0, positions_and_directions.size());
    }

    bool IsSpawnPositionFoundOnLane(mantle_api::UniqueId lane_id,
                                    const SpawnPositionFinder::PositionAndSearchDirection& position_and_direction)
    {
        const auto lane_location = lane_location_provider_->GetLaneLocation(position_and_direction.first);
        return lane_location.IsValid() && lane_location.lanes.front()->id == lane_id;
    }

    service::utility::UniqueIdProvider unique_id_provider_;
    std::unique_ptr<environment::map::AstasMap> map_;
    std::unique_ptr<environment::map::LaneLocationProvider> lane_location_provider_;

    // number of vehicles does not affect anything here, but is needed to construct AutoSpawningParameters
    static constexpr std::size_t default_number_of_vehicles{0};

    static constexpr double default_spawning_distance{10.0};
    static constexpr double default_host_velocity{30.0};
    static constexpr double traffic_min_velocity{20.0};
    static constexpr double traffic_max_velocity{40.0};
    static constexpr bool default_use_trm{false};
    static constexpr double distance_between_auto_spawn_vehicles{2.0};
    static constexpr std::size_t default_max_spawning_positions{std::numeric_limits<std::size_t>::max() - 1};
};

// NOLINTNEXTLINE(fuchsia-multiple-inheritance) - accepted: Baseclass used in different testing use-cases
class SpawnPositionFinderDistanceTest : public SpawnPositionFinderTest, public testing::WithParamInterface<double>
{
};

TEST_P(
    SpawnPositionFinderDistanceTest,
    GivenHostInTheMiddleRoadWithThreeParallelLanes_WhenFindingSpawningPosition_ThenSpawningPositionIsAtCorrectDistanceFromHost)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{150_m, 0_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    ExpectTrafficPositionsAreAtSpawningDistance(
        host_position, default_host_velocity, GetParam(), {}, default_max_spawning_positions, default_use_trm);
}

INSTANTIATE_TEST_SUITE_P(SpawningDistanceTest, SpawnPositionFinderDistanceTest, testing::Range(10.0, 110.0, 10.0));

TEST_F(SpawnPositionFinderTest,
       GivenHostIsAtBeginningOfRoadWithSingleLane_WhenFindingSpawningPosition_ThenPositionIsInFrontOfHost)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{0_m, 0_m, 0_m};

    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());

    ExpectTrafficPositionIsFoundInDirection(SpawnPositionFinder::SearchDirection::kInFrontOfTheHost,
                                            host_position,
                                            default_host_velocity,
                                            default_spawning_distance,
                                            {},
                                            default_use_trm);
}

TEST_F(SpawnPositionFinderTest,
       GivenHostIsAtEndOfRoadWithSingleLane_WhenFindingSpawningPosition_ThenNoPositionIsFoundBehindHostOnTheSameLane)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{99_m, 2_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());

    ExpectTrafficPositionNotFound(host_position, default_host_velocity, default_spawning_distance, {}, default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenHostIsAtEndOfRoadWithThreeParallelLanes_WhenFindingSpawningPosition_ThenTwoPositionsAreBehindAndNotInHostLane)
{
    constexpr int expected_spawning_positions{2};
    const mantle_api::Vec3<units::length::meter_t> host_position{297_m, 4_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());
    const auto host_lane_location = lane_location_provider_->GetLaneLocation(host_position);
    const mantle_api::UniqueId host_lane_id = host_lane_location.lanes[0]->id;

    const auto position_and_direction = FindTrafficPositionAndSearchDirection(host_position,
                                                                              default_host_velocity,
                                                                              default_spawning_distance,
                                                                              {},
                                                                              default_max_spawning_positions,
                                                                              default_use_trm);

    ASSERT_EQ(expected_spawning_positions, position_and_direction.size());
    EXPECT_EQ(position_and_direction[0].second, SpawnPositionFinder::SearchDirection::kBehindTheHost);
    EXPECT_EQ(position_and_direction[1].second, SpawnPositionFinder::SearchDirection::kBehindTheHost);
    EXPECT_FALSE(IsSpawnPositionFoundOnLane(host_lane_id, position_and_direction[0]));
    EXPECT_FALSE(IsSpawnPositionFoundOnLane(host_lane_id, position_and_direction[1]));
}

TEST_F(SpawnPositionFinderTest,
       GivenTwoValidSpawnLocations_WhenTwoSpawningPositionsAreFound_ThenFoundPositionsAreAtleastTwoMetersApart)
{
    constexpr int expected_spawning_positions{2};
    const mantle_api::Vec3<units::length::meter_t> host_position{297_m, 4_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    const auto position_and_direction = FindTrafficPositionAndSearchDirection(host_position,
                                                                              default_host_velocity,
                                                                              default_spawning_distance,
                                                                              {},
                                                                              default_max_spawning_positions,
                                                                              default_use_trm);

    const double vehicle_distance =
        service::glmwrapper::Distance(position_and_direction[0].first, position_and_direction[1].first);

    ASSERT_EQ(expected_spawning_positions, position_and_direction.size());
    EXPECT_GT(vehicle_distance, distance_between_auto_spawn_vehicles);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenFivePossibleSpawningPositionsAndFourMaxSpawningPositions_WhenFindingSpawningPosition_ThenFourSpawningPositionsAreFound)
{
    constexpr int max_spawning_positions{4};
    constexpr int expected_spawning_positions{4};
    const mantle_api::Vec3<units::length::meter_t> host_position{100_m, 4_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    const auto position_and_direction = FindTrafficPositionAndSearchDirection(
        host_position, default_host_velocity, default_spawning_distance, {}, max_spawning_positions, default_use_trm);

    ASSERT_EQ(expected_spawning_positions, position_and_direction.size());
}

TEST_F(
    SpawnPositionFinderTest,
    GivenFivePossibleSpawningPositionsAndSixMaxSpawningPositions_WhenFindingSpawningPosition_ThenFiveSpawningPositionsAreFound)
{
    constexpr int max_spawning_positions{6};
    constexpr int expected_spawning_positions{5};
    const mantle_api::Vec3<units::length::meter_t> host_position{100_m, 4_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    const auto position_and_direction = FindTrafficPositionAndSearchDirection(
        host_position, default_host_velocity, default_spawning_distance, {}, max_spawning_positions, default_use_trm);

    ASSERT_EQ(expected_spawning_positions, position_and_direction.size());
}

TEST_F(
    SpawnPositionFinderTest,
    GivenFivePossibleSpawningPositionsAndFiveMaxSpawningPositions_WhenFindingSpawningPosition_ThenFiveSpawningPositionsAreFound)
{
    constexpr int max_spawning_positions{5};
    constexpr int expected_spawning_positions{5};
    const mantle_api::Vec3<units::length::meter_t> host_position{100_m, 4_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    const auto position_and_direction = FindTrafficPositionAndSearchDirection(
        host_position, default_host_velocity, default_spawning_distance, {}, max_spawning_positions, default_use_trm);

    ASSERT_EQ(expected_spawning_positions, position_and_direction.size());
}

TEST_F(SpawnPositionFinderTest,
       GivenOneLaneWithSuccessor_WhenSpawnDistanceOverlapsSuccessorLane_ThenPositionIsInSuccessorLane)
{
    constexpr std::size_t expected_spawning_positions{1};
    const double expected_distance_to_host = 150.0;
    const mantle_api::UniqueId expected_traffic_lane_id = 7;
    const mantle_api::Vec3<units::length::meter_t> host_position{0_m, 0_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach());

    const auto position_and_direction = FindTrafficPositionAndSearchDirection(host_position,
                                                                              default_host_velocity,
                                                                              expected_distance_to_host,
                                                                              {},
                                                                              default_max_spawning_positions,
                                                                              default_use_trm);
    ASSERT_EQ(expected_spawning_positions, position_and_direction.size());
    EXPECT_TRUE(IsSpawnPositionFoundOnLane(expected_traffic_lane_id, position_and_direction[0]));
}

TEST_F(
    SpawnPositionFinderTest,
    GivenOneLaneWithOnePredecessor_WhenSpawnDistanceOverlapsPredecessorLane_ThenNoPositionIsFoundInDirectPredecessorOfHostLane)
{
    const double expected_distance_to_host = 150.0;
    const mantle_api::Vec3<units::length::meter_t> host_position{197_m, 2_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach());

    ExpectTrafficPositionNotFound(host_position, default_host_velocity, expected_distance_to_host, {}, default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenTwoParallelLanesWithPredecessors_WhenSpawnDistanceOverlapsPredecessorLanes_ThenPositionIsInPredecessorLaneThatIsNotDirectlyBehindHostLane)
{
    constexpr std::size_t expected_spawning_positions{1};
    units::length::meter_t y_of_host{4.0};
    const double expected_distance_to_host = 150.0;
    const mantle_api::Vec3<units::length::meter_t> host_position{190_m, y_of_host, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapMergeTwoLanesToTheLeftIntoOneLane());

    const auto position_and_direction = FindTrafficPositionAndSearchDirection(host_position,
                                                                              default_host_velocity,
                                                                              expected_distance_to_host,
                                                                              {},
                                                                              default_max_spawning_positions,
                                                                              default_use_trm);
    ASSERT_EQ(expected_spawning_positions, position_and_direction.size());

    const auto lane_location = lane_location_provider_->GetLaneLocation(position_and_direction[0].first);
    ASSERT_TRUE(lane_location.IsValid());
    EXPECT_NE(lane_location.projected_centerline_point.x, y_of_host);
}

TEST_F(SpawnPositionFinderTest,
       GivenRoadWithThreeParallelLanes_WhenHostVelocityIsGreaterThanMaxTrafficVelocity_ThenPositionIsInFrontOfHost)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{150_m, 6_m, 0_m};
    const double test_host_velocity = 100.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    ExpectTrafficPositionIsFoundInDirection(SpawnPositionFinder::SearchDirection::kInFrontOfTheHost,
                                            host_position,
                                            test_host_velocity,
                                            default_spawning_distance,
                                            {},
                                            default_use_trm);
}

TEST_F(SpawnPositionFinderTest,
       GivenRoadWithThreeParallelLanes_WhenHostVelocityIsLessThanMinTrafficVelocity_ThenPositionIsBehindHost)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{150_m, 6_m, 0_m};
    const double test_host_velocity = 0.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    ExpectTrafficPositionIsFoundInDirection(SpawnPositionFinder::SearchDirection::kBehindTheHost,
                                            host_position,
                                            test_host_velocity,
                                            default_spawning_distance,
                                            {},
                                            default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenHostIsAtEndOfRoadWithThreeParallelLanes_WhenHostVelocityIsGreaterThanMaxTrafficVelocity_ThenNoPositionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{296_m, 0_m, 0_m};
    const double test_host_velocity = 100.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    ExpectTrafficPositionNotFound(host_position, test_host_velocity, default_spawning_distance, {}, default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenHostIsAtBeginningOfRoadWithThreeParallelLanes_WhenHostVelocityIsLessThanMinTrafficVelocity_ThenNoPositionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{1_m, 0_m, 0_m};
    const double test_host_velocity = 0.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());

    ExpectTrafficPositionNotFound(host_position, test_host_velocity, default_spawning_distance, {}, default_use_trm);
}

TEST_F(SpawnPositionFinderTest,
       GivenHostIsAtStartOfShoulderLane_WhenHostVelocityIsGreaterThanMaxTrafficVelocity_ThenNoPositionIsFound)
{
    const mantle_api::UniqueId lane_id{3};
    const mantle_api::Vec3<units::length::meter_t> host_position{1_m, 0_m, 0_m};
    const double test_host_velocity = 100.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());
    test_utils::SetShoulderLaneFlag(lane_id, *map_);

    ExpectTrafficPositionNotFound(host_position, test_host_velocity, default_spawning_distance, {}, default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenHostIsAtEndOfRoadWithOneDrivingAndTwoParallelShoulderLanesOnEachSide_WhenHostVelocityIsLessThanMinTrafficVelocity_ThenNoPositionIsFound)
{
    const std::array<mantle_api::UniqueId, 6> shoulder_lane_ids{2, 8, 12, 18, 22, 28};
    const mantle_api::Vec3<units::length::meter_t> host_position{296_m, 6_m, 0_m};
    const double test_host_velocity = 0.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());
    for (const mantle_api::UniqueId id : shoulder_lane_ids)
    {
        test_utils::SetShoulderLaneFlag(id, *map_);
    }

    ExpectTrafficPositionNotFound(host_position, test_host_velocity, default_spawning_distance, {}, default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenHostIsAtEndOfRoadWithOneDrivingAndTwoParallelMergeLanesOnEachSide_WhenHostVelocityIsLessThanMinTrafficVelocity_ThenNoPositionIsFound)
{
    const std::array<mantle_api::UniqueId, 6> merge_lane_ids{2, 8, 12, 18, 22, 28};
    const mantle_api::Vec3<units::length::meter_t> host_position{296_m, 6_m, 0_m};
    const double test_host_velocity = 0.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());
    for (const mantle_api::UniqueId id : merge_lane_ids)
    {
        test_utils::SetMergeLaneFlag(id, *map_);
    }

    ExpectTrafficPositionNotFound(host_position, test_host_velocity, default_spawning_distance, {}, default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenHostIsAtEndOfRoadWithOneDrivingAndTwoParallelSplitLanesOnEachSide_WhenHostVelocityIsLessThanMinTrafficVelocity_ThenNoPositionIsFound)
{
    const std::array<mantle_api::UniqueId, 6> merge_lane_ids{2, 8, 12, 18, 22, 28};
    const mantle_api::Vec3<units::length::meter_t> host_position{296_m, 6_m, 0_m};
    const double test_host_velocity = 0.0;
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints());
    for (const mantle_api::UniqueId id : merge_lane_ids)
    {
        test_utils::SetSplitLaneFlag(id, *map_);
    }

    ExpectTrafficPositionNotFound(host_position, test_host_velocity, default_spawning_distance, {}, default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenOneOccupiedPosition_WhenDistanceBetweenOccupiedAndSpawnPositionIsLessOrEqualToMinimumDistanceBetweenVehicles_ThenPositionIsNotFound)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{0_m, 0_m, 0_m};
    std::vector<mantle_api::Vec3<units::length::meter_t>> occupied_positions{{8_m, 0_m, 0_m}};
    const double test_spawning_distance{occupied_positions[0].x() + distance_between_auto_spawn_vehicles / 2};

    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());

    ExpectTrafficPositionNotFound(
        host_position, default_host_velocity, test_spawning_distance, std::move(occupied_positions), default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenOneOccupiedPosition_WhenDistanceBetweenOccupiedAndSpawnPositionIsGreaterThanMinimumDistanceBetweenVehicles_ThenPositionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{0_m, 0_m, 0_m};
    std::vector<mantle_api::Vec3<units::length::meter_t>> occupied_positions{{8_m, 0_m, 0_m}};
    const double distance_from_occupied_to_spawning_position{distance_between_auto_spawn_vehicles + 1e-4};
    const double test_spawning_distance{occupied_positions[0].x() + distance_from_occupied_to_spawning_position};

    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());

    ExpectTrafficPositionsAreAtSpawningDistance(host_position,
                                                default_host_velocity,
                                                test_spawning_distance,
                                                std::move(occupied_positions),
                                                default_max_spawning_positions,
                                                default_use_trm);
}

/// Following two tests are for situations that appear on NDS, where, for split and merge lanes, lane centerline is
/// partially outside the lane boundaries. Spawning vehicles on part of the centerline outside lane boundaries cause
/// issues in LaneKeeping movement. To avoid this issue, vehicles are prevented from spawning here.
TEST_F(
    SpawnPositionFinderTest,
    GivenSplitLaneWithPartOfCenterlineOutsideLaneBoundaries_WhenFindingSpawningPositionOnCenterlineOutsideLaneBoundaries_ThenPositionIsNotFound)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{50_m, 0_m, 0_m};
    const double test_host_velocity{0};
    const double test_spawning_distance{40};
    // Normal lane is already blocked by host vehicle for vehicles that spawn behind the host. No need for to occupy it.
    std::vector<mantle_api::Vec3<units::length::meter_t>> occupied_positions{};

    test_utils::RawMapBuilder builder;

    builder.AddNewLaneGroup();
    builder.AddFromCatalogue(test_utils::LaneCatalogue::EastingSplittingTriangularLeftLaneWithHundredPoints());
    builder.AddFromCatalogue(test_utils::LaneCatalogue::StraightEastingLaneWithHundredPoints({-4_m, 0_m, 0_m}));

    map_ = builder.Build();

    ExpectTrafficPositionNotFound(
        host_position, test_host_velocity, test_spawning_distance, std::move(occupied_positions), default_use_trm);
}

TEST_F(
    SpawnPositionFinderTest,
    GivenMergeLaneWithPartOfCenterlineOutsideLaneBoundaries_WhenFindingSpawningPositionOnCenterlineOutsideLaneBoundaries_ThenPositionIsNotFound)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{50_m, 0_m, 0_m};
    const double test_host_velocity{100};
    const double test_spawning_distance{40};
    // Occupy the spawning position in normal lane, so that vehicles in front of host can only be spawned in merge lane.
    std::vector<mantle_api::Vec3<units::length::meter_t>> occupied_positions{{90_m, -4_m, 0_m}};

    test_utils::RawMapBuilder builder;

    builder.AddNewLaneGroup();
    builder.AddFromCatalogue(test_utils::LaneCatalogue::EastingMergingTriangularLeftLaneWithHundredPoints());
    builder.AddFromCatalogue(test_utils::LaneCatalogue::StraightEastingLaneWithHundredPoints({-4_m, 0_m, 0_m}));

    map_ = builder.Build();

    ExpectTrafficPositionNotFound(
        host_position, test_host_velocity, test_spawning_distance, std::move(occupied_positions), default_use_trm);
}

TEST_F(SpawnPositionFinderTest,
       GivenHostIsAtEndOfRoadWithSingleLane_WhenFindingSpawningPositionForDOG_ThenPositionIsFoundBehindHost)
{
    const mantle_api::Vec3<units::length::meter_t> host_position{99_m, 2_m, 0_m};
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPoints());
    const bool use_trm{true};

    ExpectTrafficPositionIsFoundInDirection(SpawnPositionFinder::SearchDirection::kBehindTheHost,
                                            host_position,
                                            default_host_velocity,
                                            default_spawning_distance,
                                            {},
                                            use_trm);
}

TEST_F(SpawnPositionFinderTest,
       GivenLongLaneDefinedByTwoPoints_WhenFindingSpawnPositions_ThenTwoPositionsAreFoundAtTheExpectedRange)
{
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints());

    const mantle_api::Vec3<units::length::meter_t> host_position{945_m, 0_m, 0_m};
    const bool use_trm{true};
    const double spawning_distance = 200;
    const double host_velocity = 33;
    const int max_spawn_positions = 10;

    SpawnPositionFinder spawn_position_finder = CreateSpawnPositionFinder(spawning_distance, use_trm);
    const auto found_spawn_positions = spawn_position_finder.TryFindSpawningPositionsAndDirections(
        host_position, host_velocity, max_spawn_positions, {});

    ASSERT_EQ(found_spawn_positions.size(), 2);

    const auto [first_position, first_direction] = found_spawn_positions.front();
    const auto [second_position, second_direction] = found_spawn_positions.back();

    EXPECT_EQ(first_direction, SpawnPositionFinder::SearchDirection::kInFrontOfTheHost);
    EXPECT_EQ(second_direction, SpawnPositionFinder::SearchDirection::kBehindTheHost);

    EXPECT_TRIPLE_NEAR(mantle_api::Vec3<units::length::meter_t>(1145_m, 0_m, 0_m), first_position, 1e-10)
    EXPECT_TRIPLE_NEAR(mantle_api::Vec3<units::length::meter_t>(745_m, 0_m, 0_m), second_position, 1e-10)
}

TEST_F(SpawnPositionFinderTest, GivenMapWhereAllLanesOverlap_WhenFindingSpawnPositions_ThenNoValidPositionsAreFound)
{
    SetupMapAndLaneLocationProvider(test_utils::MapCatalogue::MapWithTwoOverlappingEastingLaneWithHundredPointsEach());

    const double spawning_distance = 20;
    const bool use_trm{true};

    SpawnPositionFinder spawn_position_finder = CreateSpawnPositionFinder(spawning_distance, use_trm);

    const mantle_api::Vec3<units::length::meter_t> host_position{50_m, 0_m, 0_m};
    const double host_velocity = 5;
    const int max_spawn_positions = 10;
    const auto found_spawn_positions = spawn_position_finder.TryFindSpawningPositionsAndDirections(
        host_position, host_velocity, max_spawn_positions, {});

    ASSERT_TRUE(found_spawn_positions.empty());
}

}  // namespace astas::environment::traffic_swarm
