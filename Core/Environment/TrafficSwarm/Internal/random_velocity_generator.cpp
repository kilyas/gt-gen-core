/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/random_velocity_generator.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Common/floating_point_helper.h>

#include <random>

namespace astas::environment::traffic_swarm
{

RandomVelocityGenerator::RandomVelocityGenerator(const double min_velocity, const double max_velocity)
    : min_possible_velocity{min_velocity}, max_possible_velocity{max_velocity}
{
    if (min_velocity > max_velocity)
    {
        LogAndThrow(environment::EnvironmentException(
            "Invalid bounds: lower bound {}, upper bound {}", min_velocity, max_velocity));
    }

    // Calculates sigma so that the given range (e.g. 60 km/h to 250 km/h) is actually the range
    // [mean - 3 * sigma, mean + 3 * sigma]. This way there is a >99% probability that a random number
    // will be in the given range.
    const double stddev = (max_velocity - min_velocity) / 6.0;
    const double mean = (max_velocity + min_velocity) / 2.0;

    velocity_distribution_ = std::normal_distribution(mean,
                                                      mantle_api::AlmostEqual(stddev, 0.0, MANTLE_API_DEFAULT_EPS, true)
                                                          ? std::numeric_limits<double>::min()
                                                          : stddev);
}

double RandomVelocityGenerator::GenerateVelocityInsideBounds(const double lower_bound, const double upper_bound)
{
    if (lower_bound > upper_bound)
    {
        LogAndThrow(environment::EnvironmentException{
            "Invalid bounds: lower bound {}, upper bound {}", lower_bound, upper_bound});
    }
    if (mantle_api::AlmostEqual(lower_bound, upper_bound, MANTLE_API_DEFAULT_EPS, true))
    {
        return lower_bound;
    }
    double velocity = GenerateVelocity();
    while (velocity < lower_bound || velocity > upper_bound)
    {
        velocity = GenerateVelocity();
    }

    return velocity;
}

double RandomVelocityGenerator::GenerateVelocity()
{
    return std::clamp(velocity_distribution_(random_number_generator_), min_possible_velocity, max_possible_velocity);
}

}  // namespace astas::environment::traffic_swarm
