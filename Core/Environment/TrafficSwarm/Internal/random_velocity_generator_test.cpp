/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/random_velocity_generator.h"

#include "Core/Environment/Exception/exception.h"

#include <gtest/gtest.h>

namespace astas::environment::traffic_swarm
{

TEST(RandomVelocytiGeneratorTest, GivenInvalidBounds_WhenVelocityGeneratorIsConstructed_ThenInvalidBoundsErrorIsThrown)
{
    const double velocity_lower_bound = 40.0;
    const double velocity_upper_bound = 20.0;
    ASSERT_THROW(RandomVelocityGenerator(velocity_lower_bound, velocity_upper_bound),
                 environment::EnvironmentException);
}

TEST(RandomVelocytiGeneratorTest, GivenValidBounds_WhenVelocityIsGenerated_ThenVelocityIsInsideBounds)
{
    double velocity_lower_bound = 20.0;
    double velocity_upper_bound = 40.0;
    const double velocity_step_size = 0.5;

    while (velocity_upper_bound <= 60.0)
    {
        RandomVelocityGenerator velocity_generator{velocity_lower_bound, velocity_upper_bound};

        const auto traffic_vehicle_velocity =
            velocity_generator.GenerateVelocityInsideBounds(velocity_lower_bound, velocity_upper_bound);

        ASSERT_LE(velocity_lower_bound, traffic_vehicle_velocity);
        ASSERT_GE(velocity_upper_bound, traffic_vehicle_velocity);

        velocity_lower_bound += velocity_step_size;
        velocity_upper_bound += velocity_step_size;
    }
}

TEST(RandomVelocytiGeneratorTest,
     GivenMultipleGeneratorsWithSameBounds_WhenVelocityIsGenerated_ThenTheyProduceTheSameSequenceOfVelocities)
{
    const std::size_t number_of_sequences{100};
    double velocity_lower_bound = 20.0;
    double velocity_upper_bound = 40.0;

    std::array<RandomVelocityGenerator, 2> generators{
        RandomVelocityGenerator{velocity_lower_bound, velocity_upper_bound},
        RandomVelocityGenerator{velocity_lower_bound, velocity_upper_bound}};

    for (std::size_t i{0}; i < number_of_sequences; ++i)
    {
        EXPECT_EQ(generators[0].GenerateVelocityInsideBounds(velocity_lower_bound, velocity_upper_bound),
                  generators[1].GenerateVelocityInsideBounds(velocity_lower_bound, velocity_upper_bound));
    }
}

TEST(RandomVelocytiGeneratorTest, GivenEqualBounds_WhenGeneratingVelocity_ThenVelocityIsEqualToBoundValues)
{
    const double velocity_lower_bound = 20.0;
    const double velocity_upper_bound = 40.0;
    RandomVelocityGenerator velocity_generator{velocity_lower_bound, velocity_upper_bound};

    ASSERT_EQ(30.0, velocity_generator.GenerateVelocityInsideBounds(30.0, 30.0));
}

TEST(RandomVelocytiGeneratorTest,
     GivenBoundsOutsideMinMaxValues_WhenGeneratingVelocity_ThenVelocityIsInsideMinMaxValues)
{
    const double velocity_min = 30.0;
    const double velocity_max = 30.0;
    const double velocity_lower_bound = 20.0;
    const double velocity_upper_bound = 40.0;
    RandomVelocityGenerator velocity_generator{velocity_min, velocity_max};

    ASSERT_EQ(30.0, velocity_generator.GenerateVelocityInsideBounds(velocity_lower_bound, velocity_upper_bound));
}

}  // namespace astas::environment::traffic_swarm
