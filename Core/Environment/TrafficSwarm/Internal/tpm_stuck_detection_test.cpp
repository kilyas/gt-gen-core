/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/tpm_stuck_detection.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Service/Utility/clock.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::environment::traffic_swarm
{
using units::literals::operator""_m;
using units::literals::operator""_ms;

std::unique_ptr<entities::VehicleEntity> CreateEntity(mantle_api::UniqueId id,
                                                      const mantle_api::Vec3<units::length::meter_t>& pos)
{
    auto entity = std::make_unique<entities::VehicleEntity>(id, "dog_vehicle");
    entity->SetProperties(std::make_unique<mantle_api::VehicleProperties>());
    entity->GetProperties()->properties["external"] = "dog";
    entity->SetPosition(pos);
    return entity;
}

TEST(TpmStuckDetectionTest, GivenEntityFirstTracked_WhenIsStuck_ThenReturnFalse)
{
    service::utility::Clock::Instance().SetNow(0_ms);

    auto entity = CreateEntity(1, {0.0_m, 0.0_m, 0.0_m});
    DogStuckDetection stuck_detection;
    EXPECT_FALSE(stuck_detection.IsStuck(entity.get(), nullptr));
}

TEST(TpmStuckDetectionTest, GivenEntityMoved_WhenIsStuck_ThenReturnFalse)
{
    service::utility::Clock::Instance().SetNow(0_ms);

    auto entity = CreateEntity(1, {0.0_m, 0.0_m, 0.0_m});
    DogStuckDetection stuck_detection;
    ASSERT_FALSE(stuck_detection.IsStuck(entity.get(), nullptr));

    entity->SetPosition({10.0_m, 0.0_m, 0.0_m});
    EXPECT_FALSE(stuck_detection.IsStuck(entity.get(), nullptr));
}

TEST(TpmStuckDetectionTest, GivenEntityStuckForLessThanTimeout_WhenIsStuck_ThenReturnFalse)
{
    service::utility::Clock::Instance().SetNow(0_ms);

    auto entity = CreateEntity(1, {0.0_m, 0.0_m, 0.0_m});
    DogStuckDetection stuck_detection;
    ASSERT_FALSE(stuck_detection.IsStuck(entity.get(), nullptr));

    service::utility::Clock::Instance().SetNow(mantle_api::Time{stuck_detection.timeout - 10_ms});
    EXPECT_FALSE(stuck_detection.IsStuck(entity.get(), nullptr));
}

TEST(TpmStuckDetectionTest, GivenEntityStuckForTimeout_WhenIsStuck_ThenReturnTrue)
{
    service::utility::Clock::Instance().SetNow(0_ms);

    auto entity = CreateEntity(1, {0.0_m, 0.0_m, 0.0_m});
    DogStuckDetection stuck_detection;
    ASSERT_FALSE(stuck_detection.IsStuck(entity.get(), nullptr));

    service::utility::Clock::Instance().SetNow(stuck_detection.timeout);
    EXPECT_TRUE(stuck_detection.IsStuck(entity.get(), nullptr));
}

TEST(TpmStuckDetectionTest, GivenEntityStuckForMoreThanTimeout_WhenIsStuck_ThenReturnTrue)
{
    service::utility::Clock::Instance().SetNow(0_ms);

    auto entity = CreateEntity(1, {0.0_m, 0.0_m, 0.0_m});
    DogStuckDetection stuck_detection;
    ASSERT_FALSE(stuck_detection.IsStuck(entity.get(), nullptr));

    service::utility::Clock::Instance().SetNow(mantle_api::Time{stuck_detection.timeout + 10_ms});
    EXPECT_TRUE(stuck_detection.IsStuck(entity.get(), nullptr));
}

TEST(TpmStuckDetectionTest, GivenOneEntityStuckAndOneMoving_WhenIsStuck_ThenBothEntitiesClassifiedCorrectly)
{
    service::utility::Clock::Instance().SetNow(0_ms);

    auto stuck_entity = CreateEntity(1, {0.0_m, 0.0_m, 0.0_m});
    auto moving_entity = CreateEntity(2, {10.0_m, 0.0_m, 0.0_m});
    DogStuckDetection stuck_detection;

    service::utility::Clock::Instance().SetNow(mantle_api::Time{stuck_detection.timeout});
    EXPECT_FALSE(stuck_detection.IsStuck(moving_entity.get(), nullptr));
    EXPECT_TRUE(stuck_detection.IsStuck(stuck_entity.get(), nullptr));
}

}  // namespace astas::environment::traffic_swarm
