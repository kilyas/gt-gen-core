/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_SPAWNPOSITIONFINDER_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_SPAWNPOSITIONFINDER_H

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Environment/TrafficSwarm/Internal/traffic_swarm_parameters.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Execution/i_environment.h>

#include <optional>
#include <random>
#include <vector>

namespace astas::environment::traffic_swarm
{

class SpawnPositionFinder
{
  public:
    enum class SearchDirection
    {
        kInFrontOfTheHost = 0,
        kBehindTheHost
    };

    using PositionAndSearchDirection = std::pair<mantle_api::Vec3<units::length::meter_t>, SearchDirection>;

    /// @brief Constructs spawn position finder object
    /// @param[in] distance_between_spawned_vehicles Distance between host and possible spawn position
    /// @param[in] map Needed for the spawn lane's lookup
    /// @param[in] auto_spawn_parameters Configuration of the traffic swarm behaviour
    SpawnPositionFinder(double distance_between_spawned_vehicles,
                        const environment::map::AstasMap&
                            map,  // TODO: this is crap: remove first param, its already in the last one contained
                        TrafficSwarmParameters auto_spawn_parameters);

    /// @brief Finds position on the road at a given distance from host vehicle
    /// @param[in] host_position Position coordinates of the host vehicle
    /// @param[in] host_velocity Velocity vector of the host vehicle
    /// @param[in] max_spawning_positions Maximum count of returned pairs of position and direction
    /// @param[in] occupied_positions Positions which are already occupied by other autospawned vehicles
    /// @return vector of viable spawning positions
    std::vector<SpawnPositionFinder::PositionAndSearchDirection> TryFindSpawningPositionsAndDirections(
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        double host_velocity,
        std::size_t max_spawning_positions,
        std::vector<mantle_api::Vec3<units::length::meter_t>>&& occupied_positions);

  private:
    struct LanesAtPosition
    {
        std::vector<const environment::map::Lane*> lanes;
        mantle_api::Vec3<units::length::meter_t> position;
    };

    std::optional<SpawnPositionFinder::LanesAtPosition> FindLanesAtPositionInFrontOfHost(
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        double host_velocity);

    std::optional<SpawnPositionFinder::LanesAtPosition> FindLanesAtPositionBehindHost(
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        double host_velocity);

    std::vector<SpawnPositionFinder::PositionAndSearchDirection> PickValidSpawnPositions(
        std::size_t max_spawning_positions,
        std::optional<LanesAtPosition>& possible_lanes_behind,
        std::optional<LanesAtPosition>& possible_lanes_front);

    std::optional<LanesAtPosition> GetPositionAndLanesAtSpawningDistance(
        const mantle_api::Vec3<units::length::meter_t>& host_position,
        SearchDirection direction) const;

    static std::size_t GetNumberOfSpawningPositions(const std::optional<LanesAtPosition>& possible_lanes_behind,
                                                    const std::optional<LanesAtPosition>& possible_lanes_front,
                                                    std::size_t max_spawning_positions);

    PositionAndSearchDirection PickPositionInRandomLane(
        std::optional<LanesAtPosition>& lanes_at_spawn_distance_in_front,
        std::optional<LanesAtPosition>& lanes_at_spawn_distance_behind);

    std::optional<mantle_api::Vec3<units::length::meter_t>> FindPositionInFrontOfHost(
        const mantle_api::Vec3<units::length::meter_t>& host_position) const;

    std::optional<mantle_api::Vec3<units::length::meter_t>> FindPositionBehindHost(
        const mantle_api::Vec3<units::length::meter_t>& host_position) const;

    std::vector<const environment::map::Lane*> GetParallelDrivingLanesAtPosition(
        const mantle_api::Vec3<units::length::meter_t>& position,
        bool include_own_lane) const;

    std::vector<const environment::map::Lane*> GetLanesValidForSpawning(
        const std::vector<const environment::map::Lane*>& possible_spawn_lanes,
        const mantle_api::Vec3<units::length::meter_t>& position_at_spawning_distance) const;

    static inline std::size_t GetNumberOfLanes(const std::optional<LanesAtPosition>& lanes_at_position);

    std::size_t GetRandomIndex(std::size_t number_of_elements);

    SearchDirection GetLaneDirection(std::size_t lane_index, std::size_t num_lanes_in_front) const;

    std::vector<const environment::map::Lane*> GetLanesAtPosition(
        const mantle_api::Vec3<units::length::meter_t>& position) const;

    inline const environment::map::Lane* GetSuccessorLane(const environment::map::Lane* current_lane) const;

    inline const environment::map::Lane* GetPredecessorLane(const environment::map::Lane* current_lane) const;

    const environment::map::LaneGroup* FindLaneGroup(mantle_api::UniqueId lane_group_id) const;

    const environment::map::Lane* FindLane(mantle_api::UniqueId lane_id) const;

    bool IsLaneAtPositionInConflictWithOccupiedLanes(
        const environment::map::Lane& lane,
        const mantle_api::Vec3<units::length::meter_t>& position_projected_on_lane) const;

    bool IsPositionTooCloseToExistingVehicles(const mantle_api::Vec3<units::length::meter_t>& position,
                                              const environment::map::Lane& lane) const;

    bool IsPositionInLaneOrItsPredecessorsAndSuccessors(const mantle_api::Vec3<units::length::meter_t>& position,
                                                        const environment::map::Lane& target_lane) const;

    bool IsPositionOnLane(const mantle_api::Vec3<units::length::meter_t>& position,
                          mantle_api::UniqueId target_lane_id) const;

    const environment::map::LaneLocationProvider lane_location_provider;
    std::mt19937_64 random_number_generator_{0};

    const TrafficSwarmParameters auto_spawn_parameters;
    const double distance_between_spawned_vehicles;

    mutable std::vector<mantle_api::Vec3<units::length::meter_t>> occupied_positions_;
};

}  // namespace astas::environment::traffic_swarm

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_SPAWNPOSITIONFINDER_H
