/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_RANDOMVELOCITYGENERATOR_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_RANDOMVELOCITYGENERATOR_H

#include <random>

namespace astas::environment::traffic_swarm
{

class RandomVelocityGenerator
{
  public:
    /// Creates a normal distribution for generating random velocity
    /// @param min_velocity minimum value that can be generated
    /// @param max_velocity maximum value that can be generated
    RandomVelocityGenerator(double min_velocity, double max_velocity);

    /// Generates a random value inside given bounds
    /// @param lower_bound Minimum value of the returned value
    /// @param upper_bound Maximum value of the returned value
    /// @return value between lower_bound and upper_bound
    double GenerateVelocityInsideBounds(double lower_bound, double upper_bound);

  private:
    double GenerateVelocity();

    const double min_possible_velocity;
    const double max_possible_velocity;
    std::mt19937_64 random_number_generator_{0};
    std::normal_distribution<double> velocity_distribution_;
};

}  // namespace astas::environment::traffic_swarm

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_RANDOMVELOCITYGENERATOR_H
