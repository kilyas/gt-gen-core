/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/ray_sphere_intersection.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::traffic_swarm
{
using units::literals::operator""_m;

TEST(RaySphereIntersectionTest,
     GivenRayToRightOfSphereAndPointingToTheRight_WhenQueyingIntersection_ThenNoIntersectionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> sphere_center{0_m, 0_m, 0_m};
    const double sphere_radius{10};
    const mantle_api::Vec3<units::length::meter_t> ray_origin{units::length::meter_t(sphere_radius + 1), 0_m, 0_m};
    const mantle_api::Vec3<units::length::meter_t> ray_direction{1_m, 0_m, 0_m};

    const auto intersection = RaySphereIntersection({ray_origin, ray_direction}, {sphere_center, sphere_radius});

    EXPECT_FALSE(intersection.has_value());
}

TEST(RaySphereIntersectionTest,
     GivenRayToLeftOfSphereAndPointingToTheRight_WhenQueyingIntersection_ThenClosestIntersectionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> sphere_center{0_m, 0_m, 0_m};
    const double sphere_radius{10};
    const mantle_api::Vec3<units::length::meter_t> ray_origin{units::length::meter_t(-sphere_radius - 1), 0_m, 0_m};
    const mantle_api::Vec3<units::length::meter_t> ray_direction{1_m, 0_m, 0_m};

    const auto intersection = RaySphereIntersection({ray_origin, ray_direction}, {sphere_center, sphere_radius});

    const mantle_api::Vec3<units::length::meter_t> expected_intersection_point{
        units::length::meter_t(-sphere_radius), 0_m, 0_m};
    ASSERT_TRUE(intersection.has_value());
    EXPECT_TRIPLE_NEAR(expected_intersection_point, intersection->intersection_point, 1e-16);
}

TEST(RaySphereIntersectionTest,
     GivenRayToRightOfSphereAndPointingToTheLeft_WhenQueyingIntersection_ThenClosestIntersectionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> sphere_center{0_m, 0_m, 0_m};
    const double sphere_radius{10};
    const mantle_api::Vec3<units::length::meter_t> ray_origin{units::length::meter_t(sphere_radius + 1), 0_m, 0_m};
    const mantle_api::Vec3<units::length::meter_t> ray_direction{-1_m, 0_m, 0_m};

    const auto intersection = RaySphereIntersection({ray_origin, ray_direction}, {sphere_center, sphere_radius});

    const mantle_api::Vec3<units::length::meter_t> expected_intersection_point{
        units::length::meter_t(sphere_radius), 0_m, 0_m};
    ASSERT_TRUE(intersection.has_value());
    EXPECT_TRIPLE_NEAR(expected_intersection_point, intersection->intersection_point, 1e-16);
}

TEST(RaySphereIntersectionTest, GivenRayTangentialToSphere_WhenQueyingIntersection_ThenIntersectionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> sphere_center{0_m, 0_m, 0_m};
    const double sphere_radius{10};
    const mantle_api::Vec3<units::length::meter_t> ray_origin{
        units::length::meter_t(-sphere_radius - 1), units::length::meter_t(sphere_radius), 0_m};
    const mantle_api::Vec3<units::length::meter_t> ray_direction{1_m, 0_m, 0_m};

    const auto intersection = RaySphereIntersection({ray_origin, ray_direction}, {sphere_center, sphere_radius});

    const mantle_api::Vec3<units::length::meter_t> expected_intersection_point{
        0_m, units::length::meter_t(sphere_radius), 0_m};
    ASSERT_TRUE(intersection.has_value());
    EXPECT_TRIPLE_NEAR(expected_intersection_point, intersection->intersection_point, 1e-16);
}

TEST(RaySphereIntersectionTest, GivenRayWithOriginAtSpheresBoundary_WhenQueyingIntersection_ThenIntersectionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> sphere_center{0_m, 0_m, 0_m};
    const double sphere_radius{10};
    const mantle_api::Vec3<units::length::meter_t> ray_origin{units::length::meter_t(sphere_radius), 0_m, 0_m};
    const mantle_api::Vec3<units::length::meter_t> ray_direction{1_m, 0_m, 0_m};

    const auto intersection = RaySphereIntersection({ray_origin, ray_direction}, {sphere_center, sphere_radius});

    const mantle_api::Vec3<units::length::meter_t> expected_intersection_point{ray_origin};
    ASSERT_TRUE(intersection.has_value());
    EXPECT_TRIPLE_NEAR(expected_intersection_point, intersection->intersection_point, 1e-16);
}

TEST(RaySphereIntersectionTest, GivenRayWithOriginInsideTheSphere_WhenQueyingIntersection_ThenIntersectionIsFound)
{
    const mantle_api::Vec3<units::length::meter_t> sphere_center{0_m, 0_m, 0_m};
    const double sphere_radius{10};
    const mantle_api::Vec3<units::length::meter_t> ray_origin{0_m, 0_m, 0_m};
    const mantle_api::Vec3<units::length::meter_t> ray_direction{1_m, 0_m, 0_m};

    const auto intersection = RaySphereIntersection({ray_origin, ray_direction}, {sphere_center, sphere_radius});

    const mantle_api::Vec3<units::length::meter_t> expected_intersection_point{
        units::length::meter_t(sphere_radius), 0_m, 0_m};
    ASSERT_TRUE(intersection.has_value());
    EXPECT_TRIPLE_NEAR(expected_intersection_point, intersection->intersection_point, 1e-16);
}

}  // namespace astas::environment::traffic_swarm
