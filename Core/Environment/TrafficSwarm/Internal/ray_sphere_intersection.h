/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_RAYSPHEREINTERSECTION_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_RAYSPHEREINTERSECTION_H

#include <MantleAPI/Common/vector.h>

#include <optional>

namespace astas::environment::traffic_swarm
{

struct Intersection
{
    mantle_api::Vec3<units::length::meter_t> intersection_point{};
    double distance{0};
};

struct Ray
{
    mantle_api::Vec3<units::length::meter_t> origin{};
    mantle_api::Vec3<units::length::meter_t> direction{};
};

struct Sphere
{
    mantle_api::Vec3<units::length::meter_t> center{};
    double radius{0};
};

/// @brief Checks whether the given ray intersects the given sphere. If an intersection is found, returns the
/// intersection point and the distance from the ray origin.
std::optional<Intersection> RaySphereIntersection(const Ray& ray, const Sphere& sphere);

}  // namespace astas::environment::traffic_swarm

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICSWARM_INTERNAL_RAYSPHEREINTERSECTION_H
