/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficSwarm/Internal/ray_sphere_intersection.h"

#include "Core/Service/GlmWrapper/glm_wrapper.h"

namespace astas::environment::traffic_swarm
{

std::optional<Intersection> RaySphereIntersection(const Ray& ray, const Sphere& sphere)
{
    const mantle_api::Vec3<units::length::meter_t> normalized_ray_dir = service::glmwrapper::Normalize(ray.direction);
    const mantle_api::Vec3<units::length::meter_t> l = sphere.center - ray.origin;
    const double tca = service::glmwrapper::Dot(l, normalized_ray_dir);

    const double d2 = service::glmwrapper::Dot(l, l) - tca * tca;
    const double radius_squared = sphere.radius * sphere.radius;
    if (d2 > radius_squared)
    {
        return std::nullopt;
    }

    const double thc = sqrt(radius_squared - d2);

    double t0 = tca - thc;
    double t1 = tca + thc;

    if (t0 > t1)
    {
        std::swap(t0, t1);
    }

    if (t0 < 0)
    {
        t0 = t1;
        if (t0 < 0)
        {
            return std::nullopt;
        }
    }

    const double t = t0;

    const mantle_api::Vec3<units::length::meter_t> intersection_point = ray.origin + normalized_ray_dir * t;

    return Intersection{intersection_point, t};
}

}  // namespace astas::environment::traffic_swarm
