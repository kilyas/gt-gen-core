/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_action_builder.h"

#include <gtest/gtest.h>

namespace astas::environment::traffic_command
{
using units::literals::operator""_mps;
using units::literals::operator""_m;

TEST(TrafficActionBuilderTest, GivenUnsupportedControlStrategy_WhenCallConvertToTrafficAction_ThenNoValueReturned)
{
    mantle_api::KeepVelocityControlStrategy control_strategy;
    auto result = TrafficActionBuilder::ConvertToTrafficAction(&control_strategy);

    EXPECT_FALSE(result.has_value());
}

TEST(TrafficActionBuilderTest,
     GivenFollowVelocitySplineControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithSpeedActionReturned)
{
    mantle_api::FollowVelocitySplineControlStrategy control_strategy;
    control_strategy.default_value = 10_mps;
    auto result = TrafficActionBuilder::ConvertToTrafficAction(&control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_speed_action());
    EXPECT_EQ(control_strategy.default_value(), result.value().speed_action().absolute_target_speed());
}

TEST(TrafficActionBuilderTest,
     GivenAcquireLaneOffsetControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithLaneOffsetActionReturned)
{
    mantle_api::AcquireLaneOffsetControlStrategy control_strategy;
    control_strategy.offset = 1_m;
    auto result = TrafficActionBuilder::ConvertToTrafficAction(&control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_lane_offset_action());
    EXPECT_EQ(control_strategy.offset(), result.value().lane_offset_action().target_lane_offset());
}

TEST(TrafficActionBuilderTest,
     GivenCommandTypeAndCommand_WhenCallCreateCustomTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    std::string command_type = "route";
    std::string command = "5,6,7,8";
    auto result = TrafficActionBuilder::CreateCustomTrafficAction(command_type, command);

    ASSERT_TRUE(result.has_custom_action());
    EXPECT_EQ(command_type, result.custom_action().command_type());
    EXPECT_EQ(command, result.custom_action().command());
}

}  // namespace astas::environment::traffic_command
