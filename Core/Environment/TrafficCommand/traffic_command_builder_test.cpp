/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_command_builder.h"

#include <gtest/gtest.h>

namespace astas::environment::traffic_command
{

class TrafficCommandBuilderTest : public testing::Test
{
  public:
    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> GetOneSupportedControlStrategy()
    {
        auto control_strategy = std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>();
        std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
        control_strategies.push_back(control_strategy);
        return control_strategies;
    }

  protected:
    TrafficCommandBuilder traffic_command_builder_{};
};

TEST_F(TrafficCommandBuilderTest, GivenBuilder_WhenCallStepWithoutPreviousAddControlStrategies_ThenTrafficCommandsEmpty)
{
    traffic_command_builder_.Step();

    EXPECT_EQ(0, traffic_command_builder_.GetTrafficCommands().size());
}

TEST_F(TrafficCommandBuilderTest,
       GivenSupportedControlStrategy_WhenCallAddControlStrategiesWithoutStep_ThenTrafficCommandsEmpty)
{
    traffic_command_builder_.AddControlStrategiesForEntity(42, GetOneSupportedControlStrategy());

    EXPECT_EQ(0, traffic_command_builder_.GetTrafficCommands().size());
}

TEST_F(TrafficCommandBuilderTest,
       GivenSupportedControlStrategy_WhenCallAddControlStrategiesAndStepTwice_ThenTrafficCommandsEmpty)
{
    traffic_command_builder_.AddControlStrategiesForEntity(42, GetOneSupportedControlStrategy());
    traffic_command_builder_.Step();
    traffic_command_builder_.Step();

    EXPECT_EQ(0, traffic_command_builder_.GetTrafficCommands().size());
}

TEST_F(TrafficCommandBuilderTest,
       GivenUnsupportedControlStrategy_WhenCallAddControlStrategiesAndStep_ThenTrafficCommandsEmpty)
{
    auto control_strategy = std::make_shared<mantle_api::KeepVelocityControlStrategy>();
    std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies;
    control_strategies.push_back(control_strategy);
    traffic_command_builder_.AddControlStrategiesForEntity(42, control_strategies);
    traffic_command_builder_.Step();

    EXPECT_EQ(0, traffic_command_builder_.GetTrafficCommands().size());
}

TEST_F(TrafficCommandBuilderTest, GivenSupportedControlStrategy_WhenCallAddControlStrategiesAndStep_ThenVersionIsSet)
{
    traffic_command_builder_.AddControlStrategiesForEntity(42, GetOneSupportedControlStrategy());
    traffic_command_builder_.Step();

    ASSERT_EQ(1, traffic_command_builder_.GetTrafficCommands().size());
    ASSERT_TRUE(traffic_command_builder_.GetTrafficCommands().at(0).has_version());
    EXPECT_EQ(3, traffic_command_builder_.GetTrafficCommands().at(0).version().version_major());
    EXPECT_EQ(5, traffic_command_builder_.GetTrafficCommands().at(0).version().version_minor());
    EXPECT_EQ(0, traffic_command_builder_.GetTrafficCommands().at(0).version().version_patch());
}

TEST_F(TrafficCommandBuilderTest, GivenSupportedControlStrategy_WhenCallAddControlStrategiesAndStep_ThenTimestampIsSet)
{
    std::int64_t expected_seconds = 0;
    std::uint32_t expected_nanos = 40000000;
    traffic_command_builder_.AddControlStrategiesForEntity(42, GetOneSupportedControlStrategy());
    traffic_command_builder_.Step();

    ASSERT_EQ(1, traffic_command_builder_.GetTrafficCommands().size());
    ASSERT_TRUE(traffic_command_builder_.GetTrafficCommands().at(0).has_timestamp());
    EXPECT_EQ(expected_seconds, traffic_command_builder_.GetTrafficCommands().at(0).timestamp().seconds());
    EXPECT_EQ(expected_nanos, traffic_command_builder_.GetTrafficCommands().at(0).timestamp().nanos());
}

TEST_F(TrafficCommandBuilderTest,
       GivenSupportedControlStrategy_WhenCallAddControlStrategiesAndStep_ThenTrafficParticipantIdIsSet)
{
    mantle_api::UniqueId expected_traffic_participant_id = 42;
    traffic_command_builder_.AddControlStrategiesForEntity(expected_traffic_participant_id,
                                                           GetOneSupportedControlStrategy());
    traffic_command_builder_.Step();

    ASSERT_EQ(1, traffic_command_builder_.GetTrafficCommands().size());
    ASSERT_TRUE(traffic_command_builder_.GetTrafficCommands().at(0).has_traffic_participant_id());
    EXPECT_EQ(expected_traffic_participant_id,
              traffic_command_builder_.GetTrafficCommands().at(0).traffic_participant_id().value());
}

TEST_F(
    TrafficCommandBuilderTest,
    GivenOneSupportedControlStrategy_WhenCallAddControlStrategiesAndStep_ThenGetReturnsOneTrafficCommandWithOneAction)
{
    traffic_command_builder_.AddControlStrategiesForEntity(42, GetOneSupportedControlStrategy());
    traffic_command_builder_.Step();

    ASSERT_EQ(1, traffic_command_builder_.GetTrafficCommands().size());
    ASSERT_EQ(1, traffic_command_builder_.GetTrafficCommands().at(0).action_size());
}

TEST_F(
    TrafficCommandBuilderTest,
    GivenTwoSupportedControlStrategies_WhenCallAddControlStrategiesAndStep_ThenGetReturnsOneTrafficCommandWithTwoActions)
{
    auto control_strategies = GetOneSupportedControlStrategy();
    auto control_strategy = std::make_shared<mantle_api::FollowVelocitySplineControlStrategy>();
    control_strategies.push_back(control_strategy);
    traffic_command_builder_.AddControlStrategiesForEntity(42, control_strategies);
    traffic_command_builder_.Step();

    ASSERT_EQ(1, traffic_command_builder_.GetTrafficCommands().size());
    EXPECT_EQ(2, traffic_command_builder_.GetTrafficCommands().at(0).action_size());
}

TEST_F(
    TrafficCommandBuilderTest,
    GivenSupportedControlStrategy_WhenCallAddControlStrategiesForTwoEntitiesAndStep_ThenGetReturnsTwoTrafficCommandWithOneActionEach)
{
    traffic_command_builder_.AddControlStrategiesForEntity(0, GetOneSupportedControlStrategy());
    traffic_command_builder_.AddControlStrategiesForEntity(42, GetOneSupportedControlStrategy());
    traffic_command_builder_.Step();

    ASSERT_EQ(2, traffic_command_builder_.GetTrafficCommands().size());
    EXPECT_EQ(1, traffic_command_builder_.GetTrafficCommands().at(0).action_size());
    EXPECT_EQ(1, traffic_command_builder_.GetTrafficCommands().at(1).action_size());
}

TEST_F(
    TrafficCommandBuilderTest,
    GivenEntityIdAndCommandTypeAndCommand_WhenCallAddCustomTrafficActionForEntity_ThenGetReturnsOneTrafficCommandWithOneAction)
{
    mantle_api::UniqueId expected_traffic_participant_id = 0;
    traffic_command_builder_.AddCustomTrafficActionForEntity(
        expected_traffic_participant_id, "command_type", "command");
    traffic_command_builder_.Step();

    ASSERT_EQ(1, traffic_command_builder_.GetTrafficCommands().size());
    EXPECT_EQ(1, traffic_command_builder_.GetTrafficCommands().at(0).action_size());
    EXPECT_EQ(expected_traffic_participant_id,
              traffic_command_builder_.GetTrafficCommands().at(0).traffic_participant_id().value());
}

}  // namespace astas::environment::traffic_command
