/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICACTIONBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICACTIONBUILDER_H

#include "astas_osi_trafficcommand.pb.h"

#include <MantleAPI/Traffic/control_strategy.h>

#include <optional>
#include <string>

namespace astas::environment::traffic_command
{

class TrafficActionBuilder
{
  public:
    TrafficActionBuilder() = default;

    /// @brief Converts a control strategy to the corresponding OSI TrafficAction. If conversion is not
    ///        possible/implemented, no value is returned.
    /// @param control_strategy  The control strategy to be converted
    /// @return  OSI::TrafficAction containing the converted control strategy or no value
    static std::optional<astas_osi3::TrafficAction> ConvertToTrafficAction(
        mantle_api::ControlStrategy* control_strategy);
    static astas_osi3::TrafficAction CreateCustomTrafficAction(const std::string& command_type,
                                                               const std::string& command);
};

}  // namespace astas::environment::traffic_command

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICACTIONBUILDER_H
