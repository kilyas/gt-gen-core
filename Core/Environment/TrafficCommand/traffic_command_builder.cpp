/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_command_builder.h"

#include "Core/Environment/TrafficCommand/traffic_action_builder.h"
#include "Core/Service/Utility/clock.h"

namespace astas::environment::traffic_command
{

void TrafficCommandBuilder::Step()
{
    traffic_commands_to_send_.clear();

    for (auto& traffic_command : traffic_commands_buffer_)
    {
        traffic_commands_to_send_.push_back(traffic_command.second);
    }
    traffic_commands_buffer_.clear();

    SetVersion();
    SetTimestamp();
}

void TrafficCommandBuilder::AddControlStrategiesForEntity(
    mantle_api::UniqueId entity_id,
    const std::vector<std::shared_ptr<mantle_api::ControlStrategy>>& control_strategies)
{
    for (auto& control_strategy : control_strategies)
    {
        auto traffic_action = TrafficActionBuilder::ConvertToTrafficAction(control_strategy.get());
        if (traffic_action.has_value())
        {
            traffic_commands_buffer_[entity_id].mutable_traffic_participant_id()->set_value(entity_id);
            auto action = traffic_commands_buffer_[entity_id].add_action();
            action->CopyFrom(traffic_action.value());
        }
    }
}

const std::vector<astas_osi3::TrafficCommand>& TrafficCommandBuilder::GetTrafficCommands() const
{
    return traffic_commands_to_send_;
}

void TrafficCommandBuilder::AddCustomTrafficActionForEntity(mantle_api::UniqueId entity_id,
                                                            const std::string& command_type,
                                                            const std::string& command)
{
    auto traffic_action = TrafficActionBuilder::CreateCustomTrafficAction(command_type, command);
    auto action = traffic_commands_buffer_[entity_id].add_action();
    action->CopyFrom(traffic_action);
}

void TrafficCommandBuilder::SetVersion()
{
    for (auto& traffic_command : traffic_commands_to_send_)
    {
        traffic_command.mutable_version()->set_version_major(3);
        traffic_command.mutable_version()->set_version_minor(5);
        traffic_command.mutable_version()->set_version_patch(0);
    }
}

void TrafficCommandBuilder::SetTimestamp()
{
    for (auto& traffic_command : traffic_commands_to_send_)
    {
        auto sim_time = service::utility::Clock::Instance().Now();
        traffic_command.mutable_timestamp()->set_seconds(service::utility::Clock::Instance().Seconds(sim_time));
        traffic_command.mutable_timestamp()->set_nanos(service::utility::Clock::Instance().Nanos(sim_time));
    }
}

}  // namespace astas::environment::traffic_command
