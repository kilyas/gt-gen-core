/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICCOMMANDBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICCOMMANDBUILDER_H

#include "astas_osi_trafficcommand.pb.h"

#include <MantleAPI/Traffic/control_strategy.h>

namespace astas::environment::traffic_command
{

class TrafficCommandBuilder
{
  public:
    TrafficCommandBuilder() = default;

    /// @brief Copies the pending traffic commands from the buffer, so they can be retrieved with GetTrafficCommands.
    ///        Has to be called after AddControlStrategiesForEntity and before GetTrafficCommands
    void Step();

    /// @brief Creates an OSI::TrafficCommand for an entity and converts each passed control strategy into an
    ///        OSI::TrafficAction. The traffic command can be retrieved with GetTrafficCommands() after the next Step().
    /// @param entity_id  Id of the entity which should perform the control strategies
    /// @param control_strategies  The control strategies to be added for the respective entity
    void AddControlStrategiesForEntity(
        mantle_api::UniqueId entity_id,
        const std::vector<std::shared_ptr<mantle_api::ControlStrategy>>& control_strategies);

    /// @return Const reference to a vector of astas_osi3::TrafficCommand
    const std::vector<astas_osi3::TrafficCommand>& GetTrafficCommands() const;
    void AddCustomTrafficActionForEntity(mantle_api::UniqueId entity_id,
                                         const std::string& command_type,
                                         const std::string& command);

  private:
    void SetVersion();

    void SetTimestamp();

    // TrafficActionBuilder traffic_action_builder_;
    std::map<mantle_api::UniqueId, astas_osi3::TrafficCommand> traffic_commands_buffer_{};
    std::vector<astas_osi3::TrafficCommand> traffic_commands_to_send_{};
};

}  // namespace astas::environment::traffic_command

#endif  // GTGEN_CORE_ENVIRONMENT_TRAFFICCOMMAND_TRAFFICCOMMANDBUILDER_H
