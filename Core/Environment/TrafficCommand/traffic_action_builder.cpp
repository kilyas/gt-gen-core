/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_action_builder.h"

#include "Core/Service/Logging/logging.h"

namespace astas::environment::traffic_command
{

std::optional<astas_osi3::TrafficAction> TrafficActionBuilder::ConvertToTrafficAction(
    mantle_api::ControlStrategy* control_strategy)
{
    if (auto* follow_velocity_spline_control_strategy =
            dynamic_cast<mantle_api::FollowVelocitySplineControlStrategy*>(control_strategy))
    {
        astas_osi3::TrafficAction traffic_action;
        auto speed_action = traffic_action.mutable_speed_action();
        speed_action->set_absolute_target_speed(follow_velocity_spline_control_strategy->default_value());
        return traffic_action;
    }
    if (auto* acquire_lane_offset_control_strategy =
            dynamic_cast<mantle_api::AcquireLaneOffsetControlStrategy*>(control_strategy))
    {
        astas_osi3::TrafficAction traffic_action;
        auto lane_offset_action = traffic_action.mutable_lane_offset_action();
        lane_offset_action->set_target_lane_offset(acquire_lane_offset_control_strategy->offset());
        return traffic_action;
    }
    Debug("Conversion of control strategy {} to traffic command for external agent not yet supported.",
          control_strategy->type);
    return std::nullopt;
}

astas_osi3::TrafficAction TrafficActionBuilder::CreateCustomTrafficAction(const std::string& command_type,
                                                                          const std::string& command)
{
    astas_osi3::TrafficAction traffic_action;
    auto custom_action = traffic_action.mutable_custom_action();
    custom_action->set_command_type(command_type);
    custom_action->set_command(command);
    return traffic_action;
}

}  // namespace astas::environment::traffic_command
