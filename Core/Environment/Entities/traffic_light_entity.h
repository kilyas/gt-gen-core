/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_TRAFFICLIGHTENTITY_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_TRAFFICLIGHTENTITY_H

#include "Core/Environment/Entities/Internal/base_entity.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"

namespace astas::environment::entities
{

class TrafficLightEntity : public BaseEntity, public mantle_api::IStaticObject
{
  public:
    TrafficLightEntity(mantle_api::UniqueId id, const std::string& name);

    void SetPosition(const mantle_api::Vec3<units::length::meter_t>& position) override;

    mantle_ext::TrafficLightProperties* GetProperties() const override
    {
        return GetPropertiesAs<mantle_ext::TrafficLightProperties>();
    }

    mantle_api::Vec3<units::length::meter_t> GetBulbPosition(mantle_api::UniqueId bulb_id) const;

    void SetInitialStateFromProperties();

    void ChangeTrafficSignalState(const std::string& traffic_signal_state);

  private:
    void SetBulbPositions(const mantle_ext::TrafficLightProperties* properties);
    mantle_api::Vec3<units::length::meter_t> ComputeBulbPosition(
        const mantle_api::Vec3<units::length::meter_t>& traffic_light_position,
        const mantle_api::Orientation3<units::angle::radian_t>& traffic_light_orientation,
        const mantle_api::Dimension3& traffic_light_dimension,
        const mantle_api::Vec3<units::dimensionless::scalar_t>& offset_coeff);

    std::map<mantle_api::UniqueId, mantle_api::Vec3<units::length::meter_t>> bulb_positions_{};
};

}  // namespace astas::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_TRAFFICLIGHTENTITY_H
