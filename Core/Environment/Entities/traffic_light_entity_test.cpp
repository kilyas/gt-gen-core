/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/traffic_light_entity.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/StaticObjects/traffic_light_properties_provider.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::entities
{
using units::literals::operator""_m;

TEST(TrafficLightEntityTest, GivenTrafficLightEntity_WhenSetPosition_ThenPositionIsSet)
{
    mantle_api::Vec3<units::length::meter_t> expected_pos{5_m, 4_m, 1_m};
    TrafficLightEntity traffic_light(1, "traffic_light");
    traffic_light.SetPosition(expected_pos);

    EXPECT_EQ(expected_pos, traffic_light.GetPosition());
}

TEST(TrafficLightEntityTest, GivenTrafficLightEntity_WhenSetPosition_ThenBulbPositionsAreSet)
{
    mantle_api::Vec3<units::length::meter_t> traffic_light_position{5_m, 4_m, 1_m};

    std::vector<mantle_api::Vec3<units::dimensionless::scalar_t>> bulb_positions_offset_coeffs;
    bulb_positions_offset_coeffs.push_back({0, 0, 1. / 3.});
    bulb_positions_offset_coeffs.push_back({0, 0, 0});
    bulb_positions_offset_coeffs.push_back({0, 0, -1. / 3.});

    std::vector<mantle_api::Vec3<units::length::meter_t>> expected_bulb_pos;
    expected_bulb_pos.push_back({traffic_light_position.x, traffic_light_position.y, traffic_light_position.z + 0.5_m});
    expected_bulb_pos.push_back({traffic_light_position.x, traffic_light_position.y, traffic_light_position.z});
    expected_bulb_pos.push_back({traffic_light_position.x, traffic_light_position.y, traffic_light_position.z - 0.5_m});

    TrafficLightEntity traffic_light(1, "traffic_light");
    mantle_ext::TrafficLightProperties properties;
    properties.bounding_box.dimension = {0.5_m, 0.5_m, 1.5_m};
    for (mantle_api::UniqueId i = 0; i < 3; i++)
    {
        mantle_ext::TrafficLightBulbProperties bulb_properties;
        bulb_properties.id = i;
        bulb_properties.position_offset_coeffs = bulb_positions_offset_coeffs.at(i);
        properties.bulbs.push_back(bulb_properties);
    }
    traffic_light.SetProperties(std::make_unique<mantle_ext::TrafficLightProperties>(properties));
    traffic_light.SetPosition(traffic_light_position);

    for (mantle_api::UniqueId i = 0; i < 3; i++)
    {
        EXPECT_TRIPLE(expected_bulb_pos[i], traffic_light.GetBulbPosition(i));
    }
}

TEST(ChangeTrafficSignalStateTest,
     GivenNumberOfLightBulbsDoesNotMatchWithStateString_WhenChangeTrafficSignalState_ThenThrowException)
{
    auto properties = static_objects::TrafficLightPropertiesProvider::Get("1.000.001");

    entities::TrafficLightEntity traffic_light(1, "traffic_light");
    traffic_light.SetProperties(std::move(properties));

    EXPECT_THROW(traffic_light.ChangeTrafficSignalState("Off"), EnvironmentException);
}

TEST(ChangeTrafficSignalStateTest, GivenThreeOffStates_WhenChangeTrafficSignalState_ThenLightModesAreNotChanged)
{
    auto properties = static_objects::TrafficLightPropertiesProvider::Get("1.000.001");

    entities::TrafficLightEntity traffic_light(1, "traffic_light");
    traffic_light.SetProperties(std::move(properties));

    ASSERT_NO_THROW(traffic_light.ChangeTrafficSignalState("Off,Off,Off"));

    auto actual_bulbs = traffic_light.GetProperties()->bulbs;
    EXPECT_EQ(actual_bulbs[0].mode, mantle_api::TrafficLightBulbMode::kOff);
    EXPECT_EQ(actual_bulbs[1].mode, mantle_api::TrafficLightBulbMode::kOff);
    EXPECT_EQ(actual_bulbs[2].mode, mantle_api::TrafficLightBulbMode::kOff);
}

TEST(ChangeTrafficSignalStateTest, GivenThreeDifferentStates_WhenChangeTrafficSignalState_ThenExpectedModesReturned)
{
    auto properties = static_objects::TrafficLightPropertiesProvider::Get("1.000.001");

    entities::TrafficLightEntity traffic_light(1, "traffic_light");
    traffic_light.SetProperties(std::move(properties));

    ASSERT_NO_THROW(traffic_light.ChangeTrafficSignalState("Off,Constant,Flashing"));

    auto actual_bulbs = traffic_light.GetProperties()->bulbs;
    EXPECT_EQ(actual_bulbs[0].mode, mantle_api::TrafficLightBulbMode::kOff);
    EXPECT_EQ(actual_bulbs[1].mode, mantle_api::TrafficLightBulbMode::kConstant);
    EXPECT_EQ(actual_bulbs[2].mode, mantle_api::TrafficLightBulbMode::kFlashing);
}

}  // namespace astas::environment::entities
