/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/vehicle_entity.h"

namespace astas::environment::entities
{

VehicleEntity::VehicleEntity(mantle_api::UniqueId id, const std::string& name) : BaseEntity(id, name) {}

mantle_api::VehicleProperties* VehicleEntity::GetProperties() const
{
    return GetPropertiesAs<mantle_api::VehicleProperties>();
}

void VehicleEntity::SetIndicatorState(mantle_api::IndicatorState state)
{
    state_ = state;
}

mantle_api::IndicatorState VehicleEntity::GetIndicatorState() const
{
    return state_;
}

mantle_api::ExternalControlState VehicleEntity::GetHADControlState() const
{
    return external_control_state_;
}

void VehicleEntity::SetHADControlState(mantle_api::ExternalControlState had_control_state)
{
    external_control_state_ = had_control_state;
}

WheelStates VehicleEntity::GetWheelStates() const
{
    return wheel_states_;
}

void VehicleEntity::SetWheelStates(WheelStates wheel_states)
{
    wheel_states_ = wheel_states;
}

}  // namespace astas::environment::entities
