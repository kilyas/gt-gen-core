/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_STATICOBJECTENTITY_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_STATICOBJECTENTITY_H

#include "Core/Environment/Entities/Internal/base_entity.h"

namespace astas::environment::entities
{
class StaticObject : public BaseEntity, public mantle_api::IStaticObject
{
  public:
    StaticObject(mantle_api::UniqueId id, const std::string& name) : BaseEntity(id, name) {}

    mantle_api::StaticObjectProperties* GetProperties() const override
    {
        return GetPropertiesAs<mantle_api::StaticObjectProperties>();
    }
};
}  // namespace astas::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_STATICOBJECTENTITY_H
