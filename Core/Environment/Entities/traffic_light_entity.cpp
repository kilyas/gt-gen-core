/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/traffic_light_entity.h"

#include "Core/Environment/Entities/Internal/convert_traffic_signal_state.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Utility/string_utils.h"

namespace astas::environment::entities
{

TrafficLightEntity::TrafficLightEntity(mantle_api::UniqueId id, const std::string& name) : BaseEntity(id, name) {}

void TrafficLightEntity::SetPosition(const mantle_api::Vec3<units::length::meter_t>& position)
{
    BaseEntity::SetPosition(position);

    const auto* traffic_light_properties = GetProperties();
    if (traffic_light_properties != nullptr)
    {
        SetBulbPositions(traffic_light_properties);
    }
}

void TrafficLightEntity::SetBulbPositions(const mantle_ext::TrafficLightProperties* properties)
{
    for (const auto& bulb : properties->bulbs)
    {
        auto bulb_position = ComputeBulbPosition(
            position_, orientation_, properties->bounding_box.dimension, bulb.position_offset_coeffs);
        bulb_positions_.emplace(bulb.id, bulb_position);
    }
}

mantle_api::Vec3<units::length::meter_t> TrafficLightEntity::ComputeBulbPosition(
    const mantle_api::Vec3<units::length::meter_t>& traffic_light_position,
    const mantle_api::Orientation3<units::angle::radian_t>& traffic_light_orientation,
    const mantle_api::Dimension3& traffic_light_dimension,
    const mantle_api::Vec3<units::dimensionless::scalar_t>& offset_coeff)
{
    mantle_api::Vec3<units::length::meter_t> local_bulb_offset{traffic_light_dimension.length * offset_coeff.x,
                                                               traffic_light_dimension.width * offset_coeff.y,
                                                               traffic_light_dimension.height * offset_coeff.z};

    return service::glmwrapper::ToGlobalSpace(traffic_light_position, traffic_light_orientation, local_bulb_offset);
}

mantle_api::Vec3<units::length::meter_t> TrafficLightEntity::GetBulbPosition(mantle_api::UniqueId bulb_id) const
{
    return bulb_positions_.at(bulb_id);
}

void TrafficLightEntity::ChangeTrafficSignalState(const std::string& traffic_signal_state)
{
    auto split_state_string = service::utility::Split(traffic_signal_state);

    auto& bulbs = GetProperties()->bulbs;

    if (bulbs.size() != split_state_string.size())
    {
        throw EnvironmentException(
            "The given state string '{}' does not match with number of bulbs for entity with name '{}'. Please check "
            "your scenario file that the state string contains for each LightBulb one state",
            traffic_signal_state,
            GetName());
    }

    for (std::size_t i = 0; i < bulbs.size(); ++i)
    {
        bulbs[i].mode = ConvertTrafficSignalState(split_state_string[i]);
    }
}
void TrafficLightEntity::SetInitialStateFromProperties()
{
    auto it = properties_->properties.find("initial_state");
    if (it != properties_->properties.end())
    {
        ChangeTrafficSignalState(it->second);
    }
}

}  // namespace astas::environment::entities
