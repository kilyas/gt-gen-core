/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/Internal/convert_traffic_signal_state.h"

#include <gtest/gtest.h>

namespace astas::environment::entities
{

TEST(ConvertTrafficSignalStateTest, GivenStringUnsupported_WhenConvertTrafficSignalState_ThenStateOther)
{
    EXPECT_EQ(ConvertTrafficSignalState("Unsupported"), mantle_api::TrafficLightBulbMode::kOther);
}

TEST(ConvertTrafficSignalStateTest, GivenStringOff_WhenConvertTrafficSignalState_ThenStateOff)
{
    EXPECT_EQ(ConvertTrafficSignalState("Off"), mantle_api::TrafficLightBulbMode::kOff);
}

TEST(ConvertTrafficSignalStateTest, GivenStringConstant_WhenConvertTrafficSignalState_ThenStateConstant)
{
    EXPECT_EQ(ConvertTrafficSignalState("Constant"), mantle_api::TrafficLightBulbMode::kConstant);
}

TEST(ConvertTrafficSignalStateTest, GivenStringFlashing_WhenConvertTrafficSignalState_ThenStateFlashing)
{
    EXPECT_EQ(ConvertTrafficSignalState("Flashing"), mantle_api::TrafficLightBulbMode::kFlashing);
}

TEST(ConvertTrafficSignalStateTest, GivenStringCounting_WhenConvertTrafficSignalState_ThenStateCounting)
{
    EXPECT_EQ(ConvertTrafficSignalState("Counting"), mantle_api::TrafficLightBulbMode::kCounting);
}

TEST(ConvertTrafficSignalStateTest, GivenStateWithWhitespaces_WhenConvertTrafficSignalState_ThenWhitspacesIgnored)
{
    EXPECT_EQ(ConvertTrafficSignalState("   Constant  "), mantle_api::TrafficLightBulbMode::kConstant);
}

TEST(ConvertTrafficSignalStateTest,
     GivenStateWithUpperAndLowerCaseCharacters_WhenConvertTrafficSignalState_ThenCaseInsensitiveMatching)
{
    EXPECT_EQ(ConvertTrafficSignalState("CoNsTaNt"), mantle_api::TrafficLightBulbMode::kConstant);
}

}  // namespace astas::environment::entities
