/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/Internal/convert_traffic_signal_state.h"

#include "Core/Service/Utility/string_utils.h"

namespace astas::environment::entities
{

mantle_api::TrafficLightBulbMode ConvertTrafficSignalState(const std::string& traffic_signal_state)
{
    auto mode = mantle_api::TrafficLightBulbMode::kOther;
    auto state = service::utility::ToLower(traffic_signal_state);
    service::utility::Trim(state);

    if (state == "off")
    {
        mode = mantle_api::TrafficLightBulbMode::kOff;
    }
    else if (state == "constant")
    {
        mode = mantle_api::TrafficLightBulbMode::kConstant;
    }
    else if (state == "flashing")
    {
        mode = mantle_api::TrafficLightBulbMode::kFlashing;
    }
    else if (state == "counting")
    {
        mode = mantle_api::TrafficLightBulbMode::kCounting;
    }

    return mode;
}

}  // namespace astas::environment::entities
