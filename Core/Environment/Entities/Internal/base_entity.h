/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_INTERNAL_BASEENTITY_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_INTERNAL_BASEENTITY_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/i_entity.h>

namespace astas::environment::entities
{

class BaseEntity : virtual mantle_api::IEntity
{
  public:
    BaseEntity(mantle_api::UniqueId id, const std::string& name) : id_{id}, name_{name} {}

    ~BaseEntity() override = default;
    BaseEntity(const BaseEntity&) = delete;
    BaseEntity& operator=(const BaseEntity&) = delete;
    BaseEntity(BaseEntity&&) = default;
    BaseEntity& operator=(BaseEntity&&) = default;

    mantle_api::UniqueId GetUniqueId() const override { return id_; }

    void SetName(const std::string& name) override { name_ = name; }
    const std::string& GetName() const override { return name_; }

    void SetPosition(const mantle_api::Vec3<units::length::meter_t>& inert_pos) override { position_ = inert_pos; }

    mantle_api::Vec3<units::length::meter_t> GetPosition() const override { return position_; }

    void SetVelocity(const mantle_api::Vec3<units::velocity::meters_per_second_t>& velocity) override
    {
        velocity_ = velocity;
    }
    mantle_api::Vec3<units::velocity::meters_per_second_t> GetVelocity() const override { return velocity_; }

    void SetAcceleration(
        const mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>& acceleration) override
    {
        acceleration_ = acceleration;
    }
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> GetAcceleration() const override
    {
        return acceleration_;
    }

    void SetOrientation(const mantle_api::Orientation3<units::angle::radian_t>& orientation) override
    {
        orientation_ = orientation;
    }
    mantle_api::Orientation3<units::angle::radian_t> GetOrientation() const override { return orientation_; }

    void SetOrientationRate(
        const mantle_api::Orientation3<units::angular_velocity::radians_per_second_t>& orientation_rate) override
    {
        orientation_rate_ = orientation_rate;
    }
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> GetOrientationRate() const override
    {
        return orientation_rate_;
    }

    void SetOrientationAcceleration(
        const mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>&
            orientation_acceleration) override
    {
        orientation_acceleration_ = orientation_acceleration;
    }
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> GetOrientationAcceleration()
        const override
    {
        return orientation_acceleration_;
    }

    void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) override
    {
        properties_ = std::move(properties);
    }

    mantle_api::EntityProperties* GetProperties() const override { return properties_.get(); }

    void SetAssignedLaneIds(const std::vector<std::uint64_t>& assigned_lane_ids) override
    {
        assigned_lane_ids_ = assigned_lane_ids;
    }
    std::vector<std::uint64_t> GetAssignedLaneIds() const override { return assigned_lane_ids_; }

    void SetVisibility(const mantle_api::EntityVisibilityConfig& visibility) override { visibility_ = visibility; }
    mantle_api::EntityVisibilityConfig GetVisibility() const override { return visibility_; }

  protected:
    template <typename T>
    T* GetPropertiesAs() const
    {
        return static_cast<T*>(properties_.get());
    }

    std::unique_ptr<mantle_api::EntityProperties> properties_{nullptr};

    mantle_api::UniqueId id_{0};
    std::string name_{};
    mantle_api::IControllerConfig config_{};
    mantle_api::Vec3<units::length::meter_t> position_{};
    mantle_api::Vec3<units::velocity::meters_per_second_t> velocity_{};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> acceleration_{};
    mantle_api::Orientation3<units::angle::radian_t> orientation_{};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> orientation_rate_{};
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t> orientation_acceleration_{};
    std::vector<std::uint64_t> assigned_lane_ids_;
    mantle_api::EntityVisibilityConfig visibility_{};
};

}  // namespace astas::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_INTERNAL_BASEENTITY_H
