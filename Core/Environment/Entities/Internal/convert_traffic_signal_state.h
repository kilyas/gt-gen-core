/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_INTERNAL_CONVERTTRAFFICSIGNALSTATE_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_INTERNAL_CONVERTTRAFFICSIGNALSTATE_H

#include <MantleAPI/Traffic/traffic_light_properties.h>

#include <string>

namespace astas::environment::entities
{

mantle_api::TrafficLightBulbMode ConvertTrafficSignalState(const std::string& traffic_signal_state);

}  // namespace astas::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_INTERNAL_CONVERTTRAFFICSIGNALSTATE_H
