/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_TRAFFICSIGNPROPERTIESPROVIDER_H
#define GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_TRAFFICSIGNPROPERTIESPROVIDER_H

#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Osi/traffic_sign_types.h"

#include <map>
#include <memory>
#include <string>

namespace astas::environment::static_objects
{

class TrafficSignPropertiesProvider
{
  public:
    static std::unique_ptr<mantle_api::StaticObjectProperties> Get(const std::string& identifier);

  private:
    static osi::OsiTrafficSignType GetOsiSignType(const std::string& identifier);
    static double GetSignalValue(const std::string& identifier);
    static std::string GetTypeIdentifier(const std::string& identifier);
    static std::optional<std::string> GetSignalSubType(const std::string& identifier);

    using DataMap = std::map<std::string, osi::OsiTrafficSignType>;
    const static DataMap kStvoSignsToOsiSigns;
};

}  // namespace astas::environment::static_objects

#endif  // GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_TRAFFICSIGNPROPERTIESPROVIDER_H
