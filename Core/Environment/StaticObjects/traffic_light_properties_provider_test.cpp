/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/traffic_light_properties_provider.h"

#include <gtest/gtest.h>

namespace astas::environment::static_objects
{

TEST(TrafficLightPropertiesProviderTest, GivenEmptyTrafficLightIdentifier_WhenGet_ThenReturnNullptr)
{
    auto properties = TrafficLightPropertiesProvider::Get("");
    auto traffic_light_properties = dynamic_cast<mantle_ext::TrafficLightProperties*>(properties.get());
    ASSERT_TRUE(traffic_light_properties == nullptr);
}

TEST(TrafficLightPropertiesProviderTest, GivenInvalidTrafficLightIdentifier_WhenGet_ThenReturnNullptr)
{
    auto properties = TrafficLightPropertiesProvider::Get("notrafficlightidentifier");
    auto traffic_light_properties = dynamic_cast<mantle_ext::TrafficLightProperties*>(properties.get());
    ASSERT_TRUE(traffic_light_properties == nullptr);
}

TEST(TrafficLightPropertiesProviderTest, GivenValidTrafficLightIdentifier_WhenGet_ThenPropertiesCorrect)
{
    auto properties = TrafficLightPropertiesProvider::Get("1.000.001");
    auto traffic_light_properties = dynamic_cast<mantle_ext::TrafficLightProperties*>(properties.get());
    ASSERT_TRUE(traffic_light_properties != nullptr);

    EXPECT_EQ(2.2, traffic_light_properties->vertical_offset());
    EXPECT_EQ(0.5, traffic_light_properties->bounding_box.dimension.length());
    EXPECT_EQ(0.5, traffic_light_properties->bounding_box.dimension.width());
    EXPECT_EQ(1.5, traffic_light_properties->bounding_box.dimension.height());
    EXPECT_EQ(mantle_api::EntityType::kStatic, traffic_light_properties->type);
    EXPECT_TRUE(traffic_light_properties->model.empty());

    ASSERT_EQ(3, traffic_light_properties->bulbs.size());
    EXPECT_EQ(100'000, traffic_light_properties->bulbs.at(0).id);
    EXPECT_EQ(100'001, traffic_light_properties->bulbs.at(1).id);
    EXPECT_EQ(100'002, traffic_light_properties->bulbs.at(2).id);

    EXPECT_EQ(mantle_api::TrafficLightBulbColor::kRed, traffic_light_properties->bulbs.at(0).color);
    EXPECT_EQ(mantle_api::TrafficLightBulbColor::kYellow, traffic_light_properties->bulbs.at(1).color);
    EXPECT_EQ(mantle_api::TrafficLightBulbColor::kGreen, traffic_light_properties->bulbs.at(2).color);

    EXPECT_EQ(osi::OsiTrafficLightIcon::kNone, traffic_light_properties->bulbs.at(0).icon);
    EXPECT_EQ(osi::OsiTrafficLightIcon::kNone, traffic_light_properties->bulbs.at(1).icon);
    EXPECT_EQ(osi::OsiTrafficLightIcon::kNone, traffic_light_properties->bulbs.at(2).icon);

    EXPECT_EQ(0, traffic_light_properties->bulbs.at(0).position_offset_coeffs.x());
    EXPECT_EQ(0, traffic_light_properties->bulbs.at(0).position_offset_coeffs.y());
    EXPECT_EQ(1. / 3., traffic_light_properties->bulbs.at(0).position_offset_coeffs.z());

    EXPECT_EQ(0, traffic_light_properties->bulbs.at(1).position_offset_coeffs.x());
    EXPECT_EQ(0, traffic_light_properties->bulbs.at(1).position_offset_coeffs.y());
    EXPECT_EQ(0, traffic_light_properties->bulbs.at(1).position_offset_coeffs.z());

    EXPECT_EQ(0, traffic_light_properties->bulbs.at(2).position_offset_coeffs.x());
    EXPECT_EQ(0, traffic_light_properties->bulbs.at(2).position_offset_coeffs.y());
    EXPECT_EQ(-1. / 3., traffic_light_properties->bulbs.at(2).position_offset_coeffs.z());
}

}  // namespace astas::environment::static_objects
