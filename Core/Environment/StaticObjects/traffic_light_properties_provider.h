/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_TRAFFICLIGHTPROPERTIESPROVIDER_H
#define GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_TRAFFICLIGHTPROPERTIESPROVIDER_H

#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Osi/traffic_light_types.h"

#include <MantleAPI/Traffic/i_entity.h>

#include <map>
#include <memory>
#include <string>

namespace astas::environment::static_objects
{

class TrafficLightPropertiesProvider
{
  public:
    static std::unique_ptr<mantle_api::StaticObjectProperties> Get(const std::string& identifier);

  private:
    const static mantle_api::UniqueId kLightBulbStartId{100'000};

    struct LightBulb
    {
        mantle_api::Vec3<units::dimensionless::scalar_t> position_offset_coeffs{};
        mantle_api::TrafficLightBulbColor light_bulb_color{};
        osi::OsiTrafficLightIcon light_bulb_icon{};
    };

    using LightBulbs = std::vector<LightBulb>;
    static std::unique_ptr<mantle_api::StaticObjectProperties> ToProperties(const LightBulbs& light_bulbs);

    using DataMap = std::map<std::string, LightBulbs>;
    const static DataMap kIdentifiersToLightBulbs;
};

}  // namespace astas::environment::static_objects

#endif  // GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_TRAFFICLIGHTPROPERTIESPROVIDER_H
