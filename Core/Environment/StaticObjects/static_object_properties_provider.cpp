/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/static_object_properties_provider.h"

namespace astas::environment::static_objects
{
using units::literals::operator""_m;

std::unique_ptr<mantle_api::StaticObjectProperties> StaticObjectPropertiesProvider::Get(const std::string& identifier)
{
    auto it = kIdentifiersToStaticObjects.find(identifier);
    if (it != kIdentifiersToStaticObjects.end())
    {
        return ToProperties(it->second);
    }
    return nullptr;
}

std::unique_ptr<mantle_api::StaticObjectProperties> StaticObjectPropertiesProvider::ToProperties(
    const StaticObjectData& data)
{
    auto static_object_properties = std::make_unique<mantle_ext::StationaryObjectProperties>();
    static_object_properties->type = mantle_api::EntityType::kStatic;
    static_object_properties->bounding_box.dimension = data.dimension;
    static_object_properties->model = data.name;
    static_object_properties->object_type = data.type;
    static_object_properties->material = data.material;
    static_object_properties->vertical_offset = 0.0_m;

    // Note: The base polygon cannot be set via the JSON scenario, thus this field is not filled

    return static_object_properties;
}

/// The mapping of ID <-> Name must be the same as in Constants_StaticObjects.cs
// NOLINTNEXTLINE(cert-err58-cpp)
const StaticObjectPropertiesProvider::DataMap StaticObjectPropertiesProvider::kIdentifiersToStaticObjects{
    {"610-40",
     {"pylon",
      {0.5_m, 0.5_m, 0.77_m},
      osi::StationaryObjectEntityType::kPylon,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"615",
     {"mobile_lane_closure_left",
      {4.1_m, 1.7_m, 2.8_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kMetal}},
    {"616-30",
     {"mobile_lane_closure_led_arrow_left",
      {4.1_m, 2.2_m, 3.9_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kMetal}},
    {"616-31",
     {"mobile_lane_closure_small_led_arrow_left",
      {4.1_m, 1.7_m, 2.8_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kMetal}},
    {"620-40",
     {"reflector_post_right",
      {0.05_m, 0.1_m, 1.1_m},
      osi::StationaryObjectEntityType::kReflectiveStructure,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"620-41",
     {"reflector_post_left",
      {0.05_m, 0.1_m, 1.1_m},
      osi::StationaryObjectEntityType::kReflectiveStructure,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"628-10",
     {"vertical_panel_barricade_right",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"628-11",
     {"vertical_panel_barricade_right_2",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"628-20",
     {"vertical_panel_barricade_left",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"628-21",
     {"vertical_panel_barricade_left_2",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"629-10",
     {"vertical_panel_barricade_right_3",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"629-11",
     {"vertical_panel_barricade_right_4",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"629-20",
     {"vertical_panel_barricade_left_3",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}},
    {"629-21",
     {"vertical_panel_barricade_left_4",
      {0.5_m, 0.4_m, 1.2_m},
      osi::StationaryObjectEntityType::kConstructionSiteElement,
      osi::StationaryObjectEntityMaterial::kPlastic}}};

}  // namespace astas::environment::static_objects
