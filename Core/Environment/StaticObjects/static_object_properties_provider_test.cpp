/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/static_object_properties_provider.h"

#include <gtest/gtest.h>

namespace astas::environment::static_objects
{

TEST(StaticObjectPropertiesProvider, GivenNonStaticObjectIdentifier_WhenGet_ThenReturnNullPtr)
{
    EXPECT_TRUE(StaticObjectPropertiesProvider::Get("123456789") == nullptr);
}

TEST(StaticObjectPropertiesProvider, GivenPylonIdentifier_WhenGet_ThenReturnPylonProperties)
{
    auto properties = StaticObjectPropertiesProvider::Get("610-40");
    auto stationary_object_properties = dynamic_cast<mantle_ext::StationaryObjectProperties*>(properties.get());
    ASSERT_TRUE(stationary_object_properties != nullptr);

    EXPECT_EQ(mantle_api::EntityType::kStatic, stationary_object_properties->type);
    EXPECT_EQ("pylon", stationary_object_properties->model);
    EXPECT_EQ(0.5, stationary_object_properties->bounding_box.dimension.length());
    EXPECT_EQ(0.5, stationary_object_properties->bounding_box.dimension.width());
    EXPECT_EQ(0.77, stationary_object_properties->bounding_box.dimension.height());
    EXPECT_EQ(osi::StationaryObjectEntityType::kPylon, stationary_object_properties->object_type);
}

}  // namespace astas::environment::static_objects
