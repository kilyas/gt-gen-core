/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/traffic_light_properties_provider.h"

namespace astas::environment::static_objects
{

using units::literals::operator""_m;

std::unique_ptr<mantle_api::StaticObjectProperties> TrafficLightPropertiesProvider::Get(const std::string& identifier)
{
    auto it = kIdentifiersToLightBulbs.find(identifier);
    if (it != kIdentifiersToLightBulbs.end())
    {
        return ToProperties(it->second);
    }
    return nullptr;
}

std::unique_ptr<mantle_api::StaticObjectProperties> TrafficLightPropertiesProvider::ToProperties(
    const LightBulbs& light_bulbs)
{
    auto traffic_light_properties = std::make_unique<mantle_ext::TrafficLightProperties>();
    traffic_light_properties->properties.emplace("object_type", "traffic_light");

    static mantle_api::UniqueId light_bulb_id{kLightBulbStartId};

    units::length::meter_t default_mount_height{2.2};
    units::length::meter_t default_size{0.5};
    units::length::meter_t height = default_size * static_cast<double>(light_bulbs.size());

    traffic_light_properties->type = mantle_api::EntityType::kStatic;
    traffic_light_properties->bounding_box.dimension = {default_size, default_size, height};

    traffic_light_properties->vertical_offset = default_mount_height;

    for (const auto& light_bulb : light_bulbs)
    {
        mantle_ext::TrafficLightBulbProperties bulb_properties;
        bulb_properties.id = light_bulb_id++;
        bulb_properties.bounding_box.dimension = {default_size, default_size, default_size};
        bulb_properties.position_offset_coeffs = light_bulb.position_offset_coeffs;
        bulb_properties.icon = light_bulb.light_bulb_icon;
        bulb_properties.color = light_bulb.light_bulb_color;
        bulb_properties.mode = mantle_api::TrafficLightBulbMode::kOff;

        traffic_light_properties->bulbs.push_back(bulb_properties);
    }

    return traffic_light_properties;
}

// NOLINTNEXTLINE(cert-err58-cpp)
const TrafficLightPropertiesProvider::DataMap TrafficLightPropertiesProvider::kIdentifiersToLightBulbs{
    {"1.000.001",
     {{{0, 0, 1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kNone},
      {{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kNone},
      {{0, 0, -1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kNone}}},
    {"1.000.002",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kPedestrian},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kPedestrian}}},
    {"1.000.002-10", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kPedestrian}}},
    {"1.000.002-20", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kPedestrian}}},
    {"1.000.002-30", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kPedestrian}}},
    {"1.000.007",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kPedestrianAndBicycle},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kPedestrianAndBicycle}}},
    {"1.000.007-10",
     {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kPedestrianAndBicycle}}},
    {"1.000.007-20",
     {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kPedestrianAndBicycle}}},
    {"1.000.007-30",
     {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kPedestrianAndBicycle}}},
    {"1.000.008", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kNone}}},
    {"1.000.008-10", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowLeft}}},
    {"1.000.008-20", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowRight}}},
    {"1.000.009-10",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kNone},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kNone}}},
    {"1.000.009-20",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kNone},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kNone}}},
    {"1.000.009-30",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kNone},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kNone}}},
    {"1.000.010-10",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowLeft},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowLeft}}},
    {"1.000.010-20",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowRight},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowRight}}},
    {"1.000.011-10",
     {{{0, 0, 1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kArrowLeft},
      {{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowLeft},
      {{0, 0, -1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowLeft}}},
    {"1.000.011-20",
     {{{0, 0, 1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kArrowRight},
      {{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowRight},
      {{0, 0, -1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowRight}}},
    {"1.000.011-30",
     {{{0, 0, 1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kArrowStraightAhead},
      {{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowStraightAhead},
      {{0, 0, -1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowStraightAhead}}},
    {"1.000.011-40",
     {{{0, 0, 1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kArrowStraightAheadLeft},
      {{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowStraightAheadLeft},
      {{0, 0, -1.0 / 3.0},
       mantle_api::TrafficLightBulbColor::kGreen,
       osi::OsiTrafficLightIcon::kArrowStraightAheadLeft}}},
    {"1.000.011-50",
     {{{0, 0, 1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kArrowStraightAheadRight},
      {{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowStraightAheadRight},
      {{0, 0, -1.0 / 3.0},
       mantle_api::TrafficLightBulbColor::kGreen,
       osi::OsiTrafficLightIcon::kArrowStraightAheadRight}}},
    {"1.000.012", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kNone}}},
    {"1.000.012-10", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowLeft}}},
    {"1.000.012-20", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowRight}}},
    {"1.000.013",
     {{{0, 0, 0.25}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kBicycle},
      {{0, 0, -0.25}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kBicycle}}},
    {"1.000.013-10",
     {{{0, 0, 1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kBicycle},
      {{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kBicycle},
      {{0, 0, -1.0 / 3.0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kBicycle}}},
    {"1.000.013-20", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kBicycle}}},
    {"1.000.013-30", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kBicycle}}},
    {"1.000.013-40", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kBicycle}}},
    {"1.000.014", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kTram}}},
    {"1.000.015", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kPedestrian}}},
    {"1.000.016", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kBicycle}}},
    {"1.000.017", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kBus}}},
    {"1.000.019",
     {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kPedestrianAndBicycle}}},
    {"1.000.020", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kNone}}},
    {"1.000.021", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kGreen, osi::OsiTrafficLightIcon::kArrowDown}}},
    {"1.000.022-10",
     {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowDownLeft}}},
    {"1.000.022-20",
     {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kYellow, osi::OsiTrafficLightIcon::kArrowDownRight}}},
    {"1.000.023", {{{0, 0, 0}, mantle_api::TrafficLightBulbColor::kRed, osi::OsiTrafficLightIcon::kArrowCross}}},
};

}  // namespace astas::environment::static_objects
