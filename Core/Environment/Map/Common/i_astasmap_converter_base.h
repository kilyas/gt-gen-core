/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IASTASMAPCONVERTERBASE_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IASTASMAPCONVERTERBASE_H

#include "Core/Environment/Map/AstasMap/astas_map_finalizer.h"
#include "Core/Environment/Map/Common/i_any_to_astasmap_converter.h"
#include "Core/Environment/Map/Common/map_converter_data.h"
#include "Core/Service/Logging/scoped_logging_context.h"

namespace astas::environment::map
{
class AstasMap;

class IAstasMapConverterBase : public IAnyToAstasMapConverter
{
  public:
    IAstasMapConverterBase(service::utility::UniqueIdProvider& id_provider,
                           const MapConverterData& data,
                           environment::map::AstasMap& astas_map)
        : unique_id_provider_{id_provider}, data_{data}, astas_map_{astas_map}
    {
    }

    void Convert() override
    {
        service::logging::ScopedLoggingContext scoped_logging_context_guard("map");

        PreConvert();
        ConvertInternal();
        PostConvert();
    }

  protected:
    virtual void PreConvert() = 0;

    virtual void PostConvert()
    {
        AstasMapFinalizer finalizer(astas_map_);
        finalizer.Finalize();
    }

    virtual void ConvertInternal() = 0;

    service::utility::UniqueIdProvider& unique_id_provider_;
    MapConverterData data_;
    environment::map::AstasMap& astas_map_;
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IASTASMAPCONVERTERBASE_H
