/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_EXCEPTIONS_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_EXCEPTIONS_H

#include "Core/Service/Utility/exceptions.h"

namespace astas::environment::map::exception
{

class MapException : public service::utility::exception::AstasException
{
    using service::utility::exception::AstasException::AstasException;
};

class FileLoading : public MapException
{
    using MapException::MapException;
};

class LaneNotFound : public MapException
{
    using MapException::MapException;
};

class LaneBoundaryNotFound : public MapException
{
    using MapException::MapException;
};

}  // namespace astas::environment::map::exception

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_EXCEPTIONS_H
