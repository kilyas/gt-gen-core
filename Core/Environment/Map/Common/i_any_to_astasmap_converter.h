/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IANYTOASTASMAPCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IANYTOASTASMAPCONVERTER_H

#include <MantleAPI/Common/i_identifiable.h>

#include <map>

namespace astas::environment::map
{
class AstasMap;

class IAnyToAstasMapConverter
{
  public:
    virtual ~IAnyToAstasMapConverter() = default;

    virtual void Convert() = 0;

    virtual std::map<mantle_api::UniqueId, mantle_api::UniqueId> GetNativeToAstasTrafficLightIdMap() const = 0;

  protected:
    IAnyToAstasMapConverter() = default;
    IAnyToAstasMapConverter(const IAnyToAstasMapConverter&) = default;
    IAnyToAstasMapConverter(IAnyToAstasMapConverter&&) = default;
    IAnyToAstasMapConverter& operator=(const IAnyToAstasMapConverter&) = default;
    IAnyToAstasMapConverter& operator=(IAnyToAstasMapConverter&&) = default;
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_IANYTOASTASMAPCONVERTER_H
