/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Common/converter_utility.h"

#include "Core/Service/MantleApiExtension/formatting.h"

namespace astas::environment::map
{

std::string GetMapCoordinateString(
    const mantle_api::Vec3<units::length::meter_t>& world_coordinate,
    const IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>* coordinate_converter)
{
    std::string map_coordinate{"not available"};

    if (coordinate_converter == nullptr)
    {
        return map_coordinate;
    }

    auto map_pos = coordinate_converter->Convert(world_coordinate);
    if (map_pos)
    {
        std::visit([&map_coordinate](auto const& pos) { map_coordinate = fmt::format("{}", pos); }, *map_pos);
    }
    return map_coordinate;
}

}  // namespace astas::environment::map
