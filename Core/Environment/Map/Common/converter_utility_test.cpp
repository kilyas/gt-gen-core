/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Common/converter_utility.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::map
{
using units::literals::operator""_m;

class MockCoordinateConverter : public IConverter<UnderlyingMapCoordinate, mantle_api::Vec3<units::length::meter_t>>
{
  public:
    explicit MockCoordinateConverter(bool conversion_succeeds) : conversion_succeeds_{conversion_succeeds} {}

    MOCK_METHOD((std::optional<mantle_api::Vec3<units::length::meter_t>>),
                Convert,
                (const UnderlyingMapCoordinate&),
                (const, override));

    std::optional<UnderlyingMapCoordinate> Convert(
        const mantle_api::Vec3<units::length::meter_t>& coordinate) const override
    {
        if (conversion_succeeds_)
        {
            return mantle_api::LatLonPosition{units::angle::degree_t{coordinate.x()},
                                              units::angle::degree_t{coordinate.y()}};
        }
        else
        {
            return std::nullopt;
        }
    }

  private:
    bool conversion_succeeds_{false};
};

TEST(GetMapCoordinateString, GivenCoordinateConversionNotPossible_WhenGetMapCoordinateString_ThenNotAvailableReturned)
{
    MockCoordinateConverter converter{false};
    mantle_api::Vec3<units::length::meter_t> test_vector{1.0_m, 2.0_m, 3.0_m};

    std::string coordinate_string = GetMapCoordinateString(test_vector, &converter);
    EXPECT_THAT(coordinate_string, testing::HasSubstr("not available"));
}

TEST(GetMapCoordinateString, GivenConverterIsNull_WhenGetMapCoordinateString_ThenNotAvailableReturned)
{
    mantle_api::Vec3<units::length::meter_t> test_vector{1.0_m, 2.0_m, 3.0_m};

    std::string coordinate_string = GetMapCoordinateString(test_vector, nullptr);
    EXPECT_THAT(coordinate_string, testing::HasSubstr("not available"));
}

TEST(GetMapCoordinateString,
     GivenCoordinateConversionPossible_WhenGetMapCoordinateString_ThenFormattedPositionStringReturned)
{
    MockCoordinateConverter converter{true};
    mantle_api::Vec3<units::length::meter_t> test_vector{1.0_m, 2.0_m, 3.0_m};

    std::string coordinate_string = GetMapCoordinateString(test_vector, &converter);
    EXPECT_THAT(coordinate_string, testing::HasSubstr("1.0"));
    EXPECT_THAT(coordinate_string, testing::HasSubstr("2.0"));
}

}  // namespace astas::environment::map
