/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_COORDINATECONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_COORDINATECONVERTER_H

#include <optional>
#include <vector>

namespace astas::environment::map
{

template <typename A, typename B>
class IConverter
{
  public:
    IConverter() = default;
    virtual ~IConverter() = default;
    IConverter(const IConverter& other) = default;
    IConverter(IConverter&& other) = default;
    IConverter& operator=(const IConverter& other) = default;
    IConverter& operator=(IConverter&& other) = default;

    virtual std::optional<B> Convert(const A& coordinate) const = 0;

    virtual std::optional<A> Convert(const B& coordinate) const = 0;

    std::vector<B> Convert(const std::vector<A>& coordinates) const { return ConvertContainer<A, B>(coordinates); }

    std::vector<A> Convert(const std::vector<B>& coordinates) const { return ConvertContainer<B, A>(coordinates); }

  private:
    template <typename From, typename To>
    std::vector<To> ConvertContainer(const std::vector<From>& coordinates) const
    {
        std::vector<To> converted_coordinates{};
        converted_coordinates.reserve(coordinates.size());

        for (const auto& coordinate : coordinates)
        {
            auto converted_coordinate = Convert(coordinate);
            if (!converted_coordinate)
            {
                return {};
            }
            converted_coordinates.push_back(*converted_coordinate);
        }
        return converted_coordinates;
    }
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_COORDINATECONVERTER_H
