/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_COMMON_MAPCONVERTERDATA_H
#define GTGEN_CORE_ENVIRONMENT_MAP_COMMON_MAPCONVERTERDATA_H

#include "Core/Service/MantleApiExtension/json_map_details.h"

#include <MantleAPI/Common/position.h>

#include <string>
#include <vector>

namespace astas::environment::map
{

struct OpenDriveProperties
{
    double lane_marking_distance_in_m{};
    double lane_marking_downsampling_epsilon{};
    bool lane_marking_downsampling{true};
};

struct NdsProperties
{
    bool include_obstacles{false};
};

struct MapConverterData
{
    std::string absolute_map_path{};
    std::vector<mantle_api::LatLonPosition> waypoints{};
    std::vector<mantle_ext::FrictionPatch> friction_patches{};

    OpenDriveProperties open_drive_properties{};
    NdsProperties nds_properties{};
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_COMMON_MAPCONVERTERDATA_H
