/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/LaneLocationProvider/lane_finder.h"

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::map
{

LaneFinder::LaneFinder(const AstasMap& map) : map_{map} {}

const Lane* LaneFinder::FindLane(const mantle_api::UniqueId lane_id) const
{
    ASTAS_PROFILE_SCOPE
    return map_.FindLane(lane_id);
}

std::vector<const Lane*> LaneFinder::FindLanes(const mantle_api::Vec3<units::length::meter_t>& position) const
{
    ASTAS_PROFILE_SCOPE
    return map_.GetSpatialHash().GetLanes(position);
}

const Lane* LaneFinder::GetCachedSuccessorLane(const Lane* lane) const
{
    ASSERT(lane != nullptr && "Cannot get successor for a nullptr lane")

    if (lane->id == successor_cache_.first)
    {
        return successor_cache_.second;
    }

    const Lane* best_successor = nullptr;
    for (const auto& successor : lane->successors)
    {
        best_successor = FindLane(successor);
        if (best_successor != nullptr && AnyLaneExists(best_successor->successors))
        {
            TRACE("Successor for Lane {} -> {}", lane->id, best_successor->id)
            break;
        }
    }

    successor_cache_ = std::make_pair(lane->id, best_successor);
    return best_successor;
}

bool LaneFinder::LaneExists(const mantle_api::UniqueId lane_id) const
{
    return map_.ContainsLane(lane_id);
}

bool LaneFinder::AnyLaneExists(const std::vector<mantle_api::UniqueId>& lane_ids) const
{
    return std::any_of(lane_ids.cbegin(), lane_ids.cend(), [this](const auto& lane_id) { return LaneExists(lane_id); });
}

}  // namespace astas::environment::map
