/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANELOCATIONPROVIDER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANELOCATIONPROVIDER_H

#include "Core/Environment/AstasEnvironment/Internal/geometry_helper.h"
#include "Core/Environment/LaneFollowing/point_list_traverser.h"
#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <units.h>

#include <memory>
#include <optional>
#include <vector>

namespace astas::environment::map
{

// Forward declarations
class AstasMap;
class LaneFinder;

/// @brief Provides the lane and relative location on it - given simulation coordinates.
///
/// NB: This can be implemented as free functions, as well.
/// An advantage of using a class is that the lookups UniqueId -> Lane reference can be cached.

class LaneLocationProvider : public mantle_api::ILaneLocationQueryService
{
  public:
    explicit LaneLocationProvider(const AstasMap& map);
    LaneLocationProvider(const LaneLocationProvider&) = delete;
    LaneLocationProvider& operator=(const LaneLocationProvider&) = delete;
    LaneLocationProvider(LaneLocationProvider&&) = default;
    LaneLocationProvider& operator=(LaneLocationProvider&&) = delete;

    ~LaneLocationProvider() override;  // Destructor must be implemented where the pimpl-objects are known (cpp file)

    LaneLocation GetLaneLocationById(mantle_api::UniqueId lane_id,
                                     const mantle_api::Vec3<units::length::meter_t>& position) const;
    mantle_api::Orientation3<units::angle::radian_t> GetLaneOrientation(
        const mantle_api::Vec3<units::length::meter_t>& position) const override;

    /// @brief Takes in the road geometry and shifts a position the given amount upwards along the lane normal
    ///
    /// This function is used for shifting entities upwards, such that their bounding boxes are touching
    /// with the bottom-plane the road surface. Thus, entities do not "sink" into the road.
    ///
    /// @param position Position on road surface
    /// @param upwards_shift Amount to shift position upwards along lane normal
    /// @param allow_invalid_positions If true, an upwards vector is used instead of the lane normal for the shift
    /// @return Upwards shifted position
    mantle_api::Vec3<units::length::meter_t> GetUpwardsShiftedLanePosition(
        const mantle_api::Vec3<units::length::meter_t>& position,
        double upwards_shift,
        bool allow_invalid_positions = false) const override;

    bool IsPositionOnLane(const mantle_api::Vec3<units::length::meter_t>& position) const override;

    /// @brief Returns a list of IDs representing all lanes enclosing the passed in position within their shape(s).
    ///
    /// @param position  Position to search for the Lane IDs
    /// @return List of lane ids
    std::vector<mantle_api::UniqueId> GetLaneIdsAtPosition(
        const mantle_api::Vec3<units::length::meter_t>& position) const override;

    /// @brief Returns a list of lane IDs at a specified position, sorted based on closest in altitude followed by
    /// alignment to direction of the lane.
    ///
    /// @param position Position to search for the Lane IDs
    /// @return List of lane ids, sorted based on altitude and direction
    std::vector<mantle_api::UniqueId> GetSortedLaneIdsAtPosition(
        const mantle_api::Vec3<units::length::meter_t>& position,
        const mantle_api::Orientation3<units::angle::radian_t>& orientation) const;

    /// @brief For a given position, get the fully constructed lane location.
    ///
    /// Computing the lane normal takes more time because the closest lane boundary point has to be found, too.
    /// Therefore, the use can decide whether this is needed or not.
    /// @attention This function for now assumes that right lane boundaries exist! If they don't, no lane normal is
    /// computed for now. This should be a sane assumption, though.
    ///
    /// @param position Position in Simulation space coordinates.
    /// @return LaneLocation for the given location.
    LaneLocation GetLaneLocation(const mantle_api::Vec3<units::length::meter_t>& position) const;

    /// @brief Relays to the LaneFinder
    const Lane* GetCachedSuccessorLane(const Lane* lane) const;

    /// @brief Relays to the LaneFinder
    const Lane* FindLane(mantle_api::UniqueId lane_id) const;

    /// @brief Relays to the LaneFinder
    std::vector<const Lane*> FindLanes(const mantle_api::Vec3<units::length::meter_t>& position) const;

    /// @brief For a given position, find a "best" matching lane
    ///
    /// For a given @p position find a lane, which coincides with the @p position (@p position is located on the lane)
    /// and has a successor lane assigned; return a pointer to the first lane found.
    /// If there are lanes at @p position but none of this lanes has a successor lane assigned, then return the first
    /// lane found from the list of this lanes.
    /// If there is no lane that coincides with @p position, then return @c nullptr.
    ///
    /// @param[in] position Position in Simulation space coordinates.
    /// @return pointer to lane coincident with @p position or @c nullptr
    const Lane* FindBestLaneCandidate(const mantle_api::Vec3<units::length::meter_t>& position) const;

    const AstasMap& GetAstasMap() const { return map_; }

    std::optional<mantle_api::Pose> FindLanePoseAtDistanceFrom(const mantle_api::Pose& reference_pose_on_lane,
                                                               units::length::meter_t distance,
                                                               mantle_api::Direction direction) const override;

    /// @brief Calculate the longitudinal distance of two given positions on a lane.
    ///
    /// @param start_position  Starting position. Must be on a lane.
    /// @param target_position Target position. Must be projectable to the lane center line of the start position or one
    /// of its' successors.
    /// @return Longitudinal distance of the two positions along the lane center line.
    ///         No value returned if the distance is not calculable.
    std::optional<units::length::meter_t> GetLongitudinalLaneDistanceBetweenPositions(
        const mantle_api::Vec3<units::length::meter_t>& start_position,
        const mantle_api::Vec3<units::length::meter_t>& target_position) const override;

    /// @brief Defines a position derived from relative lane coordinates ( lane_target, distance ) to a reference pose.
    ///
    /// calculates the longitudinal distance along the centerline of the lane on which the reference pose lies.
    /// Calculates the target lane using relative_lane_target and projects the translated pose into the target lane.
    ///
    /// @param  reference_pose_on_lane Position on road surface.
    /// @param  relative_lane_target  Lateral shift in amount of lanes to the left (positive) or right (negative) from
    /// the lane where the reference pose is.
    /// @param  distance  Longitudinal distance along the lane centerline of the reference pose. Positive direction is
    /// aligned with the local coordinate system's x-axis defined by the reference pose.
    /// @param  lateral_offset lateral offset from the target lane centerline along the road surface.
    /// @return pose New calculated pose. Orientation is aligned with the target lane center line.
    std::optional<mantle_api::Pose> FindRelativeLanePoseAtDistanceFrom(
        const mantle_api::Pose& reference_pose_on_lane,
        int relative_lane_target,
        units::length::meter_t distance,
        units::length::meter_t lateral_offset) const override;

    /// @brief Calculate the lane id from a relative target distance from a given position
    ///
    ///@param reference_pose_on_lane  Starting position. Must be on a lane.
    ///@param relative_target_lane  Shift of the target position in number of lanes relative to the lane where the
    /// reference pose is located. Positive to the left, negative to the right.
    ///@return The lane id that is at the given lateral shift (relative_lane_target) from given position
    ///(reference_pose_on_lane).
    ///        No value, if reference pose is not on a lane or if the lane doesn't have a suitable adjacent lane
    std::optional<mantle_api::LaneId> GetRelativeLaneId(const mantle_api::Pose& reference_pose_on_lane,
                                                        int relative_lane_target) const override;

    /// @brief Calculate pose projected from a given position to a target lane centerline
    ///
    ///@param reference_position_on_lane  Reference position. Must be projectable onto target lane.
    ///@param target_lane_id  The id of the target lane that the reference position is projected to.
    ///@return The projected pose on the target lane centerline. No value, if the projection pose cannot be calculated,
    /// e.g. reference position not projectable, or target lane does not exist, etc.
    std::optional<mantle_api::Pose> GetProjectedPoseAtLane(
        const mantle_api::Vec3<units::length::meter_t>& reference_position_on_lane,
        mantle_api::LaneId target_lane_id) const override;

    /// @brief Tries to map a given position to a lane and then returns the projected center line point of the lane from
    /// the given position
    ///
    /// @param position  Position that shall be mapped to a lane
    /// @return Projected center line point of the lane from given position. In case the position is valid on multiple
    /// lanes,
    ///         first lane in the list is considered. No value, if the position is not on a lane.
    std::optional<mantle_api::Vec3<units::length::meter_t>> GetProjectedCenterLinePoint(
        const mantle_api::Vec3<units::length::meter_t>& position) const override;

    /// @brief Calculate the new position which is at a certain lateral distance from the reference pose.
    ///
    /// @param reference_pose  Reference pose. Must be on a lane.
    /// @param direction  Lateral displacement direction either to the left or right w.r.t the reference pose.
    ///                   If displacement direction is set to "any", position calculation to the left is executed first
    ///                   and, in case of an an invalid result, to the right next. If both are invalid, no result is
    ///                   returned (std::nullopt).
    /// @param distance  Lateral distance either to left or right of the reference pose.
    /// @return Position at the given lateral distance in the given displacement direction from the reference pose.
    ///         If the calculated position is not on a lane, no value is returned. If the position is on multiple
    ///         lanes at the same time, the first lane in the list is considered.
    std::optional<mantle_api::Vec3<units::length::meter_t>> GetPosition(
        const mantle_api::Pose& reference_pose,
        mantle_api::LateralDisplacementDirection direction,
        units::length::meter_t distance) const override;

  private:
    std::optional<mantle_api::LaneId> GetAdjacentRelativeLaneId(const Lane& lane, int relative_lane_target) const;
    lanefollowing::PointListTraverser GetLanePointTraverser(const LaneLocation& lane_location,
                                                            units::length::meter_t distance,
                                                            const mantle_api::Direction direction) const;
    std::optional<mantle_api::Pose> CalculatePoseUsingTraverser(const LaneLocation& lane_location,
                                                                units::length::meter_t distance,
                                                                const mantle_api::Pose& reference_pose_on_lane,
                                                                const mantle_api::Direction direction) const;
    void FilterLanesByAltitude(const std::vector<const environment::map::Lane*>& input_lanes,
                               const mantle_api::Vec3<units::length::meter_t>& position,
                               std::vector<const environment::map::Lane*>& output_within_threshold,
                               std::vector<const environment::map::Lane*>& output_outside_threshold,
                               const double altitude_threshold) const;
    std::vector<mantle_api::UniqueId> SortLaneIdsByAngle(
        const std::vector<const environment::map::Lane*>& lanes,
        const mantle_api::Vec3<units::length::meter_t>& position,
        const mantle_api::Orientation3<units::angle::radian_t>& orientation) const;

    const AstasMap& map_;
    std::unique_ptr<LaneFinder> lane_finder_;
    api::GeometryHelper geometry_helper_;

    mutable mantle_api::Vec3<units::length::meter_t> last_request_position_{
        std::numeric_limits<units::length::meter_t>::max(),
        std::numeric_limits<units::length::meter_t>::max(),
        std::numeric_limits<units::length::meter_t>::max()};
    mutable LaneLocation cached_lane_location_{};
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANELOCATIONPROVIDER_H
