/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/LaneLocationProvider/lane_location.h"

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/Map/AstasMap/lane_boundary.h"
#include "Core/Environment/Map/AstasMap/utility.h"
#include "Core/Environment/Map/Geometry/project_query_point_on_boundaries.h"
#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::map
{

namespace
{

mantle_api::Vec3<units::length::meter_t> DirectionOfPolyLineAtIndex(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline,
    std::size_t index)
{
    if (index == polyline.size() - 1)
    {
        const mantle_api::Vec3<units::length::meter_t> start_point = polyline.at(index - 1);
        const mantle_api::Vec3<units::length::meter_t> end_point = polyline.at(index);
        return service::glmwrapper::Normalize(end_point - start_point);
    }
    else
    {
        const mantle_api::Vec3<units::length::meter_t> start_point = polyline.at(index);
        const mantle_api::Vec3<units::length::meter_t> end_point = polyline.at(index + 1);
        return service::glmwrapper::Normalize(end_point - start_point);
    }
}
}  // namespace

LaneLocation::LaneLocation(const mantle_api::Vec3<units::length::meter_t>& position,
                           const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line,
                           const std::vector<const LaneBoundary*>& right_lane_boundaries,
                           const std::vector<const LaneBoundary*>& left_lane_boundaries,
                           const std::vector<const Lane*>& lanes)
    : lanes(lanes)
{
    ASTAS_PROFILE_SCOPE_NAMED("LaneLocation with GLM")

    {
        ASTAS_PROFILE_SCOPE_NAMED("pos_and_idx")
        auto pos_and_idx = ProjectQueryPointOnPolyline(position, center_line);
        projected_centerline_point = std::get<0>(pos_and_idx);
        centerline_point_index = std::get<1>(pos_and_idx);
    }

    {
        ASTAS_PROFILE_SCOPE_NAMED("direction")
        direction = DirectionOfPolyLineAtIndex(center_line, centerline_point_index);
    }

    std::pair<mantle_api::Vec3<units::length::meter_t>, double> right_boundary_point_projection;
    std::pair<mantle_api::Vec3<units::length::meter_t>, double> left_boundary_point_projection;
    {
        ASTAS_PROFILE_SCOPE_NAMED("projected_right_boundary_point")
        right_boundary_point_projection =
            ProjectQueryPointOnBoundaries(projected_centerline_point, right_lane_boundaries);
        projected_right_boundary_point = right_boundary_point_projection.first;
        left_boundary_point_projection =
            ProjectQueryPointOnBoundaries(projected_centerline_point, left_lane_boundaries);
        projected_left_boundary_point = left_boundary_point_projection.first;
    }

    {
        ASTAS_PROFILE_SCOPE_NAMED("lane_normal")
        // In case the center line is outside of the lane boundaries, use the center line as one lane boundary.
        // Happens on merging zip lanes where boundaries even merge to the same end point where no lane normal could be
        // calculated anymore (see CB-#9393118)
        lane_normal = service::glmwrapper::Normal(
            right_boundary_point_projection.second > 0 ? projected_right_boundary_point : projected_centerline_point,
            left_boundary_point_projection.second < 0 ? projected_left_boundary_point : projected_centerline_point,
            direction);
    }
}

mantle_api::Orientation3<units::angle::radian_t> LaneLocation::GetOrientation() const
{
    return service::glmwrapper::FromBasisVectors(direction, lane_normal);
}

bool LaneLocation::IsValid() const
{
    return !lanes.empty();
}

}  // namespace astas::environment::map
