/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANELOCATION_H
#define GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANELOCATION_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/orientation.h>
#include <MantleAPI/Common/vector.h>

#include <vector>

namespace astas::environment::map
{
struct Lane;
struct LaneBoundary;

struct LaneLocation
{
    LaneLocation() = default;

    LaneLocation(const mantle_api::Vec3<units::length::meter_t>& position,
                 const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line,
                 const std::vector<const LaneBoundary*>& right_lane_boundaries,
                 const std::vector<const LaneBoundary*>& left_lane_boundaries,
                 const std::vector<const Lane*>& lanes);

    std::size_t centerline_point_index{0};
    mantle_api::Vec3<units::length::meter_t> projected_centerline_point{};
    mantle_api::Vec3<units::length::meter_t> direction{};
    mantle_api::Vec3<units::length::meter_t> projected_right_boundary_point{};
    mantle_api::Vec3<units::length::meter_t> projected_left_boundary_point{};
    mantle_api::Vec3<units::length::meter_t> lane_normal{};
    std::vector<const Lane*> lanes{};

    mantle_api::Orientation3<units::angle::radian_t> GetOrientation() const;

    bool IsValid() const;
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANELOCATION_H
