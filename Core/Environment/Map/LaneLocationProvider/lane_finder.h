/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANEFINDER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANEFINDER_H

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>

#include <cstdint>
#include <limits>
#include <vector>

namespace astas::environment::map
{
struct Lane;
class AstasMap;

class LaneFinder
{
  public:
    explicit LaneFinder(const AstasMap& map);
    ~LaneFinder() = default;
    LaneFinder(const LaneFinder& other) = default;
    LaneFinder(LaneFinder&& other) = default;
    LaneFinder& operator=(const LaneFinder& other) = delete;
    LaneFinder& operator=(LaneFinder&& other) = delete;

    /// @brief For a given map element relation, get the associated lane.
    /// @param map_element_relation Contains the group and lane ID to look for
    /// @return Observing pointer to the associated lane, or nullptr if not associated to any lane.
    const Lane* FindLane(mantle_api::UniqueId lane_id) const;

    /// @brief For a given position, get the associated lanes.
    /// @param position Position in Simulation space coordinates
    /// @return List of lanes for the given position
    std::vector<const Lane*> FindLanes(const mantle_api::Vec3<units::length::meter_t>& position) const;

    /// @brief Returns a successor which has again a successor lane, or the last one found
    const Lane* GetCachedSuccessorLane(const Lane* lane) const;

    bool LaneExists(mantle_api::UniqueId lane_id) const;
    bool AnyLaneExists(const std::vector<mantle_api::UniqueId>& lane_ids) const;

  protected:
    const AstasMap& map_;

    using ConnectedLaneCache = std::pair<mantle_api::UniqueId, const Lane*>;
    mutable ConnectedLaneCache successor_cache_{std::numeric_limits<mantle_api::UniqueId>::max(), nullptr};
    mutable ConnectedLaneCache predecessor_cache_{std::numeric_limits<mantle_api::UniqueId>::max(), nullptr};
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_LANELOCATIONPROVIDER_LANEFINDER_H
