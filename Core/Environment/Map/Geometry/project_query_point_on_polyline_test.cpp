/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"

#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{
using units::literals::operator""_m;

TEST(ProjectQueryPointOnPolyline, GivenPointBeforSegment_WhenGettingPosAndIdx_ThenPosIsFirstPointAndIdxIsZero)
{
    ///   x      x------x------x
    ///   0      1      2      3

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
        {3.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{0.0_m, 100.0_m, 10.0_m};

    const mantle_api::Vec3<units::length::meter_t> expected_closest_point = center_line_pts.front();

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE(expected_closest_point, std::get<0>(pos_and_idx));
    EXPECT_EQ(0, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenPointEqualToFirstPolylinePoint_WhenGettingPosAndIdx_ThenPosIsFirstPointAndIdxIsZero)
{
    ///   x
    ///   x------x------x
    ///   1      2      3

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
        {3.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{1.0_m, 100.0_m, 10.0_m};

    const mantle_api::Vec3<units::length::meter_t> expected_closest_point = center_line_pts.front();

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE_NEAR(expected_closest_point, std::get<0>(pos_and_idx), orientation_accuracy);
    EXPECT_EQ(0, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenPointJustBeforeAInternalPolylinePoint_WhenGettingPosAndIdx_ThenPosIsCenterLinePointAndIndexTheOneOfThePoint)
{
    ///         x
    ///   x------x------x
    ///   1      2      3

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
        {3.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{1.9_m, 100.0_m, 10.0_m};
    const mantle_api::Vec3<units::length::meter_t> expected_point{1.9_m, 0.0_m, 0.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE_NEAR(expected_point, std::get<0>(pos_and_idx), orientation_accuracy);
    EXPECT_EQ(0, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenPointEqualToAInternalPolylinePoint_WhenGettingPosAndIdx_ThenPosIsCenterLinePointAndIndexTheOneOfThePoint)
{
    ///          x
    ///   x------x------x
    ///   1      2      3

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
        {3.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{2.0_m, 100.0_m, 10.0_m};

    const mantle_api::Vec3<units::length::meter_t> expected_closest_point = center_line_pts.at(1);

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE_NEAR(expected_closest_point, std::get<0>(pos_and_idx), orientation_accuracy);
    EXPECT_EQ(1, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenPointJustAfterAInternalPolylinePoint_WhenGettingPosAndIdx_ThenPosIsCenterLinePointAndIndexTheOneOfThePoint)
{
    ///           x
    ///   x------x------x
    ///   1      2      3

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
        {3.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{2.1_m, 100.0_m, 10.0_m};
    const mantle_api::Vec3<units::length::meter_t> expected_point{2.1_m, 0.0_m, 0.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE_NEAR(expected_point, std::get<0>(pos_and_idx), orientation_accuracy);
    EXPECT_EQ(1, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenPolylineSlopedOnZAxis_WhenProjectingPointInBetweenFirstAndLastPoint_ThenPointOnPolylineIsReturned)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {-5.0_m, 0.0_m, 0.0_m},
        {95.0_m, 0.0_m, 100.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{70_m, 0.0_m, 100.0_m};
    const mantle_api::Vec3<units::length::meter_t> expected_point{70_m, 0.0_m, 75.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE_NEAR(expected_point, std::get<0>(pos_and_idx), orientation_accuracy);
    EXPECT_EQ(0, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenPointEqualToLastPolylinePoint_WhenGettingPosAndIdx_ThenPosIsLastPointAndIdxIsLast)
{
    ///                 x
    ///   x------x------x
    ///   1      2      3

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
        {3.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{3.0_m, 100.0_m, 10.0_m};

    const mantle_api::Vec3<units::length::meter_t> expected_closest_point = center_line_pts.back();

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE_NEAR(expected_closest_point, std::get<0>(pos_and_idx), orientation_accuracy);
    EXPECT_EQ(2, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline, GivenPointAfterPolyline_WhenGettingPosAndIdx_ThenPosIsLastPointAndIdxIsLast)
{
    ///   x------x------x    x
    ///   1      2      3    4

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
        {3.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{4.0_m, 100.0_m, 10.0_m};

    const mantle_api::Vec3<units::length::meter_t> expected_closest_point = center_line_pts.back();

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_TRIPLE_NEAR(expected_closest_point, std::get<0>(pos_and_idx), orientation_accuracy);
    EXPECT_EQ(2, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline, GivenPointLeftOfPolyline_WhenProjecting_ThenReturnsLeftOfPolyline)
{
    ///      x
    ///   x-----x
    ///   1     2

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{1.5_m, 10.0_m, 10.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_GT(std::get<2>(pos_and_idx), 0.0);
}

TEST(ProjectQueryPointOnPolyline, GivenPointOnPolyline_WhenProjecting_ThenReturnsOnPolyline)
{
    ///   x--x--x
    ///   1     2

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{1.5_m, 0.0_m, 10.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_EQ(0.0, std::get<2>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline, GivenPointRightOfPolyline_WhenProjecting_ThenReturnsRightOfPolyline)
{
    ///   1     2
    ///   x-----x
    ///      x

    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
    };

    const mantle_api::Vec3<units::length::meter_t> point{1.5_m, -10.0_m, 10.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point, center_line_pts);

    EXPECT_LT(std::get<2>(pos_and_idx), 0.0);
}

TEST(ProjectQueryPointOnPolyline, GivenCenterLine_WhenLookupPointIsNegativelyProjected_ThenNearestPointReturned)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts{
        {0.243939_m, 1640_m, 0_m}, {0.243938_m, 1640.25_m, 0_m}, {0.243937_m, 1640.5_m, 0_m}};

    mantle_api::Vec3<units::length::meter_t> expected_closest_point{0.243938293_m, 1640.250019273_m, 0.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(expected_closest_point, center_line_pts);

    EXPECT_TRIPLE(expected_closest_point, std::get<0>(pos_and_idx));
    EXPECT_EQ(1, std::get<1>(pos_and_idx));
}

TEST(
    ProjectQueryPointOnPolyline,
    GivenHalfCircleCenterLineFromNdsMap_WhenGettingProjectionAndIdxForPointNearBeginOfLane_ThenProjectionIsNearTheBegin)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts = {
        {97.4664_m, 66.6953_m, -0.149994_m}, {103.525_m, 68.6932_m, -0.170013_m},  {109.555_m, 70.289_m, -0.209991_m},
        {110.474_m, 70.507_m, -0.209991_m},  {113.607_m, 71.1178_m, -0.209991_m},  {119.308_m, 71.947_m, -0.200012_m},
        {122.675_m, 72.258_m, -0.179993_m},  {126.054_m, 72.4016_m, -0.160004_m},  {127.958_m, 72.3921_m, -0.140015_m},
        {130.868_m, 72.2678_m, -0.119995_m}, {133.776_m, 71.9942_m, -0.0899963_m}, {137.804_m, 71.3579_m, -0.0299988_m},
        {141.773_m, 70.4305_m, 0.0299988_m}, {145.729_m, 69.1855_m, 0.100006_m},   {149.593_m, 67.6949_m, 0.179993_m},
        {151.892_m, 66.6823_m, 0.230011_m},  {154.145_m, 65.5656_m, 0.269989_m},   {154.93_m, 65.1448_m, 0.290009_m},
        {157.412_m, 63.7095_m, 0.350006_m},  {159.822_m, 62.1692_m, 0.410004_m},   {162.168_m, 60.5054_m, 0.48999_m},
        {164.425_m, 58.7359_m, 0.570007_m},  {166.328_m, 57.1037_m, 0.649994_m},   {168.159_m, 55.3943_m, 0.720001_m},
        {171.624_m, 51.7543_m, 0.890015_m},  {174.202_m, 48.5969_m, 1.03_m},       {176.525_m, 45.2629_m, 1.17001_m},
        {177.962_m, 42.8965_m, 1.26999_m},   {179.289_m, 40.4704_m, 1.37_m},       {180.578_m, 37.8751_m, 1.47_m},
        {181.758_m, 35.2201_m, 1.57001_m},   {182.802_m, 32.5046_m, 1.67001_m},    {183.706_m, 29.7376_m, 1.78_m},
        {184.462_m, 26.9095_m, 1.88_m},      {185.076_m, 24.0394_m, 1.98999_m},    {185.567_m, 21.1556_m, 2.10001_m},
        {185.934_m, 18.2397_m, 2.22_m},      {186.084_m, 16.6034_m, 2.29001_m},    {186.237_m, 13.5868_m, 2.42001_m},
        {186.216_m, 10.555_m, 2.54001_m},    {186.027_m, 7.53612_m, 2.67001_m},    {185.67_m, 4.52078_m, 2.79001_m},
        {185.055_m, 1.03964_m, 2.94_m},      {184.246_m, -2.42016_m, 3.07999_m},   {182.926_m, -6.73684_m, 3.26001_m},
        {181.968_m, -9.28775_m, 3.37_m},     {180.89_m, -11.7961_m, 3.48001_m},    {179.712_m, -14.2706_m, 3.59_m},
        {178.426_m, -16.6835_m, 3.69_m},     {176.944_m, -19.1778_m, 3.81_m},      {175.328_m, -21.5927_m, 3.92001_m},
        {173.579_m, -23.9281_m, 4.03_m},     {171.728_m, -26.1739_m, 4.14001_m},   {170.943_m, -27.0494_m, 4.17999_m},
        {170.673_m, -27.3385_m, 4.20001_m},  {168.629_m, -29.4322_m, 4.29999_m},   {166.515_m, -31.4538_m, 4.39999_m},
        {164.564_m, -33.1713_m, 4.48001_m},  {160.602_m, -36.2633_m, 4.64999_m},   {158.517_m, -37.6963_m, 4.73001_m},
        {156.379_m, -39.0285_m, 4.82001_m},  {152.984_m, -40.8794_m, 4.95001_m},   {149.769_m, -42.3697_m, 5.08002_m},
        {146.472_m, -43.6576_m, 5.21002_m},  {143.851_m, -44.5027_m, 5.32001_m},   {141.187_m, -45.2_m, 5.41998_m},
        {135.786_m, -46.2521_m, 5.62_m},     {135.05_m, -46.3706_m, 5.64001_m},    {132.325_m, -46.7343_m, 5.71002_m},
        {129.585_m, -46.9866_m, 5.78003_m},  {126.827_m, -47.1089_m, 5.84003_m},   {124.076_m, -47.0817_m, 5.90002_m},
        {121.6_m, -46.9053_m, 5.95001_m},    {119.121_m, -46.617_m, 6_m},
    };

    mantle_api::Vec3<units::length::meter_t> point_to_lookup{101.593330_m, 69.962494_m, 0.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point_to_lookup, center_line_pts);

    EXPECT_EQ(0, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenHalfCircleCenterLineFromNdsMap_WhenGettingProjectionAndIdxForPointNearEndOfLane_ThenProjectionIsNearTheEnd)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts = {
        {97.4664_m, 66.6953_m, -0.149994_m}, {103.525_m, 68.6932_m, -0.170013_m},  {109.555_m, 70.289_m, -0.209991_m},
        {110.474_m, 70.507_m, -0.209991_m},  {113.607_m, 71.1178_m, -0.209991_m},  {119.308_m, 71.947_m, -0.200012_m},
        {122.675_m, 72.258_m, -0.179993_m},  {126.054_m, 72.4016_m, -0.160004_m},  {127.958_m, 72.3921_m, -0.140015_m},
        {130.868_m, 72.2678_m, -0.119995_m}, {133.776_m, 71.9942_m, -0.0899963_m}, {137.804_m, 71.3579_m, -0.0299988_m},
        {141.773_m, 70.4305_m, 0.0299988_m}, {145.729_m, 69.1855_m, 0.100006_m},   {149.593_m, 67.6949_m, 0.179993_m},
        {151.892_m, 66.6823_m, 0.230011_m},  {154.145_m, 65.5656_m, 0.269989_m},   {154.93_m, 65.1448_m, 0.290009_m},
        {157.412_m, 63.7095_m, 0.350006_m},  {159.822_m, 62.1692_m, 0.410004_m},   {162.168_m, 60.5054_m, 0.48999_m},
        {164.425_m, 58.7359_m, 0.570007_m},  {166.328_m, 57.1037_m, 0.649994_m},   {168.159_m, 55.3943_m, 0.720001_m},
        {171.624_m, 51.7543_m, 0.890015_m},  {174.202_m, 48.5969_m, 1.03_m},       {176.525_m, 45.2629_m, 1.17001_m},
        {177.962_m, 42.8965_m, 1.26999_m},   {179.289_m, 40.4704_m, 1.37_m},       {180.578_m, 37.8751_m, 1.47_m},
        {181.758_m, 35.2201_m, 1.57001_m},   {182.802_m, 32.5046_m, 1.67001_m},    {183.706_m, 29.7376_m, 1.78_m},
        {184.462_m, 26.9095_m, 1.88_m},      {185.076_m, 24.0394_m, 1.98999_m},    {185.567_m, 21.1556_m, 2.10001_m},
        {185.934_m, 18.2397_m, 2.22_m},      {186.084_m, 16.6034_m, 2.29001_m},    {186.237_m, 13.5868_m, 2.42001_m},
        {186.216_m, 10.555_m, 2.54001_m},    {186.027_m, 7.53612_m, 2.67001_m},    {185.67_m, 4.52078_m, 2.79001_m},
        {185.055_m, 1.03964_m, 2.94_m},      {184.246_m, -2.42016_m, 3.07999_m},   {182.926_m, -6.73684_m, 3.26001_m},
        {181.968_m, -9.28775_m, 3.37_m},     {180.89_m, -11.7961_m, 3.48001_m},    {179.712_m, -14.2706_m, 3.59_m},
        {178.426_m, -16.6835_m, 3.69_m},     {176.944_m, -19.1778_m, 3.81_m},      {175.328_m, -21.5927_m, 3.92001_m},
        {173.579_m, -23.9281_m, 4.03_m},     {171.728_m, -26.1739_m, 4.14001_m},   {170.943_m, -27.0494_m, 4.17999_m},
        {170.673_m, -27.3385_m, 4.20001_m},  {168.629_m, -29.4322_m, 4.29999_m},   {166.515_m, -31.4538_m, 4.39999_m},
        {164.564_m, -33.1713_m, 4.48001_m},  {160.602_m, -36.2633_m, 4.64999_m},   {158.517_m, -37.6963_m, 4.73001_m},
        {156.379_m, -39.0285_m, 4.82001_m},  {152.984_m, -40.8794_m, 4.95001_m},   {149.769_m, -42.3697_m, 5.08002_m},
        {146.472_m, -43.6576_m, 5.21002_m},  {143.851_m, -44.5027_m, 5.32001_m},   {141.187_m, -45.2_m, 5.41998_m},
        {135.786_m, -46.2521_m, 5.62_m},     {135.05_m, -46.3706_m, 5.64001_m},    {132.325_m, -46.7343_m, 5.71002_m},
        {129.585_m, -46.9866_m, 5.78003_m},  {126.827_m, -47.1089_m, 5.84003_m},   {124.076_m, -47.0817_m, 5.90002_m},
        {121.6_m, -46.9053_m, 5.95001_m},    {119.121_m, -46.617_m, 6_m},
    };

    mantle_api::Vec3<units::length::meter_t> point_to_lookup{132.588898_m, -52.505730_m, 0.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point_to_lookup, center_line_pts);

    EXPECT_EQ(center_line_pts.size() - 6, std::get<1>(pos_and_idx));
}

TEST(ProjectQueryPointOnPolyline,
     GivenHalfCircleCenterLineFromNdsMap_WhenGettingProjectionAndIdxForPointNearEndOfLane_ThenProjectionIs20)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts = {
        {97.4664_m, 66.6953_m, -0.149994_m}, {103.525_m, 68.6932_m, -0.170013_m},  {109.555_m, 70.289_m, -0.209991_m},
        {110.474_m, 70.507_m, -0.209991_m},  {113.607_m, 71.1178_m, -0.209991_m},  {119.308_m, 71.947_m, -0.200012_m},
        {122.675_m, 72.258_m, -0.179993_m},  {126.054_m, 72.4016_m, -0.160004_m},  {127.958_m, 72.3921_m, -0.140015_m},
        {130.868_m, 72.2678_m, -0.119995_m}, {133.776_m, 71.9942_m, -0.0899963_m}, {137.804_m, 71.3579_m, -0.0299988_m},
        {141.773_m, 70.4305_m, 0.0299988_m}, {145.729_m, 69.1855_m, 0.100006_m},   {149.593_m, 67.6949_m, 0.179993_m},
        {151.892_m, 66.6823_m, 0.230011_m},  {154.145_m, 65.5656_m, 0.269989_m},   {154.93_m, 65.1448_m, 0.290009_m},
        {157.412_m, 63.7095_m, 0.350006_m},  {159.822_m, 62.1692_m, 0.410004_m},   {162.168_m, 60.5054_m, 0.48999_m},
        {164.425_m, 58.7359_m, 0.570007_m},  {166.328_m, 57.1037_m, 0.649994_m},   {168.159_m, 55.3943_m, 0.720001_m},
        {171.624_m, 51.7543_m, 0.890015_m},  {174.202_m, 48.5969_m, 1.03_m},       {176.525_m, 45.2629_m, 1.17001_m},
        {177.962_m, 42.8965_m, 1.26999_m},   {179.289_m, 40.4704_m, 1.37_m},       {180.578_m, 37.8751_m, 1.47_m},
        {181.758_m, 35.2201_m, 1.57001_m},   {182.802_m, 32.5046_m, 1.67001_m},    {183.706_m, 29.7376_m, 1.78_m},
        {184.462_m, 26.9095_m, 1.88_m},      {185.076_m, 24.0394_m, 1.98999_m},    {185.567_m, 21.1556_m, 2.10001_m},
        {185.934_m, 18.2397_m, 2.22_m},      {186.084_m, 16.6034_m, 2.29001_m},    {186.237_m, 13.5868_m, 2.42001_m},
        {186.216_m, 10.555_m, 2.54001_m},    {186.027_m, 7.53612_m, 2.67001_m},    {185.67_m, 4.52078_m, 2.79001_m},
        {185.055_m, 1.03964_m, 2.94_m},      {184.246_m, -2.42016_m, 3.07999_m},   {182.926_m, -6.73684_m, 3.26001_m},
        {181.968_m, -9.28775_m, 3.37_m},     {180.89_m, -11.7961_m, 3.48001_m},    {179.712_m, -14.2706_m, 3.59_m},
        {178.426_m, -16.6835_m, 3.69_m},     {176.944_m, -19.1778_m, 3.81_m},      {175.328_m, -21.5927_m, 3.92001_m},
        {173.579_m, -23.9281_m, 4.03_m},     {171.728_m, -26.1739_m, 4.14001_m},   {170.943_m, -27.0494_m, 4.17999_m},
        {170.673_m, -27.3385_m, 4.20001_m},  {168.629_m, -29.4322_m, 4.29999_m},   {166.515_m, -31.4538_m, 4.39999_m},
        {164.564_m, -33.1713_m, 4.48001_m},  {160.602_m, -36.2633_m, 4.64999_m},   {158.517_m, -37.6963_m, 4.73001_m},
        {156.379_m, -39.0285_m, 4.82001_m},  {152.984_m, -40.8794_m, 4.95001_m},   {149.769_m, -42.3697_m, 5.08002_m},
        {146.472_m, -43.6576_m, 5.21002_m},  {143.851_m, -44.5027_m, 5.32001_m},   {141.187_m, -45.2_m, 5.41998_m},
        {135.786_m, -46.2521_m, 5.62_m},     {135.05_m, -46.3706_m, 5.64001_m},    {132.325_m, -46.7343_m, 5.71002_m},
        {129.585_m, -46.9866_m, 5.78003_m},  {126.827_m, -47.1089_m, 5.84003_m},   {124.076_m, -47.0817_m, 5.90002_m},
        {121.6_m, -46.9053_m, 5.95001_m},    {119.121_m, -46.617_m, 6_m},
    };

    mantle_api::Vec3<units::length::meter_t> point_to_lookup{165.031466520_m, 64.250068230_m, 1.240229168_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point_to_lookup, center_line_pts);

    EXPECT_EQ(20, std::get<1>(pos_and_idx));  // @todo: verify if it should actually be 20
}

TEST(ProjectQueryPointOnPolyline,
     GivenProblematicLaneFromNdsMap_WhenProjectingPoint_ThenPointShallNotBeThFirstPolylinePoint)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_pts = {
        {-14486.2_m, -7345.26_m, 30.62_m}, {-14492.3_m, -7347.14_m, 30.74_m}, {-14494_m, -7347.71_m, 30.81_m},
        {-14497.1_m, -7348.69_m, 30.95_m}, {-14501.5_m, -7349.96_m, 31.14_m}, {-14506.4_m, -7351.34_m, 31.28_m},
        {-14509.1_m, -7352.05_m, 31.36_m}, {-14514.7_m, -7353.41_m, 31.53_m}, {-14516.5_m, -7353.86_m, 31.58_m},
        {-14517.5_m, -7354.1_m, 31.59_m},  {-14519.6_m, -7354.6_m, 31.63_m},  {-14522.3_m, -7355.2_m, 31.64_m},
        {-14524.6_m, -7355.76_m, 31.65_m}, {-14525.6_m, -7355.99_m, 31.67_m}, {-14527.4_m, -7356.38_m, 31.73_m},
        {-14529.5_m, -7356.86_m, 31.81_m}, {-14532.4_m, -7357.46_m, 31.9_m},  {-14534.1_m, -7357.86_m, 31.96_m},
        {-14536.6_m, -7358.4_m, 32.02_m},  {-14539_m, -7359.01_m, 32.07_m},   {-14545.7_m, -7361.28_m, 32.18_m},
        {-14548.4_m, -7362.46_m, 32.23_m}, {-14549.6_m, -7363.09_m, 32.26_m}, {-14550.6_m, -7363.81_m, 32.28_m},
        {-14551.7_m, -7364.65_m, 32.31_m}, {-14552.7_m, -7365.6_m, 32.33_m},  {-14553.7_m, -7366.78_m, 32.36_m},
        {-14554.6_m, -7368.1_m, 32.4_m},   {-14555.2_m, -7369.25_m, 32.42_m}, {-14555.7_m, -7370.48_m, 32.45_m},
        {-14556.2_m, -7371.64_m, 32.48_m}, {-14556.5_m, -7373.04_m, 32.51_m}, {-14556.5_m, -7374.53_m, 32.53_m},
        {-14556.4_m, -7375.86_m, 32.55_m}, {-14556.1_m, -7377.16_m, 32.58_m}, {-14555_m, -7380.86_m, 32.65_m},
        {-14554.6_m, -7382.29_m, 32.68_m}, {-14553.7_m, -7385.19_m, 32.71_m}, {-14553.2_m, -7387.04_m, 32.73_m},
        {-14553_m, -7387.84_m, 32.73_m},
    };

    mantle_api::Vec3<units::length::meter_t> point_to_lookup{-14494.753906_m, -7349.565430_m, 0.0_m};

    const auto pos_and_idx = ProjectQueryPointOnPolyline(point_to_lookup, center_line_pts);

    EXPECT_EQ(2, std::get<1>(pos_and_idx));
}

}  // namespace astas::environment::map
