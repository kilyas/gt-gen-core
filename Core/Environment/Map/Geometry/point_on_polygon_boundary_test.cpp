/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/point_on_polygon_boundary.h"

#include "Core/Environment/Map/Geometry/polygon_shapes.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{

TEST(PointOnPolygonBoundaryTest, GivenPolygonWithDuplicatedPoint_WhenPointOnPolygonBoundary_ThenFalse)
{
    const glm::dvec2 query_position{-3.1415, 0.0};

    EXPECT_FALSE(PointOnPolygonBoundary({{-3.1415, 3.1415}, {-3.1415, 3.1415}}, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointInsideg_square_WhenPointOnPolygonBoundaryQuery_ThenFalse)
{
    const glm::dvec2 query_position{0, -5};

    EXPECT_FALSE(PointOnPolygonBoundary(kSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnHorizontalBottomEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{0.5, 0.0};

    EXPECT_TRUE(PointOnPolygonBoundary(kSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnHorizontalTopEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{0.5, 1.0};

    EXPECT_TRUE(PointOnPolygonBoundary(kSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnVerticalLeftEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{0.0, 0.5};

    EXPECT_TRUE(PointOnPolygonBoundary(kSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnVerticalRightEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{1.0, 0.5};

    EXPECT_TRUE(PointOnPolygonBoundary(kSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointInsideRotatedg_square_WhenPointOnPolygonBoundaryQuery_ThenFalse)
{
    const glm::dvec2 query_position{1.0, 1.0};

    EXPECT_FALSE(PointOnPolygonBoundary(kRotatedSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnLowerRightEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{2.5, 1.0};

    EXPECT_TRUE(PointOnPolygonBoundary(kRotatedSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnUpperRightEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{2.0, 2.5};

    EXPECT_TRUE(PointOnPolygonBoundary(kRotatedSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnLowerLeftEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{1.0, 0.5};

    EXPECT_TRUE(PointOnPolygonBoundary(kRotatedSquare, query_position));
}

TEST(PointOnPolygonBoundaryTest, GivenPointOnUpperLeftEdge_WhenPointOnPolygonBoundary_ThenTrue)
{
    const glm::dvec2 query_position{0.5, 2};

    EXPECT_TRUE(PointOnPolygonBoundary(kRotatedSquare, query_position));
}

}  // namespace astas::environment::map
