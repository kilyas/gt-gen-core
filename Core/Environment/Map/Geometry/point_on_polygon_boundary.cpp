/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/point_on_polygon_boundary.h"

#include <MantleAPI/Common/floating_point_helper.h>

#include <cstdlib>

namespace astas::environment::map
{

namespace
{

/// This algorithm has been found on stackoverflow as pseudo-code, transformed to c++ and adapted to our needs. See:
/// https://stackoverflow.com/questions/328107/how-can-you-determine-a-point-is-between-two-other-points-on-a-line-segment
inline bool IsPointBetween(const glm::dvec2& line_start, const glm::dvec2& line_end, const glm::dvec2& point_to_test)
{
    if (mantle_api::AlmostEqual(line_start.x, line_end.x, MANTLE_API_DEFAULT_EPS, true) &&
        mantle_api::AlmostEqual(line_start.y, line_end.y, MANTLE_API_DEFAULT_EPS, true))
    {
        return false;
    }

    double cross_product = (point_to_test.y - line_start.y) * (line_end.x - line_start.x) -
                           (point_to_test.x - line_start.x) * (line_end.y - line_start.y);
    if (!mantle_api::AlmostEqual(cross_product, 0.0, MANTLE_API_DEFAULT_EPS, true))
    {
        return false;
    }

    double dot_product = (point_to_test.x - line_start.x) * (line_end.x - line_start.x) +
                         (point_to_test.y - line_start.y) * (line_end.y - line_start.y);
    if (dot_product < 0)
    {
        return false;
    }

    double squared_length_b_a = (line_end.x - line_start.x) * (line_end.x - line_start.x) +
                                (line_end.y - line_start.y) * (line_end.y - line_start.y);
    return dot_product <= squared_length_b_a;
}

}  // namespace

bool PointOnPolygonBoundary(const std::vector<glm::dvec2>& poly, const glm::dvec2& pt)
{
    for (std::size_t i = 1; i < poly.size(); i++)
    {
        if (IsPointBetween(poly.at(i - 1), poly.at(i), pt))
        {
            return true;
        }
    }

    return IsPointBetween(poly.front(), poly.back(), pt);
}

}  // namespace astas::environment::map
