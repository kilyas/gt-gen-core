/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_GEOMETRY_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_GEOMETRY_H

#include "Core/Environment/Map/Geometry/bounding_box.h"
#include "Core/Environment/Map/Geometry/polygon.h"

#include <glm/vec2.hpp>

#include <optional>

namespace astas::environment::map
{

template <class T>
struct LineSegment
{
    T start_point;
    T end_point;
};

using LineSegment2d = LineSegment<glm::dvec2>;

/// @brief Given two line segments, this function checks if they are intersecting or not
/// Code inspired from: http://www.cplusplus.com/forum/beginner/49408/
bool IsLineSegmentIntersectingOtherLineSegment(const LineSegment2d& line1, const LineSegment2d& line2);

/// @brief Given a bounding box and a line segment, this function checks if both are intersecting or not
bool IsLineSegmentIntersectingBoundingBoxBoundaries(const BoundingBox2d& bounding_box, const LineSegment2d& line);

/// @brief Given a bounding box and a polygon, this function checks if they are intersecting or not
bool IsPolygonIntersectingBoundingBox(const BoundingBox2d& bounding_box, const Polygon2d& polygon);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_GEOMETRY_H
