/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_PROJECTQUERYPOINTONBOUNDARIES_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_PROJECTQUERYPOINTONBOUNDARIES_H

#include "Core/Environment/Map/AstasMap/lane_boundary.h"

#include <MantleAPI/Common/vector.h>

#include <vector>

namespace astas::environment::map
{

/// @brief Projects a point onto the closest lane boundary
/// @param query_point point to project
/// @param lane_boundaries lane boundaries to project onto
/// @return projected point and indicator if query_point is left or right of lane boundary (>0 -> left, <0 -> right)
std::pair<mantle_api::Vec3<units::length::meter_t>, double> ProjectQueryPointOnBoundaries(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const std::vector<const LaneBoundary*>& lane_boundaries);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_PROJECTQUERYPOINTONBOUNDARIES_H
