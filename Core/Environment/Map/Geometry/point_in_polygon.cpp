/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/point_in_polygon.h"

#include <MantleAPI/Common/floating_point_helper.h>

#include <cstdlib>

namespace astas::environment::map
{

namespace
{

inline bool IsSecondPointAbove(const glm::dvec2& first, const glm::dvec2& second)
{
    return first.y < second.y;
}

inline bool IsSecondPointBelowOrEqual(const glm::dvec2& first, const glm::dvec2& second)
{
    return first.y >= second.y;
}

inline bool IsToTheRightOfPoints(const glm::dvec2& pt, const glm::dvec2& point_i, const glm::dvec2& point_j)
{
    return (point_i.x <= pt.x || point_j.x <= pt.x);
}

inline bool IsIntersectionCandidate(const glm::dvec2& ray_origin,
                                    const glm::dvec2& segment_pt_0,
                                    const glm::dvec2& segment_pt_1)
{
    return ((IsSecondPointAbove(segment_pt_0, ray_origin) && IsSecondPointBelowOrEqual(segment_pt_1, ray_origin)) ||
            (IsSecondPointAbove(segment_pt_1, ray_origin) && IsSecondPointBelowOrEqual(segment_pt_0, ray_origin))) &&
           IsToTheRightOfPoints(ray_origin, segment_pt_0, segment_pt_1);
}
inline double GetIntersectionPointXComponent(const glm::dvec2& ray_origin,
                                             const glm::dvec2& segment_pt_0,
                                             const glm::dvec2& segment_pt_1)
{
    return segment_pt_0.x +
           ((ray_origin.y - segment_pt_0.y) / (segment_pt_1.y - segment_pt_0.y)) * (segment_pt_1.x - segment_pt_0.x);
}

inline bool IsRightFacingRayIntersectingLineSegment(const glm::dvec2& ray_origin,
                                                    const glm::dvec2& segment_pt_0,
                                                    const glm::dvec2& segment_pt_1)
{
    return GetIntersectionPointXComponent(ray_origin, segment_pt_0, segment_pt_1) < ray_origin.x;
}

}  // namespace

bool PointInPolygon(const std::vector<glm::dvec2>& poly, const glm::dvec2& pt)
{
    std::size_t j = poly.size() - 1;
    bool odd_nodes = false;

    for (std::size_t i = 0; i < poly.size(); i++)
    {
        const glm::dvec2& segment_pt_0 = poly[i];
        const glm::dvec2& segment_pt_1 = poly[j];

        if (IsIntersectionCandidate(pt, segment_pt_0, segment_pt_1))
        {
            odd_nodes ^=
                static_cast<std::uint8_t>(IsRightFacingRayIntersectingLineSegment(pt, segment_pt_0, segment_pt_1));
        }

        j = i;
    }

    return odd_nodes;
}

}  // namespace astas::environment::map
