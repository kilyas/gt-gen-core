/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTONPOLYGONBOUNDARY_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTONPOLYGONBOUNDARY_H

#include "glm/vec2.hpp"

#include <vector>

namespace astas::environment::map
{

/// @brief Checks whether the given position lies on the border of the given polygon.
///
/// @verbatim While the PointOnPolygon function does not always detect a point to be inside of a polygon
/// if it the point lies directly on a border, this function can be used in combination with the PointInPolygon
/// function to a point should also be considered inside, if it is on the border
bool PointOnPolygonBoundary(const std::vector<glm::dvec2>& poly, const glm::dvec2& pt);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTONPOLYGONBOUNDARY_H
