/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTUTILS_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTUTILS_H

#include <glm/vec2.hpp>
#include <units.h>

namespace astas::environment::map
{

bool ArePointsWithinDistance(const glm::dvec2& p1, const glm::dvec2& p2, const units::length::meter_t max_distance);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTUTILS_H
