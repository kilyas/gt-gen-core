/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/geometry.h"

#include "Core/Environment/Map/Geometry/bounding_box.h"

#include <MantleAPI/Common/floating_point_helper.h>

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/geometric.hpp"
#include "glm/gtx/norm.hpp"

namespace astas::environment::map
{

inline double PerpDot(const glm::dvec2& a, const glm::dvec2& b)
{
    return (a.y * b.x) - (a.x * b.y);
}

// Code inspired from: http://www.cplusplus.com/forum/beginner/49408/
bool IsLineSegmentIntersectingOtherLineSegment(const LineSegment2d& line1, const LineSegment2d& line2)
{
    const glm::dvec2 a(line1.end_point - line1.start_point);
    const glm::dvec2 b(line2.end_point - line2.start_point);

    const double perp_dot_a_b = PerpDot(a, b);
    if (mantle_api::AlmostEqual(perp_dot_a_b, 0.0, MANTLE_API_DEFAULT_EPS, true))  // lines are parallel
    {
        return false;
    }

    const glm::dvec2 c(line2.end_point - line1.end_point);
    const double perp_dot_a_c = PerpDot(a, c);
    const double perp_dot_b_c = PerpDot(b, c);

    if (perp_dot_a_b < 0x0)
    {
        if (perp_dot_a_c > 0.0 || perp_dot_b_c > 0.0 || perp_dot_a_c < perp_dot_a_b || perp_dot_b_c < perp_dot_a_b)
        {
            return false;
        }
    }
    else
    {
        if (perp_dot_a_c < 0.0 || perp_dot_b_c < 0.0 || perp_dot_a_c > perp_dot_a_b || perp_dot_b_c > perp_dot_a_b)
        {
            return false;
        }
    }

    return true;
}

bool IsLineSegmentIntersectingBoundingBoxBoundaries(const BoundingBox2d& bounding_box, const LineSegment2d& line)
{
    const glm::dvec2 bottom_left = bounding_box.bottom_left;
    const glm::dvec2 top_left = glm::dvec2(bounding_box.bottom_left.x, bounding_box.top_right.y);
    const glm::dvec2 top_right = bounding_box.top_right;
    const glm::dvec2 bottom_right = glm::dvec2(bounding_box.top_right.x, bounding_box.bottom_left.y);

    return (IsLineSegmentIntersectingOtherLineSegment(line, {bottom_left, top_left}) ||
            IsLineSegmentIntersectingOtherLineSegment(line, {top_left, top_right}) ||
            IsLineSegmentIntersectingOtherLineSegment(line, {top_right, bottom_right}) ||
            IsLineSegmentIntersectingOtherLineSegment(line, {bottom_right, bottom_left}));
}

bool IsPolygonIntersectingBoundingBox(const BoundingBox2d& bounding_box, const Polygon2d& polygon)
{
    for (std::size_t i = 1; i < polygon.points.size(); ++i)
    {
        const auto& pt1 = polygon.points.at(i - 1);
        const auto& pt2 = polygon.points.at(i);
        if (IsLineSegmentIntersectingBoundingBoxBoundaries(bounding_box, {pt1, pt2}))
        {
            return true;
        }
    }

    // close the polygon
    return IsLineSegmentIntersectingBoundingBoxBoundaries(bounding_box,
                                                          {polygon.points.front(), polygon.points.back()});
}

}  // namespace astas::environment::map
