/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/scalar_projection_2d.h"

#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"

namespace astas::environment::map
{

std::optional<double> ScalarProjection2d(mantle_api::Vec3<units::length::meter_t> query_point_2d,
                                         mantle_api::Vec3<units::length::meter_t> start_2d,
                                         mantle_api::Vec3<units::length::meter_t> end_2d)
{
    using units::literals::operator""_m;

    query_point_2d.z = 0_m;
    start_2d.z = 0_m;
    end_2d.z = 0_m;

    // This does a scalar projection in 2D for the case, that the cosine is not known, see:
    // https://en.wikipedia.org/wiki/Scalar_projection
    const double length_squared = service::glmwrapper::Distance2(start_2d, end_2d);
    if (mantle_api::AlmostEqual(length_squared, 0.0, MANTLE_API_DEFAULT_EPS, true))
    {
        return std::nullopt;
    }

    // s = a dot b / ||b||
    const auto a = query_point_2d - start_2d;
    const auto b = end_2d - start_2d;
    const double s = service::glmwrapper::Dot(a, b) / length_squared;
    return s;
}

}  // namespace astas::environment::map
