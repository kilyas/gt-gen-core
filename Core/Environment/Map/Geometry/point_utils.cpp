/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/point_utils.h"

#include <glm/geometric.hpp>

namespace astas::environment::map
{

using units::literals::operator""_m;

bool ArePointsWithinDistance(const glm::dvec2& p1, const glm::dvec2& p2, const units::length::meter_t max_distance)
{
    const auto distance{glm::distance(p2, p1)};

    return (distance <= max_distance());
}

}  // namespace astas::environment::map
