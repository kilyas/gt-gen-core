/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_BOUNDINGBOXUTILS_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_BOUNDINGBOXUTILS_H

#include "Core/Environment/Map/Geometry/bounding_box.h"
#include "glm/common.hpp"

namespace astas::environment::map
{

/// @brief Merges a bounding box with another point. The resulting bounding box encompasses the original bounding box
/// and the given point
template <typename T>
void MergeBoundingBoxWithPoint(BoundingBox<T>& bb, const T& point)
{
    bb.bottom_left = glm::min(bb.bottom_left, point);
    bb.top_right = glm::max(bb.top_right, point);
}

/// @brief Merges two bounding boxes. The resulting bounding box encompasses both bounding boxes
template <typename T>
BoundingBox<T> MergeBoundingBoxes(const BoundingBox<T>& bb1, const BoundingBox<T>& bb2)
{
    BoundingBox<T> result;
    result.bottom_left.x = std::min(bb1.bottom_left.x, bb2.bottom_left.x);
    result.bottom_left.y = std::min(bb1.bottom_left.y, bb2.bottom_left.y);
    result.top_right.x = std::max(bb1.top_right.x, bb2.top_right.x);
    result.top_right.y = std::max(bb1.top_right.y, bb2.top_right.y);

    return result;
}

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_BOUNDINGBOXUTILS_H
