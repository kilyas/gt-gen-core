/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTINPOLYGON_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTINPOLYGON_H

#include "glm/vec2.hpp"

#include <vector>

namespace astas::environment::map
{

/// @brief Checks whether the given position lies within the given polygon.
///
/// @verbatim This method counts the number of times a ray starting from a point P crosses a polygon boundary edge
/// separating it's inside and outside. If this number is even, then the point is outside; otherwise, when the crossing
/// number is odd, the point is inside.
///
/// Based on cn_PnPoly(), crossing number test for a point in a polygon, from
/// http://geomalgorithms.com/a03-_inclusion.html (see also https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html).
bool PointInPolygon(const std::vector<glm::dvec2>& poly, const glm::dvec2& pt);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POINTINPOLYGON_H
