/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/point_in_polygon.h"

#include "Core/Environment/Map/Geometry/polygon_shapes.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{

/// g_square
/// -------------------------------------------------------------------------------------------------------------
TEST(PointInPolygonTest, GivenPointWithinConvexPolygon_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{0.5, 0.5};

    EXPECT_TRUE(PointInPolygon(kSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOutsideConvexPolygon_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{1.5, 0.5};

    EXPECT_FALSE(PointInPolygon(kSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnHorizontalBottomEdge_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{0.5, 0.0};

    EXPECT_FALSE(PointInPolygon(kSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnHorizontalTopEdge_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{0.5, 1.0};

    EXPECT_TRUE(PointInPolygon(kSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnVerticalLeftEdge_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{0.0, 0.5};

    EXPECT_FALSE(PointInPolygon(kSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnVerticalRightEdge_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{1.0, 0.5};

    EXPECT_TRUE(PointInPolygon(kSquare, query_position));
}

/// g_square with inner dent
/// ---------------------------------------------------------------------------------------------
TEST(PointInPolygonTest, GivenPointInsideConcavePolygon_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{0.20, 0.75};

    EXPECT_TRUE(PointInPolygon(kSquareWithInnerDent, query_position));
}

TEST(PointInPolygonTest, GivenPointOutsideConcavePolygon_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{0.5, 0.75};

    EXPECT_FALSE(PointInPolygon(kSquareWithInnerDent, query_position));
}

/// Rotated g_square
/// -----------------------------------------------------------------------------------------------------
TEST(PointInPolygonTest, GivenPointOnTopVertexYLevelAndOutside_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{-1.0, 3.0};

    EXPECT_FALSE(PointInPolygon(kRotatedSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnTopVertex_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{1.0, 3.0};

    EXPECT_FALSE(PointInPolygon(kRotatedSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnBottomVertexYLevelAndOutside_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{-1.0, 0.0};

    EXPECT_FALSE(PointInPolygon(kRotatedSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnBottomVertex_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{2.0, 0.0};

    EXPECT_FALSE(PointInPolygon(kRotatedSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnLeftVertexYLevelAndOutside_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{-1.0, 1.0};

    EXPECT_FALSE(PointInPolygon(kRotatedSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnLeftVertex_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{0.0, 1.0};

    EXPECT_FALSE(PointInPolygon(kRotatedSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnRightVertexYLevelAndInside_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{2.0, 2.0};

    EXPECT_TRUE(PointInPolygon(kRotatedSquare, query_position));
}

TEST(PointInPolygonTest, GivenPointOnRightVertex_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{3.0, 2.0};

    EXPECT_TRUE(PointInPolygon(kRotatedSquare, query_position));
}

/// Greek cross --------------------------------------------------------------------------------------------------------
TEST(PointInPolygonTest, GivenPointOnLeftArmTopEdge_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{0.5, 2.0};

    EXPECT_TRUE(PointInPolygon(kGreekCross, query_position));
}

TEST(PointInPolygonTest, GivenPointOnRightArmTopEdge_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{2.5, 2.0};

    EXPECT_TRUE(PointInPolygon(kGreekCross, query_position));
}

TEST(PointInPolygonTest, GivenPointOnTopArmTopEdge_WhenPointInPolygonQuery_ThenTrue)
{
    const glm::dvec2 query_position{1.5, 3.0};

    EXPECT_TRUE(PointInPolygon(kGreekCross, query_position));
}

TEST(PointInPolygonTest, GivenPointOnLeftArmBottomEdge_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{0.5, 1.0};

    EXPECT_FALSE(PointInPolygon(kGreekCross, query_position));
}

TEST(PointInPolygonTest, GivenPointOnRightArmBottomEdge_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{2.5, 1.0};

    EXPECT_FALSE(PointInPolygon(kGreekCross, query_position));
}

TEST(PointInPolygonTest, GivenPointOnBottomArmBottomEdge_WhenPointInPolygonQuery_ThenFalse)
{
    const glm::dvec2 query_position{1.5, 0.0};

    EXPECT_FALSE(PointInPolygon(kGreekCross, query_position));
}

}  // namespace astas::environment::map
