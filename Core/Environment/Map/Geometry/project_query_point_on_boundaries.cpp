/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/lane_boundary.h"
#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"

#include <algorithm>

namespace astas::environment::map
{

std::pair<mantle_api::Vec3<units::length::meter_t>, double> ProjectQueryPointOnBoundaries(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const std::vector<const LaneBoundary*>& lane_boundaries)
{
    double current_minimum_distance = std::numeric_limits<double>::max();
    mantle_api::Vec3<units::length::meter_t> current_closest_point;
    double determinant{0.0};

    for (const auto* boundary : lane_boundaries)
    {
        if (boundary != nullptr && !boundary->points.empty())
        {
            std::vector<mantle_api::Vec3<units::length::meter_t>> polyline;
            polyline.reserve(boundary->points.size());
            std::transform(std::begin(boundary->points),
                           std::end(boundary->points),
                           std::back_inserter(polyline),
                           [](const LaneBoundary::Point& point) { return point.position; });
            const auto projected_boundary_point_tuple = ProjectQueryPointOnPolyline(query_point, polyline);
            const auto projected_boundary_point = std::get<0>(projected_boundary_point_tuple);
            const auto distance = service::glmwrapper::Distance(query_point, projected_boundary_point);

            if (distance < current_minimum_distance)
            {
                current_minimum_distance = distance;
                current_closest_point = projected_boundary_point;
                determinant = std::get<2>(projected_boundary_point_tuple);
            }
        }
    }

    return {current_closest_point, determinant};
}

}  // namespace astas::environment::map
