/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/point_utils.h"

#include <gtest/gtest.h>
#include <units.h>

namespace astas::environment::map
{

using units::literals::operator""_m;

struct ArePointsWithinDistanceTestType
{
    glm::dvec2 p1;
    glm::dvec2 p2;
    units::length::meter_t distance;
    bool expected_points_within_distance;
};

using ArePointsWithinDistanceTest = testing::TestWithParam<ArePointsWithinDistanceTestType>;

INSTANTIATE_TEST_SUITE_P(PointUtils,
                         ArePointsWithinDistanceTest,
                         testing::Values(ArePointsWithinDistanceTestType{{0.0, 0.0}, {0.0, 0.0}, 0_m, true},
                                         ArePointsWithinDistanceTestType{{1.0, 0.0}, {2.0, 0.0}, 1.0_m, true},
                                         ArePointsWithinDistanceTestType{{1.0, 0.0}, {2.0, 0.0}, 0.1_m, false},
                                         ArePointsWithinDistanceTestType{{0.0, 0.0}, {-1.0, -1.0}, 1.5_m, true},
                                         ArePointsWithinDistanceTestType{{0.0, 0.0}, {-1.0, -1.0}, 1.4_m, false},
                                         ArePointsWithinDistanceTestType{{0.0, -1.0}, {0.0, -2.0}, 1.0_m, true},
                                         ArePointsWithinDistanceTestType{{0.0, -1.0}, {0.0, -2.0}, 0.1_m, false}));

TEST_P(ArePointsWithinDistanceTest, GivenTwoPoints_WhenPointsAreWithinCertainDistance_ThenTrue)
{
    const auto test_vector = GetParam();

    const bool are_points_within_distance{
        ArePointsWithinDistance(test_vector.p1, test_vector.p2, test_vector.distance)};

    EXPECT_EQ(test_vector.expected_points_within_distance, are_points_within_distance);
}

}  // namespace astas::environment::map
