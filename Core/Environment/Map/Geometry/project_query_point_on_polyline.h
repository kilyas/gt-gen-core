/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_PROJECTQUERYPOINTONPOLYLINE_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_PROJECTQUERYPOINTONPOLYLINE_H

#include <MantleAPI/Common/vector.h>

#include <optional>
#include <vector>

namespace astas::environment::map
{

/// @brief Projects a point onto the closest polyline segment
/// @param query_point point to project
/// @param polyline polyline to project onto
/// @return 1. projected point. If point is behind/in front of polyline, then it is projected onto the first/last point
///            of the polyline.
///         2. index of start point of closest polyline segment. If point is behind polyline, then index of last
///            polyline point
///         3. indicator if query_point is left or right of polyline (>0 -> left, <0 -> right)
std::tuple<mantle_api::Vec3<units::length::meter_t>, std::size_t, double> ProjectQueryPointOnPolyline(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_PROJECTQUERYPOINTONPOLYLINE_H
