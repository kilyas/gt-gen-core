/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/bounding_box_utils.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{
class BoundingBoxUtilsTest : public testing::Test
{
  protected:
    BoundingBoxUtilsTest() { original_bounding_box_ = outer_bounding_box_; }

    void CheckEquivalenceBoundingBox(const BoundingBox2d& expected, const BoundingBox2d& actual)
    {
        EXPECT_EQ(expected.bottom_left.x, actual.bottom_left.x);
        EXPECT_EQ(expected.bottom_left.y, actual.bottom_left.y);
        EXPECT_EQ(expected.top_right.x, actual.top_right.x);
        EXPECT_EQ(expected.top_right.y, actual.top_right.y);
    }

    /* Note: Bounding boxes and points are shown on below Cartesian 2d coordinate system,
     *       where 'X' denotes points and RECTANGLE surrounded by dashed lines denotes bounding boxes.
     *       Below test cases will cover every possibilities for which can be happened.
     * Y
     * ^
     * |           -----------bb1-----------
     * |           |                       |
     * |           X    ---bb2---          |----bb3----
     * |           |    |       |    X     |          |
     * |           |    ---------          |          |
     * |    -------|-----                  |-----------
     * |    |      |    |                  |
     * |    |      -----|-------------------
     * |    |           |         ----bb5-----
     * |    |           |   X     |          |
     * |    -----bb4-----         |          |
     * |                          ------------
     * -------------------------------------------------> X
     */

    BoundingBox2d outer_bounding_box_{{300, 300}, {600, 600}};        // bb1
    BoundingBox2d inner_bounding_box_{{400, 400}, {500, 500}};        // bb2
    BoundingBox2d adjacent_bounding_box_{{600, 400}, {800, 500}};     // bb3
    BoundingBox2d intersected_bounding_box_{{100, 100}, {400, 400}};  // bb4
    BoundingBox2d separated_bounding_box_{{400, 100}, {700, 200}};    // bb5

    BoundingBox2d original_bounding_box_;  // equivalent to outer_bounding_box_
};

TEST_F(BoundingBoxUtilsTest, GivenBoundingBox_WhenMergeWithInteriorPoint_ThenBoundingBoxIsNotChanged)
{
    BoundingBox2d actual_bounding_box = original_bounding_box_;
    BoundingBox2d expected_bounding_box = original_bounding_box_;

    glm::dvec2 interior_point((original_bounding_box_.bottom_left.x + original_bounding_box_.top_right.x) / 2,
                              (original_bounding_box_.bottom_left.y + original_bounding_box_.top_right.y) / 2);
    MergeBoundingBoxWithPoint(expected_bounding_box, interior_point);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

TEST_F(BoundingBoxUtilsTest, GivenBoundingBox_WhenMergeWithPointOnBoundary_ThenBoundingBoxIsNotChanged)
{
    BoundingBox2d actual_bounding_box = original_bounding_box_;
    BoundingBox2d expected_bounding_box = original_bounding_box_;

    glm::dvec2 point_on_boundary;
    point_on_boundary.x = expected_bounding_box.bottom_left.x;
    point_on_boundary.y = expected_bounding_box.bottom_left.y;
    MergeBoundingBoxWithPoint(expected_bounding_box, point_on_boundary);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

TEST_F(BoundingBoxUtilsTest, GivenBoundingBox_WhenMergeWithExteriorPoint_ThenBoundingBoxIsExpectedlyUpdated)
{
    BoundingBox2d expected_bounding_box = original_bounding_box_;
    const glm::dvec2 pad{100, 100};
    glm::dvec2 exterior_point{expected_bounding_box.top_right + pad};
    BoundingBox2d actual_bounding_box{{original_bounding_box_.bottom_left}, {original_bounding_box_.top_right + pad}};

    MergeBoundingBoxWithPoint(expected_bounding_box, exterior_point);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

TEST_F(BoundingBoxUtilsTest, GivenBoundingBoxes_WhenMergeWithIdenticalBoundingBox_ThenBoundingBoxIsNotChanged)
{
    BoundingBox2d actual_bounding_box = original_bounding_box_;
    BoundingBox2d expected_bounding_box = original_bounding_box_;

    expected_bounding_box = MergeBoundingBoxes(expected_bounding_box, expected_bounding_box);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

TEST_F(BoundingBoxUtilsTest, GivenBoundingBoxes_WhenMergeWithBoundingBoxPerfectlyContained_ThenBoundingBoxIsNotChanged)
{
    BoundingBox2d actual_bounding_box = outer_bounding_box_;

    BoundingBox2d expected_bounding_box = MergeBoundingBoxes(outer_bounding_box_, inner_bounding_box_);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

TEST_F(BoundingBoxUtilsTest,
       GivenBoundingBoxes_WhenMergeWithBoundingBoxPlacedAdjacent_ThenBoundingBoxIsExpectedlyUpdated)
{
    BoundingBox2d actual_bounding_box{{original_bounding_box_.bottom_left.x, original_bounding_box_.bottom_left.y},
                                      {adjacent_bounding_box_.top_right.x, original_bounding_box_.top_right.y}};

    BoundingBox2d expected_bounding_box = MergeBoundingBoxes(original_bounding_box_, adjacent_bounding_box_);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

TEST_F(BoundingBoxUtilsTest,
       GivenBoundingBoxes_WhenMergeWithBoundingBoxPartiallyContained_ThenBoundingBoxIsExpectedlyUpdated)
{
    BoundingBox2d actual_bounding_box{
        {intersected_bounding_box_.bottom_left.x, intersected_bounding_box_.bottom_left.y},
        {original_bounding_box_.top_right.x, original_bounding_box_.top_right.y}};

    BoundingBox2d expected_bounding_box = MergeBoundingBoxes(original_bounding_box_, intersected_bounding_box_);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

TEST_F(BoundingBoxUtilsTest,
       GivenBoundingBoxes_WhenMergeWithBoundingBoxPerfectlySeparated_ThenBoundingBoxIsExpectedlyUpdated)
{
    BoundingBox2d actual_bounding_box{{original_bounding_box_.bottom_left.x, separated_bounding_box_.bottom_left.y},
                                      {separated_bounding_box_.top_right.x, original_bounding_box_.top_right.y}};

    BoundingBox2d expected_bounding_box = MergeBoundingBoxes(original_bounding_box_, separated_bounding_box_);

    SCOPED_TRACE(::testing::UnitTest::GetInstance()->current_test_info()->name());
    CheckEquivalenceBoundingBox(expected_bounding_box, actual_bounding_box);
}

}  // namespace astas::environment::map
