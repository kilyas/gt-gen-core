/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_BOUNDINGBOX_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_BOUNDINGBOX_H

#include "glm/vec2.hpp"

namespace astas::environment::map
{

template <typename T>
struct BoundingBox
{
    T bottom_left{std::numeric_limits<double>::max()};
    T top_right{std::numeric_limits<double>::lowest()};
};

using BoundingBox2d = BoundingBox<glm::dvec2>;

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_BOUNDINGBOX_H
