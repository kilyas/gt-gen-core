/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"

#include "Core/Environment/Map/Geometry/scalar_projection_2d.h"
#include "Core/Service/GlmWrapper/glm_basic_scalar_utils.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"

namespace astas::environment::map
{

std::tuple<mantle_api::Vec3<units::length::meter_t>, std::size_t, double> ProjectQueryPointOnPolyline(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline)
{
    double min_distance = std::numeric_limits<double>::max();

    mantle_api::Vec3<units::length::meter_t> closest_point{polyline.front()};
    std::size_t idx{0};
    double determinant{0.0};
    for (std::size_t i = 0; i < polyline.size() - 1; ++i)
    {
        const auto p0 = polyline[i];
        const auto p1 = polyline[i + 1];

        auto projection_ratio = ScalarProjection2d(query_point, p0, p1);

        if (projection_ratio.has_value())
        {
            const double clamped_projection_ratio = service::glmwrapper::Clamp(projection_ratio.value(), 0.0, 1.0);

            mantle_api::Vec3<units::length::meter_t> projection;
            std::size_t projection_index = i;

            if (mantle_api::AlmostEqual(clamped_projection_ratio, 0.0, MANTLE_API_DEFAULT_EPS, true))
            {
                projection = p0;
            }
            else if (mantle_api::AlmostEqual(clamped_projection_ratio, 1.0, MANTLE_API_DEFAULT_EPS, true))
            {
                projection = p1;
                projection_index = i + 1;
            }
            else
            {
                projection = p0 + clamped_projection_ratio * (p1 - p0);
            }

            double cur_distance = service::glmwrapper::Distance2(query_point, projection);
            if (cur_distance < min_distance)
            {
                min_distance = cur_distance;
                closest_point = projection;
                idx = projection_index;
                determinant = ((p1.x - p0.x) * (query_point.y - p0.y) - (query_point.x - p0.x) * (p1.y - p0.y)).value();
            }
        }
    }

    return {closest_point, idx, determinant};
}

}  // namespace astas::environment::map
