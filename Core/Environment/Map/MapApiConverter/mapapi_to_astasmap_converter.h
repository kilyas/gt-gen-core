/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_MAPAPITOASTASMAPCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_MAPAPITOASTASMAPCONVERTER_H

#include "Core/Environment/Map/Common/i_any_to_astasmap_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/mapapi_to_astasmap_converter_impl.h"

#include <MapAPI/map.h>

namespace astas::environment::map
{

class MapApiToAstasMapConverter : public IAnyToAstasMapConverter
{
  public:
    MapApiToAstasMapConverter(service::utility::UniqueIdProvider& unique_id_provider,
                              const map_api::Map& data,
                              AstasMap& astas_map);

    MapApiToAstasMapConverter() = delete;
    MapApiToAstasMapConverter(const MapApiToAstasMapConverter&) = delete;
    MapApiToAstasMapConverter(MapApiToAstasMapConverter&&) = delete;
    MapApiToAstasMapConverter& operator=(const MapApiToAstasMapConverter&) = delete;
    MapApiToAstasMapConverter& operator=(MapApiToAstasMapConverter&&) = delete;
    ~MapApiToAstasMapConverter() override;

    /// @copydoc IAnyToAstasMapConverter::Convert()
    void Convert() override;

    /// @copydoc IAnyToAstasMapConverter::GetNativeToAstasTrafficLightIdMap()
    std::map<mantle_api::UniqueId, mantle_api::UniqueId> GetNativeToAstasTrafficLightIdMap() const override;

  private:
    std::unique_ptr<MapApiToAstasMapConverterImpl> impl_;
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_MAPAPITOASTASMAPCONVERTER_H
