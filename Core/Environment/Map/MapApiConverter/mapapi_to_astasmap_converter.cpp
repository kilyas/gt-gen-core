/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/mapapi_to_astasmap_converter.h"

namespace astas::environment::map
{

MapApiToAstasMapConverter::MapApiToAstasMapConverter(service::utility::UniqueIdProvider& unique_id_provider,
                                                     const map_api::Map& data,
                                                     AstasMap& astas_map)
    : impl_{std::make_unique<MapApiToAstasMapConverterImpl>(unique_id_provider, data, astas_map)}
{
}

MapApiToAstasMapConverter::~MapApiToAstasMapConverter() = default;

void MapApiToAstasMapConverter::Convert()
{
    impl_->Convert();
}

std::map<mantle_api::UniqueId, mantle_api::UniqueId> MapApiToAstasMapConverter::GetNativeToAstasTrafficLightIdMap()
    const
{
    return {};
}

}  // namespace astas::environment::map
