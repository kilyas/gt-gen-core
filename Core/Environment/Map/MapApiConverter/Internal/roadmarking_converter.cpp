/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/roadmarking_converter.h"

#include "Core/Environment/Map/MapApiConverter/Internal/roadmarking_types_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_converter.h"
#include "Core/Service/Logging/logging.h"

namespace astas::environment::map
{

GroundSign ConvertRoadMarking(const map_api::RoadMarking& from)
{
    GroundSign result;
    result.id = from.id;

    result.marking_type = ConvertRoadMarkingType(from.type);
    result.marking_color = ConvertRoadMarkingColor(from.monochrome_color);

    result.stvo_id = from.code;
    if (!from.sub_code.empty())
    {
        result.stvo_id += "-";
        result.stvo_id += from.sub_code;
    }

    result.type = ConvertMainSignType(from.traffic_main_sign_type);
    result.value_information = ConvertTrafficSignValue(from.value);
    if (!from.value_text.empty())
    {
        result.value_information.text = from.value_text;
    }

    result.pose = mantle_api::Pose{.position = from.base.position, .orientation = from.base.orientation};
    result.dimensions = from.base.dimension;

    for (const auto& assigned_lane : from.assigned_lanes)
    {
        result.assigned_lanes.emplace_back(assigned_lane.get().id);
    }

    return result;
}

}  // namespace astas::environment::map
