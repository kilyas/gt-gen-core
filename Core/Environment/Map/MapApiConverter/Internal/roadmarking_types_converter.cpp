/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/roadmarking_types_converter.h"

#include "Core/Service/Logging/logging.h"

namespace astas::environment::map
{
osi::OsiRoadMarkingsType ConvertRoadMarkingType(const map_api::RoadMarking::Type& from)
{
    switch (from)
    {
        case map_api::RoadMarking::Type::kUnknown:
            return osi::OsiRoadMarkingsType::kUnknown;
        case map_api::RoadMarking::Type::kOther:
            return osi::OsiRoadMarkingsType::kOther;
        case map_api::RoadMarking::Type::kPaintedTrafficSign:
            return osi::OsiRoadMarkingsType::kPaintedTrafficSign;
        case map_api::RoadMarking::Type::kSymbolicTrafficSign:
            return osi::OsiRoadMarkingsType::kSymbolicTrafficSign;
        case map_api::RoadMarking::Type::kTextualTrafficSign:
            return osi::OsiRoadMarkingsType::kTextualTrafficSign;
        case map_api::RoadMarking::Type::kGenericSymbol:
            return osi::OsiRoadMarkingsType::kGenericSymbol;
        case map_api::RoadMarking::Type::kGenericLine:
            return osi::OsiRoadMarkingsType::kGenericLine;
        case map_api::RoadMarking::Type::kGenericText:
            return osi::OsiRoadMarkingsType::kGenericText;
        default:
            Warn("The given map_api::RoadMarking::Type value={} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return osi::OsiRoadMarkingsType::kUnknown;
    }
}

osi::OsiRoadMarkingColor ConvertRoadMarkingColor(const map_api::RoadMarking::Color& from)
{
    switch (from)
    {
        case map_api::RoadMarking::Color::kUnknown:
            return osi::OsiRoadMarkingColor::kUnknown;
        case map_api::RoadMarking::Color::kOther:
            return osi::OsiRoadMarkingColor::kOther;
        case map_api::RoadMarking::Color::kWhite:
            return osi::OsiRoadMarkingColor::kWhite;
        case map_api::RoadMarking::Color::kYellow:
            return osi::OsiRoadMarkingColor::kYellow;
        case map_api::RoadMarking::Color::kBlue:
            return osi::OsiRoadMarkingColor::kBlue;
        case map_api::RoadMarking::Color::kRed:
            return osi::OsiRoadMarkingColor::kRed;
        case map_api::RoadMarking::Color::kGreen:
            return osi::OsiRoadMarkingColor::kGreen;
        case map_api::RoadMarking::Color::kViolet:
            return osi::OsiRoadMarkingColor::kViolet;
        case map_api::RoadMarking::Color::kOrange:
            return osi::OsiRoadMarkingColor::kOrange;
        default:
            Warn("The given map_api::RoadMarking::Color value={} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return osi::OsiRoadMarkingColor::kUnknown;
    }
}

}  // namespace astas::environment::map
