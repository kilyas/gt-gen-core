/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/lane_converter.h"

namespace astas::environment::map
{

static LaneFlags ConvertLaneFlags(const map_api::Lane::Type& type, const map_api::Lane::Subtype& sub_type)
{
    LaneFlags flags;

    if (type == map_api::Lane::Type::kDriving || type == map_api::Lane::Type::kIntersection)
    {
        flags.SetDrivable();
    }

    if (sub_type == map_api::Lane::Subtype::kStop || sub_type == map_api::Lane::Subtype::kShoulder ||
        sub_type == map_api::Lane::Subtype::kBorder || sub_type == map_api::Lane::Subtype::kRestricted)
    {
        flags.SetShoulderLane();
    }

    if (sub_type == map_api::Lane::Subtype::kNormal)
    {
        flags.SetNormalLane();
    }

    if (sub_type == map_api::Lane::Subtype::kEntry)
    {
        flags.SetMergeLane();
        flags.SetEntryLane();
    }

    if (sub_type == map_api::Lane::Subtype::kExit)
    {
        flags.SetSplitLane();
        flags.SetExitLane();
    }

    if (sub_type == map_api::Lane::Subtype::kParking)
    {
        flags.SetParking();
    }

    if (sub_type == map_api::Lane::Subtype::kBiking)
    {
        flags.SetBicycle();
    }

    return flags;
}

Lane ConvertLane(const map_api::Lane& from)
{
    Lane to{static_cast<mantle_api::UniqueId>(from.id)};

    to.center_line = from.centerline;

    for (const auto& antecessor_lane : from.antecessor_lanes)
    {
        to.predecessors.push_back(antecessor_lane.get().id);
    }

    for (const auto& successor_lane : from.successor_lanes)
    {
        to.successors.push_back(successor_lane.get().id);
    }

    for (const auto& left_adjacent_lane : from.left_adjacent_lanes)
    {
        to.left_adjacent_lanes.push_back(left_adjacent_lane.get().id);
    }

    for (const auto& right_adjacent_lane : from.right_adjacent_lanes)
    {
        to.right_adjacent_lanes.push_back(right_adjacent_lane.get().id);
    }

    for (const auto& left_lane_boundary : from.left_lane_boundaries)
    {
        to.left_lane_boundaries.push_back(left_lane_boundary.get().id);
    }

    for (const auto& right_lane_boundary : from.right_lane_boundaries)
    {
        to.right_lane_boundaries.push_back(right_lane_boundary.get().id);
    }

    to.flags = ConvertLaneFlags(from.type, from.sub_type);

    return to;
}

}  // namespace astas::environment::map
