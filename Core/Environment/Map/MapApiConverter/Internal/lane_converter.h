/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANECONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANECONVERTER_H

#include "Core/Environment/Map/AstasMap/lane.h"

#include <MapAPI/lane.h>

namespace astas::environment::map
{
Lane ConvertLane(const map_api::Lane& from);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_LANECONVERTER_H
