/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/roadmarking_types_converter.h"

#include <gmock/gmock.h>

namespace astas::environment::map::map
{

TEST(ConvertRoadMarkingTypeToOsiTest, GivenRoadMarkingType_WhenConvert_ThenOsiRoadMarkingTypeIsCorrect)
{
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kUnknown), osi::OsiRoadMarkingsType::kUnknown);
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kOther), osi::OsiRoadMarkingsType::kOther);
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kPaintedTrafficSign),
              osi::OsiRoadMarkingsType::kPaintedTrafficSign);
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kSymbolicTrafficSign),
              osi::OsiRoadMarkingsType::kSymbolicTrafficSign);
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kTextualTrafficSign),
              osi::OsiRoadMarkingsType::kTextualTrafficSign);
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kGenericSymbol),
              osi::OsiRoadMarkingsType::kGenericSymbol);
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kGenericLine), osi::OsiRoadMarkingsType::kGenericLine);
    EXPECT_EQ(ConvertRoadMarkingType(map_api::RoadMarking::Type::kGenericText), osi::OsiRoadMarkingsType::kGenericText);
}

TEST(ConvertRoadMarkingTypeToOsiTest,
     GivenUnsupportedRoadMarkingType_WhenConvert_ThenOsiRoadMarkingTypeIsUnknownWithWarningMessage)
{
    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertRoadMarkingType(static_cast<map_api::RoadMarking::Type>(255)), osi::OsiRoadMarkingsType::kUnknown);
    EXPECT_THAT(testing::internal::GetCapturedStdout(),
                testing::HasSubstr("The given map_api::RoadMarking::Type value=255 cannot be converted! "
                                   "Return Unknown as default."));
}

TEST(ConvertRoadMarkingColorToOsiTest, GivenRoadMarkingColor_WhenConvert_ThenOsiRoadMarkingColorIsCorrect)
{
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kUnknown), osi::OsiRoadMarkingColor::kUnknown);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kOther), osi::OsiRoadMarkingColor::kOther);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kWhite), osi::OsiRoadMarkingColor::kWhite);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kYellow), osi::OsiRoadMarkingColor::kYellow);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kBlue), osi::OsiRoadMarkingColor::kBlue);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kRed), osi::OsiRoadMarkingColor::kRed);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kGreen), osi::OsiRoadMarkingColor::kGreen);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kViolet), osi::OsiRoadMarkingColor::kViolet);
    EXPECT_EQ(ConvertRoadMarkingColor(map_api::RoadMarking::Color::kOrange), osi::OsiRoadMarkingColor::kOrange);
}

TEST(ConvertRoadMarkingColorToOsiTest,
     GivenUnsupportedRoadMarkingColor_WhenConvert_ThenOsiRoadMarkingColorIsUnknownWithWarningMessage)
{
    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertRoadMarkingColor(static_cast<map_api::RoadMarking::Color>(255)),
              osi::OsiRoadMarkingColor::kUnknown);
    EXPECT_THAT(testing::internal::GetCapturedStdout(),
                testing::HasSubstr("The given map_api::RoadMarking::Color value=255 cannot be converted! "
                                   "Return Unknown as default."));
}

}  // namespace astas::environment::map::map
