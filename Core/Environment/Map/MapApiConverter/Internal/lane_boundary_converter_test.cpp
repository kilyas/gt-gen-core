/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/lane_boundary_converter.h"

#include <gmock/gmock.h>

namespace astas::environment::map::map
{
using units::literals::operator""_m;

TEST(LaneBoundaryColorConverterTest,
     GivenMapApiLaneBoundaryColor_WhenConvertToAstasLaneBoundaryColor_ThenConversionIsCorrect)
{
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kUnknown), LaneBoundary::Color::kUnknown);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kOther), LaneBoundary::Color::kOther);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kNone), LaneBoundary::Color::kNone);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kWhite), LaneBoundary::Color::kWhite);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kYellow), LaneBoundary::Color::kYellow);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kRed), LaneBoundary::Color::kRed);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kBlue), LaneBoundary::Color::kBlue);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kGreen), LaneBoundary::Color::kGreen);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kViolet), LaneBoundary::Color::kViolet);
    EXPECT_EQ(ConvertLaneBoundaryColor(map_api::LaneBoundary::Color::kOrange), LaneBoundary::Color::kOrange);
    EXPECT_EQ(ConvertLaneBoundaryColor(static_cast<map_api::LaneBoundary::Color>(999)), LaneBoundary::Color::kUnknown);

    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertLaneBoundaryColor(static_cast<map_api::LaneBoundary::Color>(255)), LaneBoundary::Color::kUnknown);
    EXPECT_THAT(testing::internal::GetCapturedStdout(),
                testing::HasSubstr(
                    "The given map_api::LaneBoundary::Color 255 cannot be converted! Return Unknown as default."));
}

TEST(LaneBoundaryTypeConverterTest,
     GivenMapApiLaneBoundaryType_WhenConvertToAstasLaneBoundaryType_ThenConversionIsCorrect)
{
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kUnknown), LaneBoundary::Type::kUnknown);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kOther), LaneBoundary::Type::kOther);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kNoLine), LaneBoundary::Type::kNoLine);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kSolidLine), LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kDashedLine), LaneBoundary::Type::kDashedLine);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kBottsDots), LaneBoundary::Type::kBottsDots);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kRoadEdge), LaneBoundary::Type::kRoadEdge);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kSnowEdge), LaneBoundary::Type::kSnowEdge);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kGrassEdge), LaneBoundary::Type::kGrassEdge);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kGravelEdge), LaneBoundary::Type::kGravelEdge);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kSoilEdge), LaneBoundary::Type::kSoilEdge);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kGuardRail), LaneBoundary::Type::kGuardRail);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kCurb), LaneBoundary::Type::kCurb);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kStructure), LaneBoundary::Type::kStructure);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kBarrier), LaneBoundary::Type::kBarrier);
    EXPECT_EQ(ConvertLaneBoundaryType(map_api::LaneBoundary::Type::kSoundBarrier), LaneBoundary::Type::kBarrier);

    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertLaneBoundaryType(static_cast<map_api::LaneBoundary::Type>(255)), LaneBoundary::Type::kUnknown);
    EXPECT_THAT(testing::internal::GetCapturedStdout(),
                testing::HasSubstr(
                    "The given map_api::LaneBoundary::Type 255 cannot be converted! Return Unknown as default."));
}

TEST(LaneBoundaryPointConverterTest,
     GivenMapApiLaneBoundaryPointe_WhenConvertToAstasLaneBoundaryPoint_ThenConversionIsCorrect)
{
    // arrange
    const auto input = map_api::LaneBoundary::BoundaryPoint{{1.0_m, 2.0_m, 3.0_m}, 4.0_m, 5.0_m};

    // act
    LaneBoundary::Point result = ConvertBoundaryPoint(input);

    // assert
    EXPECT_EQ(result.position, input.position);
    EXPECT_EQ(result.width, input.width());
    EXPECT_EQ(result.height, input.height());
}

TEST(LaneBoundaryConverterTest, GivenMapApiLaneBoundary_WhenConvertToAstasLaneBoundary_ThenConversionIsCorrect)
{

    map_api::LaneBoundary input;
    input.id = 123;
    input.type = map_api::LaneBoundary::Type::kDashedLine;
    input.color = map_api::LaneBoundary::Color::kWhite;
    input.boundary_line.push_back(map_api::LaneBoundary::BoundaryPoint{{1.0_m, 2.0_m, 3.0_m}, 4.0_m, 5.0_m});
    input.boundary_line.push_back(map_api::LaneBoundary::BoundaryPoint{{6.0_m, 7.0_m, 8.0_m}, 9.0_m, 10.0_m});

    const auto output = ConvertLaneBoundary(input);

    EXPECT_EQ(output.id, input.id);
    EXPECT_EQ(output.type, ConvertLaneBoundaryType(input.type));
    EXPECT_EQ(output.color, ConvertLaneBoundaryColor(input.color));
    EXPECT_EQ(output.points.size(), 2);
}

}  // namespace astas::environment::map::map
