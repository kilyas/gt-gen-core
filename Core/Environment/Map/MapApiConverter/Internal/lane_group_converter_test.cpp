/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/lane_group_converter.h"

#include <gmock/gmock.h>

namespace astas::environment::map
{

static map_api::Lane CreateLane(const map_api::Identifier id = 123)
{
    map_api::Lane lane;
    lane.id = id;
    return lane;
}

static map_api::LaneBoundary CreateLaneBoundary(const map_api::Identifier id = 123)
{
    map_api::LaneBoundary lane_boundary;
    lane_boundary.id = id;
    return lane_boundary;
}

TEST(ConvertLaneGroupTypeTest, GivenMapApiLaneGroupType_WhenConvertToAstasLaneGroup_ThenConversionIsCorrect)
{
    EXPECT_EQ(ConvertLaneGroupType(map_api::LaneGroup::Type::kUnknown), LaneGroup::Type::kUnknown);
    EXPECT_EQ(ConvertLaneGroupType(map_api::LaneGroup::Type::kOther), LaneGroup::Type::kOther);
    EXPECT_EQ(ConvertLaneGroupType(map_api::LaneGroup::Type::kOneWay), LaneGroup::Type::kOneWay);
    EXPECT_EQ(ConvertLaneGroupType(map_api::LaneGroup::Type::kJunction), LaneGroup::Type::kJunction);
    EXPECT_EQ(ConvertLaneGroupType(static_cast<map_api::LaneGroup::Type>(999)), LaneGroup::Type::kUnknown);

    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertLaneGroupType(static_cast<map_api::LaneGroup::Type>(255)), LaneGroup::Type::kUnknown);
    EXPECT_THAT(
        testing::internal::GetCapturedStdout(),
        testing::HasSubstr("The given map_api::LaneGroup::Type:: 255 cannot be converted! Return Unknown as default."));
}

TEST(CreateLaneGroupTest, GivenMapApiLaneGroup_WhenCreateAstasLaneGroup_ThenLaneGroupIdAndTypeIsCorrect)
{
    map_api::LaneGroup lane_group{};
    lane_group.id = 100;

    const auto result = CreateLaneGroup(lane_group.id, lane_group.type);

    EXPECT_EQ(result.id, lane_group.id);
    EXPECT_EQ(result.type, LaneGroup::Type::kUnknown);
}

TEST(
    AppendLaneAndLaneBoundaryToLaneGroupIdMap,
    GivenMapApiLaneGroup_WhenAppendLaneAndLaneBoundaryToLaneGroupIdMap_ThenLaneAndLaneBoundaryToLaneGroupIdMapIsCorrect)
{
    map_api::LaneGroup lane_group{};
    lane_group.id = 100;

    auto lane_1 = CreateLane(101);

    auto lane_boundary_1 = CreateLaneBoundary(102);
    auto lane_boundary_2 = CreateLaneBoundary(103);

    lane_group.lanes.push_back(lane_1);
    lane_group.lane_boundaries.push_back(lane_boundary_1);
    lane_group.lane_boundaries.push_back(lane_boundary_2);

    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> lane_to_lane_group_id_map{};
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> lane_boundary_to_lane_group_id_map{};

    AppendLaneAndLaneBoundaryToLaneGroupIdMap(
        lane_group, lane_to_lane_group_id_map, lane_boundary_to_lane_group_id_map);

    ASSERT_THAT(lane_to_lane_group_id_map.size(), 1);
    EXPECT_EQ(lane_to_lane_group_id_map[lane_1.id], lane_group.id);
    ASSERT_THAT(lane_boundary_to_lane_group_id_map.size(), 2);
    EXPECT_EQ(lane_boundary_to_lane_group_id_map[lane_boundary_1.id], lane_group.id);
    EXPECT_EQ(lane_boundary_to_lane_group_id_map[lane_boundary_2.id], lane_group.id);
}

}  // namespace astas::environment::map
