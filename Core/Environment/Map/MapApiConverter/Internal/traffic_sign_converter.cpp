/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_converter.h"

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_types_converter.h"
#include "Core/Service/Logging/logging.h"

namespace astas::environment::map
{

SignValueInformation ConvertTrafficSignValue(const map_api::TrafficSignValue& from)
{
    SignValueInformation result;
    result.value = from.value;
    result.value_unit = ConvertTrafficSignValueUnit(from.value_unit);
    result.text = from.text;
    return result;
}

TrafficSign ConvertMainSign(const map_api::MainSign& from, const map_api::Identifier id)
{
    TrafficSign result;
    result.id = id;

    result.stvo_id = from.code;
    if (!from.sub_code.empty())
    {
        result.stvo_id += "-";
        result.stvo_id += from.sub_code;
    }
    result.type = ConvertMainSignType(from.type);
    result.value_information = ConvertTrafficSignValue(from.value);
    result.pose = mantle_api::Pose{.position = from.base.position, .orientation = from.base.orientation};
    result.dimensions = from.base.dimension;
    result.variability = ConvertTrafficSignVariability(from.variability);
    result.direction_scope = ConvertDirectionScope(from.direction_scope);

    for (const auto& assigned_lane : from.assigned_lanes)
    {
        result.assigned_lanes.emplace_back(assigned_lane.get().id);
    }

    return result;
}

MountedSign::SupplementarySign ConvertSupplementarySign(const map_api::SupplementarySign& from, MountedSign* main_sign)
{
    MountedSign::SupplementarySign result;
    result.main_sign = main_sign;
    result.variability = ConvertTrafficSignVariability(from.variability);
    for (const auto& actor : from.actors)
    {
        result.actors.emplace_back(ConvertTrafficSignActor(actor));
    }
    result.type = ConvertSupplementarySignType(from.type);
    for (const auto& value : from.values)
    {
        result.value_information.emplace_back(ConvertTrafficSignValue(value));
    }
    result.pose = mantle_api::Pose{.position = from.base.position, .orientation = from.base.orientation};
    result.dimensions = from.base.dimension;

    return result;
}

void FillTrafficSign(const map_api::TrafficSign& from, AstasMap& map)
{
    /// @todo ConvertGroundSign,
    // A sign is assumed to be a GroundSign when its pitch is > 45° and < 135° (it looks upwards)
    // and its z position is relatively low :)
    // see Core/Environment/Map/OpenDrive/OdrConverter/Internal/signal_converter.cpp

    auto main_sign = std::make_shared<TrafficSign>(ConvertMainSign(from.main_sign, from.id));
    map.traffic_signs.emplace_back(main_sign);
    auto mounted_sign = std::make_shared<MountedSign>();
    for (const auto& supplementary_sign : from.supplementary_signs)
    {

        mounted_sign->supplementary_signs.emplace_back(
            ConvertSupplementarySign(supplementary_sign, dynamic_cast<MountedSign*>(main_sign.get())));
    }
    map.traffic_signs.emplace_back(mounted_sign);
}

}  // namespace astas::environment::map
