/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICSIGNTYPESCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICSIGNTYPESCONVERTER_H

#include "Core/Environment/Map/AstasMap/signs.h"
#include "Core/Service/Osi/traffic_sign_types.h"

#include <MapAPI/traffic_sign.h>

namespace astas::environment::map
{
osi::OsiTrafficSignType ConvertMainSignType(const map_api::MainSignType& from);
osi::OsiTrafficSignVariability ConvertTrafficSignVariability(const map_api::TrafficSignVariability& from);
osi::OsiTrafficSignDirectionScope ConvertDirectionScope(const map_api::DirectionScope& from);
OsiSupplementaryTrafficSignActor ConvertTrafficSignActor(const map_api::SupplementarySignActor& from);
OsiSupplementarySignType ConvertSupplementarySignType(const map_api::SupplementarySignType& from);

osi::OsiTrafficSignValueUnit ConvertTrafficSignValueUnit(const map_api::TrafficSignValue::Unit& from);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICSIGNTYPESCONVERTER_H
