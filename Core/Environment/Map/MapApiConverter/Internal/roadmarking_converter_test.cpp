/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/roadmarking_converter.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using units::literals::operator""_m;
using units::literals::operator""_rad;

namespace astas::environment::map::map
{

static map_api::Lane CreateLane(const map_api::Identifier id = 123)
{
    map_api::Lane lane;
    lane.id = id;
    return lane;
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithId_WhenConvert_ThenGroundSignHasSameId)
{
    map_api::RoadMarking road_marking;
    road_marking.id = 42;

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.id, road_marking.id);
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithColorAndType_WhenConvert_ThenGroundSignHasSameColorAndType)
{
    map_api::RoadMarking road_marking;
    road_marking.monochrome_color = map_api::RoadMarking::Color::kYellow;
    road_marking.type = map_api::RoadMarking::Type::kPaintedTrafficSign;

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.marking_color, osi::OsiRoadMarkingColor::kYellow);
    EXPECT_EQ(result.marking_type, osi::OsiRoadMarkingsType::kPaintedTrafficSign);
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithCodeAndSubCode_WhenConvert_ThenGroundSignHasSameStvoId)
{
    map_api::RoadMarking road_marking;
    road_marking.code = "sign";
    road_marking.sub_code = "1";

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.stvo_id, "sign-1");
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithMainSignType_WhenConvert_ThenGroundSignHasSameMainSignType)
{
    map_api::RoadMarking road_marking;
    road_marking.traffic_main_sign_type = map_api::MainSignType::kZebraCrossing;

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.type, osi::OsiTrafficSignType::kZebraCrossing);
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithTrafficSignValue_WhenConvert_ThenGroundSignHasSameValueInformation)
{
    map_api::RoadMarking road_marking;

    map_api::TrafficSignValue sign_value;
    sign_value.value = 123.45;
    sign_value.value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
    sign_value.text = "Speed Limit 50";

    road_marking.value = sign_value;

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.value_information.value, sign_value.value);
    EXPECT_EQ(result.value_information.value_unit, osi::OsiTrafficSignValueUnit::kKilometerPerHour);
    EXPECT_EQ(result.value_information.text, sign_value.text);
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithValueText_WhenConvert_ThenGroundSignHasSameText)
{
    map_api::RoadMarking road_marking;

    road_marking.value_text = "sign";

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.value_information.text, road_marking.value_text);
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithSignValueAndValueText_WhenConvert_ThenGroundSignHasValueText)
{
    map_api::RoadMarking road_marking;

    map_api::TrafficSignValue sign_value;
    sign_value.value = 123.45;
    sign_value.value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
    sign_value.text = "Speed Limit 50";

    road_marking.value = sign_value;

    road_marking.value_text = "sign";

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.value_information.value, sign_value.value);
    EXPECT_EQ(result.value_information.value_unit, osi::OsiTrafficSignValueUnit::kKilometerPerHour);
    EXPECT_EQ(result.value_information.text, road_marking.value_text);
}

TEST(ConvertRoadMarkingTest,
     GivenRoadMarkingWithPositionOrientationDimension_WhenConvert_ThenGroundSignHasSamePoseDimension)
{
    map_api::RoadMarking road_marking;
    road_marking.base.position = {1.0_m, 2.0_m, 3.0_m};
    road_marking.base.orientation = {4.0_rad, 5.0_rad, 6.0_rad};
    road_marking.base.dimension = {7.0_m, 8.0_m, 9.0_m};

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_EQ(result.pose.position, road_marking.base.position);
    EXPECT_EQ(result.pose.orientation, road_marking.base.orientation);
    EXPECT_EQ(result.dimensions, road_marking.base.dimension);
}

TEST(ConvertRoadMarkingTest, GivenRoadMarkingWithAssignedLanes_WhenConvert_ThenGroundSignHasSameAssignedLanes)
{
    map_api::RoadMarking road_marking;
    auto lane1 = CreateLane(1);
    auto lane2 = CreateLane(2);
    road_marking.assigned_lanes = {lane1, lane2};

    const auto result = ConvertRoadMarking(road_marking);

    EXPECT_THAT(result.assigned_lanes, ::testing::ElementsAre(lane1.id, lane2.id));
}

}  // namespace astas::environment::map::map
