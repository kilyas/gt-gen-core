/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/lane_converter.h"

#include <gtest/gtest.h>

namespace astas::environment::map::map
{
using units::literals::operator""_m;

static map_api::Lane CreateLane(const map_api::Identifier id = 123)
{
    map_api::Lane lane;
    lane.id = id;
    return lane;
}

static map_api::LaneBoundary CreateLaneBoundary(const map_api::Identifier id = 123)
{
    map_api::LaneBoundary lane_boundary;
    lane_boundary.id = id;
    return lane_boundary;
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenLaneIdIsConverted)
{
    const auto from = CreateLane();

    const auto result = ConvertLane(from);

    EXPECT_EQ(result.id, from.id);
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenCenterlineIsConverted)
{
    // arrange
    auto from = CreateLane();
    from.centerline.push_back({1.0_m, 2.0_m, 3.0_m});
    from.centerline.push_back({4.0_m, 5.0_m, 6.0_m});

    // act
    const auto result = ConvertLane(from);

    // assert
    ASSERT_EQ(result.center_line.size(), from.centerline.size());
    EXPECT_EQ(result.center_line.at(0), from.centerline.at(0));
    EXPECT_EQ(result.center_line.at(1), from.centerline.at(1));
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenPredecessorLanesAreConverted)
{
    // arrange
    auto from = CreateLane();
    auto antecessor_lane_0 = CreateLane(1);
    auto antecessor_lane_1 = CreateLane(2);
    from.antecessor_lanes = {antecessor_lane_0, antecessor_lane_1};

    // act
    const auto result = ConvertLane(from);

    // assert
    ASSERT_EQ(result.predecessors.size(), from.antecessor_lanes.size());
    EXPECT_EQ(result.predecessors.at(0), antecessor_lane_0.id);
    EXPECT_EQ(result.predecessors.at(1), antecessor_lane_1.id);
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenSuccessorLanesAreConverted)
{
    // arrange
    auto from = CreateLane();
    auto successor_lane_0 = CreateLane(1);
    auto successor_lane_1 = CreateLane(2);
    from.successor_lanes = {successor_lane_0, successor_lane_1};

    // act
    const auto result = ConvertLane(from);

    // assert
    ASSERT_EQ(result.successors.size(), from.successor_lanes.size());
    EXPECT_EQ(result.successors.at(0), successor_lane_0.id);
    EXPECT_EQ(result.successors.at(1), successor_lane_1.id);
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenLeftAdjacentLanesAreConverted)
{
    // arrange
    auto from = CreateLane();
    auto left_adjacent_lane_0 = CreateLane(1);
    auto left_adjacent_lane_1 = CreateLane(2);
    from.left_adjacent_lanes = {left_adjacent_lane_0, left_adjacent_lane_1};

    // act
    const auto result = ConvertLane(from);

    // assert
    ASSERT_EQ(result.left_adjacent_lanes.size(), from.left_adjacent_lanes.size());
    EXPECT_EQ(result.left_adjacent_lanes.at(0), left_adjacent_lane_0.id);
    EXPECT_EQ(result.left_adjacent_lanes.at(1), left_adjacent_lane_1.id);
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenRightAdjacentLanesAreConverted)
{
    // arrange
    auto from = CreateLane();
    auto right_adjacent_lane_0 = CreateLane(1);
    auto right_adjacent_lane_1 = CreateLane(2);
    from.right_adjacent_lanes = {right_adjacent_lane_0, right_adjacent_lane_1};

    // act
    const auto result = ConvertLane(from);

    // assert
    ASSERT_EQ(result.right_adjacent_lanes.size(), from.right_adjacent_lanes.size());
    EXPECT_EQ(result.right_adjacent_lanes.at(0), right_adjacent_lane_0.id);
    EXPECT_EQ(result.right_adjacent_lanes.at(1), right_adjacent_lane_1.id);
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenLeftLaneBoundariesAreConverted)
{
    // arrange
    auto from = CreateLane();
    auto left_lane_boundary_0 = CreateLaneBoundary(1);
    auto left_lane_boundary_1 = CreateLaneBoundary(2);
    from.left_lane_boundaries = {left_lane_boundary_0, left_lane_boundary_1};

    // act
    const auto result = ConvertLane(from);

    // assert
    ASSERT_EQ(result.left_lane_boundaries.size(), from.left_lane_boundaries.size());
    EXPECT_EQ(result.left_lane_boundaries.at(0), left_lane_boundary_0.id);
    EXPECT_EQ(result.left_lane_boundaries.at(1), left_lane_boundary_1.id);
}

TEST(LaneConverterTest, GivenMapApiLane_WhenConvertToAstasLane_ThenRightLaneBoundariesAreConverted)
{
    // arrange
    auto from = CreateLane();
    auto right_lane_boundary_0 = CreateLaneBoundary(1);
    auto right_lane_boundary_1 = CreateLaneBoundary(2);
    from.right_lane_boundaries = {right_lane_boundary_0, right_lane_boundary_1};

    // act
    const auto result = ConvertLane(from);

    // assert
    ASSERT_EQ(result.right_lane_boundaries.size(), from.right_lane_boundaries.size());
    EXPECT_EQ(result.right_lane_boundaries.at(0), right_lane_boundary_0.id);
    EXPECT_EQ(result.right_lane_boundaries.at(1), right_lane_boundary_1.id);
}

TEST(LaneConverterTest, GivenMapApiLaneWithTypeDriving_WhenConvertToAstasLane_ThenLaneFlagsIsNormalDrivable)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kDriving;
    from.sub_type = map_api::Lane::Subtype::kNormal;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), true);
    EXPECT_EQ(result.flags.IsNormalLane(), true);
    EXPECT_EQ(result.flags.IsShoulderLane(), false);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeEntry_WhenConvertToAstasLane_ThenLaneFlagsIsMergeAndEntryLane)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kDriving;
    from.sub_type = map_api::Lane::Subtype::kEntry;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), true);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), false);
    EXPECT_EQ(result.flags.IsMergeLane(), true);
    EXPECT_EQ(result.flags.IsEntryLane(), true);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeExit_WhenConvertToAstasLane_ThenLaneFlagsIsSplitAndExitLane)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kDriving;
    from.sub_type = map_api::Lane::Subtype::kExit;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), true);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), false);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), true);
    EXPECT_EQ(result.flags.IsExitLane(), true);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithTypeIntersection_WhenConvertToAstasLane_ThenLaneFlagsIsDrivable)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kIntersection;
    from.sub_type = map_api::Lane::Subtype::kOnRamp;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), true);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), false);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeStop_WhenConvertToAstasLane_ThenLaneFlagsIsShoulder)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kNonDriving;
    from.sub_type = map_api::Lane::Subtype::kStop;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), false);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), true);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeShoulder_WhenConvertToAstasLane_ThenLaneFlagsIsShoulder)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kNonDriving;
    from.sub_type = map_api::Lane::Subtype::kShoulder;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), false);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), true);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeBorder_WhenConvertToAstasLane_ThenLaneFlagsIsShoulder)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kNonDriving;
    from.sub_type = map_api::Lane::Subtype::kBorder;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), false);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), true);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeRestricted_WhenConvertToAstasLane_ThenLaneFlagsIsShoulder)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kNonDriving;
    from.sub_type = map_api::Lane::Subtype::kRestricted;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), false);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), true);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeParking_WhenConvertToAstasLane_ThenLaneFlagsIsParking)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kNonDriving;
    from.sub_type = map_api::Lane::Subtype::kParking;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), false);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), false);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), true);
    EXPECT_EQ(result.flags.IsBicycle(), false);
}

TEST(LaneConverterTest, GivenMapApiLaneWithSubTypeBiking_WhenConvertToAstasLane_ThenLaneFlagsIsBicycle)
{
    // arrange
    auto from = CreateLane();
    from.type = map_api::Lane::Type::kNonDriving;
    from.sub_type = map_api::Lane::Subtype::kBiking;

    // act
    const auto result = ConvertLane(from);

    // assert
    EXPECT_EQ(result.flags.IsDrivable(), false);
    EXPECT_EQ(result.flags.IsNormalLane(), false);
    EXPECT_EQ(result.flags.IsShoulderLane(), false);
    EXPECT_EQ(result.flags.IsMergeLane(), false);
    EXPECT_EQ(result.flags.IsEntryLane(), false);
    EXPECT_EQ(result.flags.IsSplitLane(), false);
    EXPECT_EQ(result.flags.IsExitLane(), false);
    EXPECT_EQ(result.flags.IsParking(), false);
    EXPECT_EQ(result.flags.IsBicycle(), true);
}

}  // namespace astas::environment::map::map
