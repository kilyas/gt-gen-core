/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_types_converter.h"

#include "Core/Service/Logging/logging.h"

namespace astas::environment::map
{

osi::OsiTrafficSignType ConvertMainSignType(const map_api::MainSignType& from)
{
    return static_cast<osi::OsiTrafficSignType>(from);
}

osi::OsiTrafficSignVariability ConvertTrafficSignVariability(const map_api::TrafficSignVariability& from)
{
    switch (from)
    {
        case map_api::TrafficSignVariability::kUnknown:
            return osi::OsiTrafficSignVariability::kUnknown;
        case map_api::TrafficSignVariability::kOther:
            return osi::OsiTrafficSignVariability::kOther;
        case map_api::TrafficSignVariability::kFixed:
            return osi::OsiTrafficSignVariability::kFixed;
        case map_api::TrafficSignVariability::kVariable:
            return osi::OsiTrafficSignVariability::kVariable;
        default:
            Warn("The given map_api::TrafficSignVariability value={} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return osi::OsiTrafficSignVariability::kUnknown;
    }
}

osi::OsiTrafficSignDirectionScope ConvertDirectionScope(const map_api::DirectionScope& from)
{
    switch (from)
    {
        case map_api::DirectionScope::kUnknown:
            return osi::OsiTrafficSignDirectionScope::kUnknown;
        case map_api::DirectionScope::kOther:
            return osi::OsiTrafficSignDirectionScope::kOther;
        case map_api::DirectionScope::kNoDirection:
            return osi::OsiTrafficSignDirectionScope::kNoDirection;
        case map_api::DirectionScope::kLeft:
            return osi::OsiTrafficSignDirectionScope::kLeft;
        case map_api::DirectionScope::kRight:
            return osi::OsiTrafficSignDirectionScope::kRight;
        case map_api::DirectionScope::kLeftRight:
            return osi::OsiTrafficSignDirectionScope::kLeftRight;
        default:
            Warn("The given map_api::DirectionScope value={} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return osi::OsiTrafficSignDirectionScope::kUnknown;
    }
}

osi::OsiTrafficSignValueUnit ConvertTrafficSignValueUnit(const map_api::TrafficSignValue::Unit& from)
{
    switch (from)
    {
        case map_api::TrafficSignValue::Unit::kUnknown:
            return osi::OsiTrafficSignValueUnit::kUnknown;
        case map_api::TrafficSignValue::Unit::kOther:
            return osi::OsiTrafficSignValueUnit::kOther;
        case map_api::TrafficSignValue::Unit::kNoUnit:
            return osi::OsiTrafficSignValueUnit::kNoUnit;
        case map_api::TrafficSignValue::Unit::kKilometerPerHour:
            return osi::OsiTrafficSignValueUnit::kKilometerPerHour;
        case map_api::TrafficSignValue::Unit::kMilePerHour:
            return osi::OsiTrafficSignValueUnit::kMilePerHour;
        case map_api::TrafficSignValue::Unit::kMeter:
            return osi::OsiTrafficSignValueUnit::kMeter;
        case map_api::TrafficSignValue::Unit::kKilometer:
            return osi::OsiTrafficSignValueUnit::kKilometer;
        case map_api::TrafficSignValue::Unit::kFeet:
            return osi::OsiTrafficSignValueUnit::kFeet;
        case map_api::TrafficSignValue::Unit::kMile:
            return osi::OsiTrafficSignValueUnit::kMile;
        case map_api::TrafficSignValue::Unit::kMetricTon:
            return osi::OsiTrafficSignValueUnit::kMetricTon;
        case map_api::TrafficSignValue::Unit::kLongTon:
            return osi::OsiTrafficSignValueUnit::kLongTon;
        case map_api::TrafficSignValue::Unit::kShortTon:
            return osi::OsiTrafficSignValueUnit::kShortTon;
        case map_api::TrafficSignValue::Unit::kMinutes:
            return osi::OsiTrafficSignValueUnit::kMinutes;
        case map_api::TrafficSignValue::Unit::kDay:
            return osi::OsiTrafficSignValueUnit::kDay;
        case map_api::TrafficSignValue::Unit::kPercentage:
            return osi::OsiTrafficSignValueUnit::kPercentage;
        default:
            /// @note The following units are not supported by OsiTrafficSignValueUnit : kDurationMinute, kDurationHour,
            /// kDurationDay, kDayOfMonth,kHour
            Warn("The given map_api::TrafficSignValue::Unit value={} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return osi::OsiTrafficSignValueUnit::kUnknown;
    }
}

OsiSupplementaryTrafficSignActor ConvertTrafficSignActor(const map_api::SupplementarySignActor& from)
{
    return static_cast<OsiSupplementaryTrafficSignActor>(from);
}

OsiSupplementarySignType ConvertSupplementarySignType(const map_api::SupplementarySignType& from)
{
    return static_cast<OsiSupplementarySignType>(from);
}

}  // namespace astas::environment::map
