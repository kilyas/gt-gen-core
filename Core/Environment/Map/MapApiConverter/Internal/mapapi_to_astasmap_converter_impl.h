/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_MAPAPITOASTASMAPCONVERTERIMPL_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_MAPAPITOASTASMAPCONVERTERIMPL_H

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/AstasMap/astas_map_finalizer.h"
#include "Core/Environment/Map/Common/i_any_to_astasmap_converter.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <MapAPI/map.h>

#include <memory>
#include <vector>

namespace astas::environment::map
{

class MapApiToAstasMapConverterImpl : public IAnyToAstasMapConverter
{
  public:
    MapApiToAstasMapConverterImpl(service::utility::UniqueIdProvider& id_provider,
                                  const map_api::Map& data,
                                  AstasMap& astas_map);

    MapApiToAstasMapConverterImpl() = delete;
    MapApiToAstasMapConverterImpl(const MapApiToAstasMapConverterImpl&) = delete;
    MapApiToAstasMapConverterImpl(MapApiToAstasMapConverterImpl&&) = delete;
    MapApiToAstasMapConverterImpl& operator=(const MapApiToAstasMapConverterImpl&) = delete;
    MapApiToAstasMapConverterImpl& operator=(MapApiToAstasMapConverterImpl&&) = delete;
    ~MapApiToAstasMapConverterImpl() override;

    /// @copydoc IAnyToAstasMapConverter::Convert()
    void Convert() override;

    /// @copydoc IAnyToAstasMapConverter::GetNativeToAstasTrafficLightIdMap()
    std::map<mantle_api::UniqueId, mantle_api::UniqueId> GetNativeToAstasTrafficLightIdMap() const override;

  private:
    service::utility::UniqueIdProvider& unique_id_provider_;
    const map_api::Map& data_;
    environment::map::AstasMap& astas_map_;
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_MAPAPITOASTASMAPCONVERTERIMPL_H
