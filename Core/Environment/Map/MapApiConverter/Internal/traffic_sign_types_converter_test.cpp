/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_types_converter.h"

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_converter.h"

#include <gmock/gmock.h>

namespace astas::environment::map::map
{

TEST(ConvertMainSignTypeTest, GivenMainSignType_WhenConvert_ThenOsiMainSignTypeIsCorrect)
{
    for (std::uint8_t i = 0; i <= 240; ++i)
    {
        EXPECT_EQ(ConvertMainSignType(static_cast<map_api::MainSignType>(i)), static_cast<osi::OsiTrafficSignType>(i));
    }
}

TEST(ConvertTrafficSignVariabilityToOsiTest,
     GivenUnsupportedTrafficSignVariability_WhenConvert_ThenOsiTrafficSignVariabilityIsUnknownWithWarningMessage)
{
    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertTrafficSignVariability(static_cast<map_api::TrafficSignVariability>(255)),
              osi::OsiTrafficSignVariability::kUnknown);
    EXPECT_THAT(testing::internal::GetCapturedStdout(),
                testing::HasSubstr("The given map_api::TrafficSignVariability value=255 cannot be converted! "
                                   "Return Unknown as default."));
}

TEST(ConvertDirectionScopeToOsiTest, GivenDirectionScope_WhenConvert_ThenOsiTrafficSignDirectionScopeIsCorrect)
{

    EXPECT_EQ(ConvertDirectionScope(map_api::DirectionScope::kUnknown), osi::OsiTrafficSignDirectionScope::kUnknown);
    EXPECT_EQ(ConvertDirectionScope(map_api::DirectionScope::kOther), osi::OsiTrafficSignDirectionScope::kOther);
    EXPECT_EQ(ConvertDirectionScope(map_api::DirectionScope::kNoDirection),
              osi::OsiTrafficSignDirectionScope::kNoDirection);
    EXPECT_EQ(ConvertDirectionScope(map_api::DirectionScope::kLeft), osi::OsiTrafficSignDirectionScope::kLeft);
    EXPECT_EQ(ConvertDirectionScope(map_api::DirectionScope::kRight), osi::OsiTrafficSignDirectionScope::kRight);
    EXPECT_EQ(ConvertDirectionScope(map_api::DirectionScope::kLeftRight),
              osi::OsiTrafficSignDirectionScope::kLeftRight);
}

TEST(ConvertDirectionScopeToOsiTest,
     GivenUnsupportedConvertDirectionScope_WhenConvert_ThenOsiTrafficSignDirectionScopeIsUnknownWithWarningMessage)
{
    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertDirectionScope(static_cast<map_api::DirectionScope>(255)),
              osi::OsiTrafficSignDirectionScope::kUnknown);
    EXPECT_THAT(testing::internal::GetCapturedStdout(),
                testing::HasSubstr(
                    "The given map_api::DirectionScope value=255 cannot be converted! Return Unknown as default."));
}

TEST(ConvertTrafficSignValueUnitTest, GivenTrafficSignValueUnit_WhenConvert_ThenOsiTrafficSignValueUnitIsCorrect)
{
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kUnknown),
              osi::OsiTrafficSignValueUnit::kUnknown);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kOther),
              osi::OsiTrafficSignValueUnit::kOther);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kNoUnit),
              osi::OsiTrafficSignValueUnit::kNoUnit);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kKilometerPerHour),
              osi::OsiTrafficSignValueUnit::kKilometerPerHour);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kMilePerHour),
              osi::OsiTrafficSignValueUnit::kMilePerHour);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kMeter),
              osi::OsiTrafficSignValueUnit::kMeter);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kKilometer),
              osi::OsiTrafficSignValueUnit::kKilometer);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kFeet), osi::OsiTrafficSignValueUnit::kFeet);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kMile), osi::OsiTrafficSignValueUnit::kMile);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kMetricTon),
              osi::OsiTrafficSignValueUnit::kMetricTon);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kLongTon),
              osi::OsiTrafficSignValueUnit::kLongTon);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kShortTon),
              osi::OsiTrafficSignValueUnit::kShortTon);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kMinutes),
              osi::OsiTrafficSignValueUnit::kMinutes);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kDay), osi::OsiTrafficSignValueUnit::kDay);
    EXPECT_EQ(ConvertTrafficSignValueUnit(map_api::TrafficSignValue::Unit::kPercentage),
              osi::OsiTrafficSignValueUnit::kPercentage);
}

TEST(ConvertTrafficSignValueUnitTest,
     GivenUnsupportedTrafficSignValueUnit_WhenConvert_ThenOsiTrafficSignValueUnitIsUnknownWithWarningMessage)
{
    testing::internal::CaptureStdout();  // Capture console output
    EXPECT_EQ(ConvertTrafficSignValueUnit(static_cast<map_api::TrafficSignValue::Unit>(255)),
              osi::OsiTrafficSignValueUnit::kUnknown);
    EXPECT_THAT(
        testing::internal::GetCapturedStdout(),
        testing::HasSubstr(
            "The given map_api::TrafficSignValue::Unit value=255 cannot be converted! Return Unknown as default."));
}

TEST(ConvertTrafficSignActorTest, GivenTrafficSignActors_WhenConvert_ThenOsiTrafficSignActorsIsCorrect)
{
    for (std::uint8_t i = 0; i <= 45; ++i)
    {
        EXPECT_EQ(ConvertTrafficSignActor(static_cast<map_api::SupplementarySignActor>(i)),
                  static_cast<OsiSupplementaryTrafficSignActor>(i));
    }
}

TEST(ConvertSupplementarySignTypeTest, GivenTrafficSignActors_WhenConvert_ThenOsiTrafficSignActorsIsCorrect)
{
    for (std::uint8_t i = 0; i <= 48; ++i)
    {
        EXPECT_EQ(ConvertSupplementarySignType(static_cast<map_api::SupplementarySignType>(i)),
                  static_cast<OsiSupplementarySignType>(i));
    }
}

}  // namespace astas::environment::map::map
