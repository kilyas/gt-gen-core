/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/mapapi_to_astasmap_converter_impl.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/MapApiConverter/Internal/lane_boundary_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/lane_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/lane_group_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/roadmarking_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/traffic_light_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_converter.h"

namespace astas::environment::map
{

MapApiToAstasMapConverterImpl::MapApiToAstasMapConverterImpl(service::utility::UniqueIdProvider& id_provider,
                                                             const map_api::Map& data,
                                                             AstasMap& astas_map)
    : unique_id_provider_{id_provider}, data_{data}, astas_map_{astas_map}
{
}

MapApiToAstasMapConverterImpl::~MapApiToAstasMapConverterImpl() = default;

void MapApiToAstasMapConverterImpl::Convert()
{
    if (!(astas_map_.IsOpenDrive() || astas_map_.IsNDS()))
    {
        LogAndThrow(EnvironmentException("Astas map source type is not set!"));
    }

    astas_map_.path = data_.map_reference;
    astas_map_.projection_string = data_.projection_string;
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> lane_to_lane_group_id_map{};
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> lane_boundary_to_lane_group_id_map{};

    // convert lane groups
    for (const auto& lane_group : data_.lane_groups)
    {
        astas_map_.AddLaneGroup(CreateLaneGroup(lane_group->id, lane_group->type));
        AppendLaneAndLaneBoundaryToLaneGroupIdMap(
            *lane_group, lane_to_lane_group_id_map, lane_boundary_to_lane_group_id_map);
    }

    // convert lane_boundaries
    for (const auto& lane_boundary : data_.lane_boundaries)
    {
        astas_map_.AddLaneBoundary(lane_boundary_to_lane_group_id_map[lane_boundary->id],
                                   ConvertLaneBoundary(*lane_boundary));
    }

    // convert lanes
    for (const auto& lane : data_.lanes)
    {
        astas_map_.AddLane(lane_to_lane_group_id_map[lane->id], ConvertLane(*lane));
    }

    // convert traffic signs
    for (const auto& traffic_sign : data_.traffic_signs)
    {
        FillTrafficSign(*traffic_sign, astas_map_);
    }

    // convert traffic lights
    for (const auto& traffic_light : data_.traffic_lights)
    {
        astas_map_.traffic_lights.emplace_back(ConvertTrafficLight(*traffic_light));
    }

    // convert ground signs
    for (const auto& road_marking : data_.road_markings)
    {
        auto ground_sign = std::make_shared<GroundSign>(ConvertRoadMarking(*road_marking));
        astas_map_.traffic_signs.emplace_back(ground_sign);
    }

    ///@todo  Add conversion of road objects
}

std::map<mantle_api::UniqueId, mantle_api::UniqueId> MapApiToAstasMapConverterImpl::GetNativeToAstasTrafficLightIdMap()
    const
{
    return {};
}

}  // namespace astas::environment::map
