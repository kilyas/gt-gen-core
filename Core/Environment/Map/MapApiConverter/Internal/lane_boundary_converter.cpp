/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/lane_boundary_converter.h"

#include "Core/Service/Logging/logging.h"

namespace astas::environment::map
{

LaneBoundary ConvertLaneBoundary(const map_api::LaneBoundary& from)
{
    LaneBoundary result(static_cast<mantle_api::UniqueId>(from.id),
                        ConvertLaneBoundaryType(from.type),
                        ConvertLaneBoundaryColor(from.color));

    for (const auto& boundary_point : from.boundary_line)
    {
        result.points.push_back(ConvertBoundaryPoint(boundary_point));
    }

    ///@note  mirrored_from and parent_lane_group_id are not converted
    return result;
}

LaneBoundary::Color ConvertLaneBoundaryColor(const map_api::LaneBoundary::Color& from)
{
    switch (from)
    {
        case map_api::LaneBoundary::Color::kUnknown:
            return LaneBoundary::Color::kUnknown;
        case map_api::LaneBoundary::Color::kOther:
            return LaneBoundary::Color::kOther;
        case map_api::LaneBoundary::Color::kNone:
            return LaneBoundary::Color::kNone;
        case map_api::LaneBoundary::Color::kWhite:
            return LaneBoundary::Color::kWhite;
        case map_api::LaneBoundary::Color::kYellow:
            return LaneBoundary::Color::kYellow;
        case map_api::LaneBoundary::Color::kRed:
            return LaneBoundary::Color::kRed;
        case map_api::LaneBoundary::Color::kBlue:
            return LaneBoundary::Color::kBlue;
        case map_api::LaneBoundary::Color::kGreen:
            return LaneBoundary::Color::kGreen;
        case map_api::LaneBoundary::Color::kViolet:
            return LaneBoundary::Color::kViolet;
        case map_api::LaneBoundary::Color::kOrange:
            return LaneBoundary::Color::kOrange;
        default:
            Warn("The given map_api::LaneBoundary::Color {} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return LaneBoundary::Color::kUnknown;
    }
}

LaneBoundary::Type ConvertLaneBoundaryType(const map_api::LaneBoundary::Type& from)
{
    switch (from)
    {
        case map_api::LaneBoundary::Type::kUnknown:
            return LaneBoundary::Type::kUnknown;
        case map_api::LaneBoundary::Type::kOther:
            return LaneBoundary::Type::kOther;
        case map_api::LaneBoundary::Type::kNoLine:
            return LaneBoundary::Type::kNoLine;
        case map_api::LaneBoundary::Type::kSolidLine:
            return LaneBoundary::Type::kSolidLine;
        case map_api::LaneBoundary::Type::kDashedLine:
            return LaneBoundary::Type::kDashedLine;
        case map_api::LaneBoundary::Type::kBottsDots:
            return LaneBoundary::Type::kBottsDots;
        case map_api::LaneBoundary::Type::kRoadEdge:
            return LaneBoundary::Type::kRoadEdge;
        case map_api::LaneBoundary::Type::kSnowEdge:
            return LaneBoundary::Type::kSnowEdge;
        case map_api::LaneBoundary::Type::kGrassEdge:
            return LaneBoundary::Type::kGrassEdge;
        case map_api::LaneBoundary::Type::kGravelEdge:
            return LaneBoundary::Type::kGravelEdge;
        case map_api::LaneBoundary::Type::kSoilEdge:
            return LaneBoundary::Type::kSoilEdge;
        case map_api::LaneBoundary::Type::kGuardRail:
            return LaneBoundary::Type::kGuardRail;
        case map_api::LaneBoundary::Type::kCurb:
            return LaneBoundary::Type::kCurb;
        case map_api::LaneBoundary::Type::kStructure:
            return LaneBoundary::Type::kStructure;
        case map_api::LaneBoundary::Type::kBarrier:
            return LaneBoundary::Type::kBarrier;
        case map_api::LaneBoundary::Type::kSoundBarrier:
            return LaneBoundary::Type::kBarrier;  // Map kSoundBarrier to kBarrier in LaneBoundary::Type
        default:
            Warn("The given map_api::LaneBoundary::Type {} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return LaneBoundary::Type::kUnknown;
    }
}

LaneBoundary::Point ConvertBoundaryPoint(const map_api::LaneBoundary::BoundaryPoint& from)
{
    return LaneBoundary::Point{.position = from.position, .width = from.width(), .height = from.height()};
}

}  // namespace astas::environment::map
