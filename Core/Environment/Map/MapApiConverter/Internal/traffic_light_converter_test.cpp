/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_light_converter.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace astas::environment::map::map
{
using ::testing::ElementsAre;
using units::literals::operator""_m;
using units::literals::operator""_rad;

static map_api::Lane CreateLane(const map_api::Identifier id = 123)
{
    map_api::Lane lane;
    lane.id = id;
    return lane;
}

TEST(ConvertTrafficLightBulbTest, GivenTrafficLight_WhenConvert_ThenTrafficLightBulbIsSet)
{
    map_api::TrafficLight::TrafficSignal traffic_signal;
    traffic_signal.id = 42;
    traffic_signal.base.position = {1.0_m, 2.0_m, 3.0_m};
    traffic_signal.base.orientation = {4.0_rad, 5.0_rad, 6.0_rad};
    traffic_signal.base.dimension = {7.0_m, 8.0_m, 9.0_m};
    traffic_signal.color = map_api::TrafficLight::TrafficSignal::Color::kBlue;
    traffic_signal.icon = map_api::TrafficLight::TrafficSignal::Icon::kArrowLeft;
    traffic_signal.mode = map_api::TrafficLight::TrafficSignal::Mode::kOff;
    traffic_signal.counter = 456.0;
    auto lane1 = CreateLane(1);
    auto lane2 = CreateLane(2);
    traffic_signal.assigned_lanes = {lane1, lane2};

    const auto result = ConvertTrafficLightBulb(traffic_signal);

    EXPECT_EQ(result.id, traffic_signal.id);
    EXPECT_EQ(result.pose.position, traffic_signal.base.position);
    EXPECT_EQ(result.pose.orientation, traffic_signal.base.orientation);
    EXPECT_EQ(result.dimensions, traffic_signal.base.dimension);
    EXPECT_EQ(result.color, OsiTrafficLightColor::kBlue);
    EXPECT_EQ(result.icon, OsiTrafficLightIcon::kArrowLeft);
    EXPECT_EQ(result.mode, OsiTrafficLightMode::kOff);
    EXPECT_EQ(result.count, traffic_signal.counter);
    ASSERT_THAT(result.assigned_lanes, ElementsAre(lane1.id, lane2.id));
}

TEST(ConvertTrafficLightTest, GivenTrafficLight_WhenConvert_ThenTrafficLightIsSet)
{
    map_api::TrafficLight traffic_light{.id = 41};
    traffic_light.traffic_signals.emplace_back(
        map_api::TrafficLight::TrafficSignal{.id = 201,
                                             .color = map_api::TrafficLight::TrafficSignal::Color::kGreen,
                                             .mode = map_api::TrafficLight::TrafficSignal::Mode::kOff});
    traffic_light.traffic_signals.emplace_back(
        map_api::TrafficLight::TrafficSignal{.id = 202,
                                             .color = map_api::TrafficLight::TrafficSignal::Color::kRed,
                                             .mode = map_api::TrafficLight::TrafficSignal::Mode::kConstant});

    const auto result = ConvertTrafficLight(traffic_light);

    EXPECT_EQ(result.id, traffic_light.id);
    ASSERT_EQ(result.light_bulbs.size(), traffic_light.traffic_signals.size());
    EXPECT_EQ(result.light_bulbs[0].id, traffic_light.traffic_signals[0].id);
    EXPECT_EQ(result.light_bulbs[1].id, traffic_light.traffic_signals[1].id);
}
}  // namespace astas::environment::map::map
