/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/mapapi_to_astasmap_converter_impl.h"

#include <gmock/gmock.h>

namespace astas::environment::map::map
{
using units::literals::operator""_m;

inline auto CreateMapApiLaneBoundary(map_api::Identifier id,
                                     const units::length::meter_t start_x,
                                     const units::length::meter_t y_offset)
{
    auto lane_boundary = std::make_unique<map_api::LaneBoundary>();
    lane_boundary->id = id;
    lane_boundary->boundary_line = std::vector<map_api::LaneBoundary::BoundaryPoint>{
        map_api::LaneBoundary::BoundaryPoint{
            .position = map_api::Position3d{start_x, y_offset, 0.0_m}, .width = 0.13_m, .height = 0.05_m},
        map_api::LaneBoundary::BoundaryPoint{
            .position = map_api::Position3d{start_x + 1000.0_m, y_offset, 0.0_m}, .width = 0.13_m, .height = 0.05_m}};
    lane_boundary->type = map_api::LaneBoundary::Type::kSolidLine;
    lane_boundary->color = map_api::LaneBoundary::Color::kWhite;
    return std::move(lane_boundary);
}

inline auto CreateMapApiLane(map_api::Identifier id,
                             const units::length::meter_t start_x,
                             const units::length::meter_t y_offset)
{
    auto lane = std::make_unique<map_api::Lane>();
    lane->id = id;
    lane->type = map_api::Lane::Type::kDriving;
    lane->sub_type = map_api::Lane::Subtype::kUnknown;
    lane->centerline = map_api::Lane::Polyline{map_api::Lane::PolylinePoint{start_x, y_offset, 0.0_m},
                                               map_api::Lane::PolylinePoint{start_x + 1000.0_m, y_offset, 0.0_m}};

    return std::move(lane);
}

///////////////////////////////////////////////////////////
/// @verbatim
/// Connected Lanes
/// IDs:
/// |-----------------------------|-----------------------------| y = 4.5
/// |    Left LaneBoundary:  100  |    Left LaneBoundary:  103  |
/// |    Lane: 0                  |    Lane: 2                  | y = 3.0
/// |    Right LaneBoundary: 101  |    Right LaneBoundary: 104  |
/// |-----------------------------|-----------------------------| y = 1.5
/// |    Left LaneBoundary:  101  |    Left LaneBoundary:  104  |
/// |    Lane: 1                  |    Lane: 3                  | y = 0.0
/// |    Right LaneBoundary: 102  |    Right LaneBoundary: 105  |
/// |-----------------------------|-----------------------------| y = -1.5
/// ^                             ^                             ^
/// x(0)                         (1000)                       (2000)
///
/// @endverbatim
inline map_api::Map CreateMapWithConnectedLanes()
{
    map_api::Map map{};

    map.projection_string = "+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs";

    auto lane_boundary_100 = CreateMapApiLaneBoundary(100, 0.0_m, 4.5_m);
    auto lane_boundary_101 = CreateMapApiLaneBoundary(101, 0.0_m, 1.5_m);
    auto lane_boundary_102 = CreateMapApiLaneBoundary(102, 0.0_m, -1.5_m);
    auto lane_boundary_103 = CreateMapApiLaneBoundary(103, 1000.0_m, 4.5_m);
    auto lane_boundary_104 = CreateMapApiLaneBoundary(104, 1000.0_m, 1.5_m);
    auto lane_boundary_105 = CreateMapApiLaneBoundary(105, 1000.0_m, -1.5_m);

    auto lane_0 = CreateMapApiLane(0, 0.0_m, 3.0_m);
    auto lane_1 = CreateMapApiLane(1, 0.0_m, 3.0_m);
    auto lane_2 = CreateMapApiLane(2, 1000.0_m, 3.0_m);
    auto lane_3 = CreateMapApiLane(3, 1000.0_m, 0.0_m);

    lane_0->left_lane_boundaries.push_back(*lane_boundary_100);
    lane_0->right_lane_boundaries.push_back(*lane_boundary_101);
    lane_0->right_adjacent_lanes.push_back(*lane_1);
    lane_0->successor_lanes.push_back(*lane_2);

    lane_1->left_lane_boundaries.push_back(*lane_boundary_101);
    lane_1->right_lane_boundaries.push_back(*lane_boundary_102);
    lane_1->left_adjacent_lanes.push_back(*lane_0);
    lane_1->successor_lanes.push_back(*lane_3);

    lane_2->left_lane_boundaries.push_back(*lane_boundary_103);
    lane_2->right_lane_boundaries.push_back(*lane_boundary_104);
    lane_2->right_adjacent_lanes.push_back(*lane_3);
    lane_2->antecessor_lanes.push_back(*lane_0);

    lane_3->left_lane_boundaries.push_back(*lane_boundary_104);
    lane_3->right_lane_boundaries.push_back(*lane_boundary_105);
    lane_3->left_adjacent_lanes.push_back(*lane_2);
    lane_3->antecessor_lanes.push_back(*lane_1);

    auto lane_group_1 = std::make_unique<map_api::LaneGroup>();
    lane_group_1->id = 1;
    lane_group_1->type = map_api::LaneGroup::Type::kOneWay;
    lane_group_1->lanes.push_back(*lane_0);
    lane_group_1->lanes.push_back(*lane_1);
    lane_group_1->lane_boundaries.push_back(*lane_boundary_100);
    lane_group_1->lane_boundaries.push_back(*lane_boundary_101);
    lane_group_1->lane_boundaries.push_back(*lane_boundary_102);
    map.lane_groups.push_back(std::move(lane_group_1));

    auto lane_group_2 = std::make_unique<map_api::LaneGroup>();
    lane_group_2->id = 2;
    lane_group_2->type = map_api::LaneGroup::Type::kOther;
    lane_group_2->lanes.push_back(*lane_2);
    lane_group_2->lanes.push_back(*lane_3);
    lane_group_2->lane_boundaries.push_back(*lane_boundary_103);
    lane_group_2->lane_boundaries.push_back(*lane_boundary_104);
    lane_group_2->lane_boundaries.push_back(*lane_boundary_105);
    map.lane_groups.push_back(std::move(lane_group_2));

    map.lanes.emplace_back(std::move(lane_0));
    map.lanes.emplace_back(std::move(lane_1));
    map.lanes.emplace_back(std::move(lane_2));
    map.lanes.emplace_back(std::move(lane_3));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_100));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_101));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_102));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_103));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_104));
    map.lane_boundaries.emplace_back(std::move(lane_boundary_105));

    auto traffic_sign = std::make_unique<map_api::TrafficSign>();
    traffic_sign->id = 42;
    traffic_sign->main_sign.type = map_api::MainSignType::kSpeedLimitBegin;
    traffic_sign->supplementary_signs.emplace_back(
        map_api::SupplementarySign{.type = map_api::SupplementarySignType::kRain});
    traffic_sign->supplementary_signs.emplace_back(
        map_api::SupplementarySign{.type = map_api::SupplementarySignType::kSnow});

    auto traffic_light = std::make_unique<map_api::TrafficLight>();
    traffic_light->id = 41;
    traffic_light->traffic_signals.emplace_back(
        map_api::TrafficLight::TrafficSignal{.id = 201,
                                             .color = map_api::TrafficLight::TrafficSignal::Color::kGreen,
                                             .mode = map_api::TrafficLight::TrafficSignal::Mode::kOff});
    traffic_light->traffic_signals.emplace_back(
        map_api::TrafficLight::TrafficSignal{.id = 202,
                                             .color = map_api::TrafficLight::TrafficSignal::Color::kRed,
                                             .mode = map_api::TrafficLight::TrafficSignal::Mode::kConstant});

    map.traffic_signs.emplace_back(std::move(traffic_sign));
    map.traffic_lights.emplace_back(std::move(traffic_light));
    map.stationary_objects.emplace_back(std::make_unique<map_api::StationaryObject>());

    {
        auto road_marking = std::make_unique<map_api::RoadMarking>();
        road_marking->id = 40;
        road_marking->monochrome_color = map_api::RoadMarking::Color::kRed;
        road_marking->type = map_api::RoadMarking::Type::kPaintedTrafficSign;
        road_marking->traffic_main_sign_type = map_api::MainSignType::kZebraCrossing;

        map_api::TrafficSignValue sign_value;
        sign_value.value = 123.45;
        sign_value.value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
        sign_value.text = "Speed Limit 50";
        road_marking->value = sign_value;

        map.road_markings.emplace_back(std::move(road_marking));
    }

    map.lane_boundaries.front()->limiting_structures.push_back(*(map.stationary_objects.front()));

    return map;
}

class MapApiToAstasMapConverterImplTest : public testing::Test
{
  public:
    void SetUp() override
    {
        astas_map_.SetSourceMapType(AstasMap::SourceMapType::kOpenDrive);
        converter_ = std::make_unique<MapApiToAstasMapConverterImpl>(unique_id_provider_, osi_map_data_, astas_map_);
    }

  protected:
    AstasMap astas_map_;
    service::utility::UniqueIdProvider unique_id_provider_{};
    map_api::Map osi_map_data_{CreateMapWithConnectedLanes()};
    std::unique_ptr<MapApiToAstasMapConverterImpl> converter_;
};

TEST_F(MapApiToAstasMapConverterImplTest, GivenAstasMapThatSourceTypeIsNotSet_WhenConvert_ThenThrow)
{
    // Arrange
    map_api::Map data{};
    AstasMap astas_map{};
    MapApiToAstasMapConverterImpl converter{unique_id_provider_, data, astas_map};

    // Act & Assert
    EXPECT_ANY_THROW(converter.Convert());
}

TEST_F(MapApiToAstasMapConverterImplTest, GivenEmptyMap_WhenConvertToAstasMap_ThenNoThrow)
{
    // Arrange
    map_api::Map data{};
    MapApiToAstasMapConverterImpl converter{unique_id_provider_, data, astas_map_};

    // Act & Assert
    EXPECT_NO_THROW(converter.Convert());
}

TEST_F(MapApiToAstasMapConverterImplTest, GivenMapWithProjectString_WhenConvertToAstasMap_ThenProjectStringIsConverted)
{
    // Arrange
    map_api::Map data{};
    data.projection_string =
        "proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=-194000 +y_0=-5346000 +datum=WGS84 +units=m +no_defs";

    MapApiToAstasMapConverterImpl converter{unique_id_provider_, data, astas_map_};

    // Act
    converter.Convert();

    // Assert
    EXPECT_EQ(astas_map_.projection_string, data.projection_string);
}

TEST_F(MapApiToAstasMapConverterImplTest, GivenEmptyMap_WhenConvertToAstasMap_ThenNoLaneGroupIsCreated)
{
    // Arrange
    map_api::Map data{};
    MapApiToAstasMapConverterImpl converter{unique_id_provider_, data, astas_map_};

    // Act
    converter.Convert();

    // Assert
    ASSERT_EQ(astas_map_.GetLaneGroups().size(), 0);
    const auto default_lane_group_id = mantle_api::UniqueId{1};
}

TEST_F(MapApiToAstasMapConverterImplTest, GivenMapApiWithLaneGroups_WhenConvertToAstasMap_ThenLaneGroupsAreConverted)
{
    // Arrange
    const mantle_api::UniqueId expected_lane_group_1_id{1};
    const mantle_api::UniqueId expected_lane_group_2_id{2};

    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(astas_map_.GetLaneGroups().size(), 2);
    ASSERT_TRUE(astas_map_.ContainsLaneGroup(expected_lane_group_1_id));
    const auto lane_group_1 = astas_map_.GetLaneGroup(expected_lane_group_1_id);
    EXPECT_EQ(lane_group_1.type, LaneGroup::Type::kOneWay);
    EXPECT_THAT(lane_group_1.lane_ids, ::testing::ElementsAre(0, 1));
    EXPECT_THAT(lane_group_1.lane_boundary_ids, ::testing::ElementsAre(100, 101, 102));

    ASSERT_TRUE(astas_map_.ContainsLaneGroup(expected_lane_group_2_id));
    const auto lane_group_2 = astas_map_.GetLaneGroup(expected_lane_group_2_id);
    EXPECT_EQ(lane_group_2.type, LaneGroup::Type::kOther);
    EXPECT_THAT(lane_group_2.lane_ids, ::testing::ElementsAre(2, 3));
    EXPECT_THAT(lane_group_2.lane_boundary_ids, ::testing::ElementsAre(103, 104, 105));
}

TEST_F(MapApiToAstasMapConverterImplTest,
       GivenMapApiWithLaneBoundaries_WhenConvertToAstasMap_ThenBoundariesAreConverted)
{
    // Arrange
    const std::vector<map_api::Identifier> expect_lane_boundary_ids{100, 101, 102, 103, 104, 105};

    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(astas_map_.GetLaneBoundaries().size(), 6);
    for (const auto& lane_boundary : astas_map_.GetLaneBoundaries())
    {
        EXPECT_THAT(expect_lane_boundary_ids, ::testing::Contains(lane_boundary.id));
    }
}

TEST_F(MapApiToAstasMapConverterImplTest, GivenMapApiWithLanes_WhenConvertToAstasMap_ThenLanesAreConvertedCorrectly)
{
    // Arrange
    const std::vector<map_api::Identifier> expect_lane_ids{0, 1, 2, 3};

    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(astas_map_.GetLanes().size(), 4);

    const auto lane_0 = astas_map_.GetLane(0);
    EXPECT_EQ(lane_0.left_lane_boundaries, std::vector<mantle_api::UniqueId>{100});
    EXPECT_EQ(lane_0.right_lane_boundaries, std::vector<mantle_api::UniqueId>{101});
    EXPECT_EQ(lane_0.successors, std::vector<mantle_api::UniqueId>{2});
    EXPECT_EQ(lane_0.right_adjacent_lanes, std::vector<mantle_api::UniqueId>{1});

    const auto lane_1 = astas_map_.GetLane(1);
    EXPECT_EQ(lane_1.left_lane_boundaries, std::vector<mantle_api::UniqueId>{101});
    EXPECT_EQ(lane_1.right_lane_boundaries, std::vector<mantle_api::UniqueId>{102});
    EXPECT_EQ(lane_1.left_adjacent_lanes, std::vector<mantle_api::UniqueId>{0});
    EXPECT_EQ(lane_1.successors, std::vector<mantle_api::UniqueId>{3});

    const auto lane_2 = astas_map_.GetLane(2);
    EXPECT_EQ(lane_2.left_lane_boundaries, std::vector<mantle_api::UniqueId>{103});
    EXPECT_EQ(lane_2.right_lane_boundaries, std::vector<mantle_api::UniqueId>{104});
    EXPECT_EQ(lane_2.right_adjacent_lanes, std::vector<mantle_api::UniqueId>{3});
    EXPECT_EQ(lane_2.predecessors, std::vector<mantle_api::UniqueId>{0});

    const auto lane_3 = astas_map_.GetLane(3);
    EXPECT_EQ(lane_3.left_lane_boundaries, std::vector<mantle_api::UniqueId>{104});
    EXPECT_EQ(lane_3.right_lane_boundaries, std::vector<mantle_api::UniqueId>{105});
    EXPECT_EQ(lane_3.left_adjacent_lanes, std::vector<mantle_api::UniqueId>{2});
    EXPECT_EQ(lane_3.predecessors, std::vector<mantle_api::UniqueId>{1});
}

TEST_F(MapApiToAstasMapConverterImplTest,
       GivenMapApiWithRoadMarkings_WhenConvertToAstasMap_ThenRoadMarkingsAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(astas_map_.traffic_signs.size(), 3);
    const auto* ground_sign = dynamic_cast<GroundSign*>(astas_map_.traffic_signs.at(2).get());
    ASSERT_TRUE(ground_sign);
    EXPECT_EQ(ground_sign->id, 40);
    EXPECT_EQ(ground_sign->type, osi::OsiTrafficSignType::kZebraCrossing);
    EXPECT_EQ(ground_sign->marking_color, osi::OsiRoadMarkingColor::kRed);
    EXPECT_EQ(ground_sign->marking_type, osi::OsiRoadMarkingsType::kPaintedTrafficSign);
    EXPECT_EQ(ground_sign->value_information.value, 123.45);
    EXPECT_EQ(ground_sign->value_information.value_unit, osi::OsiTrafficSignValueUnit::kKilometerPerHour);
    EXPECT_EQ(ground_sign->value_information.text, "Speed Limit 50");
}

TEST_F(MapApiToAstasMapConverterImplTest,
       GivenMapApiWithTrafficSigns_WhenConvertToAstasMap_ThenTrafficSignsAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(astas_map_.traffic_signs.size(), 3);
    const auto* traffic_sign = dynamic_cast<TrafficSign*>(astas_map_.traffic_signs.at(0).get());
    ASSERT_TRUE(traffic_sign);
    EXPECT_EQ(traffic_sign->id, 42);
    EXPECT_EQ(traffic_sign->type, osi::OsiTrafficSignType::kSpeedLimitBegin);

    const auto* mounted_sign = dynamic_cast<MountedSign*>(astas_map_.traffic_signs.at(1).get());
    ASSERT_TRUE(mounted_sign);
    EXPECT_EQ(mounted_sign->supplementary_signs.size(), 2);
    EXPECT_EQ(mounted_sign->supplementary_signs.at(0).type, OsiSupplementarySignType::kRain);
    EXPECT_EQ(mounted_sign->supplementary_signs.at(1).type, OsiSupplementarySignType::kSnow);
}

TEST_F(MapApiToAstasMapConverterImplTest,
       GivenMapApiWithTrafficLights_WhenConvertToAstasMap_ThenTrafficLightsAreConverted)
{
    // Arrange in SetUp
    // Act
    converter_->Convert();

    // Assert
    ASSERT_EQ(astas_map_.traffic_lights.size(), 1);
    const auto traffic_light = astas_map_.traffic_lights.at(0);
    EXPECT_EQ(traffic_light.id, 41);

    ASSERT_EQ(traffic_light.light_bulbs.size(), 2);
    EXPECT_EQ(traffic_light.light_bulbs.at(0).id, 201);
    EXPECT_EQ(traffic_light.light_bulbs.at(1).id, 202);
}

}  // namespace astas::environment::map::map
