/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICLIGHTCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICLIGHTCONVERTER_H

#include "Core/Environment/Map/AstasMap/astas_map.h"

#include <MapAPI/traffic_light.h>

namespace astas::environment::map
{
TrafficLightBulb ConvertTrafficLightBulb(const map_api::TrafficLight::TrafficSignal& from);

TrafficLight ConvertTrafficLight(const map_api::TrafficLight& from);

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_TRAFFICLIGHTCONVERTER_H
