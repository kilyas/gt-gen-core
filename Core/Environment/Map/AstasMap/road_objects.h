/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_ROADOBJECTS_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_ROADOBJECTS_H

#include "Core/Service/Osi/stationary_object_types.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/vector.h>

#include <string>
#include <vector>

namespace astas::environment::map
{

struct RoadObject
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> base_polygon;
    std::string name;
    mantle_api::Pose pose;
    mantle_api::Dimension3 dimensions;
    osi::StationaryObjectEntityType type{osi::StationaryObjectEntityType::kUnknown};
    osi::StationaryObjectEntityMaterial material{osi::StationaryObjectEntityMaterial::kOther};
    mantle_api::UniqueId id{0};
};

using RoadObjects = std::vector<RoadObject>;

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_ROADOBJECTS_H
