/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_SIGNS_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_SIGNS_H

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Service/Osi/road_marking_types.h"
#include "Core/Service/Osi/traffic_sign_types.h"
#include "Core/Service/Utility/unique_id_provider.h"

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/pose.h>
#include <fmt/format.h>

#include <algorithm>
#include <cstdint>
#include <memory>
#include <string>

namespace astas::environment::map
{

enum class OsiSupplementarySignType
{
    kUnknown = 0,
    kOther = 1,
    kNoSign = 2,
    kText = 41,
    kSpace = 39,
    kTime = 26,
    kArrow = 30,
    kConstrainedTo = 46,
    kExcept = 45,
    kValidForDistance = 3,
    kPriorityRoadBottomLeftFourWay = 27,
    kPriorityRoadTopLeftFourWay = 28,
    kPriorityRoadBottomLeftThreeWayStraight = 32,
    kPriorityRoadBottomLeftThreeWaySideways = 33,
    kPriorityRoadTopLeftThreeWayStraight = 34,
    kPriorityRoadBottomRightFourWay = 29,
    kPriorityRoadTopRightFourWay = 31,
    kPriorityRoadBottomRightThreeWayStraight = 35,
    kPriorityRoadBottomRightThreeWaySideway = 36,
    kPriorityRoadTopRightThreeWayStraight = 37,
    kValidInDistance = 4,
    kStopIn = 25,
    kLeftArrow = 11,
    kLeftBendArrow = 13,
    kRightArrow = 12,
    kRightBendArrow = 14,
    kAccident = 40,
    kSnow = 9,
    kFog = 8,
    kRollingHighwayInformation = 48,
    kServices = 47,
    kTimeRange = 5,
    kParkingDiscTimeRestriction = 43,
    kWeight = 6,
    kWet = 44,
    kParkingConstraint = 42,
    kNoWaitingSideStripes = 38,
    kRain = 7,
    kSnowRain = 10,
    kNight = 19,
    kStop4Way = 21,
    kTruck = 15,
    kTractorsMayBePassed = 16,
    kHazardous = 17,
    kTrailer = 18,
    kZone = 20,
    kMotorcycle = 22,
    kMotorcycleAllowed = 23,
    kCar = 24
};

enum class OsiSupplementaryTrafficSignActor
{
    kUnknown = 0,
    kOther = 1,
    kNoActor = 2,
    kAgriculturalVehicles = 3,
    kBicycles = 4,
    kBuses = 5,
    kCampers = 6,
    kCaravans = 7,
    kCars = 8,
    kCarsWithCaravans = 9,
    kCarsWithTrailers = 10,
    kCattle = 11,
    kChildren = 12,
    kConstructionVehicles = 13,
    kDeliveryVehicles = 14,
    kDisabledPersons = 15,
    kEbikes = 16,
    kElectricVehicles = 17,
    kEmergencyVehicles = 18,
    kFerryUsers = 19,
    kForestryVehicles = 20,
    kHazardousGoodsVehicles = 21,
    kHorseCarriages = 22,
    kHorseRiders = 23,
    kInlineSkaters = 24,
    kMedicalVehicles = 25,
    kMilitaryVehicles = 26,
    kMopeds = 27,
    kMotorcycles = 28,
    kMotorizedMultitrackVehicles = 29,
    kOperationalAndUtilityVehicles = 30,
    kPedestrians = 31,
    kPublicTransportVehicles = 32,
    kRailroadTraffic = 33,
    kResidents = 34,
    kSlurryTransport = 35,
    kTaxis = 36,
    kTractors = 37,
    kTrailers = 38,
    kTrams = 39,
    kTrucks = 40,
    kTrucksWithSemitrailers = 41,
    kTrucksWithTrailers = 42,
    kVehiclesWithGreenBadges = 43,
    kVehiclesWithRedBadges = 44,
    kVehiclesWithYellowBadges = 45,
    kWaterPollutantVehicles = 46,
    kWinterSportspeople = 47
};

struct SignValueInformation
{
    std::string text;
    double value{0.0};
    osi::OsiTrafficSignValueUnit value_unit{osi::OsiTrafficSignValueUnit::kUnknown};

    bool operator==(const SignValueInformation& rhs) const
    {
        return text == rhs.text && mantle_api::AlmostEqual(value, rhs.value, MANTLE_API_DEFAULT_EPS, true) &&
               value_unit == rhs.value_unit;
    }
};

struct TrafficSign
{
    TrafficSign() = default;
    TrafficSign(const TrafficSign&) = default;
    TrafficSign(TrafficSign&&) = default;
    TrafficSign& operator=(const TrafficSign&) = default;
    TrafficSign& operator=(TrafficSign&&) = default;

    virtual ~TrafficSign() = default;

    const std::vector<mantle_api::UniqueId>& GetAssignedLanes() const& { return assigned_lanes; }

    std::vector<mantle_api::UniqueId> assigned_lanes;

    mantle_api::UniqueId id{0};
    std::string stvo_id;
    osi::OsiTrafficSignType type{osi::OsiTrafficSignType::kUnknown};
    SignValueInformation value_information;

    mantle_api::Pose pose;
    mantle_api::Dimension3 dimensions;

    osi::OsiTrafficSignVariability variability{osi::OsiTrafficSignVariability::kUnknown};
    osi::OsiTrafficSignDirectionScope direction_scope{osi::OsiTrafficSignDirectionScope::kUnknown};

    static bool AreTrafficSignTypeValueAndPositionEqual(const TrafficSign& first, const TrafficSign& second)
    {
        return first.type == second.type && first.value_information == second.value_information &&
               first.pose.position == second.pose.position && first.pose.orientation == second.pose.orientation;
    }
};

struct GroundSign : public TrafficSign
{
    GroundSign()
    {
        variability = osi::OsiTrafficSignVariability::kFixed;
        direction_scope = osi::OsiTrafficSignDirectionScope::kNoDirection;
    }

    osi::OsiRoadMarkingsType marking_type{osi::OsiRoadMarkingsType::kUnknown};
    osi::OsiRoadMarkingColor marking_color{osi::OsiRoadMarkingColor::kUnknown};
};

struct MountedSign : public TrafficSign
{
    struct SupplementarySign
    {
        MountedSign* main_sign{nullptr};
        osi::OsiTrafficSignVariability variability{osi::OsiTrafficSignVariability::kUnknown};
        std::vector<OsiSupplementaryTrafficSignActor> actors;
        OsiSupplementarySignType type{OsiSupplementarySignType::kUnknown};
        std::vector<SignValueInformation> value_information;
        mantle_api::Pose pose;
        mantle_api::Dimension3 dimensions;

        const std::vector<mantle_api::UniqueId>& GetAssignedLanes() const& { return main_sign->assigned_lanes; }

        bool operator==(const SupplementarySign& rhs) const
        {
            return this->main_sign == rhs.main_sign && this->variability == rhs.variability &&
                   this->actors == rhs.actors && this->type == rhs.type &&
                   this->value_information == rhs.value_information && this->pose.position == rhs.pose.position &&
                   this->pose.orientation == rhs.pose.orientation && this->dimensions == rhs.dimensions;
        }
    };

    std::vector<SupplementarySign> supplementary_signs;
};

/// @todo Consider using std::unique_ptr
using GroundSigns = std::vector<std::shared_ptr<GroundSign>>;
using MountedSigns = std::vector<std::shared_ptr<MountedSign>>;
using TrafficSigns = std::vector<std::shared_ptr<TrafficSign>>;

}  // namespace astas::environment::map

template <>
struct fmt::formatter<astas::environment::map::GroundSign>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const astas::environment::map::GroundSign& ground_sign, FormatContext& ctx)
    {
        return format_to(ctx.out(), "Ground-Sign {}", ground_sign.id);
    }
};

template <>
struct fmt::formatter<astas::environment::map::SignValueInformation>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const astas::environment::map::SignValueInformation& value_information, FormatContext& ctx)
    {
        return format_to(ctx.out(), "{}", value_information.value);
    }
};

template <>
struct fmt::formatter<astas::environment::map::MountedSign::SupplementarySign>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const astas::environment::map::MountedSign::SupplementarySign& supplementary_sign, FormatContext& ctx)
    {
        std::string type_string;
        switch (supplementary_sign.type)
        {
            case astas::environment::map::OsiSupplementarySignType::kSnow:
                type_string = "Snow";
                break;
            case astas::environment::map::OsiSupplementarySignType::kRain:
                type_string = "Rain";
                break;
            case astas::environment::map::OsiSupplementarySignType::kFog:
                type_string = "Fog";
                break;
            case astas::environment::map::OsiSupplementarySignType::kTime:
                type_string = "Time";
                break;
            default:
                type_string = "N/A";
                break;
        }

        return format_to(
            ctx.out(),
            "Type: {}, Value Information: {}\n",
            type_string,
            fmt::join(supplementary_sign.value_information.begin(), supplementary_sign.value_information.end(), ","));
    }
};

template <>
struct fmt::formatter<astas::environment::map::MountedSign>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const astas::environment::map::MountedSign& mounted_sign, FormatContext& ctx)
    {
        return format_to(
            ctx.out(),
            "ID: {}, value: {}, supplementary signs: \n{}",
            mounted_sign.id,
            mounted_sign.value_information,
            fmt::join(mounted_sign.supplementary_signs.begin(), mounted_sign.supplementary_signs.end(), "\n"));
    }
};

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_SIGNS_H
