/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/utility.h"

#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{
using units::literals::operator""_m;

//-------- ClosestPointOnLineSegment -----------------------------------------------------------------------------------

TEST(ClosestPointOnLineSegmentTest,
     GivenLineSegmentWithEqualPoints_WhenClosestPointIsRequested_ThenAnyLineSegmentPointWillBeReturned)
{
    mantle_api::Vec3<units::length::meter_t> point{};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{1.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point = line_seg_first_point;

    mantle_api::Vec3<units::length::meter_t> expected_closest_point{line_seg_first_point};

    const auto closest_point = ClosestPointOnLineSegment(point, line_seg_first_point, line_seg_second_point);

    EXPECT_TRIPLE_NEAR(expected_closest_point, closest_point, orientation_accuracy);
}

TEST(ClosestPointOnLineSegmentTest,
     GivenPointBeforeFirstLineSegmentPoint_WhenClosestPointIsRequested_ThenFirstPointWillBeReturned)
{
    ///   x      x---------------x
    ///   p     ls1             ls2

    mantle_api::Vec3<units::length::meter_t> point{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{2.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point{3.0_m, 0.0_m, 0.0_m};

    mantle_api::Vec3<units::length::meter_t> expected_closest_point{line_seg_first_point};

    const auto closest_point = ClosestPointOnLineSegment(point, line_seg_first_point, line_seg_second_point);

    EXPECT_TRIPLE_NEAR(expected_closest_point, closest_point, orientation_accuracy);
}

TEST(ClosestPointOnLineSegmentTest,
     GivenPointAfterSecondLineSegmentPoint_WhenClosestPointIsRequested_ThenSecondPointWillBeReturned)
{
    ///  x---------------x       x
    /// ls1             ls2      p

    mantle_api::Vec3<units::length::meter_t> point{4.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{2.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point{3.0_m, 0.0_m, 0.0_m};

    mantle_api::Vec3<units::length::meter_t> expected_closest_point{line_seg_second_point};

    const auto closest_point = ClosestPointOnLineSegment(point, line_seg_first_point, line_seg_second_point);

    EXPECT_TRIPLE_NEAR(expected_closest_point, closest_point, orientation_accuracy);
}

TEST(
    ClosestPointOnLineSegmentTest,
    GivenPointBetweenLineSegmentPointsButVerticallyShifted_WhenClosestPointIsRequested_ThenPointOnTheLineSegmentWillBeReturned)
{
    ///          p
    ///          x
    ///
    ///  x----------------x
    /// ls1              ls2

    mantle_api::Vec3<units::length::meter_t> point{2.5_m, 0.0_m, 2.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{2.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point{3.0_m, 0.0_m, 0.0_m};

    mantle_api::Vec3<units::length::meter_t> expected_closest_point{2.5_m, 0.0_m, 0.0_m};

    const auto closest_point = ClosestPointOnLineSegment(point, line_seg_first_point, line_seg_second_point);

    EXPECT_TRIPLE_NEAR(expected_closest_point, closest_point, orientation_accuracy);
}

//---------- ClosestPointOnBoundary ------------------------------------------------------------------------------------

map::LaneBoundary CreateLaneBoundary(const std::vector<mantle_api::Vec3<units::length::meter_t>>& points)
{
    const double dummy_width = 0.0;
    const double dummy_height = 0.0;
    const LaneBoundary::Type dummy_type = LaneBoundary::Type::kOther;
    const LaneBoundary::Color dummy_color = LaneBoundary::Color::kNone;
    const mantle_api::UniqueId dummy_id = 0;

    LaneBoundary::Points lane_boundary_points;

    for (const auto& point : points)
    {
        lane_boundary_points.push_back(LaneBoundary::Point{point, dummy_width, dummy_height});
    }

    return LaneBoundary{dummy_id, dummy_type, dummy_color, lane_boundary_points};
}

TEST(ClosestPointOnLaneBoundaryTest,
     GivenPointClosestToFirstLaneBoundaryPoint_WhenClosestPointIsRequested_ThenFirsLaneBoundaryPointWillBeReturned)
{
    ///   x      x------x------x
    ///   p     lb1    lb2    lb3

    const std::vector<mantle_api::Vec3<units::length::meter_t>> lane_boundary_points{
        {0.0_m, 0.0_m, 0.0_m},
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
    };

    const auto lane_boundary = CreateLaneBoundary(lane_boundary_points);
    mantle_api::Vec3<units::length::meter_t> point{0.0_m, 0.0_m, 0.0_m};

    const mantle_api::Vec3<units::length::meter_t> expected_closest_point = lane_boundary_points.front();

    const auto closest_point = ClosestPointOnBoundary(point, lane_boundary);

    EXPECT_TRIPLE_NEAR(expected_closest_point, closest_point, orientation_accuracy);
}

TEST(ClosestPointOnLaneBoundaryTest,
     GivenPointClosestToLastLaneBoundaryPoint_WhenClosestPointIsRequested_ThenLastLaneBoundaryPointWillBeReturned)
{
    ///   x------x------x       x
    ///  lb1    lb2    lb3      p

    const std::vector<mantle_api::Vec3<units::length::meter_t>> lane_boundary_points{
        {0.0_m, 0.0_m, 0.0_m},
        {1.0_m, 0.0_m, 0.0_m},
        {2.0_m, 0.0_m, 0.0_m},
    };

    const auto lane_boundary = CreateLaneBoundary(lane_boundary_points);
    mantle_api::Vec3<units::length::meter_t> point{3.0_m, 0.0_m, 0.0_m};

    const mantle_api::Vec3<units::length::meter_t> expected_closest_point = lane_boundary_points.back();

    const auto closest_point = ClosestPointOnBoundary(point, lane_boundary);

    EXPECT_TRIPLE_NEAR(expected_closest_point, closest_point, orientation_accuracy);
}

TEST(
    ClosestPointOnLaneBoundaryTest,
    GivenPointAtEqualDistanceToMultipleLaneBoundarySegments_WhenClosestPointIsRequested_ThenFirstFoundClosestPointWillBeReturned)
{
    ///  lb2       lb3
    ///   x---------x
    ///   |         |
    ///   |    x    |
    ///   |    p    |
    ///   x         x
    ///  lb1       lb4

    const std::vector<mantle_api::Vec3<units::length::meter_t>> lane_boundary_points{
        {0.0_m, 0.0_m, 0.0_m},
        {0.0_m, 1.0_m, 0.0_m},
        {1.0_m, 1.0_m, 0.0_m},
        {1.0_m, 0.0_m, 0.0_m},
    };

    const auto lane_boundary = CreateLaneBoundary(lane_boundary_points);
    mantle_api::Vec3<units::length::meter_t> point{0.5_m, 0.5_m, 0.0_m};

    mantle_api::Vec3<units::length::meter_t> expected_closest_point{0.0_m, 0.5_m, 0.0_m};

    const auto closest_point = ClosestPointOnBoundary(point, lane_boundary);

    EXPECT_TRIPLE_NEAR(expected_closest_point, closest_point, orientation_accuracy);
}

TEST(GetPolylinePointsExtremesTest,
     GivenDiagonalPolylineWithThreePoints_WhenCalculatingPolylineExtremes_ThenPolylineExtremesAreCorrect)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline_points{
        {100_m, 100_m, 1_m}, {120_m, 120_m, 1_m}, {150_m, 150_m, 1_m}};
    std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>
        expected_polyline_extremes{{100_m, 100_m, 1_m}, {150_m, 150_m, 1_m}};

    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>
        actual_polyline_extremes = GetPolylinePointsExtremes(polyline_points);

    EXPECT_EQ(expected_polyline_extremes.first.x, actual_polyline_extremes.first.x);
    EXPECT_EQ(expected_polyline_extremes.first.y, actual_polyline_extremes.first.y);
    EXPECT_EQ(expected_polyline_extremes.first.z, actual_polyline_extremes.first.z);
    EXPECT_EQ(expected_polyline_extremes.second.x, actual_polyline_extremes.second.x);
    EXPECT_EQ(expected_polyline_extremes.second.y, actual_polyline_extremes.second.y);
    EXPECT_EQ(expected_polyline_extremes.second.z, actual_polyline_extremes.second.z);
}

TEST(GetPolylinePointsExtremesTest, GivenPolyline_WhenSettingZValueToZero_ThenZCoordinateOfAllPointsIsZero)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> polyline_points{
        {100_m, 100_m, 1_m}, {120_m, 120_m, 0_m}, {150_m, 150_m, -60_m}};

    SetZValueForPoints(0, polyline_points);

    for (const auto& point : polyline_points)
    {
        EXPECT_DOUBLE_EQ(0.0, point.z());
    }
}

TEST(GetPolylinePointsExtremesTest, GivenPolyline_WhenFlatteningCoordinates_ThenXandYCoordinatesAreUnchanged)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_polyline_points{
        {100_m, 100_m, 0_m}, {120_m, 120_m, 0_m}, {150_m, 150_m, 0_m}};
    std::vector<mantle_api::Vec3<units::length::meter_t>> polyline_points{
        {100_m, 100_m, 1_m}, {120_m, 120_m, 0_m}, {150_m, 150_m, -60_m}};

    SetZValueForPoints(0, polyline_points);

    ASSERT_EQ(expected_polyline_points.size(), polyline_points.size());
    for (std::size_t i{0}; i < expected_polyline_points.size(); ++i)
    {
        EXPECT_DOUBLE_EQ(expected_polyline_points[i].x(), polyline_points[i].x());
        EXPECT_DOUBLE_EQ(expected_polyline_points[i].y(), polyline_points[i].y());
    }
}

TEST(ConnectPolylinesCounterclockwiseTest,
     GivenLeftAndRightPolylines_WhenConnectPolylinesCounterclockwiseIsCalled_ThenCorrectlyConnectedPolylineIsReturned)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_connected_polyline{
        {10_m, 0_m, 0_m}, {10_m, 10_m, 0_m}, {0_m, 10_m, 0_m}, {0_m, 0_m, 0_m}};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> right_polyline{{10_m, 0_m, 0_m}, {10_m, 10_m, 0_m}};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> left_polyline{{0_m, 0_m, 0_m}, {0_m, 10_m, 0_m}};

    const auto connected_polyline = ConnectPolylinesCounterclockwise(right_polyline, left_polyline);

    EXPECT_EQ(expected_connected_polyline, connected_polyline);
}

TEST(CalculateVectorYawTest, GivenVectorDefinedByTwoPoints_WhenCalculateVectorYaw_ThenCorrectYawIsReturned)
{
    constexpr double expected_yaw{0.98279372324732905};
    mantle_api::Vec3<units::length::meter_t> first_point{0_m, 0_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> second_point{10_m, 15_m, 20_m};

    const double calculated_yaw = CalculateVectorYaw(first_point, second_point);

    EXPECT_DOUBLE_EQ(expected_yaw, calculated_yaw);
}

TEST(TrimPolylineToRangeTest,
     GivenPolylineBetweenTwoPoints_WhenTrimPolylineToRangeIsCalled_ThenOnlyPointsInRangeAreReturned)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_trimmed_polyline{
        {0.25_m, 0.25_m, 0.25_m}, {1_m, 1_m, 1_m}, {1.75_m, 1.75_m, 1.75_m}};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline{
        {0_m, 0_m, 0_m}, {1_m, 1_m, 1_m}, {2_m, 2_m, 2_m}, {3_m, 3_m, 3_m}};
    mantle_api::Vec3<units::length::meter_t> range_start{0_m, 0.5_m, 0_m};
    mantle_api::Vec3<units::length::meter_t> range_end{1.5_m, 2_m, 2_m};

    const auto trimmed_polyline = TrimPolylineToRange(polyline, range_start, range_end);

    EXPECT_EQ(expected_trimmed_polyline, trimmed_polyline);
}

TEST(TrimPolylineToRangeTest,
     GivenPolylineAndSamePointsForStartAndEndRange_WhenTrimPolylineToRangeIsCalled_ThenTrimmedPolylineContainsOnePoint)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_trimmed_polyline{{0.5_m, 0.5_m, 0.5_m}};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline{
        {0_m, 0_m, 0_m}, {1_m, 1_m, 1_m}, {2_m, 2_m, 2_m}, {3_m, 3_m, 3_m}};
    mantle_api::Vec3<units::length::meter_t> range_start{0.5_m, 0.5_m, 0.5_m};
    mantle_api::Vec3<units::length::meter_t> range_end{0.5_m, 0.5_m, 0.5_m};

    const auto trimmed_polyline = TrimPolylineToRange(polyline, range_start, range_end);

    EXPECT_EQ(expected_trimmed_polyline, trimmed_polyline);
}

TEST(TrimPolylineToRangeTest,
     GivenPolylineAndEndRangeBeforeStartRange_WhenTrimPolylineToRangeIsCalled_ThenEmptyPolylineIsReturned)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> expected_trimmed_polyline{};
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline{
        {0_m, 0_m, 0_m}, {1_m, 1_m, 1_m}, {2_m, 2_m, 2_m}, {3_m, 3_m, 3_m}};
    mantle_api::Vec3<units::length::meter_t> range_start{3.0_m, 3.0_m, 3.0_m};
    mantle_api::Vec3<units::length::meter_t> range_end{0.0_m, 0.0_m, 0.0_m};

    const auto trimmed_polyline = TrimPolylineToRange(polyline, range_start, range_end);

    EXPECT_EQ(expected_trimmed_polyline, trimmed_polyline);
}

TEST(
    TrimPolylineToRangeTest,
    GivenPolylineAndStartRangeBeforeFirstPointAndEndRangeBeyondLastPoint_WhenTrimPolylineToRangeIsCalled_ThenPolylineIsNotChanged)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline{
        {1_m, 1_m, 1_m}, {2_m, 2_m, 2_m}, {3_m, 3_m, 3_m}};
    mantle_api::Vec3<units::length::meter_t> range_start{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> range_end{4.0_m, 4.0_m, 4.0_m};

    const auto trimmed_polyline = TrimPolylineToRange(polyline, range_start, range_end);

    EXPECT_EQ(polyline, trimmed_polyline);
}

TEST(TrimPolylineToRangeTest, GivenEmptyPolyline_WhenTrimPolylineToRangeIsCalled_ThenEmptyPolylineIsReturned)
{
    const std::vector<mantle_api::Vec3<units::length::meter_t>> polyline{};
    mantle_api::Vec3<units::length::meter_t> range_start{};
    mantle_api::Vec3<units::length::meter_t> range_end{};

    const auto trimmed_polyline = TrimPolylineToRange(polyline, range_start, range_end);

    ASSERT_TRUE(trimmed_polyline.empty());
}

TEST(NdsLandmarkParserUtilityTest,
     GivenObjectShapeExtremesAndDefaultReferencePose_WhenCalculatingPosition_ThenPositionIsInMiddleOfExtremes)
{
    mantle_api::Vec3<units::length::meter_t> expected_center_point{150_m, 150_m, 1_m};
    mantle_api::Vec3<units::length::meter_t> pos{};
    mantle_api::Orientation3<units::angle::radian_t> orientation{};
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>> extremes{
        {100_m, 100_m, 1_m}, {200_m, 200_m, 1_m}};
    const mantle_api::Dimension3 dimensions{extremes.second.x - extremes.first.x,
                                            extremes.second.y - extremes.first.y,
                                            extremes.second.z - extremes.first.z};

    const auto position = service::glmwrapper::CalculateObjectPosition(extremes, pos, orientation, dimensions);

    EXPECT_TRIPLE(expected_center_point, position);
}

TEST(
    NdsLandmarkParserUtilityTest,
    GivenObjectShapeExtremesAndEmptyReferencePoseOffsetedBy1mInXDirection_WhenCalculatingPosition_ThenPositionIsShifted1mInXDirectionFromMiddleOfExtremes)
{
    mantle_api::Vec3<units::length::meter_t> expected_center_point{151_m, 150_m, 1_m};
    mantle_api::Vec3<units::length::meter_t> pos{};
    mantle_api::Orientation3<units::angle::radian_t> orientation{};
    pos.x = 1_m;
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>> extremes{
        {100_m, 100_m, 1_m}, {200_m, 200_m, 1_m}};
    const mantle_api::Dimension3 dimensions{extremes.second.x - extremes.first.x,
                                            extremes.second.y - extremes.first.y,
                                            extremes.second.z - extremes.first.z};

    const auto position = service::glmwrapper::CalculateObjectPosition(extremes, pos, orientation, dimensions);

    EXPECT_TRIPLE(expected_center_point, position);
}

TEST(NdsLandmarkParserUtilityTest,
     GivenPolylineExtremes_WhenCalculatingObjectDimensions_ThenCalculatedDimensionsAreCorrect)
{
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>> extremes{
        {100_m, 135_m, 1_m}, {200_m, 200_m, 2_m}};
    constexpr double expected_width{65};
    constexpr double expected_length{100};
    constexpr double expected_height{1.0};
    const mantle_api::Dimension3 expected_dimensions{units::length::meter_t{expected_length},
                                                     units::length::meter_t{expected_width},
                                                     units::length::meter_t{expected_height}};

    const auto dimensions = CalculateObjectDimensions(extremes);

    EXPECT_TRIPLE(expected_dimensions, dimensions);
}

TEST(NdsLandmarkParserUtilityTest,
     GivenPolylineExtremes_WhenCalculatingObjectDimensionsWithHeigthExplicitlySet_ThenCalculatedDimensionsAreCorrect)
{
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>> extremes{
        {100_m, 135_m, 1_m}, {200_m, 200_m, 2_m}};
    constexpr double expected_width{65};
    constexpr double expected_length{100};
    constexpr double expected_height{5.0};
    const mantle_api::Dimension3 expected_dimensions{units::length::meter_t{expected_length},
                                                     units::length::meter_t{expected_width},
                                                     units::length::meter_t{expected_height}};

    const auto dimensions = CalculateObjectDimensions(extremes, expected_height);

    EXPECT_TRIPLE(expected_dimensions, dimensions);
}

}  // namespace astas::environment::map
