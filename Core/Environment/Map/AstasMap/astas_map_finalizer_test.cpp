/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/astas_map_finalizer.h"

#include "Core/Environment/Map/AstasMap/Internal/lane_utility.h"
#include "Core/Environment/Map/AstasMap/astas_map.h"
#include "Core/Environment/Map/Common/exceptions.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{

class AstasMapFinalizerTest : public testing::Test
{
  protected:
    LaneBoundary SetupLaneBoundary(mantle_api::UniqueId id,
                                   const std::vector<LaneBoundary::Point>& lane_boundary_points)
    {
        LaneBoundary boundary{id, LaneBoundary::Type::kSolidLine, LaneBoundary::Color::kWhite};
        for (auto lane_boundary_point : lane_boundary_points)
        {
            boundary.points.push_back(lane_boundary_point);
        }
        return boundary;
    }

    AstasMap map_;
};

TEST_F(AstasMapFinalizerTest,
       GivenAstasMapWithOneLane_WhenRequestingBoundingBox_ThenWorldBoundingBoxIsEqualToLaneBoundingBox)
{
    // Finalize is called by the MapCatalogue
    auto astas_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(2, 21);

    const auto& map_bbox = astas_map->axis_aligned_world_bounding_box;
    const auto& lane_bbox = astas_map->GetLanes().front().axis_aligned_bounding_box;

    EXPECT_EQ(lane_bbox.bottom_left.x, map_bbox.bottom_left.x);
    EXPECT_EQ(lane_bbox.bottom_left.y, map_bbox.bottom_left.y);
    EXPECT_EQ(lane_bbox.top_right.x, map_bbox.top_right.x);
    EXPECT_EQ(lane_bbox.top_right.y, map_bbox.top_right.y);
}

TEST_F(AstasMapFinalizerTest,
       GivenAstasMapWithTwoLanes_WhenRequestingBoundingBox_ThenWorldBoundingBoxEncompassesBothLanes)
{
    // Finalize is called by the MapCatalogue
    auto astas_map = test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach();

    const auto& map_bbox = astas_map->axis_aligned_world_bounding_box;
    const auto& lane1 = astas_map->GetLanes().front();
    const auto& lane2 = astas_map->GetLanes().back();

    EXPECT_EQ(lane1.axis_aligned_bounding_box.bottom_left.x, map_bbox.bottom_left.x);
    EXPECT_EQ(lane1.axis_aligned_bounding_box.bottom_left.y, map_bbox.bottom_left.y);
    EXPECT_EQ(lane2.axis_aligned_bounding_box.top_right.x, map_bbox.top_right.x);
    EXPECT_EQ(lane2.axis_aligned_bounding_box.top_right.y, map_bbox.top_right.y);
}

TEST_F(AstasMapFinalizerTest,
       GivenMapWithLaneWithDuplicatedPreAndSuccessorIds_WhenFinalizing_ThenDuplicatedIdsAreRemoved)
{
    using units::literals::operator""_m;

    map_.AddLaneGroup({42, LaneGroup::Type::kOther});

    LaneBoundary right_boundary = SetupLaneBoundary(10, {{{0_m, 0_m, 0_m}, 0, 0}, {{0_m, 10_m, 0_m}, 0, 0}});
    LaneBoundary left_boundary = SetupLaneBoundary(20, {{{-10_m, 0_m, 0_m}, 0, 0}, {{-10_m, 10_m, 0_m}, 0, 0}});
    map_.AddLaneBoundary(42, right_boundary);
    map_.AddLaneBoundary(42, left_boundary);

    map::Lane lane{0};
    lane.right_lane_boundaries.push_back(10);
    lane.left_lane_boundaries.push_back(20);

    // duplicated successor and predecessor ids
    lane.predecessors.push_back(1);
    lane.predecessors.push_back(1);
    lane.successors.push_back(2);
    lane.successors.push_back(2);
    map_.AddLane(42, lane);

    AstasMapFinalizer finalizer(map_);
    finalizer.Finalize();

    auto* found_lane = map_.FindLane(0);
    ASSERT_NE(found_lane, nullptr);
    EXPECT_EQ(1, found_lane->predecessors.size());
    EXPECT_EQ(1, found_lane->successors.size());
}

TEST_F(
    AstasMapFinalizerTest,
    GivenMapWithBrokenLaneConnectionsAndLanesHavingIdenticalLastCenterlinePointAndFirstCenterlinePoint_WhenFinalizing_ThenSuccessorAndPredecessorLanesAreSetWhereLanePointsAreIdentical)
{
    using units::literals::operator""_m;

    map_.AddLaneGroup({42, LaneGroup::Type::kOther});

    LaneBoundary right_boundary = SetupLaneBoundary(10, {{{0_m, 0_m, 0_m}, 0, 0}, {{0_m, 10_m, 0_m}, 0, 0}});
    LaneBoundary left_boundary = SetupLaneBoundary(20, {{{-10_m, 0_m, 0_m}, 0, 0}, {{-10_m, 10_m, 0_m}, 0, 0}});
    map_.AddLaneBoundaries(42, {right_boundary, left_boundary});

    map::Lane start_lane{0};
    start_lane.right_lane_boundaries.push_back(10);
    start_lane.left_lane_boundaries.push_back(20);

    map::Lane following_lane{1};
    following_lane.right_lane_boundaries.push_back(10);
    following_lane.left_lane_boundaries.push_back(20);

    mantle_api::Vec3<units::length::meter_t> same_center_line_point_of_following_lanes{
        units::length::meter_t(3.0), units::length::meter_t(4.0), units::length::meter_t(1.0)};
    start_lane.center_line.push_back(
        {units::length::meter_t(0.0), units::length::meter_t(0.0), units::length::meter_t(0.0)});
    start_lane.center_line.push_back(same_center_line_point_of_following_lanes);

    following_lane.center_line.push_back(same_center_line_point_of_following_lanes);
    following_lane.center_line.push_back(
        {units::length::meter_t(6.0), units::length::meter_t(7.0), units::length::meter_t(3.0)});

    map_.AddLanes(42, {start_lane, following_lane});

    AstasMapFinalizer finalizer(map_);
    finalizer.Finalize();

    auto* found_lane = map_.FindLane(0);
    ASSERT_NE(found_lane, nullptr);
    ASSERT_EQ(1, found_lane->successors.size());
    EXPECT_EQ(1, found_lane->successors.at(0));
    EXPECT_EQ(0, found_lane->predecessors.size());

    auto* found_connected_lane = map_.FindLane(1);
    ASSERT_NE(found_connected_lane, nullptr);
    ASSERT_EQ(1, found_connected_lane->predecessors.size());
    EXPECT_EQ(0, found_connected_lane->predecessors.at(0));
    EXPECT_EQ(0, found_connected_lane->successors.size());
}

TEST_F(
    AstasMapFinalizerTest,
    GivenMapWithBrokenLaneConnectionsHavingLanesLastAndFirstCenterlinePointWithTooLargeDistance_WhenFinalizing_ThenSuccessorAndPredecessorLanesAreNotSet)
{
    using units::literals::operator""_m;
    double connecting_x{3.0};
    double connecting_y{4.0};
    double connecting_z{1.0};
    double distance_treshold{0.5};

    map_.AddLaneGroup({42, LaneGroup::Type::kOther});

    LaneBoundary right_boundary = SetupLaneBoundary(10, {{{0_m, 0_m, 0_m}, 0, 0}, {{0_m, 10_m, 0_m}, 0, 0}});
    LaneBoundary left_boundary = SetupLaneBoundary(20, {{{-10_m, 0_m, 0_m}, 0, 0}, {{-10_m, 10_m, 0_m}, 0, 0}});
    map_.AddLaneBoundaries(42, {right_boundary, left_boundary});

    map::Lane start_lane{0};
    start_lane.right_lane_boundaries.push_back(10);
    start_lane.left_lane_boundaries.push_back(20);

    map::Lane following_lane{1};
    following_lane.right_lane_boundaries.push_back(10);
    following_lane.left_lane_boundaries.push_back(20);

    start_lane.center_line.push_back(
        {units::length::meter_t(0.0), units::length::meter_t(0.0), units::length::meter_t(0.0)});
    start_lane.center_line.push_back({units::length::meter_t(connecting_x),
                                      units::length::meter_t(connecting_y),
                                      units::length::meter_t(connecting_z)});

    following_lane.center_line.push_back({units::length::meter_t(connecting_x + distance_treshold),
                                          units::length::meter_t(connecting_y),
                                          units::length::meter_t(connecting_z)});
    following_lane.center_line.push_back(
        {units::length::meter_t(6.0), units::length::meter_t(7.0), units::length::meter_t(3.0)});

    map_.AddLanes(42, {start_lane, following_lane});

    AstasMapFinalizer finalizer(map_);
    finalizer.Finalize();

    auto* found_lane = map_.FindLane(0);
    ASSERT_NE(found_lane, nullptr);
    EXPECT_EQ(0, found_lane->successors.size());
    EXPECT_EQ(0, found_lane->predecessors.size());

    auto* found_connected_lane = map_.FindLane(1);
    ASSERT_NE(found_connected_lane, nullptr);
    EXPECT_EQ(0, found_connected_lane->predecessors.size());
    EXPECT_EQ(0, found_connected_lane->successors.size());
}

}  // namespace astas::environment::map
