/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_LANEFLAGS_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_LANEFLAGS_H

#include <bitset>

namespace astas::environment::map
{

struct LaneFlags
{
  public:
    enum Flags : std::size_t
    {
        kShoulder = 0,
        kDrivable,
        kMergeLane,
        kSplitLane,
        kNormalLane,
        kEntryLane,
        kExitLane,
        kContinue,
        kParking,
        kBicycle,
        kBus,
        kFullyAttributed,
        kNumberOfFlags
    };

    // Lane Classification Types
    bool IsDrivable() const { return flags_[kDrivable]; }

    void SetDrivable() { flags_[kDrivable] = true; }
    void SetNonDrivable() { flags_[kDrivable] = false; }

    // Lane Classification SubTypes
    bool IsShoulderLane() const { return flags_[kShoulder]; }
    bool IsMergeLane() const { return flags_[kMergeLane]; }
    bool IsSplitLane() const { return flags_[kSplitLane]; }
    bool IsNormalLane() const { return flags_[kNormalLane]; }
    bool IsEntryLane() const { return flags_[kEntryLane]; }
    bool IsExitLane() const { return flags_[kExitLane]; }
    bool IsParking() const { return flags_[kParking]; }
    bool IsBicycle() const { return flags_[kBicycle]; }

    void SetShoulderLane() { flags_[kShoulder] = true; }
    void SetMergeLane() { flags_[kMergeLane] = true; }
    void SetSplitLane() { flags_[kSplitLane] = true; }
    void SetNormalLane() { flags_[kNormalLane] = true; }
    void SetEntryLane() { flags_[kEntryLane] = true; }
    void SetExitLane() { flags_[kExitLane] = true; }
    void SetParking() { flags_[kParking] = true; }
    void SetBicycle() { flags_[kBicycle] = true; }

    // GTGen Internal or non-OSI supported
    bool IsBus() const { return flags_[kBus]; }
    bool IsContinue() const { return flags_[kContinue]; }
    bool IsFullyAttributed() const { return flags_[kFullyAttributed]; }

    void SetBus() { flags_[kBus] = true; }
    void SetContinue() { flags_[kContinue] = true; }
    void SetFullyAttributed() { flags_[kFullyAttributed] = true; }

  private:
    std::bitset<kNumberOfFlags> flags_{};
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_LANEFLAGS_H
