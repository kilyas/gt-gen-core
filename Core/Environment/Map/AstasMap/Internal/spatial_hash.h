/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_SPATIALHASH_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_SPATIALHASH_H

#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/Map/Geometry/bounding_box.h"

#include <MantleAPI/Common/vector.h>

#include <unordered_map>
#include <vector>

namespace astas::environment::map
{
class SpatialHash
{
  public:
    using CellId = std::int32_t;
    using CellIds = std::vector<CellId>;
    using CellElement = const Lane*;
    using HashTable = std::unordered_map<CellId, std::vector<CellElement>>;

    explicit SpatialHash(const BoundingBox2d& world_bounding_box, double cell_size = 50.0);

    void InsertLanes(const Lanes& lanes);

    std::vector<const Lane*> GetLanes(const mantle_api::Vec3<units::length::meter_t>& world_position) const;

    std::size_t Size() const;

    CellId Hash(const mantle_api::Vec3<units::length::meter_t>& world_position) const;

    double GetWidth() const { return width_; };
    double GetHeight() const { return height_; };
    CellId GetNumberOfCells() const { return number_of_cells_; };
    glm::dvec2 GetWorldSpaceOffset() const { return world_space_offset_; };
    const HashTable& GetHashTable() const { return hash_table_; };

    inline static CellId InvalidCellId() { return std::numeric_limits<CellId>::max(); };
    inline static bool IsValidCellId(const CellId& cell_id) { return cell_id != InvalidCellId(); }

    BoundingBox2d GetCellBoundingBox(CellId cell_id) const;

  private:
    CellId ComputeNumberOfCells(double a, double b) const;
    SpatialHash::CellIds GetCellsIntersectingBoundingBox(const BoundingBox2d& bounding_box) const;
    bool ShouldAssignLaneToCell(const Lane& lane, CellId cell_id) const;
    bool IsAnyPointInsideCell(const std::vector<glm::dvec2>& points, CellId cell_id) const;
    bool IsLaneShapeIntersectingCell(const Polygon2d& shape, CellId cell_id) const;
    void AddLaneToCell(const CellElement& cell_element, const CellId& cell_id);
    bool IsPositionAssociatedWithLane(const Lane* lane, const mantle_api::Vec3<units::length::meter_t>& position) const;

    CellId CellIdToGridsColumnIndex(const CellId& cell_id) const;
    CellId CellIdToGridsRowIndex(const CellId& cell_id) const;
    CellId GridIndicesToCellId(const CellId& row, const CellId& column) const;

    double cell_size_;
    CellId num_cells_x_;
    CellId num_cells_y_;
    CellId number_of_cells_;
    double width_;
    double height_;

    glm::dvec2 world_space_offset_;

    HashTable hash_table_;
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_SPATIALHASH_H
