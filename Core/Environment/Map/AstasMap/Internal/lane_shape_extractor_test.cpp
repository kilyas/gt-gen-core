/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/Internal/lane_shape_extractor.h"

#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{
TEST(LaneShapeExtractorTest, GivenLaneWithOneBoundaryOnEachSide_WhenExtractingShape_ThenShapeContainsAllFourCourners)
{
    auto map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();

    auto lane = map->GetLanes().front();
    auto left_boundaries = map->GetLeftLaneBoundaries(&lane);
    auto right_boundaries = map->GetRightLaneBoundaries(&lane);

    ExtractLaneShape(lane, left_boundaries, right_boundaries);

    ASSERT_EQ(lane.shape_2d.points.size(), 4);
    EXPECT_EQ(lane.shape_2d.points.at(0), glm::dvec2(6000.0, -2.0));
    EXPECT_EQ(lane.shape_2d.points.at(1), glm::dvec2(0.0, -2.0));
    EXPECT_EQ(lane.shape_2d.points.at(2), glm::dvec2(0.0, 2.0));
    EXPECT_EQ(lane.shape_2d.points.at(3), glm::dvec2(6000.0, 2.0));
}

TEST(LaneShapeExtractorTest, GivenLaneWithMultipleChainedBoundaries_WhenExtractingShape_ThenShapeIsInCorrectOrder)
{
    // Note: The boundaries of this lane are divided into 2 parts on both sides. Also the ordering of the left
    // side boundary is messed up
    auto map = test_utils::MapCatalogue::MapOneLaneNorthingToWestingCurve();

    auto lane = map->GetLanes().front();
    auto left_boundaries = map->GetLeftLaneBoundaries(&lane);
    auto right_boundaries = map->GetRightLaneBoundaries(&lane);

    ExtractLaneShape(lane, left_boundaries, right_boundaries);

    ASSERT_EQ(lane.shape_2d.points.size(), 11);
    EXPECT_EQ(lane.shape_2d.points.at(0), glm::dvec2(-7.0, 9.0));
    EXPECT_EQ(lane.shape_2d.points.at(1), glm::dvec2(-5.0, 9.0));
    EXPECT_EQ(lane.shape_2d.points.at(2), glm::dvec2(0.0, 6.0));
    EXPECT_EQ(lane.shape_2d.points.at(3), glm::dvec2(1.0, 4.0));
    EXPECT_EQ(lane.shape_2d.points.at(4), glm::dvec2(2.0, 2.0));
    EXPECT_EQ(lane.shape_2d.points.at(5), glm::dvec2(2.0, 0.0));
    EXPECT_EQ(lane.shape_2d.points.at(6), glm::dvec2(-2.0, 0.0));
    EXPECT_EQ(lane.shape_2d.points.at(7), glm::dvec2(-2.0, 2.0));
    EXPECT_EQ(lane.shape_2d.points.at(8), glm::dvec2(-3.0, 4.0));
    EXPECT_EQ(lane.shape_2d.points.at(9), glm::dvec2(-5.0, 5.0));
    EXPECT_EQ(lane.shape_2d.points.at(10), glm::dvec2(-7.0, 5.0));
}

TEST(LaneShapeExtractorTest,
     GivenProblematicOdrCurveLaneWithMultipleChainedBoundaries_WhenExtractingShape_ThenShapeIsInCorrectOrder)
{
    // This map has been created from an ODR map. The problem with this curve is, that it contains
    // multiple boundaries on each side (with different markings). Thus they need to be put correctly together into one
    // right and one left boundary-chain and from there put into a shape
    auto map = test_utils::MapCatalogue::MapOdrProblematicCurve();

    auto lane = map->GetLanes().front();
    auto left_boundaries = map->GetLeftLaneBoundaries(&lane);
    auto right_boundaries = map->GetRightLaneBoundaries(&lane);

    ExtractLaneShape(lane, left_boundaries, right_boundaries);

    ASSERT_EQ(lane.shape_2d.points.size(), 149);
}

TEST(LaneShapeExtractorTest, GivenOdrProblematicStraightLane_WhenExtractingShape_ThenShapeIsInCorrectOrder)
{
    auto map = test_utils::MapCatalogue::MapOdrProblematicStraightLine();

    auto lane = map->GetLanes().front();
    auto left_boundaries = map->GetLeftLaneBoundaries(&lane);
    auto right_boundaries = map->GetRightLaneBoundaries(&lane);

    ExtractLaneShape(lane, left_boundaries, right_boundaries);

    ASSERT_EQ(lane.shape_2d.points.size(), 4);
    EXPECT_EQ(lane.shape_2d.points.at(0), glm::dvec2(-692.569, 2180));
    EXPECT_EQ(lane.shape_2d.points.at(1), glm::dvec2(-697.687, 2188.59));
    EXPECT_EQ(lane.shape_2d.points.at(2), glm::dvec2(-694.552, 2190.46));
    EXPECT_EQ(lane.shape_2d.points.at(3), glm::dvec2(-689.433, 2181.87));
}

TEST(LaneShapeExtractorTest, GivenOdrProblematicCurvyLane_WhenExtractingShape_ThenShapeIsInCorrectOrder)
{
    auto map = test_utils::MapCatalogue::MapOdrProblematicCurvyLine();

    auto lane = map->GetLanes().front();
    auto left_boundaries = map->GetLeftLaneBoundaries(&lane);
    auto right_boundaries = map->GetRightLaneBoundaries(&lane);

    ExtractLaneShape(lane, left_boundaries, right_boundaries);

    ASSERT_EQ(lane.shape_2d.points.size(), 112);
}

TEST(LaneShapeExtractorTest,
     GivenLaneWithDuplicatedBoundariesOnEachSide_WhenExtractingShape_ThenShapeIgnoresDuplicatedPoints)
{
    auto map = test_utils::MapCatalogue::MapOneLaneDuplicatedBoundariesOnEachSide();

    auto lane = map->GetLanes().front();
    auto left_boundaries = map->GetLeftLaneBoundaries(&lane);
    auto right_boundaries = map->GetRightLaneBoundaries(&lane);

    ExtractLaneShape(lane, left_boundaries, right_boundaries);

    ASSERT_EQ(lane.shape_2d.points.size(), 4);
    EXPECT_EQ(lane.shape_2d.points.at(0), glm::dvec2(6000.0, -2.0));
    EXPECT_EQ(lane.shape_2d.points.at(1), glm::dvec2(0.0, -2.0));
    EXPECT_EQ(lane.shape_2d.points.at(2), glm::dvec2(0.0, 2.0));
    EXPECT_EQ(lane.shape_2d.points.at(3), glm::dvec2(6000.0, 2.0));
}

}  // namespace astas::environment::map
