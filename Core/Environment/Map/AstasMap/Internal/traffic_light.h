/*******************************************************************************
 * Copyright (c) 2018-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2018-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_TRAFFICLIGHT_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_TRAFFICLIGHT_H

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/pose.h>

#include <cstdint>
#include <map>
#include <vector>

namespace astas::environment::map
{
enum class OsiTrafficLightColor
{
    kUnknown = 0,  // osi.TrafficLight.Classification.COLOR_UNKNOWN
    kOther = 1,    // osi.TrafficLight.Classification.COLOR_OTHER
    kRed = 2,      // osi.TrafficLight.Classification.COLOR_RED
    kYellow = 3,   // osi.TrafficLight.Classification.COLOR_YELLOW
    kGreen = 4,    // osi.TrafficLight.Classification.COLOR_GREEN
    kBlue = 5,     // osi.TrafficLight.Classification.COLOR_BLUE
    kWhite = 6     // osi.TrafficLight.Classification.COLOR_WHITE
};

enum class OsiTrafficLightIcon
{
    kUnknown = 0,                  // osi.TrafficLight.Classification.ICON_UNKNOWN
    kOther = 1,                    // osi.TrafficLight.Classification.ICON_OTHER
    kNone = 2,                     // osi.TrafficLight.Classification.ICON_NONE
    kArrowStraightAhead = 3,       // osi.TrafficLight.Classification.ICON_ARROW_STRAIGHT_AHEAD
    kArrowLeft = 4,                // osi.TrafficLight.Classification.ICON_ARROW_LEFT
    kArrowDiagonalLeft = 5,        // osi.TrafficLight.Classification.ICON_ARROW_DIAG_LEFT
    kArrowStraightAheadLeft = 6,   // osi.TrafficLight.Classification.ICON_ARROW_STRAIGHT_AHEAD_LEFT
    kArrowRight = 7,               // osi.TrafficLight.Classification.ICON_ARROW_RIGHT
    kArrowDiagonalRight = 8,       // osi.TrafficLight.Classification.ICON_ARROW_DIAG_RIGHT
    kArrowStraightAheadRight = 9,  // osi.TrafficLight.Classification.ICON_ARROW_STRAIGHT_AHEAD_RIGHT
    kArrowLeftRight = 10,          // osi.TrafficLight.Classification.ICON_ARROW_LEFT_RIGHT
    kArrowDown = 11,               // osi.TrafficLight.Classification.ICON_ARROW_DOWN
    kArrowDownLeft = 12,           // osi.TrafficLight.Classification.ICON_ARROW_DOWN_LEFT
    kArrowDownRight = 13,          // osi.TrafficLight.Classification.ICON_ARROW_DOWN_RIGHT
    kArrowCross = 14,              // osi.TrafficLight.Classification.ICON_ARROW_CROSS
    kPedestrian = 15,              // osi.TrafficLight.Classification.ICON_PEDESTRIAN
    kWalk = 16,                    // osi.TrafficLight.Classification.ICON_WALK
    kDontWalk = 17,                // osi.TrafficLight.Classification.ICON_DONT_WALK
    kBicycle = 18,                 // osi.TrafficLight.Classification.ICON_BICYCLE
    kPedestrianAndBicycle = 19,    // osi.TrafficLight.Classification.ICON_PEDESTRIAN_AND_BICYCLE
    kCountdownSeconds = 20,        // osi.TrafficLight.Classification.ICON_COUNTDOWN_SECONDS
    kCountdownPercent = 21,        // osi.TrafficLight.Classification.ICON_COUNTDOWN_PERCENT
    kTram = 22,                    // osi.TrafficLight.Classification.ICON_TRAM
    kBus = 23,                     // osi.TrafficLight.Classification.ICON_BUS
    kBusAndTram = 24               // osi.TrafficLight.Classification.ICON_BUS_AND_TRAM
};

enum class OsiTrafficLightMode
{
    kUnknown = 0,   // osi.TrafficLight.Classification.MODE_UNKNOWN
    kOther = 1,     // osi.TrafficLight.Classification.MODE_OTHER
    kOff = 2,       // osi.TrafficLight.Classification.MODE_OFF
    kConstant = 3,  // osi.TrafficLight.Classification.MODE_CONSTANT
    kFlashing = 4,  // osi.TrafficLight.Classification.MODE_FLASHING
    kCounting = 5   // osi.TrafficLight.Classification.MODE_COUNTING
};

struct TrafficLightBulb
{
    mantle_api::UniqueId id{0};
    mantle_api::Pose pose;
    mantle_api::Dimension3 dimensions;

    OsiTrafficLightColor color{OsiTrafficLightColor::kUnknown};
    OsiTrafficLightIcon icon{OsiTrafficLightIcon::kUnknown};
    OsiTrafficLightMode mode{OsiTrafficLightMode::kUnknown};
    double count{0};
    std::vector<mantle_api::UniqueId> assigned_lanes{};
};

using TrafficLightBulbStates = std::map<OsiTrafficLightColor, OsiTrafficLightMode>;

struct TrafficLight
{
    mantle_api::UniqueId id{0};
    std::vector<TrafficLightBulb> light_bulbs{};

    void SetBulbStates(const TrafficLightBulbStates& bulb_states);
};
using TrafficLights = std::vector<TrafficLight>;

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_INTERNAL_TRAFFICLIGHT_H
