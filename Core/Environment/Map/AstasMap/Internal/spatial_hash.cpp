/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/Internal/spatial_hash.h"

#include "Core/Environment/Map/Geometry/geometry.h"
#include "Core/Environment/Map/Geometry/point_in_polygon.h"
#include "Core/Environment/Map/Geometry/point_on_polygon_boundary.h"
#include "Core/Environment/Map/Geometry/point_utils.h"
#include "Core/Service/Logging/logging.h"
#include "Core/Service/Profiling/profiling.h"

namespace astas::environment::map
{

using units::literals::operator""_m;

namespace
{

inline bool HashTableContainsCell(const SpatialHash::HashTable& hash_table, const SpatialHash::CellId& cell_id)
{
    return hash_table.find(cell_id) != hash_table.end();
}

inline bool IsFirstOrLastCenterLinePoint(const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line,
                                         const mantle_api::Vec3<units::length::meter_t>& position)
{
    return center_line.front() == position || center_line.back() == position;
}

inline bool IsPointCloseToFirstOrLastCenterLinePoint(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& center_line,
    const glm::dvec2& position)
{
    // see issue #8879723 - comment-44207136 for reasoning of
    // the maximal expected distance we could encounter
    const units::length::meter_t max_deviation{0.005};

    const mantle_api::Vec3<units::length::meter_t> center_line_front{center_line.front()};
    const glm::dvec2 center_line_front_2d{center_line_front.x, center_line_front.y};

    const mantle_api::Vec3<units::length::meter_t> center_line_back{center_line.back()};
    const glm::dvec2 center_line_back_2d{center_line_back.x, center_line_back.y};

    return ArePointsWithinDistance(center_line_front_2d, position, max_deviation) ||
           ArePointsWithinDistance(center_line_back_2d, position, max_deviation);
}

}  // namespace

SpatialHash::SpatialHash(const BoundingBox2d& world_bounding_box, double cell_size)
    : cell_size_{cell_size},
      num_cells_x_{ComputeNumberOfCells(world_bounding_box.top_right.x, world_bounding_box.bottom_left.x)},
      num_cells_y_{ComputeNumberOfCells(world_bounding_box.top_right.y, world_bounding_box.bottom_left.y)},
      number_of_cells_{num_cells_x_ * num_cells_y_},
      width_{num_cells_x_ * cell_size_},
      height_{num_cells_y_ * cell_size_},
      world_space_offset_{world_bounding_box.bottom_left}
{
}

SpatialHash::CellId SpatialHash::ComputeNumberOfCells(double a, double b) const
{
    const auto distance = static_cast<CellId>(std::ceil(a - b)) + 1;
    auto num_cells = static_cast<CellId>(distance / cell_size_);
    if (distance % static_cast<CellId>(cell_size_) > 0)
    {
        num_cells++;
    }
    return num_cells;
}

void SpatialHash::InsertLanes(const Lanes& lanes)
{
    ASTAS_PROFILE_SCOPE

    for (const auto& lane : lanes)
    {
        auto cell_candidates = GetCellsIntersectingBoundingBox(lane.axis_aligned_bounding_box);
        for (const auto& cell : cell_candidates)
        {
            if (ShouldAssignLaneToCell(lane, cell))
            {
                const CellElement cell_element{&lane};
                AddLaneToCell(cell_element, cell);
            }
        }
    }
}

SpatialHash::CellIds SpatialHash::GetCellsIntersectingBoundingBox(const BoundingBox2d& bounding_box) const
{
    const CellId bottom_left_cell_id = Hash({units::length::meter_t(bounding_box.bottom_left.x),
                                             units::length::meter_t(bounding_box.bottom_left.y),
                                             0.0_m});
    const CellId top_right_cell_id = Hash(
        {units::length::meter_t(bounding_box.top_right.x), units::length::meter_t(bounding_box.top_right.y), 0.0_m});

    ASSERT(IsValidCellId(bottom_left_cell_id) && "SpatialHash: lane's bounding box out of hash boundaries.")
    ASSERT(IsValidCellId(top_right_cell_id) && "SpatialHash: lane's bounding box out of hash boundaries.")

    const CellId bottom_left_row = CellIdToGridsRowIndex(bottom_left_cell_id);
    const CellId bottom_left_column = CellIdToGridsColumnIndex(bottom_left_cell_id);
    const CellId top_right_row = CellIdToGridsRowIndex(top_right_cell_id);
    const CellId top_right_column = CellIdToGridsColumnIndex(top_right_cell_id);

    CellIds ids;

    for (CellId i = bottom_left_row; i <= top_right_row; ++i)
    {
        for (CellId j = bottom_left_column; j <= top_right_column; ++j)
        {
            const CellId candidate_cell_id = GridIndicesToCellId(i, j);
            ids.emplace_back(candidate_cell_id);
        }
    }

    return ids;
}

bool SpatialHash::ShouldAssignLaneToCell(const Lane& lane, CellId cell_id) const
{
    // @todo: || CompleteCellContainedInLaneShape - edge case which should never happen
    return IsAnyPointInsideCell(lane.shape_2d.points, cell_id) || IsLaneShapeIntersectingCell(lane.shape_2d, cell_id);
}

bool SpatialHash::IsAnyPointInsideCell(const std::vector<glm::dvec2>& points, CellId cell_id) const
{
    ASTAS_PROFILE_SCOPE

    auto is_point_in_cell = [this, cell_id](const glm::dvec2& point) {
        const CellId& point_cell_id = Hash({units::length::meter_t(point.x), units::length::meter_t(point.y), 0_m});

        ASSERT(IsValidCellId(point_cell_id) && "SpatialHash: lane out of hash boundaries.")

        return point_cell_id == cell_id;
    };
    return std::any_of(points.cbegin(), points.cend(), is_point_in_cell);
}

bool SpatialHash::IsLaneShapeIntersectingCell(const Polygon2d& shape, CellId cell_id) const
{
    ASTAS_PROFILE_SCOPE

    BoundingBox2d cell_bounding_box = GetCellBoundingBox(cell_id);
    return IsPolygonIntersectingBoundingBox(cell_bounding_box, shape);
}

BoundingBox2d SpatialHash::GetCellBoundingBox(const CellId cell_id) const
{
    const CellId column = CellIdToGridsColumnIndex(cell_id);
    const CellId row = CellIdToGridsRowIndex(cell_id);

    const glm::dvec2 bottom_left = world_space_offset_ + glm::dvec2{row * cell_size_, column * cell_size_};
    const glm::dvec2 top_right = world_space_offset_ + glm::dvec2{(row + 1) * cell_size_, (column + 1) * cell_size_};

    return {bottom_left, top_right};
}

void SpatialHash::AddLaneToCell(const CellElement& cell_element, const CellId& cell_id)
{
    hash_table_[cell_id].emplace_back(cell_element);
}

SpatialHash::CellId SpatialHash::Hash(const mantle_api::Vec3<units::length::meter_t>& world_position) const
{
    const auto offset_position = glm::dvec2(world_position.x, world_position.y) - world_space_offset_;

    const auto x = static_cast<CellId>(std::floor(offset_position.x / cell_size_));
    const auto y = static_cast<CellId>(std::floor(offset_position.y / cell_size_));

    CellId hash_id = GridIndicesToCellId(x, y);

    if (hash_id < 0 || hash_id > number_of_cells_)
    {
        return InvalidCellId();
    }

    return hash_id;
}

size_t SpatialHash::Size() const
{
    std::size_t lane_count{0};
    for (const auto& it : hash_table_)
    {
        lane_count += it.second.size();
    }
    return lane_count;
}

bool SpatialHash::IsPositionAssociatedWithLane(const Lane* lane,
                                               const mantle_api::Vec3<units::length::meter_t>& position) const
{
    const glm::dvec2 position_2d{position.x, position.y};

    if (map::PointInPolygon(lane->shape_2d.points, position_2d))
    {
        return true;
    }

    if (IsFirstOrLastCenterLinePoint(lane->center_line, position))
    {
        return true;
    }

    if (map::PointOnPolygonBoundary(lane->shape_2d.points, position_2d))
    {
        return true;
    }

    if (IsPointCloseToFirstOrLastCenterLinePoint(lane->center_line, position_2d))
    {
        return true;
    }

    return false;
}

std::vector<const Lane*> SpatialHash::GetLanes(const mantle_api::Vec3<units::length::meter_t>& world_position) const
{
    ASTAS_PROFILE_SCOPE

    const auto cell_id = Hash(world_position);

    if (!IsValidCellId(cell_id))
    {
        return {};
    }

    if (!HashTableContainsCell(hash_table_, cell_id))
    {
        return {};
    }

    std::vector<const Lane*> found_lanes;

    for (const auto* lane : hash_table_.at(cell_id))
    {
        if (IsPositionAssociatedWithLane(lane, world_position))
        {
            found_lanes.emplace_back(lane);
        }
    }

    return found_lanes;
}

SpatialHash::CellId SpatialHash::CellIdToGridsColumnIndex(const SpatialHash::CellId& cell_id) const
{
    return cell_id / num_cells_x_;
}

SpatialHash::CellId SpatialHash::CellIdToGridsRowIndex(const SpatialHash::CellId& cell_id) const
{
    return cell_id % num_cells_x_;
}

SpatialHash::CellId SpatialHash::GridIndicesToCellId(const CellId& row, const CellId& column) const
{
    return column * num_cells_x_ + row;
}

}  // namespace astas::environment::map
