/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/Internal/lane_shape_extractor.h"

#include "Core/Environment/Map/AstasMap/Internal/lane_utility.h"
#include "Core/Environment/Map/AstasMap/lane.h"
#include "Core/Environment/Map/AstasMap/lane_boundary.h"
#include "Core/Environment/Map/Geometry/point_in_polygon.h"
#include "Core/Environment/Map/Geometry/point_on_polygon_boundary.h"
#include "Core/Service/Logging/logging.h"

#include <glm/glm.hpp>

namespace astas::environment::map
{

using Chain = std::vector<glm::dvec2>;
using Chains = std::vector<Chain>;

bool IsDoubleLaneBoundary(const Chain& chain1, const Chain& chain2)
{
    double dist1 = glm::distance(chain1.front(), chain2.front());
    bool first_points_equal = mantle_api::AlmostEqual(dist1, 0.0, 0.0001, true);
    double dist2 = glm::distance(chain1.back(), chain2.back());
    bool last_points_equal = mantle_api::AlmostEqual(dist2, 0.0, 0.0001, true);

    return first_points_equal && last_points_equal;
}

bool IsDoubleLaneBoundarySwapped(const Chain& chain1, const Chain& chain2)
{
    double dist1 = glm::distance(chain1.front(), chain2.back());
    bool first_points_equal = mantle_api::AlmostEqual(dist1, 0.0, 0.0001, true);
    double dist2 = glm::distance(chain1.back(), chain2.front());
    bool last_points_equal = mantle_api::AlmostEqual(dist2, 0.0, 1.1001, true);

    return first_points_equal && last_points_equal;
}

bool IsIgnoredBoundary(const LaneBoundary& boundary)
{
    return boundary.type == LaneBoundary::Type::kGuardRail || boundary.type == LaneBoundary::Type::kBarrier;
}

Chain BoundaryToChain(const LaneBoundary& boundary)
{
    Chain chain;
    for (const auto& point : boundary.points)
    {
        chain.push_back({point.position.x, point.position.y});
    }
    return chain;
}

bool ShallAppendToChain(Chain& current_chain, Chain& chain)
{
    return mantle_api::AlmostEqual(
        glm::distance(chain.back(), current_chain.front()), 0.0, MANTLE_API_DEFAULT_EPS, true);
}

bool ShallPrependToChain(Chain& current_chain, Chain& chain)
{
    return mantle_api::AlmostEqual(
        glm::distance(chain.front(), current_chain.back()), 0.0, MANTLE_API_DEFAULT_EPS, true);
}

bool IsAddedToExistingChain(Chains& chains, Chain& current_chain)
{
    for (auto& chain : chains)
    {
        if (IsDoubleLaneBoundary(chain, current_chain) || IsDoubleLaneBoundarySwapped(chain, current_chain))
        {
            TRACE("Ignore double lane boundary")
            return true;
        }

        if (ShallAppendToChain(current_chain, chain))
        {
            chain.insert(chain.end(), current_chain.begin() + 1, current_chain.end());
            return true;
        }
        if (ShallPrependToChain(current_chain, chain))
        {
            current_chain.insert(current_chain.end(), chain.begin() + 1, chain.end());
            chain = current_chain;
            return true;
        }
    }
    return false;
}

Chains GetBoundaryChains(const std::vector<const LaneBoundary*>& lane_boundaries)
{
    Chains chains;
    for (const auto* boundary : lane_boundaries)
    {
        if (IsIgnoredBoundary(*boundary))
        {
            continue;
        }

        Chain current_chain = BoundaryToChain(*boundary);

        if (!IsAddedToExistingChain(chains, current_chain))
        {
            chains.push_back(current_chain);
        }
    }

    return chains;
}

void ReverseChainIfNeeded(const Chain& left_chain, Chain& right_chain)
{
    const auto& last_left_point = left_chain.back();
    const auto& first_right_point = right_chain.front();
    const auto& last_right_point = right_chain.back();

    double dist_to_first = glm::distance(last_left_point, first_right_point);
    double dist_to_last = glm::distance(last_left_point, last_right_point);

    if (dist_to_last < dist_to_first)
    {
        std::reverse(std::begin(right_chain), std::end(right_chain));
    }
}

Polygon2d CreateShape(const Chain& left_chain, Chain& right_chain)
{
    Polygon2d shape;

    ReverseChainIfNeeded(left_chain, right_chain);

    for (const auto& point : right_chain)
    {
        shape.points.emplace_back(point);
    }

    for (const auto& point : left_chain)
    {
        shape.points.emplace_back(point);
    }

    return shape;
}

int IsPointOutsideOfPolygon(const Chain& shape, const glm::dvec2& chain_point)
{
    if (map::PointInPolygon(shape, chain_point) || map::PointOnPolygonBoundary(shape, chain_point))
    {
        return 0;
    }
    return 1;
}

int CountPointsOutsideShape(const Chain& shape, const Chain& chain_points)
{
    // Do not check all the points, 3 samples (begin, end, middle) are enough
    int number_of_outside_points{0};
    number_of_outside_points += IsPointOutsideOfPolygon(shape, chain_points.front());
    number_of_outside_points += IsPointOutsideOfPolygon(shape, chain_points.back());
    if (chain_points.size() > 2)
    {
        number_of_outside_points += IsPointOutsideOfPolygon(shape, chain_points.at(chain_points.size() / 2));
    }
    return number_of_outside_points;
}

int AllOtherChainsAreContainedInShape(const Polygon2d& shape, const Chains& chains, std::size_t chain_idx)
{
    int points_outside_shape{0};
    for (std::size_t i = 0; i < chains.size(); ++i)
    {
        if (i != chain_idx)
        {
            points_outside_shape += CountPointsOutsideShape(shape.points, chains.at(i));
        }
    }
    return points_outside_shape;
}

Polygon2d GetBestLaneShape(const Chains& left_chains, Chains& right_chains)
{
    Polygon2d best_shape_candidate;
    int minimal_points_outside_shape{std::numeric_limits<int>::max()};

    for (std::size_t left_idx = 0; left_idx < left_chains.size(); ++left_idx)
    {
        for (std::size_t right_idx = 0; right_idx < right_chains.size(); ++right_idx)
        {
            auto shape_candidate = CreateShape(left_chains.at(left_idx), right_chains.at(right_idx));
            int points_outside_shape = AllOtherChainsAreContainedInShape(shape_candidate, right_chains, right_idx);
            points_outside_shape += AllOtherChainsAreContainedInShape(shape_candidate, left_chains, left_idx);

            if (points_outside_shape == 0)
            {
                TRACE("Found shape fits all points")
                return shape_candidate;
            }

            if (points_outside_shape < minimal_points_outside_shape)
            {
                minimal_points_outside_shape = points_outside_shape;
                best_shape_candidate = shape_candidate;
                TRACE("Found a shape candidate with {} points outside", minimal_points_outside_shape)
            }
        }
    }
    return best_shape_candidate;
}

void ExtractLaneShape(Lane& astas_lane,
                      const std::vector<const LaneBoundary*>& left_lane_boundaries,
                      const std::vector<const LaneBoundary*>& right_lane_boundaries)
{
    TRACE(">>> Starting shape extraction for lane {}", astas_lane.id)

    auto left_chains = GetBoundaryChains(left_lane_boundaries);
    auto right_chains = GetBoundaryChains(right_lane_boundaries);

    TRACE("Number of left chains: {}", left_chains.size())
    TRACE("Number of right chains: {}", right_chains.size())

    Polygon2d shape = GetBestLaneShape(left_chains, right_chains);

    astas_lane.shape_2d.points.clear();
    AddPointsToLanesPolygonAndUpdateBoundingBox(astas_lane, shape.points);
}

}  // namespace astas::environment::map
