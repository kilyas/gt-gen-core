/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/Internal/traffic_light.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{

class TrafficLightTest : public testing::Test
{
  protected:
    static std::vector<TrafficLightBulb> GetDefaultBulbs()
    {
        TrafficLightBulb red_bulb{};
        red_bulb.color = OsiTrafficLightColor::kRed;
        red_bulb.mode = OsiTrafficLightMode::kOff;

        TrafficLightBulb yellow_bulb{};
        yellow_bulb.color = OsiTrafficLightColor::kYellow;
        yellow_bulb.mode = OsiTrafficLightMode::kOff;

        return {red_bulb, yellow_bulb};
    }

    static void CheckTrafficLightBulbStates(const TrafficLightBulbStates& expected_bulb_states,
                                            const std::vector<TrafficLightBulb>& actual_bulbs)
    {
        for (const auto& actual_bulb : actual_bulbs)
        {
            EXPECT_EQ(expected_bulb_states.at(actual_bulb.color), actual_bulb.mode);
        }
    }

    const mantle_api::UniqueId traffic_light_id{13};
    TrafficLight default_traffic_light_{traffic_light_id, GetDefaultBulbs()};
};

TEST_F(
    TrafficLightTest,
    GivenBulbsAreOff_WhenSettingRedToConstantAndYellowToFlashing_ThenRedBulbModeIsConstantAndYellowBulbModeIsFlashing)
{
    const TrafficLightBulbStates expected_bulb_states{{OsiTrafficLightColor::kRed, OsiTrafficLightMode::kConstant},
                                                      {OsiTrafficLightColor::kYellow, OsiTrafficLightMode::kFlashing}};

    default_traffic_light_.SetBulbStates(expected_bulb_states);

    CheckTrafficLightBulbStates(expected_bulb_states, default_traffic_light_.light_bulbs);
}

TEST_F(TrafficLightTest, GivenTrafficLightBulbStatesWithMissingModeForBulb_WhenSettingBulbStates_ThenDeath)
{
    const TrafficLightBulbStates bulb_states{{OsiTrafficLightColor::kRed, OsiTrafficLightMode::kConstant}};

    EXPECT_DEATH(default_traffic_light_.SetBulbStates(bulb_states), ".*");
}

}  // namespace astas::environment::map
