/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/Internal/lane_flags.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{
TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenInitializing_ThenNoFlagsAreSet)
{
    LaneFlags flags;

    EXPECT_FALSE(flags.IsShoulderLane());
    EXPECT_FALSE(flags.IsDrivable());
    EXPECT_FALSE(flags.IsMergeLane());
    EXPECT_FALSE(flags.IsSplitLane());
    EXPECT_FALSE(flags.IsNormalLane());
    EXPECT_FALSE(flags.IsEntryLane());
    EXPECT_FALSE(flags.IsExitLane());
    EXPECT_FALSE(flags.IsContinue());
    EXPECT_FALSE(flags.IsParking());
    EXPECT_FALSE(flags.IsBicycle());
    EXPECT_FALSE(flags.IsBus());
    EXPECT_FALSE(flags.IsFullyAttributed());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingShoulder_ThenShoulderIsSet)
{
    LaneFlags flags;
    flags.SetShoulderLane();

    EXPECT_TRUE(flags.IsShoulderLane());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingDrivable_ThenDrivableIsSet)
{
    LaneFlags flags;
    flags.SetDrivable();

    EXPECT_TRUE(flags.IsDrivable());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingMergeLane_ThenMergeLaneIsSet)
{
    LaneFlags flags;
    flags.SetMergeLane();

    EXPECT_TRUE(flags.IsMergeLane());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingSplitLane_ThenSplitLaneIsSet)
{
    LaneFlags flags;
    flags.SetSplitLane();

    EXPECT_TRUE(flags.IsSplitLane());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingNormalLane_ThenNormalLaneIsSet)
{
    LaneFlags flags;
    flags.SetNormalLane();

    EXPECT_TRUE(flags.IsNormalLane());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingEntryLane_ThenEntryLaneIsSet)
{
    LaneFlags flags;
    flags.SetEntryLane();

    EXPECT_TRUE(flags.IsEntryLane());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingExitLane_ThenExitLaneIsSet)
{
    LaneFlags flags;
    flags.SetExitLane();

    EXPECT_TRUE(flags.IsExitLane());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingContinue_ThenContinueIsSet)
{
    LaneFlags flags;
    flags.SetContinue();

    EXPECT_TRUE(flags.IsContinue());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingParking_ThenParkingIsSet)
{
    LaneFlags flags;
    flags.SetParking();

    EXPECT_TRUE(flags.IsParking());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingBicycle_ThenBicycleIsSet)
{
    LaneFlags flags;
    flags.SetBicycle();

    EXPECT_TRUE(flags.IsBicycle());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingBus_ThenBusIsSet)
{
    LaneFlags flags;
    flags.SetBus();

    EXPECT_TRUE(flags.IsBus());
}

TEST(LaneFlagsTest, GivenLaneFlagsObject_WhenSettingFullyAttributed_ThenFullyAttributedIsSet)
{
    LaneFlags flags;
    flags.SetFullyAttributed();

    EXPECT_TRUE(flags.IsFullyAttributed());
}

}  // namespace astas::environment::map
