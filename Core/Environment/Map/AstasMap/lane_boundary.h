/*******************************************************************************
 * Copyright (c) 2018-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2018-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANEBOUNDARY_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANEBOUNDARY_H

#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <vector>

namespace astas::environment::map
{

struct LaneBoundary
{
    /// @brief Represents a point of the boundary
    struct Point
    {
        mantle_api::Vec3<units::length::meter_t> position{};
        double width{0.0};
        double height{0.0};

        bool operator==(const Point& rhs) const
        {
            return position == rhs.position &&
                   mantle_api::AlmostEqual(width, rhs.width, MANTLE_API_DEFAULT_EPS, true) &&
                   mantle_api::AlmostEqual(height, rhs.height, MANTLE_API_DEFAULT_EPS, true);
        }
    };
    using Points = std::vector<Point>;

    /// @brief Supported boundary colors
    enum class Color
    {
        kUnknown,  // OSI-Colors
        kOther,
        kNone,
        kWhite,
        kYellow,
        kRed,
        kBlue,
        kGreen,
        kViolet,     // this one is supported by OSI, but neither xodr nor nds have this color defined
        kLightGray,  // additional NDS colors
        kGray,
        kDarkGray,
        kBlack,
        kCyan,
        kOrange,
        kStandard  // additional xodr color
    };

    /// @brief Supported boundary types
    enum class Type
    {
        kUnknown = 0,  // OSI Types
        kOther,
        kNoLine,
        kSolidLine,
        kDashedLine,
        kBottsDots,
        kRoadEdge,
        kSnowEdge,
        kGrassEdge,
        kGravelEdge,
        kSoilEdge,
        kGuardRail,
        kCurb,
        kStructure,
        kBarrier  // non-standard extension for OpenDrive and NDS
    };

    LaneBoundary(mantle_api::UniqueId id, Type type, Color color, const Points& points = {})
        : id{id}, type{type}, color{color}, points{points}
    {
    }

    /// @section Lane boundary attributes
    mantle_api::UniqueId id;
    Type type;
    Color color;
    Points points;

    /// @brief Convenience e.g. to prevent double rendering
    mantle_api::UniqueId mirrored_from{mantle_api::InvalidId};

    mantle_api::UniqueId parent_lane_group_id{mantle_api::InvalidId};
};

using LaneBoundaries = std::vector<LaneBoundary>;

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANEBOUNDARY_H
