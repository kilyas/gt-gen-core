/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/utility.h"

#include "Core/Environment/Map/AstasMap/lane_boundary.h"
#include "Core/Environment/Map/Geometry/project_query_point_on_polyline.h"
#include "Core/Environment/Map/Geometry/scalar_projection_2d.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"

#include <algorithm>

namespace astas::environment::map
{
/// @brief Returns only unique points from vector of points
/// @param points vector of points
/// @return Vector of unique points
static std::vector<mantle_api::Vec3<units::length::meter_t>> GetPointsWithoutAdjacentDuplicates(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& points);

std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>> GetPolylinePointsExtremes(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline_points)
{
    const auto [min_x, max_x] =
        std::minmax_element(std::begin(polyline_points),
                            std::end(polyline_points),
                            [](const mantle_api::Vec3<units::length::meter_t>& c1,
                               const mantle_api::Vec3<units::length::meter_t>& c2) { return c1.x < c2.x; });

    const auto [min_y, max_y] =
        std::minmax_element(std::begin(polyline_points),
                            std::end(polyline_points),
                            [](const mantle_api::Vec3<units::length::meter_t>& c1,
                               const mantle_api::Vec3<units::length::meter_t>& c2) { return c1.y < c2.y; });

    const auto [min_z, max_z] =
        std::minmax_element(std::begin(polyline_points),
                            std::end(polyline_points),
                            [](const mantle_api::Vec3<units::length::meter_t>& c1,
                               const mantle_api::Vec3<units::length::meter_t>& c2) { return c1.z < c2.z; });

    return {{min_x->x, min_y->y, min_z->z}, {max_x->x, max_y->y, max_z->z}};
}

mantle_api::Vec3<units::length::meter_t> ClosestPointOnLineSegment(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const mantle_api::Vec3<units::length::meter_t>& start_3d,
    const mantle_api::Vec3<units::length::meter_t>& end_3d)
{
    const auto projection_ratio = ScalarProjection2d(query_point, start_3d, end_3d);

    if (projection_ratio.has_value())
    {
        const double t = glm::clamp(projection_ratio.value(), 0.0, 1.0);

        const mantle_api::Vec3<units::length::meter_t> projection = start_3d + t * (end_3d - start_3d);

        return projection;
    }

    return start_3d;
}

mantle_api::Vec3<units::length::meter_t> ClosestPointOnBoundary(
    const mantle_api::Vec3<units::length::meter_t>& query_point,
    const map::LaneBoundary& boundary)
{
    double min_distance = std::numeric_limits<double>::max();
    mantle_api::Vec3<units::length::meter_t> closest_point{};

    for (std::size_t i = 1; i < boundary.points.size(); ++i)
    {
        const auto p0 = boundary.points[i - 1].position;
        const auto p1 = boundary.points[i].position;

        const auto closest_point_on_segment = ClosestPointOnLineSegment(query_point, p0, p1);

        const auto distance = service::glmwrapper::Distance2(query_point, closest_point_on_segment);

        if (distance < min_distance)
        {
            min_distance = distance;
            closest_point = closest_point_on_segment;
        }
    }

    return closest_point;
}

void SetZValueForPoints(const double z_value, std::vector<mantle_api::Vec3<units::length::meter_t>>& points)
{
    for (auto& point : points)
    {
        point.z = units::length::meter_t(z_value);
    }
}

std::vector<mantle_api::Vec3<units::length::meter_t>> ConnectPolylinesCounterclockwise(
    std::vector<mantle_api::Vec3<units::length::meter_t>> right_polyline,
    std::vector<mantle_api::Vec3<units::length::meter_t>> left_polyline)
{
    std::vector<mantle_api::Vec3<units::length::meter_t>> connected_polylines;
    connected_polylines.reserve(right_polyline.size() + left_polyline.size());
    std::move(right_polyline.begin(), right_polyline.end(), std::back_inserter(connected_polylines));
    std::move(left_polyline.rbegin(), left_polyline.rend(), std::back_inserter(connected_polylines));

    return connected_polylines;
}

double CalculateVectorYaw(const mantle_api::Vec3<units::length::meter_t>& start_point,
                          const mantle_api::Vec3<units::length::meter_t>& end_point)
{
    return units::math::atan2((end_point.y - start_point.y), (end_point.x - start_point.x))();
}

std::vector<mantle_api::Vec3<units::length::meter_t>> TrimPolylineToRange(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& polyline,
    const mantle_api::Vec3<units::length::meter_t>& start_point,
    const mantle_api::Vec3<units::length::meter_t>& end_point)
{
    if (polyline.empty())
    {
        return {};
    }

    const auto start_point_projection = ProjectQueryPointOnPolyline(start_point, polyline);
    const auto projected_start_point = std::get<0>(start_point_projection);
    const auto start_point_index = std::get<1>(start_point_projection);
    const auto end_point_projection = ProjectQueryPointOnPolyline(end_point, polyline);
    const auto projected_end_point = std::get<0>(end_point_projection);
    const auto end_point_index = std::get<1>(end_point_projection);

    if (start_point_index > end_point_index)
    {
        return {};
    }

    std::vector<mantle_api::Vec3<units::length::meter_t>> boundaries;
    boundaries.emplace_back(projected_start_point);
    auto begin_index_it{polyline.begin()};
    auto end_index_it{polyline.begin()};
    std::advance(begin_index_it, start_point_index + 1);
    std::advance(end_index_it, end_point_index + 1);
    std::move(begin_index_it, end_index_it, std::back_inserter(boundaries));
    boundaries.emplace_back(projected_end_point);

    return GetPointsWithoutAdjacentDuplicates(boundaries);
}

std::vector<mantle_api::Vec3<units::length::meter_t>> GetPointsWithoutAdjacentDuplicates(
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& points)
{
    if (points.empty())
    {
        return {};
    }

    std::vector<mantle_api::Vec3<units::length::meter_t>> non_repeated_points{points[0]};

    for (const auto& current_point : points)
    {
        const auto& last_point = non_repeated_points.back();
        // const bool is_consecutive_point_repeated = service::types::AreCoordinateVectorsEqual(current_point,
        // last_point);

        if (current_point != last_point)
        {
            non_repeated_points.emplace_back(current_point);
        }
    }
    return non_repeated_points;
}

mantle_api::Dimension3 CalculateObjectDimensions(
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>& shape_extremes)
{
    const double height = shape_extremes.second.z() - shape_extremes.first.z();
    const auto dimensions = CalculateObjectDimensions(shape_extremes, height);

    return dimensions;
}

mantle_api::Dimension3 CalculateObjectDimensions(
    const std::pair<mantle_api::Vec3<units::length::meter_t>, mantle_api::Vec3<units::length::meter_t>>& shape_extremes,
    const double height)
{
    mantle_api::Dimension3 dimensions;
    dimensions.length = shape_extremes.second.x - shape_extremes.first.x;
    dimensions.width = shape_extremes.second.y - shape_extremes.first.y;
    dimensions.height = units::length::meter_t(height);

    return dimensions;
}

}  // namespace astas::environment::map
