/*******************************************************************************
 * Copyright (c) 2018-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2018-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANEGROUP_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANEGROUP_H

#include <MantleAPI/Common/i_identifiable.h>
#include <fmt/format.h>

#include <vector>

namespace astas::environment::map
{

struct LaneGroup
{
    enum class Type
    {
        kUnknown = 0,
        kOther,
        kOneWay,
        kJunction
    };

    LaneGroup(mantle_api::UniqueId id, LaneGroup::Type type) : id{id}, type{type} {}

    /// @section Lane group attributes
    mantle_api::UniqueId id;
    Type type;
    std::vector<mantle_api::UniqueId> lane_boundary_ids{};
    std::vector<mantle_api::UniqueId> lane_ids{};
};

using LaneGroups = std::vector<LaneGroup>;

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANEGROUP_H
