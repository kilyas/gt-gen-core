/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/astas_map_finalizer.h"

#include "Core/Environment/Map/AstasMap/Internal/lane_shape_extractor.h"
#include "Core/Environment/Map/AstasMap/Internal/spatial_hash.h"
#include "Core/Environment/Map/Geometry/bounding_box_utils.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Service/Utility/algorithm_utils.h"

namespace astas::environment::map
{
AstasMapFinalizer::AstasMapFinalizer(AstasMap& astas_map) : astas_map_{astas_map} {}

void AstasMapFinalizer::Finalize()
{
    RemoveDuplicatedSuccessorAndPredecessors();
    FixBrokenLaneConnectionsInTileCrossing();
    ExtractLaneShapes();
    InitWorldBoundingBox();
    BuildSpatialHash();
}

void AstasMapFinalizer::RemoveDuplicatedSuccessorAndPredecessors()
{
    auto fix_successor_and_predecessor_ids = [](Lane& lane) {
        auto remove_duplicate_ids = [](std::vector<mantle_api::UniqueId>& lane_ids) {
            lane_ids.erase(std::unique(lane_ids.begin(), lane_ids.end()), lane_ids.end());
        };

        remove_duplicate_ids(lane.successors);
        remove_duplicate_ids(lane.predecessors);
    };
    astas_map_.DoForAllLanes(fix_successor_and_predecessor_ids);
}

void AstasMapFinalizer::ExtractLaneShapes()
{
    auto start = std::chrono::system_clock::now();
    Info("Extracting lane shapes");

    auto update_lane_shape = [this](Lane& lane) {
        ExtractLaneShape(lane, astas_map_.GetLeftLaneBoundaries(&lane), astas_map_.GetRightLaneBoundaries(&lane));
    };
    astas_map_.DoForAllLanes(update_lane_shape);

    auto end = std::chrono::system_clock::now();
    auto lane_extraction_time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    Info("Extracting lane shapes took: {}ms", lane_extraction_time);
}

void AstasMapFinalizer::InitWorldBoundingBox()
{
    auto find_bounding_box = [this](Lane& lane) {
        service::utility::RemoveAdjacentDuplicates(lane.shape_2d.points);

        astas_map_.axis_aligned_world_bounding_box =
            MergeBoundingBoxes<glm::dvec2>(lane.axis_aligned_bounding_box, astas_map_.axis_aligned_world_bounding_box);
    };

    astas_map_.DoForAllLanes(find_bounding_box);
}

void AstasMapFinalizer::BuildSpatialHash()
{
    auto start = std::chrono::system_clock::now();
    Info("Building spatial hash");

    auto spatial_hash = std::make_unique<environment::map::SpatialHash>(astas_map_.axis_aligned_world_bounding_box);
    spatial_hash->InsertLanes(astas_map_.GetLanes());
    astas_map_.SetSpatialHash(std::move(spatial_hash));

    auto end = std::chrono::system_clock::now();
    auto spatial_hash_building_time = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    Info("Spatial hash building took: {}ms", spatial_hash_building_time);
}
/// TODO: Remove this quickfix function for broken lane connections over tiles (Fix for:
/// CB-#5909065)
void AstasMapFinalizer::FixBrokenLaneConnectionsInTileCrossing()
{
    auto& all_lanes = astas_map_.GetLanes();

    for (auto& lane : all_lanes)
    {
        if (lane.successors.empty())
        {
            auto last_centerline_point = lane.center_line.back();
            for (auto& other_lane : all_lanes)
            {
                if (other_lane.predecessors.empty())
                {
                    auto first_centerline_point = other_lane.center_line.front();
                    if (service::glmwrapper::Distance(last_centerline_point, first_centerline_point) < 0.1)
                    {
                        Warn("Fix broken successor for lane: {} - add successor lane: {}", lane.id, other_lane.id);
                        Warn("Fix broken predecessor for lane: {} - add predecessor lane: {}", other_lane.id, lane.id);
                        lane.successors.push_back(other_lane.id);
                        other_lane.predecessors.push_back(lane.id);
                    }
                }
            }
        }
    }
}

}  // namespace astas::environment::map
