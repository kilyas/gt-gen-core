/*******************************************************************************
 * Copyright (c) 2018-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2018-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANE_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANE_H

#include "Core/Environment/Map/AstasMap/Internal/lane_flags.h"
#include "Core/Environment/Map/Geometry/bounding_box.h"
#include "Core/Environment/Map/Geometry/polygon.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <vector>

namespace astas::environment::map
{

struct Lane
{
    explicit Lane(mantle_api::UniqueId id) : id{id} {}

    mantle_api::UniqueId id{0};

    std::vector<mantle_api::Vec3<units::length::meter_t>> center_line;

    std::vector<mantle_api::UniqueId> predecessors;
    std::vector<mantle_api::UniqueId> successors;

    std::vector<mantle_api::UniqueId> left_lane_boundaries;
    std::vector<mantle_api::UniqueId> right_lane_boundaries;

    std::vector<mantle_api::UniqueId> left_adjacent_lanes;
    std::vector<mantle_api::UniqueId> right_adjacent_lanes;

    mantle_api::UniqueId parent_lane_group_id{mantle_api::InvalidId};

    Polygon2d shape_2d;
    BoundingBox2d axis_aligned_bounding_box;

    LaneFlags flags{};
};

using Lanes = std::vector<Lane>;

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_LANE_H
