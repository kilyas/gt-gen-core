/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_ASTASMAPFINALIZER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_ASTASMAPFINALIZER_H

#include "Core/Environment/Map/AstasMap/astas_map.h"

namespace astas::environment::map
{
class AstasMapFinalizer
{
  public:
    explicit AstasMapFinalizer(AstasMap& astas_map);

    void Finalize();

  protected:
    void RemoveDuplicatedSuccessorAndPredecessors();
    void ExtractLaneShapes();
    void InitWorldBoundingBox();
    void BuildSpatialHash();
    void FixBrokenLaneConnectionsInTileCrossing();

    AstasMap& astas_map_;
};

}  // namespace astas::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_ASTASMAP_ASTASMAPFINALIZER_H
