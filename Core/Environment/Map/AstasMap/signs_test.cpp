/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/AstasMap/signs.h"

#include <gtest/gtest.h>

namespace astas::environment::map
{
using units::literals::operator""_m;
using units::literals::operator""_rad;

class MountedSignTest : public testing::Test
{
  protected:
    MountedSign mounted_sign_;

    const std::vector<OsiSupplementaryTrafficSignActor> expected_supplementary_sign_actor;
    const std::vector<SignValueInformation> expected_sign_information;
    const mantle_api::Pose expected_sign_pose{{1_m, 2_m, 3_m}, {0.1_rad, 0.2_rad, 0.3_rad}};
    const mantle_api::Dimension3 expected_sign_dimension{3_m, 2_m, 4_m};

    MountedSignTest()
        : expected_supplementary_sign_actor{OsiSupplementaryTrafficSignActor::kNoActor},
          expected_sign_information{{"Kindergarten", 30.0, osi::OsiTrafficSignValueUnit::kKilometerPerHour}}
    {
        mounted_sign_.assigned_lanes.emplace_back(42);
        mounted_sign_.assigned_lanes.emplace_back(43);

        mounted_sign_.supplementary_signs.push_back({&mounted_sign_,
                                                     osi::OsiTrafficSignVariability::kOther,
                                                     expected_supplementary_sign_actor,
                                                     OsiSupplementarySignType::kOther,
                                                     expected_sign_information,
                                                     expected_sign_pose,
                                                     expected_sign_dimension});
    };
};

TEST(SignTest, GivenTrafficSign_WhenLanesAssigned_ThenReturnedLanesWillBeTheSame)
{
    TrafficSign traffic_sign;
    traffic_sign.variability = osi::OsiTrafficSignVariability::kVariable;
    EXPECT_EQ(traffic_sign.variability, osi::OsiTrafficSignVariability::kVariable);

    traffic_sign.direction_scope = osi::OsiTrafficSignDirectionScope::kLeftRight;
    EXPECT_EQ(traffic_sign.direction_scope, osi::OsiTrafficSignDirectionScope::kLeftRight);

    traffic_sign.assigned_lanes.emplace_back(101);
    traffic_sign.assigned_lanes.emplace_back(102);
    traffic_sign.assigned_lanes.emplace_back(103);

    auto lane_ids = traffic_sign.GetAssignedLanes();
    ASSERT_EQ(lane_ids.size(), 3);
    EXPECT_EQ(lane_ids.at(0), 101);
    EXPECT_EQ(lane_ids.at(1), 102);
    EXPECT_EQ(lane_ids.at(2), 103);
}

TEST(SignTest, GivenGroundSign_WhenConstruct_ThenDefaultValuesWillBeSet)
{
    GroundSign ground_sign;
    EXPECT_EQ(ground_sign.variability, osi::OsiTrafficSignVariability::kFixed);
    EXPECT_EQ(ground_sign.direction_scope, osi::OsiTrafficSignDirectionScope::kNoDirection);
}

TEST_F(MountedSignTest,
       GivenMountedSign_WhenSupplementarySignSet_ThenSupplementarySignTypeAndVariabilityWillBeTheSameValues)
{
    auto supplementary_signs = mounted_sign_.supplementary_signs;
    ASSERT_EQ(supplementary_signs.size(), 1);
    EXPECT_EQ(supplementary_signs.at(0).variability, osi::OsiTrafficSignVariability::kOther);
    EXPECT_EQ(supplementary_signs.at(0).type, OsiSupplementarySignType::kOther);
}

TEST_F(MountedSignTest, GivenMountedSign_WhenSupplementarySignSet_ThenAssignedSupplementarySignPositionWillBeTheSame)
{
    auto supplementary_signs = mounted_sign_.supplementary_signs;
    ASSERT_EQ(supplementary_signs.size(), 1);

    EXPECT_EQ(supplementary_signs.at(0).pose.position, expected_sign_pose.position);
    EXPECT_EQ(supplementary_signs.at(0).pose.orientation, expected_sign_pose.orientation);
}

TEST_F(MountedSignTest, GivenMountedSign_WhenSupplementarySignsSet_ThenAssignedSupplementarySignDimensionWillBeTheSame)
{
    auto supplementary_signs = mounted_sign_.supplementary_signs;
    ASSERT_EQ(supplementary_signs.size(), 1);

    EXPECT_EQ(supplementary_signs.at(0).dimensions, expected_sign_dimension);
}

TEST_F(MountedSignTest, GivenMountedSign_WhenSupplementarySignsSet_ThenAssignedSupplementarySignValuesWillBeTheSame)
{
    auto supplementary_signs = mounted_sign_.supplementary_signs;
    ASSERT_EQ(supplementary_signs.size(), 1);

    auto value_info = supplementary_signs.at(0).value_information;
    ASSERT_EQ(value_info.size(), 1);

    EXPECT_EQ(value_info.at(0).text, expected_sign_information.at(0).text);
    EXPECT_EQ(value_info.at(0).value, expected_sign_information.at(0).value);
    EXPECT_EQ(value_info.at(0).value_unit, expected_sign_information.at(0).value_unit);
}

TEST_F(MountedSignTest,
       GivenMountedSignWithASupplementarySign_WhenGetAssignedLanesForSupplementarySign_ThenReturnInheritedAssignedLanes)
{
    auto supplementary_signs = mounted_sign_.supplementary_signs;

    ASSERT_EQ(supplementary_signs.front().GetAssignedLanes().size(), 2);
    EXPECT_EQ(supplementary_signs.front().GetAssignedLanes().front(), 42);
    EXPECT_EQ(supplementary_signs.front().GetAssignedLanes().back(), 43);
}
}  // namespace astas::environment::map

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
