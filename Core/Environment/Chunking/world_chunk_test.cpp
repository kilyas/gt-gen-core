/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Chunking/world_chunk.h"

#include "Core/Environment/Map/AstasMap/Internal/traffic_light.h"
#include "Core/Environment/Map/AstasMap/lane_group.h"
#include "Core/Environment/Map/AstasMap/road_objects.h"
#include "Core/Environment/Map/AstasMap/signs.h"
#include "Core/Tests/TestUtils/exception_testing.h"

#include <gtest/gtest.h>

namespace astas::environment::chunking
{

TEST(ChunkKeyTest, GivenTwoChunkKeys_WhenComparingGreaterAndLessThan_ThenComparisonCorrect)
{
    const ChunkKey a{0, 0};
    const ChunkKey b{1, 0};
    const ChunkKey c{0, 1};

    EXPECT_LT(a, b);
    EXPECT_LT(a, c);
    EXPECT_LT(c, b);
    EXPECT_LE(a, a);

    EXPECT_GT(b, a);
    EXPECT_GT(c, a);
    EXPECT_GT(b, c);
    EXPECT_GE(c, c);
}

TEST(ChunkKeyTest, GivenTwoChunkKeys_WhenCheckingEquality_ThenEqualityCorrect)
{
    const ChunkKey a{1, 2};
    const ChunkKey b{2, 1};
    const ChunkKey c{2, 1};

    EXPECT_EQ(a, a);
    EXPECT_EQ(b, b);
    EXPECT_EQ(b, c);

    EXPECT_NE(a, b);
    EXPECT_NE(c, a);
}

/// @brief Helper due to gtest's inability to handle aggregate initialization properly.
inline ChunkKey CreateChunkKey(std::int32_t i, std::int32_t j)
{
    return ChunkKey{i, j};
}

TEST(ChunkKeyComparison, GivenTwoChunkKey_WhenLessThan_ThenComparisonCorrect)
{
    EXPECT_LT(CreateChunkKey(0, 0), CreateChunkKey(1, 1));
    EXPECT_LT(CreateChunkKey(10, 20), CreateChunkKey(10, 21));
    EXPECT_LT(CreateChunkKey(10, 20), CreateChunkKey(20, 10));
    EXPECT_LT(CreateChunkKey(-10, 20), CreateChunkKey(10, 20));
    EXPECT_GE(CreateChunkKey(5, 10), CreateChunkKey(2, 3));
    EXPECT_GE(CreateChunkKey(1, 1), CreateChunkKey(1, 1));
}

TEST(ChunkKeyComparison, GivenTwoChunkKey_WhenLessEqual_ThenComparisonCorrect)
{
    EXPECT_LE(CreateChunkKey(0, 0), CreateChunkKey(1, 1));
    EXPECT_LE(CreateChunkKey(10, 20), CreateChunkKey(10, 21));
    EXPECT_LE(CreateChunkKey(10, 20), CreateChunkKey(20, 10));
    EXPECT_LE(CreateChunkKey(-10, 20), CreateChunkKey(10, 20));
    EXPECT_GT(CreateChunkKey(5, 10), CreateChunkKey(2, 3));
    EXPECT_LE(CreateChunkKey(1, 1), CreateChunkKey(1, 1));
}

TEST(ChunkKeyComparison, GivenTwoChunkKey_WhenEqual_ThenComparisonCorrect)
{
    EXPECT_EQ(CreateChunkKey(0, 0), CreateChunkKey(0, 0));
    EXPECT_EQ(CreateChunkKey(-1, -1), CreateChunkKey(-1, -1));
    EXPECT_EQ(CreateChunkKey(10, -3), CreateChunkKey(10, -3));
    EXPECT_NE(CreateChunkKey(10, 3), CreateChunkKey(10, -3));
    EXPECT_NE(CreateChunkKey(1, -3), CreateChunkKey(10, -3));
}

TEST(ChunkKeyComparison, GivenTwoChunkKey_WhenNotEqual_ThenComparisonCorrect)
{
    EXPECT_NE(CreateChunkKey(0, 1), CreateChunkKey(0, 0));
    EXPECT_NE(CreateChunkKey(-1, 1), CreateChunkKey(-1, -1));
    EXPECT_NE(CreateChunkKey(-10, -3), CreateChunkKey(10, -3));
    EXPECT_EQ(CreateChunkKey(10, 3), CreateChunkKey(10, 3));
}

TEST(ChunkKeyComparison, GivenTwoChunkKey_WhenGreaterThan_ThenComparisonCorrect)
{
    EXPECT_GT(CreateChunkKey(10, 0), CreateChunkKey(1, 1));
    EXPECT_GT(CreateChunkKey(10, 1), CreateChunkKey(1, 1));
    EXPECT_LE(CreateChunkKey(-10, -2), CreateChunkKey(-10, -1));
}

TEST(ChunkKeyComparison, GivenTwoChunkKey_WhenGreaterEqual_ThenComparisonCorrect)
{
    EXPECT_GE(CreateChunkKey(10, 0), CreateChunkKey(1, 1));
    EXPECT_GE(CreateChunkKey(10, 1), CreateChunkKey(1, 1));
    EXPECT_LE(CreateChunkKey(-10, -2), CreateChunkKey(-10, -1));
    EXPECT_GE(CreateChunkKey(20, 30), CreateChunkKey(20, 30));
    EXPECT_LT(CreateChunkKey(5, 10), CreateChunkKey(10, 5));
    EXPECT_GE(CreateChunkKey(5, 10), CreateChunkKey(5, 10));
}

TEST(MapChunkValidation, GivenMapChunkWithEmptyFields_WhenValidate_ThenExpectEnvironmentException)
{
    ChunkKey chunk_key{};
    CoordinateType lower_left_position{};
    auto map_chunk = MapChunk(chunk_key, lower_left_position);

    EXPECT_TRUE(ExpectExceptionContains<EnvironmentException>(
        [&]() { map_chunk.Validate(); },
        "MapChunk validation failed: lane groups, road objects, traffic signs and traffic lights are all"
        "empty. Please Contact GTGen support"));
}

TEST(MapChunkValidation, GivenMapChunkWithTrafficSign_WhenValidate_ThenExceptionIsNotThrown)
{
    ChunkKey chunk_key{};
    CoordinateType lower_left_position{};
    auto map_chunk = MapChunk(chunk_key, lower_left_position);
    auto traffic_sign = std::make_unique<environment::map::TrafficSign>();
    map_chunk.traffic_signs = {traffic_sign.get()};

    EXPECT_NO_THROW(map_chunk.Validate());
}

TEST(MapChunkValidation, GivenMapChunkWithTrafficLight_WhenValidate_ThenExceptionIsNotThrown)
{
    ChunkKey chunk_key{};
    CoordinateType lower_left_position{};
    auto map_chunk = MapChunk(chunk_key, lower_left_position);
    auto traffic_light = std::make_unique<environment::map::TrafficLight>();
    map_chunk.traffic_lights = {traffic_light.get()};

    EXPECT_NO_THROW(map_chunk.Validate());
}

TEST(MapChunkValidation, GivenMapChunkWithRoadObject_WhenValidate_ThenExceptionIsNotThrown)
{
    ChunkKey chunk_key{};
    CoordinateType lower_left_position{};
    auto map_chunk = MapChunk(chunk_key, lower_left_position);
    auto road_object = std::make_unique<environment::map::RoadObject>();
    map_chunk.road_objects = {road_object.get()};

    EXPECT_NO_THROW(map_chunk.Validate());
}

TEST(MapChunkValidation, GivenMapChunkWithLaneGroup_WhenValidate_ThenExceptionIsNotThrown)
{
    using environment::map::LaneGroup;
    ChunkKey chunk_key{};
    CoordinateType lower_left_position{};
    auto map_chunk = MapChunk(chunk_key, lower_left_position);
    auto lane_group = std::make_unique<LaneGroup>(0, LaneGroup::Type::kUnknown);

    map_chunk.lane_groups = {lane_group.get()};

    EXPECT_NO_THROW(map_chunk.Validate());
}
}  // namespace astas::environment::chunking
