/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Environment/Chunking/chunking.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Service/MantleApiExtension/formatting.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/exception_testing.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <gtest/gtest.h>

namespace astas::environment::chunking
{
using units::literals::operator""_m;

class ChunkingTest : public testing::Test
{
  protected:
    void AddHostVehicle(const mantle_api::Vec3<units::length::meter_t>& position)
    {
        auto properties = std::make_unique<mantle_api::VehicleProperties>();
        properties->model = "BMW7S2015";
        properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};
        properties->is_host = true;

        mantle_api::IEntity& entity = entity_repository_.Create(0, "Host", *properties);
        entity.SetPosition(position);
    }

    void AddTrafficVehicle(mantle_api::UniqueId id, const mantle_api::Vec3<units::length::meter_t>& position)
    {
        auto properties = std::make_unique<mantle_api::VehicleProperties>();
        properties->model = "BMW7S2015";
        properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};

        mantle_api::IEntity& entity = entity_repository_.Create(id, "Vehicle" + std::to_string(id), *properties);
        entity.SetPosition(position);
    }

    void AddPedestrian(mantle_api::UniqueId id, const mantle_api::Vec3<units::length::meter_t>& position)
    {
        auto properties = std::make_unique<mantle_api::PedestrianProperties>();
        properties->model = "boy";
        properties->bounding_box.dimension = {4_m, 2_m, 1.5_m};

        mantle_api::IEntity& entity = entity_repository_.Create(id, "Pedestrian" + std::to_string(id), *properties);
        entity.SetPosition(position);
    }

    void AddTrafficSign(mantle_api::UniqueId id, const mantle_api::Vec3<units::length::meter_t>& position)
    {
        mantle_api::StaticObjectProperties properties{};
        properties.bounding_box.dimension = {4_m, 2_m, 1.5_m};

        mantle_api::IEntity& entity = entity_repository_.Create(id, "TrafficSign" + std::to_string(id), properties);
        entity.SetPosition(position);
    }

    void AddLaneFromCenterline(mantle_api::UniqueId lane_group_id,
                               mantle_api::UniqueId lane_id,
                               std::vector<mantle_api::Vec3<units::length::meter_t>> centerline)
    {
        auto* lane_group = map_->FindLaneGroup(lane_group_id);
        if (lane_group == nullptr)
        {
            lane_group = &map_->AddLaneGroup({lane_group_id, environment::map::LaneGroup::Type::kUnknown});
        }

        environment::map::Lane lane{lane_id};
        lane.center_line = std::move(centerline);

        map_->AddLane(lane_group->id, lane);
    }

    void AddRoadObject(mantle_api::UniqueId id,
                       const mantle_api::Vec3<units::length::meter_t>& position,
                       std::vector<mantle_api::Vec3<units::length::meter_t>> base_polygon)
    {
        environment::map::RoadObject road_object{};
        road_object.id = id;
        road_object.pose.position = position;
        road_object.base_polygon = std::move(base_polygon);

        map_->road_objects.emplace_back(std::move(road_object));
    }

    void AddTrafficLight(mantle_api::UniqueId id, const CoordinateType& position)
    {
        environment::map::TrafficLight traffic_light{};
        traffic_light.id = id;
        traffic_light.light_bulbs.emplace_back().pose.position = position;

        map_->traffic_lights.emplace_back(std::move(traffic_light));
    }

    /// @todo: Since we now store pointers we can shorten the next three functions
    const mantle_api::IEntity* GetMovingObjectPointer(mantle_api::UniqueId id,
                                                      const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities)
    {
        if (const auto object{std::find_if(entities.begin(),
                                           entities.end(),
                                           [id](const auto& entity) {
                                               return entity->GetUniqueId() == id &&
                                                      nullptr == dynamic_cast<mantle_api::StaticObjectProperties*>(
                                                                     entity->GetProperties());
                                           })};
            object != entities.end())
        {
            return (*object).get();
        }
        else
        {
            return nullptr;
        }
    }

    const environment::map::RoadObject* GetRoadObjectPointer(mantle_api::UniqueId id)
    {
        if (const auto road_object_it{
                std::find_if(map_->road_objects.begin(),
                             map_->road_objects.end(),
                             [id](const environment::map::RoadObject& road_object) { return road_object.id == id; })};
            road_object_it != map_->road_objects.end())
        {
            return &*road_object_it;
        }
        else
        {
            return nullptr;
        }
    }

    const mantle_api::IEntity* GetTrafficSignPointer(mantle_api::UniqueId id,
                                                     const std::vector<std::unique_ptr<mantle_api::IEntity>>& entities)
    {
        if (const auto object{std::find_if(entities.begin(),
                                           entities.end(),
                                           [id](const auto& entity) {
                                               return entity->GetUniqueId() == id &&
                                                      nullptr != dynamic_cast<mantle_api::StaticObjectProperties*>(
                                                                     entity->GetProperties());
                                           })};
            object != entities.end())
        {
            return (*object).get();
        }
        else
        {
            return nullptr;
        }
    }

    const environment::map::TrafficSign* GetTrafficSignPointer(
        mantle_api::UniqueId id,
        const std::vector<std::unique_ptr<environment::map::MountedSign>>& traffic_signs)
    {
        if (const auto traffic_sign_it{std::find_if(traffic_signs.begin(),
                                                    traffic_signs.end(),
                                                    [id](const auto& traffic_sign) { return traffic_sign->id == id; })};
            traffic_sign_it != traffic_signs.end())
        {
            return (*traffic_sign_it).get();
        }
        else
        {
            return nullptr;
        }
    }

    const environment::map::TrafficLight* GetTrafficLightPointer(mantle_api::UniqueId id)
    {
        if (const auto traffic_light_it{
                std::find_if(map_->traffic_lights.cbegin(),
                             map_->traffic_lights.cend(),
                             [id](const auto& traffic_light) { return traffic_light.id == id; })};
            traffic_light_it != map_->traffic_lights.cend())
        {
            return &(*traffic_light_it);
        }
        else
        {
            return nullptr;
        }
    }

    std::unique_ptr<environment::map::AstasMap> map_{test_utils::MapCatalogue::EmptyMap()};
    environment::map::LaneLocationProvider lane_location_provider_{*map_};
    service::utility::UniqueIdProvider unique_id_provider_{};
    environment::api::EntityRepository entity_repository_{&unique_id_provider_};
};

TEST_F(ChunkingTest, GivenEmptyMapWithHostVehicle_WhenChunkingInitialize_ThenEnvironmentExceptionIsExpected)
{
    AddHostVehicle({5.0_m, 5.0_m, 5.0_m});
    Chunking<PolylineChunking> chunking;

    EXPECT_TRUE(ExpectExceptionContains<EnvironmentException>([&]() { chunking.Initialize(*map_, 50, 0); },
                                                              "PolylineChunking: static chunk list is empty"));
}

TEST_F(ChunkingTest, GivenMapWithTrafficLightsAndHostVehicle_WhenChunking_ThenTrafficLightsAreCorrectlyAddedToChunks)
{
    // Traffic light ID + position
    const std::vector<std::pair<mantle_api::UniqueId, CoordinateType>> traffic_lights{
        {3000, {-40.0_m, 40.0_m, 0.0_m}},    // Chunk {-50.0_m, 0.0_m}
        {3001, {-40.0_m, 140.0_m, 0.0_m}},   // Chunk {-50.0_m, 100.0_m}
        {3002, {60.0_m, -90.0_m, 0.0_m}},    // Chunk {50.0_m, -100.0_m}
        {3003, {40.0_m, 9.0_m, 0.0_m}},      // Chunk {0.0_m, 0.0_m}
        {3004, {45.0_m, 19.0_m, 0.0_m}},     // Chunk {0.0_m, 0.0_m}
        {3005, {160.0_m, 190.0_m, 0.0_m}},   // Chunk {150.0_m, 150.0_m} outside
        {3006, {156.0_m, 190.0_m, 0.0_m}},   // Chunk {150.0_m, 150.0_m} outside
        {3007, {155.0_m, 160.0_m, 0.0_m}}};  // Chunk {150.0_m, 150.0_m} outside

    // Chunk lower left + traffic lights ids
    const std::vector<std::pair<CoordinateType, std::vector<mantle_api::UniqueId>>> expected_chunks{
        {{-50.0_m, 0.0_m, 0.0_m}, {3000}},
        {{-50.0_m, 100.0_m, 0.0_m}, {3001}},
        {{0.0_m, 0.0_m, 0.0_m}, {3003, 3004}},
        {{50.0_m, -100.0_m, 0.0_m}, {3002}}};

    AddHostVehicle({5.0_m, 5.0_m, 5.0_m});  // Chunk {0.0_m, 0.0_m, 0.0_m}
    for (const auto& [traffic_light_id, traffic_light_position] : traffic_lights)
    {
        AddTrafficLight(traffic_light_id, traffic_light_position);
    }

    Chunking<PolylineChunking> chunking;
    chunking.Initialize(*map_, 50, 2);
    const auto chunks{
        chunking.GetWorldChunks(entity_repository_.GetEntities(), entity_repository_.GetHost().GetPosition())};
    EXPECT_EQ(expected_chunks.size(), chunks.size());

    for (const auto& [chunk_lower_left_coord, expected_traffic_light_ids] : expected_chunks)
    {
        const auto actual_chunk_it{std::find_if(
            chunks.cbegin(), chunks.cend(), [chunk_lower_left = chunk_lower_left_coord](const auto& actual_chunk) {
                return actual_chunk.lower_left == chunk_lower_left;
            })};
        ASSERT_NE(actual_chunk_it, chunks.cend());
        EXPECT_EQ(actual_chunk_it->traffic_lights.size(), expected_traffic_light_ids.size())
            << fmt::format("Mismatch in chunk {}", chunk_lower_left_coord);

        // find missing traffic lights
        for (const mantle_api::UniqueId expected_traffic_light_id : expected_traffic_light_ids)
        {
            const auto* expected_traffic_light_pointer{GetTrafficLightPointer(expected_traffic_light_id)};

            EXPECT_EQ(1,
                      std::count_if(actual_chunk_it->traffic_lights.cbegin(),
                                    actual_chunk_it->traffic_lights.cend(),
                                    [&expected_traffic_light_pointer](const auto& actual_pointer) {
                                        return actual_pointer == expected_traffic_light_pointer;
                                    }))
                << fmt::format("missing traffic light id {} in chunk {}",
                               expected_traffic_light_id,
                               actual_chunk_it->lower_left);
        }
        // find unexpected traffic lights
        for (const auto* actual_traffic_light_pointer : actual_chunk_it->traffic_lights)
        {
            ASSERT_NE(nullptr, actual_traffic_light_pointer);
            EXPECT_EQ(1,
                      std::count_if(expected_traffic_light_ids.cbegin(),
                                    expected_traffic_light_ids.cend(),
                                    [this, actual_traffic_light_pointer](auto expected_traffic_light_id) {
                                        const auto* expected_traffic_light_pointer{
                                            GetTrafficLightPointer(expected_traffic_light_id)};
                                        return actual_traffic_light_pointer == expected_traffic_light_pointer;
                                    }))
                << fmt::format("unexpected traffic light id {} in chunk {}",
                               actual_traffic_light_pointer->id,
                               actual_chunk_it->lower_left);
        }
    }
}

/// @test Test with 2 cells per direction, 50m chunk-size and small lanes such that creating 30 chunks is feasible
/// Copied and update from DummyChunking
TEST_F(ChunkingTest, GivenTwoChunksPerDirection_WhenInitialize_ThenWorldChunksCorrect)
{
    Chunking<PolylineChunking> chunking;

    AddLaneFromCenterline(1, 1, {{0.0_m, 0.0_m, 0.0_m}, {5.0_m, 10.0_m, 0.0_m}});  // Chunk in {0.0_m, 0.0_m}
    AddLaneFromCenterline(2,
                          2,
                          {{-104.0_m, 30.0_m, 0.0_m},  // Chunk{-150.0_m, 0.0_m}
                           {-55.0_m, 25.0_m, 0.0_m},   // Chunk{-100.0_m, 0.0_m}
                           {-10.0_m, 10.0_m, 0.0_m},   // Chunk{-50.0_m, 0.0_m}
                           {10.0_m, 10.0_m, 0.0_m},    // Chunk{0.0_m, 0.0_m}
                           {60.0_m, 10.0_m, 0.0_m},    // Chunk{50.0_m, 0.0_m}
                           {110_m, 10.0_m, 0.0_m}});   // Chunk{100.0_m, 0.0_m}

    AddLaneFromCenterline(
        3, 3, {{-90.0_m, -90.0_m, 0.0_m}, {-40.0_m, -90.0_m, 0.0_m}, {10.0_m, -90.0_m, 0.0_m}, {60.0_m, -90.0_m, 0.0_m},
               {110.0_m, -90.0_m, 0.0_m}, {110.0_m, -40.0_m, 0.0_m}, {60.0_m, -40.0_m, 0.0_m}, {10.0_m, -40.0_m, 0.0_m},
               {-40.0_m, -40.0_m, 0.0_m}, {-90.0_m, -40.0_m, 0.0_m}, {-90.0_m, 10.0_m, 0.0_m}, {-40.0_m, 10.0_m, 0.0_m},
               {10.0_m, 10.0_m, 0.0_m},   {60.0_m, 10.0_m, 0.0_m},   {110.0_m, 10.0_m, 0.0_m}, {110.0_m, 60.0_m, 0.0_m},
               {60.0_m, 60.0_m, 0.0_m},   {10.0_m, 60.0_m, 0.0_m},   {-40.0_m, 60.0_m, 0.0_m}, {-90.0_m, 60.0_m, 0.0_m},
               {-90.0_m, 110.0_m, 0.0_m}, {-40.0_m, 110.0_m, 0.0_m}, {10.0_m, 110.0_m, 0.0_m}, {60.0_m, 110.0_m, 0.0_m},
               {110.0_m, 110.0_m, 0.0_m}});  // all relevant chunks, but nothing more

    // Chunk{0, 150}, outside of relevant zone, but in lane-group 3 which is relevant from above
    AddLaneFromCenterline(3, 4, {{10.0_m, 160.0_m, 0.0_m}, {20.0_m, 170.0_m, 0.0_m}});

    // Chunk{0, 150}, outside of relevant zone, no other lane in lane-group is relevant, shouldn't show up in final
    // chunks
    AddLaneFromCenterline(4, 5, {{10.0_m, 160.0_m, 0.0_m}, {20.0_m, 170.0_m, 0.0_m}});

    // Chunk{0, 0}, very small
    AddRoadObject(1000, {6.0_m, 23.0_m, 0.0_m}, {{0.0_m, 0.0_m, 0.0_m}, {1.0_m, 1.1_m, 0.0_m}});
    // Chunk {50, 50} and Chunk {100, 50}
    AddRoadObject(
        1001, {95.0_m, 95.0_m, 0.0_m}, {{10.0_m, 1.0_m, 0.0_m}, {10.0_m, -10.0_m, 0.0_m}, {-10.0_m, 4.0_m, 0.0_m}});
    // Chunk {0, 150}, outside
    AddRoadObject(1002, {200.0_m, 4.0_m, 0.0_m}, {});
    // Position in {-150, -150}, but by base polygon within relevant {-100, -100}
    AddRoadObject(1003,
                  {-120.0_m, -120.0_m, 0.0_m},
                  {{25.0_m, 25.0_m, 0.0_m}, {50.0_m, 50.0_m, 0.0_m}, {-20.0_m, -30.0_m, 0.0_m}});

    // Chunk {-50.0_m, 0.0_m}
    AddTrafficSign(2000, {-40.0_m, 40.0_m, 0.0_m});
    // Chunk {-50.0_m, 100.0_m}
    AddTrafficSign(2001, {-40.0_m, 140.0_m, 0.0_m});
    // Chunk {50.0_m, -100.0_m}
    AddTrafficSign(2002, {60.0_m, -90.0_m, 0.0_m});
    // Chunk {0.0_m, 0.0_m}
    AddTrafficSign(2003, {40.0_m, 9.0_m, 0.0_m});
    // Chunk {0.0_m, 0.0_m}
    AddTrafficSign(2004, {45.0_m, 19.0_m, 0.0_m});
    // Chunk {150.0_m, 150.0_m} outside
    AddTrafficSign(2005, {160.0_m, 190.0_m, 0.0_m});
    // Chunk {150.0_m, 150.0_m} outside
    AddTrafficSign(2006, {156.0_m, 190.0_m, 0.0_m});
    // Chunk {150.0_m, 150.0_m} outside
    AddTrafficSign(2007, {155.0_m, 160.0_m, 0.0_m});

    AddHostVehicle({5.0_m, 5.0_m, 5.0_m});            // Chunk{0.0_m, 0.0_m, 0.0_m}
    AddTrafficVehicle(1, {45.0_m, 23.01_m, 4.0_m});   // Chunk{0.0_m, 0.0_m, 0.0_m}
    AddTrafficVehicle(2, {23.1_m, -102.5_m, 0.0_m});  // Chunk{0.0_m, -150.0_m, 0.0_m}
    AddTrafficVehicle(3, {-93.4_m, -84.0_m, 0.0_m});  // Chunk{-100.0_m, -100.0_m, 0.0_m}
    AddTrafficVehicle(6, {-83.4_m, -84.0_m, 0.0_m});  // Chunk{-100.0_m, -100.0_m, 0.0_m}
    AddTrafficVehicle(7, {-73.4_m, -84.0_m, 0.0_m});  // Chunk{-100.0_m, -100.0_m, 0.0_m}
    AddTrafficVehicle(8, {-63.4_m, -84.0_m, 0.0_m});  // Chunk{-100.0_m, -100.0_m, 0.0_m}

    AddPedestrian(4, {0.0_m, 0.0_m, 0.0_m});   // Chunk{0.0_m, 0.0_m, 0.0_m}
    AddPedestrian(5, {55.0_m, 0.0_m, 0.0_m});  // Chunk{50.0_m, 0.0_m, 0.0_m}

    AddTrafficSign(2010, {0.0_m, 0.0_m, 0.0_m});  // Chunk {0.0_m, 0.0_m}

    const std::vector<std::tuple<CoordinateType,                      // Chunk lower left
                                 std::vector<mantle_api::UniqueId>,   // lane group ids
                                 std::vector<mantle_api::UniqueId>,   // moving objects ids
                                 std::vector<mantle_api::UniqueId>,   // road object ids
                                 std::vector<mantle_api::UniqueId>>>  // traffic sign ids
        expected_chunks{{{-100.0_m, -100.0_m, 0.0_m}, {3}, {3, 6, 7, 8}, {1003}, {}},
                        {{-100.0_m, -50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{-100.0_m, 0.0_m, 0.0_m}, {2, 3}, {}, {}, {}},
                        {{-100.0_m, 50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{-100.0_m, 100.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{-50.0_m, -100.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{-50.0_m, -50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{-50.0_m, 0.0_m, 0.0_m}, {2, 3}, {}, {}, {2000}},
                        {{-50.0_m, 50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{-50.0_m, 100.0_m, 0.0_m}, {3}, {}, {}, {2001}},
                        {{0.0_m, -100.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{0.0_m, -50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{0.0_m, 0.0_m, 0.0_m}, {1, 2, 3}, {0, 1, 4}, {1000}, {2003, 2004, 2010}},
                        {{0.0_m, 50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{0.0_m, 100.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{50.0_m, -100.0_m, 0.0_m}, {3}, {}, {}, {2002}},
                        {{50.0_m, -50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{50.0_m, 0.0_m, 0.0_m}, {2, 3}, {5}, {}, {}},
                        {{50.0_m, 50.0_m, 0.0_m}, {3}, {}, {1001}, {}},
                        {{50.0_m, 100.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{100.0_m, -100.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{100.0_m, -50.0_m, 0.0_m}, {3}, {}, {}, {}},
                        {{100.0_m, 0.0_m, 0.0_m}, {2, 3}, {}, {}, {}},
                        {{100.0_m, 50.0_m, 0.0_m}, {3}, {}, {1001}, {}},
                        {{100.0_m, 100.0_m, 0.0_m}, {3}, {}, {}, {}}};

    chunking.Initialize(*map_, 50, 2);
    const auto chunks{
        chunking.GetWorldChunks(entity_repository_.GetEntities(), entity_repository_.GetHost().GetPosition())};

    EXPECT_EQ(expected_chunks.size(), chunks.size());
    for (const auto& expected_chunk : expected_chunks)
    {
        const auto& expected_coordinate{std::get<0>(expected_chunk)};
        const auto& expected_lane_groups{std::get<1>(expected_chunk)};
        const auto& expected_moving_objects{std::get<2>(expected_chunk)};
        const auto& expected_road_objects{std::get<3>(expected_chunk)};
        const auto& expected_traffic_signs{std::get<4>(expected_chunk)};

        EXPECT_EQ(1,
                  std::count_if(chunks.begin(),
                                chunks.end(),
                                [&expected_coordinate](const auto& actual_chunk) {
                                    return actual_chunk.lower_left == expected_coordinate;
                                }))
            << fmt::format("missing chunk: {}", expected_coordinate);

        const auto actual_chunk_it{
            std::find_if(chunks.begin(), chunks.end(), [&expected_coordinate](const auto& actual_chunk) {
                return actual_chunk.lower_left == expected_coordinate;
            })};

        ASSERT_NE(chunks.end(), actual_chunk_it);
        EXPECT_EQ(expected_lane_groups.size(), actual_chunk_it->lane_groups.size())
            << fmt::format("Mismatch in chunk {}", expected_coordinate);

        // find missing lane-groups
        for (const auto& expected_lane_group_id : expected_lane_groups)
        {
            const auto* expected_lane_group_pointer{map_->FindLaneGroup(expected_lane_group_id)};
            EXPECT_EQ(1,
                      std::count_if(actual_chunk_it->lane_groups.begin(),
                                    actual_chunk_it->lane_groups.end(),
                                    [&expected_lane_group_pointer](const auto* actual_pointer) {
                                        return actual_pointer == expected_lane_group_pointer;
                                    }))
                << fmt::format(
                       "missing lane-group id {} in chunk {}", expected_lane_group_id, actual_chunk_it->lower_left);
        }

        // find unexpected lane-groups
        for (const auto* actual_lane_group_pointer : actual_chunk_it->lane_groups)
        {
            ASSERT_NE(nullptr, actual_lane_group_pointer);
            EXPECT_EQ(1,
                      std::count_if(expected_lane_groups.begin(),
                                    expected_lane_groups.end(),
                                    [this, actual_lane_group_pointer](auto expected_lane_group_id) {
                                        return actual_lane_group_pointer == map_->FindLaneGroup(expected_lane_group_id);
                                    }))
                << fmt::format("unexpected lane-group id {} in chunk {}",
                               actual_lane_group_pointer->id,
                               actual_chunk_it->lower_left);
        }

        auto amount_moving_objects = std::count_if(
            actual_chunk_it->entities.begin(), actual_chunk_it->entities.end(), [](const auto& moving_object) {
                return nullptr == dynamic_cast<mantle_api::StaticObjectProperties*>(moving_object->GetProperties());
            });
        EXPECT_EQ(expected_moving_objects.size(), amount_moving_objects)
            << fmt::format("Mismatch in chunk {}", expected_coordinate);

        // find missing moving objects
        for (const auto& expected_moving_object_id : expected_moving_objects)
        {
            const auto* expected_vehicle_pointer{
                GetMovingObjectPointer(expected_moving_object_id, entity_repository_.GetEntities())};

            EXPECT_EQ(1,
                      std::count_if(actual_chunk_it->entities.begin(),
                                    actual_chunk_it->entities.end(),
                                    [&expected_vehicle_pointer](const auto* actual_pointer) {
                                        return actual_pointer == expected_vehicle_pointer;
                                    }))
                << fmt::format(
                       "missing vehicle id {} in chunk {}", expected_moving_object_id, actual_chunk_it->lower_left);
        }

        // find unexpected moving objects
        for (auto* actual_moving_object_pointer : actual_chunk_it->entities)
        {
            if (nullptr !=
                dynamic_cast<mantle_api::StaticObjectProperties*>(actual_moving_object_pointer->GetProperties()))
            {
                continue;
            }

            ASSERT_NE(nullptr, actual_moving_object_pointer);
            EXPECT_EQ(1,
                      std::count_if(expected_moving_objects.begin(),
                                    expected_moving_objects.end(),
                                    [this, actual_moving_object_pointer](auto expected_vehicle_id) {
                                        return actual_moving_object_pointer ==
                                               GetMovingObjectPointer(expected_vehicle_id,
                                                                      entity_repository_.GetEntities());
                                    }))
                << fmt::format("unexpected moving object id {} in chunk {}",
                               actual_moving_object_pointer->GetUniqueId(),
                               actual_chunk_it->lower_left);
        }

        EXPECT_EQ(expected_road_objects.size(), actual_chunk_it->road_objects.size())
            << fmt::format("Mismatch in chunk {}", expected_coordinate);
        // find missing road objects
        for (const auto& expected_road_object_id : expected_road_objects)
        {
            const auto* expected_road_object_pointer{GetRoadObjectPointer(expected_road_object_id)};

            EXPECT_EQ(1,
                      std::count_if(actual_chunk_it->road_objects.begin(),
                                    actual_chunk_it->road_objects.end(),
                                    [&expected_road_object_pointer](const auto* actual_pointer) {
                                        return actual_pointer == expected_road_object_pointer;
                                    }))
                << fmt::format(
                       "missing road object id {} in chunk {}", expected_road_object_id, actual_chunk_it->lower_left);
        }

        // find unexpected road objects
        for (const auto* actual_road_object_pointer : actual_chunk_it->road_objects)
        {
            ASSERT_NE(nullptr, actual_road_object_pointer);
            EXPECT_EQ(1,
                      std::count_if(expected_road_objects.begin(),
                                    expected_road_objects.end(),
                                    [this, actual_road_object_pointer](auto expected_road_object_id) {
                                        return actual_road_object_pointer ==
                                               GetRoadObjectPointer(expected_road_object_id);
                                    }))
                << fmt::format("unexpected road_object id {} in chunk {}",
                               actual_road_object_pointer->id,
                               actual_chunk_it->lower_left);
        }

        auto traffic_sign_count = std::count_if(
            actual_chunk_it->entities.begin(), actual_chunk_it->entities.end(), [](const auto& moving_object) {
                return nullptr != dynamic_cast<mantle_api::StaticObjectProperties*>(moving_object->GetProperties());
            });
        EXPECT_EQ(expected_traffic_signs.size(), traffic_sign_count)
            << fmt::format("Mismatch in chunk {}", expected_coordinate);
        // find missing traffic signs
        for (const auto& expected_traffic_sign_id : expected_traffic_signs)
        {
            const auto* expected_traffic_sign_pointer{
                GetTrafficSignPointer(expected_traffic_sign_id, entity_repository_.GetEntities())};
            //            if (expected_traffic_sign_pointer == nullptr)
            //            {
            //                expected_traffic_sign_pointer =
            //                    GetTrafficSignPointer(expected_traffic_sign_id, entity_placement_.GetMountedSigns());
            //            }

            EXPECT_EQ(1,
                      std::count_if(actual_chunk_it->entities.begin(),
                                    actual_chunk_it->entities.end(),
                                    [&expected_traffic_sign_pointer](const auto* actual_pointer) {
                                        return actual_pointer == expected_traffic_sign_pointer;
                                    }))
                << fmt::format(
                       "missing traffic sign id {} in chunk {}", expected_traffic_sign_id, actual_chunk_it->lower_left);
        }

        // find unexpected traffic signs
        for (const auto* actual_traffic_sign_pointer : actual_chunk_it->entities)
        {
            if (nullptr ==
                dynamic_cast<mantle_api::StaticObjectProperties*>(actual_traffic_sign_pointer->GetProperties()))
            {
                continue;
            }

            ASSERT_NE(nullptr, actual_traffic_sign_pointer);
            EXPECT_EQ(1,
                      std::count_if(expected_traffic_signs.begin(),
                                    expected_traffic_signs.end(),
                                    [this, actual_traffic_sign_pointer](auto expected_traffic_sign_id) {
                                        const auto* expected_traffic_sign_pointer{GetTrafficSignPointer(
                                            expected_traffic_sign_id, entity_repository_.GetEntities())};
                                        return actual_traffic_sign_pointer == expected_traffic_sign_pointer;
                                    }))
                << fmt::format("unexpected traffic_sign id {} in chunk {}",
                               actual_traffic_sign_pointer->GetUniqueId(),
                               actual_chunk_it->lower_left);
        }
    }
}

}  // namespace astas::environment::chunking
