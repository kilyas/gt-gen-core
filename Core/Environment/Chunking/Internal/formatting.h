/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_FORMATTING_H
#define GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_FORMATTING_H

#include "Core/Environment/Chunking/Internal/polyline_chunking.h"

#include <fmt/format.h>

namespace fmt
{

template <>
struct formatter<astas::environment::chunking::ChunkKey>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const astas::environment::chunking::ChunkKey& key, FormatContext& ctx)
    {
        return format_to(ctx.out(), "{}, {}", key.i, key.j);
    }
};

}  // namespace fmt

// google test
namespace astas::environment::chunking
{

/// @brief GTest printer
inline void PrintTo(const ChunkKey& key, std::ostream* os)
{
    *os << fmt::format("{}", key);
}
}  // namespace astas::environment::chunking

#endif  // GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_FORMATTING_H
