/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Chunking/Internal/boost_astas_conversion.h"

#include <gtest/gtest.h>

namespace astas::environment::chunking
{

TEST(BoostAdoptionVector3, GivenVector3_WhenBoostGeometryDistance_ThenDistanceIsCorrect)
{
    namespace bg = boost::geometry;

    glm::dvec3 a{0.0, 0.0, 0.0};
    glm::dvec3 b{10.0, 20.0, 0.0};
    glm::dvec3 b_z{10.0, 20.0, 30.0};
    glm::dvec3 c;

    bg::assign_values(c, 25.0, 40.0);

    EXPECT_DOUBLE_EQ(25.0, c.x);
    EXPECT_DOUBLE_EQ(40.0, c.y);

    const double expected_distance_ab{std::sqrt(10.0 * 10.0 + 20.0 * 20.0)};
    const double expected_distance_ac{std::sqrt(25.0 * 25.0 + 40.0 * 40.0)};

    EXPECT_DOUBLE_EQ(expected_distance_ab, bg::distance(a, b));
    EXPECT_DOUBLE_EQ(expected_distance_ab, bg::distance(a, b_z));
    EXPECT_DOUBLE_EQ(expected_distance_ac, bg::distance(a, c));
}

TEST(BoostAdoptionVector3, GivenPolyline_WhenBoostGeometryLength_ThenLengthOfPolylineCorrect)
{
    namespace bg = boost::geometry;

    std::vector<glm::dvec3> line{{0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {2.0, 0.0, 0.0}, {2.0, 1.0, 0.0}, {2.0, 2.0, 3.0}};

    EXPECT_DOUBLE_EQ(4.0, static_cast<double>(bg::length(line)));
}

TEST(BoostAdoptionVector3, GivenBox_WhenIntersectsAndWithin_ThenIntersectionAndWithinDetectedCorrectly)
{
    namespace bg = boost::geometry;

    bg::model::box<glm::dvec3> box{{0.0, 0.0, 0.0}, {10.0, 10.0, 0.0}};

    EXPECT_TRUE(bg::intersects(box, glm::dvec3(5.0, 5.0, 0.0)));
    EXPECT_TRUE(bg::within(glm::dvec3(5.0, 5.0, 0.0), box));

    EXPECT_FALSE(bg::intersects(box, glm::dvec3(15.0, 5.0, 0.0)));
    EXPECT_FALSE(bg::within(glm::dvec3(155.0, 5.0, 0.0), box));

    EXPECT_TRUE(bg::intersects(box, glm::dvec3(0.0, 0.0, 0.0)));
    EXPECT_FALSE(bg::within(glm::dvec3(0.0, 0.0, 0.0), box));
}

TEST(BoostAdoptionVector3, GivenBoxAndPolylines_WhenIntersects_ThenIntersectionDetectedCorrectly)
{
    namespace bg = boost::geometry;

    bg::model::box<glm::dvec3> box{{0.0, 0.0, 0.0}, {10.0, 10.0, 0.0}};

    std::vector<glm::dvec3> goes_through{{0.0, 0.0, 0.0}, {5.0, 5.0, 0.0}, {15.0, 15.0, 0.0}};

    std::vector<glm::dvec3> along_edge{{0.0, 0.0, 0.0}, {10.0, 0.0, 0.0}, {20.0, 0.0, 0.0}};
    std::vector<glm::dvec3> completely_within{{1.0, 1.0, 0.0}, {2.0, 2.0, 0.0}, {3.0, 3.0, 0.0}};

    std::vector<glm::dvec3> edges_outside_intersects{{5.0, 11.0, 0.0}, {11.0, 5.0, 0.0}};

    std::vector<glm::dvec3> completely_outside{{100.0, 1.0, 0.0}, {200.0, 2.0, 0.0}, {300.0, 3.0, 0.0}};

    EXPECT_TRUE(bg::intersects(goes_through, box));
    EXPECT_TRUE(bg::intersects(along_edge, box));
    EXPECT_TRUE(bg::intersects(completely_within, box));
    EXPECT_TRUE(bg::intersects(edges_outside_intersects, box));

    EXPECT_FALSE(bg::intersects(completely_outside, box));
}

}  // namespace astas::environment::chunking
