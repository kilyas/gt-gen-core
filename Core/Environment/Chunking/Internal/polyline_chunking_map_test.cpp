/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Chunking/Internal/formatting.h"
#include "Core/Environment/Chunking/Internal/polyline_chunking.h"
#include "Core/Tests/TestUtils/exception_testing.h"

#include <gtest/gtest.h>

namespace astas::environment::chunking
{
using units::literals::operator""_m;

class PolylineChunkingMapTest : public testing::Test
{
  protected:
    PolylineChunkingMapTest() { chunking_.SetChunkSize(50.0); }

    PolylineChunking chunking_;
};

TEST_F(PolylineChunkingMapTest,
       GivenLaneWithTwoPointsSpreadAcrossFourDiagonalChunksNorthWest_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    using environment::map::AstasMap;
    using environment::map::Lane;
    using environment::map::LaneGroup;

    Lane lane{1};
    lane.center_line = {{-25.0_m, 25.0_m, 0.0_m}, {-175.0_m, 175.0_m, 0.0_m}};

    LaneGroup lane_group1{1001, LaneGroup::Type::kUnknown};
    AstasMap map;
    map.AddLaneGroup(lane_group1);
    map.AddLane(1001, lane);

    StaticChunkList chunks = chunking_.InitializeChunks(map);
    EXPECT_EQ(chunks.size(), 10);
}

TEST_F(PolylineChunkingMapTest,
       GivenLaneWithTwoPointsSpreadAcrossFourDiagonalChunksSouthEast_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    using environment::map::AstasMap;
    using environment::map::Lane;
    using environment::map::LaneGroup;

    Lane lane{1};
    lane.center_line = {{25.0_m, -25.0_m, 0.0_m}, {175.0_m, -175.0_m, 0.0_m}};

    LaneGroup lane_group1{1001, LaneGroup::Type::kUnknown};
    AstasMap map;
    map.AddLaneGroup(lane_group1);
    map.AddLane(1001, lane);

    StaticChunkList chunks = chunking_.InitializeChunks(map);
    EXPECT_EQ(chunks.size(), 10);
}

TEST_F(PolylineChunkingMapTest, GivenLanesInSingleChunk_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    using environment::map::AstasMap;
    using environment::map::Lane;
    using environment::map::LaneGroup;

    Lane lane1{1};
    lane1.center_line = {{0.0_m, 0.0_m, 0.0_m}, {10.0_m, 10.0_m, 0.0_m}, {20.0_m, 20.0_m, 20.0_m}};  // Chunk{0, 0}
    Lane lane2{2};
    lane2.center_line = {{60.0_m, 50.0_m, 0.0_m}, {55.0_m, 55.0_m, 0.0_m}};  // Chunk{1, 1}
    Lane lane3{3};
    lane3.center_line = {{0.0_m, 0.0_m, 0.0_m}, {10.0_m, 0.0_m, 0.0_m}, {20.0_m, 0.0_m, 0.0_m}};  // Chunk{0, 0}
    LaneGroup lane_group1{1001, LaneGroup::Type::kUnknown};
    LaneGroup lane_group2{1002, LaneGroup::Type::kUnknown};

    AstasMap map;

    map.AddLaneGroup(lane_group1);
    map.AddLane(1001, lane1);
    map.AddLane(1001, lane2);

    map.AddLaneGroup(lane_group2);
    map.AddLane(1002, lane3);

    const environment::map::LaneGroups lane_groups = map.GetLaneGroups();

    StaticChunkList chunks = chunking_.InitializeChunks(map);

    auto lane_group_count = [](const MapChunk& chunk, mantle_api::UniqueId id) {
        return std::count_if(chunk.lane_groups.begin(), chunk.lane_groups.end(), [&id](const auto* lane_group) {
            return lane_group->id == id;
        });
    };

    ASSERT_EQ(2, chunks.size());

    EXPECT_EQ(1, chunks.count({0, 0}));
    EXPECT_EQ(1, chunks.count({1, 1}));

    const auto& chunk_00{chunks.at({0, 0})};
    EXPECT_EQ(2, chunk_00.lane_groups.size());
    EXPECT_EQ(1, lane_group_count(chunk_00, 1001));
    EXPECT_EQ(1, lane_group_count(chunk_00, 1002));

    const auto& chunk_11{chunks.at({1, 1})};
    EXPECT_EQ(1, chunk_11.lane_groups.size());
    EXPECT_EQ(1, lane_group_count(chunk_11, 1001));
}

TEST_F(PolylineChunkingMapTest,
       GivenLanesInMultipleChunkButPointsInAdjacentChunks_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    using environment::map::AstasMap;
    using environment::map::Lane;
    using environment::map::LaneGroup;

    Lane lane1{1};
    // Chunk {0, 0} / {1, 0}
    lane1.center_line = {{0.0_m, 0.0_m, 0.0_m},
                         {10.0_m, 10.0_m, 0.0_m},
                         {20.0_m, 20.0_m, 20.0_m},
                         {60.0_m, 20.0_m, 0.0_m},
                         {80.0_m, 25.0_m, 0.0_m}};
    Lane lane2{2};
    // Chunk {1, 1} / {1, 2}
    lane2.center_line = {{60.0_m, 50.0_m, 0.0_m}, {55.0_m, 55.0_m, 0.0_m}, {60.0_m, 102.0_m, 0.0_m}};
    Lane lane3{3};
    // Chunk {0, 0} / {-1, 0}
    lane3.center_line = {
        {0.0_m, 0.0_m, 0.0_m}, {10.0_m, 0.0_m, 0.0_m}, {20.0_m, 0.0_m, 0.0_m}, {-10.0_m, 0.0_m, 0.0_m}};
    LaneGroup lane_group1{1001, LaneGroup::Type::kUnknown};
    LaneGroup lane_group2{1002, LaneGroup::Type::kUnknown};

    AstasMap map;

    map.AddLaneGroup(lane_group1);
    map.AddLane(1001, lane1);
    map.AddLane(1001, lane2);

    map.AddLaneGroup(lane_group2);
    map.AddLane(1002, lane3);

    const environment::map::LaneGroups lane_groups = map.GetLaneGroups();
    StaticChunkList chunks = chunking_.InitializeChunks(map);

    auto lane_group_count = [](const MapChunk& chunk, mantle_api::UniqueId id) {
        return std::count_if(chunk.lane_groups.begin(), chunk.lane_groups.end(), [&id](const auto* lane_group) {
            return lane_group->id == id;
        });
    };

    ASSERT_EQ(5, chunks.size());

    EXPECT_EQ(1, chunks.count({0, 0}));
    EXPECT_EQ(1, chunks.count({1, 0}));
    EXPECT_EQ(1, chunks.count({1, 1}));
    EXPECT_EQ(1, chunks.count({1, 2}));
    EXPECT_EQ(1, chunks.count({-1, 0}));

    const auto& chunk_00{chunks.at({0, 0})};
    EXPECT_EQ(2, chunk_00.lane_groups.size());
    EXPECT_EQ(1, lane_group_count(chunk_00, 1001));
    EXPECT_EQ(1, lane_group_count(chunk_00, 1002));

    const auto& chunk_10{chunks.at({1, 0})};
    EXPECT_EQ(1, chunk_10.lane_groups.size());
    EXPECT_EQ(1, lane_group_count(chunk_10, 1001));

    const auto& chunk_11{chunks.at({1, 1})};
    EXPECT_EQ(1, chunk_11.lane_groups.size());
    EXPECT_EQ(1, lane_group_count(chunk_11, 1001));

    const auto& chunk_12{chunks.at({1, 2})};
    EXPECT_EQ(1, chunk_12.lane_groups.size());
    EXPECT_EQ(1, lane_group_count(chunk_12, 1001));

    const auto& chunk_m10{chunks.at({-1, 0})};
    EXPECT_EQ(1, chunk_m10.lane_groups.size());
    EXPECT_EQ(1, lane_group_count(chunk_m10, 1002));
}

TEST_F(PolylineChunkingMapTest,
       GivenLanesInMultipleChunkAndPointsNotOnlyInAdjacentChunks_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    using environment::map::AstasMap;
    using environment::map::Lane;
    using environment::map::LaneBoundary;
    using environment::map::LaneGroup;

    Lane lane1{1};
    // Chunk {0, 0} / {1, 0} / {2, 1} -- intersecting {2, 0}
    lane1.center_line = {{0.0_m, 0.0_m, 0.0_m},
                         {10.0_m, 10.0_m, 0.0_m},
                         {20.0_m, 20.0_m, 20.0_m},
                         {60.0_m, 20.0_m, 0.0_m},
                         {80.0_m, 25.0_m, 0.0_m},
                         {120.0_m, 55.0_m, 0.0_m}};
    Lane lane2{2};
    // Chunk {1, 1} / {1, 2}
    lane2.center_line = {{60.0_m, 50.0_m, 0.0_m}, {55.0_m, 55.0_m, 0.0_m}, {60.0_m, 102.0_m, 0.0_m}};
    Lane lane3{3};
    // Chunk {0, 0} / {-1, 0}
    lane3.center_line = {
        {0.0_m, 0.0_m, 0.0_m}, {10.0_m, 0.0_m, 0.0_m}, {20.0_m, 0.0_m, 0.0_m}, {-10.0_m, 0.0_m, 0.0_m}};
    Lane lane4{4};
    // Chunk {2, 1} / {3, 1} / {4, 1} / {4, 2} / {5, 2} / {5, 3} / {6, 2} / {6, 3}
    // Last line segment goes "directly" from {5, 2} to {6, 3} and there also touches {5, 3} and {6, 2}
    lane4.center_line = {
        {111.0_m, 60.0_m, 0.0_m}, {220.0_m, 90.0_m, 0.0_m}, {275.0_m, 125.0_m, 0.0_m}, {325.0_m, 175.0_m, 0.0_m}};
    // make this lane boundary
    Lane lane5{5};
    // Chunk {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {6, 5} / {6, 6}
    lane5.center_line = {{131.0_m, 88.0_m, 0.0_m}, {333.0_m, 333.0_m, 0.0_m}};
    LaneGroup lane_group1{1001, LaneGroup::Type::kUnknown};
    LaneGroup lane_group2{1002, LaneGroup::Type::kUnknown};
    LaneGroup lane_group3{1003, LaneGroup::Type::kUnknown};

    AstasMap map;

    map.AddLaneGroup(lane_group1);
    map.AddLane(1001, lane1);
    map.AddLane(1001, lane2);

    map.AddLaneGroup(lane_group2);
    map.AddLane(1002, lane3);
    map.AddLane(1002, lane4);

    map.AddLaneGroup(lane_group3);
    map.AddLane(1003, lane5);

    const environment::map::LaneGroups lane_groups = map.GetLaneGroups();
    StaticChunkList chunks = chunking_.InitializeChunks(map);

    auto lane_group_count = [](const MapChunk& chunk, mantle_api::UniqueId id) {
        return std::count_if(chunk.lane_groups.begin(), chunk.lane_groups.end(), [&id](const auto* lane_group) {
            return lane_group->id == id;
        });
    };

    auto check_chunk_for_lane_groups = [&chunks, &lane_group_count](const ChunkKey& key,
                                                                    std::vector<mantle_api::UniqueId> ids) {
        const auto& chunk{chunks.at(key)};
        EXPECT_EQ(ids.size(), chunk.lane_groups.size()) << fmt::format("Error in key {}", key);
        std::for_each(ids.begin(), ids.end(), [&chunk, &lane_group_count, &key](const auto id) {
            EXPECT_EQ(1, lane_group_count(chunk, id)) << fmt::format("Error for key {} - id {}", key, id);
        });
    };

    ASSERT_EQ(23, chunks.size());

    EXPECT_EQ(1, chunks.count({0, 0}));
    EXPECT_EQ(1, chunks.count({1, 0}));
    EXPECT_EQ(1, chunks.count({1, 1}));
    EXPECT_EQ(1, chunks.count({1, 2}));
    EXPECT_EQ(1, chunks.count({-1, 0}));
    EXPECT_EQ(1, chunks.count({2, 0}));
    EXPECT_EQ(1, chunks.count({2, 1}));

    EXPECT_EQ(1, chunks.count({3, 1}));
    EXPECT_EQ(1, chunks.count({4, 1}));
    EXPECT_EQ(1, chunks.count({4, 2}));
    EXPECT_EQ(1, chunks.count({5, 2}));
    EXPECT_EQ(1, chunks.count({5, 3}));
    EXPECT_EQ(1, chunks.count({6, 2}));
    EXPECT_EQ(1, chunks.count({6, 3}));

    EXPECT_EQ(1, chunks.count({2, 2}));
    EXPECT_EQ(1, chunks.count({3, 2}));
    EXPECT_EQ(1, chunks.count({3, 3}));
    EXPECT_EQ(1, chunks.count({4, 3}));
    EXPECT_EQ(1, chunks.count({4, 4}));
    EXPECT_EQ(1, chunks.count({5, 4}));
    EXPECT_EQ(1, chunks.count({5, 5}));
    EXPECT_EQ(1, chunks.count({6, 5}));
    EXPECT_EQ(1, chunks.count({6, 6}));

    check_chunk_for_lane_groups({0, 0}, {1001, 1002});
    check_chunk_for_lane_groups({2, 1}, {1001, 1002, 1003});

    check_chunk_for_lane_groups({1, 0}, {1001});
    check_chunk_for_lane_groups({1, 1}, {1001});
    check_chunk_for_lane_groups({1, 2}, {1001});
    check_chunk_for_lane_groups({2, 0}, {1001});

    check_chunk_for_lane_groups({-1, 0}, {1002});
    check_chunk_for_lane_groups({3, 1}, {1002});
    check_chunk_for_lane_groups({4, 1}, {1002});
    check_chunk_for_lane_groups({4, 2}, {1002});
    check_chunk_for_lane_groups({5, 2}, {1002});
    check_chunk_for_lane_groups({5, 3}, {1002});
    check_chunk_for_lane_groups({6, 2}, {1002});
    check_chunk_for_lane_groups({6, 3}, {1002});

    check_chunk_for_lane_groups({2, 2}, {1003});
    check_chunk_for_lane_groups({3, 2}, {1003});
    check_chunk_for_lane_groups({3, 3}, {1003});
    check_chunk_for_lane_groups({4, 3}, {1003});
    check_chunk_for_lane_groups({4, 4}, {1003});
    check_chunk_for_lane_groups({5, 4}, {1003});
    check_chunk_for_lane_groups({5, 5}, {1003});
    check_chunk_for_lane_groups({6, 5}, {1003});
    check_chunk_for_lane_groups({6, 6}, {1003});
}

TEST_F(
    PolylineChunkingMapTest,
    GivenLanesInMultipleChunkWithJumpingChunksViaBoundariesAndOrdering_WhenInitializeChunks_ThenNumberOfChunksCorrect)
{
    using environment::map::AstasMap;
    using environment::map::Lane;
    using environment::map::LaneBoundary;
    using environment::map::LaneGroup;

    Lane lane1{0};
    // Chunk {2, 1} / {2, 2} / {3, 2} / {3, 3} / {4, 3} / {4, 4} / {5, 4} / {5, 5} / {6, 5} / {6, 6}
    // also tests the re-ordering of the chunk box that is looped over
    LaneBoundary left_lane_boundary_1{2001,
                                      LaneBoundary::Type::kSolidLine,
                                      LaneBoundary::Color::kWhite,
                                      {{{333.0_m, 333.0_m, 0.0_m}, 0.5, 0.1}, {{131.0_m, 88.0_m, 0.0_m}, 0.5, 0.1}}};

    // Adds Chunk {3, 4}
    LaneBoundary left_lane_boundary_2{2003,
                                      LaneBoundary::Type::kSolidLine,
                                      LaneBoundary::Color::kWhite,
                                      {{{165.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}, {{170.0_m, 225.0_m, 0.0_m}, 0.5, 0.1}}};

    // Adds Chunk{6, 5}
    LaneBoundary right_lane_boundary_1{2002,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{125.0_m, 95.0_m, 0.0_m}, 0.5, 0.1}, {{320.0_m, 330.0_m, 0.0_m}, 0.5, 0.1}}};

    mantle_api::UniqueId lane_group_1_id = 1001;
    lane1.left_lane_boundaries.emplace_back(2001);
    lane1.left_lane_boundaries.emplace_back(2003);
    lane1.right_lane_boundaries.emplace_back(2002);

    Lane lane2{1};
    // Chunk {2, 4} / {3, 4}
    LaneBoundary lane2_left_boundary_1{2005,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{125.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {2, 3} / {3, 3}
    LaneBoundary lane2_left_boundary_2{2006,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{125.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {2, 5} / {3, 5}
    LaneBoundary lane2_right_boundary_1{2007,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{125.0_m, 260.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 270.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {2, 6} / {3, 6}
    LaneBoundary lane2_right_boundary_2{2008,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{125.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}, {{155.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}}};

    mantle_api::UniqueId lane_group_2_id = 1002;
    lane2.left_lane_boundaries = {2005, 2006};
    lane2.right_lane_boundaries = {2007, 2008};

    Lane lane3{2};
    // Chunk {8, 4} / {9, 4}
    LaneBoundary lane3_left_boundary_1{2010,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{405.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 220.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {8, 3} / {9, 3}
    LaneBoundary lane3_left_boundary_2{2011,
                                       LaneBoundary::Type::kSolidLine,
                                       LaneBoundary::Color::kWhite,
                                       {{{405.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 180.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {8, 5} / {9, 5}
    LaneBoundary lane3_right_boundary_1{2012,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{405.0_m, 260.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 270.0_m, 0.0_m}, 0.5, 0.1}}};
    // Chunk {8, 6} / {9, 6}
    LaneBoundary lane3_right_boundary_2{2013,
                                        LaneBoundary::Type::kSolidLine,
                                        LaneBoundary::Color::kWhite,
                                        {{{405.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}, {{455.0_m, 310.0_m, 0.0_m}, 0.5, 0.1}}};
    lane3.left_lane_boundaries = {2010, 2011};
    lane3.right_lane_boundaries = {2012, 2013};

    AstasMap map;

    map.AddLaneGroup({lane_group_1_id, LaneGroup::Type::kUnknown});
    map.AddLane(lane_group_1_id, lane1);
    map.AddLaneBoundary(lane_group_1_id, left_lane_boundary_1);
    map.AddLaneBoundary(lane_group_1_id, right_lane_boundary_1);
    map.AddLaneBoundary(lane_group_1_id, left_lane_boundary_2);

    map.AddLaneGroup({lane_group_2_id, LaneGroup::Type::kUnknown});
    map.AddLane(lane_group_2_id, lane2);
    map.AddLane(lane_group_2_id, lane3);
    map.AddLaneBoundary(lane_group_2_id, lane2_left_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane2_left_boundary_2);
    map.AddLaneBoundary(lane_group_2_id, lane2_right_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane2_right_boundary_2);
    map.AddLaneBoundary(lane_group_2_id, lane3_left_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane3_left_boundary_2);
    map.AddLaneBoundary(lane_group_2_id, lane3_right_boundary_1);
    map.AddLaneBoundary(lane_group_2_id, lane3_right_boundary_2);

    const environment::map::LaneGroups lane_groups = map.GetLaneGroups();
    StaticChunkList chunks = chunking_.InitializeChunks(map);

    auto lane_group_count = [](const MapChunk& chunk, mantle_api::UniqueId id) {
        return std::count_if(chunk.lane_groups.begin(), chunk.lane_groups.end(), [&id](const auto* lane_group) {
            return lane_group->id == id;
        });
    };

    auto check_chunk_for_lane_groups = [&chunks, &lane_group_count](const ChunkKey& key,
                                                                    std::vector<mantle_api::UniqueId> ids) {
        const auto& chunk{chunks.at(key)};
        EXPECT_EQ(ids.size(), chunk.lane_groups.size()) << fmt::format("Error in key {}", key);
        std::for_each(ids.begin(), ids.end(), [&chunk, &lane_group_count, &key](const auto id) {
            EXPECT_EQ(1, lane_group_count(chunk, id)) << fmt::format("Error for key {} - id {}", key, id);
        });
    };

    ASSERT_EQ(26, chunks.size());

    EXPECT_EQ(1, chunks.count({2, 1}));
    EXPECT_EQ(1, chunks.count({2, 2}));
    EXPECT_EQ(1, chunks.count({2, 3}));
    EXPECT_EQ(1, chunks.count({2, 4}));
    EXPECT_EQ(1, chunks.count({2, 5}));
    EXPECT_EQ(1, chunks.count({2, 6}));
    EXPECT_EQ(1, chunks.count({3, 2}));
    EXPECT_EQ(1, chunks.count({3, 3}));
    EXPECT_EQ(1, chunks.count({3, 4}));
    EXPECT_EQ(1, chunks.count({3, 5}));
    EXPECT_EQ(1, chunks.count({3, 6}));
    EXPECT_EQ(1, chunks.count({4, 3}));
    EXPECT_EQ(1, chunks.count({4, 4}));
    EXPECT_EQ(1, chunks.count({5, 4}));
    EXPECT_EQ(1, chunks.count({5, 5}));
    EXPECT_EQ(1, chunks.count({5, 6}));
    EXPECT_EQ(1, chunks.count({6, 5}));
    EXPECT_EQ(1, chunks.count({6, 6}));

    EXPECT_EQ(1, chunks.count({8, 3}));
    EXPECT_EQ(1, chunks.count({8, 4}));
    EXPECT_EQ(1, chunks.count({8, 5}));
    EXPECT_EQ(1, chunks.count({8, 6}));
    EXPECT_EQ(1, chunks.count({9, 3}));
    EXPECT_EQ(1, chunks.count({9, 4}));
    EXPECT_EQ(1, chunks.count({9, 5}));
    EXPECT_EQ(1, chunks.count({9, 6}));

    check_chunk_for_lane_groups({2, 1}, {lane_group_1_id});
    check_chunk_for_lane_groups({2, 2}, {lane_group_1_id});
    check_chunk_for_lane_groups({2, 3}, {lane_group_2_id});
    check_chunk_for_lane_groups({2, 4}, {lane_group_2_id});
    check_chunk_for_lane_groups({2, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({2, 6}, {lane_group_2_id});
    check_chunk_for_lane_groups({3, 2}, {lane_group_1_id});
    check_chunk_for_lane_groups({3, 3}, {lane_group_1_id, lane_group_2_id});
    check_chunk_for_lane_groups({3, 4}, {lane_group_1_id, lane_group_2_id});
    check_chunk_for_lane_groups({3, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({3, 6}, {lane_group_2_id});
    check_chunk_for_lane_groups({4, 3}, {lane_group_1_id});
    check_chunk_for_lane_groups({4, 4}, {lane_group_1_id});
    check_chunk_for_lane_groups({5, 4}, {lane_group_1_id});
    check_chunk_for_lane_groups({5, 5}, {lane_group_1_id});
    check_chunk_for_lane_groups({5, 6}, {lane_group_1_id});
    check_chunk_for_lane_groups({6, 5}, {lane_group_1_id});
    check_chunk_for_lane_groups({6, 6}, {lane_group_1_id});

    check_chunk_for_lane_groups({8, 3}, {lane_group_2_id});
    check_chunk_for_lane_groups({8, 4}, {lane_group_2_id});
    check_chunk_for_lane_groups({8, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({8, 6}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 3}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 4}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 5}, {lane_group_2_id});
    check_chunk_for_lane_groups({9, 6}, {lane_group_2_id});
}

TEST_F(PolylineChunkingMapTest, GivenPolylineChunk_WhenSetChunkSizeToZero_ThenExpectException)
{
    EXPECT_TRUE(ExpectExceptionContains<EnvironmentException>(
        [&]() { chunking_.SetChunkSize(0); },
        "Chunk size must be greater than zero, "
        "please check the ChunkGridSize value of your User Settings and ensure that this contains a valid value."));
}

}  // namespace astas::environment::chunking
