/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_BOOSTASTASCONVERSION_H
#define GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_BOOSTASTASCONVERSION_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/linestring.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <glm/vec3.hpp>

#include <vector>

// AstasMap <-> boost
BOOST_GEOMETRY_REGISTER_POINT_2D(glm::dvec3, double, boost::geometry::cs::cartesian, x, y)
BOOST_GEOMETRY_REGISTER_LINESTRING(std::vector<glm::dvec3>)

#endif  // GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_BOOSTASTASCONVERSION_H
