/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Chunking/Internal/polyline_chunking.h"

#include <MantleAPI/Common/vector.h>
#include <gtest/gtest.h>

namespace astas::environment::chunking
{
using units::literals::operator""_m;

class PolylineChunkingBaseTest : public testing::Test
{
  protected:
    PolylineChunkingBaseTest() { chunking_.SetChunkSize(50.0); }

    PolylineChunking chunking_;
};

TEST_F(PolylineChunkingBaseTest,
       GivenChunkingWithSize50x50_WhenGetChunkKeyForZeroCoordinate_ThenCorrectChunkKeyReturned)
{
    const ChunkKey expected_key{0, 0};
    EXPECT_EQ(chunking_.GetChunkKey(mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0.0_m}), expected_key);
}

TEST_F(PolylineChunkingBaseTest,
       GivenChunkingWithSize50x50_WhenGetChunkKeyForPositiveCoordinate_ThenRoundingDownChunkKey)
{
    const ChunkKey expected_key{2, 3};
    EXPECT_EQ(chunking_.GetChunkKey(mantle_api::Vec3<units::length::meter_t>{120.0_m, 151.0_m, 0.0_m}), expected_key);
}

TEST_F(PolylineChunkingBaseTest, GivenChunkingWithSize50x50_WhenGetChunkKeyForPositiveCoordinate_ThenRoundingUpChunkKey)
{
    const ChunkKey expected_key{2, 3};
    EXPECT_EQ(chunking_.GetChunkKey(mantle_api::Vec3<units::length::meter_t>{149.999_m, 190.0_m, 200.0_m}),
              expected_key);
}

TEST_F(PolylineChunkingBaseTest,
       GivenChunkingWithSize50x50_WhenGetChunkKeyForNegativeCoordinate_ThenRoundingDownChunkKey)
{
    const ChunkKey expected_key{-2, 3};
    EXPECT_EQ(chunking_.GetChunkKey(mantle_api::Vec3<units::length::meter_t>{-55.0_m, 151.0_m, 0.0_m}), expected_key);
}

TEST_F(PolylineChunkingBaseTest, GivenChunkingWithSize50x50_WhenGetChunkKeyForNegativeCoordinate_ThenRoundingUpChunkKey)
{
    const ChunkKey expected_key{-2, -3};
    EXPECT_EQ(chunking_.GetChunkKey(mantle_api::Vec3<units::length::meter_t>{-99.999_m, -145.0_m, 200.0_m}),
              expected_key);
}

TEST_F(PolylineChunkingBaseTest, GivenChunkKeys_WhenGetLowerLeft_ThenCorrectCoordinateReturned)
{
    using chunking::CoordinateType;
    EXPECT_EQ(CoordinateType(0.0_m, 0.0_m, 0.0_m), chunking_.GetLowerLeft({0, 0}));
    EXPECT_EQ(CoordinateType(50.0_m, 250.0_m, 0.0_m), chunking_.GetLowerLeft({1, 5}));
    EXPECT_EQ(CoordinateType(-50.0_m, -250.0_m, 0.0_m), chunking_.GetLowerLeft({-1, -5}));
    EXPECT_EQ(CoordinateType(-50.0_m, 150.0_m, 0.0_m), chunking_.GetLowerLeft({-1, 3}));
    EXPECT_EQ(CoordinateType(150.0_m, -50.0_m, 0.0_m), chunking_.GetLowerLeft({3, -1}));
}

TEST_F(PolylineChunkingBaseTest, GivenChunkKeys_WhenGetUpperRight_ThenCorrectCoordinateReturned)
{
    using chunking::CoordinateType;
    EXPECT_EQ(CoordinateType(50.0_m, 50.0_m, 0.0_m), chunking_.GetUpperRight({0, 0}));
    EXPECT_EQ(CoordinateType(0.0_m, 0.0_m, 0.0_m), chunking_.GetUpperRight({-1, -1}));
    EXPECT_EQ(CoordinateType(100.0_m, 300.0_m, 0.0_m), chunking_.GetUpperRight({1, 5}));
    EXPECT_EQ(CoordinateType(0.0_m, -200.0_m, 0.0_m), chunking_.GetUpperRight({-1, -5}));
    EXPECT_EQ(CoordinateType(0.0_m, 200.0_m, 0.0_m), chunking_.GetUpperRight({-1, 3}));
    EXPECT_EQ(CoordinateType(200.0_m, 0.0_m, 0.0_m), chunking_.GetUpperRight({3, -1}));
}

}  // namespace astas::environment::chunking
