/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Chunking/chunking.h"

#include "Core/Environment/AstasEnvironment/entity_repository.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <gtest/gtest.h>

namespace astas::environment::chunking
{
using units::literals::operator""_m;

class ChunkingTest : public testing::Test
{
  protected:
    void AddAndPlaceHost(const mantle_api::Vec3<units::length::meter_t>& position)
    {
        mantle_api::VehicleProperties host_properties{};
        host_properties.is_host = true;

        auto& host = repo_.Create(0, "host", host_properties);
        host.SetPosition(position);
    }

    void AddAndPlaceVehicle(const mantle_api::Vec3<units::length::meter_t>& position)
    {
        auto& vehicle = repo_.Create(entity_id_++, "traffic", mantle_api::VehicleProperties{});
        vehicle.SetPosition(position);
    }

    void AddAndPlacePedestrian(const mantle_api::Vec3<units::length::meter_t>& position)
    {
        auto& pedestrian = repo_.Create(entity_id_++, "pedestrian", mantle_api::PedestrianProperties{});
        pedestrian.SetPosition(position);
    }

    void AddAndPlaceStaticObject(const mantle_api::Vec3<units::length::meter_t>& position)
    {
        auto& object = repo_.Create(entity_id_++, "pylon", mantle_api::StaticObjectProperties());
        object.SetPosition(position);
    }

    void AddAndPlaceTrafficLight(const mantle_api::Vec3<units::length::meter_t>& position)
    {
        auto& object = repo_.Create(entity_id_++, "1.000.001", mantle_api::StaticObjectProperties());
        object.SetPosition(position);
    }

    DefaultMapChunker chunker_{};
    service::utility::UniqueIdProvider id_provider_{};
    environment::api::EntityRepository repo_{&id_provider_};
    std::unique_ptr<environment::map::AstasMap> astas_map_{test_utils::MapCatalogue::MapStraightRoad2km()};

    mantle_api::UniqueId entity_id_{1};
};

TEST_F(ChunkingTest, GivenHostOnMap_WhenGettingSingleWorldChunk_ThenOneChunkWithHostReturned)
{
    mantle_api::Vec3<units::length::meter_t> host_position{4.0_m, 10.0_m, 0.0_m};
    AddAndPlaceHost(host_position);

    chunker_.Initialize(*astas_map_, 50, 0);
    auto world_chunks = chunker_.GetWorldChunks(repo_.GetEntities(), host_position);

    ASSERT_EQ(1, world_chunks.size());
    ASSERT_EQ(1, world_chunks.front().entities.size());
    EXPECT_EQ("host", world_chunks.front().entities.front()->GetName());
    EXPECT_EQ(host_position, world_chunks.front().entities.front()->GetPosition());

    EXPECT_FALSE(world_chunks.front().lane_groups.empty());
}

TEST_F(ChunkingTest, GivenHostOnMapWithAnotherVehicleInFront_WhenGettingSingleWorldChunk_ThenOneChunkWithHostReturned)
{
    mantle_api::Vec3<units::length::meter_t> host_position{4.0_m, 10.0_m, 0.0_m};
    AddAndPlaceHost(host_position);

    mantle_api::Vec3<units::length::meter_t> vehicle_position{4.0_m, 20.0_m, 0.0_m};
    AddAndPlaceVehicle(vehicle_position);

    chunker_.Initialize(*astas_map_, 50, 0);
    auto world_chunks = chunker_.GetWorldChunks(repo_.GetEntities(), host_position);

    ASSERT_EQ(1, world_chunks.size());
    ASSERT_EQ(2, world_chunks.at(0).entities.size());

    const auto& host_entity = world_chunks.at(0).entities.at(0);
    EXPECT_EQ(0, host_entity->GetUniqueId());
    EXPECT_EQ(host_position, host_entity->GetPosition());

    const auto& vehicle_entity = world_chunks.at(0).entities.at(1);
    EXPECT_EQ(1, vehicle_entity->GetUniqueId());
    EXPECT_EQ(vehicle_position, vehicle_entity->GetPosition());
}

TEST_F(ChunkingTest, GivenMultipleEntities_WhenGettingWorldChunks_ThenAllEntitiesContained)
{
    mantle_api::Vec3<units::length::meter_t> host_position{4.0_m, 10.0_m, 0.0_m};
    AddAndPlaceHost(host_position);

    mantle_api::Vec3<units::length::meter_t> vehicle_position{4.0_m, 20.0_m, 0.0_m};
    AddAndPlaceVehicle(vehicle_position);

    mantle_api::Vec3<units::length::meter_t> pedestrian_position{4.0_m, 100.0_m, 0.0_m};
    AddAndPlacePedestrian(pedestrian_position);

    mantle_api::Vec3<units::length::meter_t> pylon_position{4.0_m, 110.0_m, 0.0_m};
    AddAndPlaceStaticObject(pylon_position);

    mantle_api::Vec3<units::length::meter_t> traffic_light_position{4.0_m, 120.0_m, 0.0_m};
    AddAndPlaceTrafficLight(traffic_light_position);

    chunker_.Initialize(*astas_map_, 50, 2);
    auto world_chunks = chunker_.GetWorldChunks(repo_.GetEntities(), host_position);

    int number_of_vehicles{0};
    int number_of_pedestrians{0};
    int number_of_objects{0};
    for (const auto& chunk : world_chunks)
    {
        for (const auto& entity : chunk.entities)
        {
            if (dynamic_cast<const mantle_api::IVehicle*>(entity) != nullptr)
            {
                number_of_vehicles++;
            }
            if (dynamic_cast<const mantle_api::IPedestrian*>(entity) != nullptr)
            {
                number_of_pedestrians++;
            }
            if (dynamic_cast<const mantle_api::IStaticObject*>(entity) != nullptr)
            {
                number_of_objects++;
            }
        }
    }

    EXPECT_EQ(2, number_of_vehicles);
    EXPECT_EQ(1, number_of_pedestrians);
    EXPECT_EQ(2, number_of_objects);
}

}  // namespace astas::environment::chunking
