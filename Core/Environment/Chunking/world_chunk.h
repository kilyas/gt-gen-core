/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CHUNKING_WORLDCHUNK_H
#define GTGEN_CORE_ENVIRONMENT_CHUNKING_WORLDCHUNK_H

#include "Core/Environment/Exception/exception.h"

#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <vector>

// forward declarations
namespace astas
{

namespace environment::map
{
struct LaneGroup;
struct RoadObject;
struct TrafficSign;
struct TrafficLight;
}  // namespace environment::map

}  // namespace astas

namespace astas::environment::chunking
{

/// @brief Key of a given chunk, identified in 2 dimensions.
///
/// The lower-left position of a chunk and its key are related as follows:
///     - lower_left.x = chunk_size * i
///     - lower_left.y = chunk_size * j
struct ChunkKey
{
    std::int32_t i{0};
    std::int32_t j{0};
};

using CoordinateType = mantle_api::Vec3<units::length::meter_t>;

/// @brief Description of a Chunk
///
/// Chunks are 2D based only, their origin is the "lower left" corner.
struct MapChunk
{
    explicit MapChunk(const ChunkKey& chunk_key, const CoordinateType& lower_left_position)
        : key(chunk_key), lower_left(lower_left_position)
    {
    }

    /// @brief The integer based index / key of the chunk, determined by the chunk origin
    ChunkKey key;
    /// @brief The origin of the chunk, determining the chunk key
    CoordinateType lower_left;

    std::vector<const environment::map::LaneGroup*> lane_groups;
    std::vector<const environment::map::RoadObject*> road_objects;
    std::vector<const environment::map::TrafficSign*> traffic_signs;
    std::vector<const environment::map::TrafficLight*> traffic_lights;

    void Validate() const
    {
        if (lane_groups.empty() && road_objects.empty() && traffic_signs.empty() && traffic_lights.empty())
        {

            throw EnvironmentException(
                "MapChunk validation failed: lane groups, road objects, traffic signs and traffic lights are all"
                "empty. Please Contact GTGen support");
        }
    }
};

using StaticChunkList = std::map<ChunkKey, MapChunk>;

/// @brief Basic structure of a single chunk.
///
/// Chunks are axis-aligned bounding boxes (AABB) in the GTGen world coordinate system.
struct WorldChunk
{
    /// @brief Coordinate type used for the lower_left.
    //    using CoordinateType = mantle_api::Vec3<units::length::meter_t>;
    /// @brief Lower left corner in the axis-aligned chunk.
    CoordinateType lower_left{units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)};
    /// @brief Key of the chunk, see description of ChunkKey.
    ChunkKey key{};

    std::vector<const environment::map::LaneGroup*> lane_groups;
    std::vector<const environment::map::RoadObject*> road_objects;
    std::vector<const environment::map::TrafficSign*> traffic_signs;
    std::vector<const environment::map::TrafficLight*> traffic_lights;
    std::vector<const mantle_api::IEntity*> entities;
};

using WorldChunks = std::vector<WorldChunk>;

/// comparison operators ChunkKey

inline constexpr bool operator==(const ChunkKey& a, const ChunkKey& b)
{
    return a.i == b.i && a.j == b.j;
}
inline constexpr bool operator!=(const ChunkKey& a, const ChunkKey& b)
{
    return !(a == b);
}

/// @brief Compare 'less' for two ChunkKey elements following the logic described below.
///
/// key `a` is compared 'less' to key `b` in two dimensions, following the logic:
///     - a.i < b.i => a < b   --> Major ordering by 'i' component
///     - a.i > b.i => a > b
///     - if a.i == b.i:
///         a < b <=> a.j < b.j
///
/// @return True if a < b.
inline constexpr bool operator<(const ChunkKey& a, const ChunkKey& b)
{
    if (a.i < b.i)
    {
        return true;
    }
    else if (a.i > b.i)
    {
        return false;
    }

    return a.j < b.j;
}
inline constexpr bool operator>(const ChunkKey& a, const ChunkKey& b)
{
    return b < a;
}

inline constexpr bool operator<=(const ChunkKey& a, const ChunkKey& b)
{
    return !(b < a);
}
inline constexpr bool operator>=(const ChunkKey& a, const ChunkKey& b)
{
    return !(a < b);
}

}  // namespace astas::environment::chunking

#endif  // GTGEN_CORE_ENVIRONMENT_CHUNKING_WORLDCHUNK_H
