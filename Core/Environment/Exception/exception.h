/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_EXCEPTION_EXCEPTION_H
#define GTGEN_CORE_ENVIRONMENT_EXCEPTION_EXCEPTION_H

#include "Core/Service/MantleApiExtension/formatting.h"

#include <fmt/format.h>

#include <exception>
#include <string>

namespace astas::environment
{
class EnvironmentException final : public std::exception
{
  public:
    template <typename... Args>
    explicit EnvironmentException(const char* fmt, const Args&... args) noexcept : message_{fmt::format(fmt, args...)}
    {
    }

    const char* what() const noexcept override { return message_.c_str(); }

  private:
    std::string message_;
};

}  // namespace astas::environment

#endif  // GTGEN_CORE_ENVIRONMENT_EXCEPTION_EXCEPTION_H
