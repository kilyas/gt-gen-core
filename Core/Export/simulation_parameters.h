/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_EXPORT_SIMULATIONPARAMETERS_H
#define GTGEN_CORE_EXPORT_SIMULATIONPARAMETERS_H

#include <string>

namespace astas
{

/// @brief This struct contains all parameters to be passed to the simulator. If the simulator is started by the
/// cli this parameters represent the command line arguments, if the simulator is started by the ADP the
/// parameters might come from somewhere else
struct SimulationParameters
{
    /// @attention If you change something here, a NEW CLI VERSION is required as well!
    std::string scenario{};
    std::string custom_astas_data_directory{};
    std::string custom_log_directory{};
    std::string custom_user_settings_path{};
    std::string map_path{};
    std::string console_log_level{"Error"};
    bool use_gui{false};
    std::uint16_t control_server_port{0};
    std::uint16_t step_size_ms{10};
};

}  // namespace astas

#endif  // GTGEN_CORE_EXPORT_SIMULATIONPARAMETERS_H
